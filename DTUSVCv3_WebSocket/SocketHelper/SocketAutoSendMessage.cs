﻿using DTUSVCv3_Library;

namespace DTUSVCv3_WebSocket
{
    public class SocketAutoSendMessage
    {
        #region Constuctor
        public SocketAutoSendMessage()
        {
            m_queMessAll = new Queue<NotifySendAllMess>();
            if (m_objTaskAll == null)
            {
                m_objTaskAll = new Thread(async () => await SendMessageAll());
                m_objTaskAll.Start();
            }

            m_queMessOne = new Queue<NotifySendOneMess>();
            if (m_objTaskOne == null)
            {
                m_objTaskOne = new Thread(async () => await SendMessageOne());
                m_objTaskOne.Start();
            }
        }
        #endregion

        #region Fields
        private static NotificationsMessageHandler m_objWebSocket;
        private static Queue<NotifySendAllMess> m_queMessAll;
        private static Queue<NotifySendOneMess> m_queMessOne;
        private static SocketAutoSendMessage m_objSocketAutoSendMessage;
        private static Thread m_objTaskAll;
        private static Thread m_objTaskOne;
        #endregion

        #region Properties
        public NotificationsMessageHandler NotificationMessage
        {
            set
            {
                m_objWebSocket = value;
            }
        }

        public Queue<NotifySendAllMess> QueueSendAllMess
        {
            get
            {
                return m_queMessAll;
            }
        }

        public Queue<NotifySendOneMess> QueueSendOneMess
        {
            get
            {
                return m_queMessOne;
            }
        }

        public static SocketAutoSendMessage Instant
        {
            get
            {
                if (m_objSocketAutoSendMessage == null)
                {
                    m_objSocketAutoSendMessage = new SocketAutoSendMessage();
                }
                return m_objSocketAutoSendMessage;
            }
        }
        #endregion

        #region Methods

        /// <summary>
        /// Send Message One
        /// </summary>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        private async Task SendMessageOne()
        {
            string strMessage;
            NotifySendOneMess objMess;
            RegisterSocketSndModel objRegisterSocketSndModel;
            while (true)
            {
                try
                {
                    if ((m_queMessOne != null) && (m_objWebSocket != null) && (m_queMessOne.Count > 0) && (SystemSetting.IS_SYSTEM_MAINTENANCE == false))
                    {
                        objMess = m_queMessOne.Dequeue();
                        if (objMess != null)
                        {
                            strMessage = objMess.Data.ToJSON();
                            objRegisterSocketSndModel = new RegisterSocketSndModel
                            {
                                ActionId = objMess.ActionId,
                                GUIScreen = (int)objMess.Screen,
                                IsSuccsess = true,
                                Message = strMessage,
                                Type = objMess.MessageType,
                                MemberId = objMess.MemberId,
                            };
                            await m_objWebSocket.SendMessageAsync(objRegisterSocketSndModel);
                        }
                    }
                    else
                    {
                        Thread.Sleep(1000);
                    }
                }
                catch
                {
                    // No need process
                }
                Thread.Sleep(50);
            }
        }

        /// <summary>
        /// Send Message
        /// </summary>
        private async Task SendMessageAll()
        {
            string strMessage;
            NotifySendAllMess objMess;

            //	This function will always work.
            //	If there are no messages in the queue to send, it will pause for 10 milliseconds and return to the loop
            //	If a new message appears in the queue, it proceeds to get the message out and send it, then waits for 100 milliseconds before continuing the loop.
            //	=> Only allow sending 10 notifications in 1s
            RegisterSocketSndModel objRegisterSocketSndModel;
            while (true)
            {
                try
                {
                    if ((m_queMessAll != null) && (m_objWebSocket != null) && (m_queMessAll.Count > 0) && (SystemSetting.IS_SYSTEM_MAINTENANCE == false))
                    {
                        objMess = m_queMessAll.Dequeue();
                        if (objMess != null)
                        {
                            strMessage = objMess.Data.ToJSON();
                            objRegisterSocketSndModel = new RegisterSocketSndModel
                            {
                                ActionId = objMess.ActionId,
                                GUIScreen = (int)objMess.Screen,
                                IsSuccsess = true,
                                Message = strMessage,
                                Type = objMess.MessageType
                            };
                            await m_objWebSocket.SendMessageAsync(objRegisterSocketSndModel);
                        }
                    }
                    else
                    {
                        Thread.Sleep(1000);
                    }
                }
                catch
                {
                    // No need process
                }
                Thread.Sleep(50);
            }
        }
        #endregion
    }
}
