﻿//-------------------------------------------------------------------------
// Create               : Phan Ngoc Huong
//
// Create Date          : 17/12/2022
//
// Description          : Sockets Extension
//
// Editing history      :
//-------------------------------------------------------------------------
using DTUSVCv3_MemoryData;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.Extensions.Primitives;
using System.Net.WebSockets;

namespace DTUSVCv3_WebSocket
{
    public static partial class SocketsExtension
    {
        private class WebSocketManagerMiddleware
        {
            private readonly RequestDelegate m_objNext;
            private WebSocketHandler m_objWebSocketHandler { get; set; }

            public WebSocketManagerMiddleware(RequestDelegate x_objNext, WebSocketHandler x_objWebSocketHandler)
            {
                m_objNext = x_objNext;
                m_objWebSocketHandler = x_objWebSocketHandler;
            }

            /// <summary>
            /// Get Client IP Address
            /// </summary>
            public static string GetClientIPAddress(HttpContext x_objContext)
            {
                string strIp;

                try
                {
                    strIp = string.Empty;
                    if (string.IsNullOrWhiteSpace(x_objContext.Request.Headers["X-Forwarded-For"]) == false)
                    {
                        strIp = x_objContext.Request.Headers["X-Forwarded-For"];
                    }
                    else
                    {
                        strIp = x_objContext.Request.HttpContext.Features.Get<IHttpConnectionFeature>().RemoteIpAddress.ToString();
                    }
                    return strIp;
                }
                catch
                {
                    return string.Empty;
                }
            }

            /// <summary>
            /// Invoke
            /// </summary>
            public async Task Invoke(HttpContext x_objContext)
            {
                StringValues objToken;
                string strToken;
                string strIp;
                string strMemberId;
                int nLoginType;
                WebSocket objSocket;

                if ((x_objContext == null) || (x_objContext.WebSockets.IsWebSocketRequest == false))
                {
                    return;
                }

                try
                {
                    // Get connect socket query
                    x_objContext.Request.Query.TryGetValue("token", out objToken);
                    strToken = objToken;
                    if (string.IsNullOrWhiteSpace(strToken) == false)
                    {
                        nLoginType = LoginData.CheckLogin(strToken);

                        if (nLoginType == 1)
                        {
                            strIp = GetClientIPAddress(x_objContext);
                            strMemberId = LoginData.GetMemberIdByToken(strToken);
                            objSocket = await x_objContext.WebSockets.AcceptWebSocketAsync();
                            await m_objWebSocketHandler.OnConnected(objSocket, strMemberId, strIp, strToken);
                            await Receive(objSocket, async (objResult, arrBuf) =>
                            {
                                if (objResult.MessageType == WebSocketMessageType.Text)
                                {
                                    m_objWebSocketHandler.Receive(objSocket, objResult, arrBuf);
                                    return;
                                }
                                else if (objResult.MessageType == WebSocketMessageType.Close)
                                {
                                    await m_objWebSocketHandler.OnDisConnected(objSocket);
                                    return;
                                }
                            });
                        }
                    }
                }
                catch
                {
                    // No need process
                }
            }

            /// <summary>
            /// Receive
            /// </summary>
            private async Task Receive(WebSocket x_objSocket, Action<WebSocketReceiveResult, byte[]> x_arrHandleMessage)
            {
                byte[] arrBuf;
                WebSocketReceiveResult objResult;
                ArraySegment<byte> arrSegmentBuf;
                CancellationToken eCancellationToken;

                try
                {
                    if (x_objSocket == null)
                    {
                        return;
                    }

                    arrBuf = new byte[1024];
                    eCancellationToken = CancellationToken.None;
                    while (x_objSocket.State == WebSocketState.Open)
                    {
                        arrSegmentBuf = new ArraySegment<byte>(arrBuf);
                        objResult = await x_objSocket.ReceiveAsync(arrSegmentBuf, eCancellationToken);
                        x_arrHandleMessage(objResult, arrBuf);
                    }
                }
                catch
                {
                    // No need process
                }
            }
        }
    }
}
