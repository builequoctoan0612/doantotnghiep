﻿//-------------------------------------------------------------------------
// Create               : Phan Ngoc Huong
//
// Create Date          : 17/12/2022
//
// Description          : Notifications Message Handler
//
// Editing history      :
//-------------------------------------------------------------------------
using DTUSVCv3_Library;
using Newtonsoft.Json;
using System.Net.WebSockets;
using System.Text;

namespace DTUSVCv3_WebSocket
{
    public class NotificationsMessageHandler : WebSocketHandler
    {
        public NotificationsMessageHandler(ConnectionManager webSocketConnectionManager) : base(webSocketConnectionManager)
        {

        }

        /// <summary>
        /// On Connected
        /// </summary>
        public override async Task OnConnected(WebSocket x_objsocket, string x_sMemberId, string x_sIpAddres, string x_strToken)
        {
            await base.OnConnected(x_objsocket, x_sMemberId, x_sIpAddres, x_strToken);
        }

        /// <summary>
        /// OnDisConnected
        /// </summary>
        public override async Task OnDisConnected(WebSocket socket)
        {
            await base.OnDisConnected(socket);
        }

        /// <summary>
        /// Receive Async
        /// </summary>
        public override void Receive(WebSocket x_objSocket, WebSocketReceiveResult objResult, byte[] arrBuffer)
        {
            RegisterSocketRevModel objRegisterSocket;
            UserConnectModel objWs;
            string strMessage;
            string strResMess;
            string strSndMessage;
            string strMemberId;
            GUIScreen eScreen;
            int nIndex;

            try
            {
                strMessage = Encoding.UTF8.GetString(arrBuffer, 0, objResult.Count);
                objRegisterSocket = strMessage.JSONToObject<RegisterSocketRevModel>();
                strMemberId = GetMemberId(x_objSocket);
                strSndMessage = string.Empty;
                strResMess = string.Empty;

                if (objRegisterSocket != null)
                {
                    eScreen = EnumHelper.ToEnum(objRegisterSocket.GUIScreen, GUIScreen.UNKNOWN);
                    if (eScreen == GUIScreen.UNKNOWN)
                    {
                        return;
                    }

                    objWs = GetUserConn(x_objSocket);
                    // Resit event
                    if (objRegisterSocket.IsRegistEvent == true)
                    {
                        if (objWs != null)
                        {
                            objWs.ActionId.Add(objRegisterSocket.ActionId);
                            objWs.Screen.Add(eScreen);
                        }
                    }
                    // Remove event
                    else
                    {
                        if ((objWs != null) && (objWs.ActionId.Contains(objRegisterSocket.ActionId) == true) ||
                                                (objWs.Screen.Contains(eScreen) == true))
                        {
                            nIndex = objWs.ActionId.FindIndex(obj => obj.Equals(objRegisterSocket.ActionId) == true);
                            if (nIndex >= 0)
                            {
                                objWs.ActionId.RemoveAt(nIndex);
                            }

                            nIndex = objWs.Screen.FindIndex(obj => obj == eScreen);
                            if (nIndex >= 0)
                            {
                                objWs.Screen.RemoveAt(nIndex);
                            }
                        }
                    }
                }
            }
            catch
            {
                // No need process
            }
        }
    }

    [Serializable]
    public class RegisterSocketRevModel
    {
        [JsonProperty("actionId")]
        public string ActionId { get; set; }
        [JsonProperty("guiScreen")]
        public int GUIScreen { get; set; }
        [JsonProperty("isRegistEvent")]
        public bool IsRegistEvent { get; set; }
    }

    [Serializable]
    public class RegisterSocketSndModel
    {
        public RegisterSocketSndModel()
        {
            IsSuccsess = true;
        }

        [JsonProperty("actionId")]
        public string ActionId { get; set; }
        [JsonProperty("memberId")]
        public string MemberId { get; set; }
        [JsonProperty("guiScreen")]
        public int GUIScreen { get; set; }
        // 1: Add, 2: Modify, 0: Delete
        [JsonProperty("type")]
        public int Type { get; set; }
        [JsonProperty("message")]
        public string Message { get; set; }
        [JsonProperty("isSuccsess")]
        public bool IsSuccsess { get; set; }
    }
}
