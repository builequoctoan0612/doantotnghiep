﻿//-------------------------------------------------------------------------
// Create               : Phan Ngoc Huong
//
// Create Date          : 17/12/2022
//
// Description          : Sockets Extension
//
// Editing history      :
//-------------------------------------------------------------------------
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using System.Reflection;

namespace DTUSVCv3_WebSocket
{ 
    public static partial class SocketsExtension
    {
        public static IApplicationBuilder MapWebSocketManager(this IApplicationBuilder x_objApp, PathString x_objPath, WebSocketHandler x_objHandler)
        {
            return x_objApp.Map(x_objPath, (objApp) => objApp.UseMiddleware<WebSocketManagerMiddleware>(x_objHandler));
        }

        /// <summary>
        /// Add Web Socket Manager
        /// </summary>
        public static IServiceCollection AddWebSocketManager(this IServiceCollection x_objService, Assembly x_objAssembly = null)
        {
            Assembly objAssembly;
            x_objService.AddTransient<ConnectionManager>();
            objAssembly = x_objAssembly ?? Assembly.GetEntryAssembly();
            foreach (Type objType in objAssembly.ExportedTypes)
            {
                if (objType.GetTypeInfo().BaseType == typeof(WebSocketHandler))
                {
                    x_objService.AddSingleton(objType);
                }
            }

            return x_objService;
        }
    }
}
