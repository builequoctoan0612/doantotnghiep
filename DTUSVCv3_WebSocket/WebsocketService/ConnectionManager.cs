﻿//-------------------------------------------------------------------------
// Create               : Phan Ngoc Huong
//
// Create Date          : 17/12/2022
//
// Description          : User Connect Model
//
// Editing history      :
//-------------------------------------------------------------------------
using DTUSVCv3_Library;
using DTUSVCv3_MemoryData;
using System.Net.WebSockets;

namespace DTUSVCv3_WebSocket
{
    public class UserConnectModel
    {
        public UserConnectModel() 
        {
            ActionId = new List<string>();
            Screen = new List<GUIScreen>();
        }
        public string MemberId { get; set; }
        public string FullName { get; set; }
        public string SocketIp { get; set; }
        public string Token { get; set; }
        public List<string> ActionId { get; set; }
        public List<GUIScreen> Screen { get; set; }
        public DateTime DateConnect { get; set; }
        public WebSocket SocketConn { get; set; }
    }

    public class ConnectionManager
    {
        private static List<UserConnectModel> m_objSockets = new List<UserConnectModel>();

        /// <summary>
        /// Get Socket By Id
        /// </summary>
        public List<UserConnectModel> GetSocketById(string id)
        {
            List<UserConnectModel> socketById = new List<UserConnectModel>();
            foreach (var item in m_objSockets)
            {
                if (item.MemberId.Equals(id))
                {
                    socketById.Add(item);
                }
            }
            return socketById;
        }

        /// <summary>
        /// Get all user
        /// </summary>
        public List<UserConnectModel> GetAll()
        {
            return m_objSockets;
        }

        /// <summary>
        /// Get Id
        /// </summary>
        public string GetId(WebSocket x_objSocket)
        {
            UserConnectModel objUser;

            try
            {
                if(m_objSockets == null)
                {
                    return string.Empty;
                }

                objUser = m_objSockets.FirstOrDefault(obj => obj.SocketConn == x_objSocket);
                if (objUser == null)
                {
                    return string.Empty;
                }
                return objUser.MemberId;
            }
            catch
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Add socket
        /// </summary>
        public void AddSocket(WebSocket x_objSocket, string x_sMemberId, string x_sIpAddress, string x_strToken)
        {
            MemberDecryptModel objMember;
            UserConnectModel objUserConnectModel;

            try
            {
                objMember = MemberInfoMemory.Instant.GetMemberById(x_sMemberId);
                if ((objMember != null) && (objMember.IsMember == true))
                {
                    objUserConnectModel = new UserConnectModel
                    {
                        SocketConn = x_objSocket,
                        MemberId = x_sMemberId,
                        SocketIp = x_sIpAddress,
                        DateConnect = DateTimeHelper.GetCurrentDateTimeVN(),
                        FullName = $"{objMember.LastName} {objMember.FirstName}",
                        Token = x_strToken
                    };
                    m_objSockets.Add(objUserConnectModel);
                }
                else
                {
                    x_objSocket.CloseAsync(WebSocketCloseStatus.NormalClosure, "Bị cấm sử dụng socket", CancellationToken.None);
                }
            }
            catch
            {
                // No need process
            }
        }

        /// <summary>
        /// Remove Socket
        /// </summary>
        public async Task RemoveSocket(WebSocket x_objSocket)
        {
            UserConnectModel objUserConnect;

            try
            {
                objUserConnect = SearchUserConnect(x_objSocket);
                if (objUserConnect == null)
                {
                    return;
                }
                m_objSockets.Remove(objUserConnect);
                await objUserConnect.SocketConn.CloseAsync(WebSocketCloseStatus.NormalClosure, "Thoát khỏi socket.", CancellationToken.None);
            }
            catch
            {
                // No need process
            }
        }

        /// <summary>
        /// Remove socket by token
        /// </summary>
        /// <param name="x_strToken"></param>
        /// <returns></returns>
        public async Task RemoveSocket(string x_strToken)
        {
            UserConnectModel objUserConnect;

            try
            {
                for (int nSocketIndex = 0; nSocketIndex < m_objSockets.Count; nSocketIndex++)
                {
                    if (m_objSockets[nSocketIndex].Token.Equals(x_strToken) == true)
                    {
                        objUserConnect = m_objSockets[nSocketIndex];
                        m_objSockets.Remove(objUserConnect);
                        await objUserConnect.SocketConn.CloseAsync(WebSocketCloseStatus.NormalClosure, "Thoát khỏi socket.", CancellationToken.None);
                    }
                }
            }
            catch
            {
                // No need process
            }
        }

        /// <summary>
        /// Search User Connect
        /// </summary>
        public UserConnectModel SearchUserConnect(WebSocket x_objSocket)
        {
            try
            {
                for (int nSocketIndex = 0; nSocketIndex < m_objSockets.Count; nSocketIndex++)
                {
                    if (m_objSockets[nSocketIndex].SocketConn == x_objSocket)
                    {
                        return m_objSockets[nSocketIndex];
                    }
                }
            }
            catch
            {
                // No need process
            }
            return null;
        }
    }
}
