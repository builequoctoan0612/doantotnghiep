﻿//-------------------------------------------------------------------------
// Create               : Phan Ngoc Huong
//
// Create Date          : 17/12/2022
//
// Description          : WebSocket Handler
//
// Editing history      :
//-------------------------------------------------------------------------
using DTUSVCv3_Library;
using System.Net.WebSockets;
using System.Text;

namespace DTUSVCv3_WebSocket
{
    public abstract class WebSocketHandler
    {
        protected ConnectionManager WebSocketConnectManager { get; set; }

        public WebSocketHandler(ConnectionManager x_objWebSocketConnectManager)
        {
            WebSocketConnectManager = x_objWebSocketConnectManager;
        }

        /// <summary>
        /// On connected
        /// </summary>
        public virtual async Task OnConnected(WebSocket x_objsocket, string x_sMemberId, string x_sIpAddres, string x_strToken)
        {
            await Task.Run(() =>
            {
                WebSocketConnectManager.AddSocket(x_objsocket, x_sMemberId, x_sIpAddres, x_strToken);
            });
        }

        /// <summary>
        /// On DisConnected
        /// </summary>
        public virtual async Task OnDisConnected(WebSocket x_objSocket)
        {
            await WebSocketConnectManager.RemoveSocket(x_objSocket);
        }

        /// <summary>
        /// Send Message Async
        /// </summary>
        public async Task SendMessageAsync(List<UserConnectModel> x_lstUserConnect, RegisterSocketSndModel x_objRegisterSocketSndModel)
        {
            GUIScreen eSocketModel;
            byte[] arrEncoded;
            List<UserConnectModel> lstUserConnect;
            string strMessage;

            try
            {
                if ((x_lstUserConnect.Count == 0) || (x_objRegisterSocketSndModel == null) || (string.IsNullOrWhiteSpace(x_objRegisterSocketSndModel.Message) == true))
                {
                    return;
                }
                eSocketModel = EnumHelper.ToEnum(x_objRegisterSocketSndModel.GUIScreen, GUIScreen.UNKNOWN);
                strMessage = x_objRegisterSocketSndModel.ToJSON();
                arrEncoded = Encoding.UTF8.GetBytes(strMessage);
                // Find list socket
                if (x_objRegisterSocketSndModel.MemberId.MongoIdIsValid() == false)
                {
                    lstUserConnect = x_lstUserConnect;
                }
                else
                {
                    lstUserConnect = x_lstUserConnect.FindAll(obj => obj.MemberId.Equals(x_objRegisterSocketSndModel.MemberId) == true);
                }
                foreach (UserConnectModel objUserItem in x_lstUserConnect)
                {
                    if ((objUserItem.Screen.Contains(eSocketModel) == true) && (objUserItem.ActionId.Contains(x_objRegisterSocketSndModel.ActionId) == true))
                    {
                        try
                        {
                            await objUserItem.SocketConn.SendAsync(new ArraySegment<byte>(arrEncoded, 0, arrEncoded.Length),
                                                                WebSocketMessageType.Text, true, CancellationToken.None);
                        }
                        catch
                        {
                            // Disconnect if send failed
                            await OnDisConnected(objUserItem.SocketConn);
                        }
                    }
                }
            }
            catch
            {
                // No need process
            }
        }

        /// <summary>
        /// Send Message Async
        /// </summary>
        public async Task SendMessageAsync(WebSocket x_objWs, string x_strMessage)
        {
            byte[] arrEncoded;

            try
            {
                arrEncoded = Encoding.UTF8.GetBytes(x_strMessage);
                await x_objWs.SendAsync(new ArraySegment<byte>(arrEncoded, 0, arrEncoded.Length),
                                                                WebSocketMessageType.Text, true, CancellationToken.None);
            }
            catch
            {
                // Disconnect if send failed
                await OnDisConnected(x_objWs);
            }
        }

        /// <summary>
        /// Send Message Async
        /// </summary>
        public async Task SendMessageAsync(RegisterSocketSndModel x_objRegisterSocketSndModel)
        {
            List<UserConnectModel> lstUserConnect;

            lstUserConnect = WebSocketConnectManager.GetAll();
            if ((lstUserConnect == null) || (lstUserConnect.Count == 0))
            {
                return;
            }
            await SendMessageAsync(lstUserConnect, x_objRegisterSocketSndModel);
        }

        /// <summary>
        /// Disconnect By Member Id
        /// </summary>
        public async Task DisconnectByMemberId(string x_strMemberId)
        {
            List<UserConnectModel> lstUser;

            lstUser = WebSocketConnectManager.GetAll();
            foreach (UserConnectModel objUser in lstUser)
            {
                if (objUser.MemberId.Equals(x_strMemberId) == true)
                {
                    await OnDisConnected(objUser.SocketConn);
                }
            }
        }

        /// <summary>
        /// Get User Conn
        /// </summary>
        public UserConnectModel GetUserConn(WebSocket x_objWs)
        {
            List<UserConnectModel> lstUserConnect;

            lstUserConnect = WebSocketConnectManager.GetAll();
            if (lstUserConnect == null)
            {
                return null;
            }

            foreach (UserConnectModel objWsItem in lstUserConnect)
            {
                if (objWsItem.SocketConn == x_objWs)
                {
                    return objWsItem;
                }
            }
            return null;
        }

        /// <summary>
        /// Get Member Id
        /// </summary>
        public string GetMemberId(WebSocket x_objSocket)
        {
            string strMemberId;

            strMemberId = WebSocketConnectManager.GetId(x_objSocket);
            return strMemberId;
        }

        public abstract void Receive(WebSocket objSocket, WebSocketReceiveResult objResult, byte[] arrBuffer);
    }
}
