﻿using DTUSVCv3_Library;
using MongoDB.Driver;

namespace DTUSVCv3_EntityFramework
{
    public class AppDbContext
    {
        private readonly IMongoDatabase m_objDataBase;

        public AppDbContext()
        {
            MongoClient objMongoClient;

            objMongoClient = new MongoClient(DatabaseConnect.DATA_BASE_PATH);
            m_objDataBase = objMongoClient.GetDatabase(DatabaseConnect.DATA_BASE_NAME);
            DbCollertionManager.ObjDataBase = m_objDataBase;
        }
    }

    public static class DbCollertionManager
    {
        private static IMongoDatabase m_objDataBase;

        public static IMongoDatabase ObjDataBase
        {
            set
            {
                m_objDataBase = value;
            }
        }

        /// <summary>
        /// Get Collection
        /// </summary>
        public static IMongoCollection<T> GetDbCollection<T>() where T : class
        {
            string strCollectionName;
            IMongoCollection<T> objMongoCollection;
            Type objType;

            if (m_objDataBase == null)
            {
                new AppDbContext();
            }
            objType = typeof(T);
            strCollectionName = objType.Name;
            objMongoCollection = m_objDataBase.GetCollection<T>(strCollectionName);
            return objMongoCollection;
        }
    }
}
