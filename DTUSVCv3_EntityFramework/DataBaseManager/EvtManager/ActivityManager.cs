﻿using DTUSVCv3_Library;
using MongoDB.Driver;

namespace DTUSVCv3_EntityFramework
{
    public class ActivityManager
    {
        #region Constructor
        public ActivityManager()
        {
            m_objEventCheckLists = DbCollertionManager.GetDbCollection<ActivityChecklist>();
            m_objCommunityRegistEvt = DbCollertionManager.GetDbCollection<CommunityRegistEvt>();
            m_objEventInfo = DbCollertionManager.GetDbCollection<EventInfo>();
            m_objLeaveOfAbsence = DbCollertionManager.GetDbCollection<LeaveOfAbsence>();
            m_objMemberRegistEvt = DbCollertionManager.GetDbCollection<MemberRegistEvt>();
            m_objMemberSupportEvents = DbCollertionManager.GetDbCollection<MemberSupportEvents>();
            m_objCertificateTextFont = DbCollertionManager.GetDbCollection<CertificateTextFont>();
            m_objCertificateSettingText = DbCollertionManager.GetDbCollection<CertificateSettingText>();
        }
        #endregion

        #region Fields
        private readonly IMongoCollection<ActivityChecklist> m_objEventCheckLists;
        private readonly IMongoCollection<CommunityRegistEvt> m_objCommunityRegistEvt;
        private readonly IMongoCollection<EventInfo> m_objEventInfo;
        private readonly IMongoCollection<LeaveOfAbsence> m_objLeaveOfAbsence;
        private readonly IMongoCollection<MemberRegistEvt> m_objMemberRegistEvt;
        private readonly IMongoCollection<MemberSupportEvents> m_objMemberSupportEvents;
        private readonly IMongoCollection<CertificateTextFont> m_objCertificateTextFont;
        private readonly IMongoCollection<CertificateSettingText> m_objCertificateSettingText;
        #endregion

        #region Methods
        #region Member Support Events
        /// <summary>
        /// Add Member Support Events
        /// </summary>
        /// <param name="x_objCommunityRegistEvt"></param>
        /// <returns></returns>
        public async Task<bool> AddMemberSupportEvents(MemberSupportEvents x_objMemberSupportEvents)
        {
            try
            {
                if (x_objMemberSupportEvents == null)
                {
                    return false;
                }

                if (x_objMemberSupportEvents.Id.MongoIdIsValid() == true)
                {
                    return false;
                }
                x_objMemberSupportEvents.Id = string.Empty;
                await m_objMemberSupportEvents.InsertOneAsync(x_objMemberSupportEvents);
                if (x_objMemberSupportEvents.Id.MongoIdIsValid() == true)
                {
                    return true;
                }
                return false;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Get All Leave Of Absence
        /// </summary>
        /// <returns></returns>
        public async Task<List<MemberSupportEvents>> GetAllMemberSupportEventsByEvtId()
        {
            List<MemberSupportEvents> lstMemberSupportEvents;
            try
            {
                lstMemberSupportEvents = await m_objMemberSupportEvents.FindAsync(obj => true).Result.ToListAsync();
                return lstMemberSupportEvents;
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Delete Leave Of Absence
        /// </summary>
        /// <param name="x_strMemberSupportEventsId"></param>
        /// <returns></returns>
        public async Task<bool> DeleteMemberSupportEventsById(string x_strMemberSupportEventsId)
        {
            DeleteResult objResult;
            try
            {
                if (x_strMemberSupportEventsId.MongoIdIsValid() == false)
                {
                    return false;
                }

                objResult = await m_objMemberSupportEvents.DeleteOneAsync(obj => obj.Id.Equals(x_strMemberSupportEventsId) == true);
                if (objResult.DeletedCount > 0)
                {
                    return true;
                }
                return false;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Delete Leave Of Absence By Evt Id
        /// </summary>
        /// <param name="x_strEvtId"></param>
        /// <returns></returns>
        public async Task<bool> DeleteMemberSupportEventsByEvtId(string x_strEvtId)
        {
            DeleteResult objResult;
            try
            {
                if (x_strEvtId.MongoIdIsValid() == false)
                {
                    return false;
                }

                objResult = await m_objMemberSupportEvents.DeleteOneAsync(obj => obj.EventId.Equals(x_strEvtId) == true);
                if (objResult.DeletedCount > 0)
                {
                    return true;
                }
                return false;
            }
            catch
            {
                return false;
            }
        }
        #endregion

        #region Leave Of Absence
        /// <summary>
        /// Add Leave Of Absence
        /// </summary>
        /// <param name="x_objCommunityRegistEvt"></param>
        /// <returns></returns>
        public async Task<bool> AddLeaveOfAbsence(LeaveOfAbsence x_objLeaveOfAbsence)
        {
            try
            {
                if (x_objLeaveOfAbsence == null)
                {
                    return false;
                }

                if (x_objLeaveOfAbsence.Id.MongoIdIsValid() == true)
                {
                    return false;
                }
                x_objLeaveOfAbsence.Id = string.Empty;
                await m_objLeaveOfAbsence.InsertOneAsync(x_objLeaveOfAbsence);
                if (x_objLeaveOfAbsence.Id.MongoIdIsValid() == true)
                {
                    return true;
                }
                return false;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Get All Leave Of Absence
        /// </summary>
        /// <returns></returns>
        public async Task<List<LeaveOfAbsence>> GetAllLeaveOfAbsence()
        {
            List<LeaveOfAbsence> lstLeaveOfAbsence;
            try
            {
                lstLeaveOfAbsence = await m_objLeaveOfAbsence.FindAsync(obj => true).Result.ToListAsync();
                return lstLeaveOfAbsence;
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Delete Leave Of Absence
        /// </summary>
        /// <param name="x_strMemberId"></param>
        /// <param name="x_strEvtId"></param>
        /// <returns></returns>
        public async Task<bool> DeleteLeaveOfAbsenceById(string x_strMemberId, string x_strEvtId)
        {
            DeleteResult objResult;
            try
            {
                if ((x_strMemberId.MongoIdIsValid() == false) || (x_strEvtId.MongoIdIsValid() == false))
                {
                    return false;
                }

                objResult = await m_objLeaveOfAbsence.DeleteOneAsync(obj => (obj.MemberId.Equals(x_strMemberId) == true) && (obj.EventId.Equals(x_strEvtId) == true));
                if (objResult.DeletedCount > 0)
                {
                    return true;
                }
                return false;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Check Leave Exist
        /// </summary>
        /// <param name="x_strMemberId"></param>
        /// <param name="x_strActivityId"></param>
        /// <returns></returns>
        public async Task<bool> CheckLeaveExist(string x_strMemberId, string x_strActivityId)
        {
            bool bResult;
            try
            {
                if ((x_strMemberId.MongoIdIsValid() == false) || (x_strActivityId.MongoIdIsValid() == false))
                {
                    return true;
                }
                bResult = await m_objLeaveOfAbsence.FindAsync(obj => (obj.MemberId.Equals(x_strMemberId) == true) && (obj.EventId.Equals(x_strActivityId) == true)).Result.AnyAsync();
                return bResult;
            }
            catch
            {
                return true;
            }
        }

        /// <summary>
        /// Delete Leave Of Absence By Evt Id
        /// </summary>
        /// <param name="x_strEvtId"></param>
        /// <returns></returns>
        public async Task<bool> DeleteLeaveOfAbsenceByEvtId(string x_strEvtId)
        {
            DeleteResult objResult;
            try
            {
                if (x_strEvtId.MongoIdIsValid() == false)
                {
                    return false;
                }

                objResult = await m_objLeaveOfAbsence.DeleteOneAsync(obj => obj.EventId.Equals(x_strEvtId) == true);
                if (objResult.DeletedCount > 0)
                {
                    return true;
                }
                return false;
            }
            catch
            {
                return false;
            }
        }
        #endregion

        #region Community Regist Event
        /// <summary>
        /// Add Community Regist Event
        /// </summary>
        /// <param name="x_objCommunityRegistEvt"></param>
        /// <returns></returns>
        public async Task<bool> UpdateCommunityRegistEvt(CommunityRegistEvt x_objCommunityRegistEvt)
        {
            ReplaceOneResult objResult;
            try
            {
                if (x_objCommunityRegistEvt == null)
                {
                    return false;
                }

                if (x_objCommunityRegistEvt.Id.MongoIdIsValid() == false)
                {
                    x_objCommunityRegistEvt.Id = string.Empty;
                    await m_objCommunityRegistEvt.InsertOneAsync(x_objCommunityRegistEvt);
                    if (x_objCommunityRegistEvt.Id.MongoIdIsValid() == true)
                    {
                        return true;
                    }
                }
                else
                {
                    objResult = await m_objCommunityRegistEvt.ReplaceOneAsync(obj => obj.Id.Equals(x_objCommunityRegistEvt.Id) == true, x_objCommunityRegistEvt);
                    if (objResult.ModifiedCount > 0)
                    {
                        return true;
                    }
                    return false;
                }
                return false;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Get All Community Regist Event
        /// </summary>
        /// <returns></returns>
        public async Task<List<CommunityRegistEvt>> GetAllCommunityRegistEvt()
        {
            List<CommunityRegistEvt> lstCommunityRegistEvt;
            try
            {
                lstCommunityRegistEvt = await m_objCommunityRegistEvt.FindAsync(obj => true).Result.ToListAsync();
                return lstCommunityRegistEvt;
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Get Community Regist Evt
        /// </summary>
        /// <param name="x_strMemberId"></param>
        /// <returns></returns>
        public async Task<CommunityRegistEvt> GetCommunityRegistEvt(string x_strMemberId)
        {
            CommunityRegistEvt objCommunityRegistEvt;
            try
            {
                if (x_strMemberId.MongoIdIsValid() == false)
                {
                    return null;
                }
                objCommunityRegistEvt = await m_objCommunityRegistEvt.FindAsync(obj => obj.Id.Equals(x_strMemberId) == true).Result.FirstOrDefaultAsync();
                return objCommunityRegistEvt;
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Delete Member Regist Event
        /// </summary>
        /// <param name="x_strMemberRegistEvtId"></param>
        /// <returns></returns>
        public async Task<bool> DeleteCommunityRegistEvt(string x_strMemberRegistEvtId)
        {
            DeleteResult objResult;
            try
            {
                if (x_strMemberRegistEvtId.MongoIdIsValid() == false)
                {
                    return false;
                }

                objResult = await m_objCommunityRegistEvt.DeleteOneAsync(obj => obj.Id.Equals(x_strMemberRegistEvtId) == true);
                if (objResult.DeletedCount > 0)
                {
                    return true;
                }
                return false;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Delete Member Regist By Evt Id
        /// </summary>
        /// <param name="x_strEvtId"></param>
        /// <returns></returns>
        public async Task<bool> DeleteCommunityRegistByEvtId(string x_strEvtId)
        {
            DeleteResult objResult;
            try
            {
                if (x_strEvtId.MongoIdIsValid() == false)
                {
                    return false;
                }

                objResult = await m_objCommunityRegistEvt.DeleteOneAsync(obj => obj.EventId.Equals(x_strEvtId) == true);
                if (objResult.DeletedCount > 0)
                {
                    return true;
                }
                return false;
            }
            catch
            {
                return false;
            }
        }
        #endregion

        #region Member Regist Event
        /// <summary>
        /// Add Member Regist Event
        /// </summary>
        /// <param name="x_objMemberRegistEvt"></param>
        /// <returns></returns>
        public async Task<bool> UpdateMemberRegistEvt(MemberRegistEvt x_objMemberRegistEvt)
        {
            ReplaceOneResult objResult;
            try
            {
                if (x_objMemberRegistEvt == null)
                {
                    return false;
                }

                if (x_objMemberRegistEvt.Id.MongoIdIsValid() == true)
                {
                    objResult = await m_objMemberRegistEvt.ReplaceOneAsync(obj => obj.Id.Equals(x_objMemberRegistEvt.Id) == true, x_objMemberRegistEvt);
                    if (objResult.ModifiedCount > 0)
                    {
                        return true;
                    }
                    return false;
                }
                else
                {
                    x_objMemberRegistEvt.Id = string.Empty;
                    await m_objMemberRegistEvt.InsertOneAsync(x_objMemberRegistEvt);
                    if (x_objMemberRegistEvt.Id.MongoIdIsValid() == true)
                    {
                        return true;
                    }
                }
                return false;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Get All Member Regist Event
        /// </summary>
        /// <returns></returns>
        public async Task<List<MemberRegistEvt>> GetAllMemberRegistEvt()
        {
            List<MemberRegistEvt> lstMemberRegistEvt;
            try
            {
                lstMemberRegistEvt = await m_objMemberRegistEvt.FindAsync(obj => true).Result.ToListAsync();
                return lstMemberRegistEvt;
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Get All Member Regist Event
        /// </summary>
        /// <returns></returns>
        public async Task<List<MemberRegistEvt>> GetAllMemberRegistEvt(string x_strMemberId)
        {
            List<MemberRegistEvt> lstMemberRegistEvt;
            try
            {
                if (x_strMemberId.MongoIdIsValid() == false)
                {
                    return null;
                }
                lstMemberRegistEvt = await m_objMemberRegistEvt.FindAsync(obj => obj.MemberId.Equals(x_strMemberId) == true).Result.ToListAsync();
                return lstMemberRegistEvt;
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Get Member Regist Event
        /// </summary>
        /// <returns></returns>
        public async Task<MemberRegistEvt> GetMemberRegistEvtById(string x_strId)
        {
            MemberRegistEvt objMemberRegistEvt;
            try
            {
                if (x_strId.MongoIdIsValid() == false)
                {
                    return null;
                }
                objMemberRegistEvt = await m_objMemberRegistEvt.FindAsync(obj => obj.Id.Equals(x_strId)).Result.FirstOrDefaultAsync();
                return objMemberRegistEvt;
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Delete Member Regist Event
        /// </summary>
        /// <param name="x_strMemberRegistEvtId"></param>
        /// <returns></returns>
        public async Task<bool> DeleteMemberRegistEvt(string x_strMemberRegistEvtId)
        {
            DeleteResult objResult;
            try
            {
                if (x_strMemberRegistEvtId.MongoIdIsValid() == false)
                {
                    return false;
                }

                objResult = await m_objMemberRegistEvt.DeleteOneAsync(obj => obj.Id.Equals(x_strMemberRegistEvtId) == true);
                if (objResult.DeletedCount > 0)
                {
                    return true;
                }
                return false;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Delete Member Regist By Evt Id
        /// </summary>
        /// <param name="x_strEvtId"></param>
        /// <returns></returns>
        public async Task<bool> DeleteMemberRegistByEvtId(string x_strEvtId)
        {
            DeleteResult objResult;
            try
            {
                if (x_strEvtId.MongoIdIsValid() == false)
                {
                    return false;
                }

                objResult = await m_objMemberRegistEvt.DeleteOneAsync(obj => obj.EventId.Equals(x_strEvtId) == true);
                if (objResult.DeletedCount > 0)
                {
                    return true;
                }
                return false;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Delete Member Regist By Id
        /// </summary>
        /// <param name="x_strMemberId"></param>
        /// <param name="x_strEvtId"></param>
        /// <returns></returns>
        public async Task<bool> DeleteMemberRegistById(string x_strMemberId, string x_strEvtId)
        {
            DeleteResult objResult;
            try
            {
                if ((x_strEvtId.MongoIdIsValid() == false) || (x_strMemberId.MongoIdIsValid() == false))
                {
                    return false;
                }

                objResult = await m_objMemberRegistEvt.DeleteOneAsync(obj => (obj.EventId.Equals(x_strEvtId) == true) && (obj.MemberId.Equals(x_strMemberId) == true));
                if (objResult.DeletedCount > 0)
                {
                    return true;
                }
                return false;
            }
            catch
            {
                return false;
            }
        }
        #endregion

        #region Event info

        /// <summary>
        /// Update Event Info
        /// </summary>
        /// <param name="x_objEventInfo"></param>
        /// <returns></returns>
        public async Task<bool> UpdateEventInfo(EventInfo x_objEventInfo)
        {
            ReplaceOneResult objResult;
            try
            {
                if (x_objEventInfo == null)
                {
                    return false;
                }

                // Create 
                if (x_objEventInfo.Id.MongoIdIsValid() == false)
                {
                    x_objEventInfo.Id = string.Empty;
                    await m_objEventInfo.InsertOneAsync(x_objEventInfo);
                    if (x_objEventInfo.Id.MongoIdIsValid() == true)
                    {
                        return true;
                    }
                    return false;
                }
                else
                {
                    objResult = await m_objEventInfo.ReplaceOneAsync(obj => obj.Id.Equals(x_objEventInfo.Id), x_objEventInfo);
                    if (objResult.ModifiedCount > 0)
                    {
                        return true;
                    }
                    return false;
                }
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Get Event Info By Id
        /// </summary>
        /// <param name="x_strEventId"></param>
        /// <returns></returns>
        public async Task<EventInfo> GetEventInfoById(string x_strEventId)
        {
            EventInfo objEventInfo;
            try
            {
                if (x_strEventId.MongoIdIsValid() == false)
                {
                    return null;
                }

                objEventInfo = await m_objEventInfo.FindAsync(obj => obj.Id.Equals(x_strEventId) == true).Result.FirstOrDefaultAsync();
                return objEventInfo;
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Get All Event Info
        /// </summary>
        /// <returns></returns>
        public async Task<List<EventInfo>> GetAllEventInfo(bool x_bIsLogin)
        {
            List<EventInfo> lstEventInfo;
            try
            {
                if (x_bIsLogin == true)
                {
                    lstEventInfo = await m_objEventInfo.FindAsync(obj => true).Result.ToListAsync();
                }
                else
                {
                    lstEventInfo = await m_objEventInfo.FindAsync(obj => obj.IsPublic == true).Result.ToListAsync();
                }
                return lstEventInfo;
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Delete Event Info By Id
        /// </summary>
        /// <param name="x_strEventId"></param>
        /// <returns></returns>
        public async Task<bool> DeleteEventInfoById(string x_strEventId)
        {
            DeleteResult objDeleteResult;
            try
            {
                if (x_strEventId.MongoIdIsValid() == false)
                {
                    return false;
                }

                objDeleteResult = await m_objEventInfo.DeleteOneAsync(obj => obj.Id.Equals(x_strEventId) == true);
                if (objDeleteResult.DeletedCount > 0)
                {
                    return true;
                }
                return false;
            }
            catch
            {
                return false;
            }
        }

        #endregion

        #region Check List

        /// <summary>
        /// Get All Check List
        /// </summary>
        /// <returns></returns>
        public async Task<List<ActivityChecklist>> GetAllCheckList()
        {
            List<ActivityChecklist> lstCheckLists;

            try
            {
                lstCheckLists = await m_objEventCheckLists.FindAsync(obj => true).Result.ToListAsync();
                return lstCheckLists;
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Get Check List By Evt
        /// </summary>
        /// <param name="x_strEventId"></param>
        /// <returns></returns>
        public async Task<List<ActivityChecklist>> GetCheckListByEvt(string x_strEventId)
        {
            List<ActivityChecklist> lstCheckLists;

            try
            {
                if (x_strEventId.MongoIdIsValid() == false)
                {
                    return null;
                }

                lstCheckLists = await m_objEventCheckLists.FindAsync(obj => obj.EventsId.Equals(x_strEventId) == true).Result.ToListAsync();
                return lstCheckLists;
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Get Check List By Member Id
        /// </summary>
        /// <param name="x_strMemberId"></param>
        /// <returns></returns>
        public async Task<List<ActivityChecklist>> GetCheckListByMemberId(string x_strMemberId)
        {
            List<ActivityChecklist> lstCheckLists;

            try
            {
                if (x_strMemberId.MongoIdIsValid() == false)
                {
                    return null;
                }

                lstCheckLists = await m_objEventCheckLists.FindAsync(obj => obj.MemberId.Equals(x_strMemberId) == true).Result.ToListAsync();
                return lstCheckLists;
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Update Event Check List
        /// </summary>
        /// <param name="x_objEvtCheckIn"></param>
        /// <returns></returns>
        public async Task<bool> UpdateEventCheckList(ActivityChecklist x_objEvtCheckIn)
        {
            ReplaceOneResult objRes;
            try
            {
                if (x_objEvtCheckIn == null)
                {
                    return false;
                }

                if (x_objEvtCheckIn.Id.MongoIdIsValid() == false)
                {
                    x_objEvtCheckIn.Id = string.Empty;
                    await m_objEventCheckLists.InsertOneAsync(x_objEvtCheckIn);
                    return true;
                }
                else
                {
                    objRes = await m_objEventCheckLists.ReplaceOneAsync(obj => obj.Id.Equals(x_objEvtCheckIn.Id) == true, x_objEvtCheckIn);
                    if (objRes.ModifiedCount > 0)
                    {
                        return true;
                    }
                    return false;
                }
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Delete Check List By Evt Id
        /// </summary>
        /// <param name="x_strEvtId"></param>
        /// <returns></returns>
        public async Task<bool> DeleteCheckListByEvtId(string x_strEvtId)
        {
            try
            {
                if (x_strEvtId.MongoIdIsValid() == false)
                {
                    return false;
                }

                await m_objEventCheckLists.DeleteManyAsync(obj => obj.EventsId.Equals(x_strEvtId) == true);
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Delete Check List By Id
        /// </summary>
        /// <param name="x_strEvtId"></param>
        /// <returns></returns>
        public async Task<bool> DeleteCheckListById(string x_strCheckInId)
        {
            try
            {
                if (x_strCheckInId.MongoIdIsValid() == false)
                {
                    return false;
                }

                await m_objEventCheckLists.DeleteManyAsync(obj => obj.Id.Equals(x_strCheckInId) == true);
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Delete Check List By Evt Id
        /// </summary>
        /// <param name="x_strEvtId"></param>
        /// <returns></returns>
        public async Task<bool> DeleteCheckListByMemberId(string x_strMemberId, string x_strEvtId)
        {
            try
            {
                if (x_strMemberId.MongoIdIsValid() == false)
                {
                    return false;
                }

                await m_objEventCheckLists.DeleteManyAsync(obj => (obj.MemberId.Equals(x_strMemberId) == true) &&
                                                                  (obj.EventsId.Equals(x_strEvtId) == true));
                return true;
            }
            catch
            {
                return false;
            }
        }
        #endregion

        #region Certificate Text Font
        /// <summary>
        /// Update Text Font
        /// </summary>
        /// <param name="x_objCertificateTextFont"></param>
        /// <returns></returns>
        public async Task<bool> UpdateTextFont(CertificateTextFont x_objCertificateTextFont)
        {
            CertificateTextFont objFont;
            try
            {
                objFont = await m_objCertificateTextFont.FindAsync(obj => obj.FontName.Equals(x_objCertificateTextFont.FontName) == true).Result.FirstOrDefaultAsync();
                if (objFont == null)
                {
                    // Create
                    await m_objCertificateTextFont.InsertOneAsync(x_objCertificateTextFont);
                    if (x_objCertificateTextFont.Id.MongoIdIsValid() == false)
                    {
                        return false;
                    }
                    return true;
                }
                else
                {
                    x_objCertificateTextFont.Id = objFont.Id;
                    await m_objCertificateTextFont.ReplaceOneAsync(obj => obj.Id.Equals(objFont.Id) == true, x_objCertificateTextFont);
                    return true;
                }
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Get All Test Font
        /// </summary>
        /// <returns></returns>
        public async Task<List<CertificateTextFont>> GetAllTestFont()
        {
            List<CertificateTextFont> lstCertificateTextFont;
            try
            {
                lstCertificateTextFont = await m_objCertificateTextFont.FindAsync(obj => true).Result.ToListAsync();
                return lstCertificateTextFont;
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Get Text Font By Id
        /// </summary>
        /// <param name="x_strFontId"></param>
        /// <returns></returns>
        public async Task<CertificateTextFont> GetTextFontById(string x_strFontId)
        {
            CertificateTextFont objCertificateTextFont;
            try
            {
                if (x_strFontId.MongoIdIsValid() == false)
                {
                    return null;
                }
                objCertificateTextFont = await m_objCertificateTextFont.FindAsync(obj => obj.Id.Equals(x_strFontId)).Result.FirstOrDefaultAsync();
                return objCertificateTextFont;
            }
            catch
            {
                return null;
            }
        }
        #endregion

        #region Setting text
        /// <summary>
        /// Update Setting Text
        /// </summary>
        /// <param name="x_objCertificateSettingText"></param>
        /// <returns></returns>
        public async Task<bool> UpdateSettingText(CertificateSettingText x_objCertificateSettingText)
        {
            ReplaceOneResult objResult;

            try
            {
                if (x_objCertificateSettingText == null)
                {
                    return false;
                }

                // Add new
                if (x_objCertificateSettingText.Id.MongoIdIsValid() == false)
                {
                    x_objCertificateSettingText.Id = string.Empty;
                    await m_objCertificateSettingText.InsertOneAsync(x_objCertificateSettingText);
                    if (x_objCertificateSettingText.Id.MongoIdIsValid() == false)
                    {
                        return false;
                    }
                    return true;
                }
                else
                {
                    objResult = await m_objCertificateSettingText.ReplaceOneAsync(obj => obj.Id.Equals(x_objCertificateSettingText.Id) == true, x_objCertificateSettingText);
                    if (objResult.ModifiedCount == 0)
                    {
                        return false;
                    }
                    return true;
                }
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Get Setting Text By Activity Id
        /// </summary>
        /// <param name="x_strActivityId"></param>
        /// <returns></returns>
        public async Task<List<CertificateSettingText>> GetSettingTextByActivityId(string x_strActivityId)
        {
            List<CertificateSettingText> lstSettingText;
            try
            {
                if (x_strActivityId.MongoIdIsValid() == false)
                {
                    return null;
                }
                lstSettingText = await m_objCertificateSettingText.FindAsync(obj => obj.ActivityId.Equals(x_strActivityId) == true).Result.ToListAsync();
                return lstSettingText;
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Get Setting Text By Activity Id
        /// </summary>
        /// <param name="x_strActivityId"></param>
        /// <returns></returns>
        public async Task<CertificateSettingText> GetSettingTextById(string x_strTextId)
        {
            CertificateSettingText objSettingText;
            try
            {
                if (x_strTextId.MongoIdIsValid() == false)
                {
                    return null;
                }
                objSettingText = await m_objCertificateSettingText.FindAsync(obj => obj.Id.Equals(x_strTextId) == true).Result.FirstOrDefaultAsync();
                return objSettingText;
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Delete Setting Text
        /// </summary>
        /// <param name="x_strSettingTextId"></param>
        /// <returns></returns>
        public async Task<bool> DeleteSettingText(string x_strSettingTextId)
        {
            DeleteResult objResult;
            try
            {
                if (x_strSettingTextId.MongoIdIsValid() == false)
                {
                    return false;
                }

                objResult = await m_objCertificateSettingText.DeleteOneAsync(obj => obj.Id.Equals(x_strSettingTextId) == true);
                if (objResult.DeletedCount == 0)
                {
                    return false;
                }
                return true;
            }
            catch
            {
                return false;
            }
        }
        #endregion
        #endregion
    }
}
