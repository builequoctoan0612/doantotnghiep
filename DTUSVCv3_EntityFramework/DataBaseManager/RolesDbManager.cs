﻿using DTUSVCv3_Library;
using MongoDB.Driver;

namespace DTUSVCv3_EntityFramework
{
    public class RolesDbManager
    {
        #region Constant
        public RolesDbManager()
        {
            m_objRolesDb = DbCollertionManager.GetDbCollection<Roles>();
        }
        #endregion

        #region Fields
        private readonly IMongoCollection<Roles> m_objRolesDb;
        #endregion

        #region Methods
        /// <summary>
        /// Get All Role
        /// </summary>
        public async Task<List<Roles>> GetAllRole()
        {
            List<Roles> lstRoles;

            try
            {
                lstRoles = await m_objRolesDb.FindAsync(obj => true).Result.ToListAsync();
                return lstRoles;
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Get Role By Id
        /// </summary>
        public async Task<Roles> GetRoleById(string x_strRoleId)
        {
            Roles objRole;

            try
            {
                if (x_strRoleId.MongoIdIsValid() == false)
                {
                    return null;
                }

                objRole = await m_objRolesDb.FindAsync(obj => obj.Id.Equals(x_strRoleId) == true).Result.FirstOrDefaultAsync();
                return objRole;
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Update Role
        /// </summary>
        public async Task<bool> UpdateRole(Roles x_objRole)
        {
            ReplaceOneResult objResult;

            try
            {
                if (x_objRole == null)
                {
                    return false;
                }

                // add
                if (x_objRole.Id.MongoIdIsValid() == false)
                {
                    x_objRole.Id = string.Empty;
                    await m_objRolesDb.InsertOneAsync(x_objRole);
                    return true;
                }
                // update
                else
                {
                    objResult = await m_objRolesDb.ReplaceOneAsync(obj => obj.Id.Equals(x_objRole.Id) == true, x_objRole);
                    if (objResult.ModifiedCount > 0)
                    {
                        return true;
                    }
                    return false;
                }
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Delete Role
        /// </summary>
        public async Task<bool> DeleteRole(string x_strRoleId)
        {
            DeleteResult objResult;
            try
            {
                if (x_strRoleId.MongoIdIsValid() == false)
                {
                    return false;
                }

                objResult = await m_objRolesDb.DeleteOneAsync(obj => obj.Id.Equals(x_strRoleId) == true);
                if (objResult.DeletedCount > 0)
                {
                    return true;
                }
                return false;
            }
            catch
            {
                return false;
            }
        }
        #endregion
    }
}
