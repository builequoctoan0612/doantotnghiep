﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;

namespace DTUSVCv3_EntityFramework.OldDataModels
{
    [Serializable]
    public class CollectFunds
    {
        public CollectFunds()
        {
            IsConfirmed = false;
            TransactionDate = DateTime.UtcNow;
        }
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        [BsonRepresentation(BsonType.ObjectId)]
        public string TreasurerId { get; set; }
        public string Content { get; set; }
        public double Money { get; set; }
        [BsonRepresentation(BsonType.ObjectId)]
        public string PayerId { get; set; }
        public DateTime TransactionDate { get; set; }
        public bool IsConfirmed { get; set; }
    }
}
