﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;

namespace DTUSVCv3_EntityFramework.OldDataModels
{
    [Serializable]
    public class Introduce
    {
        public Introduce()
        {
            CreateDate = DateTime.UtcNow;
        }
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        [BsonRepresentation(BsonType.ObjectId)]
        public string CreaterId { get; set; }
        public string Content { get; set; }
        public string HostLine { get; set; }
        public string Email { get; set; }
        public DateTime CreateDate { get; set; }
    }
}
