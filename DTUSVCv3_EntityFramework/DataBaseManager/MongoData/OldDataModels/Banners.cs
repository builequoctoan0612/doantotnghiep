﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace DTUSVCv3_EntityFramework.OldDataModels
{
    [Serializable]
    public class Banners
    {
        public Banners()
        {
            CreateDate = DateTime.UtcNow;
        }

        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        public string BannerPath { get; set; }
        public DateTime CreateDate { get; set; }
        [BsonRepresentation(BsonType.ObjectId)]
        public string CreaterId { get; set; }
    }
}
