﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace DTUSVCv3_EntityFramework.OldDataModels
{
    [Serializable]
    public class Address
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        public string StreesNamed { get; set; }
        public string ProvinceId { get; set; }
        public string DistrictId { get; set; }
        public string WardId { get; set; }
    }
}
