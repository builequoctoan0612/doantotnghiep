﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;

namespace DTUSVCv3_EntityFramework.OldDataModels
{
    [Serializable]
    public class PaymentConfirmation
    {
        public PaymentConfirmation()
        {
            ConfirmationTime = DateTime.UtcNow;
        }
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        [BsonRepresentation(BsonType.ObjectId)]
        public string PaymentId { get; set; }
        public DateTime ConfirmationTime { get; set; }
    }
}
