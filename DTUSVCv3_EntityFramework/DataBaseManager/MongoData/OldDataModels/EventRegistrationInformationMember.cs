﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;

namespace DTUSVCv3_EntityFramework.OldDataModels
{
    [Serializable]
    public class EventRegistrationInformationMember
    {
        public EventRegistrationInformationMember()
        {
            CreateDate = DateTime.UtcNow;
        }
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        [BsonRepresentation(BsonType.ObjectId)]
        public string MemberId { get; set; }
        public DateTime CreateDate { get; set; }
        [BsonRepresentation(BsonType.ObjectId)]
        public string ActivitiesAndEventsId { get; set; }
    }
}
