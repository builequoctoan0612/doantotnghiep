﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;

namespace DTUSVCv3_EntityFramework.OldDataModels
{
    [Serializable]
    public class ActivityChecklist
    {
        public ActivityChecklist()
        {
            InStart = DateTime.UtcNow;
            OutStart = new DateTime();
        }
        public string Id { get; set; }
        public string MemberId { get; set; }
        public string ActivitiesAndEventsId { get; set; }
        public DateTime InStart { get; set; }
        public DateTime OutStart { get; set; }
        public string ManagerId { get; set; }
    }
}
