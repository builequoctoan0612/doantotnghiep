﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;

namespace DTUSVCv3_EntityFramework.OldDataModels
{
    [Serializable]
    public class Recruits
    {
        public Recruits()
        {
            IsLocked = false;
            EndOfRecruitment = false;
        }
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        public string RecruitName { get; set; }
        public string RecruitContent { get; set; }
        public DateTime DateStart { get; set; }
        public DateTime DateEnd { get; set; }
        [BsonRepresentation(BsonType.ObjectId)]
        public string CreaterId { get; set; }
        public bool IsLocked { get; set; }
        public bool EndOfRecruitment { get; set; }
    }
}
