﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace DTUSVCv3_EntityFramework.OldDataModels
{
    [Serializable]
    public class Wards
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        public string WardName { get; set; }
        public string WardId { get; set; }
        public string Type { get; set; }
        public string Location { get; set; }
        public string DistrictId { get; set; }
    }
}
