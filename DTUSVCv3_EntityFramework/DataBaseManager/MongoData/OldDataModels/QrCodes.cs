﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace DTUSVCv3_EntityFramework.OldDataModels
{
    [Serializable]
    public class QrCodes
    {
        public QrCodes()
        {
            DateCreate = DateTime.UtcNow;
        }
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        public string CreaterId { get; set; }
        public string FileName { get; set; }
        public string QrContent { get; set; }
        public DateTime DateCreate { get; set; }
    }
}
