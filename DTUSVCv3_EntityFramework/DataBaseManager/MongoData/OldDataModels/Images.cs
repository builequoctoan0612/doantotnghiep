﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace DTUSVCv3_EntityFramework.OldDataModels
{
    [Serializable]
    public class Images
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        public string CompressedImagePath { get; set; }
        public string OriginalImagePath { get; set; }
    }
}
