﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;

namespace DTUSVCv3_EntityFramework.OldDataModels
{
    [Serializable]
    public class MembershipRegistration
    {
        public MembershipRegistration()
        {
            RegistrationDate = DateTime.UtcNow;
            Approve = false;
            Scoring = false;
            SendEmail = false;
            Scores = 0;
        }
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public DateTime BirthDay { get; set; }
        public bool Sex { get; set; }
        [BsonRepresentation(BsonType.ObjectId)]
        public string HometownId { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public string LinkFacebook { get; set; }
        [BsonRepresentation(BsonType.ObjectId)]
        public string FacultyId { get; set; }
        public string ClassName { get; set; }
        public string StudentId { get; set; }
        public DateTime RegistrationDate { get; set; }
        [BsonRepresentation(BsonType.ObjectId)]
        public string RecruitId { get; set; }
        public bool Approve { get; set; }
        public bool Scoring { get; set; }
        public string ApproverId { get; set; }
        public bool RegulatoryCompliance { get; set; }
        public bool VerifiedInformation { get; set; }
        public bool SendEmail { get; set; }
        public string Reviews { get; set; }
        public float Scores { get; set; }
        public string Scorer { get; set; }
        public string Reason { get; set; }
    }
}
