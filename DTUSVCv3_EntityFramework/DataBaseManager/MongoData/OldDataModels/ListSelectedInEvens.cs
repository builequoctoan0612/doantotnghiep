﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;

namespace DTUSVCv3_EntityFramework.OldDataModels
{
    [Serializable]
    public class ListSelectedInEvens
    {
        public ListSelectedInEvens()
        {
            CreateDate = DateTime.UtcNow;
        }
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        [BsonRepresentation(BsonType.ObjectId)]
        public string MemberId { get; set; }
        [BsonRepresentation(BsonType.ObjectId)]
        public string EvenId { get; set; }
        public double CurrentScore { get; set; }
        public DateTime CreateDate { get; set; }
    }
}
