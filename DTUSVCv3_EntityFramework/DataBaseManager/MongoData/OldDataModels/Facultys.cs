﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace DTUSVCv3_EntityFramework.OldDataModels
{
    [Serializable]
    public class Facultys
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string id { get; set; }
        public string FacultyName { get; set; }
    }
}
