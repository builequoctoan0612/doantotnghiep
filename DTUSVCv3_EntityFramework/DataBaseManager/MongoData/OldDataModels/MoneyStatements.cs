﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace DTUSVCv3_EntityFramework.OldDataModels
{
    [Serializable]
    public class MoneyStatements
    {
        public MoneyStatements()
        {
            CreaterDate = DateTime.UtcNow;
            FormsForGiving = true;
        }
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        [BsonRepresentation(BsonType.ObjectId)]
        public string InviteSponsorsInfoId { get; set; }
        public double Money { get; set; }
        public string GiverName { get; set; }
        public bool FormsForGiving { get; set; }
        public string InvoiceNumber { get; set; }

        [BsonRepresentation(BsonType.ObjectId)]
        public string CreaterId { get; set; }
        public DateTime CreaterDate { get; set; }
    }
}
