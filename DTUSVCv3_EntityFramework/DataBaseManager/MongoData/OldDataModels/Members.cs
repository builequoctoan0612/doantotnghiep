﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;

namespace DTUSVCv3_EntityFramework.OldDataModels
{
    [Serializable]
    public class Members
    {
        public Members()
        {
            IsMember = true;
            IsNotAvatar = true;
            JoinDate = DateTime.UtcNow;
        }

        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public DateTime BirthDay { get; set; }
        public bool Sex { get; set; }
        [BsonRepresentation(BsonType.ObjectId)]
        public string HometownId { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public string LinkFacebook { get; set; }
        [BsonRepresentation(BsonType.ObjectId)]
        public string FacultyId { get; set; }
        public string ClassName { get; set; }
        public string StudentId { get; set; }
        public string AvatarPath { get; set; }
        public DateTime JoinDate { get; set; }
        public bool IsMember { get; set; }
        public bool IsNotAvatar { get; set; }
        public int HitsCreateMembershipCards { get; set; }
    }
}
