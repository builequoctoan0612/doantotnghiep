﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;

namespace DTUSVCv3_EntityFramework.OldDataModels
{
    [Serializable]
    public class EventRegistrationInformationPublic
    {
        public EventRegistrationInformationPublic()
        {
            CreateDate = DateTime.UtcNow;
            Joined = false;
            IsMember = false;
        }
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        [BsonRepresentation(BsonType.ObjectId)]
        public string FacultyId { get; set; }
        public string ClassName { get; set; }
        public string StudentId { get; set; }
        public string FacebookPath { get; set; }
        public DateTime CreateDate { get; set; }
        [BsonRepresentation(BsonType.ObjectId)]
        public string ActivitiesAndEventsId { get; set; }
        public bool Joined { get; set; }
        public bool IsMember { get; set; }
    }
}
