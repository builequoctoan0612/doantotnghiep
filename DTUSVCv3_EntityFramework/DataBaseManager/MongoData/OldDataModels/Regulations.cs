﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace DTUSVCv3_EntityFramework.OldDataModels
{
    [Serializable]
    public class Regulations
    {
        public Regulations()
        {
            CreateDate = DateTime.UtcNow;
        }
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        [BsonRepresentation(BsonType.ObjectId)]
        public string CreateId { get; set; }
        public string Content { get; set; }
        public DateTime CreateDate { get; set; }
    }
}
