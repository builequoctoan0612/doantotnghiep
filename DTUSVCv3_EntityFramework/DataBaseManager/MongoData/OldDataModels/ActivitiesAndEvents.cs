﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;

namespace DTUSVCv3_EntityFramework.OldDataModels
{
    [Serializable]
    public class ActivitiesAndEvents
    {
        public ActivitiesAndEvents()
        {
            IsPublic = false;
            IsListSelectedInEven = false;
        }
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        public string Title { get; set; }
        public string PosterPath { get; set; }
        public string Content { get; set; }
        [BsonRepresentation(BsonType.ObjectId)]
        public string AddressId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime RegistrationDeadline { get; set; }
        public DateTime EndDate { get; set; }
        [BsonRepresentation(BsonType.ObjectId)]
        public string CreaterId { get; set; }
        public bool IsPublic { get; set; }
        public bool IsListSelectedInEven { get; set; }
    }
}
