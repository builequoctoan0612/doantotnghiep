﻿using DTUSVCv3_EntityFramework.OldDataModels;
using DTUSVCv3_Library;
using MongoDB.Driver;

namespace DTUSVCv3_EntityFramework
{
    public static class MongoDbManager
    {
        #region Types
        private const string KEY_DECRYPT = "01682319448762101192321123368CDlCM@@";
        #endregion

        #region Fields
        private static IMongoCollection<Accounts> m_objAccountDb;
        private static IMongoCollection<MemberInfo> m_objMemberDb;
        private static IMongoCollection<AddressInfo> m_objAddressInfoDb;
        private static IMongoCollection<Provinces> m_objProvincesDb;
        private static IMongoCollection<Districts> m_objDistrictsDb;
        private static IMongoCollection<Wards> m_objWardsDb;
        private static IMongoCollection<Facultys> m_objFacultysDb;
        private static IMongoCollection<Roles> m_objRolesDb;
        private static IMongoCollection<ActivityChecklist> m_objActivitiesCheckList;
        private static IMongoCollection<RecruitmentInfo> m_objRecruitmentInfo;
        private static IMongoCollection<MembershipRegistration> m_objMembershipRegist;
        private static IMongoCollection<EventInfo> m_objEventInfo;
        private static IMongoCollection<MemberRegistEvt> m_objMemberRegistEvt;
        private static IMongoCollection<CommunityRegistEvt> m_objCommunityRegistEvt;
        private static IMongoCollection<LeaveOfAbsence> m_objLeaveOfAbsence;
        private static IMongoCollection<MemberSupportEvents> m_objMemberSupportEvents;
        private static IMongoCollection<Newspapers> m_objNewspapers;
        #endregion

        #region Methods

        /// <summary>
        /// Copy DataBase
        /// </summary>
        public static void CopyDataBase()
        {
            // Account
            List<Accounts> lstAccounts;
            List<OldDataModels.Accounts> lstOldAccounts;
            // Member
            List<MemberInfo> lstMember;
            List<OldDataModels.Members> lstOldMember;
            // Address
            List<AddressInfo> lstAddress;
            List<OldDataModels.Address> lstOldAddress;
            // Provinces
            List<Provinces> lstProvinces;
            List<OldDataModels.Provinces> lstOldProvinces;
            // Districts
            List<Districts> lstDistricts;
            List<OldDataModels.Districts> lstOldDistricts;
            // Wards
            List<Wards> lstWards;
            List<OldDataModels.Wards> lstOldWards;
            // Facultys
            List<Facultys> lstFacultys;
            List<OldDataModels.Facultys> lstOldFacultys;
            // Roles
            List<Roles> lstRoles;
            List<OldDataModels.Role> lstOldRoles;
            // EventCheckList
            List<ActivityChecklist> lstActivityCheckLists;
            List<OldDataModels.ActivityChecklist> lstOldActivityChecklist;
            // Recruitment Info
            List<RecruitmentInfo> lstRecruitmentInfo;
            List<OldDataModels.Recruits> lstOldRecruitmentInfo;
            // Membership Registration
            List<MembershipRegistration> lstMembersRegist;
            List<OldDataModels.MembershipRegistration> lstOldMembersRegist;
            // Event
            List<ActivitiesAndEvents> lstActivitiesAndEvents;
            List<EventInfo> lstEventInfo;
            // Member Register Event
            List<MemberRegistEvt> lstMemberRegistEvt;
            List<EventRegistrationInformationMember> lstEventRegistrationInformationMember;
            // Community Regist Event
            List<CommunityRegistEvt> lstCommunityRegistEvt;
            List<EventRegistrationInformationPublic> lstEventRegistrationInformationPublic;
            // Leave Of Absence
            List<LeaveOfAbsence> lstLeaveOfAbsence;
            List<OldDataModels.LeaveOfAbsence> lstOldLeaveOfAbsence;
            // Member Support Events
            List<MemberSupportEvents> lstMemberSupportEvt;
            List<OldDataModels.MemberSupportEvents> lstOldMemberSupportEvt;
            // Newspapers
            List<Newspapers> lstNewspapers;
            List<OldDataModels.Newspapers> lstOldNewspapers;


            Accounts objAccount;
            MemberInfo objNewMember;
            AddressInfo objNewAddressInfo;
            OldDataModels.MembershipRegistration objMemberRegistDecrypt;
            MembershipRegistration objMemberRegistNew;

            Task objTask;
            List<DTUSVCPermission> lstPermission;
            string strFilePath;
            string strValue;
            string strMemberId;
            string strMemberHashKey;

            // Account
            m_objAccountDb = DbCollertionManager.GetDbCollection<Accounts>();
            objTask = Task.Run(async () =>
            {
                lstAccounts = await m_objAccountDb.FindAsync(obj => true).Result.ToListAsync();
                if (lstAccounts == null || lstAccounts.Count == 0)
                {
                    strFilePath = Path.Combine(SysFolder.APP_PATH, SysFolder.DATABASE_OLD_FOLDER, DataFilePath.ACCOUNT_FILE_NAME);
                    if (File.Exists(strFilePath) == false)
                    {
                        return;
                    }
                    strValue = File.ReadAllText(strFilePath);
                    lstOldAccounts = ObjHelper.JSONToObject<List<OldDataModels.Accounts>>(strValue);
                    if (lstOldAccounts == null)
                    {
                        return;
                    }
                    lstAccounts = new List<Accounts>();
                    foreach (OldDataModels.Accounts objOldAccount in lstOldAccounts)
                    {
                        objAccount = AccountOldToNewModel(objOldAccount);
                        if (objAccount == null)
                        {
                            continue;
                        }
                        lstAccounts.Add(objAccount);
                    }
                    await m_objAccountDb.InsertManyAsync(lstAccounts);
                }
            });
            objTask.Wait();

            // Members
            m_objMemberDb = DbCollertionManager.GetDbCollection<MemberInfo>();
            objTask = Task.Run(async () =>
            {
                lstMember = await m_objMemberDb.FindAsync(obj => true).Result.ToListAsync();
                if (lstMember == null || lstMember.Count == 0)
                {
                    strFilePath = Path.Combine(SysFolder.APP_PATH, SysFolder.DATABASE_OLD_FOLDER, DataFilePath.MEMBERS_FILE_NAME);
                    if (File.Exists(strFilePath) == false)
                    {
                        return;
                    }
                    strValue = File.ReadAllText(strFilePath);
                    lstOldMember = ObjHelper.JSONToObject<List<OldDataModels.Members>>(strValue);
                    if (lstOldMember == null)
                    {
                        return;
                    }
                    lstMember = new List<MemberInfo>();
                    foreach (OldDataModels.Members objOldMember in lstOldMember)
                    {
                        objNewMember = MemberOldToNewModel(objOldMember);
                        if (objNewMember == null)
                        {
                            continue;
                        }

                        lstMember.Add(objNewMember);
                    }
                    await m_objMemberDb.InsertManyAsync(lstMember);
                }
            });
            objTask.Wait();

            // Address
            m_objAddressInfoDb = DbCollertionManager.GetDbCollection<AddressInfo>();
            objTask = Task.Run(async () =>
            {
                lstAddress = await m_objAddressInfoDb.FindAsync(obj => true).Result.ToListAsync();
                if (lstAddress == null || lstAddress.Count == 0)
                {
                    strFilePath = Path.Combine(SysFolder.APP_PATH, SysFolder.DATABASE_OLD_FOLDER, DataFilePath.ADDRESS_FILE_NAME);
                    if (File.Exists(strFilePath) == false)
                    {
                        return;
                    }
                    strValue = File.ReadAllText(strFilePath);
                    lstOldAddress = ObjHelper.JSONToObject<List<OldDataModels.Address>>(strValue);
                    if (lstOldAddress == null)
                    {
                        return;
                    }
                    lstAddress = new List<AddressInfo>();
                    foreach (OldDataModels.Address objOldAddress in lstOldAddress)
                    {
                        if (objOldAddress == null)
                        {
                            continue;
                        }

                        strMemberId = string.Empty;
                        objNewMember = await m_objMemberDb.FindAsync(obj => obj.HometownId.Equals(objOldAddress.Id) == true).Result.FirstOrDefaultAsync();
                        if (objNewMember != null)
                        {
                            strMemberId = objNewMember.Id;
                        }
                        objNewAddressInfo = AddressOldToNewModel(objOldAddress, strMemberId);
                        if (objNewAddressInfo == null)
                        {
                            continue;
                        }

                        lstAddress.Add(objNewAddressInfo);
                    }
                    await m_objAddressInfoDb.InsertManyAsync(lstAddress);
                }
            });
            objTask.Wait();

            m_objProvincesDb = DbCollertionManager.GetDbCollection<Provinces>();
            objTask = Task.Run(async () =>
            {
                lstProvinces = await m_objProvincesDb.FindAsync(obj => true).Result.ToListAsync();
                if (lstProvinces == null || lstProvinces.Count == 0)
                {
                    strFilePath = Path.Combine(SysFolder.APP_PATH, SysFolder.DATABASE_OLD_FOLDER, DataFilePath.PROVINCES_FILE_NAME);
                    if (File.Exists(strFilePath) == false)
                    {
                        return;
                    }
                    strValue = File.ReadAllText(strFilePath);
                    lstOldProvinces = ObjHelper.JSONToObject<List<OldDataModels.Provinces>>(strValue);
                    if (lstOldProvinces == null)
                    {
                        return;
                    }
                    lstProvinces = new List<Provinces>();
                    foreach (OldDataModels.Provinces objOldProvince in lstOldProvinces)
                    {
                        if (objOldProvince == null)
                        {
                            continue;
                        }

                        lstProvinces.Add(new Provinces
                        {
                            Id = objOldProvince.Id,
                            Location = objOldProvince.Location,
                            ProvinceId = objOldProvince.ProvinceId,
                            ProvinceName = objOldProvince.ProvinceName,
                            Type = objOldProvince.Type
                        });
                    }
                    await m_objProvincesDb.InsertManyAsync(lstProvinces);
                }
            });
            objTask.Wait();

            m_objDistrictsDb = DbCollertionManager.GetDbCollection<Districts>();
            objTask = Task.Run(async () =>
            {
                lstDistricts = await m_objDistrictsDb.FindAsync(obj => true).Result.ToListAsync();
                if (lstDistricts == null || lstDistricts.Count == 0)
                {
                    strFilePath = Path.Combine(SysFolder.APP_PATH, SysFolder.DATABASE_OLD_FOLDER, DataFilePath.DISTRICTS_FILE_NAME);
                    if (File.Exists(strFilePath) == false)
                    {
                        return;
                    }
                    strValue = File.ReadAllText(strFilePath);
                    lstOldDistricts = ObjHelper.JSONToObject<List<OldDataModels.Districts>>(strValue);
                    if (lstOldDistricts == null)
                    {
                        return;
                    }
                    lstDistricts = new List<Districts>();
                    foreach (OldDataModels.Districts objOldDistrict in lstOldDistricts)
                    {
                        if (objOldDistrict == null)
                        {
                            continue;
                        }

                        lstDistricts.Add(new Districts
                        {
                            Id = objOldDistrict.Id,
                            Location = objOldDistrict.Location,
                            DistrictId = objOldDistrict.DistrictId,
                            ProvinceId = objOldDistrict.ProvinceId,
                            Type = objOldDistrict.Type,
                            DistrictName = objOldDistrict.DistrictName
                        });
                    }
                    await m_objDistrictsDb.InsertManyAsync(lstDistricts);
                }
            });
            objTask.Wait();

            m_objWardsDb = DbCollertionManager.GetDbCollection<Wards>();
            objTask = Task.Run(async () =>
            {
                lstWards = await m_objWardsDb.FindAsync(obj => true).Result.ToListAsync();
                if (lstWards == null || lstWards.Count == 0)
                {
                    strFilePath = Path.Combine(SysFolder.APP_PATH, SysFolder.DATABASE_OLD_FOLDER, DataFilePath.WARDS_FILE_NAME);
                    if (File.Exists(strFilePath) == false)
                    {
                        return;
                    }
                    strValue = File.ReadAllText(strFilePath);
                    lstOldWards = ObjHelper.JSONToObject<List<OldDataModels.Wards>>(strValue);
                    if (lstOldWards == null)
                    {
                        return;
                    }
                    lstWards = new List<Wards>();
                    foreach (OldDataModels.Wards objOldWard in lstOldWards)
                    {
                        if (objOldWard == null)
                        {
                            continue;
                        }

                        lstWards.Add(new Wards
                        {
                            Id = objOldWard.Id,
                            Location = objOldWard.Location,
                            DistrictId = objOldWard.DistrictId,
                            WardId = objOldWard.WardId,
                            Type = objOldWard.Type,
                            WardName = objOldWard.WardName
                        });
                    }
                    await m_objWardsDb.InsertManyAsync(lstWards);
                }
            });
            objTask.Wait();

            // Facultys
            m_objFacultysDb = DbCollertionManager.GetDbCollection<Facultys>();
            objTask = Task.Run(async () =>
            {
                lstFacultys = await m_objFacultysDb.FindAsync(obj => true).Result.ToListAsync();
                if (lstFacultys == null || lstFacultys.Count == 0)
                {
                    strFilePath = Path.Combine(SysFolder.APP_PATH, SysFolder.DATABASE_OLD_FOLDER, DataFilePath.FACULTY_FILE_NAME);
                    if (File.Exists(strFilePath) == false)
                    {
                        return;
                    }
                    strValue = File.ReadAllText(strFilePath);
                    lstOldFacultys = ObjHelper.JSONToObject<List<OldDataModels.Facultys>>(strValue);
                    if (lstOldFacultys == null)
                    {
                        return;
                    }
                    lstFacultys = new List<Facultys>();
                    foreach (OldDataModels.Facultys objOldFaculty in lstOldFacultys)
                    {
                        if (objOldFaculty == null)
                        {
                            continue;
                        }

                        lstFacultys.Add(new Facultys
                        {
                            Id = objOldFaculty.id,
                            FacultyName = objOldFaculty.FacultyName
                        });
                    }
                    await m_objFacultysDb.InsertManyAsync(lstFacultys);
                }
            });
            objTask.Wait();

            // Roles
            m_objRolesDb = DbCollertionManager.GetDbCollection<Roles>();
            objTask = Task.Run(async () =>
            {
                int nPermissionGroup;
                lstRoles = await m_objRolesDb.FindAsync(obj => true).Result.ToListAsync();
                if (lstRoles == null || lstRoles.Count == 0)
                {
                    strFilePath = Path.Combine(SysFolder.APP_PATH, SysFolder.DATABASE_OLD_FOLDER, DataFilePath.ROLE_FILE_NAME);
                    if (File.Exists(strFilePath) == false)
                    {
                        return;
                    }
                    strValue = File.ReadAllText(strFilePath);
                    lstOldRoles = ObjHelper.JSONToObject<List<OldDataModels.Role>>(strValue);
                    if (lstOldRoles == null)
                    {
                        return;
                    }
                    lstRoles = new List<Roles>();
                    foreach (OldDataModels.Role objOldRole in lstOldRoles)
                    {
                        if (objOldRole == null)
                        {
                            continue;
                        }

                        lstRoles.Add(new Roles
                        {
                            Id = objOldRole.Id,
                            RoleName = objOldRole.RoleName,
                            CreaterId = "6445513a471ca7eab4cd4311",

                        });

                        lstPermission = Enum.GetValues(typeof(DTUSVCPermission)).Cast<DTUSVCPermission>().ToList();
                        switch (objOldRole.RoleId)
                        {
                            case 0:
                                lstRoles[lstRoles.Count - 1].Permissions = lstPermission.FindAll(obj => obj != DTUSVCPermission.NONE).Select(obj => (int)obj).ToList();
                                break;
                            case 1:
                            case 2:
                                foreach (DTUSVCPermission ePemission in lstPermission)
                                {
                                    nPermissionGroup = (int)ePemission / 1000;
                                    if ((nPermissionGroup == 1) ||
                                        (nPermissionGroup == 2) ||
                                        (nPermissionGroup == 3) ||
                                        (nPermissionGroup == 5) ||
                                        (nPermissionGroup == 7))
                                    {
                                        lstRoles[lstRoles.Count - 1].Permissions.Add((int)ePemission);
                                    }
                                }
                                lstRoles[lstRoles.Count - 1].Permissions.Add((int)DTUSVCPermission.UPDATE_MYDTU_YEAR_SETTING);
                                lstRoles[lstRoles.Count - 1].Permissions.Add((int)DTUSVCPermission.UPLOAD_IMAGE);
                                break;
                            case 3:
                                lstRoles[lstRoles.Count - 1].Permissions.Add((int)DTUSVCPermission.SEARCH_MEMBER);
                                lstRoles[lstRoles.Count - 1].Permissions.Add((int)DTUSVCPermission.GET_MEMBER_LIST);
                                lstRoles[lstRoles.Count - 1].Permissions.Add((int)DTUSVCPermission.DOWNLOAD_MEMBER_LIST);
                                lstRoles[lstRoles.Count - 1].Permissions.Add((int)DTUSVCPermission.SEARCH_MEMBER_CONTAINS);
                                lstRoles[lstRoles.Count - 1].Permissions.Add((int)DTUSVCPermission.GET_ALL_RECRUITMENT);
                                lstRoles[lstRoles.Count - 1].Permissions.Add((int)DTUSVCPermission.REVIEWER);
                                lstRoles[lstRoles.Count - 1].Permissions.Add((int)DTUSVCPermission.GET_MEMBER_INTERVIEW);
                                lstRoles[lstRoles.Count - 1].Permissions.Add((int)DTUSVCPermission.GET_MEMBER_INFO_INTERVIEW);
                                lstRoles[lstRoles.Count - 1].Permissions.Add((int)DTUSVCPermission.DELETE_MEMBER_INTERVIEW_IN_ROOM);
                                lstRoles[lstRoles.Count - 1].Permissions.Add((int)DTUSVCPermission.MOVE_INTERVIEW);
                                lstRoles[lstRoles.Count - 1].Permissions.Add((int)DTUSVCPermission.SEND_INTERVIEW_MAIL);
                                lstRoles[lstRoles.Count - 1].Permissions.Add((int)DTUSVCPermission.DOWNLOAD_MEMBER_INTERVIEW);
                                lstRoles[lstRoles.Count - 1].Permissions.Add((int)DTUSVCPermission.EXPORT_ACTIVITY);
                                lstRoles[lstRoles.Count - 1].Permissions.Add((int)DTUSVCPermission.CHECK_IN_ACTIVITY);
                                lstRoles[lstRoles.Count - 1].Permissions.Add((int)DTUSVCPermission.UPDATE_CHECK_IN_ACTIVITY);
                                lstRoles[lstRoles.Count - 1].Permissions.Add((int)DTUSVCPermission.GET_ACTIVITY_STATISTICS);
                                lstRoles[lstRoles.Count - 1].Permissions.Add((int)DTUSVCPermission.GET_LIST_MEMBER_IN_ACTIVITY);
                                break;
                            case 4:
                                foreach (DTUSVCPermission ePemission in lstPermission)
                                {
                                    nPermissionGroup = (int)ePemission / 1000;
                                    if (nPermissionGroup == 5)
                                    {
                                        lstRoles[lstRoles.Count - 1].Permissions.Add((int)ePemission);
                                    }
                                }
                                lstRoles[lstRoles.Count - 1].Permissions.Add((int)DTUSVCPermission.SEARCH_MEMBER);
                                lstRoles[lstRoles.Count - 1].Permissions.Add((int)DTUSVCPermission.GET_MEMBER_LIST);
                                lstRoles[lstRoles.Count - 1].Permissions.Add((int)DTUSVCPermission.DOWNLOAD_MEMBER_LIST);
                                lstRoles[lstRoles.Count - 1].Permissions.Add((int)DTUSVCPermission.SEARCH_MEMBER_CONTAINS);
                                lstRoles[lstRoles.Count - 1].Permissions.Add((int)DTUSVCPermission.GET_ALL_RECRUITMENT);
                                lstRoles[lstRoles.Count - 1].Permissions.Add((int)DTUSVCPermission.REVIEWER);
                                lstRoles[lstRoles.Count - 1].Permissions.Add((int)DTUSVCPermission.GET_MEMBER_INTERVIEW);
                                lstRoles[lstRoles.Count - 1].Permissions.Add((int)DTUSVCPermission.GET_MEMBER_INFO_INTERVIEW);
                                lstRoles[lstRoles.Count - 1].Permissions.Add((int)DTUSVCPermission.DELETE_MEMBER_INTERVIEW_IN_ROOM);
                                lstRoles[lstRoles.Count - 1].Permissions.Add((int)DTUSVCPermission.MOVE_INTERVIEW);
                                lstRoles[lstRoles.Count - 1].Permissions.Add((int)DTUSVCPermission.SEND_INTERVIEW_MAIL);
                                lstRoles[lstRoles.Count - 1].Permissions.Add((int)DTUSVCPermission.DOWNLOAD_MEMBER_INTERVIEW);
                                lstRoles[lstRoles.Count - 1].Permissions.Add((int)DTUSVCPermission.UPLOAD_IMAGE);
                                break;
                            case 5:
                                foreach (DTUSVCPermission ePemission in lstPermission)
                                {
                                    nPermissionGroup = (int)ePemission / 1000;
                                    if (nPermissionGroup == 6)
                                    {
                                        lstRoles[lstRoles.Count - 1].Permissions.Add((int)ePemission);
                                    }
                                }
                                lstRoles[lstRoles.Count - 1].Permissions.Add((int)DTUSVCPermission.SEARCH_MEMBER);
                                lstRoles[lstRoles.Count - 1].Permissions.Add((int)DTUSVCPermission.GET_MEMBER_LIST);
                                lstRoles[lstRoles.Count - 1].Permissions.Add((int)DTUSVCPermission.DOWNLOAD_MEMBER_LIST);
                                lstRoles[lstRoles.Count - 1].Permissions.Add((int)DTUSVCPermission.SEARCH_MEMBER_CONTAINS);
                                lstRoles[lstRoles.Count - 1].Permissions.Add((int)DTUSVCPermission.GET_ALL_RECRUITMENT);
                                lstRoles[lstRoles.Count - 1].Permissions.Add((int)DTUSVCPermission.REVIEWER);
                                lstRoles[lstRoles.Count - 1].Permissions.Add((int)DTUSVCPermission.GET_MEMBER_INTERVIEW);
                                lstRoles[lstRoles.Count - 1].Permissions.Add((int)DTUSVCPermission.GET_MEMBER_INFO_INTERVIEW);
                                lstRoles[lstRoles.Count - 1].Permissions.Add((int)DTUSVCPermission.DELETE_MEMBER_INTERVIEW_IN_ROOM);
                                lstRoles[lstRoles.Count - 1].Permissions.Add((int)DTUSVCPermission.MOVE_INTERVIEW);
                                lstRoles[lstRoles.Count - 1].Permissions.Add((int)DTUSVCPermission.SEND_INTERVIEW_MAIL);
                                lstRoles[lstRoles.Count - 1].Permissions.Add((int)DTUSVCPermission.DOWNLOAD_MEMBER_INTERVIEW);
                                lstRoles[lstRoles.Count - 1].Permissions.Add((int)DTUSVCPermission.EXPORT_ACTIVITY);
                                lstRoles[lstRoles.Count - 1].Permissions.Add((int)DTUSVCPermission.CHECK_IN_ACTIVITY);
                                lstRoles[lstRoles.Count - 1].Permissions.Add((int)DTUSVCPermission.GET_ACTIVITY_STATISTICS);
                                lstRoles[lstRoles.Count - 1].Permissions.Add((int)DTUSVCPermission.UPDATE_CHECK_IN_ACTIVITY);
                                lstRoles[lstRoles.Count - 1].Permissions.Add((int)DTUSVCPermission.GET_LIST_MEMBER_IN_ACTIVITY);
                                break;
                            case 6:
                                lstRoles[lstRoles.Count - 1].Permissions.Add((int)DTUSVCPermission.SEARCH_MEMBER);
                                lstRoles[lstRoles.Count - 1].Permissions.Add((int)DTUSVCPermission.GET_MEMBER_LIST);
                                lstRoles[lstRoles.Count - 1].Permissions.Add((int)DTUSVCPermission.DOWNLOAD_MEMBER_LIST);
                                lstRoles[lstRoles.Count - 1].Permissions.Add((int)DTUSVCPermission.SEARCH_MEMBER_CONTAINS);
                                lstRoles[lstRoles.Count - 1].Permissions.Add((int)DTUSVCPermission.GET_ALL_RECRUITMENT);
                                lstRoles[lstRoles.Count - 1].Permissions.Add((int)DTUSVCPermission.REVIEWER);
                                lstRoles[lstRoles.Count - 1].Permissions.Add((int)DTUSVCPermission.GET_MEMBER_INTERVIEW);
                                lstRoles[lstRoles.Count - 1].Permissions.Add((int)DTUSVCPermission.GET_MEMBER_INFO_INTERVIEW);
                                lstRoles[lstRoles.Count - 1].Permissions.Add((int)DTUSVCPermission.DELETE_MEMBER_INTERVIEW_IN_ROOM);
                                lstRoles[lstRoles.Count - 1].Permissions.Add((int)DTUSVCPermission.MOVE_INTERVIEW);
                                lstRoles[lstRoles.Count - 1].Permissions.Add((int)DTUSVCPermission.SEND_INTERVIEW_MAIL);
                                lstRoles[lstRoles.Count - 1].Permissions.Add((int)DTUSVCPermission.DOWNLOAD_MEMBER_INTERVIEW);
                                lstRoles[lstRoles.Count - 1].Permissions.Add((int)DTUSVCPermission.EXPORT_ACTIVITY);
                                lstRoles[lstRoles.Count - 1].Permissions.Add((int)DTUSVCPermission.CHECK_IN_ACTIVITY);
                                lstRoles[lstRoles.Count - 1].Permissions.Add((int)DTUSVCPermission.UPDATE_CHECK_IN_ACTIVITY);
                                lstRoles[lstRoles.Count - 1].Permissions.Add((int)DTUSVCPermission.GET_ACTIVITY_STATISTICS);
                                lstRoles[lstRoles.Count - 1].Permissions.Add((int)DTUSVCPermission.GET_LIST_MEMBER_IN_ACTIVITY);
                                break;
                            case 9:
                                foreach (DTUSVCPermission ePemission in lstPermission)
                                {
                                    nPermissionGroup = (int)ePemission / 1000;
                                    if (nPermissionGroup == 6)
                                    {
                                        lstRoles[lstRoles.Count - 1].Permissions.Add((int)ePemission);
                                    }
                                }
                                break;
                            case 10:
                                lstRoles[lstRoles.Count - 1].Permissions.Add((int)DTUSVCPermission.CHECK_IN_ACTIVITY);
                                lstRoles[lstRoles.Count - 1].Permissions.Add((int)DTUSVCPermission.UPDATE_CHECK_IN_ACTIVITY);
                                lstRoles[lstRoles.Count - 1].Permissions.Add((int)DTUSVCPermission.GET_LIST_MEMBER_IN_ACTIVITY);
                                lstRoles[lstRoles.Count - 1].Permissions.Add((int)DTUSVCPermission.GET_ACTIVITY_STATISTICS);
                                break;
                        }
                    }
                    await m_objRolesDb.InsertManyAsync(lstRoles);
                }
            });
            objTask.Wait();

            // EventCheckList
            m_objActivitiesCheckList = DbCollertionManager.GetDbCollection<ActivityChecklist>();
            objTask = Task.Run(async () =>
            {
                lstActivityCheckLists = await m_objActivitiesCheckList.FindAsync(obj => true).Result.ToListAsync();
                if ((lstActivityCheckLists == null) || (lstActivityCheckLists.Count == 0))
                {
                    strFilePath = Path.Combine(SysFolder.APP_PATH, SysFolder.DATABASE_OLD_FOLDER, DataFilePath.ACTIVITY_CHECK_LIST);
                    if (File.Exists(strFilePath) == false)
                    {
                        return;
                    }
                    strValue = File.ReadAllText(strFilePath);
                    lstOldActivityChecklist = ObjHelper.JSONToObject<List<OldDataModels.ActivityChecklist>>(strValue);
                    if (lstOldActivityChecklist == null)
                    {
                        return;
                    }

                    // convert object
                    lstActivityCheckLists = new List<ActivityChecklist>();
                    foreach (OldDataModels.ActivityChecklist objTemp in lstOldActivityChecklist)
                    {
                        if (objTemp == null)
                        {
                            continue;
                        }

                        lstActivityCheckLists.Add(new ActivityChecklist
                        {
                            EntryTime = objTemp.InStart,
                            TimeToLeave = objTemp.OutStart,
                            EventsId = objTemp.ActivitiesAndEventsId,
                            Id = objTemp.Id,
                            ManagerId = objTemp.ManagerId,
                            MemberId = objTemp.MemberId
                        });
                    }
                    await m_objActivitiesCheckList.InsertManyAsync(lstActivityCheckLists);
                }
            });
            objTask.Wait();

            // Recruitment Info
            objTask = Task.Run(async () =>
            {
                m_objRecruitmentInfo = DbCollertionManager.GetDbCollection<RecruitmentInfo>();
                lstRecruitmentInfo = await m_objRecruitmentInfo.FindAsync(obj => true).Result.ToListAsync();
                if ((lstRecruitmentInfo == null) || (lstRecruitmentInfo.Count == 0))
                {
                    strFilePath = Path.Combine(SysFolder.APP_PATH, SysFolder.DATABASE_OLD_FOLDER, DataFilePath.RECRUMENT);
                    if (File.Exists(strFilePath) == false)
                    {
                        return;
                    }
                    strValue = File.ReadAllText(strFilePath);
                    lstOldRecruitmentInfo = ObjHelper.JSONToObject<List<OldDataModels.Recruits>>(strValue);
                    if ((lstOldRecruitmentInfo == null) || (lstOldRecruitmentInfo.Count == 0))
                    {
                        return;
                    }

                    // Convert data
                    lstRecruitmentInfo = new List<RecruitmentInfo>();
                    foreach (OldDataModels.Recruits objTemp in lstOldRecruitmentInfo)
                    {
                        if (objTemp == null)
                        {
                            continue;
                        }

                        lstRecruitmentInfo.Add(new RecruitmentInfo
                        {
                            Content = objTemp.RecruitContent,
                            CreateId = objTemp.CreaterId,
                            EndTime = objTemp.DateEnd,
                            Id = objTemp.Id,
                            IsLock = true,
                            IsStop = true,
                            StartTime = objTemp.DateStart,
                            Title = objTemp.RecruitName
                        });
                    }
                    if (lstRecruitmentInfo.Count > 0)
                    {
                        await m_objRecruitmentInfo.InsertManyAsync(lstRecruitmentInfo);
                    }
                }
            });
            objTask.Wait();

            // Membership Registration
            objTask = Task.Run(async () =>
            {
                m_objMembershipRegist = DbCollertionManager.GetDbCollection<MembershipRegistration>();
                lstMembersRegist = await m_objMembershipRegist.FindAsync(obj => true).Result.ToListAsync();
                if ((lstMembersRegist == null) || (lstMembersRegist.Count == 0))
                {
                    strFilePath = Path.Combine(SysFolder.APP_PATH, SysFolder.DATABASE_OLD_FOLDER, DataFilePath.MEMBER_REGISTER);
                    if (File.Exists(strFilePath) == false)
                    {
                        return;
                    }
                    strValue = File.ReadAllText(strFilePath);
                    lstOldMembersRegist = ObjHelper.JSONToObject<List<OldDataModels.MembershipRegistration>>(strValue);
                    if ((lstOldMembersRegist == null) || (lstOldMembersRegist.Count == 0))
                    {
                        return;
                    }
                    lstMembersRegist = new List<MembershipRegistration>();
                    foreach (OldDataModels.MembershipRegistration objTemp in lstOldMembersRegist)
                    {
                        if (objTemp == null)
                        {
                            return;
                        }
                        // Decrypt data
                        objMemberRegistDecrypt = objTemp.ObjDecript(KEY_DECRYPT);
                        if (objMemberRegistDecrypt == null)
                        {
                            continue;
                        }
                        objMemberRegistNew = new MembershipRegistration();
                        objMemberRegistNew.Id = objTemp.Id;
                        objMemberRegistNew.BirthDay = objTemp.BirthDay.ToString("dd/MM/yyyy");
                        objMemberRegistNew.ClassName = objTemp.ClassName;
                        objMemberRegistNew.Email = objTemp.Email;
                        objMemberRegistNew.FacultyId = objTemp.FacultyId;
                        objMemberRegistNew.LastName = objTemp.LastName;
                        objMemberRegistNew.FirstName = objTemp.FirstName;
                        objMemberRegistNew.LinkFacebook = objTemp.LinkFacebook;
                        objMemberRegistNew.StudentId = objTemp.StudentId;
                        objMemberRegistNew.Sex = objTemp.Sex == true ? "Nam" : "Nữ";
                        objMemberRegistNew.RecruitId = objTemp.RecruitId;
                        objMemberRegistNew.PhoneNumber = objTemp.PhoneNumber;
                        objMemberRegistNew.HometownId = objTemp.HometownId;
                        objMemberRegistNew.Reason = objTemp.Reason;
                        objMemberRegistNew.RegisDate = objTemp.RegistrationDate;
                        // Review
                        if (objTemp.Scorer.MongoIdIsValid() == true)
                        {
                            objMemberRegistNew.InterviewInfo = new InterviewInfo();
                            objMemberRegistNew.InterviewInfo.ReviewerId = objTemp.Scorer;
                            objMemberRegistNew.InterviewInfo.Interviewed = true;
                            objMemberRegistNew.InterviewInfo.EmailSent = true;
                            objMemberRegistNew.InterviewInfo.ReviewContent = objTemp.Reviews;
                            objMemberRegistNew.InterviewInfo.ReviewDate = objTemp.RegistrationDate;
                            objMemberRegistNew.InterviewInfo.InterviewAppointmentTime = objTemp.RegistrationDate;
                            objMemberRegistNew.InterviewInfo.Scores = objTemp.Scores;
                        }
                        // Approval
                        if (objTemp.ApproverId.MongoIdIsValid() == true)
                        {
                            objMemberRegistNew.ApprovalInfo = new ApprovalInfo();
                            objMemberRegistNew.ApprovalInfo.ApproverId = objTemp.ApproverId;
                            objMemberRegistNew.ApprovalInfo.ApprovalDate = objTemp.RegistrationDate;
                        }
                        strMemberHashKey = HashKeyManager.GetNewMemberHashKey(objTemp.Id);
                        objMemberRegistNew = objMemberRegistNew.ObjEncript(strMemberHashKey);
                        lstMembersRegist.Add(objMemberRegistNew);

                        // Encrypt address and update
                        objNewAddressInfo = await m_objAddressInfoDb.FindAsync(obj => obj.Id.Equals(objTemp.HometownId) == true).Result.FirstOrDefaultAsync();
                        objNewAddressInfo = objNewAddressInfo.ObjDecript(SecurityConstant.DEFAULT_HASH_KEY);
                        objNewAddressInfo = objNewAddressInfo.ObjEncript(strMemberHashKey);
                        await m_objAddressInfoDb.ReplaceOneAsync(obj => obj.Id.Equals(objNewAddressInfo.Id) == true, objNewAddressInfo);
                    }
                    // Add to database
                    if (lstMembersRegist.Count > 0)
                    {
                        await m_objMembershipRegist.InsertManyAsync(lstMembersRegist);
                    }
                }
            });
            objTask.Wait();

            // Event
            objTask = Task.Run(async () =>
            {
                m_objEventInfo = DbCollertionManager.GetDbCollection<EventInfo>();
                lstEventInfo = await m_objEventInfo.FindAsync(obj => true).Result.ToListAsync();
                if ((lstEventInfo == null) || (lstEventInfo.Count == 0))
                {
                    strFilePath = Path.Combine(SysFolder.APP_PATH, SysFolder.DATABASE_OLD_FOLDER, DataFilePath.EVENT_INFO);
                    if (File.Exists(strFilePath) == false)
                    {
                        return;
                    }
                    strValue = File.ReadAllText(strFilePath);
                    lstActivitiesAndEvents = strValue.JSONToObject<List<ActivitiesAndEvents>>();
                    if ((lstActivitiesAndEvents == null) || (lstActivitiesAndEvents.Count == 0))
                    {
                        return;
                    }
                    lstEventInfo = new List<EventInfo>();
                    foreach (ActivitiesAndEvents objEvent in lstActivitiesAndEvents)
                    {
                        lstEventInfo.Add(new EventInfo
                        {
                            AddressId = objEvent.AddressId,
                            Content = objEvent.Content,
                            CreaterId = objEvent.CreaterId,
                            CreateTime = objEvent.StartDate,
                            EndDate = objEvent.EndDate,
                            Id = objEvent.Id,
                            StartDate = objEvent.StartDate,
                            IsPublic = objEvent.IsPublic,
                            PosterPath = objEvent.PosterPath,
                            RegistrationDeadline = objEvent.RegistrationDeadline,
                            Title = objEvent.Title
                        });
                    }
                    await m_objEventInfo.InsertManyAsync(lstEventInfo);
                }
            });
            objTask.Wait();

            // Member Register Event
            objTask = Task.Run(async () =>
            {
                m_objMemberRegistEvt = DbCollertionManager.GetDbCollection<MemberRegistEvt>();
                lstMemberRegistEvt = await m_objMemberRegistEvt.FindAsync(obj => true).Result.ToListAsync();
                if ((lstMemberRegistEvt == null) || (lstMemberRegistEvt.Count == 0))
                {
                    strFilePath = Path.Combine(SysFolder.APP_PATH, SysFolder.DATABASE_OLD_FOLDER, DataFilePath.MEMBER_REGIST_EVT);
                    if (File.Exists(strFilePath) == false)
                    {
                        return;
                    }
                    strValue = File.ReadAllText(strFilePath);
                    lstEventRegistrationInformationMember = strValue.JSONToObject<List<EventRegistrationInformationMember>>();
                    if ((lstEventRegistrationInformationMember == null) || (lstEventRegistrationInformationMember.Count == 0))
                    {
                        return;
                    }
                    lstMemberRegistEvt = new List<MemberRegistEvt>();
                    foreach (EventRegistrationInformationMember objMemberRegisterEvt in lstEventRegistrationInformationMember)
                    {
                        lstMemberRegistEvt.Add(new MemberRegistEvt
                        {
                            EventId = objMemberRegisterEvt.ActivitiesAndEventsId,
                            CreateDate = objMemberRegisterEvt.CreateDate,
                            Id = objMemberRegisterEvt.Id,
                            MemberId = objMemberRegisterEvt.MemberId
                        });
                    }
                    await m_objMemberRegistEvt.InsertManyAsync(lstMemberRegistEvt);
                }

            });
            objTask.Wait();

            // Community Regist Event
            objTask = Task.Run(async () =>
            {
                CommunityRegistEvt objCommunityRegistEvt;
                m_objCommunityRegistEvt = DbCollertionManager.GetDbCollection<CommunityRegistEvt>();
                lstCommunityRegistEvt = await m_objCommunityRegistEvt.FindAsync(obj => true).Result.ToListAsync();
                if ((lstCommunityRegistEvt == null) || (lstCommunityRegistEvt.Count == 0))
                {
                    strFilePath = Path.Combine(SysFolder.APP_PATH, SysFolder.DATABASE_OLD_FOLDER, DataFilePath.COMMUNITY_REGIST_EVT);
                    if (File.Exists(strFilePath) == false)
                    {
                        return;
                    }
                    strValue = File.ReadAllText(strFilePath);
                    lstEventRegistrationInformationPublic = strValue.JSONToObject<List<EventRegistrationInformationPublic>>();
                    if ((lstEventRegistrationInformationPublic == null) || (lstEventRegistrationInformationPublic.Count == 0))
                    {
                        return;
                    }

                    lstCommunityRegistEvt = new List<CommunityRegistEvt>();
                    foreach (EventRegistrationInformationPublic objMember in lstEventRegistrationInformationPublic)
                    {
                        objCommunityRegistEvt = CommunityRegistEvtOldToNew(objMember);
                        lstCommunityRegistEvt.Add(objCommunityRegistEvt);
                    }
                    await m_objCommunityRegistEvt.InsertManyAsync(lstCommunityRegistEvt);
                }
            });
            objTask.Wait();

            // Leave Of Absence
            objTask = Task.Run(async () =>
            {
                m_objLeaveOfAbsence = DbCollertionManager.GetDbCollection<LeaveOfAbsence>();
                lstLeaveOfAbsence = await m_objLeaveOfAbsence.FindAsync(obj => true).Result.ToListAsync();
                if ((lstLeaveOfAbsence == null) || (lstLeaveOfAbsence.Count == 0))
                {
                    strFilePath = Path.Combine(SysFolder.APP_PATH, SysFolder.DATABASE_OLD_FOLDER, DataFilePath.LEAVE_OF_ABSENCE);
                    if (File.Exists(strFilePath) == false)
                    {
                        return;
                    }
                    strValue = File.ReadAllText(strFilePath);
                    lstOldLeaveOfAbsence = strValue.JSONToObject<List<OldDataModels.LeaveOfAbsence>>();
                    if ((lstOldLeaveOfAbsence == null) || (lstOldLeaveOfAbsence.Count == 0))
                    {
                        return;
                    }

                    lstLeaveOfAbsence = new List<LeaveOfAbsence>();
                    foreach (OldDataModels.LeaveOfAbsence objLeaveOfAbsence in lstOldLeaveOfAbsence)
                    {
                        lstLeaveOfAbsence.Add(new LeaveOfAbsence
                        {
                            EventId = objLeaveOfAbsence.ActivitiesAndEventsId,
                            CreateDate = objLeaveOfAbsence.CreateDate,
                            Id = objLeaveOfAbsence.Id,
                            MemberId = objLeaveOfAbsence.MemberId,
                            Reason = objLeaveOfAbsence.Reason
                        });
                    }
                    await m_objLeaveOfAbsence.InsertManyAsync(lstLeaveOfAbsence);
                }
            });
            objTask.Wait();

            // Member Support Events
            objTask = Task.Run(async () =>
            {
                m_objMemberSupportEvents = DbCollertionManager.GetDbCollection<MemberSupportEvents>();
                lstMemberSupportEvt = await m_objMemberSupportEvents.FindAsync(obj => true).Result.ToListAsync();
                if ((lstMemberSupportEvt == null) || (lstMemberSupportEvt.Count == 0))
                {
                    strFilePath = Path.Combine(SysFolder.APP_PATH, SysFolder.DATABASE_OLD_FOLDER, DataFilePath.MEMBER_SUPPORT_EVENT);
                    if (File.Exists(strFilePath) == false)
                    {
                        return;
                    }
                    strValue = File.ReadAllText(strFilePath);
                    lstOldMemberSupportEvt = strValue.JSONToObject<List<OldDataModels.MemberSupportEvents>>();
                    if ((lstOldMemberSupportEvt == null) || (lstOldMemberSupportEvt.Count == 0))
                    {
                        return;
                    }

                    lstMemberSupportEvt = new List<MemberSupportEvents>();
                    foreach (OldDataModels.MemberSupportEvents objMemberSupport in lstOldMemberSupportEvt)
                    {
                        lstMemberSupportEvt.Add(new MemberSupportEvents
                        {
                            EventId = objMemberSupport.ActivityId,
                            CreateDate = objMemberSupport.CreateDate,
                            CreaterId = objMemberSupport.AdderId,
                            MemberId = objMemberSupport.MemberId,
                            Id = objMemberSupport.Id
                        });
                    }
                    await m_objMemberSupportEvents.InsertManyAsync(lstMemberSupportEvt);
                }
            });
            objTask.Wait();

            objTask = Task.Run(async () =>
            {
                m_objNewspapers = DbCollertionManager.GetDbCollection<Newspapers>();
                lstNewspapers = await m_objNewspapers.FindAsync(obj => true).Result.ToListAsync();
                if ((lstNewspapers == null) || (lstNewspapers.Count == 0))
                {
                    strFilePath = Path.Combine(SysFolder.APP_PATH, SysFolder.DATABASE_OLD_FOLDER, DataFilePath.NEWSPAPER);
                    if (File.Exists(strFilePath) == false)
                    {
                        return;
                    }
                    strValue = File.ReadAllText(strFilePath);
                    lstOldNewspapers = strValue.JSONToObject<List<OldDataModels.Newspapers>>();
                    if ((lstOldNewspapers == null) || (lstOldNewspapers.Count == 0))
                    {
                        return;
                    }

                    lstNewspapers = new List<Newspapers>();
                    foreach (OldDataModels.Newspapers objOldNewspaper in lstOldNewspapers)
                    {
                        lstNewspapers.Add(new Newspapers
                        {
                            Content = objOldNewspaper.Content,
                            CreateDate = objOldNewspaper.CreateDate,
                            Id = objOldNewspaper.Id,
                            PosterPath = objOldNewspaper.PosterPath,
                            Title = objOldNewspaper.Title,
                            CreaterId = objOldNewspaper.UploaderId
                        });
                    }

                    await m_objNewspapers.InsertManyAsync(lstNewspapers);
                }
            });
            objTask.Wait();
        }

        /// <summary>
        /// Convert OldTo New Model
        /// </summary>
        private static Accounts AccountOldToNewModel(OldDataModels.Accounts x_objAccountOld)
        {
            string strSalt;
            string strOldPasswd;
            Accounts objNewAccount;

            if (x_objAccountOld == null)
            {
                return null;
            }

            strSalt = x_objAccountOld.SaltPassword;
            strOldPasswd = x_objAccountOld.HashPassword.Decrypt(KEY_DECRYPT);
            objNewAccount = new Accounts();
            objNewAccount.Id = x_objAccountOld.Id;
            objNewAccount.IsDefaultPassword = x_objAccountOld.IsDefaultPassword;
            objNewAccount.IsLocked = x_objAccountOld.IsLocked;
            objNewAccount.MemberId = x_objAccountOld.MemberId;
            objNewAccount.Password = strOldPasswd;
            objNewAccount.RoleId = x_objAccountOld.RoleId;
            objNewAccount.Username = x_objAccountOld.Username;

            HashKeyManager.AddPasswdHashKey(x_objAccountOld.Id, strSalt);
            return objNewAccount;
        }

        /// <summary>
        /// Member Old To New Model
        /// </summary>
        private static MemberInfo MemberOldToNewModel(OldDataModels.Members x_objOldMember)
        {
            OldDataModels.Members objOldMember;
            MemberInfo objNewMember;
            string strNewKey;

            if (x_objOldMember == null)
            {
                return null;
            }

            objOldMember = x_objOldMember.ObjDecript(KEY_DECRYPT);
            if (objOldMember == null)
            {
                return null;
            }

            try
            {
                strNewKey = HashKeyManager.GetNewMemberHashKey(objOldMember.Id);
                objNewMember = new MemberInfo();
                objNewMember.AvatarPath = x_objOldMember.AvatarPath;
                objNewMember.BirthDay = x_objOldMember.BirthDay.AddHours(7).ToString("dd/MM/yyyy");
                objNewMember.ClassName = x_objOldMember.ClassName;
                objNewMember.Email = x_objOldMember.Email;
                objNewMember.FacultyId = x_objOldMember.FacultyId;
                objNewMember.FirstName = x_objOldMember.LastName;
                objNewMember.HometownId = x_objOldMember.HometownId;
                objNewMember.Id = x_objOldMember.Id;
                objNewMember.IsMember = x_objOldMember.IsMember;
                objNewMember.JoinDate = x_objOldMember.JoinDate;
                objNewMember.LastName = x_objOldMember.FirstName;
                objNewMember.LinkFacebook = x_objOldMember.LinkFacebook;
                objNewMember.PhoneNumber = x_objOldMember.PhoneNumber;
                objNewMember.Sex = x_objOldMember.Sex == true ? "Nam" : "Nữ";
                objNewMember.StudentId = x_objOldMember.StudentId;

                objNewMember = objNewMember.ObjEncript(strNewKey);
                return objNewMember;
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Community Regist Evt Old To New
        /// </summary>
        /// <param name="x_objMember"></param>
        /// <returns></returns>
        private static CommunityRegistEvt CommunityRegistEvtOldToNew(EventRegistrationInformationPublic x_objMember)
        {
            EventRegistrationInformationPublic objMember;
            CommunityRegistEvt objCommunityRegistEvt;
            string strEncriptKey;

            if (x_objMember == null)
            {
                return null;
            }

            objMember = x_objMember.ObjDecript(KEY_DECRYPT);
            if (objMember == null)
            {
                return null;
            }

            objCommunityRegistEvt = new CommunityRegistEvt
            {
                EventId = objMember.ActivitiesAndEventsId,
                ClassName = objMember.ClassName,
                CreateDate = objMember.CreateDate,
                Email = objMember.Email,
                FacebookPath = objMember.FacebookPath,
                FacultyId = objMember.FacultyId,
                FirstName = objMember.LastName,
                Id = objMember.Id,
                LastName = objMember.FirstName,
                PhoneNumber = objMember.PhoneNumber,
                StudentId = objMember.StudentId
            };

            strEncriptKey = HashKeyManager.GetNewMemberHashKey(objCommunityRegistEvt.Id);
            objCommunityRegistEvt = objCommunityRegistEvt.ObjEncript(strEncriptKey);
            return objCommunityRegistEvt;
        }

        /// <summary>
        /// Address Old To New Model
        /// </summary>
        private static AddressInfo AddressOldToNewModel(OldDataModels.Address x_objOldAddress, string x_strMemberId = null)
        {
            string strHashKey;
            AddressInfo objNewAddress;

            if (x_objOldAddress == null)
            {
                return null;
            }

            strHashKey = SecurityConstant.DEFAULT_HASH_KEY;
            if (string.IsNullOrEmpty(x_strMemberId) == false)
            {
                strHashKey = HashKeyManager.GetMemberHashKey(x_strMemberId);
            }

            objNewAddress = new AddressInfo();
            objNewAddress.DistrictId = x_objOldAddress.DistrictId.Encrypt(strHashKey);
            objNewAddress.Id = x_objOldAddress.Id;
            objNewAddress.ProvinceId = x_objOldAddress.ProvinceId.Encrypt(strHashKey);
            objNewAddress.SpecificAddress = x_objOldAddress.StreesNamed.Encrypt(strHashKey);
            objNewAddress.WardId = x_objOldAddress.WardId.Encrypt(strHashKey);
            return objNewAddress;
        }
        #endregion
    }
}
