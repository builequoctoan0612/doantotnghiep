﻿using DTUSVCv3_Library;
using MongoDB.Driver;
using System.Collections.Generic;

namespace DTUSVCv3_EntityFramework
{
    public class RecruitmentDbManager
    {
        #region Constructor
        public RecruitmentDbManager()
        {
            m_objRecruitmentInfo = DbCollertionManager.GetDbCollection<RecruitmentInfo>();
            m_objMembershipRegist = DbCollertionManager.GetDbCollection<MembershipRegistration>();
            m_objInterviewRooms = DbCollertionManager.GetDbCollection<InterviewRooms>();
            m_objMemberInterviewRoom = DbCollertionManager.GetDbCollection<MemberInterviewRoom>();
            m_objLock = new object();
        }
        #endregion

        #region Fields
        private readonly IMongoCollection<RecruitmentInfo> m_objRecruitmentInfo;
        private readonly IMongoCollection<MembershipRegistration> m_objMembershipRegist;
        private readonly IMongoCollection<InterviewRooms> m_objInterviewRooms;
        private readonly IMongoCollection<MemberInterviewRoom> m_objMemberInterviewRoom;
        private object m_objLock;
        #endregion

        #region Methods
        #region Recruitment
        /// <summary>
        /// Get All Recruitment
        /// </summary>
        /// <returns></returns>
        public async Task<List<RecruitmentInfo>> GetAllRecruitment()
        {
            List<RecruitmentInfo> lstRecruitmentInfo;

            try
            {
                lstRecruitmentInfo = await m_objRecruitmentInfo.FindAsync(obj => true).Result.ToListAsync();
                return lstRecruitmentInfo;
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Get Recruitment By Id
        /// </summary>
        /// <param name="x_strRecruitmentId"></param>
        /// <returns></returns>
        public async Task<RecruitmentInfo> GetRecruitmentById(string x_strRecruitmentId)
        {
            RecruitmentInfo objRecruitmentInfo;

            try
            {
                if (x_strRecruitmentId.MongoIdIsValid() == false)
                {
                    return null;
                }
                objRecruitmentInfo = await m_objRecruitmentInfo.FindAsync(obj => obj.Id.Equals(x_strRecruitmentId)).Result.FirstOrDefaultAsync();
                return objRecruitmentInfo;
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Get Recruitment Valid
        /// </summary>
        /// <returns></returns>
        public async Task<RecruitmentInfo> GetRecruitmentValid()
        {
            RecruitmentInfo objRecruitmentInfo;
            DateTime objCurrentDate;
            try
            {
                objCurrentDate = DateTimeHelper.GetCurrentDateTimeVN();
                objRecruitmentInfo = await m_objRecruitmentInfo.FindAsync(obj => (obj.EndTime > objCurrentDate) &&
                                                                                 (obj.IsStop == false)).Result.FirstOrDefaultAsync();
                return objRecruitmentInfo;
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Delete RecrumentById
        /// </summary>
        /// <param name="x_strRecruitmentId"></param>
        /// <returns></returns>
        public async Task<bool> DeleteRecrumentById(string x_strRecruitmentId)
        {
            DeleteResult objResult;
            try
            {
                if (x_strRecruitmentId.MongoIdIsValid() == false)
                {
                    return false;
                }

                objResult = await m_objRecruitmentInfo.DeleteOneAsync(obj => obj.Id.Equals(x_strRecruitmentId) == true);
                if (objResult.DeletedCount > 0)
                {
                    return true;
                }
                return false;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Update Recruitment
        /// </summary>
        /// <param name="x_objRecruitmentInfo"></param>
        /// <returns></returns>
        public async Task<bool> UpdateRecruitment(RecruitmentInfo x_objRecruitmentInfo)
        {
            ReplaceOneResult objResult;
            try
            {
                if (x_objRecruitmentInfo == null)
                {
                    return false;
                }
                if (x_objRecruitmentInfo.Id.MongoIdIsValid() == false)
                {
                    // Add new
                    x_objRecruitmentInfo.Id = string.Empty;
                    await m_objRecruitmentInfo.InsertOneAsync(x_objRecruitmentInfo);
                    return true;
                }
                else
                {
                    // Update
                    objResult = await m_objRecruitmentInfo.ReplaceOneAsync(obj => obj.Id.Equals(x_objRecruitmentInfo.Id) == true, x_objRecruitmentInfo);
                    if (objResult.ModifiedCount > 0)
                    {
                        return true;
                    }
                    return false;
                }
            }
            catch
            {
                return false;
            }
        }
        #endregion

        #region Membership Registration
        /// <summary>
        /// Get All Members By Recruit Id
        /// </summary>
        /// <param name="x_strRecruitId"></param>
        /// <returns></returns>
        public async Task<List<MembershipRegistration>> GetAllMembers()
        {
            List<MembershipRegistration> lstMembers;
            try
            {
                lstMembers = await m_objMembershipRegist.FindAsync(obj => true).Result.ToListAsync();
                return lstMembers;
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Get Member By Id
        /// </summary>
        /// <param name="x_strRecruitId"></param>
        /// <param name="x_strMemberId"></param>
        /// <returns></returns>
        public async Task<MembershipRegistration> GetMemberById(string x_strRecruitId, string x_strMemberId)
        {
            MembershipRegistration objMembers;
            try
            {
                if ((x_strRecruitId.MongoIdIsValid() == false) || (x_strMemberId.MongoIdIsValid() == false))
                {
                    return null;
                }
                objMembers = await m_objMembershipRegist.FindAsync(obj => (obj.RecruitId.Equals(x_strRecruitId) == true) &&
                                                                          (obj.Equals(x_strMemberId) == true)).Result.FirstOrDefaultAsync();
                return objMembers;
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Delete Member By Id
        /// </summary>
        /// <param name="x_strRecruitId"></param>
        /// <param name="x_strMemberId"></param>
        /// <returns></returns>
        public async Task<bool> DeleteMemberById(string x_strRecruitId, string x_strMemberId)
        {
            DeleteResult objResult;
            try
            {
                if ((x_strRecruitId.MongoIdIsValid() == false) || (x_strMemberId.MongoIdIsValid() == false))
                {
                    return false;
                }
                objResult = await m_objMembershipRegist.DeleteOneAsync(obj => (obj.RecruitId.Equals(x_strRecruitId) == true) &&
                                                                              (obj.Id.Equals(x_strMemberId) == true));
                if (objResult.DeletedCount == 0)
                {
                    return false;
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Delete All Member
        /// </summary>
        /// <param name="x_strRecruitId"></param>
        /// <returns></returns>
        public async Task<bool> DeleteAllMember(string x_strRecruitId)
        {
            DeleteResult objResult;
            try
            {
                if (x_strRecruitId.MongoIdIsValid() == false)
                {
                    return false;
                }
                objResult = await m_objMembershipRegist.DeleteManyAsync(obj => obj.RecruitId.Equals(x_strRecruitId) == true);
                if (objResult.DeletedCount > 0)
                {
                    return true;
                }
                return false;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Get All Member
        /// </summary>
        /// <param name="x_strRecruitId"></param>
        /// <returns></returns>
        public async Task<List<MembershipRegistration>> GetAllMember(string x_strRecruitId)
        {
            List<MembershipRegistration> objResult;
            try
            {
                if (x_strRecruitId.MongoIdIsValid() == false)
                {
                    return null;
                }
                objResult = await m_objMembershipRegist.FindAsync(obj => obj.RecruitId.Equals(x_strRecruitId) == true).Result.ToListAsync();
                return objResult;
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Update Member Regist
        /// </summary>
        /// <param name="x_objMembershipRegistration"></param>
        /// <returns></returns>
        public async Task<bool> UpdateMemberRegist(MembershipRegistration x_objMembershipRegistration)
        {
            ReplaceOneResult objResult;
            try
            {
                if (x_objMembershipRegistration == null)
                {
                    return false;
                }

                // Insert new
                if (x_objMembershipRegistration.Id.MongoIdIsValid() == false)
                {
                    await m_objMembershipRegist.InsertOneAsync(x_objMembershipRegistration);
                    if (x_objMembershipRegistration.Id.MongoIdIsValid() == false)
                    {
                        return false;
                    }
                    return true;
                }
                else
                {
                    objResult = await m_objMembershipRegist.ReplaceOneAsync(obj => obj.Id.Equals(x_objMembershipRegistration.Id) == true,
                                                                                    x_objMembershipRegistration);
                    if (objResult.ModifiedCount > 0)
                    {
                        return true;
                    }
                    return false;
                }
            }
            catch
            {
                return false;
            }
        }
        #endregion

        #region Room Registretion
        public async Task<bool> UpdateInterviewRoom(InterviewRooms x_objData)
        {
            ReplaceOneResult objResult;
            try
            {
                if (x_objData == null)
                {
                    return false;
                }

                // Add new
                if (x_objData.Id.MongoIdIsValid() == false)
                {
                    await m_objInterviewRooms.InsertOneAsync(x_objData);
                    if (x_objData.Id.MongoIdIsValid() == false)
                    {
                        return false;
                    }
                    return true;
                }
                // Update
                else
                {
                    objResult = await m_objInterviewRooms.ReplaceOneAsync(obj => obj.Id.Equals(x_objData.Id) == true, x_objData);
                    if ((objResult != null) && (objResult.ModifiedCount > 0))
                    {
                        return true;
                    }
                    return false;
                }
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Delete Room
        /// </summary>
        /// <param name="x_strRoomId"></param>
        /// <returns></returns>
        public async Task<bool> DeleteRoom(string x_strRoomId)
        {
            DeleteResult objResult;
            try
            {
                if (x_strRoomId.MongoIdIsValid() == false)
                {
                    return false;
                }

                objResult = await m_objInterviewRooms.DeleteOneAsync(obj => obj.Id.Equals(x_strRoomId) == true);
                if ((objResult != null) && (objResult.DeletedCount > 0))
                {
                    return true;
                }

                return false;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Get Room By Id
        /// </summary>
        /// <param name="x_strRoomId"></param>
        /// <returns></returns>
        public async Task<InterviewRooms> GetRoomById(string x_strRoomId)
        {
            InterviewRooms objInterviewRooms;
            try
            {
                objInterviewRooms = await m_objInterviewRooms.FindAsync(obj => obj.Id.Equals(x_strRoomId) == true).Result.FirstOrDefaultAsync();
                return objInterviewRooms;
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Get Rooms By Recruitment Id
        /// </summary>
        /// <param name="x_strRecruitmentId"></param>
        /// <returns></returns>
        public async Task<List<InterviewRooms>> GetRoomsByRecruitmentId(string x_strRecruitmentId)
        {
            List<InterviewRooms> lstInterviewRoom;
            try
            {
                if (x_strRecruitmentId.MongoIdIsValid() == false)
                {
                    return null;
                }

                lstInterviewRoom = await m_objInterviewRooms.FindAsync(obj => obj.RecruitId.Equals(x_strRecruitmentId) == true).Result.ToListAsync();
                return lstInterviewRoom;
            }
            catch
            {
                return null;
            }
        }
        #endregion

        #region Member Interview Room
        /// <summary>
        /// Update Member In Room
        /// </summary>
        /// <param name="x_objData"></param>
        /// <returns></returns>
        public bool UpdateMemberInRoom(MemberInterviewRoom x_objData)
        {
            ReplaceOneResult objResult;
            MemberInterviewRoom objData;
            Task objTask;
            bool bResult;

            lock (m_objLock)
            {
                bResult = false;
                objTask = Task.Run(async () =>
                {
                    try
                    {
                        if (x_objData == null)
                        {
                            return;
                        }
                        objData = x_objData.DeepClone();
                        // add new
                        if (objData.Id.MongoIdIsValid() == false)
                        {
                            bResult = await CheckMemberInRoom(objData.MemberId);
                            if (bResult == false)
                            {
                                return;
                            }

                            await m_objMemberInterviewRoom.InsertOneAsync(objData);
                            if (objData.Id.MongoIdIsValid() == false)
                            {
                                return;
                            }
                            bResult = true;
                            return;
                        }
                        else
                        {
                            objResult = await m_objMemberInterviewRoom.ReplaceOneAsync(obj => obj.Id.Equals(objData.Id) == true, objData);
                            if (objResult.ModifiedCount == 0)
                            {
                                return;
                            }
                            bResult = true;
                            return;
                        }
                    }
                    catch
                    {
                        return;
                    }
                });
                objTask.Wait();
                return bResult;
            }
        }

        /// <summary>
        /// Change Room
        /// </summary>
        /// <param name="x_strMemberRoomId"></param>
        /// <param name="x_strDestinationId"></param>
        /// <returns></returns>
        public async Task<ChangeRoomResult> ChangeRoom(string x_strMemberRoomId, string x_strDestinationId)
        {
            MemberInterviewRoom objMemberInterviewRoom;
            MemberInterviewRoom objNewMemberInterviewRoom;
            InterviewRooms objDestRoom;
            DateTime objCurrentTime;
            long lCurrentMember;
            int nMaxMember;
            try
            {
                if ((x_strMemberRoomId.MongoIdIsValid() == false) ||
                (x_strDestinationId.MongoIdIsValid() == false))
                {
                    return ChangeRoomResult.ID_INVALID;
                }

                // Get dest room
                objDestRoom = await m_objInterviewRooms.FindAsync(obj => obj.Id.Equals(x_strDestinationId) == true).Result.FirstOrDefaultAsync();
                if (objDestRoom == null)
                {
                    return ChangeRoomResult.DEST_ID_INVALID;
                }
                // Check current time and start time
                objCurrentTime = DateTimeHelper.GetCurrentDateTimeVN();
                if (objCurrentTime < objDestRoom.StartDateTime)
                {
                    return ChangeRoomResult.ROOM_STARTED;
                }
                // Get current member in room
                lCurrentMember = await m_objMemberInterviewRoom.CountAsync(obj => obj.IsChangeSchedule == false);
                // Get max member
                nMaxMember = GetMaxMemberInRoom(objDestRoom);
                if (nMaxMember <= lCurrentMember)
                {
                    return ChangeRoomResult.ROOM_FULL;
                }
                // Get member in room
                objMemberInterviewRoom = await m_objMemberInterviewRoom.FindAsync(obj => obj.Id.Equals(x_strMemberRoomId) == true).Result.FirstOrDefaultAsync();
                if (objMemberInterviewRoom == null)
                {
                    return ChangeRoomResult.MEMBER_ID_INVALID;
                }
                // Change room
                objNewMemberInterviewRoom = objMemberInterviewRoom.ChangeRoom(x_strDestinationId);
                // Update old member in room
                await m_objMemberInterviewRoom.ReplaceOneAsync(obj => obj.Id.Equals(x_strMemberRoomId), objMemberInterviewRoom);
                // Add new
                await m_objMemberInterviewRoom.InsertOneAsync(objNewMemberInterviewRoom);
                if (objNewMemberInterviewRoom.Id.MongoIdIsValid() == false)
                {
                    return ChangeRoomResult.UPDATE_FAILED;
                }
                return ChangeRoomResult.UPDATE_SUCCSESS;
            }
            catch
            {
                return ChangeRoomResult.UPDATE_FAILED;
            }
        }

        /// <summary>
        /// Delete Interview Room
        /// </summary>
        /// <param name="x_strMemberRoomId"></param>
        /// <param name="x_strSourceId"></param>
        /// <returns></returns>
        public async Task<bool> DeleteInterviewRoom(string x_strMemberRoomId, string x_strSourceId)
        {
            DeleteResult objResult;
            try
            {
                objResult = await m_objMemberInterviewRoom.DeleteOneAsync(obj => obj.Id.Equals(x_strMemberRoomId) == true && obj.RoomId.Equals(x_strSourceId) == true);
                if (objResult.DeletedCount == 0)
                {
                    return false;
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Get Max Interview Time
        /// </summary>
        /// <param name="x_strRoomId"></param>
        /// <returns></returns>
        public async Task<DateTime> GetMaxInterviewTime(string x_strRoomId)
        {
            DateTime objInterviewTime;
            List<MemberInterviewRoom> lstInterviewRoom;

            try
            {
                if (x_strRoomId.MongoIdIsValid() == false)
                {
                    return DateTime.MinValue;
                }
                lstInterviewRoom = await m_objMemberInterviewRoom.FindAsync(obj => obj.RoomId.Equals(x_strRoomId) == true).Result.ToListAsync();
                if ((lstInterviewRoom == null) || (lstInterviewRoom.Count == 0))
                {
                    return DateTime.MinValue;
                }
                lstInterviewRoom = lstInterviewRoom.OrderByDescending(obj => obj.InterviewTime).ToList();
                objInterviewTime = lstInterviewRoom[0].InterviewTime;
                return objInterviewTime;
            }
            catch
            {
                return DateTime.MinValue;
            }
        }

        /// <summary>
        /// Get Member Interview By Room
        /// </summary>
        /// <param name="x_strRoomId"></param>
        /// <returns></returns>
        public async Task<List<MemberInterviewRoom>> GetMemberInterviewByRoomId(string x_strRoomId)
        {
            List<MemberInterviewRoom> lstMember;
            try
            {
                if (x_strRoomId.MongoIdIsValid() == false)
                {
                    return null;
                }

                lstMember = await m_objMemberInterviewRoom.FindAsync(obj => obj.RoomId.Equals(x_strRoomId) == true).Result.ToListAsync();
                return lstMember;
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Get Member Interview Room By Id
        /// </summary>
        /// <param name="x_strMemberId"></param>
        /// <returns></returns>
        public async Task<MemberInterviewRoom> GetMemberInterviewRoomById(string x_strMemberId, string x_strRoomId, bool x_bIsMemberId = true)
        {
            MemberInterviewRoom objMember;
            try
            {
                if (x_strMemberId.MongoIdIsValid() == false)
                {
                    return null;
                }

                if (x_bIsMemberId == true)
                {
                    objMember = await m_objMemberInterviewRoom.FindAsync(obj => (obj.MemberId.Equals(x_strMemberId) == true) &&
                                                                            (obj.RoomId.Equals(x_strRoomId) == true)).Result.FirstOrDefaultAsync();
                }
                else
                {
                    objMember = await m_objMemberInterviewRoom.FindAsync(obj => (obj.Id.Equals(x_strMemberId) == true) &&
                                                                            (obj.RoomId.Equals(x_strRoomId) == true)).Result.FirstOrDefaultAsync();
                }
                return objMember;
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Get Member Interview Room By Id
        /// </summary>
        /// <param name="x_strMemberId"></param>
        /// <returns></returns>
        public async Task<MemberInterviewRoom> GetMemberInterviewRoomById(string x_strMemberId)
        {
            MemberInterviewRoom objMember;
            try
            {
                if (x_strMemberId.MongoIdIsValid() == false)
                {
                    return null;
                }

                objMember = await m_objMemberInterviewRoom.FindAsync(obj => obj.MemberId.Equals(x_strMemberId) == true).Result.FirstOrDefaultAsync();
                return objMember;
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Get Member Interview
        /// </summary>
        /// <param name="x_strRoomId"></param>
        /// <returns></returns>
        public async Task<List<MemberInterviewRoom>> GetMemberAllInterview()
        {
            List<MemberInterviewRoom> lstMember;
            try
            {
                lstMember = await m_objMemberInterviewRoom.FindAsync(obj => true).Result.ToListAsync();
                return lstMember;
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Check Member In Room
        /// </summary>
        /// <param name="x_strMemberId"></param>
        /// <returns></returns>
        public async Task<bool> CheckMemberInRoom(string x_strMemberId)
        {
            bool bIsExist;

            try
            {
                if (x_strMemberId.MongoIdIsValid() == false)
                {
                    return true;
                }
                bIsExist = await m_objMemberInterviewRoom.FindAsync(obj => obj.MemberId.Equals(x_strMemberId) == true && obj.IsChangeSchedule == false).Result.AnyAsync();
                return bIsExist;
            }
            catch
            {
                return true;
            }
        }

        /// <summary>
        /// Delete Member In Room
        /// </summary>
        /// <param name="x_strId"></param>
        /// <returns></returns>
        public async Task<bool> DeleteMemberInRoom(string x_strId)
        {
            DeleteResult objResult;
            try
            {
                if (x_strId.MongoIdIsValid() == false)
                {
                    return false;
                }

                objResult = await m_objMemberInterviewRoom.DeleteOneAsync(obj => obj.Id.Equals(x_strId) == true);
                if (objResult.DeletedCount == 0)
                {
                    return false;
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Get Max Member In Room
        /// </summary>
        /// <param name="x_objRoom"></param>
        /// <returns></returns>
        public int GetMaxMemberInRoom(InterviewRooms x_objRoom)
        {
            int nMaxMember;
            double dBreakTime;
            double dFullTime;
            double dInterviewTime;

            try
            {
                nMaxMember = 0;
                dBreakTime = 0;
                if (x_objRoom == null)
                {
                    return -1;
                }
                // Cal break time
                if ((x_objRoom.BreakTimes != null) && (x_objRoom.BreakTimes.Count > 0))
                {
                    foreach (BreakTime objBreakTime in x_objRoom.BreakTimes)
                    {
                        dBreakTime += objBreakTime.EndDateTime.Subtract(objBreakTime.StartDateTime).TotalMinutes;
                    }
                }

                // Cal full time
                dFullTime = x_objRoom.EndDateTime.Subtract(x_objRoom.StartDateTime).TotalMinutes;
                // Cal interview time
                dInterviewTime = dFullTime - dBreakTime;
                if (dInterviewTime < x_objRoom.RatedTime)
                {
                    return 0;
                }

                nMaxMember = (int)Math.Round((dInterviewTime / x_objRoom.RatedTime) + 0.5, 0);
                return nMaxMember;
            }
            catch
            {
                return -1;
            }
        }
        #endregion
        #endregion
    }
}
