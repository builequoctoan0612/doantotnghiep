﻿using DTUSVCv3_Library;
using MongoDB.Driver;

namespace DTUSVCv3_EntityFramework
{
    public class MemberDbManager
    {
        #region Constructor
        public MemberDbManager()
        {
            m_objMembers = DbCollertionManager.GetDbCollection<MemberInfo>();
        }
        #endregion

        #region Fields
        private readonly IMongoCollection<MemberInfo> m_objMembers;
        #endregion

        #region Methods
        /// <summary>
        /// Get All Member
        /// </summary>
        public async Task<List<MemberInfo>> GetAllMember()
        {
            List<MemberInfo> lstMembers;

            try
            {
                lstMembers = await m_objMembers.FindAsync(obj => true).Result.ToListAsync();
                return lstMembers;
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Get Member By Id
        /// </summary>
        public async Task<MemberInfo> GetMemberById(string x_strMemberId)
        {
            MemberInfo objMember;
            try
            {
                if(x_strMemberId.MongoIdIsValid() == false)
                {
                    return null;
                }

                objMember = await m_objMembers.FindAsync(obj => obj.Id.Equals(x_strMemberId) == true).Result.FirstOrDefaultAsync();
                return objMember;
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Update Member
        /// </summary>
        public async Task<bool> UpdateMember(MemberInfo x_objMember)
        {
            ReplaceOneResult objRes;

            try
            {
                if(x_objMember == null)
                {
                    return false;
                }

                if(x_objMember.Id.MongoIdIsValid() == false)
                {
                    await m_objMembers.InsertOneAsync(x_objMember);
                    return true;
                }
                else
                {
                    objRes = await m_objMembers.ReplaceOneAsync(obj => obj.Id.Equals(x_objMember.Id) == true, x_objMember);
                    if(objRes.ModifiedCount > 0)
                    {
                        return true;
                    }
                    return false;
                }
            }
            catch
            {
                return false;
            }
        }
        #endregion
    }
}
