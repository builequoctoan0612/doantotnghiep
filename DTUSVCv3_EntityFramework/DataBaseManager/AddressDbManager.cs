﻿using DTUSVCv3_Library;
using MongoDB.Driver;

namespace DTUSVCv3_EntityFramework
{
    public class AddressDbManager
    {
        #region Constructor
        public AddressDbManager()
        {
            m_objAddressInfo = DbCollertionManager.GetDbCollection<AddressInfo>();
            m_objProvinces = DbCollertionManager.GetDbCollection<Provinces>();
            m_objDistricts = DbCollertionManager.GetDbCollection<Districts>();
            m_objWards = DbCollertionManager.GetDbCollection<Wards>();
        }
        #endregion

        #region Fields
        private readonly IMongoCollection<AddressInfo> m_objAddressInfo;
        private readonly IMongoCollection<Provinces> m_objProvinces;
        private readonly IMongoCollection<Districts> m_objDistricts;
        private readonly IMongoCollection<Wards> m_objWards;
        #endregion

        #region Methods

        #region Address Info
        /// <summary>
        /// Get Address Info By Id
        /// </summary>
        public async Task<AddressInfo> GetAddressInfoById(string x_strAddressId)
        {
            AddressInfo objAddress;

            try
            {
                if (x_strAddressId.MongoIdIsValid() == false)
                {
                    return null;
                }

                objAddress = await m_objAddressInfo.FindAsync(obj => obj.Id.Equals(x_strAddressId) == true).Result.FirstOrDefaultAsync();
                return objAddress;
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Update Address
        /// </summary>
        public async Task<bool> UpdateAddress(AddressInfo x_objAddress)
        {
            ReplaceOneResult objRes;
            try
            {
                if (x_objAddress == null)
                {
                    return false;
                }
                // add
                if (x_objAddress.Id.MongoIdIsValid() == false)
                {
                    x_objAddress.Id = string.Empty;
                    await m_objAddressInfo.InsertOneAsync(x_objAddress);
                    return true;
                }
                // update
                else
                {
                    objRes = await m_objAddressInfo.ReplaceOneAsync(obj => obj.Id.Equals(x_objAddress.Id) == true, x_objAddress);
                    if (objRes.ModifiedCount > 0)
                    {
                        return true;
                    }
                    return false;
                }
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Delete Address
        /// </summary>
        public async Task<bool> DeleteAddress(string x_strAddressId)
        {
            DeleteResult objRes;
            try
            {
                if (x_strAddressId.MongoIdIsValid() == false)
                {
                    return false;
                }

                objRes = await m_objAddressInfo.DeleteOneAsync(obj => obj.Id.Equals(x_strAddressId) == true);
                if (objRes.DeletedCount > 0)
                {
                    return true;
                }

                return false;
            }
            catch
            {
                return false;
            }
        }
        #endregion

        #region Provinces
        /// <summary>
        /// Get All Provinces
        /// </summary>
        public async Task<List<Provinces>> GetAllProvinces()
        {
            List<Provinces> lstProvinces;

            try
            {
                lstProvinces = await m_objProvinces.FindAsync(obj => true).Result.ToListAsync();
                return lstProvinces;
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Get Province By Id
        /// </summary>
        public async Task<Provinces> GetProvinceById(string x_strProvinceId)
        {
            Provinces objProvinces;

            try
            {
                if (x_strProvinceId.MongoIdIsValid() == false)
                {
                    objProvinces = await m_objProvinces.FindAsync(obj => obj.ProvinceId.Equals(x_strProvinceId) == true).Result.FirstOrDefaultAsync();
                }
                else
                {
                    objProvinces = await m_objProvinces.FindAsync(obj => obj.Id.Equals(x_strProvinceId) == true).Result.FirstOrDefaultAsync();
                }
                return objProvinces;
            }
            catch
            {
                return null;
            }
        }
        #endregion

        #region Districts
        /// <summary>
        /// Get Districts By Province Id
        /// </summary>
        public async Task<List<Districts>> GetDistrictsByProvinceId(string x_strProvinceId)
        {
            List<Districts> lstDistricts;

            try
            {
                lstDistricts = await m_objDistricts.FindAsync(obj => obj.ProvinceId.Equals(x_strProvinceId) == true).Result.ToListAsync();
                return lstDistricts;
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Get District By Id
        /// </summary>
        public async Task<Districts> GetDistrictById(string x_strDistrictId)
        {
            Districts objDistrict;

            try
            {
                if(x_strDistrictId.MongoIdIsValid() == false)
                {
                    objDistrict = await m_objDistricts.FindAsync(obj => obj.DistrictId.Equals(x_strDistrictId) == true).Result.FirstOrDefaultAsync();
                }
                else
                {
                    objDistrict = await m_objDistricts.FindAsync(obj => obj.Id.Equals(x_strDistrictId) == true).Result.FirstOrDefaultAsync();
                }
                return objDistrict;
            }
            catch
            {
                return null;
            }
        }
        #endregion

        #region Wards
        /// <summary>
        /// Get Wards By District Id
        /// </summary>
        public async Task<List<Wards>> GetWardsByDistrictId(string x_strDistrictId)
        {
            List<Wards> lstWards;

            try
            {
                lstWards = await m_objWards.FindAsync(obj => obj.DistrictId.Equals(x_strDistrictId) == true).Result.ToListAsync();
                return lstWards;
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Get Ward By Id
        /// </summary>
        public async Task<Wards> GetWardById(string x_strWardId)
        {
            Wards objWard;

            try
            {
                if(x_strWardId.MongoIdIsValid() == false)
                {
                    objWard = await m_objWards.FindAsync(obj => obj.WardId.Equals(x_strWardId) == true).Result.FirstOrDefaultAsync();
                }
                else
                {
                    objWard = await m_objWards.FindAsync(obj => obj.Id.Equals(x_strWardId) == true).Result.FirstOrDefaultAsync();
                }
                return objWard;
            }
            catch
            {
                return null;
            }
        }
        #endregion
        #endregion
    }
}
