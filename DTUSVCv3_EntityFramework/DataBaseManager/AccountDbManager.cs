﻿using DTUSVCv3_Library;
using MongoDB.Driver;

namespace DTUSVCv3_EntityFramework
{
    public class AccountDbManager
    {
        #region Constructor
        public AccountDbManager()
        {
            m_objAccountDb = DbCollertionManager.GetDbCollection<Accounts>();
        }
        #endregion

        #region Fields
        private readonly IMongoCollection<Accounts> m_objAccountDb;
        #endregion

        #region Methods
        /// <summary>
        /// Get List Account
        /// </summary>
        public async Task<List<Accounts>> GetListAccount()
        {
            List<Accounts> lstAccount;

            try
            {
                lstAccount = await m_objAccountDb.FindAsync(obj => true).Result.ToListAsync();
                return lstAccount;
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Get Account By Id
        /// </summary>
        public async Task<Accounts> GetAccountById(string x_strAccountId)
        {
            Accounts objAccount;

            try
            {
                if (x_strAccountId.MongoIdIsValid() == false)
                {
                    return null;
                }

                objAccount = await m_objAccountDb.FindAsync(obj => obj.Id.Equals(x_strAccountId) == true).Result.FirstOrDefaultAsync();
                return objAccount;
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Get Account By Id
        /// </summary>
        public async Task<Accounts> GetAccountByMemberId(string x_strMemberId)
        {
            Accounts objAccount;

            try
            {
                if (x_strMemberId.MongoIdIsValid() == false)
                {
                    return null;
                }

                objAccount = await m_objAccountDb.FindAsync(obj => obj.MemberId.Equals(x_strMemberId) == true).Result.FirstOrDefaultAsync();
                return objAccount;
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Get Account By UserName
        /// </summary>
        public async Task<Accounts> GetAccountByUserName(string x_strUsername)
        {
            Accounts objAccount;

            try
            {
                x_strUsername = x_strUsername.ToLower();
                objAccount = await m_objAccountDb.FindAsync(obj => obj.Username.Equals(x_strUsername) == true).Result.FirstOrDefaultAsync();
                return objAccount;
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Update Account
        /// </summary>
        public async Task<bool> UpdateAccount(Accounts x_objAccounts)
        {
            try
            {
                if (x_objAccounts == null)
                {
                    return false;
                }

                // Add
                if (x_objAccounts.Id.MongoIdIsValid() == false)
                {
                    await m_objAccountDb.InsertOneAsync(x_objAccounts);
                }
                // update
                else
                {
                    await m_objAccountDb.ReplaceOneAsync(obj => obj.Id.Equals(x_objAccounts.Id), x_objAccounts);
                }
                return true;
            }
            catch
            {
                return false;
            }

        }
        #endregion
    }
}
