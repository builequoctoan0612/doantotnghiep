﻿using DTUSVCv3_Library;
using MongoDB.Driver;

namespace DTUSVCv3_EntityFramework
{
    public class ShortLinkDbManager
    {
        #region Constructor
        public ShortLinkDbManager()
        {
            m_objShortLinkInfo = DbCollertionManager.GetDbCollection<ShortLinkInfo>();
        }
        #endregion

        #region Fields
        private readonly IMongoCollection<ShortLinkInfo> m_objShortLinkInfo;
        #endregion

        #region Methods
        /// <summary>
        /// Get Short Link By Key Code
        /// </summary>
        /// <param name="x_strKeyCode"></param>
        /// <returns></returns>
        public async Task<ShortLinkInfo> GetShortLinkByKeyCode(string x_strKeyCode)
        {
            ShortLinkInfo objShortLinkInfo;
            try
            {
                objShortLinkInfo = await m_objShortLinkInfo.FindAsync(obj => obj.KeyCode.Equals(x_strKeyCode) == true).Result.FirstOrDefaultAsync();
                return objShortLinkInfo;
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Get Short Link By URL
        /// </summary>
        /// <param name="x_strURL"></param>
        /// <returns></returns>
        public async Task<ShortLinkInfo> GetShortLinkByURL(string x_strURL)
        {
            ShortLinkInfo objShortLinkInfo;
            try
            {
                objShortLinkInfo = await m_objShortLinkInfo.FindAsync(obj => obj.URL.Equals(x_strURL) == true).Result.FirstOrDefaultAsync();
                return objShortLinkInfo;
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Create Short Link
        /// </summary>
        /// <param name="x_objShortLinkInfo"></param>
        /// <returns></returns>
        public async Task<bool> CreateShortLink(ShortLinkInfo x_objShortLinkInfo)
        {
            try
            {
                x_objShortLinkInfo.Id = string.Empty;
                await m_objShortLinkInfo.InsertOneAsync(x_objShortLinkInfo);
                if (x_objShortLinkInfo.Id.MongoIdIsValid() == true)
                {
                    return true;
                }
                return false;
            }
            catch
            {
                return false;
            }
        }
        #endregion
    }
}
