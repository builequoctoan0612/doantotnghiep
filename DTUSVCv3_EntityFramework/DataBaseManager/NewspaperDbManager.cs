﻿using DTUSVCv3_Library;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTUSVCv3_EntityFramework.DataBaseManager
{
    public class NewspaperDbManager
    {
        #region Constructor
        public NewspaperDbManager()
        {
            m_objNewspapers = DbCollertionManager.GetDbCollection<Newspapers>();
        }
        #endregion

        #region Fields
        private readonly IMongoCollection<Newspapers> m_objNewspapers;
        #endregion

        #region Methods
        /// <summary>
        /// Get Newspaper By Id
        /// </summary>
        /// <param name="x_strNewsId"></param>
        /// <returns></returns>
        public async Task<Newspapers> GetNewspaperById(string x_strNewsId)
        {
            Newspapers objNewspapers;
            try
            {
                if (x_strNewsId.MongoIdIsValid() == false)
                {
                    return null;
                }
                objNewspapers = await m_objNewspapers.FindAsync(obj => obj.Id.Equals(x_strNewsId) == true).Result.FirstOrDefaultAsync();
                return objNewspapers;
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Get All Newspaper
        /// </summary>
        /// <returns></returns>
        public async Task<List<Newspapers>> GetAllNewspaper()
        {
            List<Newspapers> lstNewspapers;
            try
            {
                lstNewspapers = await m_objNewspapers.FindAsync(obj => true).Result.ToListAsync();
                return lstNewspapers;
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Update Newspaper
        /// </summary>
        /// <param name="x_objNewspapers"></param>
        /// <returns></returns>
        public async Task<bool> UpdateNewspaper(Newspapers x_objNewspapers)
        {
            ReplaceOneResult objResult;
            try
            {
                if (x_objNewspapers.Id.MongoIdIsValid() == false)
                {
                    x_objNewspapers.Id = string.Empty;
                    await m_objNewspapers.InsertOneAsync(x_objNewspapers);
                    if (x_objNewspapers.Id.MongoIdIsValid() == false)
                    {
                        return false;
                    }
                    return true;
                }
                else
                {
                    objResult = await m_objNewspapers.ReplaceOneAsync(obj => obj.Id.Equals(x_objNewspapers.Id) == true, x_objNewspapers);
                    if (objResult.ModifiedCount == 0)
                    {
                        return false;
                    }
                    return true;
                }
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Delete Newspaper
        /// </summary>
        /// <param name="x_strNewspaperId"></param>
        /// <returns></returns>
        public async Task<bool> DeleteNewspaper(string x_strNewspaperId)
        {
            DeleteResult objResult;
            try
            {
                if (x_strNewspaperId.MongoIdIsValid() == false)
                {
                    return false;
                }

                objResult = await m_objNewspapers.DeleteOneAsync(obj => obj.Id.Equals(x_strNewspaperId));
                if (objResult.DeletedCount == 0)
                {
                    return false;
                }
                return true;
            }
            catch
            {
                return false;
            }
        }
        #endregion
    }
}
