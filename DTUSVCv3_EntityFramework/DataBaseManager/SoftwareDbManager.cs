﻿using DTUSVCv3_Library;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTUSVCv3_EntityFramework
{
    public class SoftwareDbManager
    {
        #region Constructor
        public SoftwareDbManager()
        {
            m_objSoftwareInfo = DbCollertionManager.GetDbCollection<SoftwareInfo>();
        }
        #endregion

        #region Fields
        private static IMongoCollection<SoftwareInfo> m_objSoftwareInfo;
        #endregion

        #region Methods
        /// <summary>
        /// Get All Software
        /// </summary>
        /// <returns></returns>
        public async Task<List<SoftwareInfo>> GetAllSoftware()
        {
            List<SoftwareInfo> lstSoftwareInfo;
            try
            {
                lstSoftwareInfo = await m_objSoftwareInfo.FindAsync(obj => true).Result.ToListAsync();
                return lstSoftwareInfo;
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Get Software By Id
        /// </summary>
        /// <param name="x_strSoftWareId"></param>
        /// <returns></returns>
        public async Task<SoftwareInfo> GetSoftwareById(string x_strSoftWareId)
        {
            SoftwareInfo objSoftwareInfo;
            try
            {
                if (x_strSoftWareId.MongoIdIsValid() == false)
                {
                    return null;
                }
                objSoftwareInfo = await m_objSoftwareInfo.FindAsync(obj => obj.Id.Equals(x_strSoftWareId)).Result.FirstOrDefaultAsync();
                return objSoftwareInfo;
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Update Software
        /// </summary>
        /// <param name="x_objSoftware"></param>
        /// <returns></returns>
        public async Task<bool> UpdateSoftware(SoftwareInfo x_objSoftware)
        {
            ReplaceOneResult objResult;
            try
            {
                // Create New
                if (x_objSoftware.Id.MongoIdIsValid() == false)
                {
                    await m_objSoftwareInfo.InsertOneAsync(x_objSoftware);
                    if (x_objSoftware.Id.MongoIdIsValid() == false)
                    {
                        return false;
                    }
                }
                // Update
                else
                {
                    objResult = await m_objSoftwareInfo.ReplaceOneAsync(obj => obj.Id.Equals(x_objSoftware.Id), x_objSoftware);
                    if (objResult.ModifiedCount == 0)
                    {
                        return false;
                    }
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Delete Software
        /// </summary>
        /// <param name="x_strSoftWareId"></param>
        /// <returns></returns>
        public async Task<bool> DeleteSoftware(string x_strSoftWareId)
        {
            DeleteResult objResult;
            try
            {
                if (x_strSoftWareId.MongoIdIsValid() == false)
                {
                    return false;
                }

                objResult = await m_objSoftwareInfo.DeleteOneAsync(obj => obj.Id.Equals(x_strSoftWareId));
                if (objResult.DeletedCount == 0)
                {
                    return false;
                }
                return true;
            }
            catch
            {
                return false;
            }
        }
        #endregion
    }
}
