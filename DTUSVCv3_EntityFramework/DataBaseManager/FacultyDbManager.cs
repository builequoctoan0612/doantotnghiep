﻿using DTUSVCv3_Library;
using MongoDB.Driver;

namespace DTUSVCv3_EntityFramework
{
    public class FacultyDbManager
    {
        #region Construcstor
        public FacultyDbManager()
        {
            m_objFaculty = DbCollertionManager.GetDbCollection<Facultys>();
        }
        #endregion

        #region Fields
        private readonly IMongoCollection<Facultys> m_objFaculty;
        #endregion

        #region Methods
        /// <summary>
        /// Get All Faculty
        /// </summary>
        public async Task<List<Facultys>> GetAllFaculty()
        {
            List<Facultys> lstFacultys;

            try
            {
                lstFacultys = await m_objFaculty.FindAsync(obj => true).Result.ToListAsync();
                return lstFacultys;
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Get Faculty By Id
        /// </summary>
        public async Task<Facultys> GetFacultyById(string x_FacultyId)
        {
            Facultys objFaculty;

            try
            {
                if (x_FacultyId.MongoIdIsValid() == false)
                {
                    return null;
                }

                objFaculty = await m_objFaculty.FindAsync(obj => obj.Id.Equals(x_FacultyId) == true).Result.FirstOrDefaultAsync();
                return objFaculty;
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Update Faculty
        /// </summary>
        public async Task<bool> UpdateFaculty(Facultys x_objFaculty)
        {
            ReplaceOneResult objResult;
            try
            {
                if (x_objFaculty == null)
                {
                    return false;
                }

                // add new
                if (x_objFaculty.Id.MongoIdIsValid() == false)
                {
                    x_objFaculty.Id = string.Empty;
                    await m_objFaculty.InsertOneAsync(x_objFaculty);
                    return true;
                }
                // update
                else
                {
                    objResult = await m_objFaculty.ReplaceOneAsync(obj => obj.Id.Equals(x_objFaculty.Id), x_objFaculty);
                    if (objResult.ModifiedCount > 0)
                    {
                        return true;
                    }
                    return false;
                }
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Delete Faculty
        /// </summary>
        public async Task<bool> DeleteFaculty(string x_strFacultyId)
        {
            DeleteResult objResult;
            try
            {
                if (x_strFacultyId.MongoIdIsValid() == false)
                {
                    return false;
                }

                objResult = await m_objFaculty.DeleteOneAsync(obj => obj.Id.Equals(x_strFacultyId));
                if (objResult.DeletedCount > 0)
                {
                    return true;
                }
                return false;
            }
            catch
            {
                return false;
            }
        }
        #endregion
    }
}
