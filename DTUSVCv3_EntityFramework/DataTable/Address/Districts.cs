﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace DTUSVCv3_EntityFramework
{
    [Serializable]
    public class Districts
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        public string DistrictName { get; set; }
        public string DistrictId { get; set; }
        public string Location { get; set; }
        public string ProvinceId { get; set; }
        public string Type { get; set; }
    }
}
