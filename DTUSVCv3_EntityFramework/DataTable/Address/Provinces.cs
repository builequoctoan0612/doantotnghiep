﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace DTUSVCv3_EntityFramework
{
    [Serializable]
    public class Provinces
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        public string ProvinceName { get; set; }
        public string ProvinceId { get; set; }
        public string Type { get; set; }
        public string Location { get; set; }
    }
}
