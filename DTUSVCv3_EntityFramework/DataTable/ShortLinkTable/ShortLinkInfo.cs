﻿using DTUSVCv3_Library;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace DTUSVCv3_EntityFramework
{
    public class ShortLinkInfo
    {
        public ShortLinkInfo() {
            CreateDate = DateTimeHelper.GetCurrentDateTimeVN();
        }
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        public string KeyCode { get; set; }
        public string URL { get; set; }
        public DateTime CreateDate { get; set; }
    }
}
