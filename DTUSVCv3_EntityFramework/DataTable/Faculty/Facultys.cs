﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace DTUSVCv3_EntityFramework
{
    [Serializable]
    public class Facultys
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        public string FacultyName { get; set; }
    }
}
