﻿using DTUSVCv3_Library;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace DTUSVCv3_EntityFramework
{
    public class SoftwareInfo
    {
        public SoftwareInfo()
        {
            UpdateDate = DateTimeHelper.GetCurrentDateTimeVN();
            Permission = new List<GGDrivePermission>();
            Id = string.Empty;
        }
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string FileName { get; set; }
        public string Version { get; set; }
        public string FileId { get; set; }
        public string ViewLink { get; set; }
        public string DownloadLink { get; set; }
        [BsonRepresentation(BsonType.ObjectId)]
        public string CreaterId { get; set; }
        public DateTime UpdateDate { get; set; }
        public List<GGDrivePermission> Permission { get; set; }
    }

    public class GGDrivePermission
    {
        public string PermissionId { get; set; }
        public string Type { get; set; }
        public string Role { get; set; }
        public string EmailAddress { get; set; }
    }
}
