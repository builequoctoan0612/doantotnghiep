﻿using DTUSVCv3_Library;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace DTUSVCv3_EntityFramework
{
    public class RecruitmentInfo
    {
        public RecruitmentInfo()
        {
            StartTime = DateTimeHelper.GetCurrentDateTimeVN();
            IsLock = false;
            IsStop = false;
            IsOnline = false;
        }
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public bool IsOnline { get; set; }
        public bool IsLock { get; set; }
        public bool IsStop { get; set; }
        [BsonRepresentation(BsonType.ObjectId)]
        public string CreateId { get; set; }
    }
}
