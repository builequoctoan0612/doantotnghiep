﻿using DTUSVCv3_Library;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System.Collections.Generic;

namespace DTUSVCv3_EntityFramework
{
    public class InterviewRooms
    {
        public InterviewRooms(string x_strCreaterId)
        {
            CreateDate = DateTimeHelper.GetCurrentDateTimeVN();
            StartDateTime = CreateDate;
            EndDateTime = CreateDate;
            CreaterId = x_strCreaterId;
            EmailSending = false;
        }
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        [BsonRepresentation(BsonType.ObjectId)]
        public string RecruitId { get; set; }
        public string RoomAddress { get; set; }
        public string Note { get; set; }
        public DateTime StartDateTime { get; set; }
        public DateTime EndDateTime { get; set; }
        public List<BreakTime> BreakTimes { get; set; }
        public List<string> ManagerIds { get; set; }
        public int RatedTime { get; set; }
        [BsonRepresentation(BsonType.ObjectId)]
        public string CreaterId { get; }
        public bool EmailSending { get; set; }
        public DateTime CreateDate { get; set; }

        /// <summary>
        /// Get Max Member In Room
        /// </summary>
        /// <param name="x_objDbManager"></param>
        /// <returns></returns>
        public async Task<Tuple<int, int>> GetMaxMemberInRoom(RecruitmentDbManager x_objDbManager)
        {
            int nCurrentMemberCount;
            int nBreakTime;
            int nInterViewTime;
            int nMaxMember;
            List<MemberInterviewRoom> lstMember;
            // Value 1: Current member, Value 2: Max member
            Tuple<int, int> objCount;
            // Check Id
            if (Id.MongoIdIsValid() == false || x_objDbManager == null)
            {
                objCount = Tuple.Create(0, 0);
                return objCount;
            }
            // Get all member in room
            lstMember = await x_objDbManager.GetMemberInterviewByRoomId(Id);
            if (lstMember == null || lstMember.Count == 0)
            {
                nCurrentMemberCount = 0;
            }
            else
            {
                nCurrentMemberCount = lstMember.Count(obj => obj.IsChangeSchedule == false);
            }
            // Sum break time
            if (BreakTimes == null || BreakTimes.Count == 0)
            {
                nBreakTime = 0;
            }
            else
            {
                nBreakTime = BreakTimes.Sum(obj => obj.GetBreakTime());
            }
            // Get interview time
            nInterViewTime = GetInterViewTimeInRoom();
            nInterViewTime = nInterViewTime - nBreakTime;
            nMaxMember = (int)Math.Round((((double)nInterViewTime / (double)RatedTime) - 0.5), 0);
            objCount = Tuple.Create(nCurrentMemberCount, nMaxMember);
            return objCount;
        }

        /// <summary>
        /// Get InterView Time In Room
        /// </summary>
        /// <returns></returns>
        private int GetInterViewTimeInRoom()
        {
            int nInterviewTime;
            TimeSpan objCal;

            objCal = EndDateTime.Subtract(StartDateTime);
            nInterviewTime = (int)objCal.TotalMinutes;
            return nInterviewTime;
        }

        /// <summary>
        /// Check Time In Break
        /// </summary>
        /// <param name="x_objInterviewTime"></param>
        /// <returns></returns>
        public bool CheckTimeInBreak(DateTime x_objInterviewTime)
        {
            if (BreakTimes == null || BreakTimes.Count == 0)
            {
                return false;
            }

            foreach (BreakTime objBreakTime in BreakTimes)
            {
                if (objBreakTime == null)
                {
                    continue;
                }

                if ((objBreakTime.StartDateTime <= x_objInterviewTime) && (objBreakTime.EndDateTime >= x_objInterviewTime))
                {
                    return true;
                }
            }
            return false;
        }
    }

    [Serializable]
    public class MemberInterviewRoom
    {
        public MemberInterviewRoom()
        {
            IsChangeSchedule = false;
            IsInterviewed = false;
            SendEmailState = (int)SendEmailType.UNSENT;
            CreateDate = DateTimeHelper.GetCurrentDateTimeVN();
            InterviewTime = DateTime.MinValue;
        }

        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        [BsonRepresentation(BsonType.ObjectId)]
        public string RoomId { get; set; }
        [BsonRepresentation(BsonType.ObjectId)]
        public string RecruitId { get; set; }
        [BsonRepresentation(BsonType.ObjectId)]
        public string MemberId { get; set; }
        [BsonRepresentation(BsonType.ObjectId)]
        public string CreaterId { get; set; }
        public int SendEmailState { get; set; }
        public DateTime InterviewTime { get; set; }
        public DateTime CreateDate { get; set; }
        public bool IsInterviewed { get; set; }
        public bool IsChangeSchedule { get; set; }

        /// <summary>
        /// Change Room
        /// </summary>
        /// <param name="x_strRoomId"></param>
        /// <returns></returns>
        public MemberInterviewRoom ChangeRoom(string x_strRoomId)
        {
            MemberInterviewRoom objMemberInterviewRoom;
            if ((x_strRoomId.MongoIdIsValid() == false) || (IsInterviewed == true))
            {
                return null;
            }

            objMemberInterviewRoom = this.DeepClone();
            objMemberInterviewRoom.RoomId = x_strRoomId;
            objMemberInterviewRoom.Id = string.Empty;
            objMemberInterviewRoom.IsInterviewed = false;
            objMemberInterviewRoom.IsChangeSchedule = false;
            objMemberInterviewRoom.SendEmailState = (int)SendEmailType.WAITING_RESEND;
            IsChangeSchedule = true;

            return objMemberInterviewRoom;
        }
    }

    public class BreakTime
    {
        public DateTime StartDateTime { get; set; }
        public DateTime EndDateTime { get; set; }
        public string Note { get; set; }

        /// <summary>
        /// Get Break Time
        /// </summary>
        /// <returns></returns>
        public int GetBreakTime()
        {
            int nBreakTime;
            TimeSpan objCal;

            objCal = EndDateTime.Subtract(StartDateTime);
            nBreakTime = (int)objCal.TotalMinutes;
            return nBreakTime;
        }
    }
}
