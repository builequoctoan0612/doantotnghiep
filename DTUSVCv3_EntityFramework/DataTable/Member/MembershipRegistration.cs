﻿using DTUSVCv3_Library;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace DTUSVCv3_EntityFramework
{
    public class MembershipRegistration
    {
        public MembershipRegistration()
        {
            RegisDate = DateTimeHelper.GetCurrentDateTimeVN();
            FailedInterView = false;
        }
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string BirthDay { get; set; }
        public string Sex { get; set; }
        [BsonRepresentation(BsonType.ObjectId)]
        public string HometownId { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public string LinkFacebook { get; set; }
        [BsonRepresentation(BsonType.ObjectId)]
        public string FacultyId { get; set; }
        public string ClassName { get; set; }
        public string StudentId { get; set; }
        public DateTime RegisDate { get; set; }
        public string Reason { get; set; }
        [BsonRepresentation(BsonType.ObjectId)]
        public string RecruitId { get; set; }
        public InterviewInfo InterviewInfo { get; set; }
        public ApprovalInfo ApprovalInfo { get; set; }
        public bool FailedInterView { get; set; }
    }

    public class InterviewInfo
    {
        public InterviewInfo()
        {
            InterviewAppointmentTime = DateTimeHelper.GetCurrentDateTimeVN();
            Scores = 0;
            Interviewed = false;
            EmailSent = false;
        }

        [BsonRepresentation(BsonType.ObjectId)]
        public string ReviewerId { get; set; }
        public string ReviewContent { get; set; }
        public string Note { get; set; }
        public float Scores { get; set; }
        public bool EmailSent { get; set; }
        public bool Interviewed { get; set; }
        public DateTime InterviewAppointmentTime { get; set; }
        public DateTime ReviewDate { get; set; }

        /// <summary>
        /// Conduct Interview
        /// </summary>
        /// <param name="x_fScores"></param>
        /// <param name="x_strReviewContent"></param>
        /// <param name="x_strReviewerId"></param>
        public void ConductInterview(float x_fScores, string x_strReviewContent, string x_strReviewerId)
        {
            Scores = x_fScores;
            ReviewContent = x_strReviewContent;
            ReviewerId = x_strReviewerId;
            Interviewed = true;
            ReviewDate = DateTimeHelper.GetCurrentDateTimeVN();
        }
    }

    public class ApprovalInfo
    {
        public ApprovalInfo()
        {
            ApprovalDate = DateTimeHelper.GetCurrentDateTimeVN();
        }

        [BsonRepresentation(BsonType.ObjectId)]
        public string ApproverId { get; set; }
        public DateTime ApprovalDate { get; set; }
    }
}
