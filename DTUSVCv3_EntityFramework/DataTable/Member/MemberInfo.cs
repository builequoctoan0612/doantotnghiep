﻿using DTUSVCv3_Library;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace DTUSVCv3_EntityFramework
{
    public class MemberInfo
    {
        public MemberInfo()
        {
            IsMember = true;
            JoinDate = DateTimeHelper.GetCurrentDateTimeVN();
        }

        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string BirthDay { get; set; }
        public string Sex { get; set; }
        [BsonRepresentation(BsonType.ObjectId)]
        public string HometownId { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public string LinkFacebook { get; set; }
        [BsonRepresentation(BsonType.ObjectId)]
        public string FacultyId { get; set; }
        public string ClassName { get; set; }
        public string StudentId { get; set; }
        public string AvatarPath { get; set; }
        public DateTime JoinDate { get; set; }
        public bool IsMember { get; set; }
    }
}
