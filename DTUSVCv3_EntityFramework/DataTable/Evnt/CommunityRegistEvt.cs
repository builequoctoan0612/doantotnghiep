﻿using DTUSVCv3_Library;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;

namespace DTUSVCv3_EntityFramework
{
    [Serializable]
    public class CommunityRegistEvt
    {
        public CommunityRegistEvt()
        {
            CreateDate = DateTimeHelper.GetCurrentDateTimeVN();
            SendCertificate = CreateDate;
        }
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        [BsonRepresentation(BsonType.ObjectId)]
        public string FacultyId { get; set; }
        public string ClassName { get; set; }
        public string StudentId { get; set; }
        public string FacebookPath { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime SendCertificate { get; set; }
        [BsonRepresentation(BsonType.ObjectId)]
        public string EventId { get; set; }
    }
}
