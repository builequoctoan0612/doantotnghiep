﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace DTUSVCv3_EntityFramework
{
    public class MemberSupportEvents
    {
        public MemberSupportEvents()
        {
            CreateDate = DateTime.UtcNow;
        }
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        [BsonRepresentation(BsonType.ObjectId)]
        public string MemberId { get; set; }
        [BsonRepresentation(BsonType.ObjectId)]
        public string EventId { get; set; }
        [BsonRepresentation(BsonType.ObjectId)]
        public string CreaterId { get; set; }
        public DateTime CreateDate { get; set; }
    }
}
