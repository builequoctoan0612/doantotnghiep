﻿using DTUSVCv3_Library;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;

namespace DTUSVCv3_EntityFramework
{
    public class MemberRegistEvt
    {
        public MemberRegistEvt()
        {
            CreateDate = DateTimeHelper.GetCurrentDateTimeVN();
            SendCertificate = CreateDate;
        }
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        [BsonRepresentation(BsonType.ObjectId)]
        public string MemberId { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime SendCertificate { get; set; }
        [BsonRepresentation(BsonType.ObjectId)]
        public string EventId { get; set; }
    }
}
