﻿using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTUSVCv3_Library;

namespace DTUSVCv3_EntityFramework
{
    public class CertificateSettingText
    {
        public CertificateSettingText()
        {
            CreateDate = DateTimeHelper.GetCurrentDateTimeVN();

        }
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        [BsonRepresentation(BsonType.ObjectId)]
        public string ActivityId { get; set; }
        public float FontSize { get; set; }
        public float LocationX { get; set; }
        public float LocationY { get; set; }
        public int MaxWidth { get; set; }
        public int MaxHeight { get; set; }
        [BsonRepresentation(BsonType.ObjectId)]
        public string FontId { get; set; }
        public int Vertical { get; set; }
        public int Horizontal { get; set; }
        public string FontStyle { get; set; }
        public string Color { get; set; }
        public string DefaultText { get; set; }
        public int ValueType { get; set; }

        [BsonRepresentation(BsonType.ObjectId)]
        public string CreaterId { get; set; }
        public DateTime CreateDate { get; set; }
    }
}
