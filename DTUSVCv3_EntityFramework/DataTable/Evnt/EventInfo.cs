﻿using DTUSVCv3_Library;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;

namespace DTUSVCv3_EntityFramework
{
    public class EventInfo
    {
        public EventInfo()
        {
            IsPublic = false;
            IsCertificateSending = false;
            CreateTime = DateTimeHelper.GetCurrentDateTimeVN();
        }
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        public string Title { get; set; }
        public string PosterPath { get; set; }
        public string Content { get; set; }
        public string CertificateFile { get; set; }
        [BsonRepresentation(BsonType.ObjectId)]
        public string AddressId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime RegistrationDeadline { get; set; }
        public DateTime EndDate { get; set; }
        public DateTime CreateTime { get; set; }
        [BsonRepresentation(BsonType.ObjectId)]
        public string CreaterId { get; set; }
        public bool IsPublic { get; set; }
        public bool IsCertificateSending { get; set; }
    }
}
