﻿using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DTUSVCv3_Library;

namespace DTUSVCv3_EntityFramework
{
    public class CertificateTextFont
    {
        public CertificateTextFont()
        {
            UploadDate = DateTimeHelper.GetCurrentDateTimeVN();
            Id = string.Empty;
        }

        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        public string FontName { get; set; }
        public string FontPath { get; set; }
        public DateTime UploadDate { get; set; }
    }
}
