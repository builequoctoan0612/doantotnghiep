﻿using DTUSVCv3_Library;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace DTUSVCv3_EntityFramework
{
    public class LeaveOfAbsence
    {
        public LeaveOfAbsence()
        {
            CreateDate = DateTimeHelper.GetCurrentDateTimeVN();
        }
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        [BsonRepresentation(BsonType.ObjectId)]
        public string MemberId { get; set; }
        [BsonRepresentation(BsonType.ObjectId)]
        public string EventId { get; set; }
        public string Reason { get; set; }
        public DateTime CreateDate { get; set; }
    }
}
