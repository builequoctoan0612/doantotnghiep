﻿using DTUSVCv3_Library;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;

namespace DTUSVCv3_EntityFramework
{
    [Serializable]
    public class ActivityChecklist
    {
        public ActivityChecklist()
        {
            EntryTime = DateTimeHelper.GetCurrentDateTimeVN();
            TimeToLeave = EntryTime;
            Id = string.Empty;
        }
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        [BsonRepresentation(BsonType.ObjectId)]
        public string MemberId { get; set; }
        [BsonRepresentation(BsonType.ObjectId)]
        public string EventsId { get; set; }
        public DateTime EntryTime { get; set; }
        public DateTime TimeToLeave { get; set; }
        [BsonRepresentation(BsonType.ObjectId)]
        public string ManagerId { get; set; }

        /// <summary>
        /// Calculator Score
        /// </summary>
        /// <returns></returns>
        public double CalScore()
        {
            const double SCORE_RATIO = 60;
            double dScore;
            TimeSpan objSubtract;

            try
            {
                objSubtract = TimeToLeave.Subtract(EntryTime);
                dScore = Math.Round(Math.Abs(objSubtract.TotalMinutes) / SCORE_RATIO, 1);
                return dScore;
            }
            catch
            {
                return 0;
            }
        }
    }
}
