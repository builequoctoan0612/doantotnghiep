﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace DTUSVCv3_EntityFramework
{
    public class Accounts
    {
        public Accounts()
        {
            IsLocked = false;
            IsDefaultPassword = true;
        }
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        [BsonRepresentation(BsonType.ObjectId)]
        public string MemberId { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        [BsonRepresentation(BsonType.ObjectId)]
        public string RoleId { get; set; }
        public bool IsLocked { get; set; }
        public bool IsDefaultPassword { get; set; }
    }
}
