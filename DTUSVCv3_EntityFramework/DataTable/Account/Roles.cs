﻿using DTUSVCv3_Library;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace DTUSVCv3_EntityFramework
{
    public class Roles
    {
        public Roles()
        {
            CreateDate = DateTimeHelper.GetCurrentDateTimeVN();

            // Set default Permission
            Permissions = new List<int>();
        }

        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        public string RoleName { get; set; }
        public List<int> Permissions { get; set; }

        [BsonRepresentation(BsonType.ObjectId)]
        public string CreaterId { get; set; }
        public DateTime CreateDate { get; set; }
    }
}
