﻿using Microsoft.Extensions.DependencyInjection;

namespace DTUSVCv3_Services
{
    public static class DependencyInjection
    {
        /// <summary>
        /// Add Infrastructure
        /// </summary>
        /// <param name="x_objServices"></param>
        /// <returns></returns>
        public static IServiceCollection AddInfrastructure(this IServiceCollection x_objServices)
        {
            x_objServices.AddScoped<IAccountService, AccountService>();
            x_objServices.AddScoped<IMemberService, MemberService>();
            x_objServices.AddScoped<IRecruitmentService, RecruitmentService>();
            x_objServices.AddScoped<IActivitiesService, ActivitiesService>();
            x_objServices.AddScoped<IAddressService, AddressService>();
            x_objServices.AddScoped<INewspaperService, NewspaperService>();
            x_objServices.AddScoped<IFacultyService, FacultyService>();
            x_objServices.AddScoped<IRoleService, RoleService>();
            x_objServices.AddScoped<IShortLinkService, ShortLinkService>();
            x_objServices.AddScoped<ISoftwareService, SoftwareService>();
            return x_objServices;
        }
    }
}
