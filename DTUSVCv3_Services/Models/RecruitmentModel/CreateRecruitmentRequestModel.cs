﻿using DTUSVCv3_EntityFramework;

namespace DTUSVCv3_Services
{
    public class CreateRecruitmentRequestModel
    {
        public string Title { get; set; }
        public string Content { get; set; }
        public string EndTime { get; set; }
    }
    
    public class UpdateRecruitmentRequestModel
    {
        public string Id { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public string EndTime { get; set; }
    }

    public class CandidateInfoRequestModel
    {
        public string FullName { get; set; }
        public string BirthDay { get; set; }
        public string Sex { get; set; }
        public string ProvinceId { get; set; }
        public string DistrictId { get; set; }
        public string WardId { get; set; }
        public string SpecificAddress { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public string LinkFacebook { get; set; }
        public string FacultyId { get; set; }
        public string ClassName { get; set; }
        public string StudentId { get; set; }
        public string Reason { get; set; }
        public string RecruitId { get; set; }
        public string Token { get; set; }
        public string Captcha { get; set; }
    }

    public class CreateInterviewRoomRequestModel
    {
        public string RecruitId { get; set; }
        public string RoomAddress { get; set; }
        public string Note { get; set; }
        public string StartDateTime { get; set; }
        public string EndDateTime { get; set; }
        public List<BreakTimeRequest> BreakTimes { get; set; }
        public List<string> ManagerIds { get; set; }
        public int RatedTime { get; set; }
    }
    public class BreakTimeRequest
    {
        public string StartDateTime { get; set; }
        public string EndDateTime { get; set; }
        public string Note { get; set; }
    }

    public class UpdateInterviewRoomRequestModel
    {
        public string Id { get; set; }
        public string RecruitId { get; set; }
        public string RoomAddress { get; set; }
        public string Note { get; set; }
        public string StartDateTime { get; set; }
        public string EndDateTime { get; set; }
        public List<BreakTimeRequest> BreakTimes { get; set; }
        public List<string> ManagerIds { get; set; }
        public int RatedTime { get; set; }
    }

    public class AddInterviewList
    {
        public string RoomId { get; set; }
        public string RecruitId { get; set; }
        public List<string> InterviewerIds { get; set; }
        public bool IsAuto { get; set; }
    }

    public class UpdateReviewReqeusrModel
    {
        public string MemberId { get; set; }
        public string RoomId { get; set; }
        public string RecruitId { get; set; }
        public string ReviewContent { get; set; }
        public string Note { get; set; }
        public float Scorces { get; set; }
    }

    [Serializable]
    public class MemberApprovalRequestModel
    {
        public List<string> MembersId { get; set; }
        public string RecruitId { get; set; }
        public bool IsAddMessaggeUrl { get; set; }
        public string MessaggeUrl { get; set; }
    }

    public class MoveMemberFromRoom
    {
        public string MemberId { get; set; }
        public string SourceRoomId { get; set; }
        public string DestRoomId { get; set; }
        public string RecruitId { get; set; }
    }
}
