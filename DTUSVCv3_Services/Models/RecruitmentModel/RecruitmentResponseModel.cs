﻿using DTUSVCv3_EntityFramework;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson;
using DTUSVCv3_MemoryData;

namespace DTUSVCv3_Services
{
    public class RecruitmentResponseModel
    {
        public string Id { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public bool IsLock { get; set; }
        public bool IsStop { get; set; }
        public string CreateName { get; set; }
    }

    public class RecruitmentResponseSocketData
    {
        public string Id { get; set; }
        public string MemberId { get; set; }
        public int Type { get; set; }       // 0: Failed; 1: Succsess
    }

    public class RecruitmentMemberInfoExcel
    {
        public RecruitmentMemberInfoExcel()
        {
            Reviewer = string.Empty;
            ReviewContent = string.Empty;
            Note = string.Empty;
            InterviewAppointmentTime = string.Empty;
            ReviewDate = string.Empty;
            Approver = string.Empty;
            ApprovalDate = string.Empty;
            Scores = 0;
            EmailSent = false;
            Interviewed = false;
            FailedInterView = false;
        }
        public DateTime RegisDate { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string BirthDay { get; set; }
        public string Sex { get; set; }
        public string Province { get; set; }
        public string District { get; set; }
        public string Ward { get; set; }
        public string SpecificAddress { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public string LinkFacebook { get; set; }
        public string Faculty { get; set; }
        public string ClassName { get; set; }
        public string StudentId { get; set; }
        public string Reason { get; set; }
        public string Reviewer { get; set; }
        public string ReviewContent { get; set; }
        public string Note { get; set; }
        public float Scores { get; set; }
        public bool EmailSent { get; set; }
        public bool Interviewed { get; set; }
        public string InterviewAppointmentTime { get; set; }
        public string ReviewDate { get; set; }
        public string Approver { get; set; }
        public string ApprovalDate { get; set; }
        public bool FailedInterView { get; set; }
    }
}
