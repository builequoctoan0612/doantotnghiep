﻿namespace DTUSVCv3_Services
{
    public class NewspaperResponseModel
    {
        public string Id { get; set; }
        public string CreaterName { get; set; }
        public string PosterPath { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public DateTime CreateDate { get; set; }
    }
}
