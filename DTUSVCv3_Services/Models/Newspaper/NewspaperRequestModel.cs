﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTUSVCv3_Services
{
    public class CreateNewspaperRequestModel
    {
        public IFormFile Poster { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
    }

    public class UpdateNewspaperRequestModel
    {
        public string Id { get; set; }
        public IFormFile? Poster { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
    }

    public class UploadImage
    {
        public IFormFile Poster { get; set; }
    }
}
