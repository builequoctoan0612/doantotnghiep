﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTUSVCv3_Services.Models.AccountModel
{
    public class ChangeRoleOfAccountRequestModel
    {
        public string RoleId { get; set; }
        public string MemberId { get; set; }
    }
}
