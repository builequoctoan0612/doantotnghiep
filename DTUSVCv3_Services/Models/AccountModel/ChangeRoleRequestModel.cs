﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTUSVCv3_Services
{
    public class ChangeRoleRequestModel
    {
        public string MemberId { get; set; }
        public string RoleId { get; set; }
    }
}
