﻿using DTUSVCv3_EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTUSVCv3_Services
{
    public class LoginResponseModel
    {
        public string AccountId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string AvatarPath { get; set; }
        public Roles Role { get; set; }
        public string AccessToken { get; set; }
        public string ViewFileToken { get; set; }
        public string RefreshToken { get; set; }
        public bool IsDefaultPasswd { get; set; }
    }

    public class RefreshTokenResponse
    {
        public string AccessToken { get; set; }
        public string ViewFileToken { get; set; }
    }
}
