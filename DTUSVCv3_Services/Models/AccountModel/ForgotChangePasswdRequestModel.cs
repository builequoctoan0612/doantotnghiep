﻿namespace DTUSVCv3_Services
{
    public class ForgotChangePasswdRequestModel
    {
        public string Token { get; set; }
        public string NewPassword { get; set; }
        public string PasswordConfirmation { get; set; }
    }

    public class GetCaptchaRequestModel
    {
        public string Token { get; set; }
    }

    public class GetCaptchaResModel
    {
        public string Token { get; set; }
        public string CaptchaPath { get; set; }
    }

    public class ForgotPasswdRequesModel
    {
        public string Token { get; set; }
        public string StudenId { get; set; }
        public string CaptchaValue { get; set; }
    }
}
