﻿namespace DTUSVCv3_Services.Models.AccountModel
{
    public class GetListAccountResponseModel
    {
        public string AccountId { get; set; }
        public string Username { get; set; }
        public bool IsLocked { get; set; }
        public string MemberId { get; set; }
        public string AvatarPath { get; set; }
        public string StudentId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }
        public string FacultyName { get; set; }
    }
}
