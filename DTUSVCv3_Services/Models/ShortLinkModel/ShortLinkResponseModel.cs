﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTUSVCv3_Services
{
    public class ShortLinkResponseModel
    {
        public string KeyCode { get; set; }
        public string URL { get; set; }
    }
}
