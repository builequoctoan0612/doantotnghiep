﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTUSVCv3_Services
{
    public class ShortLinkRequestModel
    {
        public string URL { get; set; }
    }
}
