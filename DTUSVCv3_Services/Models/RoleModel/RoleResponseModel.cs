﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTUSVCv3_Services.Models.RoleModel
{
    public class RoleResponseModel
    {
        public RoleResponseModel()
        {
            Permissions = new List<PermissionsGroup>();
        }
        public string Id { get; set; }
        public string RoleName { get; set; }
        public List<PermissionsGroup> Permissions { get; set; }
    }

    public class PermissionsResponseModel
    {
        public string PermissionsName { get; set; }
        public int PermissionsId { get; set; }
    }

    public class PermissionsGroup
    {
        public PermissionsGroup()
        {
            Permissions = new List<PermissionsResponseModel>();
        }
        public int Id { get; set; }
        public string Name { get; set; }
        public List<PermissionsResponseModel> Permissions { get; set; }
    }
}
