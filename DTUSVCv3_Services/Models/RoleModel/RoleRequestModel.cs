﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTUSVCv3_Services
{
    public class CreateRoleRequestModel
    {
        public string RoleName { get; set; }
        public List<int> Permissions { get; set; }
    }

    public class UpdateRoleRequestModel : CreateRoleRequestModel
    {
        public string Id { get; set; }
    }
}
