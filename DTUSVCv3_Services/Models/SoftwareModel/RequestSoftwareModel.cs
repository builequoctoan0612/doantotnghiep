﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTUSVCv3_Services
{
    public class CreateRequestSoftwareModel
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public string FileName { get; set; }
        public string Version { get; set; }
        public IFormFile SoftwareFile { get; set; }
    }

    public class UpdateRequestSoftwareModel : CreateRequestSoftwareModel
    {
        public string Id { get; set; }
    }

    public class AddRoleRequestSoftwareModel
    {
        public string Id { get; set; }
        public string Type { get; set; }
        public string Role { get; set; }
        public string EmailAddress { get; set; }
    }

    public class DeleteRoleRequestSoftwareModel
    {
        public string Id { get; set; }
        public string PermissionId { get; set; }
    }
}
