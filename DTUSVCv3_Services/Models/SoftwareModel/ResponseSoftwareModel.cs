﻿using DTUSVCv3_EntityFramework;

namespace DTUSVCv3_Services
{
    public class ResponseSoftwareModel
    {
        public string Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string FileName { get; set; }
        public string Version { get; set; }
        public string FileId { get; set; }
        public string ViewLink { get; set; }
        public string DownloadLink { get; set; }
        public string CreaterName { get; set; }
        public DateTime UpdateDate { get; set; }
        public List<GGDrivePermission> Permission { get; set; }
    }

    public class ResponseSoftwareListModel
    {
        public string Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string FileName { get; set; }
        public string Version { get; set; }
        public string FileId { get; set; }
        public string ViewLink { get; set; }
        public string DownloadLink { get; set; }
        public string CreaterName { get; set; }
        public DateTime UpdateDate { get; set; }
    }
}
