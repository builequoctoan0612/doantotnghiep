﻿namespace DTUSVCv3_Services
{
    public class CreateFacultyRequestModel
    {
        public string FacultyName { get; set; }
    }

    public class UpdateFacultyRequestModel : CreateFacultyRequestModel
    {
        public string Id { get; set; }
    }
}
