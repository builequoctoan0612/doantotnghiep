﻿using DTUSVCv3_EntityFramework;
using DTUSVCv3_MemoryData;
using DTUSVCv3_MemoryData.EventData;

namespace DTUSVCv3_Services.Models.ActivitiesModel
{
    public class ActivityResponseModel
    {
        public string Id { get; set; }
        public string Title { get; set; }
        public string PosterPath { get; set; }
        public string Content { get; set; }
        public string CertificateFile { get; set; }
        public AddressInfoModel Address { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime RegistrationDeadline { get; set; }
        public DateTime EndDate { get; set; }
        public DateTime CreateTime { get; set; }
        public string CreaterName { get; set; }
        public bool IsPublic { get; set; }
    }

    public class ActivityHistoryResponseModel : ActivityResponseModel
    {
        public List<ActivityChecklistData> CheckInList { get; set; }

        /// <summary>
        /// Get checkin list
        /// </summary>
        /// <param name="x_strMemberId"></param>
        /// <returns></returns>
        public async Task GetCheckInList(string x_strMemberId)
        {
            CheckInList = await MemberRegistEvtData.Instant.GetCheckList(x_strMemberId, Id);
        }
    }

    public class CheckInResponse
    {
        public CheckInResponse()
        {
            IsMember = false;
            IsCheckIn = false;
            IsCheckOutAll = false;
        }
        public string FullName { get; set; }
        public string StudentId { get; set; }
        public string FacultyName { get; set; }
        public bool IsMember { get; set; }
        public bool IsCheckIn { get; set; }
        public bool IsCheckOutAll { get; set; }
    }

    public class SettingTextResponse : CertificateSettingText
    {
        public string CertificateTemp { get; set; }
    }

    public class ConfirmCertificateModel
    {
        public ConfirmCertificateModel()
        {
            CheckInData = new List<ConfirmCertificateCheckInModel>();
            TotalScore = 0;
        }
        public string FullName { get; set; }
        public string StudentId { get; set; }
        public string FacultyName { get; set; }
        public string EventName { get; set; }
        public string EventPosterPath { get; set; }
        public double TotalScore { get; set; }
        public DateTime SendCertificate { get; set; }
        public List<ConfirmCertificateCheckInModel> CheckInData { get; set; }
    }

    public class ConfirmCertificateCheckInModel
    {
        public DateTime EntryTime { get; set; }
        public DateTime TimeToLeave { get; set; }
        public string ManagerName { get; set; }
        public double Score { get; set; }
    }

    public class TotalActivityStatistics
    {
        public List<ActivityStatisticsByFaculty> Faculty { get; set; }
        public ActivityStatisticsByParticipants Participants { get; set; }
        public ActivityStatisticsByMemberOrCommunity MemberOrCommunity { get; set; }
        public List<GroupByCreateDate> GroupByCreateDate { get; set; }
    }

    public class ActivityStatisticsByFaculty
    {
        public string FacultyName { get; set; }
        public int MemberCount { get; set; }
    }

    public class ActivityStatisticsByParticipants
    {
        public int TotalMember { get; set; }
        public int Joined { get; set; }
        public int NotParticipate { get; set; }
    }

    public class ActivityStatisticsByMemberOrCommunity
    {
        public int TotalMember { get; set; }
        public int CountMember { get; set; }
        public int CountCommunity { get; set; }
    }

    public class GroupByCreateDate
    {
        public string CreateDate { get; set; }
        public int Count { get; set; }
    }
}
