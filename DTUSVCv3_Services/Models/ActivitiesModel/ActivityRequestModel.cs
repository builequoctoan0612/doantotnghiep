﻿using Microsoft.AspNetCore.Http;

namespace DTUSVCv3_Services
{
    public class CreateActivityRequestModel
    {
        public string Title { get; set; }
        public IFormFile? Poster { get; set; }
        public string Content { get; set; }
        public string ProvinceId { get; set; }
        public string DistrictId { get; set; }
        public string WardId { get; set; }
        public string SpecificAddress { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string RegistrationDeadline { get; set; }
        public bool IsPublic { get; set; }
    }

    public class UpdateActivityRequestModel : CreateActivityRequestModel
    {
        public string Id { get; set; }
    }

    public class CommunityRegisterRequestModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public string FacultyId { get; set; }
        public string ClassName { get; set; }
        public string StudentId { get; set; }
        public string FacebookPath { get; set; }
        public string EventId { get; set; }
        public string Token { get; set; }
        public string CaptCha { get; set; }
    }

    public class MemberLeaveActivityRequestModel
    {
        public string EventId { get; set; }
        public string Rason { get; set; }
    }

    public class UpdateCheckInRequestModel
    {
        public string ActivityId { get; set; }
        public string MemberId { get; set; }
        public string CheckInId { get; set; }
        public string CheckInTime { get; set; }
        public string CheckOutTime { get; set; }
        public bool AppliesToAll { get; set; }
    }

    public class UploadTextRequestFont
    {
        public IFormFile FontFile { get; set; }
    }

    public class SetupCertificateTextRequestModel
    {
        public string CertificateTextId { get; set; }
        public string ActivityId { get; set; }
        public int FontSize { get; set; }
        public int LocationX { get; set; }
        public int LocationY { get; set; }
        public int MaxWidth { get; set; }
        public int MaxHeight { get; set; }
        public string FontId { get; set; }
        public int Vertical { get; set; }
        public int Horizontal { get; set; }
        public string Color { get; set; }
        public string DefaultText { get; set; }
        public string FontStyle { get; set; }
        public int ValueType { get; set; }
    }

    public class UploadCertificateRequestModel
    {
        public string ActivityId { get; set; }
        public IFormFile CertificateImage { get; set; }
    }

    [Serializable]
    public class SendCertificateRequestModel
    {
        public string ActivityId { get; set; }
        public List<string>? MemberIds { get; set; }
        public bool IsSendAll { get; set; }
    }
}
