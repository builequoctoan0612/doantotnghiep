﻿namespace DTUSVCv3_Services
{
    public class ChangeMyInfoReqestModel
    {
        public string FullName { get; set; }
        public string BirthDay { get; set; }
        public string Sex { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public string LinkFacebook { get; set; }
        public string FacultyId { get; set; }
        public string ClassName { get; set; }
        public string StudentId { get; set; }
    }

    public class UpdateHometownRequest
    {
        public string SpecificAddress { get; set; }
        public string ProvinceId { get; set; }
        public string DistrictId { get; set; }
        public string WardId { get; set; }
    }
}
