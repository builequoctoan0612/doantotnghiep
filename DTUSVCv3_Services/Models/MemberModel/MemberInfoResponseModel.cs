﻿using DTUSVCv3_EntityFramework;
using DTUSVCv3_MemoryData;
using System.ComponentModel.DataAnnotations;

namespace DTUSVCv3_Services
{
    public class MemberInfoResponseModel
    {
        public string MemberId { get; set; }
        public string FullName { get; set; }
        public string StudentId { get; set; }
        public string BirthDay { get; set; }
        public string Sex { get; set; }
        public AddressInfoModel Hometown { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public string ClassName { get; set; }
        public Facultys Faculty { get; set; }
        public string FacebookPath { get; set; }
        public int MemberType { get; set; }
        public string RoleName { get; set; }
        public string JoinDate { get; set; }
        public double Scores { get; set; }
        public string AvatarPath { get; set; }
    }

    public class MemberInfoExportExcel
    {
        [Display(Name = "MSSV")]
        public string StudentId { get; set; }
        [Display(Name = "Họ")]
        public string FirstName { get; set; }
        [Display(Name = "Tên")]
        public string LastName { get; set; }
        [Display(Name = "Giới tính")]
        public string Sex { get; set; }
        [Display(Name = "Ngày sinh")]
        public string BirthDay { get; set; }
        [Display(Name = "Quê quán")]
        public string Hometown { get; set; }
        [Display(Name = "Lớp")]
        public string ClassName { get; set; }
        [Display(Name = "Khoa")]
        public string FacultyName { get; set; }
        [Display(Name = "Email")]
        public string Email { get; set; }
        [Display(Name = "Số điện thoại")]
        public string PhoneNumber { get; set; }
        [Display(Name = "Chức vụ")]
        public string RoleName { get; set; }
        [Display(Name = "Điểm hoạt động")]
        public double Scores { get; set; }
    }

    public class MemberCardExportData
    {
        public string StudentId { get; set; }
        public string FullName { get; set; }
        public string BirthDay { get; set; }
        public string FacultyName { get; set; }
        public string RoleName { get; set; }
        public string QrCodeName { get; set; }
        public string AvatarName { get; set; }
    }

    public class SeartchMemberData
    {
        public string MemberId { get; set; }
        public string StudentId { get; set; }
        public string FullName { get; set; }
        public string BirthDay { get; set; }
        public string FacultyName { get; set; }
        public string RoleName { get; set; }
        public string AvatarPath { get; set; }
        public string FacebookPath { get; set; }
    }

    public class MemberListResponseModel
    {
        public string AvatarPath { get; set; }
        public string StudentId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Sex { get; set; }
        public string PhoneNumber { get; set; }
        public string FacultyName { get; set; }
        public int MemberType { get; set; }
        public string MemberId { get; set; }
        public string FacebookPath { get; set; }
    }
}
