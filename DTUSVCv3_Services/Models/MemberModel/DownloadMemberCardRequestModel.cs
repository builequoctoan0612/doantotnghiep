﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTUSVCv3_Services
{
    public class DownloadMemberCardRequestModel
    {
        public List<string> MemberId { get; set; }
    }
}
