﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTUSVCv3_Services
{
    public class ChangeAvatarRequestModel
    {
        public IFormFile Avatar { get; set; }
    }
}
