﻿using DTUSVCv3_EntityFramework;
using DTUSVCv3_Library;
using DTUSVCv3_MemoryData;
using DTUSVCv3_MemoryData.EventData;
using Microsoft.AspNetCore.Http;

namespace DTUSVCv3_Services
{
    public class FacultyService : BaseSerivce, IFacultyService
    {
        public FacultyService() : base()
        {

        }

        private const string SOURCE = "FacultyService";

        /// <summary>
        /// Create Faculty
        /// </summary>
        /// <param name="x_objRequest"></param>
        /// <param name="x_strAccountId"></param>
        /// <param name="x_objContext"></param>
        /// <returns></returns>
        public async Task<ResponseModel> CreateFaculty(CreateFacultyRequestModel x_objRequest, string x_strAccountId, HttpContext x_objContext)
        {
            Accounts objAccount;
            MemberDecryptModel objMemberInfo;
            Roles objRole;
            Facultys objFacultys;
            ResponseModel objResponse;
            List<Facultys> lstFacultys;
            string strClientIp;
            bool bResult;

            //----------------------------------
            // Get IP Address
            //----------------------------------
            strClientIp = x_objContext.GetClientIPAddress();
            //----------------------------------
            // Validate request
            //----------------------------------
            if (string.IsNullOrWhiteSpace(x_objRequest.FacultyName) == true)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.FACULTY_NAME_INVALID, ErrorCode.FACULTY_NAME_INVALID);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.FACULTY_NAME_INVALID}", SOURCE);

                return objResponse;
            }
            lstFacultys = await FacultyDbManager.GetAllFaculty();
            objFacultys = lstFacultys.Find(obj => obj.FacultyName.EqualsIgnoreCase(x_objRequest.FacultyName) == true);
            if (objFacultys != null)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.FACULTY_NAME_EXIST, ErrorCode.FACULTY_NAME_EXIST);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.FACULTY_NAME_EXIST}", SOURCE);

                return objResponse;
            }
            x_objRequest.FacultyName = x_objRequest.FacultyName.Trim();

            // Account
            objAccount = await GetAccount(x_strAccountId);
            if (objAccount == null)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.ACCOUNT_IS_LOCKER, ErrorCode.ACCOUNT_IS_LOCKER);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.ACCOUNT_IS_LOCKER}", SOURCE);

                return objResponse;
            }

            // Member
            objMemberInfo = MemberInfoManager.GetMemberById(objAccount.MemberId);
            if (objMemberInfo == null)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.MEMBER_INFO_IS_NULL, ErrorCode.MEMBER_INFO_IS_NULL);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.MEMBER_INFO_IS_NULL}", SOURCE);

                return objResponse;
            }

            // Role
            objRole = await RolesDbManager.GetRoleById(objAccount.RoleId);
            if ((objRole == null) || (objRole.Permissions.Contains((int)DTUSVCPermission.CREATE_FACULTY) == false))
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.AUTHORITY_INSUFFICIENT, ErrorCode.AUTHORITY_INSUFFICIENT);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMemberInfo.GetMemberFullName(), $"{SOURCE}: {ErrorConstant.MEMBER_INFO_IS_NULL}", SOURCE);
                return objResponse;
            }

            objFacultys = new Facultys
            {
                FacultyName = x_objRequest.FacultyName
            };

            bResult = await FacultyDbManager.UpdateFaculty(objFacultys);
            if (bResult == false)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.UPDATE_FACULTY_FAILED, ErrorCode.UPDATE_FACULTY_FAILED);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMemberInfo.GetMemberFullName(), $"{SOURCE}: {ErrorConstant.UPDATE_FACULTY_FAILED}", SOURCE);
                return objResponse;
            }

            objResponse = ResponseManager.GetSuccsessResp(SuccsessConstant.UPDATE_FACULTY_SUCCSESS, objFacultys);
            return objResponse;
        }

        /// <summary>
        /// Get All Faculty
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseModel> GetAllFacultys()
        {
            List<Facultys> lstFacultys;
            ResponseModel objResponse;

            lstFacultys = await FacultyDbManager.GetAllFaculty();

            objResponse = ResponseManager.GetSuccsessResp(string.Empty, lstFacultys, lstFacultys.Count);
            return objResponse;
        }

        /// <summary>
        /// Update Faculty
        /// </summary>
        /// <param name="x_objRequest"></param>
        /// <param name="x_strAccountId"></param>
        /// <param name="x_objContext"></param>
        /// <returns></returns>
        public async Task<ResponseModel> UpdateFaculty(UpdateFacultyRequestModel x_objRequest, string x_strAccountId, HttpContext x_objContext)
        {
            Accounts objAccount;
            MemberDecryptModel objMemberInfo;
            Roles objRole;
            Facultys objFacultys;
            ResponseModel objResponse;
            Task objTask;
            List<Facultys> lstFacultys;
            string strClientIp;
            bool bResult;

            //----------------------------------
            // Get IP Address
            //----------------------------------
            strClientIp = x_objContext.GetClientIPAddress();
            //----------------------------------
            // Validate request
            //----------------------------------
            if (string.IsNullOrWhiteSpace(x_objRequest.FacultyName) == true)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.FACULTY_NAME_INVALID, ErrorCode.FACULTY_NAME_INVALID);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.FACULTY_NAME_INVALID}", SOURCE);

                return objResponse;
            }
            lstFacultys = await FacultyDbManager.GetAllFaculty();
            objFacultys = lstFacultys.Find(obj => obj.Id.Equals(x_objRequest.Id) == true);
            if (objFacultys == null)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.FACULTY_IS_NULL, ErrorCode.FACULTY_IS_NULL);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.FACULTY_IS_NULL}", SOURCE);

                return objResponse;
            }
            objFacultys = lstFacultys.Find(obj => obj.FacultyName.EqualsIgnoreCase(x_objRequest.FacultyName) == true);
            if (objFacultys != null)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.FACULTY_NAME_EXIST, ErrorCode.FACULTY_NAME_EXIST);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.FACULTY_NAME_EXIST}", SOURCE);

                return objResponse;
            }
            x_objRequest.FacultyName = x_objRequest.FacultyName.Trim();

            // Account
            objAccount = await GetAccount(x_strAccountId);
            if (objAccount == null)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.ACCOUNT_IS_LOCKER, ErrorCode.ACCOUNT_IS_LOCKER);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.ACCOUNT_IS_LOCKER}", SOURCE);

                return objResponse;
            }

            // Member
            objMemberInfo = MemberInfoManager.GetMemberById(objAccount.MemberId);
            if (objMemberInfo == null)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.MEMBER_INFO_IS_NULL, ErrorCode.MEMBER_INFO_IS_NULL);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.MEMBER_INFO_IS_NULL}", SOURCE);

                return objResponse;
            }

            // Role
            objRole = await RolesDbManager.GetRoleById(objAccount.RoleId);
            if ((objRole == null) || (objRole.Permissions.Contains((int)DTUSVCPermission.CREATE_FACULTY) == false))
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.AUTHORITY_INSUFFICIENT, ErrorCode.AUTHORITY_INSUFFICIENT);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMemberInfo.GetMemberFullName(), $"{SOURCE}: {ErrorConstant.MEMBER_INFO_IS_NULL}", SOURCE);
                return objResponse;
            }

            objFacultys = new Facultys
            {
                FacultyName = x_objRequest.FacultyName,
                Id = x_objRequest.Id
            };

            bResult = await FacultyDbManager.UpdateFaculty(objFacultys);
            if (bResult == false)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.UPDATE_FACULTY_FAILED, ErrorCode.UPDATE_FACULTY_FAILED);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMemberInfo.GetMemberFullName(), $"{SOURCE}: {ErrorConstant.UPDATE_FACULTY_FAILED}", SOURCE);
                return objResponse;
            }

            objTask = Task.Run(() =>
            {
                UpdateMemberData(objFacultys);
            });

            objResponse = ResponseManager.GetSuccsessResp(SuccsessConstant.UPDATE_FACULTY_SUCCSESS, objFacultys);
            return objResponse;
        }

        /// <summary>
        /// Update Member Data
        /// </summary>
        /// <param name="x_objFacultys"></param>
        private void UpdateMemberData(Facultys x_objFacultys)
        {
            MemberInfoManager.UpdateFaculty(x_objFacultys);
            MemberRegisterMemory.UpdateFaculty(x_objFacultys);
            MemberRegistEvtData.Instant.UpdateFaculty(x_objFacultys);
        }
    }
}
