﻿using DTUSVCv3_Library;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTUSVCv3_Services
{
    public interface IFacultyService
    {
        Task<ResponseModel> GetAllFacultys();
        Task<ResponseModel> CreateFaculty(CreateFacultyRequestModel x_objRequest, string x_strAccountid, HttpContext x_objContext);
        Task<ResponseModel> UpdateFaculty(UpdateFacultyRequestModel x_objRequest, string x_strAccountid, HttpContext x_objContext);
    }
}
