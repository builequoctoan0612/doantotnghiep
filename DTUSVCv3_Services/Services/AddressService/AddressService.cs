﻿using DocumentFormat.OpenXml.Bibliography;
using DTUSVCv3_EntityFramework;
using DTUSVCv3_Library;
using DTUSVCv3_MemoryData;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTUSVCv3_Services
{
    public class AddressService : BaseSerivce, IAddressService
    {
        private const string SOURCE = "AddressService";

        /// <summary>
        /// Get Districts By Province Id
        /// </summary>
        /// <param name="x_strProvinceId"></param>
        /// <returns></returns>
        public async Task<ResponseModel> GetDistrictsByProvinceId(string x_strProvinceId)
        {
            List<Districts> lstDistricts;
            ResponseModel objResponseModel;

            lstDistricts = await AddressDbManager.GetDistrictsByProvinceId(x_strProvinceId);
            objResponseModel = ResponseManager.GetSuccsessResp("", lstDistricts, lstDistricts.Count);
            return objResponseModel;
        }

        /// <summary>
        /// Get Provinces
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseModel> GetProvinces()
        {
            List<Provinces> lstProvinces;
            ResponseModel objResponseModel;

            lstProvinces = await AddressDbManager.GetAllProvinces();
            objResponseModel = ResponseManager.GetSuccsessResp("", lstProvinces, lstProvinces.Count);
            return objResponseModel;
        }

        /// <summary>
        /// Get Wards By District Id
        /// </summary>
        /// <param name="x_strDistrictId"></param>
        /// <returns></returns>
        public async Task<ResponseModel> GetWardsByDistrictId(string x_strDistrictId)
        {
            List<Wards> lstWards;
            ResponseModel objResponseModel;

            lstWards = await AddressDbManager.GetWardsByDistrictId(x_strDistrictId);
            objResponseModel = ResponseManager.GetSuccsessResp("", lstWards, lstWards.Count);
            return objResponseModel;
        }

        /// <summary>
        /// Update Hometown
        /// </summary>
        /// <param name="x_objRequest"></param>
        /// <param name="x_strAccountId"></param>
        /// <param name="x_objContext"></param>
        /// <returns></returns>
        public async Task<ResponseModel> UpdateHometown(UpdateHometownRequest x_objRequest, string x_strAccountId, HttpContext x_objContext)
        {
            Accounts objAccount;
            MemberDecryptModel objMember;
            AddressInfo objAddress;
            Provinces objProvince;
            Districts objDistrict;
            Wards objWard;
            ResponseModel objResponse;
            bool bResult;
            string strClientIp;

            //----------------------------------
            // Get client Ip
            //----------------------------------
            strClientIp = x_objContext.GetClientIPAddress();

            //----------------------------------
            // Get Account
            //----------------------------------
            objAccount = await GetAccount(x_strAccountId);
            if ((objAccount == null) || (objAccount.IsLocked == true))
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.ACCOUNT_IS_NULL, ErrorCode.ACCOUNT_IS_NULL);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.ACCOUNT_IS_NULL}", SOURCE);
                return objResponse;
            }

            //----------------------------------
            // Get Member info
            //----------------------------------
            objMember = MemberInfoManager.GetMemberById(objAccount.MemberId);
            if (objMember == null)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.MEMBER_INFO_IS_NULL, ErrorCode.MEMBER_INFO_IS_NULL);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.MEMBER_INFO_IS_NULL}", SOURCE);
                return objResponse;
            }

            //----------------------------------
            // Check address
            //----------------------------------
            objResponse = CheckAddress(x_objRequest.ProvinceId, x_objRequest.DistrictId, x_objRequest.WardId, x_objRequest.SpecificAddress,
                                        out objAddress, out objProvince, out objDistrict, out objWard);
            if (objResponse.IsSuccsess == false)
            {
                return objResponse;
            }

            objMember.Hometown.District = objDistrict;
            objMember.Hometown.Province = objProvince;
            objMember.Hometown.Ward = objWard;
            objMember.Hometown.SpecificAddress = objAddress.SpecificAddress;
            bResult = await MemberInfoManager.UpdateMemberInfo(objMember);
            if (bResult == false)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.UPDATE_MEMBER_INFO_FAILED, ErrorCode.UPDATE_MEMBER_INFO_FAILED);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.UPDATE_MEMBER_INFO_FAILED}", SOURCE);
                return objResponse;
            }

            objResponse = ResponseManager.GetSuccsessResp(SuccsessConstant.UPDATE_MEMBER_INFO_SUCCSESS);
            return objResponse;
        }
    }
}
