﻿using DTUSVCv3_Library;
using Microsoft.AspNetCore.Http;

namespace DTUSVCv3_Services
{
    public interface IAddressService
    {
        /// <summary>
        /// Get Provinces
        /// </summary>
        /// <returns></returns>
        Task<ResponseModel> GetProvinces();
        /// <summary>
        /// Get Districts By Province Id
        /// </summary>
        /// <param name="x_strProvinceId"></param>
        /// <returns></returns>
        Task<ResponseModel> GetDistrictsByProvinceId(string x_strProvinceId);
        /// <summary>
        /// Get Wards By District Id
        /// </summary>
        /// <param name="x_strDistrictId"></param>
        /// <returns></returns>
        Task<ResponseModel> GetWardsByDistrictId(string x_strDistrictId);
        /// <summary>
        /// Update Hometown
        /// </summary>
        /// <param name="x_objRequest"></param>
        /// <param name="x_strAccountId"></param>
        /// <param name="x_objContext"></param>
        /// <returns></returns>
        Task<ResponseModel> UpdateHometown(UpdateHometownRequest x_objRequest, string x_strAccountId, HttpContext x_objContext);
    }
}
