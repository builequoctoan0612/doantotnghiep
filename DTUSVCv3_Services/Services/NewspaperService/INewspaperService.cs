﻿using DTUSVCv3_Library;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTUSVCv3_Services
{
    public interface INewspaperService
    {
        /// <summary>
        /// Get Newspaper List
        /// </summary>
        /// <param name="x_nPageIndex"></param>
        /// <param name="x_nPageSize"></param>
        /// <param name="x_objContext"></param>
        /// <returns></returns>
        Task<ResponseModel> GetNewspaperList(int x_nPageIndex, int x_nPageSize, HttpContext x_objContext);
        /// <summary>
        /// Get Newspaper By Id
        /// </summary>
        /// <param name="x_strNewspaperId"></param>
        /// <param name="x_objContext"></param>
        /// <returns></returns>
        Task<ResponseModel> GetNewspaperById(string x_strNewspaperId, HttpContext x_objContext);
        /// <summary>
        /// Create Newspaper
        /// </summary>
        /// <param name="x_objRequest"></param>
        /// <param name="x_strAccountId"></param>
        /// <param name="x_objContext"></param>
        /// <returns></returns>
        Task<ResponseModel> CreateNewspaper(CreateNewspaperRequestModel x_objRequest, string x_strAccountId, HttpContext x_objContext);
        /// <summary>
        /// Update Newspaper
        /// </summary>
        /// <param name="x_objRequest"></param>
        /// <param name="x_strAccountId"></param>
        /// <param name="x_objContext"></param>
        /// <returns></returns>
        Task<ResponseModel> UpdateNewspaper(UpdateNewspaperRequestModel x_objRequest, string x_strAccountId, HttpContext x_objContext);
        /// <summary>
        /// Delete Newspaper
        /// </summary>
        /// <param name="x_strNewspaperId"></param>
        /// <param name="x_strAccountId"></param>
        /// <param name="x_objContext"></param>
        /// <returns></returns>
        Task<ResponseModel> DeleteNewspaper(string x_strNewspaperId, string x_strAccountId, HttpContext x_objContext);
        /// <summary>
        /// Upload Image
        /// </summary>
        /// <param name="x_objRequest"></param>
        /// <param name="x_strAccountId"></param>
        /// <param name="x_objContext"></param>
        /// <returns></returns>
        Task<ResponseModel> UploadImage(UploadImage x_objRequest, string x_strAccountId, HttpContext x_objContext);
    }
}
