﻿using DTUSVCv3_EntityFramework;
using DTUSVCv3_Library;
using DTUSVCv3_MemoryData;
using Microsoft.AspNetCore.Http;
using System.Drawing;

namespace DTUSVCv3_Services
{
    public class NewspaperService : BaseSerivce, INewspaperService
    {
        public NewspaperService()
            : base()
        {
        }

        #region MyRegion
        private const string SOURCE = "NewspaperService";
        #endregion

        #region Methods

        /// <summary>
        /// Update Model Validate
        /// </summary>
        /// <param name="x_objRequest"></param>
        /// <returns></returns>
        private ResponseModel UpdateModelValidate(UpdateNewspaperRequestModel x_objRequest)
        {
            ResponseModel objResponse;
            string strContent;
            string strBuf;

            if (x_objRequest == null)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.CREATE_NEWS_REQUEST_INVALID, ErrorCode.CREATE_NEWS_REQUEST_INVALID);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, string.Empty, string.Empty, ErrorConstant.CREATE_NEWS_REQUEST_INVALID, SOURCE);
                return objResponse;
            }

            if (x_objRequest.Id.MongoIdIsValid() == false)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.NEWSPAPER_IS_NULL, ErrorCode.NEWSPAPER_IS_NULL);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, string.Empty, string.Empty, ErrorConstant.NEWSPAPER_IS_NULL, SOURCE);
                return objResponse;
            }

            // Check title
            if (string.IsNullOrWhiteSpace(x_objRequest.Title) == true)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.NEWS_TITLE_INVALID, ErrorCode.NEWS_TITLE_INVALID);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, string.Empty, string.Empty, ErrorConstant.NEWS_TITLE_INVALID, SOURCE);
                return objResponse;
            }
            x_objRequest.Title = x_objRequest.Title.Trim();
            // Check content
            strContent = x_objRequest.Content.RemoveHTMLTag();
            if (string.IsNullOrWhiteSpace(strContent) == true)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.NEWS_CONTENT_INVALID, ErrorCode.NEWS_CONTENT_INVALID);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, string.Empty, string.Empty, ErrorConstant.NEWS_CONTENT_INVALID, SOURCE);
                return objResponse;
            }
            x_objRequest.Content = x_objRequest.Content.Trim();
            // Check Poster
            if (x_objRequest.Poster != null)
            {
                if (x_objRequest.Poster.FileIsValid(ContentType.IMAGE_CONTENT_TYPE, out strBuf) == false)
                {
                    if (strBuf.EqualsIgnoreCase(ErrorConstant.FILE_DOWNLOADED_IN_WRONG_FORMAT) == true)
                    {
                        strBuf = string.Format(ErrorConstant.FILE_DOWNLOADED_IN_WRONG_FORMAT, string.Join(" | ", ContentType.IMAGE_CONTENT_TYPE));
                    }
                    objResponse = ResponseManager.GetErrorResp(strBuf, ErrorCode.ACTIVITY_POSTER_INVALID);
                    return objResponse;
                }

                // Scan virus
                strBuf = x_objRequest.Poster.ScanVirus();
                if (strBuf.EqualsIgnoreCase(VirusScanner.EXIST_VIRUS) == true)
                {
                    objResponse = ResponseManager.GetErrorResp(strBuf, ErrorCode.ACTIVITY_POSTER_INVALID);
                    return objResponse;
                }
            }

            return new ResponseModel();
        }

        /// <summary>
        /// Create Model Validate
        /// </summary>
        /// <param name="x_objRequest"></param>
        /// <returns></returns>
        private ResponseModel CreateModelValidate(CreateNewspaperRequestModel x_objRequest)
        {
            ResponseModel objResponse;
            string strContent;
            string strBuf;

            if (x_objRequest == null)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.CREATE_NEWS_REQUEST_INVALID, ErrorCode.CREATE_NEWS_REQUEST_INVALID);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, string.Empty, string.Empty, ErrorConstant.CREATE_NEWS_REQUEST_INVALID, SOURCE);
                return objResponse;
            }
            // Check title
            if (string.IsNullOrWhiteSpace(x_objRequest.Title) == true)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.NEWS_TITLE_INVALID, ErrorCode.NEWS_TITLE_INVALID);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, string.Empty, string.Empty, ErrorConstant.NEWS_TITLE_INVALID, SOURCE);
                return objResponse;
            }
            x_objRequest.Title = x_objRequest.Title.Trim();
            // Check content
            strContent = x_objRequest.Content.RemoveHTMLTag();
            if (string.IsNullOrWhiteSpace(strContent) == true)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.NEWS_CONTENT_INVALID, ErrorCode.NEWS_CONTENT_INVALID);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, string.Empty, string.Empty, ErrorConstant.NEWS_CONTENT_INVALID, SOURCE);
                return objResponse;
            }
            x_objRequest.Content = x_objRequest.Content.Trim();
            // Check Poster
            if (x_objRequest.Poster == null)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.NEWS_POSTER_INVALID, ErrorCode.NEWS_POSTER_INVALID);
                return objResponse;
            }

            if (x_objRequest.Poster.FileIsValid(ContentType.IMAGE_CONTENT_TYPE, out strBuf) == false)
            {
                if (strBuf.EqualsIgnoreCase(ErrorConstant.FILE_DOWNLOADED_IN_WRONG_FORMAT) == true)
                {
                    strBuf = string.Format(ErrorConstant.FILE_DOWNLOADED_IN_WRONG_FORMAT, string.Join(" | ", ContentType.IMAGE_CONTENT_TYPE));
                }
                objResponse = ResponseManager.GetErrorResp(strBuf, ErrorCode.ACTIVITY_POSTER_INVALID);
                return objResponse;
            }

            // Scan virus
            strBuf = x_objRequest.Poster.ScanVirus();
            if (strBuf.EqualsIgnoreCase(VirusScanner.EXIST_VIRUS) == true)
            {
                objResponse = ResponseManager.GetErrorResp(strBuf, ErrorCode.ACTIVITY_POSTER_INVALID);
                return objResponse;
            }

            return new ResponseModel();
        }

        /// <summary>
        /// Convert New Db To Model
        /// </summary>
        /// <param name="x_objNewspaper"></param>
        /// <returns></returns>
        private NewspaperResponseModel ConvertNewDbToModel(Newspapers x_objNewspaper)
        {
            NewspaperResponseModel objNewspaper;
            MemberDecryptModel objCreater;

            if (x_objNewspaper == null)
            {
                return null;
            }

            objCreater = MemberInfoManager.GetMemberById(x_objNewspaper.CreaterId);
            objNewspaper = new NewspaperResponseModel();
            if (objCreater == null)
            {
                objNewspaper.CreaterName = "Không xác định";
            }

            objNewspaper.PosterPath = Path.Combine(SysFolder.NEWSPAPER_IMAGE, x_objNewspaper.PosterPath);
            objNewspaper.Content = x_objNewspaper.Content;
            objNewspaper.Title = x_objNewspaper.Title;
            objNewspaper.CreateDate = x_objNewspaper.CreateDate;
            objNewspaper.CreaterName = objCreater.GetMemberFullName();
            objNewspaper.Id = x_objNewspaper.Id;
            return objNewspaper;
        }

        /// <summary>
        /// Create Newspaper
        /// </summary>
        /// <param name="x_objRequest"></param>
        /// <param name="x_strAccountId"></param>
        /// <param name="x_objContext"></param>
        /// <returns></returns>
        public async Task<ResponseModel> CreateNewspaper(CreateNewspaperRequestModel x_objRequest, string x_strAccountId, HttpContext x_objContext)
        {
            Accounts objAccount;
            MemberDecryptModel objMemberInfo;
            Roles objRole;
            Newspapers objNewspaper;
            ResponseModel objResponse;
            Bitmap objPoster; 
            NewspaperResponseModel objNewspaperRes;

            string strClientIp;
            string strFileName;
            string strFilePath;
            string strExtension;
            string strBuf;
            bool bResult;

            // --------------------------------------------
            // Get client ip
            // --------------------------------------------
            strClientIp = x_objContext.GetClientIPAddress();
            // --------------------------------------------
            // Validate request
            // --------------------------------------------
            objResponse = CreateModelValidate(x_objRequest);
            if (objResponse.IsSuccsess == false)
            {
                return objResponse;
            }
            // --------------------------------------------
            // Validate role
            // --------------------------------------------
            // Account
            objAccount = await GetAccount(x_strAccountId);
            if (objAccount == null || objAccount.IsLocked == true)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.ACCOUNT_IS_NULL, ErrorCode.ACCOUNT_IS_NULL);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, string.Empty, string.Empty, ErrorConstant.ACCOUNT_IS_NULL, SOURCE);
                return objResponse;
            }
            // Member
            objMemberInfo = MemberInfoManager.GetMemberById(objAccount.MemberId);
            if (objMemberInfo == null || objMemberInfo.IsMember == false)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.MEMBER_INFO_IS_NULL, ErrorCode.MEMBER_INFO_IS_NULL);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, string.Empty, string.Empty, ErrorConstant.MEMBER_INFO_IS_NULL, SOURCE);
                return objResponse;
            }
            // Role
            objRole = await RolesDbManager.GetRoleById(objAccount.RoleId);
            if (objRole == null || objRole.Permissions == null || objRole.Permissions.Contains((int)DTUSVCPermission.CREATE_NEWSPAPER) == false)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.AUTHORITY_INSUFFICIENT, ErrorCode.AUTHORITY_INSUFFICIENT);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMemberInfo.GetMemberFullName(), $"{SOURCE}: {ErrorConstant.AUTHORITY_INSUFFICIENT}", SOURCE);

                return objResponse;
            }
            // Save poster
            strExtension = Path.GetExtension(x_objRequest.Poster.FileName);
            strFileName = FileHelper.GetRandomFileName(strExtension);
            strFilePath = FileHelper.GetFilePath(SysFolder.NEWSPAPER_IMAGE, strFileName);
            strBuf = await x_objRequest.Poster.SaveFormFile(strFilePath);
            if (string.IsNullOrEmpty(strBuf) == false)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(strBuf, ErrorCode.UPLOAD_FILE_IS_NULL);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMemberInfo.GetMemberFullName(), $"{SOURCE}: {strBuf}", SOURCE);

                return objResponse;
            }
            // Crop image
            objPoster = FileHelper.OpenImageFile(strFilePath);
            objPoster = objPoster.CropImage(ImageHelper.POSTER_IMAGE, out strBuf);
            if (string.IsNullOrEmpty(strBuf) == true)
            {
                objPoster.SaveBitmapStream(strFilePath);
            }

            objNewspaper = new Newspapers
            {
                Content = x_objRequest.Content,
                CreaterId = objMemberInfo.Id,
                PosterPath = strFileName,
                Title = x_objRequest.Title
            };
            bResult = await NewsDbManager.UpdateNewspaper(objNewspaper);
            if (bResult == false)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.UPDATE_NEWSPAPER_FAILED, ErrorCode.UPDATE_NEWSPAPER_FAILED);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, string.Empty, string.Empty, ErrorConstant.UPDATE_NEWSPAPER_FAILED, SOURCE);
                return objResponse;
            }
            objNewspaperRes = ConvertNewDbToModel(objNewspaper);
            objResponse = ResponseManager.GetSuccsessResp(SuccsessConstant.UPDATE_NEWSPAPER_SUCCSESS, objNewspaperRes);
            return objResponse;
        }

        /// <summary>
        /// Delete Newspaper
        /// </summary>
        /// <param name="x_strNewspaperId"></param>
        /// <param name="x_strAccountId"></param>
        /// <param name="x_objContext"></param>
        public async Task<ResponseModel> DeleteNewspaper(string x_strNewspaperId, string x_strAccountId, HttpContext x_objContext)
        {
            Accounts objAccount;
            MemberDecryptModel objMemberInfo;
            Roles objRole;
            Newspapers objNewspaper;
            ResponseModel objResponse;

            string strClientIp;
            bool bResult;

            // --------------------------------------------
            // Get client ip
            // --------------------------------------------
            strClientIp = x_objContext.GetClientIPAddress();
            // --------------------------------------------
            // Validate role
            // --------------------------------------------
            // Account
            objAccount = await GetAccount(x_strAccountId);
            if (objAccount == null || objAccount.IsLocked == true)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.ACCOUNT_IS_NULL, ErrorCode.ACCOUNT_IS_NULL);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, string.Empty, string.Empty, ErrorConstant.ACCOUNT_IS_NULL, SOURCE);
                return objResponse;
            }
            // Member
            objMemberInfo = MemberInfoManager.GetMemberById(objAccount.MemberId);
            if (objMemberInfo == null || objMemberInfo.IsMember == false)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.MEMBER_INFO_IS_NULL, ErrorCode.MEMBER_INFO_IS_NULL);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, string.Empty, string.Empty, ErrorConstant.MEMBER_INFO_IS_NULL, SOURCE);
                return objResponse;
            }
            // Role
            objRole = await RolesDbManager.GetRoleById(objAccount.RoleId);
            if (objRole == null || objRole.Permissions == null || objRole.Permissions.Contains((int)DTUSVCPermission.DELETE_NEWSPAPER) == false)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.AUTHORITY_INSUFFICIENT, ErrorCode.AUTHORITY_INSUFFICIENT);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMemberInfo.GetMemberFullName(), $"{SOURCE}: {ErrorConstant.AUTHORITY_INSUFFICIENT}", SOURCE);

                return objResponse;
            }

            objNewspaper = await NewsDbManager.GetNewspaperById(x_strNewspaperId);
            if (objNewspaper == null)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.NEWSPAPER_IS_NULL, ErrorCode.NEWSPAPER_IS_NULL);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, string.Empty, string.Empty, ErrorConstant.NEWSPAPER_IS_NULL, SOURCE);
                return objResponse;
            }
            // Delete
            bResult = await NewsDbManager.DeleteNewspaper(x_strNewspaperId);
            if (bResult == false)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.DELETE_NEWS_FAILED, ErrorCode.DELETE_NEWS_FAILED);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, string.Empty, string.Empty, ErrorConstant.DELETE_NEWS_FAILED, SOURCE);
                return objResponse;
            }
            objResponse = ResponseManager.GetSuccsessResp(SuccsessConstant.DELETE_NEWSPAPER_SUCCSESS);
            return objResponse;
        }

        /// <summary>
        /// Get Newspaper By Id
        /// </summary>
        /// <param name="x_strNewspaperId"></param>
        /// <param name="x_objContext"></param>
        /// <returns></returns>
        public async Task<ResponseModel> GetNewspaperById(string x_strNewspaperId, HttpContext x_objContext)
        {
            Newspapers objNewspapers;
            NewspaperResponseModel objNewspaperResponseModel;
            ResponseModel objResponse;

            objNewspapers = await NewsDbManager.GetNewspaperById(x_strNewspaperId);
            if (objNewspapers == null)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.NEWSPAPER_IS_NULL, ErrorCode.NEWSPAPER_IS_NULL);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, string.Empty, string.Empty, ErrorConstant.NEWSPAPER_IS_NULL, SOURCE);
                return objResponse;
            }

            objNewspaperResponseModel = ConvertNewDbToModel(objNewspapers);
            objResponse = ResponseManager.GetSuccsessResp(string.Empty, objNewspaperResponseModel);
            return objResponse;
        }

        /// <summary>
        /// Get Newspaper List
        /// </summary>
        /// <param name="x_nPageIndex"></param>
        /// <param name="x_nPageSize"></param>
        /// <param name="x_objContext"></param>
        /// <returns></returns>
        public async Task<ResponseModel> GetNewspaperList(int x_nPageIndex, int x_nPageSize, HttpContext x_objContext)
        {
            List<Newspapers> lstNewsDB;
            NewspaperResponseModel objNewspaperResponseModel;
            List<NewspaperResponseModel> lstNewsResponse;
            ResponseModel objResponse;
            int nTotalItem;

            lstNewsDB = await NewsDbManager.GetAllNewspaper();
            if (lstNewsDB == null || lstNewsDB.Count == 0)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.NEWSPAPER_IS_NULL, ErrorCode.NEWSPAPER_IS_NULL);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, string.Empty, string.Empty, ErrorConstant.NEWSPAPER_IS_NULL, SOURCE);
                return objResponse;
            }
            nTotalItem = lstNewsDB.Count;
            lstNewsDB = lstNewsDB.OrderByDescending(obj => obj.CreateDate).ToList();
            if (x_nPageSize < 0)
            {
                x_nPageSize = 0;
            }
            if (x_nPageIndex < 0)
            {
                x_nPageIndex = 0;
            }
            lstNewsDB = lstNewsDB.Skip(x_nPageIndex * x_nPageSize).Take(x_nPageSize).ToList();
            if (lstNewsDB == null || lstNewsDB.Count == 0)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.NEWSPAPER_IS_NULL, ErrorCode.NEWSPAPER_IS_NULL);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, string.Empty, string.Empty, ErrorConstant.NEWSPAPER_IS_NULL, SOURCE);
                return objResponse;
            }
            lstNewsResponse = new List<NewspaperResponseModel>();
            foreach (Newspapers objNews in lstNewsDB)
            {
                objNewspaperResponseModel = ConvertNewDbToModel(objNews);
                if (objNewspaperResponseModel == null)
                {
                    continue;
                }
                lstNewsResponse.Add(objNewspaperResponseModel);
            }
            objResponse = ResponseManager.GetSuccsessResp(string.Empty, lstNewsResponse, nTotalItem);
            return objResponse;
        }

        /// <summary>
        /// Update Newspaper
        /// </summary>
        /// <param name="x_objRequest"></param>
        /// <param name="x_strAccountId"></param>
        /// <param name="x_objContext"></param>
        /// <returns></returns>
        public async Task<ResponseModel> UpdateNewspaper(UpdateNewspaperRequestModel x_objRequest, string x_strAccountId, HttpContext x_objContext)
        {
            Accounts objAccount;
            MemberDecryptModel objMemberInfo;
            Roles objRole;
            Newspapers objNewspaper;
            ResponseModel objResponse;
            Bitmap objPoster;
            NewspaperResponseModel objNewspaperRes;

            string strClientIp;
            string strFileName;
            string strFilePath;
            string strExtension;
            string strBuf;
            bool bResult;

            // --------------------------------------------
            // Get client ip
            // --------------------------------------------
            strClientIp = x_objContext.GetClientIPAddress();
            // --------------------------------------------
            // Validate request
            // --------------------------------------------
            objResponse = UpdateModelValidate(x_objRequest);
            if (objResponse.IsSuccsess == false)
            {
                return objResponse;
            }
            // --------------------------------------------
            // Validate role
            // --------------------------------------------
            // Account
            objAccount = await GetAccount(x_strAccountId);
            if (objAccount == null || objAccount.IsLocked == true)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.ACCOUNT_IS_NULL, ErrorCode.ACCOUNT_IS_NULL);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, ErrorConstant.ACCOUNT_IS_NULL, SOURCE);
                return objResponse;
            }
            // Member
            objMemberInfo = MemberInfoManager.GetMemberById(objAccount.MemberId);
            if (objMemberInfo == null || objMemberInfo.IsMember == false)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.MEMBER_INFO_IS_NULL, ErrorCode.MEMBER_INFO_IS_NULL);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, ErrorConstant.MEMBER_INFO_IS_NULL, SOURCE);
                return objResponse;
            }
            // Role
            objRole = await RolesDbManager.GetRoleById(objAccount.RoleId);
            if (objRole == null || objRole.Permissions == null || objRole.Permissions.Contains((int)DTUSVCPermission.UPDATE_NEWSPAPER) == false)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.AUTHORITY_INSUFFICIENT, ErrorCode.AUTHORITY_INSUFFICIENT);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMemberInfo.GetMemberFullName(), $"{SOURCE}: {ErrorConstant.AUTHORITY_INSUFFICIENT}", SOURCE);

                return objResponse;
            }

            objNewspaper = await NewsDbManager.GetNewspaperById(x_objRequest.Id);
            if (objNewspaper == null)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.NEWSPAPER_IS_NULL, ErrorCode.NEWSPAPER_IS_NULL);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMemberInfo.GetMemberFullName(), ErrorConstant.NEWSPAPER_IS_NULL, SOURCE);
                return objResponse;
            }
            if (x_objRequest.Poster != null)
            {
                // Save poster
                strExtension = Path.GetExtension(x_objRequest.Poster.FileName);
                strFileName = FileHelper.GetRandomFileName(strExtension);
                strFilePath = FileHelper.GetFilePath(SysFolder.NEWSPAPER_IMAGE, strFileName);
                strBuf = await x_objRequest.Poster.SaveFormFile(strFilePath);
                if (string.IsNullOrEmpty(strBuf) == false)
                {
                    // Get error
                    objResponse = ResponseManager.GetErrorResp(strBuf, ErrorCode.UPLOAD_FILE_IS_NULL);
                    // Write log
                    LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMemberInfo.GetMemberFullName(), $"{SOURCE}: {strBuf}", SOURCE);

                    return objResponse;
                }
                // Crop image
                objPoster = FileHelper.OpenImageFile(strFilePath);
                objPoster = objPoster.CropImage(ImageHelper.POSTER_IMAGE, out strBuf);
                if (string.IsNullOrEmpty(strBuf) == true)
                {
                    objPoster.SaveBitmapStream(strFilePath);
                }
                objNewspaper.PosterPath = strFileName;
            }

            objNewspaper.Content = x_objRequest.Content;
            objNewspaper.CreaterId = objMemberInfo.Id;
            objNewspaper.Title = x_objRequest.Title;
            bResult = await NewsDbManager.UpdateNewspaper(objNewspaper);
            if (bResult == false)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.UPDATE_NEWSPAPER_FAILED, ErrorCode.UPDATE_NEWSPAPER_FAILED);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMemberInfo.GetMemberFullName(), ErrorConstant.UPDATE_NEWSPAPER_FAILED, SOURCE);
                return objResponse;
            }
            objNewspaperRes = ConvertNewDbToModel(objNewspaper);
            objResponse = ResponseManager.GetSuccsessResp(SuccsessConstant.UPDATE_NEWSPAPER_SUCCSESS, objNewspaperRes);
            return objResponse;
        }

        /// <summary>
        /// Upload Image
        /// </summary>
        /// <param name="x_objRequest"></param>
        /// <param name="x_strAccountId"></param>
        /// <param name="x_objContext"></param>
        /// <returns></returns>
        public async Task<ResponseModel> UploadImage(UploadImage x_objRequest, string x_strAccountId, HttpContext x_objContext)
        {
            Accounts objAccount;
            MemberDecryptModel objMemberInfo;
            Roles objRole;
            ResponseModel objResponse;
            Bitmap objPoster;

            string strClientIp;
            string strFileName;
            string strFilePath;
            string strExtension;
            string strBuf;

            // --------------------------------------------
            // Get client ip
            // --------------------------------------------
            strClientIp = x_objContext.GetClientIPAddress();
            // --------------------------------------------
            // Validate request
            // --------------------------------------------
            // Check Poster
            if (x_objRequest.Poster != null)
            {
                if (x_objRequest.Poster.FileIsValid(ContentType.IMAGE_CONTENT_TYPE, out strBuf) == false)
                {
                    if (strBuf.EqualsIgnoreCase(ErrorConstant.FILE_DOWNLOADED_IN_WRONG_FORMAT) == true)
                    {
                        strBuf = string.Format(ErrorConstant.FILE_DOWNLOADED_IN_WRONG_FORMAT, string.Join(" | ", ContentType.IMAGE_CONTENT_TYPE));
                    }
                    objResponse = ResponseManager.GetErrorResp(strBuf, ErrorCode.ACTIVITY_POSTER_INVALID);
                    return objResponse;
                }

                // Scan virus
                strBuf = x_objRequest.Poster.ScanVirus();
                if (strBuf.EqualsIgnoreCase(VirusScanner.EXIST_VIRUS) == true)
                {
                    objResponse = ResponseManager.GetErrorResp(strBuf, ErrorCode.ACTIVITY_POSTER_INVALID);
                    return objResponse;
                }
            }
            else
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.UPLOAD_INVALID, ErrorCode.UPLOAD_INVALID);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, ErrorConstant.UPLOAD_INVALID, SOURCE);
                return objResponse;
            }
            // --------------------------------------------
            // Validate role
            // --------------------------------------------
            // Account
            objAccount = await GetAccount(x_strAccountId);
            if (objAccount == null || objAccount.IsLocked == true)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.ACCOUNT_IS_NULL, ErrorCode.ACCOUNT_IS_NULL);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, ErrorConstant.ACCOUNT_IS_NULL, SOURCE);
                return objResponse;
            }
            // Member
            objMemberInfo = MemberInfoManager.GetMemberById(objAccount.MemberId);
            if (objMemberInfo == null || objMemberInfo.IsMember == false)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.MEMBER_INFO_IS_NULL, ErrorCode.MEMBER_INFO_IS_NULL);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, ErrorConstant.MEMBER_INFO_IS_NULL, SOURCE);
                return objResponse;
            }
            // Role
            objRole = await RolesDbManager.GetRoleById(objAccount.RoleId);
            if (objRole == null || objRole.Permissions == null || objRole.Permissions.Contains((int)DTUSVCPermission.UPLOAD_IMAGE) == false)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.AUTHORITY_INSUFFICIENT, ErrorCode.AUTHORITY_INSUFFICIENT);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMemberInfo.GetMemberFullName(), $"{SOURCE}: {ErrorConstant.AUTHORITY_INSUFFICIENT}", SOURCE);

                return objResponse;
            }
            // Save poster
            strExtension = Path.GetExtension(x_objRequest.Poster.FileName);
            strFileName = FileHelper.GetRandomFileName(strExtension);
            strFilePath = FileHelper.GetFilePath(SysFolder.NEWSPAPER_IMAGE, strFileName);
            strBuf = await x_objRequest.Poster.SaveFormFile(strFilePath);
            if (string.IsNullOrEmpty(strBuf) == false)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(strBuf, ErrorCode.UPLOAD_FILE_IS_NULL);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMemberInfo.GetMemberFullName(), $"{SOURCE}: {strBuf}", SOURCE);

                return objResponse;
            }
            // Crop image
            objPoster = FileHelper.OpenImageFile(strFilePath);
            objPoster = objPoster.CropImage(ImageHelper.NEWS_IMAGE, out strBuf);
            if (string.IsNullOrEmpty(strBuf) == true)
            {
                objPoster.SaveBitmapStream(strFilePath);
            }

            strFilePath = Path.Combine(SysFolder.NEWSPAPER_IMAGE, strFileName);
            objResponse = ResponseManager.GetSuccsessResp(string.Empty, strFilePath);
            return objResponse;
        }
        #endregion
    }
}
