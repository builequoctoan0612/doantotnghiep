﻿using DTUSVCv3_Library;

namespace DTUSVCv3_Services
{
    public interface IShortLinkService
    {
        Task<ResponseModel> GetShortLink(string x_strKeyCode);
        Task<ResponseModel> CreateShortLink(ShortLinkRequestModel x_objRequest);
    }
}
