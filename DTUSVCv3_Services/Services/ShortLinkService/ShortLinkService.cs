﻿using DTUSVCv3_EntityFramework;
using DTUSVCv3_Library;
using MongoDB.Driver;

namespace DTUSVCv3_Services
{
    public class ShortLinkService : BaseSerivce, IShortLinkService
    {
        public ShortLinkService()
            : base()
        {
            m_objShortLinkDbManager = new ShortLinkDbManager();
        }

        private readonly ShortLinkDbManager m_objShortLinkDbManager;
        private const string STR_CHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

        /// <summary>
        /// Create Key Code
        /// </summary>
        /// <returns></returns>
        private async Task<string> CreateKeyCode()
        {
            string strKeyCode;
            Random objRandom;
            ShortLinkInfo objShortLinkInfo;

            objRandom = new Random();
            do
            {
                // MAX Key code = 62^5 = 916,132,832
                strKeyCode = new string(Enumerable.Repeat(STR_CHARS, 5).Select(str => str[objRandom.Next(str.Length)]).ToArray());
                objShortLinkInfo = await m_objShortLinkDbManager.GetShortLinkByKeyCode(strKeyCode);
            } while (objShortLinkInfo != null);
            return strKeyCode;
        }

        /// <summary>
        /// Create Short Link
        /// </summary>
        /// <param name="x_objRequest"></param>
        /// <returns></returns>
        public async Task<ResponseModel> CreateShortLink(ShortLinkRequestModel x_objRequest)
        {
            ShortLinkResponseModel objShortLinkResponseModel;
            ShortLinkInfo objShortLinkInfo;
            ResponseModel objResponse;
            bool bResult;
            int nIndex;
            string strBuf;
            List<string> lstValue;

            try
            {
                if (x_objRequest == null)
                {
                    // Error
                    objResponse = ResponseManager.GetErrorResp(ErrorConstant.URL_IS_NULL, ErrorCode.URL_IS_NULL);
                    return objResponse;
                }

                if (x_objRequest.URL.IsValidUrl() == false)
                {
                    // Error
                    objResponse = ResponseManager.GetErrorResp(ErrorConstant.URL_IS_NULL, ErrorCode.URL_IS_NULL);
                    return objResponse;
                }

                if (x_objRequest.URL.ContainsIgnoreCase("dtusvc.com") == true)
                {
                    lstValue = x_objRequest.URL.Split('/').ToList();
                    nIndex = lstValue.FindIndex(obj => obj.ContainsIgnoreCase("dtusvc.com") == true);
                    if (lstValue.Count > nIndex + 1)
                    {
                        strBuf = lstValue[nIndex + 1].Replace("/", "").Trim();
                        objShortLinkInfo = await m_objShortLinkDbManager.GetShortLinkByURL(strBuf);
                        if (objShortLinkInfo != null)
                        {
                            // Error
                            objResponse = ResponseManager.GetErrorResp(ErrorConstant.URL_IS_NULL, ErrorCode.URL_IS_NULL);
                            return objResponse;
                        }
                    }
                }

                objShortLinkInfo = await m_objShortLinkDbManager.GetShortLinkByURL(x_objRequest.URL.Trim());
                if (objShortLinkInfo != null)
                {
                    objShortLinkResponseModel = new ShortLinkResponseModel
                    {
                        KeyCode = objShortLinkInfo.KeyCode,
                        URL = objShortLinkInfo.URL
                    };
                    objResponse = ResponseManager.GetSuccsessResp(string.Empty, objShortLinkResponseModel);
                    return objResponse;
                }

                objShortLinkInfo = new ShortLinkInfo();
                objShortLinkInfo.KeyCode = await CreateKeyCode();
                objShortLinkInfo.URL = x_objRequest.URL.Trim();
                bResult = await m_objShortLinkDbManager.CreateShortLink(objShortLinkInfo);
                if (bResult == false)
                {
                    // Error
                    objResponse = ResponseManager.GetErrorResp(ErrorConstant.CREATE_SHORT_LINK_FAILED, ErrorCode.CREATE_SHORT_LINK_FAILED);
                    return objResponse;
                }

                objShortLinkResponseModel = new ShortLinkResponseModel
                {
                    KeyCode = objShortLinkInfo.KeyCode,
                    URL = objShortLinkInfo.URL
                };
                objResponse = ResponseManager.GetSuccsessResp(string.Empty, objShortLinkResponseModel);
                return objResponse;
            }
            catch
            {
                // Error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.CREATE_SHORT_LINK_FAILED, ErrorCode.CREATE_SHORT_LINK_FAILED);
                return objResponse;
            }
        }

        /// <summary>
        /// Get Short Link
        /// </summary>
        /// <param name="x_strKeyCode"></param>
        /// <returns></returns>
        public async Task<ResponseModel> GetShortLink(string x_strKeyCode)
        {
            ShortLinkResponseModel objShortLinkResponseModel;
            ShortLinkInfo objShortLinkInfo;
            ResponseModel objResponse;

            objShortLinkInfo = await m_objShortLinkDbManager.GetShortLinkByKeyCode(x_strKeyCode);

            if (objShortLinkInfo == null)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.SHORT_LINK_IS_NULL, ErrorCode.SHORT_LINK_IS_NULL);
            }
            else
            {
                objShortLinkResponseModel = new ShortLinkResponseModel
                {
                    KeyCode = objShortLinkInfo.KeyCode,
                    URL = objShortLinkInfo.URL
                };
                objResponse = ResponseManager.GetSuccsessResp(string.Empty, objShortLinkResponseModel);
            }

            return objResponse;
        }
    }
}
