﻿using DTUSVCv3_Library;
using Microsoft.AspNetCore.Http;

namespace DTUSVCv3_Services
{
    public interface IMemberService
    {
        /// <summary>
        /// Get My Info
        /// </summary>
        /// <param name="x_strAccountId"></param>
        /// <param name="x_objContext"></param>
        /// <returns></returns>
        Task<ResponseModel> GetMyInfo(string x_strAccountId, HttpContext x_objContext);
        /// <summary>
        /// Update My Info
        /// </summary>
        /// <param name="x_objRequestModel"></param>
        /// <param name="x_strAccountId"></param>
        /// <param name="x_objContext"></param>
        /// <returns></returns>
        Task<ResponseModel> UpdateMyInfo(ChangeMyInfoReqestModel x_objRequestModel, string x_strAccountId, HttpContext x_objContext);
        /// <summary>
        /// Get Member Rank
        /// </summary>
        /// <param name="x_strAccountId"></param>
        /// <param name="x_objContext"></param>
        /// <returns></returns>
        Task<ResponseModel> GetMemberRank(string x_strAccountId, HttpContext x_objContext);
        /// <summary>
        /// Get Member Info
        /// </summary>
        /// <param name="x_strMemberId"></param>
        /// <param name="x_strAccountId"></param>
        /// <param name="x_objContext"></param>
        /// <returns></returns>
        Task<ResponseModel> GetMemberInfo(string x_strMemberId, string x_strAccountId, HttpContext x_objContext);
        /// <summary>
        /// Delete Member
        /// </summary>
        /// <param name="x_strMemberId"></param>
        /// <param name="x_strAccountId"></param>
        /// <param name="x_objContext"></param>
        /// <returns></returns>
        Task<ResponseModel> DeleteMember(string x_strMemberId, string x_strAccountId, HttpContext x_objContext);
        /// <summary>
        /// Get Member List
        /// </summary>
        /// <param name="x_nMemberType"></param>
        /// <param name="x_strAccountId"></param>
        /// <param name="x_objContext"></param>
        /// <returns></returns>
        Task<ResponseModel> GetMemberList(int x_nMemberType, string x_strAccountId, HttpContext x_objContext);
        /// <summary>
        /// Download Member Card
        /// </summary>
        /// <param name="x_objRequestModel"></param>
        /// <param name="x_strAccountId"></param>
        /// <param name="x_objContext"></param>
        /// <returns></returns>
        Task<ResponseDownloadFile> DownloadMemberCard(DownloadMemberCardRequestModel x_objRequestModel, string x_strAccountId, HttpContext x_objContext);
        /// <summary>
        /// Download Member List
        /// </summary>
        /// <param name="x_nMemberType"></param>
        /// <param name="x_strAccountId"></param>
        /// <param name="x_objContext"></param>
        /// <returns></returns>
        Task<ResponseDownloadFile> DownloadMemberList(int x_nMemberType, string x_strAccountId, HttpContext x_objContext);
        /// <summary>
        /// Change Avatar
        /// </summary>
        /// <param name="x_objRequestModel"></param>
        /// <param name="x_strAccountId"></param>
        /// <param name="x_objContext"></param>
        /// <returns></returns>
        Task<ResponseModel> UpdateAvatar(ChangeAvatarRequestModel x_objRequestModel, string x_strAccountId, HttpContext x_objContext);
        /// <summary>
        /// Search Member Info
        /// </summary>
        /// <param name="x_strMemberId"></param>
        /// <param name="x_strAccountId"></param>
        /// <param name="x_objContext"></param>
        /// <returns></returns>
        Task<ResponseModel> SearchMemberInfo(string x_strMemberId, string x_strAccountId, HttpContext x_objContext);
    }
}
