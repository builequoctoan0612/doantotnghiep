﻿using DTUSVCv3_EntityFramework;
using DTUSVCv3_Library;
using DTUSVCv3_MemoryData;
using Microsoft.AspNetCore.Http;
using System.Drawing;

namespace DTUSVCv3_Services
{
    public class MemberService : BaseSerivce, IMemberService
    {
        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        public MemberService()
            : base()
        {
        }
        #endregion

        #region Types
        private const string SOURCE = "Member Service";
        #endregion

        #region Methods
        /// <summary>
        /// Get Member Info
        /// </summary>
        /// <param name="x_strMemberId"></param>
        /// <param name="x_objAccountId"></param>
        /// <param name="x_strToken"></param>
        /// <param name="x_strIpAddress"></param>
        /// <returns></returns>
        private async Task<ResponseModel> GetMemberInfo(string x_strMemberId, Accounts x_objAccountId, string x_strRoleName,
                                                        string x_strIpAddress, bool x_bCheckIsMember = true)
        {
            MemberInfoResponseModel objMemberInfo;
            MemberDecryptModel objMemberDecrypt;
            Accounts objAccount;
            ResponseModel objResponseModel;
            string strClientIp;
            int nMemberType;
            double dScore;

            strClientIp = x_strIpAddress;
            //------------------------------------
            // Get account
            //------------------------------------
            objAccount = x_objAccountId;
            if (objAccount == null)
            {
                // Get error
                objResponseModel = ResponseManager.GetErrorResp(ErrorConstant.MEMBER_INFO_IS_NULL, ErrorCode.MEMBER_INFO_IS_NULL);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.MEMBER_INFO_IS_NULL}", SOURCE);

                return objResponseModel;
            }

            //------------------------------------
            // Get member info
            //------------------------------------
            objMemberDecrypt = MemberInfoManager.GetMemberById(x_strMemberId);
            if (objMemberDecrypt == null)
            {
                // Get error
                objResponseModel = ResponseManager.GetErrorResp(ErrorConstant.MEMBER_INFO_IS_NULL, ErrorCode.MEMBER_INFO_IS_NULL);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.MEMBER_INFO_IS_NULL}", SOURCE);

                return objResponseModel;
            }
            if ((x_bCheckIsMember == true) && (objMemberDecrypt.IsMember == false))
            {
                // Get error
                objResponseModel = ResponseManager.GetErrorResp(ErrorConstant.MEMBER_INFO_IS_NULL, ErrorCode.MEMBER_INFO_IS_NULL);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.MEMBER_INFO_IS_NULL}", SOURCE);

                return objResponseModel;
            }
            //------------------------------------
            // Get Score
            //------------------------------------
            dScore = await GetMemberScore(objMemberDecrypt.Id);
            //------------------------------------
            // Get member Type
            //------------------------------------
            if (objMemberDecrypt.IsMember == false)
            {
                nMemberType = 1;
            }
            else if (objAccount.IsLocked == true)
            {
                nMemberType = 2;
            }
            else if (objAccount.IsDefaultPassword)
            {
                nMemberType = 3;
            }
            else
            {
                nMemberType = 0;
            }
            //------------------------------------
            // Create member info
            //------------------------------------
            objMemberInfo = new MemberInfoResponseModel();
            objMemberInfo.AvatarPath = objMemberDecrypt.AvatarPath;
            objMemberInfo.BirthDay = objMemberDecrypt.BirthDay;
            objMemberInfo.ClassName = objMemberDecrypt.ClassName;
            objMemberInfo.Email = objMemberDecrypt.Email;
            objMemberInfo.FacebookPath = objMemberDecrypt.LinkFacebook;
            objMemberInfo.Faculty = objMemberDecrypt.Faculty;
            objMemberInfo.FullName = objMemberDecrypt.GetMemberFullName();
            objMemberInfo.Hometown = objMemberDecrypt.Hometown;
            objMemberInfo.JoinDate = objMemberDecrypt.JoinDate.ToString("dd/MM/yyyy");
            objMemberInfo.MemberId = objMemberDecrypt.Id;
            objMemberInfo.MemberType = nMemberType;
            objMemberInfo.PhoneNumber = objMemberDecrypt.PhoneNumber;
            objMemberInfo.Sex = objMemberDecrypt.Sex;
            objMemberInfo.StudentId = objMemberDecrypt.StudentId;
            objMemberInfo.RoleName = x_strRoleName;
            objMemberInfo.Scores = dScore;

            objResponseModel = ResponseManager.GetSuccsessResp(SuccsessConstant.GET_MY_INFO_SUCCSESS, objMemberInfo);
            return objResponseModel;
        }

        #region Override methods
        /// <summary>
        /// Get My Info
        /// </summary>
        /// <param name="x_strAccountId"></param>
        /// <param name="x_objContext"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public async Task<ResponseModel> GetMyInfo(string x_strAccountId, HttpContext x_objContext)
        {
            Accounts objAccount;
            Roles objRole;
            ResponseModel objResponseModel;
            string strClientIp;

            strClientIp = x_objContext.GetClientIPAddress();
            //------------------------------------
            // Get account
            //------------------------------------
            objAccount = await GetAccount(x_strAccountId);
            if (objAccount == null || objAccount.IsLocked == true)
            {
                // Get error
                objResponseModel = ResponseManager.GetErrorResp(ErrorConstant.ACCOUNT_IS_LOCKER, ErrorCode.ACCOUNT_IS_LOCKER);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.ACCOUNT_IS_LOCKER}", SOURCE);

                return objResponseModel;
            }
            //------------------------------------
            // Get Role
            //------------------------------------
            objRole = await RolesDbManager.GetRoleById(objAccount.RoleId);
            if (objRole == null)
            {
                // Get error
                objResponseModel = ResponseManager.GetErrorResp(ErrorConstant.AUTHORITY_INSUFFICIENT, ErrorCode.AUTHORITY_INSUFFICIENT);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objAccount.Id, $"{SOURCE}: {ErrorConstant.AUTHORITY_INSUFFICIENT}", SOURCE);

                return objResponseModel;
            }

            //------------------------------------
            // Get member info
            //------------------------------------
            objResponseModel = await GetMemberInfo(objAccount.MemberId, objAccount, objRole.RoleName, strClientIp);
            return objResponseModel;
        }

        /// <summary>
        /// Update My Info
        /// </summary>
        /// <param name="x_objRequestModel"></param>
        /// <param name="x_strAccountId"></param>
        /// <param name="x_objContext"></param>
        /// <returns></returns>
        public async Task<ResponseModel> UpdateMyInfo(ChangeMyInfoReqestModel x_objRequestModel, string x_strAccountId, HttpContext x_objContext)
        {
            MemberDecryptModel objMemberDecrypt;
            Accounts objAccount;
            ResponseModel objResponseModel;
            string strClientIp;
            string strLastName;
            string strFirstName;
            string strBirthDate;
            int nCountDataValid;
            bool bUpdateSuccsess;

            strClientIp = x_objContext.GetClientIPAddress();

            //------------------------------------
            // Check null request model
            //------------------------------------
            if (x_objRequestModel == null)
            {
                // Get error
                objResponseModel = ResponseManager.GetErrorResp(ErrorConstant.CHANGE_MEMBER_INFO_MODEL_IS_NULL, ErrorCode.CHANGE_MEMBER_INFO_MODEL_IS_NULL);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.CHANGE_MEMBER_INFO_MODEL_IS_NULL}", SOURCE);

                return objResponseModel;
            }

            //------------------------------------
            // Get account
            //------------------------------------
            objAccount = await GetAccount(x_strAccountId);
            if (objAccount == null || objAccount.IsLocked == true)
            {
                // Get error
                objResponseModel = ResponseManager.GetErrorResp(ErrorConstant.ACCOUNT_IS_LOCKER, ErrorCode.ACCOUNT_IS_LOCKER);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.ACCOUNT_IS_LOCKER}", SOURCE);

                return objResponseModel;
            }

            //------------------------------------
            // Get member info
            //------------------------------------
            objMemberDecrypt = MemberInfoManager.GetMemberById(objAccount.MemberId);
            if ((objMemberDecrypt == null) || (objMemberDecrypt.IsMember == false))
            {
                // Get error
                objResponseModel = ResponseManager.GetErrorResp(ErrorConstant.MEMBER_INFO_IS_NULL, ErrorCode.MEMBER_INFO_IS_NULL);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.MEMBER_INFO_IS_NULL}", SOURCE);

                return objResponseModel;
            }

            //------------------------------------
            // Validate and update data
            //------------------------------------
            nCountDataValid = 0;
            // Check Name
            if (x_objRequestModel.FullName.CheckNameIsValid() == true)
            {
                x_objRequestModel.FullName.NameSeparation(out strFirstName, out strLastName);

                if ((string.IsNullOrEmpty(strFirstName) == false) && (string.IsNullOrEmpty(strLastName) == false))
                {
                    objMemberDecrypt.FirstName = strFirstName.ToTitleCase();
                    objMemberDecrypt.LastName = strLastName.ToTitleCase();
                    nCountDataValid++;
                }
            }
            // Check birthday
            if (x_objRequestModel.BirthDay.CheckDateTimeFormat(out strBirthDate) == true)
            {
                objMemberDecrypt.BirthDay = strBirthDate;
                nCountDataValid++;
            }
            // Check sex
            if (x_objRequestModel.Sex.ValidateSex() == true)
            {
                objMemberDecrypt.Sex = x_objRequestModel.Sex.ToTitleCase();
                nCountDataValid++;
            }
            // Check Phone number
            if (x_objRequestModel.PhoneNumber.PhoneNumberIsValid() == true)
            {
                objMemberDecrypt.PhoneNumber = x_objRequestModel.PhoneNumber;
                nCountDataValid++;
            }
            // Check email
            if (x_objRequestModel.Email.EmailIsValid() == true)
            {
                objMemberDecrypt.Email = x_objRequestModel.Email;
                nCountDataValid++;
            }
            // Check Link Facebook
            if (x_objRequestModel.LinkFacebook.CheckFbValid() == true)
            {
                objMemberDecrypt.LinkFacebook = x_objRequestModel.LinkFacebook;
                nCountDataValid++;
            }
            // Check Faculty
            if (await CheckFacultyId(x_objRequestModel.FacultyId) == true)
            {
                objMemberDecrypt.Faculty.Id = x_objRequestModel.FacultyId;
                nCountDataValid++;
            }
            // Check class name
            if ((string.IsNullOrWhiteSpace(x_objRequestModel.ClassName) == false) && (x_objRequestModel.ClassName.Length > 0))
            {
                objMemberDecrypt.ClassName = x_objRequestModel.ClassName.Trim();
                nCountDataValid++;
            }
            // Check student id
            if (x_objRequestModel.StudentId.StudentIsValid() == true)
            {
                if (MemberInfoManager.CheckStudentIdExist(x_objRequestModel.StudentId, objAccount.MemberId) == true)
                {
                    // Get error
                    objResponseModel = ResponseManager.GetErrorResp(ErrorConstant.STUDENT_ID_IS_EXIST, ErrorCode.STUDENT_ID_IS_EXIST);
                    // Write log
                    LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.STUDENT_ID_IS_EXIST}", SOURCE);

                    return objResponseModel;
                }
                if (MemberRegisterMemory.CheckStudentIdIsExist(x_objRequestModel.StudentId, string.Empty) == true)
                {
                    // Get error
                    objResponseModel = ResponseManager.GetErrorResp(ErrorConstant.STUDENT_ID_IS_EXIST, ErrorCode.STUDENT_ID_IS_EXIST);
                    // Write log
                    LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.STUDENT_ID_IS_EXIST}", SOURCE);

                    return objResponseModel;
                }
                objMemberDecrypt.StudentId = x_objRequestModel.StudentId.Trim();
                nCountDataValid++;
            }
            // Update data
            if (nCountDataValid == 0)
            {
                // Get error
                objResponseModel = ResponseManager.GetErrorResp(ErrorConstant.UPDATE_MEMBER_INFO_INVALID, ErrorCode.UPDATE_MEMBER_INFO_INVALID);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMemberDecrypt.GetMemberFullName(), $"{SOURCE}: {ErrorConstant.UPDATE_MEMBER_INFO_INVALID}", SOURCE);

                return objResponseModel;
            }
            bUpdateSuccsess = await MemberInfoManager.UpdateMemberInfo(objMemberDecrypt);
            if (bUpdateSuccsess == false)
            {
                // Get error
                objResponseModel = ResponseManager.GetErrorResp(ErrorConstant.UPDATE_MEMBER_INFO_FAILED, ErrorCode.UPDATE_MEMBER_INFO_FAILED);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMemberDecrypt.GetMemberFullName(),
                                    $"{SOURCE}: {ErrorConstant.UPDATE_MEMBER_INFO_FAILED}", SOURCE);
                return objResponseModel;
            }
            objResponseModel = ResponseManager.GetSuccsessResp(SuccsessConstant.UPDATE_MEMBER_INFO_SUCCSESS);
            return objResponseModel;
        }

        /// <summary>
        /// Get Member Rank
        /// </summary>
        /// <param name="x_strAccountId"></param>
        /// <param name="x_objContext"></param>
        /// <returns></returns>
        public async Task<ResponseModel> GetMemberRank(string x_strAccountId, HttpContext x_objContext)
        {
            List<MemberRankInfoResponseModel> lstMemberRank;
            List<MemberDecryptModel> lstMemberDecrypt;
            MemberRankInfoResponseModel objMemberRankInfoResponseModel;
            ResponseModel objResponseModel;
            Accounts objAccount;
            string strClientIp;
            string strAvatarPath;
            double dScore;

            strClientIp = x_objContext.GetClientIPAddress();
            //-------------------------------------------
            // Get Account
            //-------------------------------------------
            objAccount = await GetAccount(x_strAccountId);
            if (objAccount == null || objAccount.IsLocked == true)
            {
                // Get error
                objResponseModel = ResponseManager.GetErrorResp(ErrorConstant.ACCOUNT_IS_LOCKER, ErrorCode.ACCOUNT_IS_LOCKER);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.ACCOUNT_IS_LOCKER}", SOURCE);

                return objResponseModel;
            }
            //-------------------------------------------
            // Get All Member
            //-------------------------------------------
            lstMemberDecrypt = MemberInfoManager.GetAllMemberList(GetMemberType.IS_MEMBER);

            if (lstMemberDecrypt == null)
            {
                // Get error
                objResponseModel = ResponseManager.GetErrorResp(ErrorConstant.MEMBER_IS_NOT_EXIST_IN_DATABASE, ErrorCode.MEMBER_IS_NOT_EXIST_IN_DATABASE);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.MEMBER_IS_NOT_EXIST_IN_DATABASE}", SOURCE);

                return objResponseModel;
            }

            // Create list
            lstMemberRank = new List<MemberRankInfoResponseModel>();
            foreach (MemberDecryptModel objTemp in lstMemberDecrypt)
            {
                if (objTemp == null)
                {
                    continue;
                }

                strAvatarPath = objTemp.AvatarPath;
                dScore = await GetMemberScore(objTemp.Id);
                objMemberRankInfoResponseModel = new MemberRankInfoResponseModel
                {
                    AvatarPath = strAvatarPath,
                    FirstName = objTemp.FirstName,
                    LastName = objTemp.LastName,
                    MemberId = objTemp.Id,
                    Score = dScore,
                    StudentId = objTemp.StudentId,
                };
                lstMemberRank.Add(objMemberRankInfoResponseModel);
            }
            // Sort
            if (lstMemberRank != null && lstMemberRank.Count > 0)
            {
                lstMemberRank = lstMemberRank.OrderByDescending(obj => obj.Score).ThenBy(obj => obj.LastName).ThenBy(obj => obj.FirstName).ToList();
            }
            for (int nIndex = 0; nIndex < lstMemberRank.Count; nIndex++)
            {
                lstMemberRank[nIndex].RankNo = nIndex + 1;
            }

            objResponseModel = ResponseManager.GetSuccsessResp("Lấy danh sách thành công", lstMemberRank, lstMemberRank.Count);
            return objResponseModel;
        }

        /// <summary>
        /// Get Member Info
        /// </summary>
        /// <param name="x_strMemberId"></param>
        /// <param name="x_strAccountId"></param>
        /// <param name="x_objContext"></param>
        /// <returns></returns>
        public async Task<ResponseModel> GetMemberInfo(string x_strMemberId, string x_strAccountId, HttpContext x_objContext)
        {
            Accounts objAccount;
            Roles objRole;
            ResponseModel objResponseModel;
            string strClientIp;

            strClientIp = x_objContext.GetClientIPAddress();
            //------------------------------------
            // Get account
            //------------------------------------
            objAccount = await GetAccount(x_strAccountId);
            if (objAccount == null || objAccount.IsLocked == true)
            {
                // Get error
                objResponseModel = ResponseManager.GetErrorResp(ErrorConstant.ACCOUNT_IS_LOCKER, ErrorCode.ACCOUNT_IS_LOCKER);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.ACCOUNT_IS_LOCKER}", SOURCE);

                return objResponseModel;
            }
            //------------------------------------
            // Get Role
            //------------------------------------
            objRole = await RolesDbManager.GetRoleById(objAccount.RoleId);
            if ((objRole == null) || (objRole.Permissions.Contains((int)DTUSVCPermission.SEARCH_MEMBER) == false))
            {
                // Get error
                objResponseModel = ResponseManager.GetErrorResp(ErrorConstant.AUTHORITY_INSUFFICIENT, ErrorCode.AUTHORITY_INSUFFICIENT);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objAccount.Id, $"{SOURCE}: {ErrorConstant.AUTHORITY_INSUFFICIENT}", SOURCE);

                return objResponseModel;
            }

            //------------------------------------
            // Get member account
            //------------------------------------
            objAccount = await GetAccount(x_strMemberId, true);
            if (objAccount == null)
            {
                // Get error
                objResponseModel = ResponseManager.GetErrorResp(ErrorConstant.MEMBER_INFO_IS_NULL, ErrorCode.MEMBER_INFO_IS_NULL);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.MEMBER_INFO_IS_NULL}", SOURCE);

                return objResponseModel;
            }
            //------------------------------------
            // Get member Role
            //------------------------------------
            objRole = await RolesDbManager.GetRoleById(objAccount.RoleId);
            if (objRole == null)
            {
                // Get error
                objResponseModel = ResponseManager.GetErrorResp(ErrorConstant.MEMBER_INFO_IS_NULL, ErrorCode.MEMBER_INFO_IS_NULL);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objAccount.Id, $"{SOURCE}: {ErrorConstant.MEMBER_INFO_IS_NULL}", SOURCE);

                return objResponseModel;
            }
            //------------------------------------
            // Get member info
            //------------------------------------
            objResponseModel = await GetMemberInfo(x_strMemberId, objAccount, objRole.RoleName, strClientIp, false);
            return objResponseModel;
        }

        /// <summary>
        /// Delete Member
        /// </summary>
        /// <param name="x_strMemberId"></param>
        /// <param name="x_strAccountId"></param>
        /// <param name="x_objContext"></param>
        /// <returns></returns>
        public async Task<ResponseModel> DeleteMember(string x_strMemberId, string x_strAccountId, HttpContext x_objContext)
        {
            Accounts objAccount;
            Roles objRole;
            ResponseModel objResponseModel;
            MemberDecryptModel objMemberDecrypt;
            string strClientIp;
            bool bUpdateSuccsess;

            strClientIp = x_objContext.GetClientIPAddress();
            //------------------------------------
            // Get account
            //------------------------------------
            objAccount = await GetAccount(x_strAccountId);
            if (objAccount == null || objAccount.IsLocked == true)
            {
                // Get error
                objResponseModel = ResponseManager.GetErrorResp(ErrorConstant.ACCOUNT_IS_LOCKER, ErrorCode.ACCOUNT_IS_LOCKER);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.ACCOUNT_IS_LOCKER}", SOURCE);

                return objResponseModel;
            }

            //------------------------------------
            // Get Role
            //------------------------------------
            objRole = await RolesDbManager.GetRoleById(objAccount.RoleId);
            if ((objRole == null) || (objRole.Permissions.Contains((int)DTUSVCPermission.DELETE_MEMBER) == false))
            {
                // Get error
                objResponseModel = ResponseManager.GetErrorResp(ErrorConstant.AUTHORITY_INSUFFICIENT, ErrorCode.AUTHORITY_INSUFFICIENT);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objAccount.Id, $"{SOURCE}: {ErrorConstant.AUTHORITY_INSUFFICIENT}", SOURCE);

                return objResponseModel;
            }

            //------------------------------------
            // Get member account
            //------------------------------------
            objAccount = await GetAccount(x_strMemberId, true);
            if (objAccount != null)
            {
                objAccount.IsLocked = true;
            }
            //------------------------------------
            // Get member info
            //------------------------------------
            objMemberDecrypt = MemberInfoManager.GetMemberById(x_strMemberId);
            if (objMemberDecrypt == null)
            {
                // Get error
                objResponseModel = ResponseManager.GetErrorResp(ErrorConstant.MEMBER_INFO_IS_NULL, ErrorCode.MEMBER_INFO_IS_NULL);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objAccount.Id, $"{SOURCE}: {ErrorConstant.MEMBER_INFO_IS_NULL}", SOURCE);

                return objResponseModel;
            }
            // Update member to database
            objMemberDecrypt.IsMember = false;
            bUpdateSuccsess = await MemberInfoManager.UpdateMemberInfo(objMemberDecrypt);
            if (bUpdateSuccsess == false)
            {
                // Get error
                objResponseModel = ResponseManager.GetErrorResp(ErrorConstant.UPDATE_MEMBER_INFO_FAILED, ErrorCode.UPDATE_MEMBER_INFO_FAILED);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMemberDecrypt.GetMemberFullName(),
                                    $"{SOURCE}: {ErrorConstant.UPDATE_MEMBER_INFO_FAILED}", SOURCE);
                return objResponseModel;
            }
            await AccountDbManager.UpdateAccount(objAccount);
            objResponseModel = ResponseManager.GetSuccsessResp(SuccsessConstant.DELETE_MEMBER_IS_SUCCSESS);
            LoginData.LogoutById(objAccount.Id);
            return objResponseModel;
        }

        /// <summary>
        /// Get Member List
        /// </summary>
        /// <param name="x_nMemberType"></param>
        /// <param name="x_strAccountId"></param>
        /// <param name="x_objContext"></param>
        /// <returns></returns>
        public async Task<ResponseModel> GetMemberList(int x_nMemberType, string x_strAccountId, HttpContext x_objContext)
        {
            GetMemberType eMemberType;
            string strClientIp;
            string strAvatarPath;
            int nMemberType;
            List<MemberListResponseModel> lstMemberListResponse;
            MemberListResponseModel objMemberListResponse;
            List<MemberDecryptModel> lstMemberInfo;
            Accounts objAccount;
            ResponseModel objResponseModel;
            Roles objRole;

            strClientIp = x_objContext.GetClientIPAddress();
            //------------------------------------
            // Get account
            //------------------------------------
            objAccount = await GetAccount(x_strAccountId);
            if (objAccount == null || objAccount.IsLocked == true)
            {
                // Get error
                objResponseModel = ResponseManager.GetErrorResp(ErrorConstant.ACCOUNT_IS_LOCKER, ErrorCode.ACCOUNT_IS_LOCKER);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.ACCOUNT_IS_LOCKER}", SOURCE);

                return objResponseModel;
            }

            //------------------------------------
            // Get Role
            //------------------------------------
            objRole = await RolesDbManager.GetRoleById(objAccount.RoleId);
            if ((objRole == null) || (objRole.Permissions.Contains((int)DTUSVCPermission.GET_MEMBER_LIST) == false))
            {
                // Get error
                objResponseModel = ResponseManager.GetErrorResp(ErrorConstant.AUTHORITY_INSUFFICIENT, ErrorCode.AUTHORITY_INSUFFICIENT);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objAccount.Id, $"{SOURCE}: {ErrorConstant.AUTHORITY_INSUFFICIENT}", SOURCE);

                return objResponseModel;
            }

            // Get member list
            eMemberType = x_nMemberType.ToEnum(GetMemberType.IS_MEMBER);
            lstMemberInfo = MemberInfoManager.GetAllMemberList(eMemberType);
            if (lstMemberInfo == null)
            {
                // Get error
                objResponseModel = ResponseManager.GetErrorResp(ErrorConstant.MEMBER_IS_NOT_EXIST_IN_DATABASE, ErrorCode.MEMBER_IS_NOT_EXIST_IN_DATABASE);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.MEMBER_IS_NOT_EXIST_IN_DATABASE}", SOURCE);

                return objResponseModel;
            }

            lstMemberListResponse = new List<MemberListResponseModel>();
            foreach (MemberDecryptModel objTemp in lstMemberInfo)
            {
                if (objTemp == null)
                {
                    continue;
                }
                strAvatarPath = objTemp.AvatarPath;
                //------------------------------------
                // Get member Type
                //------------------------------------
                if (objTemp.IsMember == false)
                {
                    nMemberType = 1;
                }
                else
                {
                    nMemberType = 0;
                }

                objMemberListResponse = new MemberListResponseModel
                {
                    AvatarPath = strAvatarPath,
                    FacebookPath = objTemp.AvatarPath,
                    FacultyName = objTemp.Faculty.FacultyName,
                    FirstName = objTemp.FirstName,
                    LastName = objTemp.LastName,
                    MemberId = objTemp.Id,
                    MemberType = nMemberType,
                    PhoneNumber = objTemp.PhoneNumber,
                    Sex = objTemp.Sex,
                    StudentId = objTemp.StudentId
                };
                lstMemberListResponse.Add(objMemberListResponse);
            }
            // Sort
            lstMemberListResponse = lstMemberListResponse.OrderBy(obj => obj.LastName).ThenBy(obj => obj.FirstName).ToList();
            objResponseModel = ResponseManager.GetSuccsessResp(SuccsessConstant.GET_LIST_MEMBER_SUCCSESS, lstMemberListResponse, lstMemberListResponse.Count);
            return objResponseModel;
        }

        /// <summary>
        /// Download Member Card
        /// </summary>
        /// <param name="x_objRequestModel"></param>
        /// <param name="x_strAccountId"></param>
        /// <param name="x_objContext"></param>
        /// <returns></returns>
        public async Task<ResponseDownloadFile> DownloadMemberCard(DownloadMemberCardRequestModel x_objRequestModel, string x_strAccountId, HttpContext x_objContext)
        {
            const string FILE_NAME_FORMAT = "MemberCardList_{0}_{1}.xlsx";
            const string FOLDER_NAME = "MemberCard_{0}";
            const string DATE_TIME_FORMAT = "yyyyMMddHHmmssfff";
            const string SHEET_NAME = "MemberCardList";
            const string ZIP_FILE_NAME_FORMAT = "MemberCard_{0}.zip";
            string strClientIp;
            string strExcelFileName;
            string strExcelFilePath;
            string strFolderName;
            string strFolderPath;
            string strAvatarPathSource;
            string strAvatarPathDest;
            string strQrCodePathSource;
            string strQrCodePathDest;
            string strQrCodeName;
            string strZipPath;
            string strZipFileName;
            string strZipFolder;
            string strCardFormPathSource;
            string strCardFormPathDest;
            bool bResult;
            Accounts objAccount;
            Roles objRole;
            Random objRandom;
            DateTime objCurrentTime;
            List<Accounts> lstAccount;
            List<Roles> lstRoles;
            List<MemberDecryptModel> lstMemberInfo;
            List<MemberCardExportData> lstMemberExport;
            Dictionary<string, List<MemberCardExportData>> dicExportData;
            MemberCardExportData objMemberExport;
            ResponseDownloadFile objResponseModel;
            Bitmap objQrCodeImage;

            strClientIp = x_objContext.GetClientIPAddress();
            //------------------------------------
            // Get account
            //------------------------------------
            objAccount = await GetAccount(x_strAccountId);
            if (objAccount == null || objAccount.IsLocked == true)
            {
                // Get error
                objResponseModel = ResponseManager.GetDownloadErrorResp(ErrorConstant.ACCOUNT_IS_LOCKER, ErrorCode.ACCOUNT_IS_LOCKER);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.ACCOUNT_IS_LOCKER}", SOURCE);

                return objResponseModel;
            }

            //------------------------------------
            // Get Role
            //------------------------------------
            objRole = await RolesDbManager.GetRoleById(objAccount.RoleId);
            if ((objRole == null) || (objRole.Permissions.Contains((int)DTUSVCPermission.DOWNLOAD_MEMBER_LIST) == false))
            {
                // Get error
                objResponseModel = ResponseManager.GetDownloadErrorResp(ErrorConstant.AUTHORITY_INSUFFICIENT, ErrorCode.AUTHORITY_INSUFFICIENT);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objAccount.Id, $"{SOURCE}: {ErrorConstant.AUTHORITY_INSUFFICIENT}", SOURCE);

                return objResponseModel;
            }

            // Get member list
            lstMemberInfo = MemberInfoManager.GetAllMemberList(GetMemberType.IS_MEMBER);
            if (lstMemberInfo == null)
            {
                // Get error
                objResponseModel = ResponseManager.GetDownloadErrorResp(ErrorConstant.MEMBER_IS_NOT_EXIST_IN_DATABASE, ErrorCode.MEMBER_IS_NOT_EXIST_IN_DATABASE);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.MEMBER_IS_NOT_EXIST_IN_DATABASE}", SOURCE);

                return objResponseModel;
            }

            // Get Role list
            lstRoles = await RolesDbManager.GetAllRole();
            lstAccount = await AccountDbManager.GetListAccount();
            objCurrentTime = DateTimeHelper.GetCurrentDateTimeVN();
            strFolderName = string.Format(FOLDER_NAME, objCurrentTime.ToString(DATE_TIME_FORMAT));
            // Get member list
            lstMemberExport = new List<MemberCardExportData>();
            foreach (MemberDecryptModel objTemp in lstMemberInfo)
            {
                if ((objTemp == null) || ((x_objRequestModel.MemberId.Count > 0) && (x_objRequestModel.MemberId.Contains(objTemp.Id) == false)))
                {
                    continue;
                }
                objMemberExport = new MemberCardExportData();
                // Skip if does not exist avatar
                if (objTemp.AvatarPath.EndsWith(Path.Combine(SysFolder.LOGO_FOLDER, SysFile.LOGO_FILE_NAME)) == true)
                {
                    continue;
                }
                // Copy avatar
                objMemberExport.AvatarName = Path.GetFileName(objTemp.AvatarPath);
                strAvatarPathSource = Path.Combine(SysFolder.WWWROOT_PATH, objTemp.AvatarPath);
                strAvatarPathDest = FileHelper.GetFilePath(SysFolder.EXPORT_CARD_ALL, strFolderName, SysFolder.EXPORT_CARD_AVATAR,
                                                                objMemberExport.AvatarName);
                FileHelper.CopyFile(strAvatarPathSource, strAvatarPathDest);

                // Copy Qr-Code
                strQrCodeName = string.Format(SysFile.QR_CODE_NAME_FORMAT, objTemp.StudentId);
                strQrCodePathSource = FileHelper.GetFilePath(SysFolder.QR_CODE_FOLDER, strQrCodeName);
                if (File.Exists(strQrCodePathSource) == false)
                {
                    objQrCodeImage = QRCodeHelper.CreateQrCode(objTemp.Id);
                    bResult = objQrCodeImage.SaveBitmapStream(strQrCodePathSource);
                    if (bResult == false)
                    {
                        continue;
                    }
                }
                strQrCodePathDest = strAvatarPathDest = FileHelper.GetFilePath(SysFolder.EXPORT_CARD_ALL, strFolderName,
                                                                                SysFolder.EXPORT_CARD_QR_CODE, strQrCodeName);
                FileHelper.CopyFile(strQrCodePathSource, strQrCodePathDest);
                objMemberExport.QrCodeName = strQrCodeName;

                // Get Account
                objAccount = lstAccount.Find(obj => obj.MemberId.Equals(objTemp.Id) == true);
                if (objAccount == null)
                {
                    continue;
                }
                // Get Role
                objRole = lstRoles.Find(obj => obj.Id.Equals(objAccount.RoleId) == true);
                if (objRole == null)
                {
                    continue;
                }
                // Set data
                objMemberExport.StudentId = objTemp.StudentId;
                objMemberExport.FullName = objTemp.GetMemberFullName();
                objMemberExport.BirthDay = objTemp.BirthDay;
                objMemberExport.FacultyName = objTemp.Faculty.FacultyName;
                objMemberExport.RoleName = objRole.RoleName;
                lstMemberExport.Add(objMemberExport);
            }

            // sort data
            if (lstMemberExport.Count > 0)
            {
                lstMemberExport = lstMemberExport.OrderBy(obj => obj.FullName).ToList();
            }

            // Create export data
            dicExportData = new Dictionary<string, List<MemberCardExportData>>();
            dicExportData[SHEET_NAME] = lstMemberExport;

            // Get file path
            objRandom = new Random();
            objCurrentTime = DateTimeHelper.GetCurrentDateTimeVN();
            strExcelFileName = string.Format(FILE_NAME_FORMAT, objCurrentTime.ToString(DATE_TIME_FORMAT), objRandom.Next(1000, 9999));
            strExcelFilePath = FileHelper.GetFilePath(SysFolder.EXPORT_CARD_ALL, strFolderName, strExcelFileName);
            // Export file
            bResult = dicExportData.ExportDataToExcel(strExcelFilePath);
            if (bResult == false)
            {
                // Get error
                objResponseModel = ResponseManager.GetDownloadErrorResp(ErrorConstant.EXPORT_EXCEL_FAILED, ErrorCode.EXPORT_EXCEL_FAILED);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.EXPORT_EXCEL_FAILED}", SOURCE);

                return objResponseModel;
            }
            // Copy card form
            strCardFormPathSource = FileHelper.GetFilePath(SysFolder.MEMBER_CARD_FORM, SysFile.MEMBER_CARD_FROM_FILE, true);
            strCardFormPathDest = FileHelper.GetFilePath(SysFolder.EXPORT_CARD_ALL, strFolderName, SysFile.MEMBER_CARD_FROM_FILE);
            FileHelper.CopyFile(strCardFormPathSource, strCardFormPathDest);
            // Zip file
            strZipFileName = string.Format(ZIP_FILE_NAME_FORMAT, objCurrentTime.ToString(DATE_TIME_FORMAT));
            strZipFolder = FileHelper.GetFolderFullPath(SysFolder.EXPORT_CARD_ALL, strFolderName);
            strZipPath = FileHelper.GetFilePath(SysFolder.EXPORT_CARD_ALL, strZipFileName);
            FileHelper.CreateZipFile(strZipFolder, strZipPath);
            // Delete temp folder
            strFolderPath = FileHelper.GetFolderFullPath(SysFolder.EXPORT_CARD_ALL, strFolderName);
            if (Directory.Exists(strFolderPath) == true)
            {
                Directory.Delete(strFolderPath, true);
            }

            // Return response
            objResponseModel = new ResponseDownloadFile();
            objResponseModel.IsSuccsess = true;
            objResponseModel.FilePath = strZipPath;
            return objResponseModel;
        }

        /// <summary>
        /// Download Member List
        /// </summary>
        /// <param name="x_nMemberType"></param>
        /// <param name="x_strAccountId"></param>
        /// <param name="x_objContext"></param>
        /// <returns></returns>
        public async Task<ResponseDownloadFile> DownloadMemberList(int x_nMemberType, string x_strAccountId, HttpContext x_objContext)
        {
            const string FILE_NAME_FORMAT = "MemberList_{0}_{1}.xlsx";
            const string DATE_TIME_FORMAT = "yyyyMMddHHmmssfff";
            const string SHEET_NAME = "MemberList";
            string strClientIp;
            string strFileName;
            string strFilePath;
            bool bExportResult;
            GetMemberType eMemberType;
            Accounts objAccount;
            Roles objRole;
            Random objRandom;
            DateTime objCurrentTime;
            List<Accounts> lstAccount;
            List<Roles> lstRoles;
            List<MemberDecryptModel> lstMemberInfo;
            List<MemberInfoExportExcel> lstMemberExport;
            Dictionary<string, List<MemberInfoExportExcel>> dicExportData;
            MemberInfoExportExcel objMemberExport;
            ResponseDownloadFile objResponseModel;

            strClientIp = x_objContext.GetClientIPAddress();
            //------------------------------------
            // Get account
            //------------------------------------
            objAccount = await GetAccount(x_strAccountId);
            if (objAccount == null || objAccount.IsLocked == true)
            {
                // Get error
                objResponseModel = ResponseManager.GetDownloadErrorResp(ErrorConstant.ACCOUNT_IS_LOCKER, ErrorCode.ACCOUNT_IS_LOCKER);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.ACCOUNT_IS_LOCKER}", SOURCE);

                return objResponseModel;
            }

            //------------------------------------
            // Get Role
            //------------------------------------
            objRole = await RolesDbManager.GetRoleById(objAccount.RoleId);
            if ((objRole == null) || (objRole.Permissions.Contains((int)DTUSVCPermission.DOWNLOAD_MEMBER_LIST) == false))
            {
                // Get error
                objResponseModel = ResponseManager.GetDownloadErrorResp(ErrorConstant.AUTHORITY_INSUFFICIENT, ErrorCode.AUTHORITY_INSUFFICIENT);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objAccount.Id, $"{SOURCE}: {ErrorConstant.AUTHORITY_INSUFFICIENT}", SOURCE);

                return objResponseModel;
            }

            // Get member list
            eMemberType = x_nMemberType.ToEnum(GetMemberType.IS_MEMBER);
            lstMemberInfo = MemberInfoManager.GetAllMemberList(eMemberType);
            if (lstMemberInfo == null)
            {
                // Get error
                objResponseModel = ResponseManager.GetDownloadErrorResp(ErrorConstant.MEMBER_IS_NOT_EXIST_IN_DATABASE, ErrorCode.MEMBER_IS_NOT_EXIST_IN_DATABASE);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.MEMBER_IS_NOT_EXIST_IN_DATABASE}", SOURCE);

                return objResponseModel;
            }

            // Get Role list
            lstRoles = await RolesDbManager.GetAllRole();
            lstAccount = await AccountDbManager.GetListAccount();

            // Get member list
            lstMemberExport = new List<MemberInfoExportExcel>();
            foreach (MemberDecryptModel objTemp in lstMemberInfo)
            {
                if (objTemp == null)
                {
                    continue;
                }
                // Get Account
                objAccount = lstAccount.Find(obj => obj.MemberId.Equals(objTemp.Id) == true);
                if (objAccount == null)
                {
                    continue;
                }
                // Get Role
                objRole = lstRoles.Find(obj => obj.Id.Equals(objAccount.RoleId) == true);
                if (objRole == null)
                {
                    continue;
                }
                // Set data
                objMemberExport = new MemberInfoExportExcel
                {
                    BirthDay = objTemp.BirthDay,
                    ClassName = objTemp.ClassName,
                    Email = objTemp.Email,
                    FacultyName = objTemp.Faculty.FacultyName,
                    FirstName = objTemp.FirstName,
                    Hometown = objTemp.Hometown.ToString(),
                    LastName = objTemp.LastName,
                    PhoneNumber = objTemp.PhoneNumber,
                    RoleName = objRole.RoleName,
                    Scores = await GetMemberScore(objTemp.Id),
                    Sex = objTemp.Sex,
                    StudentId = objTemp.StudentId
                };
                lstMemberExport.Add(objMemberExport);
            }
            // sort data
            if (lstMemberExport.Count > 0)
            {
                lstMemberExport = lstMemberExport.OrderBy(obj => obj.LastName).ThenBy(obj => obj.FirstName).ThenByDescending(obj => obj.Scores).ToList();
            }

            // Create export data
            dicExportData = new Dictionary<string, List<MemberInfoExportExcel>>();
            dicExportData[SHEET_NAME] = lstMemberExport;

            // Get file path
            objRandom = new Random();
            objCurrentTime = DateTimeHelper.GetCurrentDateTimeVN();
            strFileName = string.Format(FILE_NAME_FORMAT, objCurrentTime.ToString(DATE_TIME_FORMAT), objRandom.Next(1000, 9999));
            strFilePath = FileHelper.GetFilePath(SysFolder.EXPORT_MEMBER_LIST, strFileName);
            // Export file
            bExportResult = dicExportData.ExportDataToExcel(strFilePath);
            if (bExportResult == false)
            {
                // Get error
                objResponseModel = ResponseManager.GetDownloadErrorResp(ErrorConstant.EXPORT_EXCEL_FAILED, ErrorCode.EXPORT_EXCEL_FAILED);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.EXPORT_EXCEL_FAILED}", SOURCE);

                return objResponseModel;
            }

            objResponseModel = new ResponseDownloadFile();
            objResponseModel.IsSuccsess = true;
            objResponseModel.FilePath = strFilePath;
            return objResponseModel;
        }

        /// <summary>
        /// Update Avatar
        /// </summary>
        /// <param name="x_objRequestModel"></param>
        /// <param name="x_strAccountId"></param>
        /// <param name="x_objContext"></param>
        /// <returns></returns>
        public async Task<ResponseModel> UpdateAvatar(ChangeAvatarRequestModel x_objRequestModel, string x_strAccountId, HttpContext x_objContext)
        {
            const string AVATAR_NAME_FORMAT = "{0}{1}";
            MemberDecryptModel objMemberDecrypt;
            Accounts objAccount;
            ResponseModel objResponseModel;
            Bitmap objImage;
            DateTime objCurrentDate;
            string strClientIp;
            string strFileExention;
            string strFileName;
            string strFilePath;
            string strOldFilePath;
            string strResult;
            bool bUpdateSuccsess;

            strClientIp = x_objContext.GetClientIPAddress();

            //------------------------------------
            // Check null request model
            //------------------------------------
            if (x_objRequestModel == null)
            {
                // Get error
                objResponseModel = ResponseManager.GetErrorResp(ErrorConstant.UPLOAD_FILE_IS_NULL, ErrorCode.UPLOAD_FILE_IS_NULL);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.UPLOAD_FILE_IS_NULL}", SOURCE);

                return objResponseModel;
            }
            if (x_objRequestModel.Avatar.FileIsValid(ContentType.IMAGE_CONTENT_TYPE, out strResult) == false)
            {
                if (strResult.Equals(ErrorConstant.FILE_DOWNLOADED_IN_WRONG_FORMAT) == true)
                {
                    strResult = string.Format(strResult, string.Join(" | ", ContentType.IMAGE_CONTENT_TYPE));
                }
                // Get error
                objResponseModel = ResponseManager.GetErrorResp(strResult, ErrorCode.UPLOAD_FILE_IS_NULL);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {strResult}", SOURCE);

                return objResponseModel;
            }

            //------------------------------------
            // Get account
            //------------------------------------
            objAccount = await GetAccount(x_strAccountId);
            if (objAccount == null || objAccount.IsLocked == true)
            {
                // Get error
                objResponseModel = ResponseManager.GetErrorResp(ErrorConstant.ACCOUNT_IS_LOCKER, ErrorCode.ACCOUNT_IS_LOCKER);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.ACCOUNT_IS_LOCKER}", SOURCE);

                return objResponseModel;
            }

            //------------------------------------
            // Get member info
            //------------------------------------
            objMemberDecrypt = MemberInfoManager.GetMemberById(objAccount.MemberId);
            if ((objMemberDecrypt == null) || (objMemberDecrypt.IsMember == false))
            {
                // Get error
                objResponseModel = ResponseManager.GetErrorResp(ErrorConstant.MEMBER_INFO_IS_NULL, ErrorCode.MEMBER_INFO_IS_NULL);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.MEMBER_INFO_IS_NULL}", SOURCE);

                return objResponseModel;
            }

            //------------------------------------
            // Validate and update data
            //------------------------------------
            if (x_objRequestModel.Avatar == null)
            {
                // Get error
                objResponseModel = ResponseManager.GetErrorResp(ErrorConstant.UPLOAD_FILE_IS_NULL, ErrorCode.UPLOAD_FILE_IS_NULL);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMemberDecrypt.GetMemberFullName(),
                                                        $"{SOURCE}: {ErrorConstant.UPLOAD_FILE_IS_NULL}", SOURCE);

                return objResponseModel;
            }
            // Virus scan
            strResult = x_objRequestModel.Avatar.ScanVirus();
            if (strResult.Equals(VirusScanner.EXIST_VIRUS))
            {
                // Get error
                objResponseModel = ResponseManager.GetErrorResp(strResult, ErrorCode.UPLOAD_FILE_IS_NULL);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMemberDecrypt.GetMemberFullName(),
                                                        $"{SOURCE}: {strResult}", SOURCE);

                return objResponseModel;
            }
            // Copy file
            if ((string.IsNullOrWhiteSpace(strResult) == true) || (strResult.Equals(VirusScanner.IS_NOT_EXIST_VIRUS) == true) ||
                (strResult.Equals(VirusScanner.IS_DISCONNECT_SERVER) == true))
            {
                objCurrentDate = DateTimeHelper.GetCurrentDateTimeVN();
                strFileExention = Path.GetExtension(x_objRequestModel.Avatar.FileName);
                strFileName = string.Format(AVATAR_NAME_FORMAT, objCurrentDate.Ticks, strFileExention);
                strFilePath = FileHelper.GetFilePath(SysFolder.AVATAR_FOLDER, strFileName);
                strResult = await x_objRequestModel.Avatar.SaveFormFile(strFilePath);
                if (string.IsNullOrWhiteSpace(strResult) == false)
                {
                    // Get error
                    objResponseModel = ResponseManager.GetErrorResp(strResult, ErrorCode.UPLOAD_FILE_IS_NULL);
                    // Write log
                    LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMemberDecrypt.GetMemberFullName(),
                                                            $"{SOURCE}: {strResult}", SOURCE);

                    return objResponseModel;
                }

                objImage = FileHelper.OpenImageFile(strFilePath);
                objImage = objImage.CropImage(ImageHelper.AVATAR_IMAGE, out strResult);
                if (objImage == null)
                {
                    // Get error
                    objResponseModel = ResponseManager.GetErrorResp(strResult, ErrorCode.UPLOAD_FILE_IS_NULL);
                    // Write log
                    LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMemberDecrypt.GetMemberFullName(),
                                                            $"{SOURCE}: {strResult}", SOURCE);

                    return objResponseModel;
                }
                // Save image
                objImage.SaveBitmapStream(strFilePath);
                // Update to Database
                strOldFilePath = objMemberDecrypt.AvatarPath;
                objMemberDecrypt.AvatarPath = Path.Combine(SysFolder.AVATAR_FOLDER, strFileName);
                bUpdateSuccsess = await MemberInfoManager.UpdateMemberInfo(objMemberDecrypt);
                if (bUpdateSuccsess == false)
                {
                    // Get error
                    objResponseModel = ResponseManager.GetErrorResp(ErrorConstant.UPDATE_MEMBER_INFO_FAILED, ErrorCode.UPDATE_MEMBER_INFO_FAILED);
                    // Write log
                    LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMemberDecrypt.GetMemberFullName(),
                                        $"{SOURCE}: {ErrorConstant.UPDATE_MEMBER_INFO_FAILED}", SOURCE);
                    return objResponseModel;
                }
                // Delete old file
                strOldFilePath = Path.Combine(SysFolder.WWWROOT_PATH, strOldFilePath);
                try
                {
                    if (File.Exists(strOldFilePath) == true)
                    {
                        File.Delete(strOldFilePath);
                    }
                }
                catch { }

                strFilePath = objMemberDecrypt.AvatarPath;
                objResponseModel = ResponseManager.GetSuccsessResp(SuccsessConstant.UPDATE_AVATAR_SUCCSESS, strFilePath);
                return objResponseModel;
            }

            // Get error
            objResponseModel = ResponseManager.GetErrorResp(ErrorConstant.UPDATE_MEMBER_INFO_FAILED, ErrorCode.UPDATE_MEMBER_INFO_FAILED);
            // Write log
            LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMemberDecrypt.GetMemberFullName(),
                                $"{SOURCE}: {ErrorConstant.UPDATE_MEMBER_INFO_FAILED}", SOURCE);
            return objResponseModel;
        }

        /// <summary>
        /// Search Member Info
        /// </summary>
        /// <param name="x_strMemberId"></param>
        /// <param name="x_strAccountId"></param>
        /// <param name="x_objContext"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public async Task<ResponseModel> SearchMemberInfo(string x_strMemberId, string x_strAccountId, HttpContext x_objContext)
        {
            ResponseModel objResponse;
            MemberDecryptModel objMemberInfo;
            Accounts objAccount;
            Roles objRole;
            SeartchMemberData objSeartchMemberData;
            string strAvatarPath;
            string strClientIp;

            //---------------------------------------------------
            // Get client ip
            //---------------------------------------------------
            strClientIp = x_objContext.GetClientIPAddress();
            if (string.IsNullOrWhiteSpace(x_strMemberId) == true)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.MEMBER_INFO_IS_NULL, ErrorCode.MEMBER_INFO_IS_NULL);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.MEMBER_INFO_IS_NULL}", SOURCE);

                return objResponse;
            }
            //---------------------------------------------------
            // Get login Account
            //---------------------------------------------------
            objAccount = await GetAccount(x_strAccountId);
            if ((objAccount == null) || (objAccount.IsLocked == true))
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.ACCOUNT_IS_LOCKER, ErrorCode.ACCOUNT_IS_LOCKER);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.ACCOUNT_IS_LOCKER}", SOURCE);

                return objResponse;
            }

            //------------------------------------
            // Get login member info
            //------------------------------------
            objMemberInfo = MemberInfoManager.GetMemberById(objAccount.MemberId);
            if ((objMemberInfo == null) || (objMemberInfo.IsMember == false))
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.MEMBER_INFO_IS_NULL, ErrorCode.MEMBER_INFO_IS_NULL);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.MEMBER_INFO_IS_NULL}", SOURCE);

                return objResponse;
            }
            //------------------------------------
            // Get login role
            //------------------------------------
            objRole = await RolesDbManager.GetRoleById(objAccount.RoleId);
            if ((objRole == null) || (objRole.Permissions.Contains((int)DTUSVCPermission.SEARCH_MEMBER_CONTAINS) == false))
            {
                //------------------------------------
                // Get member info
                //------------------------------------
                objMemberInfo = MemberInfoManager.GetMemberById(x_strMemberId);
            }
            else
            {
                //------------------------------------
                // Get member info
                //------------------------------------
                objMemberInfo = MemberInfoManager.GetMemberById(x_strMemberId, FindType.CONTAINS);
            }
            if ((objMemberInfo == null) || (objMemberInfo.IsMember == false))
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.MEMBER_INFO_IS_NULL, ErrorCode.MEMBER_INFO_IS_NULL);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.MEMBER_INFO_IS_NULL}", SOURCE);

                return objResponse;
            }
            //------------------------------------
            // Get member Account
            //------------------------------------
            objAccount = await GetAccount(objMemberInfo.Id, true);
            if ((objAccount == null) || (objAccount.IsLocked == true))
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.MEMBER_INFO_IS_NULL, ErrorCode.MEMBER_INFO_IS_NULL);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMemberInfo.GetMemberFullName(),
                                    $"{SOURCE}: {ErrorConstant.MEMBER_INFO_IS_NULL}", SOURCE);

                return objResponse;
            }
            //------------------------------------
            // Get member role
            //------------------------------------
            objRole = await RolesDbManager.GetRoleById(objAccount.RoleId);
            if (objRole == null)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.MEMBER_INFO_IS_NULL, ErrorCode.MEMBER_INFO_IS_NULL);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMemberInfo.GetMemberFullName(),
                                    $"{SOURCE}: {ErrorConstant.MEMBER_INFO_IS_NULL}", SOURCE);

                return objResponse;
            }
            // Create Response model
            strAvatarPath = objMemberInfo.AvatarPath;
            objSeartchMemberData = new SeartchMemberData
            {
                AvatarPath = strAvatarPath,
                BirthDay = objMemberInfo.BirthDay,
                FacebookPath = objMemberInfo.LinkFacebook,
                FacultyName = objMemberInfo.Faculty.FacultyName,
                FullName = objMemberInfo.GetMemberFullName(),
                MemberId = objMemberInfo.Id,
                RoleName = objRole.RoleName,
                StudentId = objMemberInfo.StudentId
            };
            objResponse = ResponseManager.GetSuccsessResp(SuccsessConstant.GET_MY_INFO_SUCCSESS, objSeartchMemberData);
            return objResponse;
        }
        #endregion
        #endregion
    }
}
