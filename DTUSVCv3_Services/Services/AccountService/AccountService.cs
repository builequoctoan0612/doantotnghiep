﻿using DTUSVCv3_EntityFramework;
using DTUSVCv3_Library;
using DTUSVCv3_Library.LibraryModel;
using DTUSVCv3_MemoryData;
using DTUSVCv3_MemoryData.ForgotPasswData;
using DTUSVCv3_Services.Models.AccountModel;
using Microsoft.AspNetCore.Http;
using FileHelper = DTUSVCv3_Library.FileHelper;

namespace DTUSVCv3_Services
{
    public class AccountService : BaseSerivce, IAccountService
    {
        public AccountService()
            : base()
        {
            m_objGetCaptchaLock = new object();
        }

        #region Types
        private const string SOURCE = "AccountService";
        #endregion

        #region Fields
        private object m_objGetCaptchaLock;
        #endregion

        #region Methods
        /// <summary>
        /// Compare Passwd
        /// </summary>
        /// <param name="x_strPasswd1"></param>
        /// <param name="x_strPasswd2"></param>
        /// <returns></returns>
        private bool ComparePasswd(string x_strPasswd1, string x_strPasswd2)
        {
            if ((string.IsNullOrWhiteSpace(x_strPasswd1) == true) || (string.IsNullOrWhiteSpace(x_strPasswd2) == true))
            {
                return false;
            }

            if (x_strPasswd1.Equals(x_strPasswd2) == false)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Get Hash Email
        /// </summary>
        /// <param name="x_strEmail"></param>
        /// <returns></returns>
        private string GetHashEmail(string x_strEmail)
        {
            string[] arrSplit;
            string strEmail;
            List<char> lstEmail;
            if (string.IsNullOrWhiteSpace(x_strEmail) == true)
            {
                return string.Empty;
            }

            arrSplit = x_strEmail.Split('@');
            lstEmail = new List<char>();
            for (int nIndex = 0; nIndex < arrSplit[0].Length; nIndex++)
            {
                if ((nIndex > 1) && ((nIndex + 3) <= arrSplit[0].Length))
                {
                    lstEmail.Add('*');
                }
                else
                {
                    lstEmail.Add(arrSplit[0][nIndex]);
                }
            }

            arrSplit[0] = string.Join("", lstEmail);
            strEmail = string.Join("@", arrSplit);
            return strEmail;
        }

        /// <summary>
        /// Create Default Passwd
        /// </summary>
        /// <param name="x_strStudentId"></param>
        /// <param name="x_strBirthDay"></param>
        /// <returns></returns>
        private string CreateDefaultPasswd(string x_strStudentId, string x_strBirthDay)
        {
            if (string.IsNullOrWhiteSpace(x_strStudentId) == true || string.IsNullOrWhiteSpace(x_strBirthDay) == true)
            {
                return string.Empty;
            }

            return string.Format("{0}{1}", x_strStudentId, x_strBirthDay.Replace("/", ""));
        }
        #endregion

        #region Override Methods
        /// <summary>
        /// Change Password
        /// </summary>
        /// <param name="x_objRequestModel"></param>
        /// <param name="x_strAccountId"></param>
        /// <param name="x_objContext"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public async Task<ResponseModel> ChangePassword(ChangePasswdRequestModel x_objRequestModel, string x_strAccountId, HttpContext x_objContext)
        {
            bool bPasswdIsValid;
            string strKey;
            string strNewKey;
            string strClientIp;
            string strNewPasswd;
            string strToken;
            Accounts objAccount;
            ResponseModel objResponseModel;

            // get ip
            strClientIp = x_objContext.GetClientIPAddress();
            if (x_objRequestModel == null)
            {
                // Get error
                objResponseModel = ResponseManager.GetErrorResp(ErrorConstant.CHANGE_PASS_MODEL_INVALID, ErrorCode.CHANGE_PASS_MODEL_INVALID);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.CHANGE_PASS_MODEL_INVALID}", SOURCE);

                return objResponseModel;
            }

            // -----------------------------
            // Validate data
            // -----------------------------
            bPasswdIsValid = x_objRequestModel.NewPassword.PasswdIsValid();
            if (bPasswdIsValid == false)
            {
                // Get error
                objResponseModel = ResponseManager.GetErrorResp(ErrorConstant.NEW_PASSWD_INVALID, ErrorCode.NEW_PASSWD_INVALID);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.NEW_PASSWD_INVALID}", SOURCE);

                return objResponseModel;
            }

            bPasswdIsValid = ComparePasswd(x_objRequestModel.NewPassword, x_objRequestModel.RePassword);
            if (bPasswdIsValid == false)
            {
                // Get error
                objResponseModel = ResponseManager.GetErrorResp(ErrorConstant.NEW_PASSWD_NOT_MATCH_REPASSWD, ErrorCode.NEW_PASSWD_NOT_MATCH_REPASSWD);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.NEW_PASSWD_NOT_MATCH_REPASSWD}", SOURCE);

                return objResponseModel;
            }

            // Get account
            objAccount = await GetAccount(x_strAccountId);
            if ((objAccount == null) || (objAccount.IsLocked == true))
            {
                // Get error
                objResponseModel = ResponseManager.GetErrorResp(ErrorConstant.ACCOUNT_IS_LOCKER, ErrorCode.ACCOUNT_IS_LOCKER);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.ACCOUNT_IS_LOCKER}", SOURCE);

                return objResponseModel;
            }

            // Check old passwd
            // Get hash key
            strKey = HashKeyManager.GetPasswdHashKey(objAccount.Id);
            if (string.IsNullOrWhiteSpace(strKey) == true)
            {
                // Get error
                objResponseModel = ResponseManager.GetErrorResp(ErrorConstant.OLD_PASSWD_INVALID, ErrorCode.OLD_PASSWD_INVALID);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.OLD_PASSWD_INVALID}", SOURCE);

                return objResponseModel;
            }

            // Check passwd
            bPasswdIsValid = DTUSVCv3Aes.VerifyPassword(strKey, x_objRequestModel.OldPassword, objAccount.Password);
            if (bPasswdIsValid == false)
            {
                // Get error
                objResponseModel = ResponseManager.GetErrorResp(ErrorConstant.OLD_PASSWD_INVALID, ErrorCode.OLD_PASSWD_INVALID);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.OLD_PASSWD_INVALID}", SOURCE);

                return objResponseModel;
            }
            // Update data to db
            strNewKey = HashKeyManager.GetNewPasswdHashKey(objAccount.Id);
            strNewPasswd = DTUSVCv3Aes.HashPasswordWithSalt(x_objRequestModel.NewPassword, strNewKey);
            objAccount.Password = strNewPasswd;
            objAccount.IsDefaultPassword = false;
            bPasswdIsValid = await AccountDbManager.UpdateAccount(objAccount);
            // Resest passwd hash key
            if (bPasswdIsValid == false)
            {
                HashKeyManager.AddPasswdHashKey(x_strAccountId, strKey);
                // Get error
                objResponseModel = ResponseManager.GetErrorResp(ErrorConstant.CHANGE_PASSWD_FAILED, ErrorCode.CHANGE_PASSWD_FAILD);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.CHANGE_PASSWD_FAILED}", SOURCE);

                return objResponseModel;
            }

            objResponseModel = ResponseManager.GetSuccsessResp(SuccsessConstant.CHANGE_PASSWD_SUCCSESS);
            // Logout account with out token
            strToken = x_objContext.GetTokenByHttpContext();
            LoginData.LogoutWithOutToken(strToken, objAccount.Id);
            return objResponseModel;
        }

        /// <summary>
        /// Check For Existing Tokens
        /// </summary>
        /// <param name="x_objToken"></param>
        /// <returns></returns>
        public ResponseModel CheckForExistingTokens(string x_objToken)
        {
            ForgotPasswdModel objForgotPasswdModel;
            ResponseModel objResponseModel;
            DateTime objDTNow;
            TimeSpan objTimeSpan;

            objForgotPasswdModel = ForgotPasswdManager.GetForgotPasswd(x_objToken);
            if (objForgotPasswdModel == null)
            {
                // Get error
                objResponseModel = ResponseManager.GetErrorResp(ErrorConstant.EXPIRED_PASSWORD_RECOVERY_PATH, ErrorCode.EXPIRED_PASSWORD_RECOVERY_PATH);

                return objResponseModel;
            }

            objDTNow = DateTimeHelper.GetCurrentDateTimeVN();
            objTimeSpan = objForgotPasswdModel.DeadLine.Subtract(objDTNow);
            if (objTimeSpan.TotalSeconds <= 0)
            {
                ForgotPasswdManager.RemoveToken(objForgotPasswdModel.AccountId);
                // Get error
                objResponseModel = ResponseManager.GetErrorResp(ErrorConstant.EXPIRED_PASSWORD_RECOVERY_PATH, ErrorCode.EXPIRED_PASSWORD_RECOVERY_PATH);

                return objResponseModel;
            }

            objResponseModel = ResponseManager.GetSuccsessResp(SuccsessConstant.PASSWORD_RECOVERY_PATH_IS_STILL_ACTIVE);
            return objResponseModel;
        }

        /// <summary>
        /// Check Login
        /// </summary>
        /// <param name="x_strAccountId"></param>
        /// <param name="x_objContext"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public async Task<ResponseModel> CheckLogin(string x_strAccountId, HttpContext x_objContext)
        {
            string strClientIp;
            Accounts objAccount;
            ResponseModel objResponseModel;

            //-------------------------------------
            //Get Client IP
            //-------------------------------------
            strClientIp = x_objContext.GetClientIPAddress();
            //-------------------------------------
            //Get account
            //-------------------------------------
            objAccount = await GetAccount(x_strAccountId);
            if (objAccount == null || objAccount.IsLocked == true)
            {
                // Get error
                objResponseModel = ResponseManager.GetErrorResp(ErrorConstant.ACCOUNT_IS_LOCKER, ErrorCode.ACCOUNT_IS_LOCKER);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.ACCOUNT_IS_LOCKER}", SOURCE);

                return objResponseModel;
            }

            objResponseModel = ResponseManager.GetSuccsessResp(SuccsessConstant.LOGIN_STILL_VALID);
            return objResponseModel;
        }

        /// <summary>
        /// Forgot Password
        /// </summary>
        /// <param name="x_strStudentId"></param>
        /// <returns></returns>
        public async Task<ResponseModel> ForgotPassword(ForgotPasswdRequesModel x_objRequestModel, HttpContext x_objContext)
        {
            const string FORMAT_DATE = "HH:mm:ss dd/MM/yyyy";
            string strClientIp;
            string strEmailTempFilePath;
            string strEmailContent;
            string strEmailSubject;
            string strResetPath;
            string strEmail;
            SendEmailModel objSendEmailModel;
            Accounts objAccount;
            ResponseModel objResponseModel;
            ForgotPasswdModel objForgotPasswdModel;
            MemberDecryptModel objMemberDecryptModel;
            CaptchaModel objCaptchaModel;

            strClientIp = x_objContext.GetClientIPAddress();
            if (x_objRequestModel == null)
            {
                // Get error
                objResponseModel = ResponseManager.GetErrorResp(ErrorConstant.MEMBER_INFO_IS_NULL, ErrorCode.MEMBER_INFO_IS_NULL);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.MEMBER_INFO_IS_NULL}", SOURCE);

                return objResponseModel;
            }

            // Check captcha
            if (string.IsNullOrWhiteSpace(x_objRequestModel.Token) == true ||
                string.IsNullOrWhiteSpace(x_objRequestModel.CaptchaValue) == true)
            {
                // Get error
                objResponseModel = ResponseManager.GetErrorResp(ErrorConstant.CAPTCHA_INVALID, ErrorCode.CAPTCHA_INVALID);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.CAPTCHA_INVALID}", SOURCE);

                return objResponseModel;
            }
            objCaptchaModel = ForgotPasswdManager.GetCaptcha(x_objRequestModel.Token);
            if ((objCaptchaModel == null) || (x_objRequestModel.CaptchaValue.Equals(objCaptchaModel.CaptchaCode) == false))
            {
                // Get error
                objResponseModel = ResponseManager.GetErrorResp(ErrorConstant.CAPTCHA_INVALID, ErrorCode.CAPTCHA_INVALID);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.CAPTCHA_INVALID}", SOURCE);

                return objResponseModel;
            }

            // Get member info
            objMemberDecryptModel = MemberInfoManager.GetMemberExistById(x_objRequestModel.StudenId);
            if ((objMemberDecryptModel == null) || (objMemberDecryptModel.IsMember == false))
            {
                // Get error
                objResponseModel = ResponseManager.GetErrorResp(ErrorConstant.MEMBER_INFO_IS_NULL, ErrorCode.MEMBER_INFO_IS_NULL);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.MEMBER_INFO_IS_NULL}", SOURCE);

                return objResponseModel;
            }

            // Get account
            objAccount = await GetAccount(objMemberDecryptModel.Id, true);
            if ((objAccount == null) || (objAccount.IsLocked == true))
            {
                // Get error
                objResponseModel = ResponseManager.GetErrorResp(ErrorConstant.ACCOUNT_IS_LOCKER, ErrorCode.ACCOUNT_IS_LOCKER);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp,
                    objMemberDecryptModel.GetMemberFullName(), $"{SOURCE}: {ErrorConstant.ACCOUNT_IS_LOCKER}", SOURCE);

                return objResponseModel;
            }

            // Set forgot pass
            objForgotPasswdModel = ForgotPasswdManager.SetForgotPasswd(objAccount.Id);
            strResetPath = string.Format(SysFolder.RESET_PASSWD_PATH, objForgotPasswdModel.Token);
            // Get email path
            strEmailTempFilePath = FileHelper.GetFilePath(SysFolder.EMAIL_TEMP_FOLDER, SysFile.FORGOT_PASSWD_TEMP, true);
            strEmailContent = FileHelper.ReadTextFile(strEmailTempFilePath);
            // Repalce data
            strEmailContent = strEmailContent.Replace(EmailReplaceText.STUDENT_ID, objMemberDecryptModel.StudentId)
                                             .Replace(EmailReplaceText.MEMBER_FULL_NAME, objMemberDecryptModel.GetMemberFullName())
                                             .Replace(EmailReplaceText.RESET_PASSWD_PATH, strResetPath)
                                             .Replace(EmailReplaceText.END_DATETIME, objForgotPasswdModel.DeadLine.ToString(FORMAT_DATE));
            strEmailSubject = string.Format(EmailSubject.FORGOT_PASSWD, objForgotPasswdModel.CreateDate.ToString(FORMAT_DATE));
            objSendEmailModel = new SendEmailModel
            {
                EmailBody = strEmailContent,
                EmailRecipient = objMemberDecryptModel.Email,
                Subject = strEmailSubject,
                EmailType = EmailType.RESET_PASSWD,
                IsSendCC = false
            };
            // Enqueue
            EmailHelper.Instant.ResetPasswdQueue.Enqueue(objSendEmailModel);

            ForgotPasswdManager.DeleteCaptcha(objCaptchaModel);
            strEmail = GetHashEmail(objMemberDecryptModel.Email);
            objResponseModel = ResponseManager.GetSuccsessResp(string.Format(SuccsessConstant.SEND_RESET_PASSWD_SUCCSESS, strEmail));
            return objResponseModel;
        }

        /// <summary>
        /// Get Account By Id
        /// </summary>
        public Accounts GetAccountById(string x_strAccountId)
        {
            Accounts objAccount;
            Task objTask;

            objAccount = null;
            objTask = Task.Run(async () =>
            {
                objAccount = await GetAccount(x_strAccountId);

            });
            objTask.Wait();

            return objAccount;
        }

        /// <summary>
        /// Get List Account
        /// </summary>
        /// <param name="x_strAccountId"></param>
        /// <param name="x_objContext"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public async Task<ResponseModel> GetListAccount(string x_strAccountId, HttpContext x_objContext)
        {
            string strClientIp;
            string strAvatarPath;
            List<GetListAccountResponseModel> lstAccountsInfo;
            GetListAccountResponseModel objAccountInfo;
            MemberDecryptModel objMemberInfo;
            List<Accounts> lstAccount;
            Accounts objAccount;
            Roles objRole;
            ResponseModel objResponseModel;

            //-------------------------------------
            //Get Client IP
            //-------------------------------------
            strClientIp = x_objContext.GetClientIPAddress();

            //-------------------------------------
            //Get Admin account
            //-------------------------------------
            objAccount = await GetAccount(x_strAccountId);
            if (objAccount == null || objAccount.IsLocked == true)
            {
                // Get error
                objResponseModel = ResponseManager.GetErrorResp(ErrorConstant.ACCOUNT_IS_LOCKER, ErrorCode.ACCOUNT_IS_LOCKER);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.ACCOUNT_IS_LOCKER}", SOURCE);

                return objResponseModel;
            }
            //-------------------------------------
            //Get Admin Role
            //-------------------------------------
            objRole = await RolesDbManager.GetRoleById(objAccount.RoleId);
            if (objRole == null || objRole.Permissions.Contains((int)DTUSVCPermission.GET_ALL_ACCOUNT) == false)
            {
                // Get error
                objResponseModel = ResponseManager.GetErrorResp(ErrorConstant.AUTHORITY_INSUFFICIENT, ErrorCode.AUTHORITY_INSUFFICIENT);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.AUTHORITY_INSUFFICIENT}", SOURCE);

                return objResponseModel;
            }

            //-------------------------------------
            //Get All Account from database
            //-------------------------------------
            lstAccount = await AccountDbManager.GetListAccount();
            if (lstAccount == null)
            {
                // Get error
                objResponseModel = ResponseManager.GetErrorResp(ErrorConstant.GET_ALL_ACCOUNT_FAILED, ErrorCode.GET_ALL_ACCOUNT_FAILED);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.GET_ALL_ACCOUNT_FAILED}", SOURCE);

                return objResponseModel;
            }
            //-------------------------------------
            //Create List Account response
            //-------------------------------------
            lstAccountsInfo = new List<GetListAccountResponseModel>();
            foreach (Accounts objTempAccount in lstAccount)
            {
                objMemberInfo = MemberInfoManager.GetMemberById(objTempAccount.MemberId);
                if (objMemberInfo == null || objMemberInfo.IsMember == false)
                {
                    continue;
                }

                strAvatarPath = objMemberInfo.AvatarPath;
                objAccountInfo = new GetListAccountResponseModel();
                objAccountInfo.AccountId = objTempAccount.Id;
                objAccountInfo.AvatarPath = strAvatarPath;
                objAccountInfo.FacultyName = objMemberInfo.Faculty.FacultyName;
                objAccountInfo.FirstName = objMemberInfo.FirstName;
                objAccountInfo.IsLocked = objTempAccount.IsLocked;
                objAccountInfo.LastName = objMemberInfo.LastName;
                objAccountInfo.MemberId = objMemberInfo.Id;
                objAccountInfo.PhoneNumber = objMemberInfo.PhoneNumber;
                objAccountInfo.StudentId = objMemberInfo.StudentId;
                objAccountInfo.Username = objTempAccount.Username;

                lstAccountsInfo.Add(objAccountInfo);
            }
            // Sort
            if (lstAccountsInfo.Count > 0)
            {
                lstAccountsInfo = lstAccountsInfo.OrderBy(obj => obj.LastName).ThenBy(obj => obj.FirstName).ThenBy(obj => obj.StudentId).ToList();
            }

            objResponseModel = ResponseManager.GetSuccsessResp(SuccsessConstant.GET_ALL_ACCOUNT_SUCCSESS, lstAccountsInfo, lstAccountsInfo.Count);
            return objResponseModel;
        }

        /// <summary>
        /// Lock Or Unlock Account
        /// </summary>
        /// <param name="x_strMemberId"></param>
        /// <param name="x_strAccountId"></param>
        /// <param name="x_objContext"></param>
        /// <returns></returns>
        public async Task<ResponseModel> LockOrUnlockAccount(string x_strMemberId, string x_strAccountId, HttpContext x_objContext)
        {
            bool bUpdateSuccsess;
            string strClientIp;
            MemberDecryptModel objMemberInfo;
            Accounts objAccount;
            Accounts objAdminAccount;
            Roles objAdminRole;
            ResponseModel objResponseModel;

            strClientIp = x_objContext.GetClientIPAddress();

            //Check role
            objAdminAccount = await GetAccount(x_strAccountId);
            if (objAdminAccount == null || objAdminAccount.IsLocked == true)
            {
                // Get error
                objResponseModel = ResponseManager.GetErrorResp(ErrorConstant.ACCOUNT_IS_LOCKER, ErrorCode.ACCOUNT_IS_LOCKER);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.ACCOUNT_IS_LOCKER}", SOURCE);

                return objResponseModel;
            }
            objAdminRole = await RolesDbManager.GetRoleById(objAdminAccount.RoleId);
            if (objAdminRole == null || objAdminRole.Permissions.Contains((int)DTUSVCPermission.LOCK_UNLOCK_ACCOUNT) == false)
            {
                // Get error
                objResponseModel = ResponseManager.GetErrorResp(ErrorConstant.AUTHORITY_INSUFFICIENT, ErrorCode.AUTHORITY_INSUFFICIENT);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.AUTHORITY_INSUFFICIENT}", SOURCE);

                return objResponseModel;
            }

            objMemberInfo = MemberInfoManager.GetMemberById(x_strMemberId);
            if (objMemberInfo == null || objMemberInfo.IsMember == false)
            {
                // Get error
                objResponseModel = ResponseManager.GetErrorResp(ErrorConstant.MEMBER_INFO_IS_NULL, ErrorCode.MEMBER_INFO_IS_NULL);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.MEMBER_INFO_IS_NULL}", SOURCE);

                return objResponseModel;
            }
            // Get account info
            objAccount = await GetAccount(objMemberInfo.Id, true);
            if (objAccount == null)
            {
                // Get error
                objResponseModel = ResponseManager.GetErrorResp(ErrorConstant.ACCOUNT_IS_NULL, ErrorCode.ACCOUNT_IS_NULL);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.ACCOUNT_IS_NULL}", SOURCE);

                return objResponseModel;
            }
            // --------------------------------------------------
            // set lock/unlock
            // --------------------------------------------------
            objAccount.IsLocked = objAccount.IsLocked == false;

            bUpdateSuccsess = await AccountDbManager.UpdateAccount(objAccount);
            if (bUpdateSuccsess == false)
            {
                // Get error
                objResponseModel = ResponseManager.GetErrorResp(ErrorConstant.UPDATE_ACCOUNT_FAILED, ErrorCode.UPDATE_ACCOUNT_FAILED);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.UPDATE_ACCOUNT_FAILED}", SOURCE);

                return objResponseModel;
            }

            if (objAccount.IsLocked == true)
            {
                objResponseModel = ResponseManager.GetSuccsessResp(SuccsessConstant.LOCK_SUCCSESS);
                // Logout all
                LoginData.LogoutById(objAccount.Id);
            }
            else
            {
                objResponseModel = ResponseManager.GetSuccsessResp(SuccsessConstant.UNLOCK_SUCCSESS);
            }
            return objResponseModel;
        }

        /// <summary>
        /// Login
        /// </summary>
        public async Task<ResponseModel> Login(LoginRequestModel x_objRequestModel, HttpContext x_objContext)
        {
            ResponseModel objResponse;
            Accounts objAccount;
            MemberDecryptModel objMemberInfo;
            Roles objRole;
            LoginInfoModel objLoginInfoModel;
            LoginResponseModel objLoginResponseModel;
            bool bLoginSuccsess;
            string strKey;
            string strAccsesToken;
            string strIdAddress;

            try
            {
                Thread.Sleep(3000);
                if (x_objRequestModel == null)
                {
                    objResponse = ResponseManager.GetErrorResp(ErrorConstant.USER_PASSWD_INVALID, ErrorCode.LOGIN_FAILD);
                    return objResponse;
                }

                // get account
                objAccount = await GetAccount(x_objRequestModel.Username.ToLower());
                if (objAccount == null)
                {
                    objResponse = ResponseManager.GetErrorResp(ErrorConstant.USER_PASSWD_INVALID, ErrorCode.LOGIN_FAILD);
                    return objResponse;
                }

                if (objAccount.IsLocked == true)
                {
                    // Get error
                    objResponse = ResponseManager.GetErrorResp(ErrorConstant.ACCOUNT_IS_LOCKER, ErrorCode.ACCOUNT_IS_LOCKER);
                    return objResponse;
                }

                // Get hash key
                strKey = HashKeyManager.GetPasswdHashKey(objAccount.Id);
                if (string.IsNullOrWhiteSpace(strKey) == true)
                {
                    objResponse = ResponseManager.GetErrorResp(ErrorConstant.USER_PASSWD_INVALID, ErrorCode.LOGIN_FAILD);
                    return objResponse;
                }

                // Check passwd
                bLoginSuccsess = DTUSVCv3Aes.VerifyPassword(strKey, x_objRequestModel.Password, objAccount.Password);
                if (bLoginSuccsess == false)
                {
                    objResponse = ResponseManager.GetErrorResp(ErrorConstant.USER_PASSWD_INVALID, ErrorCode.LOGIN_FAILD);
                    return objResponse;
                }

                // get member info
                objMemberInfo = MemberInfoManager.GetMemberById(objAccount.MemberId);
                if (objMemberInfo == null)
                {
                    objResponse = ResponseManager.GetErrorResp(ErrorConstant.USER_PASSWD_INVALID, ErrorCode.LOGIN_FAILD);
                    return objResponse;
                }
                if (objMemberInfo.IsMember == false)
                {
                    // Get error
                    objResponse = ResponseManager.GetErrorResp(ErrorConstant.MEMBER_INFO_IS_NULL, ErrorCode.MEMBER_INFO_IS_NULL);

                    return objResponse;
                }

                // get Role 
                objRole = await RolesDbManager.GetRoleById(objAccount.RoleId);
                if (objRole == null)
                {
                    objResponse = ResponseManager.GetErrorResp(ErrorConstant.USER_PASSWD_INVALID, ErrorCode.LOGIN_FAILD);
                    return objResponse;
                }

                // Create token
                strAccsesToken = CreateToken(objAccount.Id, objMemberInfo.StudentId, objRole.Id);
                // get ip
                strIdAddress = x_objContext.GetClientIPAddress(false);
                // Set login
                objLoginInfoModel = LoginData.SetLogin(objRole.Id, objRole.Permissions, objMemberInfo.StudentId, objMemberInfo.Id,
                    strAccsesToken, strIdAddress, objAccount.Id);

                objLoginResponseModel = new LoginResponseModel();
                objLoginResponseModel.AvatarPath = objMemberInfo.AvatarPath;
                objLoginResponseModel.AccessToken = strAccsesToken;
                objLoginResponseModel.AccountId = objAccount.Id;
                objLoginResponseModel.FirstName = objMemberInfo.FirstName;
                objLoginResponseModel.ViewFileToken = objLoginInfoModel.ViewFileToken;
                objLoginResponseModel.LastName = objMemberInfo.LastName;
                objLoginResponseModel.RefreshToken = objLoginInfoModel.RefreshToken;
                objLoginResponseModel.Role = objRole;
                objLoginResponseModel.IsDefaultPasswd = objAccount.IsDefaultPassword;

                objResponse = ResponseManager.GetSuccsessResp(SuccsessConstant.LOGIN_SUCCSESS, objLoginResponseModel);
                return objResponse;
            }
            catch
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.USER_PASSWD_INVALID, ErrorCode.LOGIN_FAILD);
                return objResponse;
            }
        }

        /// <summary>
        /// Logout
        /// </summary>
        public ResponseModel Logout(HttpContext x_objContext)
        {
            bool bLogoutSuccsess;
            ResponseModel objResponse;

            bLogoutSuccsess = LogoutBase(x_objContext);
            if (bLogoutSuccsess == false)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.LOGOUT_FAILED, ErrorCode.LOGOUT_FAILED);
                return objResponse;
            }

            objResponse = ResponseManager.GetSuccsessResp(SuccsessConstant.LOGOUT_SUCCSESS);
            return objResponse;
        }

        /// <summary>
        /// Refresh Access Token
        /// </summary>
        public ResponseModel RefreshAccessToken(RefeshTokenRequestModel x_objRefreshToken, HttpContext x_objContext)
        {
            bool bIsMember;
            bool bRefeshSuccsess;
            string strAccsesToken;
            string strClientIp;
            LoginInfoModel objLoginInfo;
            ResponseModel objResponseModel;
            RefreshTokenResponse objRefreshTokenResponse;

            strClientIp = x_objContext.GetClientIPAddress();
            if (x_objRefreshToken == null)
            {
                // Get error
                objResponseModel = ResponseManager.GetErrorResp(ErrorConstant.NOT_LOGIN, ErrorCode.NOT_LOGIN);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.NOT_LOGIN}", SOURCE);

                return objResponseModel;
            }
            objLoginInfo = LoginData.GetLoginInfo(x_objRefreshToken.RefeshToken, x_objContext);
            if (objLoginInfo == null)
            {
                // Get error
                objResponseModel = ResponseManager.GetErrorResp(ErrorConstant.NOT_LOGIN, ErrorCode.NOT_LOGIN);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.NOT_LOGIN}", SOURCE);

                return objResponseModel;
            }

            // Check is member
            bIsMember = MemberInfoManager.CheckIsMember(objLoginInfo.MemberId);
            if (bIsMember == false)
            {
                LoginData.Logout(x_objRefreshToken.RefeshToken);
                // Get error
                objResponseModel = ResponseManager.GetErrorResp(ErrorConstant.NOT_LOGIN, ErrorCode.NOT_LOGIN);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.NOT_LOGIN}", SOURCE);

                return objResponseModel;
            }

            // Create token
            strAccsesToken = CreateToken(objLoginInfo.UserId, objLoginInfo.StudentId, objLoginInfo.RoleDbId);
            bRefeshSuccsess = LoginData.ReferhToken(strAccsesToken, objLoginInfo);
            if (bRefeshSuccsess == false)
            {
                LoginData.Logout(x_objRefreshToken.RefeshToken);
                // Get error
                objResponseModel = ResponseManager.GetErrorResp(ErrorConstant.NOT_LOGIN, ErrorCode.NOT_LOGIN);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.NOT_LOGIN}", SOURCE);

                return objResponseModel;
            }

            objRefreshTokenResponse = new RefreshTokenResponse
            {
                AccessToken = strAccsesToken,
                ViewFileToken = objLoginInfo.ViewFileToken
            };
            objResponseModel = ResponseManager.GetSuccsessResp(SuccsessConstant.REFESH_TOKEN_SUCCSESS, objRefreshTokenResponse);
            return objResponseModel;
        }

        /// <summary>
        /// Reset Password
        /// </summary>
        /// <param name="x_objRequestModel"></param>
        /// <param name="x_objContext"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public async Task<ResponseModel> ResetPassword(ForgotChangePasswdRequestModel x_objRequestModel, HttpContext x_objContext)
        {
            bool bPasswdIsValid;
            string strClientIp;
            string strAccountHashKey;
            string strOldAccountHashKey;
            string strNewPasswdHash;
            ResponseModel objResponseModel;
            ForgotPasswdModel objForgotModel;
            Accounts objAccount;
            DateTime objDateTimeNow;

            strClientIp = x_objContext.GetClientIPAddress();
            if (x_objRequestModel == null)
            {
                // Get error
                objResponseModel = ResponseManager.GetErrorResp(ErrorConstant.NOT_LOGIN, ErrorCode.NOT_LOGIN);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.NOT_LOGIN}", SOURCE);

                return objResponseModel;
            }

            // -----------------------------
            // Validate data
            // -----------------------------
            bPasswdIsValid = x_objRequestModel.NewPassword.PasswdIsValid();
            if (bPasswdIsValid == false)
            {
                // Get error
                objResponseModel = ResponseManager.GetErrorResp(ErrorConstant.NEW_PASSWD_INVALID, ErrorCode.NEW_PASSWD_INVALID);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.NEW_PASSWD_INVALID}", SOURCE);

                return objResponseModel;
            }

            bPasswdIsValid = ComparePasswd(x_objRequestModel.NewPassword, x_objRequestModel.PasswordConfirmation);
            if (bPasswdIsValid == false)
            {
                // Get error
                objResponseModel = ResponseManager.GetErrorResp(ErrorConstant.NEW_PASSWD_NOT_MATCH_REPASSWD, ErrorCode.NEW_PASSWD_NOT_MATCH_REPASSWD);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.NEW_PASSWD_NOT_MATCH_REPASSWD}", SOURCE);

                return objResponseModel;
            }

            // Check token
            objForgotModel = ForgotPasswdManager.GetForgotPasswd(x_objRequestModel.Token);
            objDateTimeNow = DateTimeHelper.GetCurrentDateTimeVN();
            if ((objForgotModel == null) || (objForgotModel.DeadLine < objDateTimeNow))
            {
                // Get error
                objResponseModel = ResponseManager.GetErrorResp(ErrorConstant.FORGOT_PASSWD_TOKEN_INVALID, ErrorCode.FORGOT_PASSWD_TOKEN_INVALID);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.FORGOT_PASSWD_TOKEN_INVALID}", SOURCE);

                return objResponseModel;
            }

            objAccount = await GetAccount(objForgotModel.AccountId);
            if ((objAccount == null) || (objAccount.IsLocked == true))
            {
                // Get error
                objResponseModel = ResponseManager.GetErrorResp(ErrorConstant.ACCOUNT_IS_LOCKER, ErrorCode.ACCOUNT_IS_LOCKER);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.ACCOUNT_IS_LOCKER}", SOURCE);

                return objResponseModel;
            }

            strOldAccountHashKey = HashKeyManager.GetPasswdHashKey(objForgotModel.AccountId);
            strAccountHashKey = HashKeyManager.GetNewPasswdHashKey(objForgotModel.AccountId);
            // hash passwd
            strNewPasswdHash = DTUSVCv3Aes.HashPasswordWithSalt(x_objRequestModel.NewPassword, strAccountHashKey);
            objAccount.IsDefaultPassword = false;
            objAccount.Password = strNewPasswdHash;
            // Update to database
            bPasswdIsValid = await AccountDbManager.UpdateAccount(objAccount);
            if (bPasswdIsValid == false)
            {
                HashKeyManager.AddPasswdHashKey(objAccount.Id, strOldAccountHashKey);
                // Get error
                objResponseModel = ResponseManager.GetErrorResp(ErrorConstant.CHANGE_PASSWD_FAILED, ErrorCode.CHANGE_PASSWD_FAILD);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.CHANGE_PASSWD_FAILED}", SOURCE);

                return objResponseModel;
            }

            objResponseModel = ResponseManager.GetSuccsessResp(SuccsessConstant.CHANGE_PASSWD_SUCCSESS);
            // Delete token
            ForgotPasswdManager.RemoveToken(objAccount.Id);
            // Logout all
            LoginData.LogoutById(objAccount.Id);
            return objResponseModel;
        }

        /// <summary>
        /// Reset Password By Admin
        /// </summary>
        /// <param name="x_strMemberId"></param>
        /// <param name="x_strAccountId"></param>
        /// <param name="x_objContext"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public async Task<ResponseModel> ResetPasswordByAdmin(string x_strMemberId, string x_strAccountId, HttpContext x_objContext)
        {
            bool bUpdateSuccsess;
            string strClientIp;
            string strNewPasswd;
            string strOldHashKey;
            string strNewHashKey;
            MemberDecryptModel objMemberInfo;
            Accounts objAccount;
            Accounts objAdminAccount;
            Roles objAdminRole;
            ResponseModel objResponseModel;

            strClientIp = x_objContext.GetClientIPAddress();

            //Check role
            objAdminAccount = await GetAccount(x_strAccountId);
            if (objAdminAccount == null || objAdminAccount.IsLocked == true)
            {
                // Get error
                objResponseModel = ResponseManager.GetErrorResp(ErrorConstant.ACCOUNT_IS_LOCKER, ErrorCode.ACCOUNT_IS_LOCKER);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.ACCOUNT_IS_LOCKER}", SOURCE);

                return objResponseModel;
            }
            objAdminRole = await RolesDbManager.GetRoleById(objAdminAccount.RoleId);
            if (objAdminRole == null || objAdminRole.Permissions.Contains((int)DTUSVCPermission.ADMIN_RESET_PASSWD) == false)
            {
                // Get error
                objResponseModel = ResponseManager.GetErrorResp(ErrorConstant.AUTHORITY_INSUFFICIENT, ErrorCode.AUTHORITY_INSUFFICIENT);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.AUTHORITY_INSUFFICIENT}", SOURCE);

                return objResponseModel;
            }

            objMemberInfo = MemberInfoManager.GetMemberById(x_strMemberId);
            if (objMemberInfo == null || objMemberInfo.IsMember == false)
            {
                // Get error
                objResponseModel = ResponseManager.GetErrorResp(ErrorConstant.MEMBER_INFO_IS_NULL, ErrorCode.MEMBER_INFO_IS_NULL);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.MEMBER_INFO_IS_NULL}", SOURCE);

                return objResponseModel;
            }
            // Get account info
            objAccount = await GetAccount(objMemberInfo.Id, true);
            if (objAccount == null || objAccount.IsLocked == true)
            {
                // Get error
                objResponseModel = ResponseManager.GetErrorResp(ErrorConstant.ACCOUNT_MEMBER_IS_LOCKER, ErrorCode.ACCOUNT_MEMBER_IS_LOCKER);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.ACCOUNT_MEMBER_IS_LOCKER}", SOURCE);

                return objResponseModel;
            }
            // Get new hash key
            strOldHashKey = HashKeyManager.GetPasswdHashKey(objAccount.Id);
            strNewHashKey = HashKeyManager.GetNewPasswdHashKey(objAccount.Id);
            //Hash passwd
            strNewPasswd = CreateDefaultPasswd(objMemberInfo.StudentId, objMemberInfo.BirthDay);
            strNewPasswd = DTUSVCv3Aes.HashPasswordWithSalt(strNewPasswd, strNewHashKey);
            //Update to account
            objAccount.Password = strNewPasswd;
            objAccount.IsDefaultPassword = true;

            bUpdateSuccsess = await AccountDbManager.UpdateAccount(objAccount);
            if (bUpdateSuccsess == false)
            {
                HashKeyManager.AddPasswdHashKey(objAccount.Id, strOldHashKey);
                // Get error
                objResponseModel = ResponseManager.GetErrorResp(ErrorConstant.CHANGE_PASSWD_FAILED, ErrorCode.CHANGE_PASSWD_FAILD);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.CHANGE_PASSWD_FAILED}", SOURCE);

                return objResponseModel;
            }

            objResponseModel = ResponseManager.GetSuccsessResp(SuccsessConstant.CHANGE_PASSWD_SUCCSESS);
            // Logout all
            LoginData.LogoutById(objAccount.Id);
            return objResponseModel;
        }

        /// <summary>
        /// Get Captcha Forgot
        /// </summary>
        /// <param name="x_objRequestModel"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public ResponseModel GetCaptcha(GetCaptchaRequestModel x_objRequestModel)
        {
            string strToken;
            string strFileName;
            string strFilePath;
            CaptchaModel objCaptchaModel;
            GetCaptchaResModel objGetCaptchaResModel;
            ResponseModel objResponse;

            lock (m_objGetCaptchaLock)
            {
                strToken = string.Empty;
                if (x_objRequestModel != null)
                {
                    strToken = x_objRequestModel.Token;
                }

                objCaptchaModel = ForgotPasswdManager.SetCaptcha(strToken);
                objGetCaptchaResModel = new GetCaptchaResModel();
                strFileName = Path.GetFileName(objCaptchaModel.CaptchaPath);
                strFilePath = Path.Combine(SysFolder.CAPTCHA_FOLDER, strFileName);
                objGetCaptchaResModel.CaptchaPath = strFilePath;
                objGetCaptchaResModel.Token = objCaptchaModel.Token;

                objResponse = ResponseManager.GetSuccsessResp(SuccsessConstant.GET_CAPTCHA_SUCCSESS, objGetCaptchaResModel);
                return objResponse;
            }
        }

        /// <summary>
        /// Change Role Of Member
        /// </summary>
        /// <param name="x_objRequest"></param>
        /// <param name="x_strAccountId"></param>
        /// <param name="x_objContext"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public async Task<ResponseModel> ChangeRoleOfMember(ChangeRoleOfAccountRequestModel x_objRequest, string x_strAccountId, HttpContext x_objContext)
        {
            Accounts objAccount;
            Accounts objCurrentAccount;
            MemberDecryptModel objMemberInfo;
            Roles objRole;
            ResponseModel objResponse;
            bool bResult;
            string strClientIp;

            //----------------------------------
            // Get IP Address
            //----------------------------------
            strClientIp = x_objContext.GetClientIPAddress();
            //----------------------------------
            // Validate request role
            //----------------------------------
            // Account
            objAccount = await GetAccount(x_strAccountId);
            if (objAccount == null || objAccount.IsLocked == true)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.ACCOUNT_IS_LOCKER, ErrorCode.ACCOUNT_IS_LOCKER);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.ACCOUNT_IS_LOCKER}", SOURCE);

                return objResponse;
            }
            
            // Member
            objMemberInfo = MemberInfoManager.GetMemberById(objAccount.MemberId);
            if (objMemberInfo == null)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.MEMBER_INFO_IS_NULL, ErrorCode.MEMBER_INFO_IS_NULL);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.MEMBER_INFO_IS_NULL}", SOURCE);

                return objResponse;
            }

            // Role
            objRole = await RolesDbManager.GetRoleById(objAccount.RoleId);
            if ((objRole == null) || (objRole.Permissions.Contains((int)DTUSVCPermission.CHANGE_ROLE) == false))
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.AUTHORITY_INSUFFICIENT, ErrorCode.AUTHORITY_INSUFFICIENT);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMemberInfo.GetMemberFullName(), $"{SOURCE}: {ErrorConstant.MEMBER_INFO_IS_NULL}", SOURCE);
                return objResponse;
            }

            objCurrentAccount = await GetAccount(x_strAccountId);
            if (objCurrentAccount == null || objCurrentAccount.IsLocked == true)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.ACCOUNT_MEMBER_IS_LOCKER, ErrorCode.ACCOUNT_MEMBER_IS_LOCKER);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.ACCOUNT_MEMBER_IS_LOCKER}", SOURCE);

                return objResponse;
            }
            // Role
            objRole = await RolesDbManager.GetRoleById(objCurrentAccount.RoleId);
            if (objRole != null && objRole.RoleName.EqualsIgnoreCase("System Admin") == true)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.AUTHORITY_INSUFFICIENT, ErrorCode.AUTHORITY_INSUFFICIENT);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMemberInfo.GetMemberFullName(), $"{SOURCE}: {ErrorConstant.MEMBER_INFO_IS_NULL}", SOURCE);
                return objResponse;
            }

            objRole = await RolesDbManager.GetRoleById(x_objRequest.RoleId);
            if (objRole == null)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.ROLE_IS_NULL, ErrorCode.ROLE_IS_NULL);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMemberInfo.GetMemberFullName(), $"{SOURCE}: {ErrorConstant.ROLE_IS_NULL}", SOURCE);
                return objResponse;
            }
            if (objRole.RoleName.EqualsIgnoreCase("System Admin") == true)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.AUTHORITY_INSUFFICIENT, ErrorCode.AUTHORITY_INSUFFICIENT);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMemberInfo.GetMemberFullName(), $"{SOURCE}: {ErrorConstant.MEMBER_INFO_IS_NULL}", SOURCE);
                return objResponse;
            }

            objCurrentAccount.RoleId = objRole.Id;
            bResult = await AccountDbManager.UpdateAccount(objCurrentAccount);
            if (bResult == false)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.UPDATE_ROLE_FAILED, ErrorCode.UPDATE_ROLE_FAILED);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMemberInfo.GetMemberFullName(), $"{SOURCE}: {ErrorConstant.UPDATE_ROLE_FAILED}", SOURCE);
                return objResponse;
            }

            objResponse = ResponseManager.GetSuccsessResp(SuccsessConstant.UPDATE_MEMBER_ROLE_SUCCSESS);
            return objResponse;
        }
        #endregion
    }
}
