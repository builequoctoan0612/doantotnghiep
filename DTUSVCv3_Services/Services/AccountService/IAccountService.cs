﻿using DTUSVCv3_EntityFramework;
using DTUSVCv3_Library;
using DTUSVCv3_Services.Models.AccountModel;
using Microsoft.AspNetCore.Http;

namespace DTUSVCv3_Services
{
    public interface IAccountService
    {
        /// <summary>
        /// Get Account By Id
        /// </summary>
        Accounts GetAccountById(string x_strAccountId);
        /// <summary>
        /// Login
        /// </summary>
        Task<ResponseModel> Login(LoginRequestModel x_objRequestModel, HttpContext x_objContext);
        /// <summary>
        /// Logout
        /// </summary>
        ResponseModel Logout(HttpContext x_objContext);
        /// <summary>
        /// Refresh Access Token
        /// </summary>
        ResponseModel RefreshAccessToken(RefeshTokenRequestModel x_objRefreshToke, HttpContext x_objContext);
        /// <summary>
        /// Forgot Password
        /// </summary>
        Task<ResponseModel> ForgotPassword(ForgotPasswdRequesModel x_objRequestModel, HttpContext x_objContext);
        /// <summary>
        /// Get Captcha Forgot
        /// </summary>
        ResponseModel GetCaptcha(GetCaptchaRequestModel x_objRequestModel);
        /// <summary>
        /// Check For Existing Tokens
        /// </summary>
        ResponseModel CheckForExistingTokens(string x_objToken);
        /// <summary>
        /// Reset Password
        /// </summary>
        Task<ResponseModel> ResetPassword(ForgotChangePasswdRequestModel x_objRequestModel, HttpContext x_objContext);
        /// <summary>
        /// Reset Password
        /// </summary>
        Task<ResponseModel> ResetPasswordByAdmin(string x_strMemberId, string x_strAccountId, HttpContext x_objContext);
        /// <summary>
        /// Change Password
        /// </summary>
        Task<ResponseModel> ChangePassword(ChangePasswdRequestModel x_objRequestModel, string x_strAccountId, HttpContext x_objContext);
        /// <summary>
        /// Lock Or Unlock Account
        /// </summary>
        Task<ResponseModel> LockOrUnlockAccount(string x_strMemberId, string x_strAccountId, HttpContext x_objContext);
        /// <summary>
        /// Check Login
        /// </summary>
        Task<ResponseModel> CheckLogin(string x_strAccountId, HttpContext x_objContext);
        /// <summary>
        /// Get List Account
        /// </summary>
        Task<ResponseModel> GetListAccount(string x_strAccountId, HttpContext x_objContext);
        /// <summary>
        /// Change Role Of Account
        /// </summary>
        /// <param name="x_objRequest"></param>
        /// <param name="x_strAccountId"></param>
        /// <param name="x_objContext"></param>
        /// <returns></returns>
        Task<ResponseModel> ChangeRoleOfMember(ChangeRoleOfAccountRequestModel x_objRequest, string x_strAccountId, HttpContext x_objContext);
    }
}
