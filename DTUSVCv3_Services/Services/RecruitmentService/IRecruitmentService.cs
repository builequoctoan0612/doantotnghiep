﻿using DTUSVCv3_Library;
using Microsoft.AspNetCore.Http;

namespace DTUSVCv3_Services
{
    public interface IRecruitmentService
    {
        /// <summary>
        /// Create Recruitment
        /// </summary>
        /// <param name="x_objRequestModel"></param>
        /// <param name="x_strAccountId"></param>
        /// <param name="x_objContext"></param>
        /// <returns></returns>
        Task<ResponseModel> CreateRecruitment(CreateRecruitmentRequestModel x_objRequestModel, string x_strAccountId, HttpContext x_objContext);
        /// <summary>
        /// Get Recruitment Info
        /// </summary>
        /// <param name="x_strRecruitmentId"></param>
        /// <param name="x_objContext"></param>
        /// <returns></returns>
        Task<ResponseModel> GetRecruitmentInfo(string x_strRecruitmentId, HttpContext x_objContext);
        /// <summary>
        /// Get Recruitment Valid
        /// </summary>
        /// <param name="x_strRecruitmentId"></param>
        /// <param name="x_objContext"></param>
        /// <returns></returns>
        Task<ResponseModel> GetRecruitmentValid(HttpContext x_objContext);
        /// <summary>
        /// Get Recruitment List
        /// </summary>
        /// <param name="x_strAccountId"></param>
        /// <param name="x_objContext"></param>
        /// <returns></returns>
        Task<ResponseModel> GetRecruitmentList(string x_strAccountId, HttpContext x_objContext);
        /// <summary>
        /// Update Recruitment
        /// </summary>
        /// <param name="x_objRequestModel"></param>
        /// <param name="x_strAccountId"></param>
        /// <param name="x_objContext"></param>
        /// <returns></returns>
        Task<ResponseModel> UpdateRecruitment(UpdateRecruitmentRequestModel x_objRequestModel, string x_strAccountId, HttpContext x_objContext);
        /// <summary>
        /// Delete Recruitment
        /// </summary>
        /// <param name="x_strRecruitmentId"></param>
        /// <param name="x_strAccountId"></param>
        /// <param name="x_objContext"></param>
        /// <returns></returns>
        Task<ResponseModel> DeleteRecruitment(string x_strRecruitmentId, string x_strAccountId, HttpContext x_objContext);
        /// <summary>
        /// Lock Or Unlock Recruitment
        /// </summary>
        /// <param name="x_strRecruitmentId"></param>
        /// <param name="x_strAccountId"></param>
        /// <param name="x_objContext"></param>
        /// <returns></returns>
        Task<ResponseModel> LockOrUnlockRecruitment(string x_strRecruitmentId, string x_strAccountId, HttpContext x_objContext);
        /// <summary>
        /// Set Finish Recruitment
        /// </summary>
        /// <param name="x_strRecruitmentId"></param>
        /// <param name="x_strAccountId"></param>
        /// <param name="x_objContext"></param>
        /// <returns></returns>
        Task<ResponseModel> SetFinishRecruitment(string x_strRecruitmentId, string x_strAccountId, HttpContext x_objContext);
        /// <summary>
        /// Applying For Member
        /// </summary>
        /// <param name="x_objRequestModel"></param>
        /// <param name="x_objContext"></param>
        /// <returns></returns>
        Task<ResponseModel> ApplyingForMember(CandidateInfoRequestModel x_objRequestModel, HttpContext x_objContext);
        /// <summary>
        /// Get Applying For Members
        /// </summary>
        /// <param name="x_strRecruitmentId"></param>
        /// <param name="x_strAccountId"></param>
        /// <param name="x_objContext"></param>
        /// <returns></returns>
        Task<ResponseModel> GetApplyingForMembers(string x_strRecruitmentId, string x_strAccountId, HttpContext x_objContext);
        /// <summary>
        /// Get Applying For Member Info
        /// </summary>
        /// <param name="x_strMemberId"></param>
        /// <param name="x_strRecruitmentId"></param>
        /// <param name="x_strAccountId"></param>
        /// <param name="x_objContext"></param>
        /// <returns></returns>
        Task<ResponseModel> GetApplyingMemberInfo(string x_strMemberId, string x_strRecruitmentId, string x_strAccountId, HttpContext x_objContext);
        /// <summary>
        /// Download Applying For Members
        /// </summary>
        /// <param name="x_strRecruitmentId"></param>
        /// <param name="x_strAccountId"></param>
        /// <param name="x_objContext"></param>
        /// <returns></returns>
        Task<ResponseModel> DownloadApplyingForMembers(string x_strRecruitmentId, string x_strAccountId, HttpContext x_objContext);
        /// <summary>
        /// Delete Applying For Member Info
        /// </summary>
        /// <param name="x_strMemberId"></param>
        /// <param name="x_strRecruitmentId"></param>
        /// <param name="x_strAccountId"></param>
        /// <param name="x_objContext"></param>
        /// <returns></returns>
        Task<ResponseModel> DeleteApplyingForMemberInfo(string x_strMemberId, string x_strRecruitmentId, string x_strAccountId, HttpContext x_objContext);
        /// <summary>
        /// Create Interview Room
        /// </summary>
        /// <param name="x_objRequestModel"></param>
        /// <param name="x_strAccountId"></param>
        /// <param name="x_objContext"></param>
        /// <returns></returns>
        Task<ResponseModel> CreateInterviewRoom(CreateInterviewRoomRequestModel x_objRequestModel, string x_strAccountId, HttpContext x_objContext);
        /// <summary>
        /// Delete Interview Room
        /// </summary>
        /// <param name="x_strRoomId"></param>
        /// <param name="x_strRecruitmentId"></param>
        /// <param name="x_strAccountId"></param>
        /// <param name="x_objContext"></param>
        /// <returns></returns>
        Task<ResponseModel> DeleteInterviewRoom(string x_strRoomId, string x_strRecruitmentId, string x_strAccountId, HttpContext x_objContext);
        /// <summary>
        /// Update Interview Room
        /// </summary>
        /// <param name="x_objRequestModel"></param>
        /// <param name="x_strAccountId"></param>
        /// <param name="x_objContext"></param>
        /// <returns></returns>
        Task<ResponseModel> UpdateInterviewRoom(UpdateInterviewRoomRequestModel x_objRequestModel, string x_strAccountId, HttpContext x_objContext);
        /// <summary>
        /// Add Interview To Room
        /// </summary>
        /// <param name="x_objRequestModel"></param>
        /// <param name="x_strAccountId"></param>
        /// <param name="x_objContext"></param>
        /// <returns></returns>
        Task<ResponseModel> AddInterviewToRoom(AddInterviewList x_objRequestModel, string x_strAccountId, HttpContext x_objContext);
        /// <summary>
        /// Remove Interview From Room
        /// </summary>
        /// <param name="x_strMemberIdId"></param>
        /// <param name="x_strRoomId"></param>
        /// <param name="x_strRecruitmentId"></param>
        /// <param name="x_strAccountId"></param>
        /// <param name="x_objContext"></param>
        /// <returns></returns>
        Task<ResponseModel> RemoveInterviewFromRoom(string x_strMemberIdId, string x_strRoomId, string x_strRecruitmentId, string x_strAccountId, HttpContext x_objContext);
        /// <summary>
        /// Get Room By Recruitment Id
        /// </summary>
        /// <param name="x_strRecruitmentId"></param>
        /// <param name="x_strAccountId"></param>
        /// <param name="x_objContext"></param>
        /// <returns></returns>
        Task<ResponseModel> GetRoomByRecruitmentId(string x_strRecruitmentId, string x_strAccountId, HttpContext x_objContext);
        /// <summary>
        /// Move Interview
        /// </summary>
        /// <param name="x_objRequestModel"></param>
        /// <param name="x_strAccountId"></param>
        /// <param name="x_objContext"></param>
        /// <returns></returns>
        Task<ResponseModel> MoveInterview(MoveMemberFromRoom x_objRequestModel, string x_strAccountId, HttpContext x_objContext);
        /// <summary>
        /// Send Email Interview
        /// </summary>
        /// <param name="x_strRoomId"></param>
        /// <param name="x_strRecruitmentId"></param>
        /// <param name="x_strAccountId"></param>
        /// <param name="x_objContext"></param>
        /// <returns></returns>
        Task<ResponseModel> SendEmailInterview(string x_strRoomId, string x_strRecruitmentId, string x_strAccountId, HttpContext x_objContext);
        /// <summary>
        /// Update Review
        /// </summary>
        /// <param name="x_objRequestModel"></param>
        /// <param name="x_strAccountId"></param>
        /// <param name="x_objContext"></param>
        /// <returns></returns>
        Task<ResponseModel> UpdateReview(UpdateReviewReqeusrModel x_objRequestModel, string x_strAccountId, HttpContext x_objContext);
        /// <summary>
        /// Member Approval
        /// </summary>
        /// <param name="x_objRequestModel"></param>
        /// <param name="x_strAccountId"></param>
        /// <param name="x_objContext"></param>
        /// <returns></returns>
        Task<ResponseModel> MemberApproval(MemberApprovalRequestModel x_objRequestModel, string x_strAccountId, HttpContext x_objContext);
    }
}
