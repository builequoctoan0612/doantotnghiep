﻿using DTUSVCv3_EntityFramework;
using DTUSVCv3_Library;
using DTUSVCv3_Library.LibraryModel;
using DTUSVCv3_MemoryData;
using DTUSVCv3_MemoryData.ForgotPasswData;
using DTUSVCv3_WebSocket;
using Microsoft.AspNetCore.Http;

namespace DTUSVCv3_Services
{
    public class RecruitmentService : BaseSerivce, IRecruitmentService
    {
        #region Constructor
        public RecruitmentService()
            : base()
        {
            m_objLockEvt = new object();
            m_objLockApproval = new object();
        }
        #endregion

        #region Types
        private const string SOURCE = "RecruitmentService.cs";
        #endregion

        #region Fields
        private object m_objLockEvt;
        private object m_objLockApproval;
        #endregion

        #region Methods

        /// <summary>
        /// Send Cancel Interview Email
        /// </summary>
        /// <param name="x_strRoomId"></param>
        /// <returns></returns>
        private async Task SendCancelInterviewEmail(string x_strRoomId)
        {
            List<MemberInterviewRoom> lstMemberInterView;
            MemberRegisterDecryptModel objMemberInfo;
            SendEmailModel objEmail;
            string strEmailPath;
            string strEmailTemp;
            string strEmailContent;

            await Task.Run(async () =>
            {
                if (x_strRoomId.MongoIdIsValid() == false)
                {
                    return;
                }

                lstMemberInterView = await RecruitmentDbManager.GetMemberInterviewByRoomId(x_strRoomId);
                if ((lstMemberInterView == null) || (lstMemberInterView.Count == 0))
                {
                    return;
                }
                // send email
                strEmailPath = FileHelper.GetFilePath(SysFolder.EMAIL_TEMP_FOLDER, SysFile.INTERVIEW_SCHEDULE_CANCEL, true);
                strEmailTemp = FileHelper.ReadTextFile(strEmailPath);
                foreach (MemberInterviewRoom objTemp in lstMemberInterView)
                {
                    if (objTemp.SendEmailState == (int)SendEmailType.SENT)
                    {
                        objMemberInfo = MemberRegisterMemory.Instant.GetMemberById(objTemp.MemberId);
                        if ((objMemberInfo == null) || (objMemberInfo.ApprovalInfo != null))
                        {
                            continue;
                        }
                        strEmailContent = strEmailTemp.Replace(EmailReplaceText.MEMBER_FULL_NAME, objMemberInfo.GetMemberFullName())
                                                      .Replace(EmailReplaceText.STUDENT_ID, objMemberInfo.StudentId)
                                                      .Replace(EmailReplaceText.INTERVIEW_TIME, objTemp.InterviewTime.ToString("HH:mm dd/MM/yyyy"));
                        objEmail = new SendEmailModel
                        {
                            EmailBody = strEmailContent,
                            IsSendCC = false,
                            EmailRecipient = objMemberInfo.Email,
                            EmailType = EmailType.NOTIFICATION,
                            Subject = EmailSubject.INTERVIEW_SCHEDULE_CANCEL
                        };
                        // Enqueue email
                        EmailHelper.Instant.EmailNomalQueue.Enqueue(objEmail);

                        if (objMemberInfo.InterviewInfo != null)
                        {
                            objMemberInfo.InterviewInfo.EmailSent = false;
                            await MemberRegisterMemory.UpdateMember(objMemberInfo);
                        }
                    }
                    await RecruitmentDbManager.DeleteMemberInRoom(objTemp.Id);
                }
                return;
            });
        }

        /// <summary>
        /// Convert Recruitment Db To Model
        /// </summary>
        /// <param name="x_objRecruitmentInfo"></param>
        /// <param name="x_objCreater"></param>
        /// <returns></returns>
        private RecruitmentResponseModel ConvertRecruitmentDbToModel(RecruitmentInfo x_objRecruitmentInfo, MemberDecryptModel x_objCreater)
        {
            RecruitmentResponseModel objRecruitmentRes;

            objRecruitmentRes = new RecruitmentResponseModel
            {
                Content = x_objRecruitmentInfo.Content,
                CreateName = x_objCreater.GetMemberFullName(),
                EndTime = x_objRecruitmentInfo.EndTime,
                Id = x_objRecruitmentInfo.Id,
                IsLock = x_objRecruitmentInfo.IsLock,
                IsStop = x_objRecruitmentInfo.IsStop,
                StartTime = x_objRecruitmentInfo.StartTime,
                Title = x_objRecruitmentInfo.Title
            };
            return objRecruitmentRes;
        }

        /// <summary>
        /// Send Failed Interview Result Email
        /// </summary>
        /// <param name="x_strRecruitmentId"></param>
        private async Task SendFailedInterviewResultEmail(string x_strRecruitmentId)
        {
            List<MemberRegisterDecryptModel> objMembers;
            SendEmailModel objSendEmailModel;
            NotifyData objNotifyData;
            NotifySendAllMess objNotifycationMess;
            string strEmailFilePath;
            string strEmailTemp;
            string strEmailValue;
            string strEmailSubject;

            // Get interview members
            objMembers = MemberRegisterMemory.GetListMemberByRecruitId(x_strRecruitmentId);
            if ((objMembers == null) || (objMembers.Count == 0))
            {
                return;
            }

            // Get email path
            strEmailFilePath = FileHelper.GetFilePath(SysFolder.EMAIL_TEMP_FOLDER, SysFile.FAILED_INTERVIEW_RESULT, true);
            strEmailTemp = FileHelper.ReadTextFile(strEmailFilePath);
            // Send email
            foreach (MemberRegisterDecryptModel objTemp in objMembers)
            {
                if ((objTemp == null) == (objTemp.ApprovalInfo != null))
                {
                    strEmailValue = strEmailTemp.Replace(EmailReplaceText.STUDENT_ID, objTemp.StudentId)
                                             .Replace(EmailReplaceText.MEMBER_FULL_NAME, objTemp.GetMemberFullName());
                    strEmailSubject = string.Format(EmailSubject.INTERVIEW_RESULT, objTemp.GetMemberFullName());
                    objSendEmailModel = new SendEmailModel
                    {
                        EmailBody = strEmailValue,
                        EmailRecipient = objTemp.Email,
                        Subject = strEmailSubject,
                        EmailType = EmailType.NOTIFICATION
                    };
                    // Enqueue
                    EmailHelper.Instant.EmailNomalQueue.Enqueue(objSendEmailModel);
                    // Update to Database
                    objTemp.FailedInterView = true;
                    await MemberRegisterMemory.UpdateMember(objTemp);
                    // Send notification
                    objNotifyData = new NotifyData
                    {
                        ItemId = objTemp.Id,
                        MemberRegistState = new ApprovalMember
                        {
                            IsApproved = false
                        }
                    };
                    // Enqueue
                    objNotifycationMess = new NotifySendAllMess
                    {
                        Data = objNotifyData,
                        Screen = GUIScreen.INTERVIEW,
                        MessageType = 2,
                        ActionId = objTemp.RecruitId
                    };
                    SocketAutoSendMessage.Instant.QueueSendAllMess.Enqueue(objNotifycationMess);
                }
            }
        }

        /// <summary>
        /// Candidate Validate
        /// </summary>
        /// <param name="x_objRequestModel"></param>
        /// <param name="x_strClientIp"></param>
        /// <returns></returns>
        private async Task<ResponseModel> CandidateValidate(CandidateInfoRequestModel x_objRequestModel, string x_strClientIp)
        {
            ResponseModel objResponse;
            string strBirthDay;
            bool bResult;

            if (x_objRequestModel == null)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.MEMBER_APPLYING_MODEL_IS_NULL, ErrorCode.MEMBER_APPLYING_MODEL_IS_NULL);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, x_strClientIp, string.Empty, ErrorConstant.MEMBER_APPLYING_MODEL_IS_NULL, SOURCE);
                return objResponse;
            }
            // Check full name
            if (x_objRequestModel.FullName.CheckNameIsValid() == false)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.MEMBER_FULL_NAME_INVALID, ErrorCode.MEMBER_FULL_NAME_INVALID);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, x_strClientIp, string.Empty, ErrorConstant.MEMBER_FULL_NAME_INVALID, SOURCE);
                return objResponse;
            }
            // Check birth day
            if (x_objRequestModel.BirthDay.CheckDateTimeFormat(out strBirthDay) == false)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.BIRTH_DAY_INVALID, ErrorCode.BIRTH_DAY_INVALID);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, x_strClientIp, string.Empty, ErrorConstant.BIRTH_DAY_INVALID, SOURCE);
                return objResponse;
            }
            x_objRequestModel.BirthDay = strBirthDay;
            // Check sex
            if (x_objRequestModel.Sex.ValidateSex() == false)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.SEX_INVALID, ErrorCode.SEX_INVALID);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, x_strClientIp, string.Empty, ErrorConstant.SEX_INVALID, SOURCE);
                return objResponse;
            }
            x_objRequestModel.Sex = x_objRequestModel.Sex.ToTitleCase();
            // Check Phone number
            if (x_objRequestModel.PhoneNumber.PhoneNumberIsValid() == false)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.PHONE_NUMBER_INVALID, ErrorCode.PHONE_NUMBER_INVALID);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, x_strClientIp, string.Empty, ErrorConstant.PHONE_NUMBER_INVALID, SOURCE);
                return objResponse;
            }
            // Check email
            if (x_objRequestModel.Email.EmailIsValid() == false)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.EMAIL_INVALID, ErrorCode.EMAIL_INVALID);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, x_strClientIp, string.Empty, ErrorConstant.EMAIL_INVALID, SOURCE);
                return objResponse;
            }
            bResult = MemberRegisterMemory.CheckEmailIsExist(x_objRequestModel.Email, x_objRequestModel.RecruitId);
            if (bResult == true)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.EMAIL_IS_EXIST, ErrorCode.EMAIL_IS_EXIST);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, x_strClientIp, string.Empty, ErrorConstant.EMAIL_IS_EXIST, SOURCE);
                return objResponse;
            }
            bResult = MemberInfoManager.CheckEmailExist(x_objRequestModel.Email);
            if (bResult == true)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.EMAIL_IS_EXIST, ErrorCode.EMAIL_IS_EXIST);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, x_strClientIp, string.Empty, ErrorConstant.EMAIL_IS_EXIST, SOURCE);
                return objResponse;
            }
            // Link facebook
            if (x_objRequestModel.LinkFacebook.CheckFbValid() == false)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.LINK_FACEBOOK_INVALID, ErrorCode.LINK_FACEBOOK_INVALID);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, x_strClientIp, string.Empty, ErrorConstant.LINK_FACEBOOK_INVALID, SOURCE);
                return objResponse;
            }
            // Check Faculy
            if (await CheckFacultyId(x_objRequestModel.FacultyId) == false)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.FACULTY_INVALID, ErrorCode.FACULTY_INVALID);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, x_strClientIp, string.Empty, ErrorConstant.FACULTY_INVALID, SOURCE);
                return objResponse;
            }
            // Check Class name
            if (string.IsNullOrWhiteSpace(x_objRequestModel.ClassName) == true)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.CLASS_NAME_INVALID, ErrorCode.CLASS_NAME_INVALID);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, x_strClientIp, string.Empty, ErrorConstant.CLASS_NAME_INVALID, SOURCE);
                return objResponse;
            }
            // Check Student Id
            if (x_objRequestModel.StudentId.StudentIsValid() == false)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.STUDENT_ID_INVALID, ErrorCode.STUDENT_ID_INVALID);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, x_strClientIp, string.Empty, ErrorConstant.STUDENT_ID_INVALID, SOURCE);
                return objResponse;
            }
            bResult = MemberRegisterMemory.CheckStudentIdIsExist(x_objRequestModel.StudentId, x_objRequestModel.RecruitId);
            if (bResult == true)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.STUDENT_ID_IS_EXIST, ErrorCode.STUDENT_ID_IS_EXIST);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, x_strClientIp, string.Empty, ErrorConstant.STUDENT_ID_IS_EXIST, SOURCE);
                return objResponse;
            }
            bResult = MemberInfoManager.CheckStudentIdExist(x_objRequestModel.StudentId);
            if (bResult == true)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.STUDENT_ID_IS_EXIST, ErrorCode.STUDENT_ID_IS_EXIST);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, x_strClientIp, string.Empty, ErrorConstant.STUDENT_ID_IS_EXIST, SOURCE);
                return objResponse;
            }
            // Check Reason
            if (string.IsNullOrWhiteSpace(x_objRequestModel.Reason) == true)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.REASON_INVALID, ErrorCode.REASON_INVALID);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, x_strClientIp, string.Empty, ErrorConstant.REASON_INVALID, SOURCE);
                return objResponse;
            }
            objResponse = new ResponseModel();
            return objResponse;
        }

        /// <summary>
        /// Get Manager Id Valid
        /// </summary>
        /// <param name="x_lstManagerId"></param>
        /// <param name="x_strCreaterId"></param>
        /// <returns></returns>
        private List<string> GetManagerIdValid(List<string> x_lstManagerId, string x_strCreaterId)
        {
            List<string> lstManagerId;
            List<int> lstPermissions;

            lstManagerId = new List<string>();
            lstManagerId.Add(x_strCreaterId);
            if (x_lstManagerId == null)
            {
                return lstManagerId;
            }

            foreach (string strId in x_lstManagerId)
            {
                // get permisstions
                if (TryGetPermistionByMemberId(strId, out lstPermissions) == false)
                {
                    continue;
                }
                // Check role
                if (lstPermissions.Contains((int)DTUSVCPermission.REVIEWER) == false)
                {
                    continue;
                }
                // Add to list
                lstManagerId.Add(strId);
            }
            return lstManagerId;
        }

        /// <summary>
        /// Get Valid Break Time
        /// </summary>
        /// <param name="x_strBreakTime"></param>
        /// <param name="x_objStartTime"></param>
        /// <param name="x_objEndTime"></param>
        /// <returns></returns>
        private List<BreakTime> GetValidBreakTime(List<BreakTimeRequest> x_strBreakTime, DateTime x_objStartTime, DateTime x_objEndTime)
        {
            List<BreakTimeRequest> lstBreakTime;
            List<BreakTime> lstNewBreakTime;
            DateTime objStartDate;
            DateTime objEndDate;
            DateTime objFstStartDate;
            DateTime objFstEndDate;
            int nIndex;

            if (x_strBreakTime == null || x_strBreakTime.Count == 0)
            {
                return new List<BreakTime>();
            }
            // sort
            lstBreakTime = x_strBreakTime.OrderBy(obj => obj.StartDateTime).ThenBy(obj => obj.EndDateTime).ToList();
            // Get first datetime valid
            lstNewBreakTime = new List<BreakTime>();
            objFstStartDate = DateTimeHelper.GetCurrentDateTimeVN();
            objFstEndDate = objFstStartDate;
            for (nIndex = 0; nIndex < lstBreakTime.Count; nIndex++)
            {
                // check datetime format
                if (TryParseDateTime(lstBreakTime[nIndex].StartDateTime, out objFstStartDate) == false)
                {
                    continue;
                }
                if (TryParseDateTime(lstBreakTime[nIndex].EndDateTime, out objFstEndDate) == false)
                {
                    continue;
                }
                // Check datetime valid
                if ((x_objStartTime < objFstStartDate) || (x_objEndTime > objFstEndDate) ||
                    (objFstStartDate >= objFstEndDate))
                {
                    continue;
                }
                lstNewBreakTime.Add(new BreakTime
                {
                    EndDateTime = objFstEndDate,
                    Note = lstBreakTime[nIndex].Note,
                    StartDateTime = objFstStartDate
                });
                break;
            }
            // Get all break time
            for (; nIndex < lstBreakTime.Count; nIndex++)
            {
                // check datetime format
                if (TryParseDateTime(lstBreakTime[nIndex].StartDateTime, out objStartDate) == false)
                {
                    continue;
                }
                if (TryParseDateTime(lstBreakTime[nIndex].EndDateTime, out objEndDate) == false)
                {
                    continue;
                }
                // Check datetime valid
                if ((objStartDate >= objEndDate) || (objEndDate > x_objEndTime) || (objStartDate <= objFstEndDate))
                {
                    continue;
                }
                lstNewBreakTime.Add(new BreakTime
                {
                    EndDateTime = objEndDate,
                    Note = lstBreakTime[nIndex].Note,
                    StartDateTime = objStartDate
                });
                objFstEndDate = objEndDate;
                objFstStartDate = objStartDate;
            }
            return lstNewBreakTime;
        }

        /// <summary>
        /// Member Approval
        /// </summary>
        /// <param name="x_objRequestModel"></param>
        private void MemberApproval(MemberApprovalRequestModel x_objRequestModel, MemberDecryptModel x_strApprovalId)
        {
            MemberApprovalRequestModel objRequest;
            Task objTask;

            objTask = Task.Run(async () =>
            {
                MemberRegisterDecryptModel objMember;
                Accounts objAccount;
                MemberDecryptModel objMemberInfo;
                SendEmailModel objSendEmailModel;
                NotifyData objNotifyData;
                NotifySendAllMess objNotifycationMess;
                string strNewMemberId;
                string strEmailPath;
                string strEmailBodyTemp;
                string strEmailBody;
                lock (m_objLockApproval)
                {
                    objRequest = x_objRequestModel.DeepClone();
                    if (objRequest == null || objRequest.MembersId == null || objRequest.MembersId.Count == 0)
                    {
                        return;
                    }
                }

                if ((x_objRequestModel.IsAddMessaggeUrl == true) && (string.IsNullOrWhiteSpace(x_objRequestModel.MessaggeUrl) == false))
                {
                    strEmailPath = FileHelper.GetFilePath(SysFolder.EMAIL_TEMP_FOLDER, SysFile.WELLCOME_TO_DTUSVC_FBURL_EMAIL_TEMP, true);
                }
                else
                {
                    strEmailPath = FileHelper.GetFilePath(SysFolder.EMAIL_TEMP_FOLDER, SysFile.WELLCOME_TO_DTUSVC_EMAIL_TEMP, true);
                    x_objRequestModel.MessaggeUrl = string.Empty;
                }
                strEmailBodyTemp = FileHelper.ReadTextFile(strEmailPath);

                foreach (string strMemberId in objRequest.MembersId)
                {
                    objMember = MemberRegisterMemory.GetMemberById(strMemberId);
                    if (objMember == null)
                    {
                        continue;
                    }
                    objMember.ApprovalInfo = new ApprovalDecryptInfo
                    {
                        ApprovalDate = DateTimeHelper.GetCurrentDateTimeVN(),
                        Approver = x_strApprovalId
                    };
                    strNewMemberId = await MemberRegisterMemory.MemberApproval(objMember);
                    if (string.IsNullOrWhiteSpace(strNewMemberId) == true)
                    {
                        continue;
                    }
                    // Get member info
                    objMemberInfo = MemberInfoManager.GetMemberById(strNewMemberId);
                    if (objMemberInfo == null)
                    {
                        continue;
                    }
                    // Get Account
                    objAccount = await AccountDbManager.GetAccountByMemberId(strNewMemberId);
                    if (objAccount == null)
                    {
                        continue;
                    }

                    strEmailBody = strEmailBodyTemp.Replace(EmailReplaceText.MEMBER_FULL_NAME, objMemberInfo.GetMemberFullName())
                                                   .Replace(EmailReplaceText.STUDENT_ID, objMemberInfo.StudentId)
                                                   .Replace(EmailReplaceText.USERNAME, objAccount.Username)
                                                   .Replace(EmailReplaceText.FB_MESSAGE_URL, x_objRequestModel.MessaggeUrl);
                    objSendEmailModel = new SendEmailModel
                    {
                        EmailBody = strEmailBody,
                        IsSendCC = true,
                        EmailRecipient = objMemberInfo.Email,
                        EmailType = EmailType.WELLCOME_TO_DTUSVC,
                        Subject = string.Format(EmailSubject.INTERVIEW_RESULT, objMemberInfo.GetMemberFullName())
                    };
                    EmailHelper.Instant.EmailNomalQueue.Enqueue(objSendEmailModel);
                    objNotifyData = new NotifyData
                    {
                        ItemId = objMemberInfo.Id,
                        MemberRegistState = new ApprovalMember
                        {
                            IsApproved = true,
                            Data = objMember.ApprovalInfo
                        }
                    };
                    objNotifycationMess = new NotifySendAllMess
                    {
                        Data = objNotifyData,
                        Screen = GUIScreen.INTERVIEW,
                        MessageType = 2,
                        ActionId = objRequest.RecruitId
                    };
                    SocketAutoSendMessage.Instant.QueueSendAllMess.Enqueue(objNotifycationMess);
                }
            });
        }

        /// <summary>
        /// Auto Add Member To Room
        /// </summary>
        /// <param name="x_strRoomId"></param>
        /// <param name="x_RecruitmentId"></param>
        /// <param name="x_strMemberId"></param>
        /// <param name="x_nLimit"></param>
        /// <returns></returns>
        private async Task AutoAddMemberToRoom(string x_strRoomId, string x_RecruitmentId, string x_strMemberId, int x_nLimit)
        {
            List<MembershipRegistration> objResult;
            MemberInterviewRoom objMemberInterviewRoom;
            NotifySendAllMess objMess;
            NotifyData objNotifyData;
            MemberInRoomModel objMemberInRoomModel;
            int nLimit;
            bool bResult;

            if (x_nLimit == 0)
            {
                return;
            }
            // Get member list
            objResult = await RecruitmentDbManager.GetAllMember(x_RecruitmentId);
            if (objResult == null)
            {
                return;
            }
            nLimit = x_nLimit;
            foreach (MembershipRegistration objMemberTemp in objResult)
            {
                if (nLimit == 0)
                {
                    break;
                }

                objMemberInterviewRoom = new MemberInterviewRoom
                {
                    MemberId = objMemberTemp.Id,
                    RecruitId = x_RecruitmentId,
                    RoomId = x_strRoomId,
                    CreaterId = x_strMemberId
                };

                bResult = RecruitmentDbManager.UpdateMemberInRoom(objMemberInterviewRoom);
                if (bResult == false)
                {
                    continue;
                }
                nLimit--;
                objMemberInRoomModel = await MemberRegisterMemory.UpdateMemberToInterview(objMemberInterviewRoom.RecruitId,
                                                                                            objMemberInterviewRoom.RoomId,
                                                                                            objMemberInterviewRoom.MemberId,
                                                                                            false);
                // Send notifycation to client
                objNotifyData = new NotifyData
                {
                    ItemId = objMemberTemp.Id,
                    TempData = objMemberInRoomModel
                };
                objMess = new NotifySendAllMess
                {
                    Data = objNotifyData,
                    Screen = GUIScreen.INTERVIEW,
                    ActionId = objMemberInterviewRoom.RecruitId,
                    MessageType = 1
                };
                SocketAutoSendMessage.Instant.QueueSendAllMess.Enqueue(objMess);
            }
        }

        /// <summary>
        /// Send Email Interview
        /// </summary>
        /// <param name="x_strlstInterviewRoom"></param>
        /// <param name="x_strAddress"></param>
        /// <param name="x_objInterviewMaxTime"></param>
        /// <param name="x_nRatedTime"></param>
        /// <param name="x_bInterviewType"></param>
        /// <returns></returns>
        private bool SendEmailInterview(List<MemberInterviewRoom> x_strlstInterviewRoom,
                                                    string x_strAddress,
                                                    DateTime x_objInterviewMaxTime,
                                                    int x_nRatedTime,
                                                    bool x_bInterviewType)
        {
            List<MemberInterviewRoom> lstInterviewRoom;
            MemberRegisterDecryptModel objMemberInfo;
            EmailInterviewModel objEmailInterviewModel;
            Task objTask;
            string strEmailTempPath;
            string strEmailTemp;
            string strEmailContent;

            strEmailTempPath = FileHelper.GetFilePath(SysFolder.EMAIL_TEMP_FOLDER, SysFile.INTERVIEW_EMAIL_TEMP, true);
            strEmailTemp = FileHelper.ReadTextFile(strEmailTempPath);
            if (string.IsNullOrWhiteSpace(strEmailTemp) == true)
            {
                return false;
            }
            objTask = Task.Run(async () =>
            {
                InterviewRooms objRoom;

                lstInterviewRoom = x_strlstInterviewRoom.DeepClone();
                objRoom = await RecruitmentDbManager.GetRoomById(lstInterviewRoom[0].RoomId);
                objRoom.EmailSending = true;
                await RecruitmentDbManager.UpdateInterviewRoom(objRoom);
                await MemberRegisterMemory.UpdateInterviewRoom(objRoom.RecruitId, objRoom.Id, false);
                foreach (MemberInterviewRoom objInterviewRoom in lstInterviewRoom)
                {
                    if ((objInterviewRoom.SendEmailState == (int)SendEmailType.SENT) ||
                    (objInterviewRoom.SendEmailState == (int)SendEmailType.SENDING))
                    {
                        continue;
                    }

                    objMemberInfo = MemberRegisterMemory.GetMemberById(objInterviewRoom.MemberId);
                    if ((objMemberInfo == null) || (objMemberInfo.FailedInterView == true) || (objMemberInfo.ApprovalInfo != null) ||
                    ((objMemberInfo.InterviewInfo != null) && (objMemberInfo.InterviewInfo.EmailSent == true)))
                    {
                        continue;
                    }

                    x_objInterviewMaxTime.AddMinutes(x_nRatedTime);
                    objInterviewRoom.InterviewTime = x_objInterviewMaxTime;
                    strEmailContent = strEmailTemp.Replace(EmailReplaceText.MEMBER_FULL_NAME, objMemberInfo.GetMemberFullName())
                                                  .Replace(EmailReplaceText.STUDENT_ID, objMemberInfo.StudentId)
                                                  .Replace(EmailReplaceText.INTERVIEW_FORMAT, x_bInterviewType == true ? "Online" : "Trực tiếp")
                                                  .Replace(EmailReplaceText.INTERVIEW_TIME, objInterviewRoom.InterviewTime.ToString("dd/MM/yyyy HH:mm:ss"))
                                                  .Replace(EmailReplaceText.INTERVIEW_ADDRESS, x_strAddress);
                    objEmailInterviewModel = new EmailInterviewModel
                    {
                        EmailBody = strEmailContent,
                        IsSendCC = true,
                        EmailRecipient = objMemberInfo.Email,
                        EmailType = EmailType.INTERVIEW,
                        MemberId = objMemberInfo.Id,
                        MemberInRoomId = objInterviewRoom.Id,
                        InterviewTime = x_objInterviewMaxTime,
                        Subject = "[DTUSVC] - Thư mời phỏng vấn"
                    };

                    objEmailInterviewModel.SendEmailResult += new EventHandler<SendEmailInterviewArg>(SendEmailInterviewResult_Evt);
                    EmailHelper.Instant.EmailInterviewQueue.Enqueue(objEmailInterviewModel);
                }
                objRoom.EmailSending = false;
                await RecruitmentDbManager.UpdateInterviewRoom(objRoom);
            });
            return true;
        }

        /// <summary>
        /// Send Email Interview Result Evt
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SendEmailInterviewResult_Evt(object? sender, SendEmailInterviewArg e)
        {
            Task objTask;

            objTask = Task.Run(async () =>
            {
                SendEmailInterviewArg objEvtData;
                NotifySendAllMess objNotifycationMess;
                MemberInterviewRoom objInterviewRoom;
                MemberRegisterDecryptModel objMemberInfo;
                MemberInRoomModel objMemberInRoomModel;

                lock (m_objLockEvt)
                {
                    objEvtData = e.DeepClone();
                    objMemberInfo = MemberRegisterMemory.GetMemberById(objEvtData.MemberId);
                    if (objMemberInfo != null)
                    {
                        if (objMemberInfo.InterviewInfo == null)
                        {
                            objMemberInfo.InterviewInfo = new InterviewDecryptInfo();
                        }
                        objMemberInfo.InterviewInfo.EmailSent = true;
                        objMemberInfo.InterviewInfo.InterviewAppointmentTime = objEvtData.InterviewTime;
                    }
                }
                // Update member
                await MemberRegisterMemory.UpdateMember(objMemberInfo);

                objInterviewRoom = await RecruitmentDbManager.GetMemberInterviewRoomById(objEvtData.MemberId, objEvtData.RoomId);
                if (objInterviewRoom != null)
                {
                    objInterviewRoom.SendEmailState = (int)objEvtData.EmailType;
                    objInterviewRoom.InterviewTime = objEvtData.InterviewTime;
                    RecruitmentDbManager.UpdateMemberInRoom(objInterviewRoom);
                }

                objMemberInRoomModel = await MemberRegisterMemory.UpdateMemberToInterview(objInterviewRoom.RecruitId,
                                                                        objInterviewRoom.RoomId,
                                                                        objInterviewRoom.Id,
                                                                        false);
                objNotifycationMess = new NotifySendAllMess
                {
                    Data = new NotifyData
                    {
                        TempData = objMemberInRoomModel
                    },
                    Screen = GUIScreen.INTERVIEW,
                    ActionId = objInterviewRoom.RecruitId,
                    MessageType = 2
                };
                SocketAutoSendMessage.Instant.QueueSendAllMess.Enqueue(objNotifycationMess);
            });
        }

        #region Override methods
        /// <summary>
        /// Add Interview To Room
        /// </summary>
        /// <param name="x_objRequestModel"></param>
        /// <param name="x_strAccountId"></param>
        /// <param name="x_objContext"></param>
        /// <returns></returns>
        public async Task<ResponseModel> AddInterviewToRoom(AddInterviewList x_objRequestModel, string x_strAccountId, HttpContext x_objContext)
        {
            ResponseModel objResponse;
            Accounts objAccount;
            Roles objRole;
            RecruitmentInfo objRecruitment;
            InterviewRooms objRoom;
            MemberDecryptModel objMember;
            MemberInterviewRoom objMemberInterviewRoom;
            MemberRegisterDecryptModel objMemberInterview;
            NotifySendAllMess objMess;

            MemberInRoomModel objMemberInRoomModel;
            DateTime objCurrentDate;
            Task objTask;
            Tuple<int, int> objLimit;
            int nLimit;
            string strClientIp;
            bool bResult;

            //----------------------------
            // Get IP address
            //----------------------------
            strClientIp = x_objContext.GetClientIPAddress();
            //----------------------------
            // Get login account
            //----------------------------
            objAccount = await GetAccount(x_strAccountId);
            if ((objAccount == null) || (objAccount.IsLocked == true))
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.ACCOUNT_IS_LOCKER, ErrorCode.ACCOUNT_IS_LOCKER);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, ErrorConstant.ACCOUNT_IS_LOCKER, SOURCE);
                return objResponse;
            }
            //----------------------------
            // Get login Role
            //----------------------------
            objRole = await RolesDbManager.GetRoleById(objAccount.RoleId);
            if ((objRole == null) || (objRole.Permissions.Contains((int)DTUSVCPermission.CREATE_INTERVIEW_ROOM) == false))
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.AUTHORITY_INSUFFICIENT, ErrorCode.AUTHORITY_INSUFFICIENT);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, ErrorConstant.AUTHORITY_INSUFFICIENT, SOURCE);
                return objResponse;
            }
            //----------------------------
            // Get login member
            //----------------------------
            objMember = MemberInfoManager.GetMemberById(objAccount.MemberId);
            if ((objMember == null) || (objMember.IsMember == false))
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.AUTHORITY_INSUFFICIENT, ErrorCode.AUTHORITY_INSUFFICIENT);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, ErrorConstant.AUTHORITY_INSUFFICIENT, SOURCE);
                return objResponse;
            }
            //----------------------------
            // Get recruitment
            //----------------------------
            objRecruitment = await RecruitmentDbManager.GetRecruitmentById(x_objRequestModel.RecruitId);
            if (objRecruitment == null)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.RECRUITMENT_IS_NULL, ErrorCode.RECRUITMENT_IS_NULL);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMember.GetMemberFullName(), ErrorConstant.RECRUITMENT_IS_NULL, SOURCE);
                return objResponse;
            }
            objCurrentDate = DateTimeHelper.GetCurrentDateTimeVN();
            if ((objCurrentDate < objRecruitment.EndTime) || (objRecruitment.IsStop == true))
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.RECRUITMENT_IS_EXPIRED, ErrorCode.RECRUITMENT_IS_EXPIRED);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMember.GetMemberFullName(), ErrorConstant.RECRUITMENT_IS_EXPIRED, SOURCE);
                return objResponse;
            }
            //----------------------------
            // Validate
            //----------------------------
            objRoom = await RecruitmentDbManager.GetRoomById(x_objRequestModel.RoomId);
            if (objRoom == null)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.INTERVIEW_ROOM_IS_NULL, ErrorCode.INTERVIEW_ROOM_IS_NULL);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMember.GetMemberFullName(), ErrorConstant.INTERVIEW_ROOM_IS_NULL, SOURCE);
                return objResponse;
            }
            if (objRoom.ManagerIds.Contains(objMember.Id) == false)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.YOU_DONT_ROOM_MANAGER, ErrorCode.YOU_DONT_ROOM_MANAGER);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, ErrorConstant.YOU_DONT_ROOM_MANAGER, SOURCE);
                return objResponse;
            }
            if (objRoom.EndDateTime < objCurrentDate)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.ROOM_IS_END, ErrorCode.ROOM_IS_END);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, ErrorConstant.ROOM_IS_END, SOURCE);
                return objResponse;
            }
            // Get Limit
            objLimit = await objRoom.GetMaxMemberInRoom(RecruitmentDbManager);
            if ((objLimit.Item1 != 0) && (objLimit.Item1 == objLimit.Item2))
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.MEMBER_IN_ROOM_IS_MAX, ErrorCode.MEMBER_IN_ROOM_IS_MAX);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, ErrorConstant.MEMBER_IN_ROOM_IS_MAX, SOURCE);
                return objResponse;
            }
            nLimit = objLimit.Item2 - objLimit.Item1;
            // Add from list
            if ((x_objRequestModel.InterviewerIds != null) && (x_objRequestModel.InterviewerIds.Count > 0))
            {
                foreach (string strMemberId in x_objRequestModel.InterviewerIds)
                {
                    if (nLimit == 0)
                    {
                        break;
                    }

                    if (strMemberId.MongoIdIsValid() == false)
                    {
                        continue;
                    }
                    // Check member is exist
                    objMemberInterview = MemberRegisterMemory.GetMemberById(strMemberId);
                    if ((objMemberInterview == null) || (objMemberInterview.ApprovalInfo != null))
                    {
                        continue;
                    }

                    objMemberInterviewRoom = await RecruitmentDbManager.GetMemberInterviewRoomById(strMemberId, x_objRequestModel.RoomId);
                    if (objMemberInterviewRoom != null)
                    {
                        continue;
                    }
                    objMemberInterviewRoom = new MemberInterviewRoom
                    {
                        MemberId = strMemberId,
                        RecruitId = objRecruitment.Id,
                        RoomId = objRoom.Id,
                        CreaterId = objAccount.MemberId
                    };

                    bResult = RecruitmentDbManager.UpdateMemberInRoom(objMemberInterviewRoom);
                    if (bResult == false)
                    {
                        continue;
                    }
                    // Send notifycation to client
                    objMemberInRoomModel = await MemberRegisterMemory.UpdateMemberToInterview(objMemberInterviewRoom.RecruitId,
                                                            objMemberInterviewRoom.RoomId,
                                                            objMemberInterviewRoom.Id,
                                                            false);
                    objMess = new NotifySendAllMess
                    {
                        Data = new NotifyData
                        {
                            TempData = objMemberInRoomModel
                        },
                        Screen = GUIScreen.INTERVIEW,
                        ActionId = objMemberInterviewRoom.RecruitId,
                        MessageType = 2
                    };
                    SocketAutoSendMessage.Instant.QueueSendAllMess.Enqueue(objMess);
                    nLimit--;
                }
            }
            // Return if nlimit = 0
            if (nLimit == 0)
            {
                objResponse = ResponseManager.GetSuccsessResp(SuccsessConstant.ADD_MEMBER_TO_ROOM_SUCCSESS);
                return objResponse;
            }

            // Add auto
            if (x_objRequestModel.IsAuto == true)
            {
                objTask = Task.Run(async () =>
                {
                    string strRoomId;
                    string strRecruitmentId;
                    string strMemberId;
                    int nTaskLimit;

                    strRoomId = objRoom.Id;
                    strRecruitmentId = objRecruitment.Id;
                    strMemberId = objMember.Id;
                    nTaskLimit = nLimit;

                    await AutoAddMemberToRoom(strRoomId, strRecruitmentId, strMemberId, nTaskLimit);
                });
            }

            objResponse = ResponseManager.GetSuccsessResp(SuccsessConstant.ADD_MEMBER_TO_ROOM_SUCCSESS);
            return objResponse;
        }

        /// <summary>
        /// Applying For Member
        /// </summary>
        /// <param name="x_objRequestModel"></param>
        /// <param name="x_objContext"></param>
        /// <returns></returns>
        public async Task<ResponseModel> ApplyingForMember(CandidateInfoRequestModel x_objRequestModel, HttpContext x_objContext)
        {
            ResponseModel objResponse;
            CaptchaModel objCaptchaModel;
            RecruitmentInfo objRecruitment;
            AddressInfo objAddress;
            Provinces objProvince;
            Districts objDistrict;
            Wards objWard;
            MemberRegisterDecryptModel objMemberDecrypt;
            AddressInfoModel objHometown;
            Facultys objFaculty;
            DateTime objCurrentTime;
            string strClientIp;
            string strFirstName;
            string strLastName;
            bool bUpdateSuccsess;

            //----------------------------------
            // Get IP Address
            //----------------------------------
            strClientIp = x_objContext.GetClientIPAddress();
            //----------------------------------
            // Validate Captcha
            //----------------------------------
            if (string.IsNullOrWhiteSpace(x_objRequestModel.Token) == true ||
                string.IsNullOrWhiteSpace(x_objRequestModel.Captcha) == true)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.CAPTCHA_INVALID, ErrorCode.CAPTCHA_INVALID);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.CAPTCHA_INVALID}", SOURCE);

                return objResponse;
            }
            objCaptchaModel = ForgotPasswdManager.GetCaptcha(x_objRequestModel.Token);
            if ((objCaptchaModel == null) || (x_objRequestModel.Captcha.Equals(objCaptchaModel.CaptchaCode) == false))
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.CAPTCHA_INVALID, ErrorCode.CAPTCHA_INVALID);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.CAPTCHA_INVALID}", SOURCE);

                return objResponse;
            }
            //----------------------------------
            // Validate recruit
            //----------------------------------
            objRecruitment = await RecruitmentDbManager.GetRecruitmentById(x_objRequestModel.RecruitId);
            if (objRecruitment == null)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.RECRUITMENT_IS_NULL, ErrorCode.RECRUITMENT_IS_NULL);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, ErrorConstant.RECRUITMENT_IS_NULL, SOURCE);
                return objResponse;
            }
            objCurrentTime = DateTimeHelper.GetCurrentDateTimeVN();
            if ((objRecruitment.IsStop == true) || (objRecruitment.EndTime < objCurrentTime))
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.RECRUITMENT_IS_EXPIRED, ErrorCode.RECRUITMENT_IS_EXPIRED);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, ErrorConstant.RECRUITMENT_IS_EXPIRED, SOURCE);
                return objResponse;
            }
            //----------------------------------
            // Validate member data
            //----------------------------------
            objResponse = await CandidateValidate(x_objRequestModel, strClientIp);
            if (objResponse.IsSuccsess == false)
            {
                return objResponse;
            }
            //----------------------------------
            // Validate Address
            //----------------------------------
            objResponse = CheckAddress(x_objRequestModel.ProvinceId, x_objRequestModel.DistrictId, x_objRequestModel.WardId, x_objRequestModel.SpecificAddress,
                                        out objAddress, out objProvince, out objDistrict, out objWard);
            if (objResponse.IsSuccsess == false)
            {
                return objResponse;
            }
            // Update to database
            bUpdateSuccsess = await AddressDbManager.UpdateAddress(objAddress);
            if (bUpdateSuccsess == false)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.UPDATE_ADDRESS_TO_DB_FAILED, ErrorCode.UPDATE_ADDRESS_TO_DB_FAILED);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, ErrorConstant.UPDATE_ADDRESS_TO_DB_FAILED, SOURCE);
                return objResponse;
            }
            // Create model
            objHometown = new AddressInfoModel
            {
                Id = objAddress.Id,
                District = objDistrict,
                Province = objProvince,
                SpecificAddress = x_objRequestModel.SpecificAddress,
                Ward = objWard
            };
            // Create name
            x_objRequestModel.FullName.NameSeparation(out strFirstName, out strLastName);

            if ((string.IsNullOrEmpty(strFirstName) == false) && (string.IsNullOrEmpty(strLastName) == false))
            {
                strFirstName = strFirstName.ToTitleCase();
                strLastName = strLastName.ToTitleCase();
            }
            // Get faculty
            objFaculty = await FacultyDbManager.GetFacultyById(x_objRequestModel.FacultyId);
            objMemberDecrypt = new MemberRegisterDecryptModel
            {
                BirthDay = x_objRequestModel.BirthDay,
                ClassName = x_objRequestModel.ClassName,
                Email = x_objRequestModel.Email,
                Faculty = objFaculty,
                FirstName = strFirstName,
                Hometown = objHometown,
                LastName = strLastName,
                LinkFacebook = x_objRequestModel.LinkFacebook,
                PhoneNumber = x_objRequestModel.PhoneNumber,
                Reason = x_objRequestModel.Reason,
                RecruitId = x_objRequestModel.RecruitId,
                Sex = x_objRequestModel.Sex,
                StudentId = x_objRequestModel.StudentId
            };
            bUpdateSuccsess = await MemberRegisterMemory.UpdateMember(objMemberDecrypt);
            if (bUpdateSuccsess == false)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.CREATE_MEMBER_TO_DB_FAILED, ErrorCode.CREATE_MEMBER_TO_DB_FAILED);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, ErrorConstant.CREATE_MEMBER_TO_DB_FAILED, SOURCE);
                await AddressDbManager.DeleteAddress(objAddress.Id);
                return objResponse;
            }

            objResponse = ResponseManager.GetSuccsessResp(SuccsessConstant.REGISTER_SUCCSESS);
            return objResponse;
        }

        /// <summary>
        /// Create Interview Room
        /// </summary>
        /// <param name="x_objRequestModel"></param>
        /// <param name="x_strAccountId"></param>
        /// <param name="x_objContext"></param>
        /// <returns></returns>
        public async Task<ResponseModel> CreateInterviewRoom(CreateInterviewRoomRequestModel x_objRequestModel, string x_strAccountId, HttpContext x_objContext)
        {
            Accounts objAccount;
            MemberDecryptModel objMember;
            Roles objRole;
            ResponseModel objResponse;
            RecruitmentInfo objRecruitment;
            InterviewRooms objRoom;
            InterviewRoomReponse objInterviewRoom;
            DateTime objCurrentDate;
            DateTime objStartDate;
            DateTime objEndDate;
            string strClientIp;
            bool bUpdateSuccsess;

            //----------------------------
            // Get IP address
            //----------------------------
            strClientIp = x_objContext.GetClientIPAddress();
            //----------------------------
            // Get login account
            //----------------------------
            objAccount = await GetAccount(x_strAccountId);
            if ((objAccount == null) || (objAccount.IsLocked == true))
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.ACCOUNT_IS_LOCKER, ErrorCode.ACCOUNT_IS_LOCKER);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, ErrorConstant.ACCOUNT_IS_LOCKER, SOURCE);
                return objResponse;
            }
            //----------------------------
            // Get login Role
            //----------------------------
            objRole = await RolesDbManager.GetRoleById(objAccount.RoleId);
            if ((objRole == null) || (objRole.Permissions.Contains((int)DTUSVCPermission.CREATE_INTERVIEW_ROOM) == false))
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.AUTHORITY_INSUFFICIENT, ErrorCode.AUTHORITY_INSUFFICIENT);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, ErrorConstant.AUTHORITY_INSUFFICIENT, SOURCE);
                return objResponse;
            }
            //----------------------------
            // Get login member
            //----------------------------
            objMember = MemberInfoManager.GetMemberById(objAccount.MemberId);
            if ((objMember == null) || (objMember.IsMember == false))
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.AUTHORITY_INSUFFICIENT, ErrorCode.AUTHORITY_INSUFFICIENT);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, ErrorConstant.AUTHORITY_INSUFFICIENT, SOURCE);
                return objResponse;
            }
            //----------------------------
            // Get recruitment
            //----------------------------
            objRecruitment = await RecruitmentDbManager.GetRecruitmentById(x_objRequestModel.RecruitId);
            if (objRecruitment == null)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.RECRUITMENT_IS_NULL, ErrorCode.RECRUITMENT_IS_NULL);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMember.GetMemberFullName(), ErrorConstant.RECRUITMENT_IS_NULL, SOURCE);
                return objResponse;
            }
            objCurrentDate = DateTimeHelper.GetCurrentDateTimeVN();
            if ((objCurrentDate < objRecruitment.EndTime) || (objRecruitment.IsStop == true))
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.RECRUITMENT_IS_EXPIRED, ErrorCode.RECRUITMENT_IS_EXPIRED);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMember.GetMemberFullName(), ErrorConstant.RECRUITMENT_IS_EXPIRED, SOURCE);
                return objResponse;
            }
            //----------------------------
            // Validate
            //----------------------------
            objRoom = new InterviewRooms(objMember.Id);
            // Convert Start & end time
            if (TryParseDateTime(x_objRequestModel.StartDateTime, out objStartDate) == false)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.DATETIME_FORMAT_INVALID, ErrorCode.DATETIME_FORMAT_INVALID);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMember.GetMemberFullName(), ErrorConstant.DATETIME_FORMAT_INVALID, SOURCE);
                return objResponse;
            }
            // 
            if (TryParseDateTime(x_objRequestModel.EndDateTime, out objEndDate) == false)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.DATETIME_FORMAT_INVALID, ErrorCode.DATETIME_FORMAT_INVALID);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMember.GetMemberFullName(), ErrorConstant.DATETIME_FORMAT_INVALID, SOURCE);
                return objResponse;
            }
            if (objStartDate > objEndDate)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.START_DATE_AND_END_DATE_INVALID, ErrorCode.START_DATE_AND_END_DATE_INVALID);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMember.GetMemberFullName(), ErrorConstant.START_DATE_AND_END_DATE_INVALID, SOURCE);
                return objResponse;
            }
            objRoom.StartDateTime = objStartDate;
            objRoom.EndDateTime = objEndDate;
            // Check room address
            if (string.IsNullOrWhiteSpace(x_objRequestModel.RoomAddress) == true)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.ROOM_ADDRESS_INVALID, ErrorCode.ROOM_ADDRESS_INVALID);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMember.GetMemberFullName(), ErrorConstant.ROOM_ADDRESS_INVALID, SOURCE);
                return objResponse;
            }
            objRoom.RoomAddress = x_objRequestModel.RoomAddress;
            // Check RatedTime
            if (x_objRequestModel.RatedTime <= 0)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.RATED_TIME_INVALID, ErrorCode.RATED_TIME_INVALID);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMember.GetMemberFullName(), ErrorConstant.RATED_TIME_INVALID, SOURCE);
                return objResponse;
            }
            objRoom.RatedTime = x_objRequestModel.RatedTime;
            objRoom.BreakTimes = GetValidBreakTime(x_objRequestModel.BreakTimes, objStartDate, objEndDate);
            objRoom.ManagerIds = GetManagerIdValid(x_objRequestModel.ManagerIds, objMember.Id);
            objRoom.Note = x_objRequestModel.Note;
            objRoom.RecruitId = x_objRequestModel.RecruitId;
            // Update to database
            bUpdateSuccsess = await RecruitmentDbManager.UpdateInterviewRoom(objRoom);
            if (bUpdateSuccsess == false)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.UPDATE_ROOM_FAILED, ErrorCode.UPDATE_ROOM_FAILED);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMember.GetMemberFullName(), ErrorConstant.UPDATE_ROOM_FAILED, SOURCE);
                return objResponse;
            }
            objInterviewRoom = await MemberRegisterMemory.UpdateInterviewRoom(objRoom.RecruitId, objRoom.Id, false);

            objResponse = ResponseManager.GetSuccsessResp(SuccsessConstant.UPDATE_ROOM_SUCCSESS, objInterviewRoom);
            return objResponse;
        }

        /// <summary>
        /// Create Recruitment
        /// </summary>
        /// <param name="x_objRequestModel"></param>
        /// <param name="x_strAccountId"></param>
        /// <param name="x_objContext"></param>
        /// <returns></returns>
        public async Task<ResponseModel> CreateRecruitment(CreateRecruitmentRequestModel x_objRequestModel, string x_strAccountId, HttpContext x_objContext)
        {
            RecruitmentResponseModel objRecruitmentResponse;
            RecruitmentInfo objRecruitmentInfo;
            ResponseModel objResponse;
            MemberDecryptModel objMember;
            Accounts objAccount;
            Roles objRole;
            DateTime objCurrentTime;
            DateTime objFinishTime;
            string strClientIp;
            string strContent;
            string strOutVal;
            bool bIsValid;

            //----------------------------------
            // Get IP Address
            //----------------------------------
            strClientIp = x_objContext.GetClientIPAddress();
            //----------------------------------
            // Validate request model
            //----------------------------------
            if (x_objRequestModel == null)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.CREATE_RECRUITMENT_DATA_REQUSET_NULL, ErrorCode.CREATE_RECRUITMENT_DATA_REQUSET_NULL);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, ErrorConstant.CREATE_RECRUITMENT_DATA_REQUSET_NULL, SOURCE);
                return objResponse;
            }
            if (string.IsNullOrWhiteSpace(x_objRequestModel.Title) == true)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.RECRUITMENT_TITLE_INVALID, ErrorCode.RECRUITMENT_TITLE_INVALID);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, ErrorConstant.RECRUITMENT_TITLE_INVALID, SOURCE);
                return objResponse;
            }
            strContent = x_objRequestModel.Content.RemoveHTMLTag();
            if (string.IsNullOrWhiteSpace(strContent) == true)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.RECRUITMENT_CONTENT_INVALID, ErrorCode.RECRUITMENT_CONTENT_INVALID);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, ErrorConstant.RECRUITMENT_CONTENT_INVALID, SOURCE);
                return objResponse;
            }
            bIsValid = x_objRequestModel.EndTime.CheckDateTimeFormat(out strOutVal);
            if (bIsValid == false)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.RECRUITMENT_DATE_INVALID, ErrorCode.RECRUITMENT_DATE_INVALID);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, ErrorConstant.RECRUITMENT_DATE_INVALID, SOURCE);
                return objResponse;
            }
            objFinishTime = x_objRequestModel.EndTime.ConvertStrToDateTime();
            objCurrentTime = DateTimeHelper.GetCurrentDateTimeVN();
            if (objFinishTime < objCurrentTime)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.RECRUITMENT_DATE_INVALID, ErrorCode.RECRUITMENT_DATE_INVALID);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, ErrorConstant.RECRUITMENT_DATE_INVALID, SOURCE);
                return objResponse;
            }
            // Check recruitment valid in database
            objRecruitmentInfo = await RecruitmentDbManager.GetRecruitmentValid();
            if (objRecruitmentInfo != null)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.EXIST_RECRUITMENT_VALID, ErrorCode.EXIST_RECRUITMENT_VALID);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, ErrorConstant.EXIST_RECRUITMENT_VALID, SOURCE);
                return objResponse;
            }
            //----------------------------------
            // Get Login account
            //----------------------------------
            objAccount = await GetAccount(x_strAccountId);
            if ((objAccount == null) || (objAccount.IsLocked == true))
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.ACCOUNT_IS_LOCKER, ErrorCode.ACCOUNT_IS_LOCKER);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, ErrorConstant.ACCOUNT_IS_LOCKER, SOURCE);
                return objResponse;
            }
            //----------------------------------
            // Get Login Roldes
            //----------------------------------
            objRole = await RolesDbManager.GetRoleById(objAccount.RoleId);
            if ((objRole == null) || (objRole.Permissions.Contains((int)DTUSVCPermission.CREATE_RECRUITMENT) == false))
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.AUTHORITY_INSUFFICIENT, ErrorCode.AUTHORITY_INSUFFICIENT);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, ErrorConstant.AUTHORITY_INSUFFICIENT, SOURCE);
                return objResponse;
            }
            //----------------------------------
            // Get Login Member
            //----------------------------------
            objMember = MemberInfoManager.GetMemberById(objAccount.MemberId);
            if ((objMember == null) || (objMember.IsMember == false))
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.AUTHORITY_INSUFFICIENT, ErrorCode.AUTHORITY_INSUFFICIENT);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, ErrorConstant.AUTHORITY_INSUFFICIENT, SOURCE);
                return objResponse;
            }
            //----------------------------------
            // Create Recruitment
            //----------------------------------
            objRecruitmentInfo = new RecruitmentInfo
            {
                Content = x_objRequestModel.Content,
                CreateId = objMember.Id,
                EndTime = objFinishTime,
                Title = x_objRequestModel.Title
            };
            bIsValid = await RecruitmentDbManager.UpdateRecruitment(objRecruitmentInfo);
            if (bIsValid == false)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.UPDATE_RECRUITMENT_FAILED, ErrorCode.UPDATE_RECRUITMENT_FAILED);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, ErrorConstant.UPDATE_RECRUITMENT_FAILED, SOURCE);
                return objResponse;
            }
            // Create response data
            objRecruitmentResponse = new RecruitmentResponseModel
            {
                Content = objRecruitmentInfo.Content,
                CreateName = objMember.GetMemberFullName(),
                EndTime = objRecruitmentInfo.EndTime,
                Id = objRecruitmentInfo.Id,
                IsLock = objRecruitmentInfo.IsLock,
                IsStop = objRecruitmentInfo.IsStop,
                StartTime = objRecruitmentInfo.StartTime,
                Title = objRecruitmentInfo.Title
            };
            objResponse = ResponseManager.GetSuccsessResp(SuccsessConstant.CREATE_RECRUITMENT_SUCCSESS, objRecruitmentResponse);
            return objResponse;
        }

        /// <summary>
        /// Delete Applying For Member Info
        /// </summary>
        /// <param name="x_strMemberId"></param>
        /// <param name="x_strRecruitmentId"></param>
        /// <param name="x_strAccountId"></param>
        /// <param name="x_objContext"></param>
        /// <returns></returns>
        public async Task<ResponseModel> DeleteApplyingForMemberInfo(string x_strMemberId, string x_strRecruitmentId, string x_strAccountId, HttpContext x_objContext)
        {
            ResponseModel objResponse;
            RecruitmentInfo objRecruitment;
            Accounts objAccount;
            Roles objRole;
            MemberInterviewRoom objMemberInRoom;
            MemberRegisterDecryptModel objMembershipRegistration;
            string strClientIp;
            bool bDeleteResult;

            //----------------------------------
            // Get IP Address
            //----------------------------------
            strClientIp = x_objContext.GetClientIPAddress();
            //----------------------------------
            // Get Login Account
            //----------------------------------
            objAccount = await GetAccount(x_strAccountId);
            if ((objAccount == null) || (objAccount.IsLocked == true))
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.ACCOUNT_IS_LOCKER, ErrorCode.ACCOUNT_IS_LOCKER);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, x_strAccountId, ErrorConstant.ACCOUNT_IS_LOCKER, SOURCE);
                return objResponse;
            }
            //----------------------------------
            // Get Login Roldes
            //----------------------------------
            objRole = await RolesDbManager.GetRoleById(objAccount.RoleId);
            if ((objRole == null) || (objRole.Permissions.Contains((int)DTUSVCPermission.DELETE_MEMBER_INTERVIEW) == false))
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.AUTHORITY_INSUFFICIENT, ErrorCode.AUTHORITY_INSUFFICIENT);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, ErrorConstant.AUTHORITY_INSUFFICIENT, SOURCE);
                return objResponse;
            }
            //----------------------------------
            // Get Recuitment
            //----------------------------------
            objRecruitment = await RecruitmentDbManager.GetRecruitmentById(x_strRecruitmentId);
            if (objRecruitment == null)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.RECRUITMENT_IS_NULL, ErrorCode.RECRUITMENT_IS_NULL);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, ErrorConstant.RECRUITMENT_IS_NULL, SOURCE);
                return objResponse;
            }
            if (objRecruitment.IsStop == true)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.RECRUITMENT_IS_EXPIRED, ErrorCode.RECRUITMENT_IS_EXPIRED);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, ErrorConstant.RECRUITMENT_IS_EXPIRED, SOURCE);
                return objResponse;
            }
            //----------------------------------
            // Get Interview member
            //----------------------------------
            objMembershipRegistration = MemberRegisterMemory.GetMemberById(x_strMemberId);
            if (objMembershipRegistration == null)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.MEMBER_INTERVIEW_NULL, ErrorCode.MEMBER_INTERVIEW_NULL);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, ErrorConstant.MEMBER_INTERVIEW_NULL, SOURCE);
                return objResponse;
            }
            if ((objMembershipRegistration.FailedInterView == false) || (objMembershipRegistration.ApprovalInfo != null))
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.MEMBER_IS_INTERVIEWED, ErrorCode.MEMBER_IS_INTERVIEWED);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, ErrorConstant.MEMBER_IS_INTERVIEWED, SOURCE);
                return objResponse;
            }
            if (objMembershipRegistration.RecruitId.Equals(x_strRecruitmentId) == false)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.RECRUITMENT_IS_NULL, ErrorCode.RECRUITMENT_IS_NULL);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, ErrorConstant.RECRUITMENT_IS_NULL, SOURCE);
                return objResponse;
            }
            //----------------------------------
            // Get Member in Room
            //----------------------------------
            objMemberInRoom = await RecruitmentDbManager.GetMemberInterviewRoomById(x_strMemberId);
            if (objMemberInRoom != null)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.MEMBER_IS_IN_ROOM, ErrorCode.MEMBER_IS_IN_ROOM);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, ErrorConstant.MEMBER_IS_IN_ROOM, SOURCE);
                return objResponse;
            }
            bDeleteResult = await MemberRegisterMemory.DeleteMemberRegister(x_strRecruitmentId, x_strMemberId);
            if (bDeleteResult == false)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.DELETE_MEMBER_INTERVIEW_FAILED, ErrorCode.DELETE_MEMBER_INTERVIEW_FAILED);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, ErrorConstant.DELETE_MEMBER_INTERVIEW_FAILED, SOURCE);
                return objResponse;
            }

            objResponse = ResponseManager.GetSuccsessResp(SuccsessConstant.DELETE_MEMBER_INTERVIEW_SUCCSESS);
            return objResponse;
        }

        /// <summary>
        /// Delete Interview Room
        /// </summary>
        /// <param name="x_strRoomId"></param>
        /// <param name="x_strRecruitmentId"></param>
        /// <param name="x_strAccountId"></param>
        /// <param name="x_objContext"></param>
        /// <returns></returns>
        public async Task<ResponseModel> DeleteInterviewRoom(string x_strRoomId, string x_strRecruitmentId, string x_strAccountId, HttpContext x_objContext)
        {
            ResponseModel objResponse;
            RecruitmentInfo objRecruitment;
            InterviewRooms objRoom;
            Accounts objAccount;
            Roles objRole;
            DateTime objCurrentTime;
            string strClientIp;
            bool bDeleteResult;

            //----------------------------------
            // Get IP Address
            //----------------------------------
            strClientIp = x_objContext.GetClientIPAddress();
            //----------------------------------
            // Get Login Account
            //----------------------------------
            objAccount = await GetAccount(x_strAccountId);
            if ((objAccount == null) || (objAccount.IsLocked == true))
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.ACCOUNT_IS_LOCKER, ErrorCode.ACCOUNT_IS_LOCKER);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, x_strAccountId, ErrorConstant.ACCOUNT_IS_LOCKER, SOURCE);
                return objResponse;
            }
            //----------------------------------
            // Get Login Roldes
            //----------------------------------
            objRole = await RolesDbManager.GetRoleById(objAccount.RoleId);
            if ((objRole == null) || (objRole.Permissions.Contains((int)DTUSVCPermission.DELETE_INTERVIEW_ROOM) == false))
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.AUTHORITY_INSUFFICIENT, ErrorCode.AUTHORITY_INSUFFICIENT);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, ErrorConstant.AUTHORITY_INSUFFICIENT, SOURCE);
                return objResponse;
            }
            //----------------------------------
            // Get Recuitment
            //----------------------------------
            objRecruitment = await RecruitmentDbManager.GetRecruitmentById(x_strRecruitmentId);
            if (objRecruitment == null)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.RECRUITMENT_IS_NULL, ErrorCode.RECRUITMENT_IS_NULL);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, ErrorConstant.RECRUITMENT_IS_NULL, SOURCE);
                return objResponse;
            }
            if (objRecruitment.IsStop == true)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.RECRUITMENT_IS_EXPIRED, ErrorCode.RECRUITMENT_IS_EXPIRED);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, ErrorConstant.RECRUITMENT_IS_EXPIRED, SOURCE);
                return objResponse;
            }
            //----------------------------------
            // Get Interview room
            //----------------------------------
            objRoom = await RecruitmentDbManager.GetRoomById(x_strRoomId);
            if (objRoom == null)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.INTERVIEW_ROOM_IS_NULL, ErrorCode.INTERVIEW_ROOM_IS_NULL);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, ErrorConstant.INTERVIEW_ROOM_IS_NULL, SOURCE);
                return objResponse;
            }
            if (objRoom.ManagerIds.Contains(objAccount.MemberId) == false)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.YOU_DONT_ROOM_MANAGER, ErrorCode.YOU_DONT_ROOM_MANAGER);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, ErrorConstant.YOU_DONT_ROOM_MANAGER, SOURCE);
                return objResponse;
            }
            objCurrentTime = DateTimeHelper.GetCurrentDateTimeVN();
            if (objRoom.StartDateTime < objCurrentTime)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.ROOM_IS_STARTED, ErrorCode.ROOM_IS_STARTED);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, ErrorConstant.ROOM_IS_STARTED, SOURCE);
                return objResponse;
            }
            if (objRoom.EmailSending == true)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.ROOM_EMAIL_SENDING, ErrorCode.ROOM_EMAIL_SENDING);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, ErrorConstant.ROOM_EMAIL_SENDING, SOURCE);
                return objResponse;
            }
            // Send email cancel interview schedule
            await SendCancelInterviewEmail(x_strRoomId);
            // Delete Room
            bDeleteResult = await RecruitmentDbManager.DeleteRoom(x_strRoomId);
            if (bDeleteResult == false)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.DELETE_ROOM_FAILED, ErrorCode.DELETE_ROOM_FAILED);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, ErrorConstant.DELETE_ROOM_FAILED, SOURCE);
                return objResponse;
            }

            await MemberRegisterMemory.UpdateInterviewRoom(objRoom.RecruitId, objRoom.Id, true);
            objResponse = ResponseManager.GetSuccsessResp(SuccsessConstant.DELETE_ROOM_SUCCSESS, x_strRoomId);
            return objResponse;
        }

        /// <summary>
        /// Delete Recruitment
        /// </summary>
        /// <param name="x_strRecruitmentId"></param>
        /// <param name="x_strAccountId"></param>
        /// <param name="x_objContext"></param>
        /// <returns></returns>
        public async Task<ResponseModel> DeleteRecruitment(string x_strRecruitmentId, string x_strAccountId, HttpContext x_objContext)
        {
            ResponseModel objResponse;
            MemberDecryptModel objMember;
            Accounts objAccount;
            Roles objRole;
            RecruitmentInfo objRecruitment;
            DateTime objCurrentTime;
            bool bDeleteSuccsess;
            string strClientIp;

            //----------------------------------
            // Get IP Address
            //----------------------------------
            strClientIp = x_objContext.GetClientIPAddress();
            //----------------------------------
            // Get Login account
            //----------------------------------
            objAccount = await GetAccount(x_strAccountId);
            if ((objAccount == null) || (objAccount.IsLocked == true))
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.ACCOUNT_IS_LOCKER, ErrorCode.ACCOUNT_IS_LOCKER);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, ErrorConstant.ACCOUNT_IS_LOCKER, SOURCE);
                return objResponse;
            }
            //----------------------------------
            // Get Login Roldes
            //----------------------------------
            objRole = await RolesDbManager.GetRoleById(objAccount.RoleId);
            if ((objRole == null) || (objRole.Permissions.Contains((int)DTUSVCPermission.DELETE_RECRUITMENT) == false))
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.AUTHORITY_INSUFFICIENT, ErrorCode.AUTHORITY_INSUFFICIENT);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, ErrorConstant.AUTHORITY_INSUFFICIENT, SOURCE);
                return objResponse;
            }
            //----------------------------------
            // Get Login Member
            //----------------------------------
            objMember = MemberInfoManager.GetMemberById(objAccount.MemberId);
            if ((objMember == null) || (objMember.IsMember == false))
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.AUTHORITY_INSUFFICIENT, ErrorCode.AUTHORITY_INSUFFICIENT);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, ErrorConstant.AUTHORITY_INSUFFICIENT, SOURCE);
                return objResponse;
            }
            //----------------------------------
            // Get Recruitment
            //----------------------------------
            objRecruitment = await RecruitmentDbManager.GetRecruitmentById(x_strRecruitmentId);
            if (objRecruitment == null)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.RECRUITMENT_IS_NULL, ErrorCode.RECRUITMENT_IS_NULL);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, ErrorConstant.RECRUITMENT_IS_NULL, SOURCE);
                return objResponse;
            }
            objCurrentTime = DateTimeHelper.GetCurrentDateTimeVN();
            if ((objRecruitment.IsStop == true) || (objRecruitment.EndTime < objCurrentTime))
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.RECRUITMENT_IS_EXPIRED, ErrorCode.RECRUITMENT_IS_EXPIRED);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, ErrorConstant.RECRUITMENT_IS_EXPIRED, SOURCE);
                return objResponse;
            }
            // Delete
            bDeleteSuccsess = await RecruitmentDbManager.DeleteRecrumentById(x_strRecruitmentId);
            if (bDeleteSuccsess == false)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.DELETE_RECRUITMENT_FAILED, ErrorCode.DELETE_RECRUITMENT_FAILED);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, ErrorConstant.DELETE_RECRUITMENT_FAILED, SOURCE);
                return objResponse;
            }

            MemberRegisterMemory.DeleteRecruit(x_strRecruitmentId);
            objResponse = ResponseManager.GetSuccsessResp(SuccsessConstant.DELETE_RECRUITMENT_SUCCSESS);
            return objResponse;
        }

        /// <summary>
        /// Download Applying For Members
        /// </summary>
        /// <param name="x_strRecruitmentId"></param>
        /// <param name="x_strAccountId"></param>
        /// <param name="x_objContext"></param>
        /// <returns></returns>
        public async Task<ResponseModel> DownloadApplyingForMembers(string x_strRecruitmentId, string x_strAccountId, HttpContext x_objContext)
        {
            const string FILE_NAME_FORMAT = "{0}_{1}.xlsx";
            const string DATE_TIME_FORMAT = "yyyyMMddHHmmssfff";
            const string SHEET_NAME = "MemberRegisterList";
            ResponseModel objResponse;
            RecruitmentInfo objRecruitment;
            DateTime objCurrentTime;
            RecruitmentMemberInfoExcel objMemberExcel;
            List<MemberRegisterDecryptModel> lstMemberRegister;
            List<RecruitmentMemberInfoExcel> lstMemberRegisterExcelModel;
            Dictionary<string, List<RecruitmentMemberInfoExcel>> dicExportData;
            Accounts objAccount;
            MemberDecryptModel objMemberInfo;
            Roles objRole;
            string strFileName;
            string strFilePath;
            string strClientIp;
            bool bExportResult;

            //----------------------------
            // Get IP address
            //----------------------------
            strClientIp = x_objContext.GetClientIPAddress();
            //----------------------------------
            // Validate request role
            //----------------------------------
            // Account
            objAccount = await GetAccount(x_strAccountId);
            if (objAccount == null)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.ACCOUNT_IS_LOCKER, ErrorCode.ACCOUNT_IS_LOCKER);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.ACCOUNT_IS_LOCKER}", SOURCE);

                return objResponse;
            }

            // Member
            objMemberInfo = MemberInfoManager.GetMemberById(objAccount.MemberId);
            if (objMemberInfo == null)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.MEMBER_INFO_IS_NULL, ErrorCode.MEMBER_INFO_IS_NULL);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.MEMBER_INFO_IS_NULL}", SOURCE);

                return objResponse;
            }

            // Role
            objRole = await RolesDbManager.GetRoleById(objAccount.RoleId);
            if ((objRole == null) || (objRole.Permissions.Contains((int)DTUSVCPermission.DOWNLOAD_MEMBER_INTERVIEW) == false))
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.AUTHORITY_INSUFFICIENT, ErrorCode.AUTHORITY_INSUFFICIENT);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMemberInfo.GetMemberFullName(), $"{SOURCE}: {ErrorConstant.MEMBER_INFO_IS_NULL}", SOURCE);

                return objResponse;
            }
            //----------------------------------
            // Get Recruitment
            //----------------------------------
            objRecruitment = await RecruitmentDbManager.GetRecruitmentById(x_strRecruitmentId);
            if (objRecruitment == null)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.RECRUITMENT_IS_NULL, ErrorCode.RECRUITMENT_IS_NULL);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, ErrorConstant.RECRUITMENT_IS_NULL, SOURCE);
                return objResponse;
            }
            // Get List Member
            objResponse = await GetApplyingForMembers(x_strRecruitmentId, x_strAccountId, x_objContext);
            if (objResponse.IsSuccsess == false)
            {
                return objResponse;
            }
            // Convert data to list member
            lstMemberRegister = objResponse.DataValue as List<MemberRegisterDecryptModel>;
            if ((lstMemberRegister == null) || (lstMemberRegister.Count == 0))
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.MEMBER_LIST_REGISTER_IS_NULL, ErrorCode.MEMBER_LIST_REGISTER_IS_NULL);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, ErrorConstant.MEMBER_LIST_REGISTER_IS_NULL, SOURCE);
                return objResponse;
            }

            lstMemberRegisterExcelModel = new List<RecruitmentMemberInfoExcel>();
            foreach (MemberRegisterDecryptModel objMember in lstMemberRegister)
            {
                if (objMember == null)
                {
                    continue;
                }
                objMemberExcel = new RecruitmentMemberInfoExcel
                {
                    FirstName = objMember.FirstName,
                    LastName = objMember.LastName,
                    BirthDay = objMember.BirthDay,
                    Sex = objMember.Sex,
                    Province = objMember.Hometown.Province.ProvinceName,
                    District = objMember.Hometown.District.DistrictName,
                    Ward = objMember.Hometown.Ward.WardName,
                    SpecificAddress = objMember.Hometown.SpecificAddress,
                    PhoneNumber = objMember.PhoneNumber,
                    Email = objMember.Email,
                    LinkFacebook = objMember.LinkFacebook,
                    Faculty = objMember.Faculty.FacultyName,
                    ClassName = objMember.ClassName,
                    StudentId = objMember.StudentId,
                    Reason = objMember.Reason,
                    FailedInterView = objMember.FailedInterView
                };
                // Set interview
                if (objMember.InterviewInfo != null)
                {
                    objMemberExcel.ReviewContent = objMember.InterviewInfo.ReviewContent;
                    objMemberExcel.Note = objMember.InterviewInfo.Note;
                    objMemberExcel.Scores = objMember.InterviewInfo.Scores;
                    objMemberExcel.EmailSent = objMember.InterviewInfo.EmailSent;
                    objMemberExcel.Interviewed = objMember.InterviewInfo.Interviewed;
                    objMemberExcel.InterviewAppointmentTime = objMember.InterviewInfo.InterviewAppointmentTime.ToString("dd/MM/yyyy HH:mm:ss");
                    objMemberExcel.ReviewDate = objMember.InterviewInfo.ReviewDate.ToString("dd/MM/yyyy HH:mm:ss");
                }
                // Set Approval info
                if (objMember.ApprovalInfo != null)
                {
                    objMemberExcel.Approver = objMember.ApprovalInfo.Approver.GetMemberFullName();
                    objMemberExcel.ApprovalDate = objMember.ApprovalInfo.ApprovalDate.ToString("dd/MM/yyyy HH:mm:ss");
                }

                lstMemberRegisterExcelModel.Add(objMemberExcel);
            }
            objCurrentTime = DateTimeHelper.GetCurrentDateTimeVN();
            strFileName = string.Format(FILE_NAME_FORMAT, objRecruitment.Title.RemoveUnicode().Replace(" ", "_"), objCurrentTime.ToString(DATE_TIME_FORMAT));
            strFilePath = FileHelper.GetFilePath(SysFolder.EXPORT_MEMBER_REGISTER_LIST, strFileName);
            dicExportData = new Dictionary<string, List<RecruitmentMemberInfoExcel>>();
            dicExportData[SHEET_NAME] = lstMemberRegisterExcelModel;
            // Export file
            bExportResult = dicExportData.ExportDataToExcel(strFilePath);
            if (bExportResult == false)
            {
                // Get error
                objResponse = ResponseManager.GetDownloadErrorResp(ErrorConstant.EXPORT_EXCEL_FAILED, ErrorCode.EXPORT_EXCEL_FAILED);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.EXPORT_EXCEL_FAILED}", SOURCE);

                return objResponse;
            }
            objResponse = new ResponseDownloadFile()
            {
                IsSuccsess = true,
                FilePath = strFilePath
            };
            return objResponse;
        }

        /// <summary>
        /// Get Applying Member Info
        /// </summary>
        /// <param name="x_strMemberId"></param>
        /// <param name="x_strRecruitmentId"></param>
        /// <param name="x_strAccountId"></param>
        /// <param name="x_objContext"></param>
        /// <returns></returns>
        public async Task<ResponseModel> GetApplyingMemberInfo(string x_strMemberId, string x_strRecruitmentId, string x_strAccountId, HttpContext x_objContext)
        {
            Accounts objAccount;
            Roles objRole;
            MemberDecryptModel objMember;
            ResponseModel objResponse;
            RecruitmentInfo objRecruitment;
            MemberRegisterDecryptModel objMemberRegister;
            string strClientIp;

            //----------------------------
            // Get IP address
            //----------------------------
            strClientIp = x_objContext.GetClientIPAddress();
            //----------------------------
            // Get login account
            //----------------------------
            objAccount = await GetAccount(x_strAccountId);
            if ((objAccount == null) || (objAccount.IsLocked == true))
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.ACCOUNT_IS_LOCKER, ErrorCode.ACCOUNT_IS_LOCKER);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, ErrorConstant.ACCOUNT_IS_LOCKER, SOURCE);
                return objResponse;
            }
            //----------------------------
            // Get login Role
            //----------------------------
            objRole = await RolesDbManager.GetRoleById(objAccount.RoleId);
            if ((objRole == null) || (objRole.Permissions.Contains((int)DTUSVCPermission.GET_MEMBER_INFO_INTERVIEW) == false))
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.AUTHORITY_INSUFFICIENT, ErrorCode.AUTHORITY_INSUFFICIENT);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, ErrorConstant.AUTHORITY_INSUFFICIENT, SOURCE);
                return objResponse;
            }
            //----------------------------
            // Get login member
            //----------------------------
            objMember = MemberInfoManager.GetMemberById(objAccount.MemberId);
            if ((objMember == null) || (objMember.IsMember == false))
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.AUTHORITY_INSUFFICIENT, ErrorCode.AUTHORITY_INSUFFICIENT);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, ErrorConstant.AUTHORITY_INSUFFICIENT, SOURCE);
                return objResponse;
            }
            //----------------------------------
            // Get Recruitment
            //----------------------------------
            objRecruitment = await RecruitmentDbManager.GetRecruitmentById(x_strRecruitmentId);
            if (objRecruitment == null)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.RECRUITMENT_IS_NULL, ErrorCode.RECRUITMENT_IS_NULL);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, ErrorConstant.RECRUITMENT_IS_NULL, SOURCE);
                return objResponse;
            }
            //----------------------------------
            // Get Member info
            //----------------------------------
            objMemberRegister = MemberRegisterMemory.GetMemberById(x_strMemberId);
            objResponse = ResponseManager.GetSuccsessResp(SuccsessConstant.GET_MEMBER_REGISTER_SUCCSESS, objMemberRegister);
            return objResponse;
        }

        /// <summary>
        /// Get Applying For Members
        /// </summary>
        /// <param name="x_strRecruitmentId"></param>
        /// <param name="x_strAccountId"></param>
        /// <param name="x_objContext"></param>
        /// <returns></returns>
        public async Task<ResponseModel> GetApplyingForMembers(string x_strRecruitmentId, string x_strAccountId, HttpContext x_objContext)
        {
            List<MemberRegisterDecryptModel> lstMemberRegister;
            RecruitmentInfo objRecruitment;
            MemberDecryptModel objMember;
            Accounts objAccount;
            Roles objRole;
            ResponseModel objResponse;
            string strClientIp;

            //----------------------------
            // Get IP address
            //----------------------------
            strClientIp = x_objContext.GetClientIPAddress();
            //----------------------------
            // Get login account
            //----------------------------
            objAccount = await GetAccount(x_strAccountId);
            if ((objAccount == null) || (objAccount.IsLocked == true))
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.ACCOUNT_IS_LOCKER, ErrorCode.ACCOUNT_IS_LOCKER);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, ErrorConstant.ACCOUNT_IS_LOCKER, SOURCE);
                return objResponse;
            }
            //----------------------------
            // Get login Role
            //----------------------------
            objRole = await RolesDbManager.GetRoleById(objAccount.RoleId);
            if ((objRole == null) || (objRole.Permissions.Contains((int)DTUSVCPermission.GET_MEMBER_INTERVIEW) == false))
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.AUTHORITY_INSUFFICIENT, ErrorCode.AUTHORITY_INSUFFICIENT);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, ErrorConstant.AUTHORITY_INSUFFICIENT, SOURCE);
                return objResponse;
            }
            //----------------------------
            // Get login member
            //----------------------------
            objMember = MemberInfoManager.GetMemberById(objAccount.MemberId);
            if ((objMember == null) || (objMember.IsMember == false))
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.AUTHORITY_INSUFFICIENT, ErrorCode.AUTHORITY_INSUFFICIENT);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, ErrorConstant.AUTHORITY_INSUFFICIENT, SOURCE);
                return objResponse;
            }
            //----------------------------------
            // Get Recruitment
            //----------------------------------
            objRecruitment = await RecruitmentDbManager.GetRecruitmentById(x_strRecruitmentId);
            if (objRecruitment == null)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.RECRUITMENT_IS_NULL, ErrorCode.RECRUITMENT_IS_NULL);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, ErrorConstant.RECRUITMENT_IS_NULL, SOURCE);
                return objResponse;
            }
            //----------------------------------
            // Get List Member
            //----------------------------------
            lstMemberRegister = MemberRegisterMemory.GetListMemberByRecruitId(x_strRecruitmentId);
            if ((lstMemberRegister == null) || (lstMemberRegister.Count == 0))
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.MEMBER_LIST_REGISTER_IS_NULL, ErrorCode.MEMBER_LIST_REGISTER_IS_NULL);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, ErrorConstant.MEMBER_LIST_REGISTER_IS_NULL, SOURCE);
                return objResponse;
            }

            objResponse = ResponseManager.GetSuccsessResp(SuccsessConstant.GET_LIST_MEMBER_REGISTER_SUCCSESS, lstMemberRegister, lstMemberRegister.Count);
            return objResponse;
        }

        /// <summary>
        /// Get Recruitment Info
        /// </summary>
        /// <param name="x_strRecruitmentId"></param>
        /// <param name="x_objContext"></param>
        /// <returns></returns>
        public async Task<ResponseModel> GetRecruitmentInfo(string x_strRecruitmentId, HttpContext x_objContext)
        {
            RecruitmentResponseModel objRecruitmentRes;
            RecruitmentInfo objRecruitmentInfo;
            MemberDecryptModel objMember;
            ResponseModel objResponse;
            string strIpAddress;

            //----------------------------
            // Get IP address
            //----------------------------
            strIpAddress = x_objContext.GetClientIPAddress();
            //----------------------------
            // Get Recruitment
            //----------------------------
            objRecruitmentInfo = await RecruitmentDbManager.GetRecruitmentById(x_strRecruitmentId);
            if (objRecruitmentInfo == null)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.RECRUITMENT_IS_NULL, ErrorCode.RECRUITMENT_IS_NULL);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strIpAddress, string.Empty, ErrorConstant.RECRUITMENT_IS_NULL, SOURCE);
                return objResponse;
            }
            //----------------------------
            // Get Creater
            //----------------------------
            objMember = MemberInfoManager.GetMemberById(objRecruitmentInfo.CreateId);
            if (objMember == null)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.RECRUITMENT_IS_NULL, ErrorCode.RECRUITMENT_IS_NULL);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strIpAddress, string.Empty, ErrorConstant.RECRUITMENT_IS_NULL, SOURCE);
                return objResponse;
            }
            objRecruitmentRes = ConvertRecruitmentDbToModel(objRecruitmentInfo, objMember);
            objResponse = ResponseManager.GetSuccsessResp(SuccsessConstant.GET_RECRUITMENT_SUCCSESS, objRecruitmentRes);
            return objResponse;
        }

        /// <summary>
        /// Get Recruitment Valid
        /// </summary>
        /// <param name="x_objContext"></param>
        /// <returns></returns>
        public async Task<ResponseModel> GetRecruitmentValid(HttpContext x_objContext)
        {
            RecruitmentResponseModel objRecruitmentRes;
            RecruitmentInfo objRecruitmentInfo;
            MemberDecryptModel objMember;
            ResponseModel objResponse;
            string strIpAddress;

            //----------------------------
            // Get IP address
            //----------------------------
            strIpAddress = x_objContext.GetClientIPAddress();
            //----------------------------
            // Get Recruitment
            //----------------------------
            objRecruitmentInfo = await RecruitmentDbManager.GetRecruitmentValid();
            if (objRecruitmentInfo == null)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.RECRUITMENT_IS_NULL, ErrorCode.RECRUITMENT_IS_NULL);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strIpAddress, string.Empty, ErrorConstant.RECRUITMENT_IS_NULL, SOURCE);
                return objResponse;
            }
            //----------------------------
            // Get Creater
            //----------------------------
            objMember = MemberInfoManager.GetMemberById(objRecruitmentInfo.CreateId);
            if (objMember == null)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.RECRUITMENT_IS_NULL, ErrorCode.RECRUITMENT_IS_NULL);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strIpAddress, string.Empty, ErrorConstant.RECRUITMENT_IS_NULL, SOURCE);
                return objResponse;
            }
            objRecruitmentRes = ConvertRecruitmentDbToModel(objRecruitmentInfo, objMember);
            objResponse = ResponseManager.GetSuccsessResp(SuccsessConstant.GET_RECRUITMENT_SUCCSESS, objRecruitmentRes);
            return objResponse;
        }

        /// <summary>
        /// Get Recruitment List
        /// </summary>
        /// <param name="x_strAccountId"></param>
        /// <param name="x_objContext"></param>
        /// <returns></returns>
        public async Task<ResponseModel> GetRecruitmentList(string x_strAccountId, HttpContext x_objContext)
        {
            RecruitmentResponseModel objRecruitmentRes;
            List<RecruitmentResponseModel> lstRecruitmentRes;
            List<RecruitmentInfo> lstRecruitmentInfo;
            MemberDecryptModel objMember;
            Accounts objAccount;
            Roles objRole;
            ResponseModel objResponse;
            string strClientIp;

            //----------------------------
            // Get IP address
            //----------------------------
            strClientIp = x_objContext.GetClientIPAddress();
            //----------------------------
            // Get login account
            //----------------------------
            objAccount = await GetAccount(x_strAccountId);
            if ((objAccount == null) || (objAccount.IsLocked == true))
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.ACCOUNT_IS_LOCKER, ErrorCode.ACCOUNT_IS_LOCKER);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, ErrorConstant.ACCOUNT_IS_LOCKER, SOURCE);
                return objResponse;
            }
            //----------------------------
            // Get login Role
            //----------------------------
            objRole = await RolesDbManager.GetRoleById(objAccount.RoleId);
            if ((objRole == null) || (objRole.Permissions.Contains((int)DTUSVCPermission.GET_ALL_RECRUITMENT) == false))
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.AUTHORITY_INSUFFICIENT, ErrorCode.AUTHORITY_INSUFFICIENT);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, ErrorConstant.AUTHORITY_INSUFFICIENT, SOURCE);
                return objResponse;
            }
            //----------------------------
            // Get login member
            //----------------------------
            objMember = MemberInfoManager.GetMemberById(objAccount.MemberId);
            if ((objMember == null) || (objMember.IsMember == false))
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.AUTHORITY_INSUFFICIENT, ErrorCode.AUTHORITY_INSUFFICIENT);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, ErrorConstant.AUTHORITY_INSUFFICIENT, SOURCE);
                return objResponse;
            }
            //----------------------------
            // Get list recruitment
            //----------------------------
            lstRecruitmentInfo = await RecruitmentDbManager.GetAllRecruitment();
            if ((lstRecruitmentInfo == null) || (lstRecruitmentInfo.Count == 0))
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.RECRUITMENT_IS_NULL, ErrorCode.RECRUITMENT_IS_NULL);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, ErrorConstant.RECRUITMENT_IS_NULL, SOURCE);
                return objResponse;
            }

            //----------------------------
            // Get list recruitment
            //----------------------------
            lstRecruitmentRes = new List<RecruitmentResponseModel>();
            foreach (RecruitmentInfo objTemp in lstRecruitmentInfo)
            {
                if (objTemp == null)
                {
                    continue;
                }
                //----------------------------
                // Get Creater
                //----------------------------
                objMember = MemberInfoManager.GetMemberById(objTemp.CreateId);
                objRecruitmentRes = ConvertRecruitmentDbToModel(objTemp, objMember);
                if (objRecruitmentRes == null)
                {
                    continue;
                }
                lstRecruitmentRes.Add(objRecruitmentRes);
            }
            //----------------------------
            // Sort data
            //----------------------------
            if (lstRecruitmentRes.Count > 0)
            {
                lstRecruitmentRes = lstRecruitmentRes.OrderByDescending(obj => obj.StartTime).ThenByDescending(obj => obj.EndTime).ToList();
            }

            objResponse = ResponseManager.GetSuccsessResp(SuccsessConstant.GET_LIST_RECRUITMENT_SUCCSESS, lstRecruitmentRes, lstRecruitmentRes.Count);
            return objResponse;
        }

        /// <summary>
        /// Lock Or Unlock Recruitment
        /// </summary>
        /// <param name="x_strRecruitmentId"></param>
        /// <param name="x_strAccountId"></param>
        /// <param name="x_objContext"></param>
        /// <returns></returns>
        public async Task<ResponseModel> LockOrUnlockRecruitment(string x_strRecruitmentId, string x_strAccountId, HttpContext x_objContext)
        {
            ResponseModel objResponse;
            MemberDecryptModel objMember;
            Accounts objAccount;
            Roles objRole;
            RecruitmentInfo objRecruitment;
            DateTime objCurrentTime;
            bool bLockUnlockSuccsess;
            string strClientIp;

            //----------------------------------
            // Get IP Address
            //----------------------------------
            strClientIp = x_objContext.GetClientIPAddress();
            //----------------------------------
            // Get Login account
            //----------------------------------
            objAccount = await GetAccount(x_strAccountId);
            if ((objAccount == null) || (objAccount.IsLocked == true))
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.ACCOUNT_IS_LOCKER, ErrorCode.ACCOUNT_IS_LOCKER);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, ErrorConstant.ACCOUNT_IS_LOCKER, SOURCE);
                return objResponse;
            }
            //----------------------------------
            // Get Login Roldes
            //----------------------------------
            objRole = await RolesDbManager.GetRoleById(objAccount.RoleId);
            if ((objRole == null) || (objRole.Permissions.Contains((int)DTUSVCPermission.LOCK_RECRUITMENT) == false))
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.AUTHORITY_INSUFFICIENT, ErrorCode.AUTHORITY_INSUFFICIENT);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, ErrorConstant.AUTHORITY_INSUFFICIENT, SOURCE);
                return objResponse;
            }
            //----------------------------------
            // Get Login Member
            //----------------------------------
            objMember = MemberInfoManager.GetMemberById(objAccount.MemberId);
            if ((objMember == null) || (objMember.IsMember == false))
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.AUTHORITY_INSUFFICIENT, ErrorCode.AUTHORITY_INSUFFICIENT);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, ErrorConstant.AUTHORITY_INSUFFICIENT, SOURCE);
                return objResponse;
            }
            //----------------------------------
            // Get Recruitment
            //----------------------------------
            objRecruitment = await RecruitmentDbManager.GetRecruitmentById(x_strRecruitmentId);
            if (objRecruitment == null)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.RECRUITMENT_IS_NULL, ErrorCode.RECRUITMENT_IS_NULL);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, ErrorConstant.RECRUITMENT_IS_NULL, SOURCE);
                return objResponse;
            }
            objCurrentTime = DateTimeHelper.GetCurrentDateTimeVN();
            if ((objRecruitment.IsStop == true) || (objRecruitment.EndTime < objCurrentTime))
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.RECRUITMENT_IS_EXPIRED, ErrorCode.RECRUITMENT_IS_EXPIRED);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, ErrorConstant.RECRUITMENT_IS_EXPIRED, SOURCE);
                return objResponse;
            }
            // Set data
            objRecruitment.IsLock = objRecruitment.IsLock == false;
            // Update data
            bLockUnlockSuccsess = await RecruitmentDbManager.UpdateRecruitment(objRecruitment);
            if (bLockUnlockSuccsess == false)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.LOCK_RECRUITMENT_FAILED, ErrorCode.LOCK_RECRUITMENT_FAILED);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, ErrorConstant.LOCK_RECRUITMENT_FAILED, SOURCE);
                return objResponse;
            }

            if (objRecruitment.IsLock == true)
            {
                objResponse = ResponseManager.GetSuccsessResp(SuccsessConstant.LOCK_RECRUITMENT_SUCCSESS);
            }
            else
            {
                objResponse = ResponseManager.GetSuccsessResp(SuccsessConstant.UNLOCK_RECRUITMENT_SUCCSESS);
            }

            return objResponse;
        }

        /// <summary>
        /// Member Approval
        /// </summary>
        /// <param name="x_objRequestModel"></param>
        /// <param name="x_strAccountId"></param>
        /// <param name="x_objContext"></param>
        /// <returns></returns>
        public async Task<ResponseModel> MemberApproval(MemberApprovalRequestModel x_objRequestModel, string x_strAccountId, HttpContext x_objContext)
        {
            Accounts objAccount;
            Roles objRole;
            MemberDecryptModel objMember;
            RecruitmentInfo objRecruitment;
            ResponseModel objResponse;
            string strClientIp;

            //----------------------------------
            // Get IP Address
            //----------------------------------
            strClientIp = x_objContext.GetClientIPAddress();
            //----------------------------------
            // Get Login account
            //----------------------------------
            objAccount = await GetAccount(x_strAccountId);
            if ((objAccount == null) || (objAccount.IsLocked == true))
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.ACCOUNT_IS_LOCKER, ErrorCode.ACCOUNT_IS_LOCKER);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, ErrorConstant.ACCOUNT_IS_LOCKER, SOURCE);
                return objResponse;
            }
            //----------------------------------
            // Get Login Roldes
            //----------------------------------
            objRole = await RolesDbManager.GetRoleById(objAccount.RoleId);
            if ((objRole == null) || (objRole.Permissions.Contains((int)DTUSVCPermission.LOCK_RECRUITMENT) == false))
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.AUTHORITY_INSUFFICIENT, ErrorCode.AUTHORITY_INSUFFICIENT);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, ErrorConstant.AUTHORITY_INSUFFICIENT, SOURCE);
                return objResponse;
            }
            //----------------------------------
            // Get Login Member
            //----------------------------------
            objMember = MemberInfoManager.GetMemberById(objAccount.MemberId);
            if ((objMember == null) || (objMember.IsMember == false))
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.AUTHORITY_INSUFFICIENT, ErrorCode.AUTHORITY_INSUFFICIENT);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, ErrorConstant.AUTHORITY_INSUFFICIENT, SOURCE);
                return objResponse;
            }
            //----------------------------------
            // Get Recruitment
            //----------------------------------
            objRecruitment = await RecruitmentDbManager.GetRecruitmentById(x_objRequestModel.RecruitId);
            if (objRecruitment == null)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.RECRUITMENT_IS_NULL, ErrorCode.RECRUITMENT_IS_NULL);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, ErrorConstant.RECRUITMENT_IS_NULL, SOURCE);
                return objResponse;
            }
            if (objRecruitment.IsStop == true)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.RECRUITMENT_IS_EXPIRED, ErrorCode.RECRUITMENT_IS_EXPIRED);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, ErrorConstant.RECRUITMENT_IS_EXPIRED, SOURCE);
                return objResponse;
            }
            // Approval
            MemberApproval(x_objRequestModel, objMember);
            objResponse = ResponseManager.GetSuccsessResp(SuccsessConstant.APPROVAL_SUCCSESS);
            return objResponse;
        }

        /// <summary>
        /// Move Interview
        /// </summary>
        /// <param name="x_objRequestModel"></param>
        /// <param name="x_strAccountId"></param>
        /// <param name="x_objContext"></param>
        /// <returns></returns>
        public async Task<ResponseModel> MoveInterview(MoveMemberFromRoom x_objRequestModel, string x_strAccountId, HttpContext x_objContext)
        {
            ResponseModel objResponse;
            Accounts objAccount;
            Roles objRole;
            InterviewRooms objRoom;
            MemberInterviewRoom objMemberInRoom;
            RecruitmentInfo objRecruitment;
            MemberDecryptModel objMember;
            MemberInterviewRoom objMemberInterviewRoom;
            DateTime objCurrentTime;
            ChangeRoomResult eChangeRoomResult;
            NotifySendAllMess objMess;
            MemberInRoomModel objMemberInRoomModel;
            string strClientIp;
            bool bResult;

            //----------------------------------
            // Get IP Address
            //----------------------------------
            strClientIp = x_objContext.GetClientIPAddress();
            //----------------------------------
            // Get Login account
            //----------------------------------
            objAccount = await GetAccount(x_strAccountId);
            if ((objAccount == null) || (objAccount.IsLocked == true))
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.ACCOUNT_IS_LOCKER, ErrorCode.ACCOUNT_IS_LOCKER);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, ErrorConstant.ACCOUNT_IS_LOCKER, SOURCE);
                return objResponse;
            }
            //----------------------------------
            // Get Login Roldes
            //----------------------------------
            objRole = await RolesDbManager.GetRoleById(objAccount.RoleId);
            if ((objRole == null) || (objRole.Permissions.Contains((int)DTUSVCPermission.MOVE_INTERVIEW) == false))
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.AUTHORITY_INSUFFICIENT, ErrorCode.AUTHORITY_INSUFFICIENT);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, ErrorConstant.AUTHORITY_INSUFFICIENT, SOURCE);
                return objResponse;
            }
            //----------------------------------
            // Get Login Member
            //----------------------------------
            objMember = MemberInfoManager.GetMemberById(objAccount.MemberId);
            if ((objMember == null) || (objMember.IsMember == false))
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.AUTHORITY_INSUFFICIENT, ErrorCode.AUTHORITY_INSUFFICIENT);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, ErrorConstant.AUTHORITY_INSUFFICIENT, SOURCE);
                return objResponse;
            }
            //----------------------------------
            // Get Recruitment
            //----------------------------------
            objRecruitment = await RecruitmentDbManager.GetRecruitmentById(x_objRequestModel.RecruitId);
            if (objRecruitment == null)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.RECRUITMENT_IS_NULL, ErrorCode.RECRUITMENT_IS_NULL);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMember.GetMemberFullName(), ErrorConstant.RECRUITMENT_IS_NULL, SOURCE);
                return objResponse;
            }
            objCurrentTime = DateTimeHelper.GetCurrentDateTimeVN();
            if ((objRecruitment.IsStop == true) || (objRecruitment.EndTime < objCurrentTime))
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.RECRUITMENT_IS_EXPIRED, ErrorCode.RECRUITMENT_IS_EXPIRED);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMember.GetMemberFullName(), ErrorConstant.RECRUITMENT_IS_EXPIRED, SOURCE);
                return objResponse;
            }
            //----------------------------------
            // Get Interview Room
            //----------------------------------
            objRoom = await RecruitmentDbManager.GetRoomById(x_objRequestModel.SourceRoomId);
            if (objRoom == null)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.INTERVIEW_ROOM_IS_NULL, ErrorCode.INTERVIEW_ROOM_IS_NULL);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMember.GetMemberFullName(), ErrorConstant.INTERVIEW_ROOM_IS_NULL, SOURCE);
                return objResponse;
            }
            if (objRoom.ManagerIds.Contains(objMember.Id) == false)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.YOU_DONT_ROOM_MANAGER, ErrorCode.YOU_DONT_ROOM_MANAGER);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, ErrorConstant.YOU_DONT_ROOM_MANAGER, SOURCE);
                return objResponse;
            }
            objRoom = await RecruitmentDbManager.GetRoomById(x_objRequestModel.DestRoomId);
            if (objRoom == null)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.INTERVIEW_DEST_ROOM_IS_NULL, ErrorCode.INTERVIEW_DEST_ROOM_IS_NULL);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMember.GetMemberFullName(), ErrorConstant.INTERVIEW_DEST_ROOM_IS_NULL, SOURCE);
                return objResponse;
            }
            if (objRoom.EndDateTime < objCurrentTime)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.ROOM_IS_END, ErrorCode.ROOM_IS_END);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMember.GetMemberFullName(), ErrorConstant.ROOM_IS_END, SOURCE);
                return objResponse;
            }
            //----------------------------------
            // Get Member in Room
            //----------------------------------
            objMemberInRoom = await RecruitmentDbManager.GetMemberInterviewRoomById(x_objRequestModel.MemberId, x_objRequestModel.DestRoomId);
            if (objMemberInRoom != null)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.MEMBER_IS_IN_DEST_ROOM, ErrorCode.MEMBER_IS_IN_DEST_ROOM);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMember.GetMemberFullName(), ErrorConstant.MEMBER_IS_IN_DEST_ROOM, SOURCE);
                return objResponse;
            }
            objMemberInRoom = await RecruitmentDbManager.GetMemberInterviewRoomById(x_objRequestModel.MemberId, x_objRequestModel.SourceRoomId);
            if ((objMemberInRoom != null) && (objMemberInRoom.IsChangeSchedule == false) && (objMemberInRoom.SendEmailState == (int)SendEmailType.SENT))
            {
                // Move
                eChangeRoomResult = await RecruitmentDbManager.ChangeRoom(objMemberInRoom.Id, x_objRequestModel.DestRoomId);
                if (eChangeRoomResult != ChangeRoomResult.UPDATE_SUCCSESS)
                {
                    objResponse = ResponseManager.GetErrorResp(eChangeRoomResult.DisplayName(), ErrorCode.MOVE_INTERVIEW_FAILED);
                    LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMember.GetMemberFullName(), eChangeRoomResult.DisplayName(), SOURCE);
                    return objResponse;
                }
                objResponse = ResponseManager.GetSuccsessResp("1");
            }
            else
            {
                if (objMemberInRoom != null)
                {
                    eChangeRoomResult = await RecruitmentDbManager.ChangeRoom(objMemberInRoom.Id, x_objRequestModel.DestRoomId);
                    if (eChangeRoomResult != ChangeRoomResult.UPDATE_SUCCSESS)
                    {
                        objResponse = ResponseManager.GetErrorResp(eChangeRoomResult.DisplayName(), ErrorCode.MOVE_INTERVIEW_FAILED);
                        LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMember.GetMemberFullName(), eChangeRoomResult.DisplayName(), SOURCE);
                        return objResponse;
                    }
                }
                else
                {
                    objMemberInterviewRoom = new MemberInterviewRoom
                    {
                        MemberId = objMemberInRoom.MemberId,
                        RecruitId = objRecruitment.Id,
                        RoomId = x_objRequestModel.DestRoomId,
                        CreaterId = objAccount.MemberId
                    };

                    bResult = RecruitmentDbManager.UpdateMemberInRoom(objMemberInterviewRoom);
                    if (bResult == false)
                    {
                        objResponse = ResponseManager.GetErrorResp(ErrorConstant.UPDATE_ROOM_FAILED, ErrorCode.UPDATE_ROOM_FAILED);
                        LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMember.GetMemberFullName(), ErrorConstant.UPDATE_ROOM_FAILED, SOURCE);
                        return objResponse;
                    }
                }

                // Delete
                await RecruitmentDbManager.DeleteInterviewRoom(objMemberInRoom.Id, objMemberInRoom.RoomId);
                objMemberInRoomModel = await MemberRegisterMemory.UpdateMemberToInterview(objMemberInRoom.RecruitId,
                                                                        objMemberInRoom.RoomId,
                                                                        objMemberInRoom.Id,
                                                                        true);
                // Send notifycation to client
                objMess = new NotifySendAllMess
                {
                    Data = new NotifyData
                    {
                        TempData = objMemberInRoomModel,
                    },
                    Screen = GUIScreen.INTERVIEW,
                    ActionId = x_objRequestModel.RecruitId,
                    MessageType = 0
                };
                SocketAutoSendMessage.Instant.QueueSendAllMess.Enqueue(objMess);
                objResponse = ResponseManager.GetSuccsessResp("2");
            }

            //----------------------------------
            // Get Member in Room
            //----------------------------------
            objMemberInRoom = await RecruitmentDbManager.GetMemberInterviewRoomById(x_objRequestModel.MemberId, x_objRequestModel.DestRoomId);
            if (objMemberInRoom != null)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.UPDATE_ROOM_FAILED, ErrorCode.UPDATE_ROOM_FAILED);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMember.GetMemberFullName(), ErrorConstant.UPDATE_ROOM_FAILED, SOURCE);
                return objResponse;
            }

            objMemberInRoomModel = await MemberRegisterMemory.UpdateMemberToInterview(objMemberInRoom.RecruitId,
                                                                        objMemberInRoom.RoomId,
                                                                        objMemberInRoom.Id,
                                                                        false);
            // Send notifycation to client
            objMess = new NotifySendAllMess
            {
                Data = new NotifyData
                {
                    TempData = objMemberInRoomModel
                },
                Screen = GUIScreen.INTERVIEW,
                ActionId = x_objRequestModel.RecruitId,
                MessageType = 1
            };

            SocketAutoSendMessage.Instant.QueueSendAllMess.Enqueue(objMess);

            objMemberInRoom = await RecruitmentDbManager.GetMemberInterviewRoomById(x_objRequestModel.MemberId, x_objRequestModel.SourceRoomId);
            if (objMemberInRoom != null)
            {
                objMemberInRoomModel = await MemberRegisterMemory.UpdateMemberToInterview(objMemberInRoom.RecruitId,
                                                                        objMemberInRoom.RoomId,
                                                                        objMemberInRoom.Id,
                                                                        false);
                // Send notifycation to client
                objMess = new NotifySendAllMess
                {
                    Data = new NotifyData
                    {
                        TempData = objMemberInRoomModel
                    },
                    Screen = GUIScreen.INTERVIEW,
                    ActionId = x_objRequestModel.RecruitId,
                    MessageType = 2
                };
                SocketAutoSendMessage.Instant.QueueSendAllMess.Enqueue(objMess);
            }
            return objResponse;
        }

        /// <summary>
        /// Remove Interview From Room
        /// </summary>
        /// <param name="x_strMemberId"></param>
        /// <param name="x_strRoomId"></param>
        /// <param name="x_strRecruitmentId"></param>
        /// <param name="x_strAccountId"></param>
        /// <param name="x_objContext"></param>
        /// <returns></returns>
        public async Task<ResponseModel> RemoveInterviewFromRoom(string x_strMemberId, string x_strRoomId, string x_strRecruitmentId, string x_strAccountId, HttpContext x_objContext)
        {
            ResponseModel objResponse;
            Accounts objAccount;
            Roles objRole;
            InterviewRooms objRoom;
            MemberInterviewRoom objMemberInRoom;
            RecruitmentInfo objRecruitment;
            MemberDecryptModel objMember;
            DateTime objCurrentTime;
            string strClientIp;

            //----------------------------------
            // Get IP Address
            //----------------------------------
            strClientIp = x_objContext.GetClientIPAddress();
            //----------------------------------
            // Get Login account
            //----------------------------------
            objAccount = await GetAccount(x_strAccountId);
            if ((objAccount == null) || (objAccount.IsLocked == true))
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.ACCOUNT_IS_LOCKER, ErrorCode.ACCOUNT_IS_LOCKER);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, ErrorConstant.ACCOUNT_IS_LOCKER, SOURCE);
                return objResponse;
            }
            //----------------------------------
            // Get Login Roldes
            //----------------------------------
            objRole = await RolesDbManager.GetRoleById(objAccount.RoleId);
            if ((objRole == null) || (objRole.Permissions.Contains((int)DTUSVCPermission.DELETE_MEMBER_INTERVIEW_IN_ROOM) == false))
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.AUTHORITY_INSUFFICIENT, ErrorCode.AUTHORITY_INSUFFICIENT);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, ErrorConstant.AUTHORITY_INSUFFICIENT, SOURCE);
                return objResponse;
            }
            //----------------------------------
            // Get Login Member
            //----------------------------------
            objMember = MemberInfoManager.GetMemberById(objAccount.MemberId);
            if ((objMember == null) || (objMember.IsMember == false))
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.AUTHORITY_INSUFFICIENT, ErrorCode.AUTHORITY_INSUFFICIENT);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, ErrorConstant.AUTHORITY_INSUFFICIENT, SOURCE);
                return objResponse;
            }
            //----------------------------------
            // Get Recruitment
            //----------------------------------
            objRecruitment = await RecruitmentDbManager.GetRecruitmentById(x_strRecruitmentId);
            if (objRecruitment == null)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.RECRUITMENT_IS_NULL, ErrorCode.RECRUITMENT_IS_NULL);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, ErrorConstant.RECRUITMENT_IS_NULL, SOURCE);
                return objResponse;
            }
            objCurrentTime = DateTimeHelper.GetCurrentDateTimeVN();
            if ((objRecruitment.IsStop == true) || (objRecruitment.EndTime < objCurrentTime))
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.RECRUITMENT_IS_EXPIRED, ErrorCode.RECRUITMENT_IS_EXPIRED);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, ErrorConstant.RECRUITMENT_IS_EXPIRED, SOURCE);
                return objResponse;
            }
            //----------------------------------
            // Get Interview Room
            //----------------------------------
            objRoom = await RecruitmentDbManager.GetRoomById(x_strRoomId);
            if (objRoom == null)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.INTERVIEW_ROOM_IS_NULL, ErrorCode.INTERVIEW_ROOM_IS_NULL);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMember.GetMemberFullName(), ErrorConstant.INTERVIEW_ROOM_IS_NULL, SOURCE);
                return objResponse;
            }
            if (objRoom.ManagerIds.Contains(objMember.Id) == false)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.YOU_DONT_ROOM_MANAGER, ErrorCode.YOU_DONT_ROOM_MANAGER);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, ErrorConstant.YOU_DONT_ROOM_MANAGER, SOURCE);
                return objResponse;
            }
            if (objRoom.EndDateTime < objCurrentTime)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.ROOM_IS_END, ErrorCode.ROOM_IS_END);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, ErrorConstant.ROOM_IS_END, SOURCE);
                return objResponse;
            }
            //----------------------------------
            // Get Member in Room
            //----------------------------------
            objMemberInRoom = await RecruitmentDbManager.GetMemberInterviewRoomById(x_strMemberId, x_strRoomId);
            if ((objMemberInRoom != null) && (objMemberInRoom.IsChangeSchedule == false) && (objMemberInRoom.SendEmailState == (int)SendEmailType.SENT))
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.DELETE_MEMBER_IN_ROOM_FAILED_EMAIL_SENNT, ErrorCode.DELETE_MEMBER_IN_ROOM_FAILED_EMAIL_SENNT);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, ErrorConstant.DELETE_MEMBER_IN_ROOM_FAILED_EMAIL_SENNT, SOURCE);
                return objResponse;
            }
            //----------------------------------
            // Get Delette member in Room
            //----------------------------------
            await RecruitmentDbManager.DeleteMemberInRoom(objMemberInRoom.Id);
            await MemberRegisterMemory.UpdateMemberToInterview(objMemberInRoom.RecruitId,
                                                               objMemberInRoom.RoomId,
                                                               objMemberInRoom.MemberId,
                                                               true);
            objResponse = ResponseManager.GetSuccsessResp(SuccsessConstant.DELETE_MEMBER_IN_ROOM_SUCCSESS);
            return objResponse;
        }

        /// <summary>
        /// Send Email Interview
        /// </summary>
        /// <param name="x_strRoomId"></param>
        /// <param name="x_strRecruitmentId"></param>
        /// <param name="x_strAccountId"></param>
        /// <param name="x_objContext"></param>
        /// <returns></returns>
        public async Task<ResponseModel> SendEmailInterview(string x_strRoomId, string x_strRecruitmentId, string x_strAccountId, HttpContext x_objContext)
        {
            Accounts objAccount;
            MemberDecryptModel objMember;
            Roles objRole;
            RecruitmentInfo objRecruitment;
            InterviewRooms objInterviewRoom;
            ResponseModel objResponse;
            DateTime objCurrentTime;
            DateTime objMaxInterviewTime;
            List<MemberInterviewRoom> lstInterviewRoom;
            bool bRes;
            string strClientIp;

            //----------------------------------
            // Get IP Address
            //----------------------------------
            strClientIp = x_objContext.GetClientIPAddress();
            //----------------------------------
            // Get Login account
            //----------------------------------
            objAccount = await GetAccount(x_strAccountId);
            if ((objAccount == null) || (objAccount.IsLocked == true))
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.ACCOUNT_IS_LOCKER, ErrorCode.ACCOUNT_IS_LOCKER);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, ErrorConstant.ACCOUNT_IS_LOCKER, SOURCE);
                return objResponse;
            }
            //----------------------------------
            // Get Login Roldes
            //----------------------------------
            objRole = await RolesDbManager.GetRoleById(objAccount.RoleId);
            if ((objRole == null) || (objRole.Permissions.Contains((int)DTUSVCPermission.SEND_INTERVIEW_MAIL) == false))
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.AUTHORITY_INSUFFICIENT, ErrorCode.AUTHORITY_INSUFFICIENT);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, ErrorConstant.AUTHORITY_INSUFFICIENT, SOURCE);
                return objResponse;
            }
            //----------------------------------
            // Get Login Member
            //----------------------------------
            objMember = MemberInfoManager.GetMemberById(objAccount.MemberId);
            if ((objMember == null) || (objMember.IsMember == false))
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.AUTHORITY_INSUFFICIENT, ErrorCode.AUTHORITY_INSUFFICIENT);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, ErrorConstant.AUTHORITY_INSUFFICIENT, SOURCE);
                return objResponse;
            }
            //----------------------------------
            // Get Recruitment
            //----------------------------------
            objRecruitment = await RecruitmentDbManager.GetRecruitmentById(x_strRecruitmentId);
            if (objRecruitment == null)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.RECRUITMENT_IS_NULL, ErrorCode.RECRUITMENT_IS_NULL);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, ErrorConstant.RECRUITMENT_IS_NULL, SOURCE);
                return objResponse;
            }
            objCurrentTime = DateTimeHelper.GetCurrentDateTimeVN();
            if ((objRecruitment.IsStop == true) || (objRecruitment.EndTime < objCurrentTime))
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.RECRUITMENT_IS_EXPIRED, ErrorCode.RECRUITMENT_IS_EXPIRED);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, ErrorConstant.RECRUITMENT_IS_EXPIRED, SOURCE);
                return objResponse;
            }
            //----------------------------------
            // Get Interview Room
            //----------------------------------
            objInterviewRoom = await RecruitmentDbManager.GetRoomById(x_strRoomId);
            if (objInterviewRoom == null)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.INTERVIEW_ROOM_IS_NULL, ErrorCode.INTERVIEW_ROOM_IS_NULL);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMember.GetMemberFullName(), ErrorConstant.INTERVIEW_ROOM_IS_NULL, SOURCE);
                return objResponse;
            }
            if (objInterviewRoom.EndDateTime < objCurrentTime)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.ROOM_IS_END, ErrorCode.ROOM_IS_END);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, ErrorConstant.ROOM_IS_END, SOURCE);
                return objResponse;
            }
            if (objInterviewRoom.EmailSending == true)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.ROOM_EMAIL_SENDING, ErrorCode.ROOM_EMAIL_SENDING);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, ErrorConstant.ROOM_EMAIL_SENDING, SOURCE);
                return objResponse;
            }
            if (objInterviewRoom.ManagerIds.Contains(objMember.Id) == false)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.YOU_DONT_ROOM_MANAGER, ErrorCode.YOU_DONT_ROOM_MANAGER);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, ErrorConstant.YOU_DONT_ROOM_MANAGER, SOURCE);
                return objResponse;
            }
            //----------------------------------
            // Get max interview time
            //----------------------------------
            objMaxInterviewTime = await RecruitmentDbManager.GetMaxInterviewTime(x_strRoomId);
            if (objMaxInterviewTime == DateTime.MinValue)
            {
                objMaxInterviewTime = objInterviewRoom.StartDateTime;
            }
            //----------------------------------
            // Get member interview
            //----------------------------------
            lstInterviewRoom = await RecruitmentDbManager.GetMemberInterviewByRoomId(x_strRoomId);
            if ((lstInterviewRoom == null) || (lstInterviewRoom.Count == 0))
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.ROOM_IS_EMPTY, ErrorCode.ROOM_IS_EMPTY);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, ErrorConstant.ROOM_IS_EMPTY, SOURCE);
                return objResponse;
            }
            // Send email
            bRes = SendEmailInterview(lstInterviewRoom.DeepClone(), objInterviewRoom.RoomAddress, objMaxInterviewTime, objInterviewRoom.RatedTime, objRecruitment.IsOnline);
            if (bRes == false)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.ROOM_IS_EMPTY, ErrorCode.ROOM_IS_EMPTY);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, ErrorConstant.ROOM_IS_EMPTY, SOURCE);
                return objResponse;
            }
            objResponse = ResponseManager.GetSuccsessResp(SuccsessConstant.SEND_INTERVIEW_EMAIL_SUCCSESS);
            return objResponse;
        }

        /// <summary>
        /// Set Finish Recruitment
        /// </summary>
        /// <param name="x_strRecruitmentId"></param>
        /// <param name="x_strAccountId"></param>
        /// <param name="x_objContext"></param>
        /// <returns></returns>
        public async Task<ResponseModel> SetFinishRecruitment(string x_strRecruitmentId, string x_strAccountId, HttpContext x_objContext)
        {
            ResponseModel objResponse;
            Task objTask;
            Accounts objAccount;
            MemberDecryptModel objMember;
            Roles objRole;
            RecruitmentInfo objRecruitment;
            DateTime objCurrentTime;
            string strClientIp;
            bool bUpdateSuccsess;

            //----------------------------------
            // Get IP Address
            //----------------------------------
            strClientIp = x_objContext.GetClientIPAddress();
            //----------------------------------
            // Get Login account
            //----------------------------------
            objAccount = await GetAccount(x_strAccountId);
            if ((objAccount == null) || (objAccount.IsLocked == true))
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.ACCOUNT_IS_LOCKER, ErrorCode.ACCOUNT_IS_LOCKER);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, ErrorConstant.ACCOUNT_IS_LOCKER, SOURCE);
                return objResponse;
            }
            //----------------------------------
            // Get Login Roldes
            //----------------------------------
            objRole = await RolesDbManager.GetRoleById(objAccount.RoleId);
            if ((objRole == null) || (objRole.Permissions.Contains((int)DTUSVCPermission.STOP_RECRUITMENT) == false))
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.AUTHORITY_INSUFFICIENT, ErrorCode.AUTHORITY_INSUFFICIENT);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, ErrorConstant.AUTHORITY_INSUFFICIENT, SOURCE);
                return objResponse;
            }
            //----------------------------------
            // Get Login Member
            //----------------------------------
            objMember = MemberInfoManager.GetMemberById(objAccount.MemberId);
            if ((objMember == null) || (objMember.IsMember == false))
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.AUTHORITY_INSUFFICIENT, ErrorCode.AUTHORITY_INSUFFICIENT);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, ErrorConstant.AUTHORITY_INSUFFICIENT, SOURCE);
                return objResponse;
            }
            //----------------------------------
            // Get Recruitment
            //----------------------------------
            objRecruitment = await RecruitmentDbManager.GetRecruitmentById(x_strRecruitmentId);
            if (objRecruitment == null)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.RECRUITMENT_IS_NULL, ErrorCode.RECRUITMENT_IS_NULL);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, ErrorConstant.RECRUITMENT_IS_NULL, SOURCE);
                return objResponse;
            }
            objCurrentTime = DateTimeHelper.GetCurrentDateTimeVN();
            if ((objRecruitment.IsStop == true) || (objRecruitment.EndTime < objCurrentTime))
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.RECRUITMENT_IS_EXPIRED, ErrorCode.RECRUITMENT_IS_EXPIRED);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, ErrorConstant.RECRUITMENT_IS_EXPIRED, SOURCE);
                return objResponse;
            }
            // Set data
            objRecruitment.IsLock = true;
            objRecruitment.IsStop = true;
            // Update data
            bUpdateSuccsess = await RecruitmentDbManager.UpdateRecruitment(objRecruitment);
            if (bUpdateSuccsess == false)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.SET_STOP_RECRUITMENT_FAILED, ErrorCode.SET_STOP_RECRUITMENT_FAILED);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, ErrorConstant.SET_STOP_RECRUITMENT_FAILED, SOURCE);
                return objResponse;
            }
            //----------------------------------
            // Send email Failed interview result
            //----------------------------------
            objTask = Task.Run(() => SendFailedInterviewResultEmail(objRecruitment.Id));

            objResponse = ResponseManager.GetSuccsessResp(SuccsessConstant.STOP_RECRUITMENT_SUCCSESS);
            return objResponse;
        }

        /// <summary>
        /// Update Interview Room
        /// </summary>
        /// <param name="x_objRequestModel"></param>
        /// <param name="x_strAccountId"></param>
        /// <param name="x_objContext"></param>
        /// <returns></returns>
        public async Task<ResponseModel> UpdateInterviewRoom(UpdateInterviewRoomRequestModel x_objRequestModel, string x_strAccountId, HttpContext x_objContext)
        {
            Accounts objAccount;
            MemberDecryptModel objMember;
            Roles objRole;
            ResponseModel objResponse;
            RecruitmentInfo objRecruitment;
            InterviewRooms objRoom;
            InterviewRoomReponse objInterviewRoom;
            DateTime objCurrentDate;
            DateTime objStartDate;
            DateTime objEndDate;
            DateTime objCurrentTime;
            string strClientIp;
            bool bUpdateSuccsess;

            //----------------------------
            // Get IP address
            //----------------------------
            strClientIp = x_objContext.GetClientIPAddress();
            //----------------------------
            // Get login account
            //----------------------------
            objAccount = await GetAccount(x_strAccountId);
            if ((objAccount == null) || (objAccount.IsLocked == true))
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.ACCOUNT_IS_LOCKER, ErrorCode.ACCOUNT_IS_LOCKER);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, ErrorConstant.ACCOUNT_IS_LOCKER, SOURCE);
                return objResponse;
            }
            //----------------------------
            // Get login Role
            //----------------------------
            objRole = await RolesDbManager.GetRoleById(objAccount.RoleId);
            if ((objRole == null) || (objRole.Permissions.Contains((int)DTUSVCPermission.CREATE_INTERVIEW_ROOM) == false))
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.AUTHORITY_INSUFFICIENT, ErrorCode.AUTHORITY_INSUFFICIENT);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, ErrorConstant.AUTHORITY_INSUFFICIENT, SOURCE);
                return objResponse;
            }
            //----------------------------
            // Get login member
            //----------------------------
            objMember = MemberInfoManager.GetMemberById(objAccount.MemberId);
            if ((objMember == null) || (objMember.IsMember == false))
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.AUTHORITY_INSUFFICIENT, ErrorCode.AUTHORITY_INSUFFICIENT);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, ErrorConstant.AUTHORITY_INSUFFICIENT, SOURCE);
                return objResponse;
            }
            //----------------------------
            // Get recruitment
            //----------------------------
            objRecruitment = await RecruitmentDbManager.GetRecruitmentById(x_objRequestModel.RecruitId);
            if (objRecruitment == null)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.RECRUITMENT_IS_NULL, ErrorCode.RECRUITMENT_IS_NULL);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMember.GetMemberFullName(), ErrorConstant.RECRUITMENT_IS_NULL, SOURCE);
                return objResponse;
            }
            objCurrentDate = DateTimeHelper.GetCurrentDateTimeVN();
            if ((objCurrentDate < objRecruitment.EndTime) || (objRecruitment.IsStop == true))
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.RECRUITMENT_IS_EXPIRED, ErrorCode.RECRUITMENT_IS_EXPIRED);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMember.GetMemberFullName(), ErrorConstant.RECRUITMENT_IS_EXPIRED, SOURCE);
                return objResponse;
            }
            //----------------------------
            // Validate
            //----------------------------
            objRoom = await RecruitmentDbManager.GetRoomById(x_objRequestModel.Id);
            if (objRoom == null)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.INTERVIEW_ROOM_IS_NULL, ErrorCode.INTERVIEW_ROOM_IS_NULL);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMember.GetMemberFullName(), ErrorConstant.INTERVIEW_ROOM_IS_NULL, SOURCE);
                return objResponse;
            }
            if (objRoom.ManagerIds.Contains(objMember.Id) == false)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.YOU_DONT_ROOM_MANAGER, ErrorCode.YOU_DONT_ROOM_MANAGER);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, ErrorConstant.YOU_DONT_ROOM_MANAGER, SOURCE);
                return objResponse;
            }
            objCurrentTime = DateTimeHelper.GetCurrentDateTimeVN();
            if (objRoom.StartDateTime < objCurrentTime)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.ROOM_IS_STARTED, ErrorCode.ROOM_IS_STARTED);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, ErrorConstant.ROOM_IS_STARTED, SOURCE);
                return objResponse;
            }
            // Convert Start & end time
            if (TryParseDateTime(x_objRequestModel.StartDateTime, out objStartDate) == false)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.DATETIME_FORMAT_INVALID, ErrorCode.DATETIME_FORMAT_INVALID);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMember.GetMemberFullName(), ErrorConstant.DATETIME_FORMAT_INVALID, SOURCE);
                return objResponse;
            }
            // 
            if (TryParseDateTime(x_objRequestModel.EndDateTime, out objEndDate) == false)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.DATETIME_FORMAT_INVALID, ErrorCode.DATETIME_FORMAT_INVALID);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMember.GetMemberFullName(), ErrorConstant.DATETIME_FORMAT_INVALID, SOURCE);
                return objResponse;
            }
            if (objStartDate > objEndDate)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.START_DATE_AND_END_DATE_INVALID, ErrorCode.START_DATE_AND_END_DATE_INVALID);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMember.GetMemberFullName(), ErrorConstant.START_DATE_AND_END_DATE_INVALID, SOURCE);
                return objResponse;
            }
            objRoom.StartDateTime = objStartDate;
            objRoom.EndDateTime = objEndDate;
            // Check room address
            if (string.IsNullOrWhiteSpace(x_objRequestModel.RoomAddress) == true)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.ROOM_ADDRESS_INVALID, ErrorCode.ROOM_ADDRESS_INVALID);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMember.GetMemberFullName(), ErrorConstant.ROOM_ADDRESS_INVALID, SOURCE);
                return objResponse;
            }
            objRoom.RoomAddress = x_objRequestModel.RoomAddress;
            // Check RatedTime
            if (x_objRequestModel.RatedTime <= 0)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.RATED_TIME_INVALID, ErrorCode.RATED_TIME_INVALID);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMember.GetMemberFullName(), ErrorConstant.RATED_TIME_INVALID, SOURCE);
                return objResponse;
            }
            objRoom.RatedTime = x_objRequestModel.RatedTime;
            objRoom.BreakTimes = GetValidBreakTime(x_objRequestModel.BreakTimes, objStartDate, objEndDate);
            objRoom.ManagerIds = GetManagerIdValid(x_objRequestModel.ManagerIds, objMember.Id);
            objRoom.Note = x_objRequestModel.Note;
            objRoom.RecruitId = x_objRequestModel.RecruitId;
            // Update to database
            bUpdateSuccsess = await RecruitmentDbManager.UpdateInterviewRoom(objRoom);
            if (bUpdateSuccsess == false)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.UPDATE_ROOM_FAILED, ErrorCode.UPDATE_ROOM_FAILED);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMember.GetMemberFullName(), ErrorConstant.UPDATE_ROOM_FAILED, SOURCE);
                return objResponse;
            }

            objInterviewRoom = await MemberRegisterMemory.UpdateInterviewRoom(objRoom.RecruitId, objRoom.Id, false);
            objResponse = ResponseManager.GetSuccsessResp(SuccsessConstant.UPDATE_ROOM_SUCCSESS, objInterviewRoom);
            return objResponse;
        }

        /// <summary>
        /// Update Recruitment
        /// </summary>
        /// <param name="x_objRequestModel"></param>
        /// <param name="x_strAccountId"></param>
        /// <param name="x_objContext"></param>
        /// <returns></returns>
        public async Task<ResponseModel> UpdateRecruitment(UpdateRecruitmentRequestModel x_objRequestModel, string x_strAccountId, HttpContext x_objContext)
        {
            RecruitmentResponseModel objRecruitmentRes;
            RecruitmentInfo objRecruitmentInfo;
            MemberDecryptModel objMember;
            Accounts objAccount;
            Roles objRole;
            ResponseModel objResponse;
            DateTime objCurrentDate;
            DateTime objEndDate;
            string strClientIp;
            string strContent;
            string strOutVal;
            int nCountValueChange;
            bool bIsValid;

            //----------------------------
            // Get IP address
            //----------------------------
            strClientIp = x_objContext.GetClientIPAddress();
            //----------------------------
            // check null model
            //----------------------------
            if (x_objRequestModel == null)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.UPDATE_RECRUITMENT_MODEL_IS_NULL, ErrorCode.UPDATE_RECRUITMENT_MODEL_IS_NULL);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, ErrorConstant.UPDATE_RECRUITMENT_MODEL_IS_NULL, SOURCE);
                return objResponse;
            }
            //----------------------------
            // Get login account
            //----------------------------
            objAccount = await GetAccount(x_strAccountId);
            if ((objAccount == null) || (objAccount.IsLocked == true))
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.ACCOUNT_IS_LOCKER, ErrorCode.ACCOUNT_IS_LOCKER);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, ErrorConstant.ACCOUNT_IS_LOCKER, SOURCE);
                return objResponse;
            }
            //----------------------------
            // Get login Role
            //----------------------------
            objRole = await RolesDbManager.GetRoleById(objAccount.RoleId);
            if ((objRole == null) || (objRole.Permissions.Contains((int)DTUSVCPermission.UPDATE_RECRUITMENT) == false))
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.AUTHORITY_INSUFFICIENT, ErrorCode.AUTHORITY_INSUFFICIENT);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, ErrorConstant.AUTHORITY_INSUFFICIENT, SOURCE);
                return objResponse;
            }
            //----------------------------
            // Get login member
            //----------------------------
            objMember = MemberInfoManager.GetMemberById(objAccount.MemberId);
            if ((objMember == null) || (objMember.IsMember == false))
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.AUTHORITY_INSUFFICIENT, ErrorCode.AUTHORITY_INSUFFICIENT);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, ErrorConstant.AUTHORITY_INSUFFICIENT, SOURCE);
                return objResponse;
            }
            //----------------------------
            // Get recruitment
            //----------------------------
            objRecruitmentInfo = await RecruitmentDbManager.GetRecruitmentById(x_objRequestModel.Id);
            if (objRecruitmentInfo == null)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.RECRUITMENT_IS_NULL, ErrorCode.RECRUITMENT_IS_NULL);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMember.GetMemberFullName(), ErrorConstant.RECRUITMENT_IS_NULL, SOURCE);
                return objResponse;
            }
            // Check valid
            objCurrentDate = DateTimeHelper.GetCurrentDateTimeVN();
            if ((objRecruitmentInfo.IsStop == true) || (objRecruitmentInfo.EndTime < objCurrentDate))
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.RECRUITMENT_IS_EXPIRED, ErrorCode.RECRUITMENT_IS_EXPIRED);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMember.GetMemberFullName(), ErrorConstant.RECRUITMENT_IS_EXPIRED, SOURCE);
                return objResponse;
            }
            nCountValueChange = 0;
            if (string.IsNullOrWhiteSpace(x_objRequestModel.Title) == false)
            {
                objRecruitmentInfo.Title = x_objRequestModel.Title;
                nCountValueChange++;
            }
            strContent = x_objRequestModel.Content.RemoveHTMLTag();
            if (string.IsNullOrWhiteSpace(strContent) == false)
            {
                objRecruitmentInfo.Content = strContent;
                nCountValueChange++;
            }
            bIsValid = x_objRequestModel.EndTime.CheckDateTimeFormat(out strOutVal);
            if (bIsValid == true)
            {
                objEndDate = x_objRequestModel.EndTime.ConvertStrToDateTime();
                if (objEndDate > objCurrentDate)
                {
                    objRecruitmentInfo.EndTime = objEndDate;
                    nCountValueChange++;
                }
            }
            if (nCountValueChange == 0)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.UPDATE_RECRUITMENT_MODEL_IS_NULL, ErrorCode.UPDATE_RECRUITMENT_MODEL_IS_NULL);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMember.GetMemberFullName(), ErrorConstant.UPDATE_RECRUITMENT_MODEL_IS_NULL, SOURCE);
                return objResponse;
            }
            objRecruitmentInfo.CreateId = objMember.Id;
            // Update to database
            bIsValid = await RecruitmentDbManager.UpdateRecruitment(objRecruitmentInfo);
            if (bIsValid == false)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.UPDATE_RECRUITMENT_FAILED, ErrorCode.UPDATE_RECRUITMENT_FAILED);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMember.GetMemberFullName(), ErrorConstant.UPDATE_RECRUITMENT_FAILED, SOURCE);
                return objResponse;
            }
            // Create response model
            objRecruitmentRes = ConvertRecruitmentDbToModel(objRecruitmentInfo, objMember);
            objResponse = ResponseManager.GetSuccsessResp(SuccsessConstant.UPDATE_RECRUITMENT_SUCCSESS, objRecruitmentRes);
            return objResponse;
        }

        /// <summary>
        /// Update Review
        /// </summary>
        /// <param name="x_objRequestModel"></param>
        /// <param name="x_strAccountId"></param>
        /// <param name="x_objContext"></param>
        /// <returns></returns>
        public async Task<ResponseModel> UpdateReview(UpdateReviewReqeusrModel x_objRequestModel, string x_strAccountId, HttpContext x_objContext)
        {
            Accounts objAccount;
            MemberDecryptModel objMember;
            Roles objRole;
            RecruitmentInfo objRecruitmentInfo;
            InterviewRooms objRoom;
            ResponseModel objResponse;
            MemberInterviewRoom objMemberInRoom;
            MemberRegisterDecryptModel objMembershipRegistration;
            MemberInRoomModel objMemberInRoomModel;

            DateTime objCurrentDate;
            string strClientIp;

            //----------------------------
            // Get IP address
            //----------------------------
            strClientIp = x_objContext.GetClientIPAddress();
            //----------------------------
            // check null model
            //----------------------------
            if (x_objRequestModel == null)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.UPDATE_RECRUITMENT_MODEL_IS_NULL, ErrorCode.UPDATE_RECRUITMENT_MODEL_IS_NULL);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, ErrorConstant.UPDATE_RECRUITMENT_MODEL_IS_NULL, SOURCE);
                return objResponse;
            }
            //----------------------------
            // Get login account
            //----------------------------
            objAccount = await GetAccount(x_strAccountId);
            if ((objAccount == null) || (objAccount.IsLocked == true))
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.ACCOUNT_IS_LOCKER, ErrorCode.ACCOUNT_IS_LOCKER);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, ErrorConstant.ACCOUNT_IS_LOCKER, SOURCE);
                return objResponse;
            }
            //----------------------------
            // Get login Role
            //----------------------------
            objRole = await RolesDbManager.GetRoleById(objAccount.RoleId);
            if ((objRole == null) || (objRole.Permissions.Contains((int)DTUSVCPermission.REVIEWER) == false))
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.AUTHORITY_INSUFFICIENT, ErrorCode.AUTHORITY_INSUFFICIENT);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, ErrorConstant.AUTHORITY_INSUFFICIENT, SOURCE);
                return objResponse;
            }
            //----------------------------
            // Get login member
            //----------------------------
            objMember = MemberInfoManager.GetMemberById(objAccount.MemberId);
            if ((objMember == null) || (objMember.IsMember == false))
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.AUTHORITY_INSUFFICIENT, ErrorCode.AUTHORITY_INSUFFICIENT);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, ErrorConstant.AUTHORITY_INSUFFICIENT, SOURCE);
                return objResponse;
            }
            //----------------------------
            // Get recruitment
            //----------------------------
            objRecruitmentInfo = await RecruitmentDbManager.GetRecruitmentById(x_objRequestModel.RecruitId);
            if (objRecruitmentInfo == null)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.RECRUITMENT_IS_NULL, ErrorCode.RECRUITMENT_IS_NULL);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMember.GetMemberFullName(), ErrorConstant.RECRUITMENT_IS_NULL, SOURCE);
                return objResponse;
            }
            objCurrentDate = DateTimeHelper.GetCurrentDateTimeVN();
            if ((objRecruitmentInfo.IsStop == true) || (objRecruitmentInfo.EndTime < objCurrentDate))
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.RECRUITMENT_IS_EXPIRED, ErrorCode.RECRUITMENT_IS_EXPIRED);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMember.GetMemberFullName(), ErrorConstant.RECRUITMENT_IS_EXPIRED, SOURCE);
                return objResponse;
            }
            //----------------------------------
            // Get Interview Room
            //----------------------------------
            objRoom = await RecruitmentDbManager.GetRoomById(x_objRequestModel.RoomId);
            if (objRoom == null)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.INTERVIEW_ROOM_IS_NULL, ErrorCode.INTERVIEW_ROOM_IS_NULL);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMember.GetMemberFullName(), ErrorConstant.INTERVIEW_ROOM_IS_NULL, SOURCE);
                return objResponse;
            }
            if (objRoom.EndDateTime < objCurrentDate)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.ROOM_IS_END, ErrorCode.ROOM_IS_END);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, ErrorConstant.ROOM_IS_END, SOURCE);
                return objResponse;
            }
            if (objRoom.EmailSending == true)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.ROOM_EMAIL_SENDING, ErrorCode.ROOM_EMAIL_SENDING);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, ErrorConstant.ROOM_EMAIL_SENDING, SOURCE);
                return objResponse;
            }
            if (objRoom.ManagerIds.Contains(objMember.Id) == false)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.YOU_DONT_ROOM_MANAGER, ErrorCode.YOU_DONT_ROOM_MANAGER);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, ErrorConstant.YOU_DONT_ROOM_MANAGER, SOURCE);
                return objResponse;
            }
            objMemberInRoom = await RecruitmentDbManager.GetMemberInterviewRoomById(x_objRequestModel.MemberId, x_objRequestModel.RoomId);
            if (objMemberInRoom != null)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.MEMBER_IS_IN_DEST_ROOM, ErrorCode.MEMBER_IS_IN_DEST_ROOM);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMember.GetMemberFullName(), ErrorConstant.MEMBER_IS_IN_DEST_ROOM, SOURCE);
                return objResponse;
            }
            if (objMemberInRoom.SendEmailState != (int)SendEmailType.SENT)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.MEMBER_INTERVIEW_IS_UNSEND_EMAIL, ErrorCode.MEMBER_INTERVIEW_IS_UNSEND_EMAIL);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMember.GetMemberFullName(), ErrorConstant.MEMBER_INTERVIEW_IS_UNSEND_EMAIL, SOURCE);
                return objResponse;
            }
            if (objMemberInRoom.IsChangeSchedule == true)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.MEMBER_INTERVIEW_IS_MOVE_ROOM, ErrorCode.MEMBER_INTERVIEW_IS_MOVE_ROOM);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMember.GetMemberFullName(), ErrorConstant.MEMBER_INTERVIEW_IS_MOVE_ROOM, SOURCE);
                return objResponse;
            }
            objMembershipRegistration = MemberRegisterMemory.GetMemberById(x_objRequestModel.MemberId);
            if (objMembershipRegistration == null)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.MEMBER_INTERVIEW_NULL, ErrorCode.MEMBER_INTERVIEW_NULL);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMember.GetMemberFullName(), ErrorConstant.MEMBER_INTERVIEW_NULL, SOURCE);
                return objResponse;
            }
            if (objMembershipRegistration.ApprovalInfo != null)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.MEMBER_INTERVIEW_NULL, ErrorCode.MEMBER_INTERVIEW_NULL);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMember.GetMemberFullName(), ErrorConstant.MEMBER_INTERVIEW_NULL, SOURCE);
                return objResponse;
            }
            // Validate
            if (x_objRequestModel.Scorces < 0 || x_objRequestModel.Scorces > 10)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.SCORCES_INVALID, ErrorCode.SCORCES_INVALID);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMember.GetMemberFullName(), ErrorConstant.SCORCES_INVALID, SOURCE);
                return objResponse;
            }
            if (string.IsNullOrWhiteSpace(x_objRequestModel.ReviewContent) == true)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.REVIEW_CONTENT_IS_NULL, ErrorCode.REVIEW_CONTENT_IS_NULL);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMember.GetMemberFullName(), ErrorConstant.REVIEW_CONTENT_IS_NULL, SOURCE);
                return objResponse;
            }
            // Update review
            if (objMembershipRegistration.InterviewInfo == null)
            {
                objMembershipRegistration.InterviewInfo = new InterviewDecryptInfo();
            }
            objMembershipRegistration.InterviewInfo.ReviewDate = DateTimeHelper.GetCurrentDateTimeVN();
            objMembershipRegistration.InterviewInfo.Reviewer = objMember;
            objMembershipRegistration.InterviewInfo.Scores = x_objRequestModel.Scorces;
            objMembershipRegistration.InterviewInfo.ReviewContent = x_objRequestModel.ReviewContent;
            objMembershipRegistration.InterviewInfo.Note = x_objRequestModel.Note;
            await MemberRegisterMemory.UpdateMember(objMembershipRegistration);

            objMemberInRoom.IsInterviewed = true;
            RecruitmentDbManager.UpdateMemberInRoom(objMemberInRoom); 
            objMemberInRoomModel = await MemberRegisterMemory.UpdateMemberToInterview(objMemberInRoom.RecruitId,
                                                            objMemberInRoom.RoomId,
                                                            objMemberInRoom.Id,
                                                            false);
            objResponse = ResponseManager.GetSuccsessResp(SuccsessConstant.UPDATE_REVIEW_SUCCSESS, objMemberInRoomModel);
            return objResponse;
        }

        /// <summary>
        /// Get Room By Recruitment Id
        /// </summary>
        /// <param name="x_strRecruitmentId"></param>
        /// <param name="x_strAccountId"></param>
        /// <param name="x_objContext"></param>
        /// <returns></returns>
        public async Task<ResponseModel> GetRoomByRecruitmentId(string x_strRecruitmentId, string x_strAccountId, HttpContext x_objContext)
        {
            Accounts objAccount;
            MemberDecryptModel objMember;
            Roles objRole;
            ResponseModel objResponse;
            RecruitmentInfo objRecruitmentInfo;
            List<InterviewRoomReponse> lstInterviewRoom;
            string strClientIp;
            //----------------------------
            // Get IP address
            //----------------------------
            strClientIp = x_objContext.GetClientIPAddress();
            //----------------------------
            // Get login account
            //----------------------------
            objAccount = await GetAccount(x_strAccountId);
            if ((objAccount == null) || (objAccount.IsLocked == true))
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.ACCOUNT_IS_LOCKER, ErrorCode.ACCOUNT_IS_LOCKER);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, ErrorConstant.ACCOUNT_IS_LOCKER, SOURCE);
                return objResponse;
            }
            //----------------------------
            // Get login Role
            //----------------------------
            objRole = await RolesDbManager.GetRoleById(objAccount.RoleId);
            if ((objRole == null) || (objRole.Permissions.Contains((int)DTUSVCPermission.REVIEWER) == false))
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.AUTHORITY_INSUFFICIENT, ErrorCode.AUTHORITY_INSUFFICIENT);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, ErrorConstant.AUTHORITY_INSUFFICIENT, SOURCE);
                return objResponse;
            }
            //----------------------------
            // Get login member
            //----------------------------
            objMember = MemberInfoManager.GetMemberById(objAccount.MemberId);
            if ((objMember == null) || (objMember.IsMember == false))
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.AUTHORITY_INSUFFICIENT, ErrorCode.AUTHORITY_INSUFFICIENT);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, ErrorConstant.AUTHORITY_INSUFFICIENT, SOURCE);
                return objResponse;
            }
            //----------------------------
            // Get recruitment
            //----------------------------
            objRecruitmentInfo = await RecruitmentDbManager.GetRecruitmentById(x_strRecruitmentId);
            if (objRecruitmentInfo == null)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.RECRUITMENT_IS_NULL, ErrorCode.RECRUITMENT_IS_NULL);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMember.GetMemberFullName(), ErrorConstant.RECRUITMENT_IS_NULL, SOURCE);
                return objResponse;
            }
            lstInterviewRoom = MemberRegisterMemory.GetRoomsByRecruitId(x_strRecruitmentId);
            objResponse = ResponseManager.GetSuccsessResp(string.Empty, lstInterviewRoom, lstInterviewRoom != null ? lstInterviewRoom.Count : 0);
            return objResponse;
        }

        #endregion
        #endregion
    }
}
