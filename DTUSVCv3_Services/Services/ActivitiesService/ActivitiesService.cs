﻿using DTUSVCv3_EntityFramework;
using DTUSVCv3_Library;
using DTUSVCv3_Library.LibraryModel;
using DTUSVCv3_MemoryData;
using DTUSVCv3_MemoryData.EventData;
using DTUSVCv3_MemoryData.ForgotPasswData;
using DTUSVCv3_Services.Models.ActivitiesModel;
using Microsoft.AspNetCore.Http;
using System.Drawing;

namespace DTUSVCv3_Services
{
    public class ActivitiesService : BaseSerivce, IActivitiesService
    {
        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        public ActivitiesService()
            : base()
        {
        }
        #endregion

        #region Types
        private const string SOURCE = "Activities Service";
        #endregion

        #region Methods
        /// <summary>
        /// Activity Validate
        /// </summary>
        /// <param name="x_objRequest"></param>
        /// <param name="x_objAddressInfo"></param>
        /// <returns></returns>
        private ResponseModel ActivityValidate(CreateActivityRequestModel x_objRequest, out AddressInfo x_objAddressInfo)
        {
            ResponseModel objResponseModel;
            Provinces objProvince;
            Districts objDistrict;
            Wards objWard;
            AddressInfo objAddress;
            DateTime objCurrentTime;
            DateTime objStartTime;
            DateTime objEndTime;
            DateTime objDeadlineTime;
            bool bIsUpdate;
            string strActivityId;
            string strBuf;

            objResponseModel = new ResponseModel();
            x_objAddressInfo = null;
            // Check Update
            bIsUpdate = false;
            if (x_objRequest.GetType() == typeof(UpdateActivityRequestModel))
            {
                strActivityId = ((UpdateActivityRequestModel)(x_objRequest))?.Id;
                bIsUpdate = true;
                if (strActivityId.MongoIdIsValid() == false)
                {
                    objResponseModel = ResponseManager.GetErrorResp(ErrorConstant.ACTIVITY_IS_NULL, ErrorCode.ACTIVITY_IS_NULL);
                    return objResponseModel;
                }
            }

            // Check Title
            if (string.IsNullOrWhiteSpace(x_objRequest.Title) == true)
            {
                objResponseModel = ResponseManager.GetErrorResp(ErrorConstant.ACTIVITY_TITLE_INVALID, ErrorCode.ACTIVITY_TITLE_INVALID);
                return objResponseModel;
            }
            x_objRequest.Title = x_objRequest.Title.Trim();

            // Check Content
            strBuf = x_objRequest.Content.RemoveHTMLTag();
            if (string.IsNullOrWhiteSpace(strBuf) == true)
            {
                objResponseModel = ResponseManager.GetErrorResp(ErrorConstant.ACTIVITY_CONTENT_INVALID, ErrorCode.ACTIVITY_CONTENT_INVALID);
                return objResponseModel;
            }
            x_objRequest.Content = x_objRequest.Content.Trim();

            // Check Poster
            if (bIsUpdate == false || x_objRequest.Poster != null)
            {
                if (x_objRequest.Poster == null)
                {
                    objResponseModel = ResponseManager.GetErrorResp(ErrorConstant.ACTIVITY_POSTER_INVALID, ErrorCode.ACTIVITY_POSTER_INVALID);
                    return objResponseModel;
                }

                if (x_objRequest.Poster.FileIsValid(ContentType.IMAGE_CONTENT_TYPE, out strBuf) == false)
                {
                    if (strBuf.EqualsIgnoreCase(ErrorConstant.FILE_DOWNLOADED_IN_WRONG_FORMAT) == true)
                    {
                        strBuf = string.Format(ErrorConstant.FILE_DOWNLOADED_IN_WRONG_FORMAT, string.Join(" | ", ContentType.IMAGE_CONTENT_TYPE));
                    }
                    objResponseModel = ResponseManager.GetErrorResp(strBuf, ErrorCode.ACTIVITY_POSTER_INVALID);
                    return objResponseModel;
                }

                // Scan virus
                strBuf = x_objRequest.Poster.ScanVirus();
                if (strBuf.EqualsIgnoreCase(VirusScanner.EXIST_VIRUS) == true)
                {
                    objResponseModel = ResponseManager.GetErrorResp(strBuf, ErrorCode.ACTIVITY_POSTER_INVALID);
                    return objResponseModel;
                }
            }

            // Check address
            objResponseModel = CheckAddress(x_objRequest.ProvinceId, x_objRequest.DistrictId, x_objRequest.WardId, x_objRequest.SpecificAddress,
                                            out objAddress, out objProvince, out objDistrict, out objWard);
            if (objResponseModel.IsSuccsess == false)
            {
                return objResponseModel;
            }
            x_objAddressInfo = objAddress;

            objCurrentTime = DateTimeHelper.GetCurrentDateTimeVN();
            // Check Start Date
            if (x_objRequest.StartDate.CheckDateTimeFormat(out strBuf) == false)
            {
                objResponseModel = ResponseManager.GetErrorResp(ErrorConstant.ACTIVITY_START_TIME_INVALID, ErrorCode.ACTIVITY_START_TIME_INVALID);
                return objResponseModel;
            }
            objStartTime = x_objRequest.StartDate.ConvertStrToDateTime();
            if (bIsUpdate == false)
            {
                if (objCurrentTime > objStartTime)
                {
                    objResponseModel = ResponseManager.GetErrorResp(ErrorConstant.ACTIVITY_START_TIME_INVALID, ErrorCode.ACTIVITY_START_TIME_INVALID);
                    return objResponseModel;
                }
            }

            // Check End Date
            if (x_objRequest.EndDate.CheckDateTimeFormat(out strBuf) == false)
            {
                objResponseModel = ResponseManager.GetErrorResp(ErrorConstant.ACTIVITY_END_TIME_INVALID, ErrorCode.ACTIVITY_END_TIME_INVALID);
                return objResponseModel;
            }
            objEndTime = x_objRequest.EndDate.ConvertStrToDateTime();
            if ((objStartTime >= objEndTime) || (objEndTime < objCurrentTime))
            {
                objResponseModel = ResponseManager.GetErrorResp(ErrorConstant.ACTIVITY_END_TIME_INVALID, ErrorCode.ACTIVITY_END_TIME_INVALID);
                return objResponseModel;
            }

            // Check Deadline Date
            if (x_objRequest.RegistrationDeadline.CheckDateTimeFormat(out strBuf) == false)
            {
                objResponseModel = ResponseManager.GetErrorResp(ErrorConstant.ACTIVITY_DEADLINE_TIME_INVALID, ErrorCode.ACTIVITY_DEADLINE_TIME_INVALID);
                return objResponseModel;
            }
            objDeadlineTime = x_objRequest.RegistrationDeadline.ConvertStrToDateTime();
            if ((objStartTime > objDeadlineTime) || (objEndTime < objDeadlineTime))
            {
                objResponseModel = ResponseManager.GetErrorResp(ErrorConstant.ACTIVITY_DEADLINE_TIME_INVALID, ErrorCode.ACTIVITY_DEADLINE_TIME_INVALID);
                return objResponseModel;
            }

            return objResponseModel;
        }

        /// <summary>
        /// Convert Activity Db To Model
        /// </summary>
        /// <param name="x_objActivityDb"></param>
        /// <returns></returns>
        private async Task<ActivityResponseModel> ConvertActivityDbToModel(EventInfo x_objActivityDb)
        {
            ActivityResponseModel objActivityResponseModel;
            AddressInfoModel objAddressModel;
            MemberDecryptModel objCreater;
            string strFilePath;

            if (x_objActivityDb == null)
            {
                return null;
            }
            // Get address
            objAddressModel = await GetAddressModel(x_objActivityDb.AddressId);
            strFilePath = Path.Combine(SysFolder.ACTIVITY_POSTER_PATH, x_objActivityDb.PosterPath);
            // Get Creater
            objCreater = MemberInfoManager.GetMemberById(x_objActivityDb.CreaterId);
            objActivityResponseModel = new ActivityResponseModel
            {
                PosterPath = strFilePath,
                Address = objAddressModel,
                Content = x_objActivityDb.Content,
                CreaterName = objCreater.GetMemberFullName(),
                CreateTime = x_objActivityDb.CreateTime,
                EndDate = x_objActivityDb.EndDate,
                Id = x_objActivityDb.Id,
                IsPublic = x_objActivityDb.IsPublic,
                RegistrationDeadline = x_objActivityDb.RegistrationDeadline,
                StartDate = x_objActivityDb.StartDate,
                Title = x_objActivityDb.Title
            };
            return objActivityResponseModel;
        }

        /// <summary>
        /// Setting Text Validate
        /// </summary>
        /// <param name="x_objRequest"></param>
        /// <returns></returns>
        private ResponseModel SettingTextValidate(SetupCertificateTextRequestModel x_objRequest)
        {
            ResponseModel objResponse;
            System.Drawing.Color objColor;

            if (x_objRequest == null)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.MEMBER_REGISTER_INVALID, ErrorCode.MEMBER_REGISTER_INVALID);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, string.Empty, string.Empty, $"{SOURCE}: {ErrorConstant.MEMBER_REGISTER_INVALID}", SOURCE);

                return objResponse;
            }

            if (x_objRequest.ActivityId.MongoIdIsValid() == false)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.ACTIVITY_IS_NULL, ErrorCode.ACTIVITY_IS_NULL);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, string.Empty, string.Empty, $"{SOURCE}: {ErrorConstant.ACTIVITY_IS_NULL}", SOURCE);

                return objResponse;
            }
            // Check max width
            if (x_objRequest.MaxWidth <= 100)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.MAX_WIDTH_INVALID, ErrorCode.MAX_WIDTH_INVALID);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, string.Empty, string.Empty, $"{SOURCE}: {ErrorConstant.MAX_WIDTH_INVALID}", SOURCE);

                return objResponse;
            }
            // Check max height
            if (x_objRequest.MaxHeight <= 10)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.MAX_HEIGHT_INVALID, ErrorCode.MAX_HEIGHT_INVALID);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, string.Empty, string.Empty, $"{SOURCE}: {ErrorConstant.MAX_HEIGHT_INVALID}", SOURCE);

                return objResponse;
            }
            // Check location x
            if (x_objRequest.LocationX <= 0)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.LOCATION_X_INVALID, ErrorCode.LOCATION_X_INVALID);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, string.Empty, string.Empty, $"{SOURCE}: {ErrorConstant.LOCATION_X_INVALID}", SOURCE);

                return objResponse;
            }
            // Check loaction y
            if (x_objRequest.LocationY <= 0)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.LOCATION_Y_INVALID, ErrorCode.LOCATION_Y_INVALID);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, string.Empty, string.Empty, $"{SOURCE}: {ErrorConstant.LOCATION_Y_INVALID}", SOURCE);

                return objResponse;
            }
            // Check font size
            if (x_objRequest.FontSize <= 0)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.FONT_SIZE_INVALID, ErrorCode.FONT_SIZE_INVALID);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, string.Empty, string.Empty, $"{SOURCE}: {ErrorConstant.FONT_SIZE_INVALID}", SOURCE);

                return objResponse;
            }
            // Check font Id
            if (x_objRequest.FontId.MongoIdIsValid() == false)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.FONT_INVALID, ErrorCode.FONT_INVALID);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, string.Empty, string.Empty, $"{SOURCE}: {ErrorConstant.FONT_INVALID}", SOURCE);

                return objResponse;
            }
            // Check Vertical
            if (x_objRequest.Vertical < 0 || x_objRequest.Vertical > 2)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.VERTICAL_INVALID, ErrorCode.VERTICAL_INVALID);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, string.Empty, string.Empty, $"{SOURCE}: {ErrorConstant.VERTICAL_INVALID}", SOURCE);

                return objResponse;
            }
            // Check Horizontal
            if (x_objRequest.Horizontal < 0 || x_objRequest.Horizontal > 2)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.HORIZONTAL_INVALID, ErrorCode.HORIZONTAL_INVALID);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, string.Empty, string.Empty, $"{SOURCE}: {ErrorConstant.HORIZONTAL_INVALID}", SOURCE);

                return objResponse;
            }
            // Check color
            if (TextFontHelper.TryParseStringToColor(x_objRequest.Color, out objColor) == false)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.COLOR_INVALID, ErrorCode.COLOR_INVALID);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, string.Empty, string.Empty, $"{SOURCE}: {ErrorConstant.COLOR_INVALID}", SOURCE);

                return objResponse;
            }
            // Check font style
            if ((x_objRequest.FontStyle.EqualsIgnoreCase(FontStyle.Regular.ToString()) == false) &&
                x_objRequest.FontStyle.EqualsIgnoreCase(FontStyle.Bold.ToString()) == false &&
                x_objRequest.FontStyle.EqualsIgnoreCase(FontStyle.Italic.ToString()) == false &&
                x_objRequest.FontStyle.EqualsIgnoreCase(FontStyle.Underline.ToString()) == false &&
                x_objRequest.FontStyle.EqualsIgnoreCase(FontStyle.Strikeout.ToString()) == false)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.FONT_STYLE_INVALID, ErrorCode.FONT_STYLE_INVALID);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, string.Empty, string.Empty, $"{SOURCE}: {ErrorConstant.FONT_STYLE_INVALID}", SOURCE);

                return objResponse;
            }

            return new ResponseModel();
        }

        /// <summary>
        /// Send Certificate
        /// </summary>
        /// <param name="x_objRequest"></param>
        /// <returns></returns>
        private async Task SendCertificate(SendCertificateRequestModel x_objRequest)
        {
            const string FILE_NAME_FORMAT = "{0}_{1}.png";
            const string EMAIL_SUBJECT_FOMAT = "Giấy chứng nhận {0} - {1}";
            List<CertificateSettingText> lstSettingText;
            List<DrawTextSetting> lstDrawTextSetting;
            List<MemberRegistEvtModel> lstMemberRegistEvt;
            EventInfo objEventInfo;
            MemberRegistEvtModel objMemberRegistEvtModel;
            DrawTextInfo objDrawTextInfo;
            DrawTextSetting objDrawTextSetting;
            AddressInfoModel objAddressInfoModel;
            Bitmap objCertificate;
            SendEmailModel objSendEmailModel;
            string strFileName;
            string strFilePath;
            string strActivityName;
            string strAddress;
            string strEmailContent;
            string strSubjectBody;
            bool bResult;
            // Get Event info
            objEventInfo = await ActivityDbManager.GetEventInfoById(x_objRequest.ActivityId);
            if (objEventInfo == null)
            {
                return;
            }
            objAddressInfoModel = await GetAddressModel(objEventInfo.AddressId);
            strAddress = objAddressInfoModel.ToString();

            // Get setting text
            lstSettingText = await ActivityDbManager.GetSettingTextByActivityId(objEventInfo.Id);
            if (lstSettingText == null)
            {
                lstSettingText = new List<CertificateSettingText>();
            }
            lstDrawTextSetting = new List<DrawTextSetting>();
            foreach (CertificateSettingText objSetting in lstSettingText)
            {
                objDrawTextSetting = await ConvertTextDbToModel(objSetting);
                if (objDrawTextSetting == null)
                {
                    continue;
                }
                lstDrawTextSetting.Add(objDrawTextSetting);
            }

            // Get Member
            if (x_objRequest.IsSendAll == true)
            {
                lstMemberRegistEvt = MemberRegistEvtData.Instant.GetListMemberRegistEvt(x_objRequest.ActivityId);
            }
            else
            {
                lstMemberRegistEvt = new List<MemberRegistEvtModel>();
                foreach (string objMemberId in x_objRequest.MemberIds)
                {
                    objMemberRegistEvtModel = MemberRegistEvtData.Instant.GetMemberRegisEvt(objMemberId, x_objRequest.ActivityId);
                    if ((objMemberRegistEvtModel == null) || (objMemberRegistEvtModel.TotalScore <= 0))
                    {
                        continue;
                    }
                    lstMemberRegistEvt.Add(objMemberRegistEvtModel);
                }
            }
            if (lstMemberRegistEvt.Count == 0)
            {
                return;
            }

            // Create Image
            // Open Certificate
            strFilePath = FileHelper.GetFilePath(SysFolder.ACTIVITY_CERTIFICATE_PATH, objEventInfo.CertificateFile);
            strActivityName = FileHelper.CreateFileNameByTitle(objEventInfo.Title);
            foreach (MemberRegistEvtModel objMember in lstMemberRegistEvt)
            {
                if (objMember.TotalScore == 0)
                {
                    continue;
                }

                objCertificate = FileHelper.OpenImageFile(strFilePath);
                // Create new file name
                strFileName = string.Format(FILE_NAME_FORMAT, objMember.StudentId, strActivityName);
                strFilePath = FileHelper.GetFilePath(SysFolder.ACTIVITY_CERTIFICATE_PATH, strFileName);

                objDrawTextInfo = new DrawTextInfo
                {
                    StudentId = objMember.StudentId,
                    ClassName = objMember.ClassName,
                    FacultyName = objMember.Faculty.FacultyName,
                    FirstName = objMember.FirstName,
                    LastName = objMember.LastName
                };

                bResult = ImageHelper.CreateCertificate(objCertificate, lstDrawTextSetting, strFilePath, false, objDrawTextInfo);
                if (bResult == false)
                {
                    continue;
                }
                // Send Email
                objMember.SendCertificate = DateTimeHelper.GetCurrentDateTimeVN();
                await MemberRegistEvtData.Instant.UpdateMemberRegister(objMember);
                strEmailContent = GetEmailContent(objMember, objEventInfo.Title, strAddress);
                strSubjectBody = string.Format(EMAIL_SUBJECT_FOMAT, objEventInfo.Title, objMember.StudentId);
                objSendEmailModel = new SendEmailModel
                {
                    AttachFilePath = new List<string> { strFilePath },
                    DeleteFileAfterSent = true,
                    EmailBody = strEmailContent,
                    EmailRecipient = objMember.Email,
                    IsSendCC = true,
                    EmailType = EmailType.CERTIFICATE,
                    Subject = strSubjectBody,
                };
                EmailHelper.Instant.EmailNomalQueue.Enqueue(objSendEmailModel);
                Thread.Sleep(10);
            }
            // Update Activity state
            objEventInfo.IsCertificateSending = true;
            await ActivityDbManager.UpdateEventInfo(objEventInfo);
        }

        /// <summary>
        /// Get Email Content
        /// </summary>
        /// <param name="x_objMember"></param>
        /// <param name="x_strActivityName"></param>
        /// <param name="x_strAddress"></param>
        /// <returns></returns>
        private string GetEmailContent(MemberRegistEvtModel x_objMember, string x_strActivityName, string x_strAddress)
        {
            const string FORMAT_DATE_TIME = "HH:mm:ss dd/MM/yyyy";
            const string FRONT_END_FORMAT = "https://dtusvc.com/ConfirmCertificate?x_strStudentId={0}&x_strActivityId={1}";
            string strFilePath;
            string strEmailMainTemp;
            string strEmailMainContent;
            string strEmailTableTemp;
            string strEmailTableContent;
            string strEmailScoreTemp;
            string strEmailScoreContent;
            string strCertificateConfirmPath;

            // Read email temp
            strFilePath = FileHelper.GetFilePath(SysFolder.EMAIL_TEMP_FOLDER, SysFile.SEND_CERTIFICATE);
            strEmailMainTemp = FileHelper.ReadTextFile(strFilePath);
            strFilePath = FileHelper.GetFilePath(SysFolder.EMAIL_TEMP_FOLDER, SysFile.TABLE_CHECK_IN);
            strEmailTableTemp = FileHelper.ReadTextFile(strFilePath);
            strFilePath = FileHelper.GetFilePath(SysFolder.EMAIL_TEMP_FOLDER, SysFile.TABLE_SCORE);
            strEmailScoreTemp = FileHelper.ReadTextFile(strFilePath);

            strEmailTableContent = string.Empty;
            if (x_objMember.EvtCheckList != null)
            {
                foreach (ActivityChecklistData objCheckIn in x_objMember.EvtCheckList)
                {
                    if (objCheckIn.Score == 0)
                    {
                        continue;
                    }
                    strEmailTableContent += strEmailTableTemp.Replace(EmailReplaceText.CHECK_IN_TIME, objCheckIn.EntryTime.ToString(FORMAT_DATE_TIME))
                                                             .Replace(EmailReplaceText.CHECK_OUT_TIME, objCheckIn.TimeToLeave.ToString(FORMAT_DATE_TIME))
                                                             .Replace(EmailReplaceText.MANAGER_NAME, objCheckIn.ManagerName)
                                                             .Replace(EmailReplaceText.SCORE, objCheckIn.Score.ToString("F1"));
                    strEmailTableContent += "\n";
                }
            }

            strEmailScoreContent = strEmailScoreTemp.Replace(EmailReplaceText.TOTAL_SCORE, x_objMember.TotalScore.ToString("F1"));
            strCertificateConfirmPath = string.Format(FRONT_END_FORMAT, x_objMember.StudentId, x_objMember.EventId);
            strEmailMainContent = strEmailMainTemp.Replace(EmailReplaceText.MEMBER_FULL_NAME, x_objMember.GetMemberFullName())
                                                  .Replace(EmailReplaceText.STUDENT_ID, x_objMember.StudentId)
                                                  .Replace(EmailReplaceText.MEMBER_NAME, x_objMember.LastName)
                                                  .Replace(EmailReplaceText.ACTIVITY_NAME, x_strActivityName)
                                                  .Replace(EmailReplaceText.ADDRESS, x_strAddress)
                                                  .Replace(EmailReplaceText.TABLE_CHECK_IN_CONTENT, strEmailTableContent)
                                                  .Replace(EmailReplaceText.TOTAL_SCORE_TABLE, strEmailScoreContent)
                                                  .Replace(EmailReplaceText.CERTIFICATE_CONFIRM_PATH, strCertificateConfirmPath);
            return strEmailMainContent;
        }

        /// <summary>
        /// Create Certificate Temp
        /// </summary>
        /// <param name="x_strActivityId"></param>
        /// <returns></returns>
        private async Task<string> CreateCertificateTemp(string x_strActivityId)
        {
            const string FILE_NAME_FORMAT = "Temp_{0}";
            EventInfo objEventInfo;
            List<CertificateSettingText> lstSettingText;
            List<DrawTextSetting> lstDrawTextSetting;
            DrawTextSetting objDrawTextSetting;
            Bitmap objCertificate;
            string strFileName;
            string strFilePath;

            objEventInfo = await ActivityDbManager.GetEventInfoById(x_strActivityId);
            if (objEventInfo == null)
            {
                return string.Empty;
            }

            if (string.IsNullOrEmpty(objEventInfo.CertificateFile) == true)
            {
                return string.Empty;
            }

            strFilePath = FileHelper.GetFilePath(SysFolder.ACTIVITY_CERTIFICATE_PATH, objEventInfo.CertificateFile);
            if (File.Exists(strFilePath) == false)
            {
                return string.Empty;
            }

            lstSettingText = await ActivityDbManager.GetSettingTextByActivityId(x_strActivityId);
            if ((lstSettingText == null) || (lstSettingText.Count == 0))
            {
                strFilePath = Path.Combine(SysFolder.ACTIVITY_CERTIFICATE_PATH, objEventInfo.CertificateFile);
                return strFilePath;
            }
            // Convert setting Text To model
            lstDrawTextSetting = new List<DrawTextSetting>();
            foreach (CertificateSettingText objSettingText in lstSettingText)
            {
                objDrawTextSetting = await ConvertTextDbToModel(objSettingText);
                if (objDrawTextSetting == null)
                {
                    continue;
                }
                lstDrawTextSetting.Add(objDrawTextSetting);
            }
            // Openb Certificate
            objCertificate = FileHelper.OpenImageFile(strFilePath);
            // Create new file name
            strFileName = string.Format(FILE_NAME_FORMAT, objEventInfo.CertificateFile);
            strFilePath = FileHelper.GetFilePath(SysFolder.ACTIVITY_CERTIFICATE_PATH, strFileName);
            ImageHelper.CreateCertificate(objCertificate, lstDrawTextSetting, strFilePath);
            return Path.Combine(SysFolder.ACTIVITY_CERTIFICATE_PATH, strFileName);
        }

        /// <summary>
        /// Convert Text Db To Model
        /// </summary>
        /// <param name="x_objCertificateSettingText"></param>
        /// <returns></returns>
        private async Task<DrawTextSetting> ConvertTextDbToModel(CertificateSettingText x_objCertificateSettingText)
        {
            DrawTextSetting objDrawTextSetting;
            List<CertificateTextFont> lstTextFont;
            CertificateTextFont objTextFont;
            string strFilePath;
            if (x_objCertificateSettingText == null)
            {
                return null;
            }

            lstTextFont = await ActivityDbManager.GetAllTestFont();
            if (lstTextFont == null || lstTextFont.Count == 0)
            {
                return null;
            }

            objTextFont = lstTextFont.FirstOrDefault(obj => obj.Id.Equals(x_objCertificateSettingText.FontId) == true);
            if (objTextFont == null)
            {
                return null;
            }
            strFilePath = FileHelper.GetFilePath(SysFolder.FONT_FILE_FOLDER, objTextFont.FontPath);
            objDrawTextSetting = new DrawTextSetting
            {
                FontPath = strFilePath,
                Color = x_objCertificateSettingText.Color,
                DefaultText = x_objCertificateSettingText.DefaultText,
                FontSize = x_objCertificateSettingText.FontSize,
                Horizontal = x_objCertificateSettingText.Horizontal,
                LocationX = x_objCertificateSettingText.LocationX,
                LocationY = x_objCertificateSettingText.LocationX,
                MaxHeight = x_objCertificateSettingText.MaxHeight,
                MaxWidth = x_objCertificateSettingText.MaxWidth,
                ValueType = x_objCertificateSettingText.ValueType,
                Vertical = x_objCertificateSettingText.Vertical,
                FontStyleStr = x_objCertificateSettingText.FontStyle
            };
            return objDrawTextSetting;
        }

        /// <summary>
        /// Member Info Validate
        /// </summary>
        /// <param name="x_objRequest"></param>
        /// <returns></returns>
        private async Task<ResponseModel> MemberInfoValidate(CommunityRegisterRequestModel x_objRequest, string x_strClientIp)
        {
            ResponseModel objResponse;
            Facultys objFaculty;
            EventInfo objEvt;
            CaptchaModel objCaptchaModel;
            DateTime objCurrentDate;
            string strFullName;

            if (x_objRequest == null)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.MEMBER_REGISTER_INVALID, ErrorCode.MEMBER_REGISTER_INVALID);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, x_strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.MEMBER_REGISTER_INVALID}", SOURCE);

                return objResponse;
            }

            // Check full name
            if ((string.IsNullOrWhiteSpace(x_objRequest.FirstName) == true) || (string.IsNullOrWhiteSpace(x_objRequest.LastName) == true))
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.MEMBER_REGISTER_FULL_NAME_INVALID, ErrorCode.MEMBER_REGISTER_FULL_NAME_INVALID);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, x_strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.MEMBER_REGISTER_FULL_NAME_INVALID}", SOURCE);

                return objResponse;
            }
            strFullName = string.Format("{0} {1}", x_objRequest.FirstName, x_objRequest.LastName);
            if (strFullName.CheckNameIsValid() == false)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.MEMBER_REGISTER_FULL_NAME_INVALID, ErrorCode.MEMBER_REGISTER_FULL_NAME_INVALID);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, x_strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.MEMBER_REGISTER_FULL_NAME_INVALID}", SOURCE);

                return objResponse;
            }
            x_objRequest.FirstName = x_objRequest.FirstName.Trim();
            x_objRequest.FirstName = x_objRequest.FirstName.ToTitleCase();
            x_objRequest.LastName = x_objRequest.LastName.Trim();
            x_objRequest.LastName = x_objRequest.LastName.ToTitleCase();

            // Check Phonenumber
            if (x_objRequest.PhoneNumber.PhoneNumberIsValid() == false)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.MEMBER_REGISTER_PHONENUMBER_INVALID, ErrorCode.MEMBER_REGISTER_PHONENUMBER_INVALID);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, x_strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.MEMBER_REGISTER_PHONENUMBER_INVALID}", SOURCE);

                return objResponse;
            }
            x_objRequest.PhoneNumber = x_objRequest.PhoneNumber.Trim();

            // Check Email
            if (x_objRequest.Email.EmailIsValid() == false)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.MEMBER_REGISTER_EMAIL_INVALID, ErrorCode.MEMBER_REGISTER_EMAIL_INVALID);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, x_strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.MEMBER_REGISTER_EMAIL_INVALID}", SOURCE);

                return objResponse;
            }
            x_objRequest.Email = x_objRequest.Email.Trim();

            // Check FacultyId
            objFaculty = await FacultyDbManager.GetFacultyById(x_objRequest.FacultyId);
            if (objFaculty == null)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.MEMBER_REGISTER_FACULTY_INVALID, ErrorCode.MEMBER_REGISTER_FACULTY_INVALID);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, x_strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.MEMBER_REGISTER_FACULTY_INVALID}", SOURCE);

                return objResponse;
            }

            // Check class name
            if ((string.IsNullOrWhiteSpace(x_objRequest.ClassName) == true) || (x_objRequest.ClassName.Length < 3))
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.MEMBER_REGISTER_CLASS_NAME_INVALID, ErrorCode.MEMBER_REGISTER_CLASS_NAME_INVALID);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, x_strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.MEMBER_REGISTER_CLASS_NAME_INVALID}", SOURCE);

                return objResponse;
            }
            x_objRequest.ClassName = x_objRequest.ClassName.Trim();

            // Check Student Id
            if (MemberInfoManager.CheckStudentIdExist(x_objRequest.StudentId) == true)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.MEMBER_REGISTER_STUDENTID_IS_MEMBER, ErrorCode.MEMBER_REGISTER_STUDENTID_IS_MEMBER);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, x_strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.MEMBER_REGISTER_STUDENTID_IS_MEMBER}", SOURCE);

                return objResponse;
            }
            if (MemberRegistEvtData.Instant.CheckRegistExist(x_objRequest.StudentId, x_objRequest.EventId) == true)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.MEMBER_REGISTER_STUDENTID_IS_EXIST, ErrorCode.MEMBER_REGISTER_STUDENTID_IS_EXIST);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, x_strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.MEMBER_REGISTER_STUDENTID_IS_EXIST}", SOURCE);

                return objResponse;
            }
            x_objRequest.StudentId = x_objRequest.StudentId.Trim();

            // Check Activity Id
            objEvt = await ActivityDbManager.GetEventInfoById(x_objRequest.EventId);
            if (objEvt == null)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.ACTIVITY_IS_NULL, ErrorCode.ACTIVITY_IS_NULL);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, x_strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.ACTIVITY_IS_NULL}", SOURCE);

                return objResponse;
            }
            objCurrentDate = DateTimeHelper.GetCurrentDateTimeVN();
            if (objCurrentDate > objEvt.RegistrationDeadline)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.ACTIVITY_IS_EXPIRED_REGISTER, ErrorCode.ACTIVITY_IS_EXPIRED_REGISTER);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, x_strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.ACTIVITY_IS_EXPIRED_REGISTER}", SOURCE);

                return objResponse;
            }
            if (objEvt.IsPublic == false)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.ACTIVITY_IS_NOT_PUBLIC, ErrorCode.ACTIVITY_IS_NOT_PUBLIC);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, x_strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.ACTIVITY_IS_NOT_PUBLIC}", SOURCE);

                return objResponse;
            }

            // Check Captcha
            objCaptchaModel = ForgotPasswdManager.GetCaptcha(x_objRequest.Token);
            if ((objCaptchaModel == null) || (x_objRequest.CaptCha.Equals(objCaptchaModel.CaptchaCode) == false))
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.CAPTCHA_INVALID, ErrorCode.CAPTCHA_INVALID);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, x_strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.CAPTCHA_INVALID}", SOURCE);

                return objResponse;
            }

            objResponse = new ResponseModel();
            return objResponse;
        }

        /// <summary>
        /// Check Out All
        /// </summary>
        /// <param name="x_strManaerId"></param>
        /// <param name="x_strActivityId"></param>
        /// <returns></returns>
        private async Task CheckOutAll(string x_strManaerId, string x_strActivityId)
        {
            List<MemberRegistEvtModel> lstMemberRegistEvtModel;
            MemberRegistEvtModel objMemberRegistEvtModel;
            ActivityChecklistData objCheckInData;
            DateTime objCurrentDate;
            bool bResult;

            lstMemberRegistEvtModel = MemberRegistEvtData.Instant.GetListMemberRegistEvt(x_strActivityId);
            if (lstMemberRegistEvtModel == null)
            {
                return;
            }

            objCurrentDate = DateTimeHelper.GetCurrentDateTimeVN();
            for (int nIndex = 0; nIndex < lstMemberRegistEvtModel.Count; nIndex++)
            {
                objMemberRegistEvtModel = lstMemberRegistEvtModel[nIndex];
                bResult = objMemberRegistEvtModel.GetCheckIn(objCurrentDate, out objCheckInData);
                if ((objCheckInData == null) || (objCheckInData.ManagerId.Equals(x_strManaerId) == false) || bResult == false)
                {
                    continue;
                }
                // Check out
                objCheckInData.TimeToLeave = objCurrentDate;
                // Update check in
                await MemberRegistEvtData.Instant.UpdateCheckIn(objCheckInData);
                objMemberRegistEvtModel.TotalScore = objMemberRegistEvtModel.EvtCheckList.Sum(obj => obj.Score);
            }
        }

        /// <summary>
        /// Update Check in All
        /// </summary>
        /// <param name="x_strActivityId"></param>
        /// <param name="objStartDate"></param>
        /// <param name="objEndDate"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        private async Task UpdateCheckinAll(string x_strActivityId, DateTime x_objStartDate, DateTime x_objEndDate)
        {
            const string DATETIME_FORMAT = "dd/MM/yyyy";
            List<MemberRegistEvtModel> lstMemberRegistEvtModel;
            ActivityChecklistData objActivityChecklistData;
            MemberRegistEvtModel objMemberRegistEvtModel;
            string strStartParam;
            string strStartCheckin;

            lstMemberRegistEvtModel = MemberRegistEvtData.Instant.GetListMemberRegistEvt(x_strActivityId);
            if (lstMemberRegistEvtModel == null || lstMemberRegistEvtModel.Count == 0)
            {
                return;
            }

            strStartParam = x_objStartDate.ToString(DATETIME_FORMAT);
            for (int nIndex = 0; nIndex < lstMemberRegistEvtModel.Count; nIndex++)
            {
                objMemberRegistEvtModel = lstMemberRegistEvtModel[nIndex];

                for (int nCheckInIndex = 0; nCheckInIndex < objMemberRegistEvtModel.EvtCheckList.Count; nCheckInIndex++)
                {
                    objActivityChecklistData = objMemberRegistEvtModel.EvtCheckList[nCheckInIndex];
                    strStartCheckin = objActivityChecklistData.EntryTime.ToString(DATETIME_FORMAT);
                    if (strStartParam.Equals(strStartCheckin) == true)
                    {
                        if ((objActivityChecklistData.EntryTime == objActivityChecklistData.TimeToLeave) &&
                            (objActivityChecklistData.EntryTime < x_objEndDate))
                        {
                            objActivityChecklistData.TimeToLeave = x_objEndDate;
                            await MemberRegistEvtData.Instant.UpdateCheckIn(objActivityChecklistData);
                        }
                    }
                }
                objMemberRegistEvtModel.TotalScore = objMemberRegistEvtModel.EvtCheckList.Sum(obj => obj.Score);
            }
        }
        #endregion

        #region Implement service

        /// <summary>
        /// Check In
        /// </summary>
        /// <param name="x_strMemberId"></param>
        /// <param name="x_strActivityId"></param>
        /// <param name="x_strAccountId"></param>
        /// <param name="x_objContext"></param>
        /// <returns></returns>
        public async Task<ResponseModel> CheckIn(string x_strMemberId, string x_strActivityId, string x_strAccountId, HttpContext x_objContext)
        {
            Accounts objAccount;
            Roles objRole;
            EventInfo objEventInfo;
            MemberDecryptModel objMemberInfo;
            MemberDecryptModel objCheckinMemberInfo;
            MemberRegistEvtModel objMemberRegistEvtModel;
            ResponseModel objResponse;
            ActivityChecklistData objCheckInData;
            CheckInResponse objCheckInResponse;
            DateTime objCurrentDate;
            Task objTask;
            string strClientIp;
            bool bResult;

            //----------------------------------
            // Get IP Address
            //----------------------------------
            strClientIp = x_objContext.GetClientIPAddress();
            //----------------------------------
            // Validate request role
            //----------------------------------
            // Account
            objAccount = await GetAccount(x_strAccountId);
            if (objAccount == null)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.ACCOUNT_IS_LOCKER, ErrorCode.ACCOUNT_IS_LOCKER);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.ACCOUNT_IS_LOCKER}", SOURCE);

                return objResponse;
            }

            // Member
            objMemberInfo = MemberInfoManager.GetMemberById(objAccount.MemberId);
            if (objMemberInfo == null)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.MEMBER_INFO_IS_NULL, ErrorCode.MEMBER_INFO_IS_NULL);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.MEMBER_INFO_IS_NULL}", SOURCE);

                return objResponse;
            }

            // Role
            objRole = await RolesDbManager.GetRoleById(objAccount.RoleId);
            if ((objRole == null) || (objRole.Permissions.Contains((int)DTUSVCPermission.CHECK_IN_ACTIVITY) == false))
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.AUTHORITY_INSUFFICIENT, ErrorCode.AUTHORITY_INSUFFICIENT);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMemberInfo.GetMemberFullName(), $"{SOURCE}: {ErrorConstant.MEMBER_INFO_IS_NULL}", SOURCE);

                return objResponse;
            }

            // Get Event info
            objEventInfo = await ActivityDbManager.GetEventInfoById(x_strActivityId);
            if (objEventInfo == null)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.ACTIVITY_IS_NULL, ErrorCode.ACTIVITY_IS_NULL);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMemberInfo.GetMemberFullName(), $"{SOURCE}: {ErrorConstant.ACTIVITY_IS_NULL}", SOURCE);
                return objResponse;
            }
            // Check end date
            objCurrentDate = DateTimeHelper.GetCurrentDateTimeVN();
            if (objEventInfo.EndDate < objCurrentDate)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.ACTIVITY_IS_FINISHED, ErrorCode.ACTIVITY_IS_FINISHED);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMemberInfo.GetMemberFullName(), $"{SOURCE}: {ErrorConstant.ACTIVITY_IS_FINISHED}", SOURCE);
                return objResponse;
            }

            objMemberRegistEvtModel = MemberRegistEvtData.Instant.GetMemberRegisEvt(x_strMemberId, x_strActivityId);
            objCheckInResponse = new CheckInResponse();
            // Is Registed
            if (objMemberRegistEvtModel != null)
            {
                objCheckInResponse.FullName = objMemberRegistEvtModel.GetMemberFullName();
                objCheckInResponse.StudentId = objMemberRegistEvtModel.StudentId;
                objCheckInResponse.FacultyName = objMemberRegistEvtModel.Faculty.FacultyName;
                objCheckInResponse.IsMember = objMemberRegistEvtModel.IsMember;

                if (objMemberRegistEvtModel.MemberId.Equals(objMemberInfo.Id) == true)
                {
                    // Check out all
                    objTask = Task.Run(async () =>
                    {
                        await CheckOutAll(objMemberInfo.Id, objEventInfo.Id);
                    });
                    objCheckInResponse.IsCheckOutAll = true;
                }
                else
                {
                    bResult = objMemberRegistEvtModel.GetCheckIn(objCurrentDate, out objCheckInData);
                    if (bResult == false)
                    {
                        // Check in
                        objCheckInData = new ActivityChecklistData
                        {
                            EventsId = x_strActivityId,
                            ManagerId = objMemberInfo.Id,
                            MemberId = objMemberRegistEvtModel.MemberId
                        };

                        objCheckInResponse.IsCheckIn = true;
                    }
                    else
                    {
                        if (objCheckInData != null)
                        {
                            // Check out
                            objCheckInData.TimeToLeave = objCurrentDate;
                            objCheckInResponse.IsCheckIn = false;
                        }
                    }

                    if (objCheckInData != null)
                    {
                        // Update check in
                        bResult = await MemberRegistEvtData.Instant.UpdateCheckIn(objCheckInData);
                        if (bResult == false)
                        {
                            objResponse = ResponseManager.GetErrorResp(ErrorConstant.CHECKIN_ACTIVITY_FAILED, ErrorCode.CHECKIN_ACTIVITY_FAILED);
                            // Write log
                            LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMemberInfo.GetMemberFullName(), $"{SOURCE}: {ErrorConstant.CHECKIN_ACTIVITY_FAILED}", SOURCE);
                            return objResponse;
                        }
                        objMemberRegistEvtModel.TotalScore = objMemberRegistEvtModel.EvtCheckList.Sum(obj => obj.Score);
                    }
                }
            }
            else
            {
                objCheckinMemberInfo = MemberInfoManager.GetMemberExistById(x_strMemberId, true);
                if (objCheckinMemberInfo == null)
                {
                    objResponse = ResponseManager.GetErrorResp(ErrorConstant.MEMBER_IS_NOT_EXIST_ACTIVITY, ErrorCode.MEMBER_IS_NOT_EXIST_ACTIVITY);
                    // Write log
                    LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMemberInfo.GetMemberFullName(), $"{SOURCE}: {ErrorConstant.MEMBER_IS_NOT_EXIST_ACTIVITY}", SOURCE);
                    return objResponse;
                }

                if (objCheckinMemberInfo.Id.Equals(objMemberInfo.Id) == true)
                {
                    // Is checkout all
                    objTask = Task.Run(async () =>
                    {
                        await CheckOutAll(objMemberInfo.Id, objEventInfo.Id);
                    });
                    objCheckInResponse.IsCheckOutAll = true;
                }
                else
                {
                    // Check in
                    objCheckInData = new ActivityChecklistData
                    {
                        EventsId = x_strActivityId,
                        ManagerId = objMemberInfo.Id,
                        MemberId = objMemberRegistEvtModel.MemberId
                    };
                    // Update check in
                    bResult = await MemberRegistEvtData.Instant.UpdateCheckIn(objCheckInData);
                    if (bResult == false)
                    {
                        objResponse = ResponseManager.GetErrorResp(ErrorConstant.CHECKIN_ACTIVITY_FAILED, ErrorCode.CHECKIN_ACTIVITY_FAILED);
                        // Write log
                        LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMemberInfo.GetMemberFullName(), $"{SOURCE}: {ErrorConstant.CHECKIN_ACTIVITY_FAILED}", SOURCE);
                        return objResponse;
                    }

                    objCheckInResponse.IsCheckIn = true;
                }
            }
            objResponse = ResponseManager.GetSuccsessResp(SuccsessConstant.CHECKIN_ACTIVITY_SUCCSESS, objCheckInResponse);
            return objResponse;
        }

        /// <summary>
        /// Community Registration
        /// </summary>
        /// <param name="x_objRequest"></param>
        /// <param name="x_objContext"></param>
        /// <returns></returns>
        public async Task<ResponseModel> CommunityRegistration(CommunityRegisterRequestModel x_objRequest, HttpContext x_objContext)
        {
            ResponseModel objResponse;
            CommunityRegistEvtModel objCommunityRegistEvtModel;
            MemberRegistEvtModel objMemberRegistEvtModel;
            EventInfo objEvt;
            AddressInfoModel objAddress;
            Bitmap objQrCode;
            SendEmailModel objSendEmailModel;
            string strClientIp;
            string strQrCodeFileName;
            string strQrCodeFilePath;
            string strEmailFilePath;
            string strEmailContent;
            string strEmailContentBuf;
            string strEmailSubject;

            //----------------------------------
            // Get IP Address
            //----------------------------------
            strClientIp = x_objContext.GetClientIPAddress();

            //----------------------------------
            // Validate data
            //----------------------------------
            objResponse = await MemberInfoValidate(x_objRequest, strClientIp);
            if (objResponse.IsSuccsess == false)
            {
                return objResponse;
            }
            objEvt = await ActivityDbManager.GetEventInfoById(x_objRequest.EventId);
            if (objEvt == null)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.ACTIVITY_IS_NULL, ErrorCode.ACTIVITY_IS_NULL);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.ACTIVITY_IS_NULL}", SOURCE);

                return objResponse;
            }
            //----------------------------------
            // Registry
            //----------------------------------
            objCommunityRegistEvtModel = new CommunityRegistEvtModel
            {
                ClassName = x_objRequest.ClassName,
                Email = x_objRequest.Email,
                EventId = x_objRequest.EventId,
                FacebookPath = x_objRequest.FacebookPath,
                FacultyId = x_objRequest.FacultyId,
                StudentId = x_objRequest.StudentId,
                FirstName = x_objRequest.FirstName,
                LastName = x_objRequest.LastName,
                PhoneNumber = x_objRequest.PhoneNumber
            };
            objMemberRegistEvtModel = await MemberRegistEvtData.Instant.CommunityRegisEvent(objCommunityRegistEvtModel);
            if (objMemberRegistEvtModel == null)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.MEMBER_REGISTER_ACTIVITY_FAILED, ErrorCode.MEMBER_REGISTER_ACTIVITY_FAILED);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.MEMBER_REGISTER_ACTIVITY_FAILED}", SOURCE);

                return objResponse;
            }

            // Create Qr-Code
            strQrCodeFileName = string.Format(SysFile.QR_CODE_NAME_FORMAT, x_objRequest.StudentId);
            strQrCodeFilePath = FileHelper.GetFilePath(SysFolder.QR_CODE_FOLDER, strQrCodeFileName);
            objQrCode = QRCodeHelper.CreateQrCode(objMemberRegistEvtModel.MemberId);
            objQrCode.SaveBitmapStream(strQrCodeFilePath);

            // Read email temp file
            strEmailFilePath = FileHelper.GetFilePath(SysFolder.EMAIL_TEMP_FOLDER, SysFile.CONFIRM_REGISTER_ACTIVITY);
            strEmailContentBuf = FileHelper.ReadTextFile(strEmailFilePath);
            if (string.IsNullOrWhiteSpace(strEmailContentBuf) == false)
            {
                objAddress = await GetAddressModel(objEvt.AddressId);
                strEmailContent = strEmailContentBuf.Replace(EmailReplaceText.MEMBER_FULL_NAME, objMemberRegistEvtModel.GetMemberFullName())
                                                    .Replace(EmailReplaceText.STUDENT_ID, objMemberRegistEvtModel.StudentId)
                                                    .Replace(EmailReplaceText.ACTIVITY_NAME, objEvt.Title)
                                                    .Replace(EmailReplaceText.REGIST_DATE, objMemberRegistEvtModel.CreateDate.ToString("dd/MM/yyyy HH:mm"))
                                                    .Replace(EmailReplaceText.START_DATE, objEvt.StartDate.ToString("dd/MM/yyyy HH:mm"))
                                                    .Replace(EmailReplaceText.ADDRESS, objAddress.ToString())
                                                    .Replace(EmailReplaceText.MEMBER_NAME, objMemberRegistEvtModel.LastName);
                strEmailSubject = string.Format(EmailSubject.REGISTER_ACTIVITY, objEvt.Title);
                objSendEmailModel = new SendEmailModel
                {
                    DeleteFileAfterSent = true,
                    EmailBody = strEmailContent,
                    EmailRecipient = objMemberRegistEvtModel.Email,
                    EmailType = EmailType.NOTIFICATION,
                    IsSendCC = true,
                    Subject = strEmailSubject
                };
                objSendEmailModel.AttachFilePath.Add(strQrCodeFilePath);
                EmailHelper.Instant.EmailNomalQueue.Enqueue(objSendEmailModel);
            }

            objResponse = ResponseManager.GetSuccsessResp(SuccsessConstant.REGISTER_AVTIVITY_SUCCSESS);
            return objResponse;
        }

        /// <summary>
        /// Confirm Certificate
        /// </summary>
        /// <param name="x_strStudentId"></param>
        /// <param name="x_strActivityId"></param>
        /// <param name="x_objContext"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public async Task<ResponseModel> ConfirmCertificate(string x_strStudentId, string x_strActivityId, HttpContext x_objContext)
        {
            ResponseModel objResponse;
            EventInfo objEvt;
            ConfirmCertificateModel objConfirmCertificateModel;
            ConfirmCertificateCheckInModel objCheckInData;
            MemberRegistEvtModel objMember;
            string strClientIp;
            //----------------------------------
            // Get IP Address
            //----------------------------------
            strClientIp = x_objContext.GetClientIPAddress();

            objEvt = await ActivityDbManager.GetEventInfoById(x_strActivityId);
            if (objEvt == null)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.ACTIVITY_IS_NULL, ErrorCode.ACTIVITY_IS_NULL);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.ACTIVITY_IS_NULL}", SOURCE);

                return objResponse;
            }
            // Get Member
            objMember = MemberRegistEvtData.Instant.GetMemberRegisEvt(x_strStudentId, x_strActivityId);
            if (objMember == null)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.MEMBER_IS_NOT_EXIST_ACTIVITY, ErrorCode.MEMBER_IS_NOT_EXIST_ACTIVITY);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.MEMBER_IS_NOT_EXIST_ACTIVITY}", SOURCE);

                return objResponse;
            }

            if ((objMember.CreateDate >= objMember.SendCertificate) || (objMember.TotalScore <= 0))
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.IS_NOT_SEND_CERTIFICATE, ErrorCode.IS_NOT_SEND_CERTIFICATE);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMember.GetMemberFullName(), $"{SOURCE}: {ErrorConstant.IS_NOT_SEND_CERTIFICATE}", SOURCE);

                return objResponse;
            }

            objConfirmCertificateModel = new ConfirmCertificateModel
            {
                SendCertificate = objMember.SendCertificate,
                EventName = objEvt.Title,
                FacultyName = objMember.Faculty.FacultyName,
                FullName = objMember.GetMemberFullName(),
                StudentId = objMember.StudentId,
                TotalScore = objMember.TotalScore,
                EventPosterPath = Path.Combine(SysFolder.ACTIVITY_POSTER_PATH, objEvt.PosterPath)
            };

            if (objMember.EvtCheckList != null)
            {
                foreach (ActivityChecklistData objTemp in objMember.EvtCheckList)
                {
                    if (objTemp == null || objTemp.Score <= 0)
                    {
                        continue;
                    }
                    objCheckInData = new ConfirmCertificateCheckInModel
                    {
                        EntryTime = objTemp.EntryTime,
                        ManagerName = objTemp.ManagerName,
                        Score = objTemp.Score,
                        TimeToLeave = objTemp.TimeToLeave
                    };
                    objConfirmCertificateModel.CheckInData.Add(objCheckInData);
                }
            }

            objResponse = ResponseManager.GetSuccsessResp(string.Empty, objConfirmCertificateModel);
            return objResponse;
        }

        /// <summary>
        /// Create Activitie
        /// </summary>
        /// <param name="x_objRequest"></param>
        /// <param name="x_strAccountId"></param>
        /// <param name="x_objContext"></param>
        /// <returns></returns>
        public async Task<ResponseModel> CreateActivitie(CreateActivityRequestModel x_objRequest, string x_strAccountId, HttpContext x_objContext)
        {
            Accounts objAccount;
            Roles objRole;
            MemberDecryptModel objMemberInfo;
            ResponseModel objResponse;
            AddressInfo objAddress;
            Bitmap objPoster;
            EventInfo objEventInfo;
            ActivityResponseModel objActivityResponseModel;
            DateTime objStartTime;
            DateTime objEndTime;
            DateTime objDeadlineTime;

            bool bRes;
            string strClientIp;
            string strBuf;
            string strExtension;
            string strFileName;
            string strFilePath;

            //----------------------------------
            // Get IP Address
            //----------------------------------
            strClientIp = x_objContext.GetClientIPAddress();

            //----------------------------------
            // Validate request data
            //----------------------------------
            objResponse = ActivityValidate(x_objRequest, out objAddress);
            if (objResponse.IsSuccsess == false)
            {
                return objResponse;
            }

            //----------------------------------
            // Validate request role
            //----------------------------------
            // Account
            objAccount = await GetAccount(x_strAccountId);
            if (objAccount == null)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.ACCOUNT_IS_LOCKER, ErrorCode.ACCOUNT_IS_LOCKER);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.ACCOUNT_IS_LOCKER}", SOURCE);

                return objResponse;
            }

            // Member
            objMemberInfo = MemberInfoManager.GetMemberById(objAccount.MemberId);
            if (objMemberInfo == null)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.MEMBER_INFO_IS_NULL, ErrorCode.MEMBER_INFO_IS_NULL);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.MEMBER_INFO_IS_NULL}", SOURCE);

                return objResponse;
            }

            // Role
            objRole = await RolesDbManager.GetRoleById(objAccount.RoleId);
            if ((objRole == null) || (objRole.Permissions.Contains((int)DTUSVCPermission.CREATE_ACTIVITY) == false))
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.AUTHORITY_INSUFFICIENT, ErrorCode.AUTHORITY_INSUFFICIENT);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMemberInfo.GetMemberFullName(), $"{SOURCE}: {ErrorConstant.MEMBER_INFO_IS_NULL}", SOURCE);

                return objResponse;
            }

            // Copy image
            strExtension = Path.GetExtension(x_objRequest.Poster.FileName);
            strFileName = FileHelper.GetRandomFileName(strExtension);
            strFilePath = FileHelper.GetFilePath(SysFolder.ACTIVITY_POSTER_PATH, strFileName);
            strBuf = await x_objRequest.Poster.SaveFormFile(strFilePath);
            if (string.IsNullOrEmpty(strBuf) == false)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(strBuf, ErrorCode.UPLOAD_FILE_IS_NULL);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMemberInfo.GetMemberFullName(), $"{SOURCE}: {strBuf}", SOURCE);

                return objResponse;
            }
            // Crop image
            objPoster = FileHelper.OpenImageFile(strFilePath);
            objPoster = objPoster.CropImage(ImageHelper.POSTER_IMAGE, out strBuf);
            if (string.IsNullOrEmpty(strBuf) == true)
            {
                objPoster.SaveBitmapStream(strFilePath);
            }

            // Update Address
            bRes = await AddressDbManager.UpdateAddress(objAddress);
            if (bRes == false)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.SYSTEM_DISCONNECT, ErrorCode.SYSTEM_DISCONNECT);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMemberInfo.GetMemberFullName(), $"{SOURCE}: {ErrorConstant.SYSTEM_DISCONNECT}", SOURCE);

                return objResponse;
            }

            // Create Activity info
            objDeadlineTime = x_objRequest.RegistrationDeadline.ConvertStrToDateTime();
            objEndTime = x_objRequest.EndDate.ConvertStrToDateTime();
            objStartTime = x_objRequest.StartDate.ConvertStrToDateTime();
            objEventInfo = new EventInfo
            {
                AddressId = objAddress.Id,
                Id = string.Empty,
                Content = x_objRequest.Content,
                CreaterId = objMemberInfo.Id,
                EndDate = objEndTime,
                IsPublic = x_objRequest.IsPublic,
                PosterPath = strFileName,
                RegistrationDeadline = objDeadlineTime,
                StartDate = objStartTime,
                Title = x_objRequest.Title
            };
            bRes = await ActivityDbManager.UpdateEventInfo(objEventInfo);
            if (bRes == false)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.SYSTEM_DISCONNECT, ErrorCode.SYSTEM_DISCONNECT);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMemberInfo.GetMemberFullName(), $"{SOURCE}: {ErrorConstant.SYSTEM_DISCONNECT}", SOURCE);

                return objResponse;
            }

            objActivityResponseModel = await ConvertActivityDbToModel(objEventInfo);
            objResponse = ResponseManager.GetSuccsessResp(SuccsessConstant.CREATE_ACTIVITY_SUCCSESS, objActivityResponseModel);
            return objResponse;
        }

        /// <summary>
        /// Delete Activitie
        /// </summary>
        /// <param name="x_strActivityId"></param>
        /// <param name="x_strAccountId"></param>
        /// <param name="x_objContext"></param>
        /// <returns></returns>
        public async Task<ResponseModel> DeleteActivitie(string x_strActivityId, string x_strAccountId, HttpContext x_objContext)
        {
            Accounts objAccount;
            Roles objRole;
            MemberDecryptModel objMemberInfo;
            ResponseModel objResponse;
            EventInfo objEventInfo;
            DateTime objCurrentDate;

            bool bRes;
            string strClientIp;

            //----------------------------------
            // Get IP Address
            //----------------------------------
            strClientIp = x_objContext.GetClientIPAddress();
            //----------------------------------
            // Validate request role
            //----------------------------------
            // Account
            objAccount = await GetAccount(x_strAccountId);
            if (objAccount == null)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.ACCOUNT_IS_LOCKER, ErrorCode.ACCOUNT_IS_LOCKER);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.ACCOUNT_IS_LOCKER}", SOURCE);

                return objResponse;
            }

            // Member
            objMemberInfo = MemberInfoManager.GetMemberById(objAccount.MemberId);
            if (objMemberInfo == null)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.MEMBER_INFO_IS_NULL, ErrorCode.MEMBER_INFO_IS_NULL);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.MEMBER_INFO_IS_NULL}", SOURCE);

                return objResponse;
            }

            // Role
            objRole = await RolesDbManager.GetRoleById(objAccount.RoleId);
            if ((objRole == null) || (objRole.Permissions.Contains((int)DTUSVCPermission.DELETE_ACTIVITY) == false))
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.AUTHORITY_INSUFFICIENT, ErrorCode.AUTHORITY_INSUFFICIENT);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMemberInfo.GetMemberFullName(), $"{SOURCE}: {ErrorConstant.MEMBER_INFO_IS_NULL}", SOURCE);

                return objResponse;
            }

            // Get Event info
            objEventInfo = await ActivityDbManager.GetEventInfoById(x_strActivityId);
            if (objEventInfo == null)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.ACTIVITY_IS_NULL, ErrorCode.ACTIVITY_IS_NULL);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMemberInfo.GetMemberFullName(), $"{SOURCE}: {ErrorConstant.ACTIVITY_IS_NULL}", SOURCE);
                return objResponse;
            }
            // Check end date
            objCurrentDate = DateTimeHelper.GetCurrentDateTimeVN();
            if (objEventInfo.EndDate < objCurrentDate)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.ACTIVITY_IS_FINISHED, ErrorCode.ACTIVITY_IS_FINISHED);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMemberInfo.GetMemberFullName(), $"{SOURCE}: {ErrorConstant.ACTIVITY_IS_FINISHED}", SOURCE);
                return objResponse;
            }
            if (objEventInfo.IsCertificateSending == true)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.CERTIFICATE_IS_SENDING, ErrorCode.CERTIFICATE_IS_SENDING);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMemberInfo.GetMemberFullName(), $"{SOURCE}: {ErrorConstant.CERTIFICATE_IS_SENDING}", SOURCE);
                return objResponse;
            }

            // Delete activity
            bRes = await ActivityDbManager.DeleteEventInfoById(x_strActivityId);
            if (bRes == false)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.DELETE_ACTIVITY_FAILED, ErrorCode.DELETE_ACTIVITY_FAILED);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMemberInfo.GetMemberFullName(), $"{SOURCE}: {ErrorConstant.DELETE_ACTIVITY_FAILED}", SOURCE);
                return objResponse;
            }
            await ActivityDbManager.DeleteCheckListByEvtId(x_strActivityId);
            await ActivityDbManager.DeleteCommunityRegistByEvtId(x_strActivityId);
            await ActivityDbManager.DeleteLeaveOfAbsenceByEvtId(x_strActivityId);
            await ActivityDbManager.DeleteMemberSupportEventsByEvtId(x_strActivityId);
            // Delete address
            await AddressDbManager.DeleteAddress(objEventInfo.AddressId);
            objResponse = ResponseManager.GetSuccsessResp(SuccsessConstant.DELETE_ACTIVITY_SUCCSESS);
            return objResponse;
        }

        /// <summary>
        /// Delete Setup Certificate
        /// </summary>
        /// <param name="x_strCertificateTextId"></param>
        /// <param name="x_strAccountId"></param>
        /// <param name="x_objContext"></param>
        /// <returns></returns>
        public async Task<ResponseModel> DeleteSetupCertificate(string x_strCertificateTextId, string x_strAccountId, HttpContext x_objContext)
        {
            Accounts objAccount;
            Roles objRole;
            MemberDecryptModel objMemberInfo;
            ResponseModel objResponse;
            EventInfo objEventInfo;
            CertificateSettingText objSettingText;

            string strClientIp;
            string strFilePath;
            bool bResult;

            //----------------------------------
            // Get IP Address
            //----------------------------------
            strClientIp = x_objContext.GetClientIPAddress();
            //----------------------------------
            // Validate request role
            //----------------------------------
            // Account
            objAccount = await GetAccount(x_strAccountId);
            if (objAccount == null)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.ACCOUNT_IS_LOCKER, ErrorCode.ACCOUNT_IS_LOCKER);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.ACCOUNT_IS_LOCKER}", SOURCE);

                return objResponse;
            }

            // Member
            objMemberInfo = MemberInfoManager.GetMemberById(objAccount.MemberId);
            if (objMemberInfo == null)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.MEMBER_INFO_IS_NULL, ErrorCode.MEMBER_INFO_IS_NULL);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.MEMBER_INFO_IS_NULL}", SOURCE);

                return objResponse;
            }

            // Role
            objRole = await RolesDbManager.GetRoleById(objAccount.RoleId);
            if ((objRole == null) || (objRole.Permissions.Contains((int)DTUSVCPermission.DELETE_SETTING_TEXT) == false))
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.AUTHORITY_INSUFFICIENT, ErrorCode.AUTHORITY_INSUFFICIENT);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMemberInfo.GetMemberFullName(), $"{SOURCE}: {ErrorConstant.MEMBER_INFO_IS_NULL}", SOURCE);

                return objResponse;
            }
            objSettingText = await ActivityDbManager.GetSettingTextById(x_strCertificateTextId);
            if (objSettingText == null)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.DELETE_SETTING_TEXT_FAILED, ErrorCode.DELETE_SETTING_TEXT_FAILED);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMemberInfo.GetMemberFullName(), $"{SOURCE}: {ErrorConstant.DELETE_SETTING_TEXT_FAILED}", SOURCE);

                return objResponse;
            }

            // Get Event info
            objEventInfo = await ActivityDbManager.GetEventInfoById(objSettingText.ActivityId);
            if (objEventInfo == null)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.ACTIVITY_IS_NULL, ErrorCode.ACTIVITY_IS_NULL);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMemberInfo.GetMemberFullName(), $"{SOURCE}: {ErrorConstant.ACTIVITY_IS_NULL}", SOURCE);
                return objResponse;
            }
            if (objEventInfo.IsCertificateSending == true)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.CERTIFICATE_IS_SENDING, ErrorCode.CERTIFICATE_IS_SENDING);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMemberInfo.GetMemberFullName(), $"{SOURCE}: {ErrorConstant.CERTIFICATE_IS_SENDING}", SOURCE);
                return objResponse;
            }

            bResult = await ActivityDbManager.DeleteSettingText(x_strCertificateTextId);
            if (bResult == false)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.DELETE_SETTING_TEXT_FAILED, ErrorCode.DELETE_SETTING_TEXT_FAILED);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMemberInfo.GetMemberFullName(), $"{SOURCE}: {ErrorConstant.DELETE_SETTING_TEXT_FAILED}", SOURCE);

                return objResponse;
            }

            strFilePath = string.Empty;
            if (string.IsNullOrWhiteSpace(objEventInfo.CertificateFile) == false)
            {
                strFilePath = await CreateCertificateTemp(objEventInfo.Id);
            }
            objResponse = ResponseManager.GetSuccsessResp(SuccsessConstant.DELETE_CERTIFICATE_TEXT_SUCCSESS, strFilePath);
            return objResponse;
        }

        /// <summary>
        /// Download List Member In Activity
        /// </summary>
        /// <param name="x_strActivityId"></param>
        /// <param name="x_strAccountId"></param>
        /// <param name="x_objContext"></param>
        /// <returns></returns>
        public async Task<ResponseModel> DownloadActivityReport(string x_strActivityId, string x_strAccountId, HttpContext x_objContext)
        {
            Accounts objAccount;
            Roles objRole;
            MemberDecryptModel objMemberInfo;
            ResponseModel objResponse;
            EventInfo objEventInfo;
            List<MemberRegistEvtModel> lstMemberRegistEvt;
            List<ExportMemberRegistEvt> lstMemberRegistExcelEvt;
            List<ExportMemberScore> x_lstExportMemberScore;
            List<ScoreInfo> lstScoreData;

            string strClientIp;
            string strFileName;
            string strFilePath;
            bool bResult;

            //----------------------------------
            // Get IP Address
            //----------------------------------
            strClientIp = x_objContext.GetClientIPAddress();
            //----------------------------------
            // Validate request role
            //----------------------------------
            // Account
            objAccount = await GetAccount(x_strAccountId);
            if (objAccount == null)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.ACCOUNT_IS_LOCKER, ErrorCode.ACCOUNT_IS_LOCKER);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.ACCOUNT_IS_LOCKER}", SOURCE);

                return objResponse;
            }

            // Member
            objMemberInfo = MemberInfoManager.GetMemberById(objAccount.MemberId);
            if (objMemberInfo == null)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.MEMBER_INFO_IS_NULL, ErrorCode.MEMBER_INFO_IS_NULL);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.MEMBER_INFO_IS_NULL}", SOURCE);

                return objResponse;
            }

            // Role
            objRole = await RolesDbManager.GetRoleById(objAccount.RoleId);
            if ((objRole == null) || (objRole.Permissions.Contains((int)DTUSVCPermission.EXPORT_ACTIVITY) == false))
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.AUTHORITY_INSUFFICIENT, ErrorCode.AUTHORITY_INSUFFICIENT);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMemberInfo.GetMemberFullName(), $"{SOURCE}: {ErrorConstant.MEMBER_INFO_IS_NULL}", SOURCE);

                return objResponse;
            }

            // Get Event info
            objEventInfo = await ActivityDbManager.GetEventInfoById(x_strActivityId);
            if (objEventInfo == null)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.ACTIVITY_IS_NULL, ErrorCode.ACTIVITY_IS_NULL);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMemberInfo.GetMemberFullName(), $"{SOURCE}: {ErrorConstant.ACTIVITY_IS_NULL}", SOURCE);
                return objResponse;
            }

            lstMemberRegistEvt = MemberRegistEvtData.Instant.GetListMemberRegistEvt(x_strActivityId);
            if (lstMemberRegistEvt == null || lstMemberRegistEvt.Count == 0)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.MEMBER_IN_ACTIVITY_IS_NULL, ErrorCode.MEMBER_IN_ACTIVITY_IS_NULL);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMemberInfo.GetMemberFullName(), $"{SOURCE}: {ErrorConstant.MEMBER_IN_ACTIVITY_IS_NULL}", SOURCE);
                return objResponse;
            }

            lstMemberRegistExcelEvt = new List<ExportMemberRegistEvt>();
            x_lstExportMemberScore = new List<ExportMemberScore>();
            foreach (MemberRegistEvtModel objMemberInEvt in lstMemberRegistEvt)
            {
                lstMemberRegistExcelEvt.Add(new ExportMemberRegistEvt
                {
                    ClassName = objMemberInEvt.ClassName,
                    Email = objMemberInEvt.Email,
                    FacebookPath = objMemberInEvt.FacebookPath,
                    Faculty = objMemberInEvt.Faculty.FacultyName,
                    FirstName = objMemberInEvt.FirstName,
                    IsMember = objMemberInEvt.IsMember,
                    LastName = objMemberInEvt.LastName,
                    PhoneNumber = objMemberInEvt.PhoneNumber,
                    StudentId = objMemberInEvt.StudentId,
                    TotalScore = objMemberInEvt.TotalScore,
                    MemberId = objMemberInEvt.MemberId
                });

                lstScoreData = new List<ScoreInfo>();
                foreach (ActivityChecklistData objScoreData in objMemberInEvt.EvtCheckList)
                {
                    lstScoreData.Add(new ScoreInfo
                    {
                        EntryTime = objScoreData.EntryTime,
                        ManagerName = objScoreData.ManagerName,
                        Score = objScoreData.Score,
                        TimeToLeave = objScoreData.TimeToLeave
                    });
                }

                x_lstExportMemberScore.Add(new ExportMemberScore
                {
                    MemberId = objMemberInEvt.MemberId,
                    StudentId = objMemberInEvt.StudentId,
                    ScoreData = lstScoreData
                });
            }

            strFileName = FileHelper.CreateFileNameByTitle(objEventInfo.Title, FileExtension.EXCEL_EXTENSION);
            strFilePath = FileHelper.GetFilePath(SysFolder.EXPORT_MEMBER_REGISTER_EVT, strFileName);
            bResult = FileHelper.ExportMemberRegistEvtToExcel(lstMemberRegistExcelEvt, x_lstExportMemberScore, strFilePath);
            if (bResult == false)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.MEMBER_IN_ACTIVITY_EXPORT_FAILED, ErrorCode.MEMBER_IN_ACTIVITY_EXPORT_FAILED);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMemberInfo.GetMemberFullName(), $"{SOURCE}: {ErrorConstant.MEMBER_IN_ACTIVITY_EXPORT_FAILED}", SOURCE);
                return objResponse;
            }

            objResponse = new ResponseDownloadFile()
            {
                IsSuccsess = true,
                FilePath = strFilePath
            };
            return objResponse;
        }

        /// <summary>
        /// Get Activitie By Id
        /// </summary>
        /// <param name="x_strActivityId"></param>
        /// <param name="x_strAccountId"></param>
        /// <param name="x_objContext"></param>
        /// <returns></returns>
        public async Task<ResponseModel> GetActivitieById(string x_strActivityId, string x_strAccountId, HttpContext x_objContext)
        {
            ResponseModel objResponse;
            Accounts objAccount;
            EventInfo objEventInfo;
            ActivityResponseModel objActivityResponseModel;
            Roles objRoles;
            string strToken;
            string strCertificate;
            string strClientIp;
            bool bIsLogin;

            //----------------------------------
            // Get IP Address
            //----------------------------------
            strClientIp = x_objContext.GetClientIPAddress();

            //----------------------------------
            // Check Login
            //----------------------------------
            bIsLogin = false;
            strToken = x_objContext.GetTokenByHttpContext();
            objAccount = null;
            if (string.IsNullOrWhiteSpace(strToken) == false)
            {
                objAccount = await AccountDbManager.GetAccountById(x_strAccountId);
                if ((objAccount != null) && (objAccount.IsLocked == false))
                {
                    bIsLogin = true;
                }
            }

            objEventInfo = await ActivityDbManager.GetEventInfoById(x_strActivityId);
            if (objEventInfo == null)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.ACTIVITY_IS_NULL, ErrorCode.ACTIVITY_IS_NULL);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.ACTIVITY_IS_NULL}", SOURCE);
                return objResponse;
            }

            if ((objEventInfo.IsPublic == false) && (bIsLogin == false))
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.ACTIVITY_IS_PRIVATE, ErrorCode.ACTIVITY_IS_PRIVATE);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.ACTIVITY_IS_PRIVATE}", SOURCE);
                return objResponse;
            }

            objActivityResponseModel = await ConvertActivityDbToModel(objEventInfo);
            if (bIsLogin == true)
            {
                objRoles = await RolesDbManager.GetRoleById(objAccount.RoleId);
                if ((objRoles != null) && (objRoles.Permissions.Contains((int)DTUSVCPermission.UPDATE_CERTIFICATE) == true))
                {
                    strCertificate = objEventInfo.CertificateFile;
                    if (string.IsNullOrWhiteSpace(strCertificate) == false)
                    {
                        objActivityResponseModel.CertificateFile = Path.Combine(SysFolder.ACTIVITY_CERTIFICATE_PATH, strCertificate);
                    }
                }
            }
            objResponse = ResponseManager.GetSuccsessResp(string.Empty, objActivityResponseModel);
            return objResponse;
        }

        /// <summary>
        /// Get Activity Histories
        /// </summary>
        /// <param name="x_strAccountId"></param>
        /// <param name="x_objContext"></param>
        /// <returns></returns>
        public async Task<ResponseModel> GetActivityHistories(string x_strAccountId, HttpContext x_objContext)
        {
            ResponseModel objResponse;
            Accounts objAccount;
            MemberDecryptModel objMemberInfo;
            ActivityHistoryResponseModel objActivityResponseModel;
            List<MemberRegistEvt> lstMemberRegistEvt;
            EventInfo objEventInfo;
            List<ActivityHistoryResponseModel> lstResponseEventInfo;

            string strClientIp;

            //----------------------------------
            // Get IP Address
            //----------------------------------
            strClientIp = x_objContext.GetClientIPAddress();

            //----------------------------------
            // Validate request role
            //----------------------------------
            // Account
            objAccount = await GetAccount(x_strAccountId);
            if (objAccount == null)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.ACCOUNT_IS_LOCKER, ErrorCode.ACCOUNT_IS_LOCKER);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.ACCOUNT_IS_LOCKER}", SOURCE);

                return objResponse;
            }

            // Member
            objMemberInfo = MemberInfoManager.GetMemberById(objAccount.MemberId);
            if (objMemberInfo == null)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.MEMBER_INFO_IS_NULL, ErrorCode.MEMBER_INFO_IS_NULL);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.MEMBER_INFO_IS_NULL}", SOURCE);

                return objResponse;
            }

            lstMemberRegistEvt = await ActivityDbManager.GetAllMemberRegistEvt(objAccount.MemberId);
            lstResponseEventInfo = new List<ActivityHistoryResponseModel>();
            if (lstMemberRegistEvt == null)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.ACTIVITY_HISTORIES_IS_NULL, ErrorCode.ACTIVITY_HISTORIES_IS_NULL);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMemberInfo.GetMemberFullName(), $"{SOURCE}: {ErrorConstant.ACTIVITY_HISTORIES_IS_NULL}", SOURCE);

                return objResponse;
            }

            // Get List Event
            foreach (MemberRegistEvt objAct in lstMemberRegistEvt)
            {
                objEventInfo = await ActivityDbManager.GetEventInfoById(objAct.EventId);
                if (objEventInfo == null)
                {
                    continue;
                }
                objActivityResponseModel = (ActivityHistoryResponseModel)await ConvertActivityDbToModel(objEventInfo);
                if (objActivityResponseModel != null)
                {
                    await objActivityResponseModel.GetCheckInList(objAct.MemberId);
                    lstResponseEventInfo.Add(objActivityResponseModel);
                }
            }
            lstResponseEventInfo = lstResponseEventInfo.OrderByDescending(obj => obj.CreateTime).ThenByDescending(obj => obj.StartDate).ToList();

            objResponse = ResponseManager.GetSuccsessResp(SuccsessConstant.GET_HISTORIES_ACTIVITY_SUCCSESS, lstResponseEventInfo, lstResponseEventInfo.Count);
            return objResponse;
        }

        /// <summary>
        /// Get List Activities
        /// </summary>
        /// <param name="x_strAccountId"></param>
        /// <param name="x_objContext"></param>
        /// <returns></returns>
        public async Task<ResponseModel> GetListActivities(int x_nPageIndex, int x_nPageSize, string x_strAccountId, HttpContext x_objContext)
        {
            ResponseModel objResponse;
            Accounts objAccount;
            List<EventInfo> lstEventInfo;
            List<ActivityResponseModel> lstResponseEventInfo;
            ActivityResponseModel objActivityResponseModel;
            string strToken;
            string strClientIp;
            bool bIsLogin;
            int nTotalItem;

            //----------------------------------
            // Get IP Address
            //----------------------------------
            strClientIp = x_objContext.GetClientIPAddress();

            //----------------------------------
            // Check Login
            //----------------------------------
            bIsLogin = false;
            strToken = x_objContext.GetTokenByHttpContext();
            if (string.IsNullOrWhiteSpace(strToken) == false)
            {
                objAccount = await AccountDbManager.GetAccountById(x_strAccountId);
                if ((objAccount != null) && (objAccount.IsLocked == false))
                {
                    bIsLogin = true;
                }
            }

            lstEventInfo = await ActivityDbManager.GetAllEventInfo(bIsLogin);
            nTotalItem = lstEventInfo.Count();
            if (x_nPageSize < 0)
            {
                x_nPageSize = 0;
            }
            if (x_nPageIndex < 0)
            {
                x_nPageIndex = 0;
            }
            lstEventInfo = lstEventInfo.OrderByDescending(obj => obj.CreateTime).ThenByDescending(obj => obj.StartDate).ToList();
            lstEventInfo = lstEventInfo.Skip(x_nPageSize * x_nPageIndex).Take(x_nPageSize).ToList();
            lstResponseEventInfo = new List<ActivityResponseModel>();
            // Convert to response model
            if (lstEventInfo != null)
            {
                foreach (EventInfo objEvtDb in lstEventInfo)
                {
                    objActivityResponseModel = await ConvertActivityDbToModel(objEvtDb);
                    if (objActivityResponseModel != null)
                    {
                        lstResponseEventInfo.Add(objActivityResponseModel);
                    }
                }
            }

            objResponse = ResponseManager.GetSuccsessResp(string.Empty, lstResponseEventInfo, nTotalItem);
            return objResponse;
        }

        /// <summary>
        /// Get List Member In Activity
        /// </summary>
        /// <param name="x_strActivityId"></param>
        /// <param name="x_strAccountId"></param>
        /// <param name="x_objContext"></param>
        /// <returns></returns>
        public async Task<ResponseModel> GetListMemberInActivity(string x_strActivityId, string x_strAccountId, HttpContext x_objContext)
        {
            Accounts objAccount;
            Roles objRole;
            EventInfo objEventInfo;
            MemberDecryptModel objMemberInfo;
            ResponseModel objResponse;
            List<MemberRegistEvtModel> lstMemberRegistEvtModel;
            string strClientIp;

            //----------------------------------
            // Get IP Address
            //----------------------------------
            strClientIp = x_objContext.GetClientIPAddress();
            //----------------------------------
            // Validate request role
            //----------------------------------
            // Account
            objAccount = await GetAccount(x_strAccountId);
            if (objAccount == null)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.ACCOUNT_IS_LOCKER, ErrorCode.ACCOUNT_IS_LOCKER);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.ACCOUNT_IS_LOCKER}", SOURCE);

                return objResponse;
            }

            // Member
            objMemberInfo = MemberInfoManager.GetMemberById(objAccount.MemberId);
            if (objMemberInfo == null)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.MEMBER_INFO_IS_NULL, ErrorCode.MEMBER_INFO_IS_NULL);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.MEMBER_INFO_IS_NULL}", SOURCE);

                return objResponse;
            }

            // Role
            objRole = await RolesDbManager.GetRoleById(objAccount.RoleId);
            if ((objRole == null) || (objRole.Permissions.Contains((int)DTUSVCPermission.GET_LIST_MEMBER_IN_ACTIVITY) == false))
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.AUTHORITY_INSUFFICIENT, ErrorCode.AUTHORITY_INSUFFICIENT);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMemberInfo.GetMemberFullName(), $"{SOURCE}: {ErrorConstant.MEMBER_INFO_IS_NULL}", SOURCE);

                return objResponse;
            }

            // Get Event info
            objEventInfo = await ActivityDbManager.GetEventInfoById(x_strActivityId);
            if (objEventInfo == null)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.ACTIVITY_IS_NULL, ErrorCode.ACTIVITY_IS_NULL);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMemberInfo.GetMemberFullName(), $"{SOURCE}: {ErrorConstant.ACTIVITY_IS_NULL}", SOURCE);
                return objResponse;
            }

            lstMemberRegistEvtModel = MemberRegistEvtData.Instant.GetListMemberRegistEvt(x_strActivityId);
            if ((lstMemberRegistEvtModel == null) || (lstMemberRegistEvtModel.Count == 0))
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.MEMBER_IN_ACTIVITY_IS_NULL, ErrorCode.MEMBER_IN_ACTIVITY_IS_NULL);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMemberInfo.GetMemberFullName(), $"{SOURCE}: {ErrorConstant.MEMBER_IN_ACTIVITY_IS_NULL}", SOURCE);
                return objResponse;
            }

            objResponse = ResponseManager.GetSuccsessResp(SuccsessConstant.GET_LIST_MEMBER_IN_ACTIVITY_SUCCSESS, lstMemberRegistEvtModel, lstMemberRegistEvtModel.Count);
            return objResponse;
        }

        /// <summary>
        /// Get List Text Font
        /// </summary>
        /// <param name="x_strAccountId"></param>
        /// <param name="x_objContext"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public async Task<ResponseModel> GetListTextFont(string x_strAccountId, HttpContext x_objContext)
        {
            ResponseModel objResponse;
            List<CertificateTextFont> lstCertificateTextFont;
            string strFilePath;

            lstCertificateTextFont = await ActivityDbManager.GetAllTestFont();
            if (lstCertificateTextFont == null)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.TEXT_FONT_IS_EMPTY, ErrorCode.TEXT_FONT_IS_EMPTY);
                return objResponse;
            }

            for (int nIndex = 0; nIndex < lstCertificateTextFont.Count; nIndex++)
            {
                strFilePath = Path.Combine(SysFolder.FONT_FILE_FOLDER, lstCertificateTextFont[nIndex].FontPath);
                lstCertificateTextFont[nIndex].FontPath = strFilePath;
            }

            objResponse = ResponseManager.GetSuccsessResp(string.Empty, lstCertificateTextFont, lstCertificateTextFont.Count);
            return objResponse;
        }

        /// <summary>
        /// Member Leave Registration
        /// </summary>
        /// <param name="x_objRequest"></param>
        /// <param name="x_strAccountId"></param>
        /// <param name="x_objContext"></param>
        /// <returns></returns>
        public async Task<ResponseModel> MemberLeaveRegistration(MemberLeaveActivityRequestModel x_objRequest, string x_strAccountId, HttpContext x_objContext)
        {
            Accounts objAccount;
            MemberDecryptModel objMemberInfo;
            ResponseModel objResponse;
            EventInfo objEvent;
            LeaveOfAbsence objLeaveOfAbsence;
            DateTime objCurrentTime;
            bool bResult;
            string strClientIp;

            //----------------------------------
            // Get IP Address
            //----------------------------------
            strClientIp = x_objContext.GetClientIPAddress();
            //----------------------------------
            // Validate request model
            //----------------------------------
            if (x_objRequest == null)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.MEMBER_LEAVE_ACTIVITY_MODEL_INVALID, ErrorCode.MEMBER_LEAVE_ACTIVITY_MODEL_INVALID);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.MEMBER_LEAVE_ACTIVITY_MODEL_INVALID}", SOURCE);

                return objResponse;
            }
            if (string.IsNullOrWhiteSpace(x_objRequest.Rason) == true)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.MEMBER_LEAVE_ACTIVITY_RASON_INVALID, ErrorCode.MEMBER_LEAVE_ACTIVITY_RASON_INVALID);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.MEMBER_LEAVE_ACTIVITY_RASON_INVALID}", SOURCE);

                return objResponse;
            }
            //----------------------------------
            // Validate request role
            //----------------------------------
            // Account
            objAccount = await GetAccount(x_strAccountId);
            if (objAccount == null)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.ACCOUNT_IS_LOCKER, ErrorCode.ACCOUNT_IS_LOCKER);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.ACCOUNT_IS_LOCKER}", SOURCE);

                return objResponse;
            }

            // Member
            objMemberInfo = MemberInfoManager.GetMemberById(objAccount.MemberId);
            if (objMemberInfo == null)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.MEMBER_INFO_IS_NULL, ErrorCode.MEMBER_INFO_IS_NULL);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.MEMBER_INFO_IS_NULL}", SOURCE);

                return objResponse;
            }
            //----------------------------------
            // Get Event
            //----------------------------------
            objEvent = await ActivityDbManager.GetEventInfoById(x_objRequest.EventId);
            if (objEvent == null)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.ACTIVITY_IS_NULL, ErrorCode.ACTIVITY_IS_NULL);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMemberInfo.GetMemberFullName(), $"{SOURCE}: {ErrorConstant.ACTIVITY_IS_NULL}", SOURCE);

                return objResponse;
            }
            objCurrentTime = DateTimeHelper.GetCurrentDateTimeVN();
            if (objCurrentTime > objEvent.EndDate)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.ACTIVITY_IS_FINISHED, ErrorCode.ACTIVITY_IS_FINISHED);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMemberInfo.GetMemberFullName(), $"{SOURCE}: {ErrorConstant.ACTIVITY_IS_FINISHED}", SOURCE);

                return objResponse;
            }
            bResult = await ActivityDbManager.CheckLeaveExist(objMemberInfo.Id, objEvent.Id);
            if (bResult == true)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.LEAVE_ACTIVITY_IS_EXIST, ErrorCode.LEAVE_ACTIVITY_IS_EXIST);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMemberInfo.GetMemberFullName(), $"{SOURCE}: {ErrorConstant.LEAVE_ACTIVITY_IS_EXIST}", SOURCE);

                return objResponse;
            }

            // register leave
            objLeaveOfAbsence = new LeaveOfAbsence
            {
                MemberId = objMemberInfo.Id,
                EventId = objEvent.Id,
                Reason = x_objRequest.Rason
            };
            bResult = await MemberRegistEvtData.Instant.UpdateLeave(objLeaveOfAbsence);
            if (bResult == false)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.REGISTER_LEAVE_ACTIVITY_FAILED, ErrorCode.REGISTER_LEAVE_ACTIVITY_FAILED);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMemberInfo.GetMemberFullName(), $"{SOURCE}: {ErrorConstant.REGISTER_LEAVE_ACTIVITY_FAILED}", SOURCE);

                return objResponse;
            }

            objResponse = ResponseManager.GetSuccsessResp(SuccsessConstant.REGISTER_LEAVE_ACTIVITY_SUCCSESS);
            return objResponse;
        }

        /// <summary>
        /// Member Registration
        /// </summary>
        /// <param name="x_strActivityId"></param>
        /// <param name="x_strAccountId"></param>
        /// <param name="x_objContext"></param>
        /// <returns></returns>
        public async Task<ResponseModel> MemberRegistration(string x_strActivityId, string x_strAccountId, HttpContext x_objContext)
        {
            Accounts objAccount;
            MemberDecryptModel objMemberInfo;
            ResponseModel objResponse;
            EventInfo objEvent;
            MemberRegistEvtModel objMemberRegistEvtModel;
            DateTime objCurrentTime;
            string strClientIp;

            //----------------------------------
            // Get IP Address
            //----------------------------------
            strClientIp = x_objContext.GetClientIPAddress();
            //----------------------------------
            // Validate request role
            //----------------------------------
            // Account
            objAccount = await GetAccount(x_strAccountId);
            if (objAccount == null)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.ACCOUNT_IS_LOCKER, ErrorCode.ACCOUNT_IS_LOCKER);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.ACCOUNT_IS_LOCKER}", SOURCE);

                return objResponse;
            }

            // Member
            objMemberInfo = MemberInfoManager.GetMemberById(objAccount.MemberId);
            if (objMemberInfo == null)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.MEMBER_INFO_IS_NULL, ErrorCode.MEMBER_INFO_IS_NULL);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.MEMBER_INFO_IS_NULL}", SOURCE);

                return objResponse;
            }
            //----------------------------------
            // Get Event
            //----------------------------------
            objEvent = await ActivityDbManager.GetEventInfoById(x_strActivityId);
            if (objEvent == null)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.ACTIVITY_IS_NULL, ErrorCode.ACTIVITY_IS_NULL);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMemberInfo.GetMemberFullName(), $"{SOURCE}: {ErrorConstant.ACTIVITY_IS_NULL}", SOURCE);

                return objResponse;
            }
            objCurrentTime = DateTimeHelper.GetCurrentDateTimeVN();
            if (objCurrentTime > objEvent.EndDate)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.ACTIVITY_IS_FINISHED, ErrorCode.ACTIVITY_IS_FINISHED);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMemberInfo.GetMemberFullName(), $"{SOURCE}: {ErrorConstant.ACTIVITY_IS_FINISHED}", SOURCE);

                return objResponse;
            }
            // Check registed
            if (MemberRegistEvtData.Instant.CheckRegistExist(objMemberInfo.StudentId, objEvent.Id) == true)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.MEMBER_REGISTER_STUDENTID_IS_EXIST, ErrorCode.MEMBER_REGISTER_STUDENTID_IS_EXIST);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMemberInfo.GetMemberFullName(), $"{SOURCE}: {ErrorConstant.MEMBER_REGISTER_STUDENTID_IS_EXIST}", SOURCE);

                return objResponse;
            }
            objMemberRegistEvtModel = await MemberRegistEvtData.Instant.MemberRegisEvent(objMemberInfo.Id, objEvent.Id);
            if (objMemberRegistEvtModel == null)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.MEMBER_REGISTER_ACTIVITY_FAILED, ErrorCode.MEMBER_REGISTER_ACTIVITY_FAILED);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMemberInfo.GetMemberFullName(), $"{SOURCE}: {ErrorConstant.MEMBER_REGISTER_ACTIVITY_FAILED}", SOURCE);

                return objResponse;
            }
            objResponse = ResponseManager.GetSuccsessResp(SuccsessConstant.REGISTER_AVTIVITY_SUCCSESS);
            return objResponse;
        }

        /// <summary>
        /// Delete Check In
        /// </summary>
        /// <param name="x_strMemberId"></param>
        /// <param name="x_strActivityId"></param>
        /// <param name="x_strCheckInId"></param>
        /// <param name="x_strAccountId"></param>
        /// <param name="x_objContext"></param>
        /// <returns></returns>
        public async Task<ResponseModel> DeleteCheckIn(string x_strMemberId, string x_strActivityId, string x_strCheckInId, string x_strAccountId, HttpContext x_objContext)
        {
            Accounts objAccount;
            Roles objRole;
            EventInfo objEventInfo;
            MemberDecryptModel objMemberInfo;
            ResponseModel objResponse;
            string strClientIp;
            bool bResult;

            //----------------------------------
            // Get IP Address
            //----------------------------------
            strClientIp = x_objContext.GetClientIPAddress();
            //----------------------------------
            // Validate request role
            //----------------------------------
            // Account
            objAccount = await GetAccount(x_strAccountId);
            if (objAccount == null)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.ACCOUNT_IS_LOCKER, ErrorCode.ACCOUNT_IS_LOCKER);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.ACCOUNT_IS_LOCKER}", SOURCE);

                return objResponse;
            }

            // Member
            objMemberInfo = MemberInfoManager.GetMemberById(objAccount.MemberId);
            if (objMemberInfo == null)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.MEMBER_INFO_IS_NULL, ErrorCode.MEMBER_INFO_IS_NULL);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.MEMBER_INFO_IS_NULL}", SOURCE);

                return objResponse;
            }

            // Role
            objRole = await RolesDbManager.GetRoleById(objAccount.RoleId);
            if ((objRole == null) || (objRole.Permissions.Contains((int)DTUSVCPermission.DEL_CHECK_IN_ACTIVITY) == false))
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.AUTHORITY_INSUFFICIENT, ErrorCode.AUTHORITY_INSUFFICIENT);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMemberInfo.GetMemberFullName(), $"{SOURCE}: {ErrorConstant.MEMBER_INFO_IS_NULL}", SOURCE);

                return objResponse;
            }

            // Get Event info
            objEventInfo = await ActivityDbManager.GetEventInfoById(x_strActivityId);
            if (objEventInfo == null)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.ACTIVITY_IS_NULL, ErrorCode.ACTIVITY_IS_NULL);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMemberInfo.GetMemberFullName(), $"{SOURCE}: {ErrorConstant.ACTIVITY_IS_NULL}", SOURCE);
                return objResponse;
            }

            bResult = await MemberRegistEvtData.Instant.DeleteCheckIn(x_strMemberId, x_strActivityId, x_strCheckInId);
            if (bResult == false)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.ACTIVITY_IS_NULL, ErrorCode.DEL_CHECKIN_FAILED);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMemberInfo.GetMemberFullName(), $"{SOURCE}: {ErrorConstant.DEL_CHECKIN_FAILED}", SOURCE);
                return objResponse;
            }

            objResponse = ResponseManager.GetSuccsessResp(SuccsessConstant.DEL_CHECKIN_ACTIVITY_SUCCSESS);
            return objResponse;
        }

        /// <summary>
        /// Remove Registration
        /// </summary>
        /// <param name="x_strMemberId"></param>
        /// <param name="x_strActivityId"></param>
        /// <param name="x_strAccountId"></param>
        /// <param name="x_objContext"></param>
        /// <returns></returns>
        public async Task<ResponseModel> DeleteRegistration(string x_strMemberId, string x_strActivityId, string x_strAccountId, HttpContext x_objContext)
        {
            Accounts objAccount;
            Roles objRole;
            MemberDecryptModel objMemberInfo;
            ResponseModel objResponse;
            EventInfo objEventInfo;
            DateTime objCurrentDate;

            bool bRes;
            string strClientIp;

            //----------------------------------
            // Get IP Address
            //----------------------------------
            strClientIp = x_objContext.GetClientIPAddress();
            //----------------------------------
            // Validate request role
            //----------------------------------
            // Account
            objAccount = await GetAccount(x_strAccountId);
            if (objAccount == null)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.ACCOUNT_IS_LOCKER, ErrorCode.ACCOUNT_IS_LOCKER);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.ACCOUNT_IS_LOCKER}", SOURCE);

                return objResponse;
            }

            // Member
            objMemberInfo = MemberInfoManager.GetMemberById(objAccount.MemberId);
            if (objMemberInfo == null)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.MEMBER_INFO_IS_NULL, ErrorCode.MEMBER_INFO_IS_NULL);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.MEMBER_INFO_IS_NULL}", SOURCE);

                return objResponse;
            }

            // Role
            objRole = await RolesDbManager.GetRoleById(objAccount.RoleId);
            if ((objRole == null) || (objRole.Permissions.Contains((int)DTUSVCPermission.DELETE_MEMBER_IN_ACTIVITY) == false))
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.AUTHORITY_INSUFFICIENT, ErrorCode.AUTHORITY_INSUFFICIENT);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMemberInfo.GetMemberFullName(), $"{SOURCE}: {ErrorConstant.MEMBER_INFO_IS_NULL}", SOURCE);

                return objResponse;
            }

            // Get Event info
            objEventInfo = await ActivityDbManager.GetEventInfoById(x_strActivityId);
            if (objEventInfo == null)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.ACTIVITY_IS_NULL, ErrorCode.ACTIVITY_IS_NULL);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMemberInfo.GetMemberFullName(), $"{SOURCE}: {ErrorConstant.ACTIVITY_IS_NULL}", SOURCE);
                return objResponse;
            }
            // Check end date
            objCurrentDate = DateTimeHelper.GetCurrentDateTimeVN();
            if (objEventInfo.EndDate < objCurrentDate)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.ACTIVITY_IS_FINISHED, ErrorCode.ACTIVITY_IS_FINISHED);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMemberInfo.GetMemberFullName(), $"{SOURCE}: {ErrorConstant.ACTIVITY_IS_FINISHED}", SOURCE);
                return objResponse;
            }

            bRes = await MemberRegistEvtData.Instant.DeleteMemberRegistEvt(x_strMemberId, x_strActivityId);
            if (bRes == false)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.DELETE_MEMBER_ACTIVITY_FAILED, ErrorCode.DELETE_MEMBER_ACTIVITY_FAILED);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMemberInfo.GetMemberFullName(), $"{SOURCE}: {ErrorConstant.DELETE_MEMBER_ACTIVITY_FAILED}", SOURCE);
                return objResponse;
            }
            objResponse = ResponseManager.GetSuccsessResp(SuccsessConstant.DELETE_MEMBER_ACTIVITY_SUCCSESS);
            return objResponse;
        }

        /// <summary>
        /// Send Certificate
        /// </summary>
        /// <param name="x_objRequest"></param>
        /// <param name="x_strAccountId"></param>
        /// <param name="x_objContext"></param>
        /// <returns></returns>
        public async Task<ResponseModel> SendCertificate(SendCertificateRequestModel x_objRequest, string x_strAccountId, HttpContext x_objContext)
        {
            Accounts objAccount;
            Roles objRole;
            MemberDecryptModel objMemberInfo;
            ResponseModel objResponse;
            EventInfo objEventInfo;
            Task objTask;

            string strClientIp;
            string strFilePath;

            //----------------------------------
            // Get IP Address
            //----------------------------------
            strClientIp = x_objContext.GetClientIPAddress();
            //----------------------------------
            // Validate request model
            //----------------------------------
            if ((x_objRequest == null) || ((x_objRequest.IsSendAll == false) && ((x_objRequest.MemberIds == null) || (x_objRequest.MemberIds.Count == 0))))
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.SEND_CERTIFICATE_REQUEST_INVALID, ErrorCode.SEND_CERTIFICATE_REQUEST_INVALID);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.SEND_CERTIFICATE_REQUEST_INVALID}", SOURCE);

                return objResponse;
            }
            //----------------------------------
            // Validate request role
            //----------------------------------
            // Account
            objAccount = await GetAccount(x_strAccountId);
            if (objAccount == null)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.ACCOUNT_IS_LOCKER, ErrorCode.ACCOUNT_IS_LOCKER);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.ACCOUNT_IS_LOCKER}", SOURCE);

                return objResponse;
            }

            // Member
            objMemberInfo = MemberInfoManager.GetMemberById(objAccount.MemberId);
            if (objMemberInfo == null)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.MEMBER_INFO_IS_NULL, ErrorCode.MEMBER_INFO_IS_NULL);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.MEMBER_INFO_IS_NULL}", SOURCE);

                return objResponse;
            }

            // Role
            objRole = await RolesDbManager.GetRoleById(objAccount.RoleId);
            if ((objRole == null) || (objRole.Permissions.Contains((int)DTUSVCPermission.GET_SETTING_TEXT) == false))
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.AUTHORITY_INSUFFICIENT, ErrorCode.AUTHORITY_INSUFFICIENT);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMemberInfo.GetMemberFullName(), $"{SOURCE}: {ErrorConstant.MEMBER_INFO_IS_NULL}", SOURCE);

                return objResponse;
            }

            // Get Event info
            objEventInfo = await ActivityDbManager.GetEventInfoById(x_objRequest.ActivityId);
            if (objEventInfo == null)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.ACTIVITY_IS_NULL, ErrorCode.ACTIVITY_IS_NULL);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMemberInfo.GetMemberFullName(), $"{SOURCE}: {ErrorConstant.ACTIVITY_IS_NULL}", SOURCE);
                return objResponse;
            }
            if (objEventInfo.IsCertificateSending == true)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.CERTIFICATE_IS_SENDING, ErrorCode.CERTIFICATE_IS_SENDING);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMemberInfo.GetMemberFullName(), $"{SOURCE}: {ErrorConstant.CERTIFICATE_IS_SENDING}", SOURCE);
                return objResponse;
            }
            // Check Certificate
            if (string.IsNullOrWhiteSpace(objEventInfo.CertificateFile) == true)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.CERTIFICATE_IS_NULL, ErrorCode.CERTIFICATE_IS_NULL);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMemberInfo.GetMemberFullName(), $"{SOURCE}: {ErrorConstant.CERTIFICATE_IS_NULL}", SOURCE);
                return objResponse;
            }
            strFilePath = FileHelper.GetFilePath(SysFolder.ACTIVITY_CERTIFICATE_PATH, objEventInfo.CertificateFile);
            if (File.Exists(strFilePath) == false)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.CERTIFICATE_IS_NULL, ErrorCode.CERTIFICATE_IS_NULL);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMemberInfo.GetMemberFullName(), $"{SOURCE}: {ErrorConstant.CERTIFICATE_IS_NULL}", SOURCE);
                return objResponse;
            }

            // Update Activity state
            objEventInfo.IsCertificateSending = true;
            await ActivityDbManager.UpdateEventInfo(objEventInfo);
            objTask = Task.Run(async () =>
            {
                await SendCertificate(x_objRequest.DeepClone());
            });
            objResponse = ResponseManager.GetSuccsessResp(SuccsessConstant.SEND_CERTIFICATE_SUCCSESS);
            return objResponse;
        }

        /// <summary>
        /// Update Activitie
        /// </summary>
        /// <param name="x_objRequest"></param>
        /// <param name="x_strAccountId"></param>
        /// <param name="x_objContext"></param>
        /// <returns></returns>
        public async Task<ResponseModel> UpdateActivitie(UpdateActivityRequestModel x_objRequest, string x_strAccountId, HttpContext x_objContext)
        {
            Accounts objAccount;
            Roles objRole;
            MemberDecryptModel objMemberInfo;
            ResponseModel objResponse;
            AddressInfo objAddress;
            Bitmap objPoster;
            EventInfo objEventInfo;
            ActivityResponseModel objActivityResponseModel;
            DateTime objStartTime;
            DateTime objEndTime;
            DateTime objDeadlineTime;
            DateTime objCurrentDate;

            bool bRes;
            string strClientIp;
            string strBuf;
            string strExtension;
            string strFileName;
            string strFilePath;

            //----------------------------------
            // Get IP Address
            //----------------------------------
            strClientIp = x_objContext.GetClientIPAddress();

            //----------------------------------
            // Validate request data
            //----------------------------------
            objResponse = ActivityValidate(x_objRequest, out objAddress);
            if (objResponse.IsSuccsess == false)
            {
                return objResponse;
            }

            //----------------------------------
            // Validate request role
            //----------------------------------
            // Account
            objAccount = await GetAccount(x_strAccountId);
            if (objAccount == null)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.ACCOUNT_IS_LOCKER, ErrorCode.ACCOUNT_IS_LOCKER);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.ACCOUNT_IS_LOCKER}", SOURCE);

                return objResponse;
            }

            // Member
            objMemberInfo = MemberInfoManager.GetMemberById(objAccount.MemberId);
            if (objMemberInfo == null)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.MEMBER_INFO_IS_NULL, ErrorCode.MEMBER_INFO_IS_NULL);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.MEMBER_INFO_IS_NULL}", SOURCE);

                return objResponse;
            }

            // Role
            objRole = await RolesDbManager.GetRoleById(objAccount.RoleId);
            if ((objRole == null) || (objRole.Permissions.Contains((int)DTUSVCPermission.UPDATE_ACTIVITY) == false))
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.AUTHORITY_INSUFFICIENT, ErrorCode.AUTHORITY_INSUFFICIENT);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMemberInfo.GetMemberFullName(), $"{SOURCE}: {ErrorConstant.MEMBER_INFO_IS_NULL}", SOURCE);

                return objResponse;
            }

            // Get Event info
            objEventInfo = await ActivityDbManager.GetEventInfoById(x_objRequest.Id);
            if (objEventInfo == null)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.ACTIVITY_IS_NULL, ErrorCode.ACTIVITY_IS_NULL);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMemberInfo.GetMemberFullName(), $"{SOURCE}: {ErrorConstant.ACTIVITY_IS_NULL}", SOURCE);
                return objResponse;
            }
            // Check end date
            objCurrentDate = DateTimeHelper.GetCurrentDateTimeVN();
            if (objEventInfo.EndDate < objCurrentDate)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.ACTIVITY_IS_FINISHED, ErrorCode.ACTIVITY_IS_FINISHED);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMemberInfo.GetMemberFullName(), $"{SOURCE}: {ErrorConstant.ACTIVITY_IS_FINISHED}", SOURCE);
                return objResponse;
            }

            if (x_objRequest.Poster != null)
            {
                // Copy image
                strExtension = Path.GetExtension(x_objRequest.Poster.FileName);
                strFileName = FileHelper.GetRandomFileName(strExtension);
                strFilePath = FileHelper.GetFilePath(SysFolder.ACTIVITY_POSTER_PATH, strFileName);
                strBuf = await x_objRequest.Poster.SaveFormFile(strFilePath);
                if (string.IsNullOrEmpty(strBuf) == false)
                {
                    // Get error
                    objResponse = ResponseManager.GetErrorResp(strBuf, ErrorCode.UPLOAD_FILE_IS_NULL);
                    // Write log
                    LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMemberInfo.GetMemberFullName(), $"{SOURCE}: {strBuf}", SOURCE);

                    return objResponse;
                }
                // Crop image
                objPoster = FileHelper.OpenImageFile(strFilePath);
                objPoster = objPoster.CropImage(ImageHelper.POSTER_IMAGE, out strBuf);
                if (string.IsNullOrEmpty(strBuf) == true)
                {
                    objPoster.SaveBitmapStream(strFilePath);
                }
                objEventInfo.PosterPath = strFileName;
            }

            // Update Address
            objAddress.Id = objEventInfo.AddressId;
            bRes = await AddressDbManager.UpdateAddress(objAddress);
            if (bRes == false)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.SYSTEM_DISCONNECT, ErrorCode.SYSTEM_DISCONNECT);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMemberInfo.GetMemberFullName(), $"{SOURCE}: {ErrorConstant.SYSTEM_DISCONNECT}", SOURCE);

                return objResponse;
            }

            // Change Activity info
            objDeadlineTime = x_objRequest.RegistrationDeadline.ConvertStrToDateTime();
            objEndTime = x_objRequest.EndDate.ConvertStrToDateTime();
            objStartTime = x_objRequest.StartDate.ConvertStrToDateTime();
            objEventInfo.AddressId = objAddress.Id;
            objEventInfo.Content = x_objRequest.Content;
            objEventInfo.CreaterId = objMemberInfo.Id;
            objEventInfo.EndDate = objEndTime;
            objEventInfo.IsPublic = x_objRequest.IsPublic;
            objEventInfo.RegistrationDeadline = objDeadlineTime;
            objEventInfo.StartDate = objStartTime;
            objEventInfo.Title = x_objRequest.Title;
            bRes = await ActivityDbManager.UpdateEventInfo(objEventInfo);
            if (bRes == false)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.SYSTEM_DISCONNECT, ErrorCode.SYSTEM_DISCONNECT);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMemberInfo.GetMemberFullName(), $"{SOURCE}: {ErrorConstant.SYSTEM_DISCONNECT}", SOURCE);

                return objResponse;
            }

            objActivityResponseModel = await ConvertActivityDbToModel(objEventInfo);
            objResponse = ResponseManager.GetSuccsessResp(SuccsessConstant.UPDATE_ACTIVITY_SUCCSESS, objActivityResponseModel);
            return objResponse;
        }

        /// <summary>
        /// Update Check In
        /// </summary>
        /// <param name="x_objRequest"></param>
        /// <param name="x_strAccountId"></param>
        /// <param name="x_objContext"></param>
        /// <returns></returns>
        public async Task<ResponseModel> UpdateCheckIn(UpdateCheckInRequestModel x_objRequest, string x_strAccountId, HttpContext x_objContext)
        {
            const string DATE_TIME_FORMAT = "dd/MM/yyyy";
            Accounts objAccount;
            Roles objRole;
            MemberDecryptModel objMemberInfo;
            ResponseModel objResponse;
            MemberRegistEvtModel objMemberRegistEvtModel;
            EventInfo objEventInfo;
            ActivityChecklistData objActivityChecklistData;
            Task objTask;
            DateTime objCurrentDate;
            DateTime objStartDate;
            DateTime objEndDate;
            string strClientIp;
            string strBuf;
            string strCheckIn;
            string strCheckOut;
            bool bResult;

            //----------------------------------
            // Get IP Address
            //----------------------------------
            strClientIp = x_objContext.GetClientIPAddress();

            //----------------------------------
            // Validate request data
            //----------------------------------
            if (x_objRequest == null)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.UPDATE_CHECKIN_MODEL_INVALID, ErrorCode.UPDATE_CHECKIN_MODEL_INVALID);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.UPDATE_CHECKIN_MODEL_INVALID}", SOURCE);

                return objResponse;
            }
            if (x_objRequest.CheckInTime.CheckDateTimeFormat(out strBuf) == false)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.CHECKIN_TIME_INVALID, ErrorCode.CHECKIN_TIME_INVALID);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.CHECKIN_TIME_INVALID}", SOURCE);

                return objResponse;
            }
            objStartDate = x_objRequest.CheckInTime.ConvertStrToDateTime();
            strCheckIn = objStartDate.ToString(DATE_TIME_FORMAT);

            if (x_objRequest.CheckOutTime.CheckDateTimeFormat(out strBuf) == false)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.CHECKOUT_TIME_INVALID, ErrorCode.CHECKOUT_TIME_INVALID);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.CHECKOUT_TIME_INVALID}", SOURCE);

                return objResponse;
            }
            objEndDate = x_objRequest.CheckOutTime.ConvertStrToDateTime();
            strCheckOut = objEndDate.ToString(DATE_TIME_FORMAT);
            if (objStartDate == objEndDate)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.CHECKOUT_TIME_INVALID, ErrorCode.CHECKOUT_TIME_INVALID);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.CHECKOUT_TIME_INVALID}", SOURCE);

                return objResponse;
            }

            if (strCheckIn.Equals(strCheckOut) == false)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.DIFFERENT_DAY, ErrorCode.DIFFERENT_DAY);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.DIFFERENT_DAY}", SOURCE);

                return objResponse;
            }

            if (objStartDate > objEndDate)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.START_BIGGER_END_DATETIME, ErrorCode.START_BIGGER_END_DATETIME);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.START_BIGGER_END_DATETIME}", SOURCE);

                return objResponse;
            }

            if (x_objRequest.CheckInId.MongoIdIsValid() == false)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.CHECKIN_DOES_NOT_EXIST, ErrorCode.CHECKIN_DOES_NOT_EXIST);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.CHECKIN_DOES_NOT_EXIST}", SOURCE);
                return objResponse;
            }

            //----------------------------------
            // Validate request role
            //----------------------------------
            // Account
            objAccount = await GetAccount(x_strAccountId);
            if (objAccount == null)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.ACCOUNT_IS_LOCKER, ErrorCode.ACCOUNT_IS_LOCKER);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.ACCOUNT_IS_LOCKER}", SOURCE);

                return objResponse;
            }

            // Member
            objMemberInfo = MemberInfoManager.GetMemberById(objAccount.MemberId);
            if (objMemberInfo == null)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.MEMBER_INFO_IS_NULL, ErrorCode.MEMBER_INFO_IS_NULL);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.MEMBER_INFO_IS_NULL}", SOURCE);

                return objResponse;
            }

            // Role
            objRole = await RolesDbManager.GetRoleById(objAccount.RoleId);
            if ((objRole == null) || (objRole.Permissions.Contains((int)DTUSVCPermission.UPDATE_CHECK_IN_ACTIVITY) == false))
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.AUTHORITY_INSUFFICIENT, ErrorCode.AUTHORITY_INSUFFICIENT);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMemberInfo.GetMemberFullName(), $"{SOURCE}: {ErrorConstant.MEMBER_INFO_IS_NULL}", SOURCE);

                return objResponse;
            }

            // Get Event info
            objEventInfo = await ActivityDbManager.GetEventInfoById(x_objRequest.ActivityId);
            if (objEventInfo == null)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.ACTIVITY_IS_NULL, ErrorCode.ACTIVITY_IS_NULL);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMemberInfo.GetMemberFullName(), $"{SOURCE}: {ErrorConstant.ACTIVITY_IS_NULL}", SOURCE);
                return objResponse;
            }
            // Check end date
            objCurrentDate = DateTimeHelper.GetCurrentDateTimeVN();
            if (objEventInfo.EndDate < objCurrentDate)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.ACTIVITY_IS_FINISHED, ErrorCode.ACTIVITY_IS_FINISHED);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMemberInfo.GetMemberFullName(), $"{SOURCE}: {ErrorConstant.ACTIVITY_IS_FINISHED}", SOURCE);
                return objResponse;
            }

            objMemberRegistEvtModel = MemberRegistEvtData.Instant.GetMemberRegisEvt(x_objRequest.MemberId, x_objRequest.ActivityId);
            if (objMemberRegistEvtModel == null)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.MEMBER_IS_NOT_EXIST_ACTIVITY, ErrorCode.MEMBER_IS_NOT_EXIST_ACTIVITY);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMemberInfo.GetMemberFullName(), $"{SOURCE}: {ErrorConstant.MEMBER_IS_NOT_EXIST_ACTIVITY}", SOURCE);
                return objResponse;
            }

            objActivityChecklistData = objMemberRegistEvtModel.EvtCheckList.Find(obj => obj.Id.Equals(x_objRequest.CheckInId) == true);
            if (objActivityChecklistData == null)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.CHECKIN_DOES_NOT_EXIST, ErrorCode.CHECKIN_DOES_NOT_EXIST);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMemberInfo.GetMemberFullName(), $"{SOURCE}: {ErrorConstant.CHECKIN_DOES_NOT_EXIST}", SOURCE);
                return objResponse;
            }
            strBuf = objActivityChecklistData.EntryTime.ToString(DATE_TIME_FORMAT);
            if (strBuf.Equals(strCheckIn) == false)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.UPDATE_CHECKIN_ONLY_DATE, ErrorCode.UPDATE_CHECKIN_ONLY_DATE);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMemberInfo.GetMemberFullName(), $"{SOURCE}: {ErrorConstant.UPDATE_CHECKIN_ONLY_DATE}", SOURCE);
                return objResponse;
            }
            if (x_objRequest.AppliesToAll == true)
            {
                // Update all
                objTask = Task.Run(async () =>
                {
                    await UpdateCheckinAll(x_objRequest.ActivityId, objStartDate, objEndDate);
                });
                objResponse = ResponseManager.GetSuccsessResp(SuccsessConstant.UPDATE_ALL_CHECKIN_ACTIVITY_SUCCSESS);
                return objResponse;
            }
            else
            {
                // Update one
                objActivityChecklistData.EntryTime = objStartDate;
                objActivityChecklistData.TimeToLeave = objEndDate;
                bResult = await MemberRegistEvtData.Instant.UpdateCheckIn(objActivityChecklistData);
                objMemberRegistEvtModel.TotalScore = objMemberRegistEvtModel.EvtCheckList.Sum(obj => obj.Score);
            }

            objResponse = ResponseManager.GetSuccsessResp(SuccsessConstant.UPDATE_CHECKIN_ACTIVITY_SUCCSESS);
            return objResponse;
        }

        /// <summary>
        /// Update Setup Certificate
        /// </summary>
        /// <param name="x_objRequest"></param>
        /// <param name="x_strAccountId"></param>
        /// <param name="x_objContext"></param>
        /// <returns></returns>
        public async Task<ResponseModel> UpdateSetupCertificate(SetupCertificateTextRequestModel x_objRequest, string x_strAccountId, HttpContext x_objContext)
        {
            Accounts objAccount;
            Roles objRole;
            EventInfo objEventInfo;
            CertificateSettingText objSettingText;
            MemberDecryptModel objMemberInfo;
            ResponseModel objResponse;
            SettingTextResponse objSettingTextResponse;
            CertificateTextFont objFont;
            string strClientIp;
            string strFilePath;
            bool bResult;

            //----------------------------------
            // Get IP Address
            //----------------------------------
            strClientIp = x_objContext.GetClientIPAddress();
            //----------------------------------
            // Validate request Model
            //----------------------------------
            objResponse = SettingTextValidate(x_objRequest);
            if (objResponse.IsSuccsess == false)
            {
                return objResponse;
            }

            //----------------------------------
            // Validate request role
            //----------------------------------
            // Account
            objAccount = await GetAccount(x_strAccountId);
            if (objAccount == null)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.ACCOUNT_IS_LOCKER, ErrorCode.ACCOUNT_IS_LOCKER);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.ACCOUNT_IS_LOCKER}", SOURCE);

                return objResponse;
            }

            // Member
            objMemberInfo = MemberInfoManager.GetMemberById(objAccount.MemberId);
            if (objMemberInfo == null)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.MEMBER_INFO_IS_NULL, ErrorCode.MEMBER_INFO_IS_NULL);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.MEMBER_INFO_IS_NULL}", SOURCE);

                return objResponse;
            }

            // Role
            objRole = await RolesDbManager.GetRoleById(objAccount.RoleId);
            if ((objRole == null) || (objRole.Permissions.Contains((int)DTUSVCPermission.UPDATE_SETTING_TEXT) == false))
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.AUTHORITY_INSUFFICIENT, ErrorCode.AUTHORITY_INSUFFICIENT);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMemberInfo.GetMemberFullName(), $"{SOURCE}: {ErrorConstant.MEMBER_INFO_IS_NULL}", SOURCE);

                return objResponse;
            }

            // Get Event info
            objEventInfo = await ActivityDbManager.GetEventInfoById(x_objRequest.ActivityId);
            if (objEventInfo == null)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.ACTIVITY_IS_NULL, ErrorCode.ACTIVITY_IS_NULL);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMemberInfo.GetMemberFullName(), $"{SOURCE}: {ErrorConstant.ACTIVITY_IS_NULL}", SOURCE);
                return objResponse;
            }
            if (objEventInfo.IsCertificateSending == true)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.CERTIFICATE_IS_SENDING, ErrorCode.CERTIFICATE_IS_SENDING);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMemberInfo.GetMemberFullName(), $"{SOURCE}: {ErrorConstant.CERTIFICATE_IS_SENDING}", SOURCE);
                return objResponse;
            }

            // Check Font Id
            objFont = await ActivityDbManager.GetTextFontById(x_objRequest.FontId);
            if (objFont == null)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.FONT_INVALID, ErrorCode.FONT_INVALID);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMemberInfo.GetMemberFullName(), $"{SOURCE}: {ErrorConstant.FONT_INVALID}", SOURCE);
                return objResponse;
            }

            if (x_objRequest.CertificateTextId.MongoIdIsValid() == true)
            {
                objSettingText = await ActivityDbManager.GetSettingTextById(x_objRequest.CertificateTextId);
                objSettingText.FontId = x_objRequest.FontId;
                objSettingText.ActivityId = x_objRequest.ActivityId;
                objSettingText.Color = x_objRequest.Color;
                objSettingText.CreaterId = objMemberInfo.Id;
                objSettingText.FontSize = x_objRequest.FontSize;
                objSettingText.DefaultText = x_objRequest.DefaultText;
                objSettingText.FontStyle = x_objRequest.FontStyle;
                objSettingText.Horizontal = x_objRequest.Horizontal;
                objSettingText.LocationX = x_objRequest.LocationX;
                objSettingText.LocationY = x_objRequest.LocationX;
                objSettingText.MaxHeight = x_objRequest.MaxHeight;
                objSettingText.MaxWidth = x_objRequest.MaxWidth;
                objSettingText.ValueType = x_objRequest.ValueType;
                objSettingText.Vertical = x_objRequest.Vertical;
            }
            else
            {
                objSettingText = new CertificateSettingText
                {
                    FontId = x_objRequest.FontId,
                    ActivityId = x_objRequest.ActivityId,
                    Color = x_objRequest.Color,
                    CreaterId = objMemberInfo.Id,
                    FontSize = x_objRequest.FontSize,
                    Id = string.Empty,
                    DefaultText = x_objRequest.DefaultText,
                    FontStyle = x_objRequest.FontStyle,
                    Horizontal = x_objRequest.Horizontal,
                    LocationX = x_objRequest.LocationX,
                    LocationY = x_objRequest.LocationX,
                    MaxHeight = x_objRequest.MaxHeight,
                    MaxWidth = x_objRequest.MaxWidth,
                    ValueType = x_objRequest.ValueType,
                    Vertical = x_objRequest.Vertical
                };
            }
            bResult = await ActivityDbManager.UpdateSettingText(objSettingText);
            if (bResult == false)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.UPDATE_SETTING_TEXT_FAILED, ErrorCode.UPDATE_SETTING_TEXT_FAILED);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMemberInfo.GetMemberFullName(), $"{SOURCE}: {ErrorConstant.UPDATE_SETTING_TEXT_FAILED}", SOURCE);
                return objResponse;
            }

            objSettingTextResponse = (SettingTextResponse)objSettingText;
            if (string.IsNullOrWhiteSpace(objEventInfo.CertificateFile) == false)
            {
                strFilePath = await CreateCertificateTemp(objEventInfo.Id);
                objSettingTextResponse.CertificateTemp = strFilePath;
            }
            objResponse = ResponseManager.GetSuccsessResp(SuccsessConstant.UPLOAD_CERTIFICATE_SUCCSESS, objSettingTextResponse);
            return objResponse;
        }

        /// <summary>
        /// Upload Certificate
        /// </summary>
        /// <param name="x_objRequest"></param>
        /// <param name="x_strAccountId"></param>
        /// <param name="x_objContext"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public async Task<ResponseModel> UploadCertificate(UploadCertificateRequestModel x_objRequest, string x_strAccountId, HttpContext x_objContext)
        {
            Accounts objAccount;
            Roles objRole;
            EventInfo objEventInfo;
            MemberDecryptModel objMemberInfo;
            ResponseModel objResponse;
            ActivityResponseModel objActivityResponseModel;
            string strClientIp;
            string strBuf;
            string strFileName;
            string strFilePath;
            bool bResult;

            //----------------------------------
            // Get IP Address
            //----------------------------------
            strClientIp = x_objContext.GetClientIPAddress();
            //----------------------------------
            // Validate request Model
            //----------------------------------
            if (x_objRequest == null)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.CERTIFICATE_UPDATE_INVALID, ErrorCode.CERTIFICATE_UPDATE_INVALID);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.CERTIFICATE_UPDATE_INVALID}", SOURCE);

                return objResponse;
            }
            // Check Poster
            if (x_objRequest.CertificateImage == null)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.CERTIFICATE_UPDATE_INVALID, ErrorCode.CERTIFICATE_UPDATE_INVALID);
                return objResponse;
            }
            // Check format
            if (x_objRequest.CertificateImage.FileIsValid(ContentType.IMAGE_CONTENT_TYPE, out strBuf) == false)
            {
                if (strBuf.EqualsIgnoreCase(ErrorConstant.FILE_DOWNLOADED_IN_WRONG_FORMAT) == true)
                {
                    strBuf = string.Format(ErrorConstant.FILE_DOWNLOADED_IN_WRONG_FORMAT, string.Join(" | ", ContentType.IMAGE_CONTENT_TYPE));
                }
                objResponse = ResponseManager.GetErrorResp(strBuf, ErrorCode.CERTIFICATE_UPDATE_INVALID);
                return objResponse;
            }
            // Scan virus
            strBuf = x_objRequest.CertificateImage.ScanVirus();
            if (strBuf.EqualsIgnoreCase(VirusScanner.EXIST_VIRUS) == true)
            {
                objResponse = ResponseManager.GetErrorResp(strBuf, ErrorCode.CERTIFICATE_UPDATE_INVALID);
                return objResponse;
            }

            //----------------------------------
            // Validate request role
            //----------------------------------
            // Account
            objAccount = await GetAccount(x_strAccountId);
            if (objAccount == null)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.ACCOUNT_IS_LOCKER, ErrorCode.ACCOUNT_IS_LOCKER);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.ACCOUNT_IS_LOCKER}", SOURCE);

                return objResponse;
            }

            // Member
            objMemberInfo = MemberInfoManager.GetMemberById(objAccount.MemberId);
            if (objMemberInfo == null)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.MEMBER_INFO_IS_NULL, ErrorCode.MEMBER_INFO_IS_NULL);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.MEMBER_INFO_IS_NULL}", SOURCE);

                return objResponse;
            }

            // Role
            objRole = await RolesDbManager.GetRoleById(objAccount.RoleId);
            if ((objRole == null) || (objRole.Permissions.Contains((int)DTUSVCPermission.UPDATE_CERTIFICATE) == false))
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.AUTHORITY_INSUFFICIENT, ErrorCode.AUTHORITY_INSUFFICIENT);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMemberInfo.GetMemberFullName(), $"{SOURCE}: {ErrorConstant.MEMBER_INFO_IS_NULL}", SOURCE);

                return objResponse;
            }

            // Get Event info
            objEventInfo = await ActivityDbManager.GetEventInfoById(x_objRequest.ActivityId);
            if (objEventInfo == null)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.ACTIVITY_IS_NULL, ErrorCode.ACTIVITY_IS_NULL);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMemberInfo.GetMemberFullName(), $"{SOURCE}: {ErrorConstant.ACTIVITY_IS_NULL}", SOURCE);
                return objResponse;
            }
            if (objEventInfo.IsCertificateSending == true)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.CERTIFICATE_IS_SENDING, ErrorCode.CERTIFICATE_IS_SENDING);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMemberInfo.GetMemberFullName(), $"{SOURCE}: {ErrorConstant.CERTIFICATE_IS_SENDING}", SOURCE);
                return objResponse;
            }

            // Save Certificate Image
            strFileName = FileHelper.GetRandomFileName(Path.GetExtension(x_objRequest.CertificateImage.FileName));
            strFilePath = FileHelper.GetFilePath(SysFolder.ACTIVITY_CERTIFICATE_PATH, strFileName);
            strBuf = await x_objRequest.CertificateImage.SaveFormFile(strFilePath);
            // Delete old file
            if (string.IsNullOrWhiteSpace(objEventInfo.CertificateFile) == false)
            {
                strFilePath = FileHelper.GetFilePath(SysFolder.ACTIVITY_CERTIFICATE_PATH, objEventInfo.CertificateFile);
                FileHelper.DeleteFile(strFilePath, out strBuf);
            }
            objEventInfo.CertificateFile = strFileName;
            // Update to Database
            bResult = await ActivityDbManager.UpdateEventInfo(objEventInfo);
            if (bResult == false)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.UPDATE_CERTIFICATE_FAILED, ErrorCode.UPDATE_CERTIFICATE_FAILED);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMemberInfo.GetMemberFullName(), $"{SOURCE}: {ErrorConstant.UPDATE_CERTIFICATE_FAILED}", SOURCE);
                return objResponse;
            }
            objActivityResponseModel = await ConvertActivityDbToModel(objEventInfo);
            // Create Certificate Temp
            strFilePath = await CreateCertificateTemp(objEventInfo.Id);
            objActivityResponseModel.CertificateFile = strFilePath;
            objResponse = ResponseManager.GetSuccsessResp(SuccsessConstant.UPLOAD_CERTIFICATE_SUCCSESS, objActivityResponseModel);
            return objResponse;
        }

        /// <summary>
        /// Upload Text Font
        /// </summary>
        /// <param name="x_objRequest"></param>
        /// <param name="x_strAccountId"></param>
        /// <param name="x_objContext"></param>
        /// <returns></returns>
        public async Task<ResponseModel> UploadTextFont(UploadTextRequestFont x_objRequest, string x_strAccountId, HttpContext x_objContext)
        {
            Accounts objAccount;
            MemberDecryptModel objMemberInfo;
            ResponseModel objResponse;
            Roles objRole;
            CertificateTextFont objCertificateTextFont;
            FontFamily objFont;
            string strClientIp;
            string strBuf;
            string strFileName;
            string strFilePath;
            bool bRes;

            //----------------------------------
            // Get IP Address
            //----------------------------------
            strClientIp = x_objContext.GetClientIPAddress();
            //----------------------------------
            // Validate font
            //----------------------------------

            if (x_objRequest.FontFile == null)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.UPLOAD_FILE_IS_NULL, ErrorCode.UPLOAD_FILE_IS_NULL);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.UPLOAD_FILE_IS_NULL}", SOURCE);
                return objResponse;
            }
            if (x_objRequest.FontFile.FileIsValid(ContentType.FONT_CONTENT_TYPE, out strBuf) == false)
            {
                if (strBuf.Equals(ErrorConstant.FILE_DOWNLOADED_IN_WRONG_FORMAT) == true)
                {
                    strBuf = string.Format(strBuf, string.Join(',', ContentType.FONT_CONTENT_TYPE));
                    objResponse = ResponseManager.GetErrorResp(strBuf, ErrorCode.FONT_TYPE_INVALID);
                    LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {strBuf}", SOURCE);
                    return objResponse;
                }
                else
                {
                    objResponse = ResponseManager.GetErrorResp(strBuf, ErrorCode.FONT_SIZE_INVALIE);
                    LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {strBuf}", SOURCE);
                    return objResponse;
                }
            }
            strBuf = x_objRequest.FontFile.ScanVirus();
            if (strBuf.Equals(VirusScanner.EXIST_VIRUS) == true)
            {
                objResponse = ResponseManager.GetErrorResp(strBuf, ErrorCode.FONT_SIZE_INVALIE);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {strBuf}", SOURCE);
                return objResponse;
            }
            //----------------------------------
            // Validate request role
            //----------------------------------
            // Account
            objAccount = await GetAccount(x_strAccountId);
            if (objAccount == null)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.ACCOUNT_IS_LOCKER, ErrorCode.ACCOUNT_IS_LOCKER);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.ACCOUNT_IS_LOCKER}", SOURCE);

                return objResponse;
            }

            // Member
            objMemberInfo = MemberInfoManager.GetMemberById(objAccount.MemberId);
            if (objMemberInfo == null)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.MEMBER_INFO_IS_NULL, ErrorCode.MEMBER_INFO_IS_NULL);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.MEMBER_INFO_IS_NULL}", SOURCE);

                return objResponse;
            }

            // Role
            objRole = await RolesDbManager.GetRoleById(objAccount.RoleId);
            if ((objRole == null) || (objRole.Permissions.Contains((int)DTUSVCPermission.UPDATE_CERTIFICATE_TEXT_FONT) == false))
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.AUTHORITY_INSUFFICIENT, ErrorCode.AUTHORITY_INSUFFICIENT);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMemberInfo.GetMemberFullName(), $"{SOURCE}: {ErrorConstant.MEMBER_INFO_IS_NULL}", SOURCE);

                return objResponse;
            }
            // Save file
            strFileName = x_objRequest.FontFile.FileName;
            strFilePath = FileHelper.GetFilePath(SysFolder.FONT_FILE_FOLDER, strFileName);
            await x_objRequest.FontFile.SaveFormFile(strFilePath, true);

            if (TextFontHelper.TryGetFont(strFilePath, out objFont) == false)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.FONT_INVALID, ErrorCode.FONT_INVALID);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.FONT_INVALID}", SOURCE);
                return objResponse;
            }
            // Update Database
            objCertificateTextFont = new CertificateTextFont
            {
                FontName = objFont.Name,
                FontPath = strFileName,
            };
            bRes = await ActivityDbManager.UpdateTextFont(objCertificateTextFont);
            if (bRes == false)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.FONT_UPLOAD_FAILED, ErrorCode.FONT_UPLOAD_FAILED);
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.FONT_UPLOAD_FAILED}", SOURCE);
                return objResponse;
            }
            strFilePath = Path.Combine(SysFolder.FONT_FILE_FOLDER, strFileName);
            objCertificateTextFont.FontPath = strFilePath;
            objResponse = ResponseManager.GetSuccsessResp(SuccsessConstant.UPLOAD_TEXT_FONT_SUCCSESS, objCertificateTextFont);
            return objResponse;
        }

        /// <summary>
        /// Get List Setup Certificate
        /// </summary>
        /// <param name="x_strActivityId"></param>
        /// <param name="x_strAccountId"></param>
        /// <param name="x_objContext"></param>
        /// <returns></returns>
        public async Task<ResponseModel> GetListSetupCertificate(string x_strActivityId, string x_strAccountId, HttpContext x_objContext)
        {
            Accounts objAccount;
            Roles objRole;
            MemberDecryptModel objMemberInfo;
            ResponseModel objResponse;
            EventInfo objEventInfo;
            List<CertificateSettingText> lstCertificateSettingText;

            string strClientIp;

            //----------------------------------
            // Get IP Address
            //----------------------------------
            strClientIp = x_objContext.GetClientIPAddress();
            //----------------------------------
            // Validate request role
            //----------------------------------
            // Account
            objAccount = await GetAccount(x_strAccountId);
            if (objAccount == null)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.ACCOUNT_IS_LOCKER, ErrorCode.ACCOUNT_IS_LOCKER);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.ACCOUNT_IS_LOCKER}", SOURCE);

                return objResponse;
            }

            // Member
            objMemberInfo = MemberInfoManager.GetMemberById(objAccount.MemberId);
            if (objMemberInfo == null)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.MEMBER_INFO_IS_NULL, ErrorCode.MEMBER_INFO_IS_NULL);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.MEMBER_INFO_IS_NULL}", SOURCE);

                return objResponse;
            }

            // Role
            objRole = await RolesDbManager.GetRoleById(objAccount.RoleId);
            if ((objRole == null) || (objRole.Permissions.Contains((int)DTUSVCPermission.GET_SETTING_TEXT) == false))
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.AUTHORITY_INSUFFICIENT, ErrorCode.AUTHORITY_INSUFFICIENT);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMemberInfo.GetMemberFullName(), $"{SOURCE}: {ErrorConstant.MEMBER_INFO_IS_NULL}", SOURCE);

                return objResponse;
            }

            // Get Event info
            objEventInfo = await ActivityDbManager.GetEventInfoById(x_strActivityId);
            if (objEventInfo == null)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.ACTIVITY_IS_NULL, ErrorCode.ACTIVITY_IS_NULL);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMemberInfo.GetMemberFullName(), $"{SOURCE}: {ErrorConstant.ACTIVITY_IS_NULL}", SOURCE);
                return objResponse;
            }

            lstCertificateSettingText = await ActivityDbManager.GetSettingTextByActivityId(x_strActivityId);
            if (lstCertificateSettingText == null || lstCertificateSettingText.Count == 0)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.SETTING_TEXT_IS_EMPTY, ErrorCode.SETTING_TEXT_IS_EMPTY);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMemberInfo.GetMemberFullName(), $"{SOURCE}: {ErrorConstant.SETTING_TEXT_IS_EMPTY}", SOURCE);
                return objResponse;
            }
            objResponse = ResponseManager.GetSuccsessResp(string.Empty, lstCertificateSettingText, lstCertificateSettingText.Count);
            return objResponse;
        }

        /// <summary>
        /// Get Statistics
        /// </summary>
        /// <param name="x_strActivityId"></param>
        /// <param name="x_strAccountId"></param>
        /// <param name="x_objContext"></param>
        /// <returns></returns>
        public async Task<ResponseModel> GetStatistics(string x_strActivityId, string x_strAccountId, HttpContext x_objContext)
        {
            const string DATETIME_FORMAT = "dd/MM/yyyy hh:MM";
            string strClientIp;
            int nCount;
            List<string> lstCreateDay;
            Accounts objAccount;
            MemberDecryptModel objMemberInfo;
            Roles objRole;
            ResponseModel objResponse;
            EventInfo objEventInfo;
            TotalActivityStatistics objTotalActivityStatistics;
            List<Facultys> lstFacultys;
            List<MemberRegistEvtModel> lstMemberRegistEvt;

            //----------------------------------
            // Get IP Address
            //----------------------------------
            strClientIp = x_objContext.GetClientIPAddress();
            //----------------------------------
            // Validate request role
            //----------------------------------
            // Account
            objAccount = await GetAccount(x_strAccountId);
            if (objAccount == null)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.ACCOUNT_IS_LOCKER, ErrorCode.ACCOUNT_IS_LOCKER);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.ACCOUNT_IS_LOCKER}", SOURCE);

                return objResponse;
            }

            // Member
            objMemberInfo = MemberInfoManager.GetMemberById(objAccount.MemberId);
            if (objMemberInfo == null)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.MEMBER_INFO_IS_NULL, ErrorCode.MEMBER_INFO_IS_NULL);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.MEMBER_INFO_IS_NULL}", SOURCE);

                return objResponse;
            }

            // Role
            objRole = await RolesDbManager.GetRoleById(objAccount.RoleId);
            if ((objRole == null) || (objRole.Permissions.Contains((int)DTUSVCPermission.GET_ACTIVITY_STATISTICS) == false))
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.AUTHORITY_INSUFFICIENT, ErrorCode.AUTHORITY_INSUFFICIENT);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMemberInfo.GetMemberFullName(), $"{SOURCE}: {ErrorConstant.MEMBER_INFO_IS_NULL}", SOURCE);

                return objResponse;
            }

            // Get Event info
            objEventInfo = await ActivityDbManager.GetEventInfoById(x_strActivityId);
            if (objEventInfo == null)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.ACTIVITY_IS_NULL, ErrorCode.ACTIVITY_IS_NULL);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMemberInfo.GetMemberFullName(), $"{SOURCE}: {ErrorConstant.ACTIVITY_IS_NULL}", SOURCE);
                return objResponse;
            }

            objTotalActivityStatistics = new TotalActivityStatistics();
            lstMemberRegistEvt = MemberRegistEvtData.Instant.GetListMemberRegistEvt(x_strActivityId);
            if (lstMemberRegistEvt == null || lstMemberRegistEvt.Count == 0)
            {
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.STATISTICS_IS_EMPTY, ErrorCode.STATISTICS_IS_EMPTY);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMemberInfo.GetMemberFullName(), $"{SOURCE}: {ErrorConstant.STATISTICS_IS_EMPTY}", SOURCE);
                return objResponse;
            }
            // By create day
            lstCreateDay = lstMemberRegistEvt.Select(obj => obj.CreateDate.ToString(DATETIME_FORMAT)).ToList();
            objTotalActivityStatistics.GroupByCreateDate = lstMemberRegistEvt.GroupBy(obj => obj.CreateDate.ToString(DATETIME_FORMAT))
                                                                             .Select(obj => new GroupByCreateDate
                                                                             {
                                                                                 CreateDate = obj.Key,
                                                                                 Count = obj.Count()
                                                                             })
                                                                             .ToList();

            // By Faculty
            objTotalActivityStatistics.Faculty = new List<ActivityStatisticsByFaculty>();
            lstFacultys = await FacultyDbManager.GetAllFaculty();
            foreach (Facultys objFacultyTemp in lstFacultys)
            {
                if (objFacultyTemp == null)
                {
                    continue;
                }
                nCount = lstMemberRegistEvt.FindAll(obj => obj.Faculty.Id.Equals(objFacultyTemp.Id) == true).Count();
                objTotalActivityStatistics.Faculty.Add(new ActivityStatisticsByFaculty
                {
                    FacultyName = objFacultyTemp.FacultyName,
                    MemberCount = nCount
                });
            }

            // By Participants
            objTotalActivityStatistics.Participants = new ActivityStatisticsByParticipants();
            nCount = lstMemberRegistEvt.FindAll(obj => obj.EvtCheckList != null && obj.EvtCheckList.Count > 0).Count();
            objTotalActivityStatistics.Participants.Joined = nCount;
            objTotalActivityStatistics.Participants.TotalMember = lstMemberRegistEvt.Count;
            objTotalActivityStatistics.Participants.NotParticipate = lstMemberRegistEvt.Count - nCount;

            // by Member Or Community
            objTotalActivityStatistics.MemberOrCommunity = new ActivityStatisticsByMemberOrCommunity();
            nCount = lstMemberRegistEvt.FindAll(obj => obj.IsMember == true).Count();
            objTotalActivityStatistics.MemberOrCommunity.CountMember = nCount;
            objTotalActivityStatistics.MemberOrCommunity.TotalMember = lstMemberRegistEvt.Count;
            objTotalActivityStatistics.MemberOrCommunity.CountCommunity = lstMemberRegistEvt.Count - nCount;

            objResponse = ResponseManager.GetSuccsessResp(string.Empty, objTotalActivityStatistics);
            return objResponse;
        }
        #endregion
    }
}
