﻿using DTUSVCv3_Library;
using Microsoft.AspNetCore.Http;

namespace DTUSVCv3_Services
{
    public interface IActivitiesService
    {
        #region CRUD Activity

        /// <summary>
        /// Create Activitie
        /// </summary>
        /// <param name="x_objRequest"></param>
        /// <param name="x_strAccountId"></param>
        /// <param name="x_objContext"></param>
        /// <returns></returns>
        Task<ResponseModel> CreateActivitie(CreateActivityRequestModel x_objRequest, string x_strAccountId, HttpContext x_objContext);
        /// <summary>
        /// Update Activitie
        /// </summary>
        /// <param name="x_objRequest"></param>
        /// <param name="x_strAccountId"></param>
        /// <param name="x_objContext"></param>
        /// <returns></returns>
        Task<ResponseModel> UpdateActivitie(UpdateActivityRequestModel x_objRequest, string x_strAccountId, HttpContext x_objContext);
        /// <summary>
        /// Delete Activitie
        /// </summary>
        /// <param name="x_strActivityId"></param>
        /// <param name="x_strAccountId"></param>
        /// <param name="x_objContext"></param>
        /// <returns></returns>
        Task<ResponseModel> DeleteActivitie(string x_strActivityId, string x_strAccountId, HttpContext x_objContext);
        /// <summary>
        /// Get List Activities
        /// </summary>
        /// <param name="x_strAccountId"></param>
        /// <param name="x_objContext"></param>
        /// <returns></returns>
        Task<ResponseModel> GetListActivities(int x_nPageIndex, int x_nPageSize, string x_strAccountId, HttpContext x_objContext);
        /// <summary>
        /// Get Activitie By Id
        /// </summary>
        /// <param name="x_strActivityId"></param>
        /// <param name="x_strAccountId"></param>
        /// <param name="x_objContext"></param>
        /// <returns></returns>
        Task<ResponseModel> GetActivitieById(string x_strActivityId, string x_strAccountId, HttpContext x_objContext);
        /// <summary>
        /// Get Activity Histories
        /// </summary>
        /// <param name="x_strAccountId"></param>
        /// <param name="x_objContext"></param>
        /// <returns></returns>
        Task<ResponseModel> GetActivityHistories(string x_strAccountId, HttpContext x_objContext);
        #endregion

        #region Registration
        /// <summary>
        /// Community Registration
        /// </summary>
        /// <param name="x_objRequest"></param>
        /// <param name="x_objContext"></param>
        /// <returns></returns>
        Task<ResponseModel> CommunityRegistration(CommunityRegisterRequestModel x_objRequest, HttpContext x_objContext);
        /// <summary>
        /// Member Registration
        /// </summary>
        /// <param name="x_strActivityId"></param>
        /// <param name="x_strAccountId"></param>
        /// <param name="x_objContext"></param>
        /// <returns></returns>
        Task<ResponseModel> MemberRegistration(string x_strActivityId, string x_strAccountId, HttpContext x_objContext);
        /// <summary>
        /// Member Leave Registration
        /// </summary>
        /// <param name="x_objRequest"></param>
        /// <param name="x_strAccountId"></param>
        /// <param name="x_objContext"></param>
        /// <returns></returns>
        Task<ResponseModel> MemberLeaveRegistration(MemberLeaveActivityRequestModel x_objRequest, string x_strAccountId, HttpContext x_objContext);
        /// <summary>
        /// Remove Registration
        /// </summary>
        /// <param name="x_strMemberId"></param>
        /// <param name="x_strActivityId"></param>
        /// <param name="x_strAccountId"></param>
        /// <param name="x_objContext"></param>
        /// <returns></returns>
        Task<ResponseModel> DeleteRegistration(string x_strMemberId, string x_strActivityId, string x_strAccountId, HttpContext x_objContext);
        /// <summary>
        /// Check In
        /// </summary>
        /// <param name="x_strMemberId"></param>
        /// <param name="x_strActivityId"></param>
        /// <param name="x_strAccountId"></param>
        /// <param name="x_objContext"></param>
        /// <returns></returns>
        Task<ResponseModel> CheckIn(string x_strMemberId, string x_strActivityId, string x_strAccountId, HttpContext x_objContext);
        /// <summary>
        /// Delete Check In
        /// </summary>
        /// <param name="x_strMemberId"></param>
        /// <param name="x_strActivityId"></param>
        /// <param name="x_strCheckInId"></param>
        /// <param name="x_strAccountId"></param>
        /// <param name="x_objContext"></param>
        /// <returns></returns>
        Task<ResponseModel> DeleteCheckIn(string x_strMemberId, string x_strActivityId, string x_strCheckInId, string x_strAccountId, HttpContext x_objContext);
        /// <summary>
        /// Update Check In
        /// </summary>
        /// <param name="x_objRequest"></param>
        /// <param name="x_strAccountId"></param>
        /// <param name="x_objContext"></param>
        /// <returns></returns>
        Task<ResponseModel> UpdateCheckIn(UpdateCheckInRequestModel x_objRequest, string x_strAccountId, HttpContext x_objContext);
        /// <summary>
        /// Get List Member In Activity
        /// </summary>
        /// <param name="x_strActivityId"></param>
        /// <param name="x_strAccountId"></param>
        /// <param name="x_objContext"></param>
        /// <returns></returns>
        Task<ResponseModel> GetListMemberInActivity(string x_strActivityId, string x_strAccountId, HttpContext x_objContext);
        /// <summary>
        /// Download List Member In Activity
        /// </summary>
        /// <param name="x_strActivityId"></param>
        /// <param name="x_strAccountId"></param>
        /// <param name="x_objContext"></param>
        /// <returns></returns>
        Task<ResponseModel> DownloadActivityReport(string x_strActivityId, string x_strAccountId, HttpContext x_objContext);
        #endregion

        #region Certificate
        /// <summary>
        /// Upload Text Font
        /// </summary>
        /// <param name="x_objRequest"></param>
        /// <param name="x_strAccountId"></param>
        /// <param name="x_objContext"></param>
        /// <returns></returns>
        Task<ResponseModel> UploadTextFont(UploadTextRequestFont x_objRequest, string x_strAccountId, HttpContext x_objContext);
        /// <summary>
        /// Upload Certificate
        /// </summary>
        /// <param name="x_objRequest"></param>
        /// <param name="x_strAccountId"></param>
        /// <param name="x_objContext"></param>
        /// <returns></returns>
        Task<ResponseModel> UploadCertificate(UploadCertificateRequestModel x_objRequest, string x_strAccountId, HttpContext x_objContext);
        /// <summary>
        /// Get List Text Font
        /// </summary>
        /// <param name="x_strAccountId"></param>
        /// <param name="x_objContext"></param>
        /// <returns></returns>
        Task<ResponseModel> GetListTextFont(string x_strAccountId, HttpContext x_objContext);
        /// <summary>
        /// Update Setup Certificate
        /// </summary>
        /// <param name="x_objRequest"></param>
        /// <param name="x_strAccountId"></param>
        /// <param name="x_objContext"></param>
        /// <returns></returns>
        Task<ResponseModel> UpdateSetupCertificate(SetupCertificateTextRequestModel x_objRequest, string x_strAccountId, HttpContext x_objContext);
        /// <summary>
        /// Delete Setup Certificate
        /// </summary>
        /// <param name="x_strCertificateTextId"></param>
        /// <param name="x_strAccountId"></param>
        /// <param name="x_objContext"></param>
        /// <returns></returns>
        Task<ResponseModel> DeleteSetupCertificate(string x_strCertificateTextId, string x_strAccountId, HttpContext x_objContext);
        /// <summary>
        /// Get List Setup Certificate
        /// </summary>
        /// <param name="x_strActivityId"></param>
        /// <param name="x_strAccountId"></param>
        /// <param name="x_objContext"></param>
        /// <returns></returns>
        Task<ResponseModel> GetListSetupCertificate(string x_strActivityId, string x_strAccountId, HttpContext x_objContext);
        /// <summary>
        /// Send Certificate
        /// </summary>
        /// <param name="x_objRequest"></param>
        /// <param name="x_strAccountId"></param>
        /// <param name="x_objContext"></param>
        /// <returns></returns>
        Task<ResponseModel> SendCertificate(SendCertificateRequestModel x_objRequest, string x_strAccountId, HttpContext x_objContext);
        /// <summary>
        /// Confirm Certificate
        /// </summary>
        /// <param name="x_strStudentId"></param>
        /// <param name="x_strActivityId"></param>
        /// <param name="x_objContext"></param>
        /// <returns></returns>
        Task<ResponseModel> ConfirmCertificate(string x_strStudentId, string x_strActivityId, HttpContext x_objContext);
        /// <summary>
        /// Statistics
        /// </summary>
        /// <param name="x_strActivityId"></param>
        /// <param name="x_objContext"></param>
        /// <returns></returns>
        Task<ResponseModel> GetStatistics(string x_strActivityId, string x_strAccountId, HttpContext x_objContext);
        #endregion
    }
}
