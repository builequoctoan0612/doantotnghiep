﻿using DTUSVCv3_EntityFramework;
using DTUSVCv3_Library;
using DTUSVCv3_MemoryData;
using Google.Apis.Drive.v3.Data;
using Microsoft.AspNetCore.Http;
using static Google.Apis.Requests.BatchRequest;

namespace DTUSVCv3_Services
{
    public class SoftwareService : BaseSerivce, ISoftwareService
    {
        public SoftwareService()
        {
            m_objSoftwareDbManager = new SoftwareDbManager();
        }

        private readonly SoftwareDbManager m_objSoftwareDbManager;
        private const string SOURCE = "SoftwareService";
        /// <summary>
        /// Validate Update Data
        /// </summary>
        /// <param name="x_objRequest"></param>
        /// <param name="x_bIsUpdate"></param>
        /// <returns></returns>
        private ResponseModel ValidateUpdateData(CreateRequestSoftwareModel x_objRequest, bool x_bIsUpdate)
        {
            ResponseModel objResponse;
            string strBuf;

            if (x_objRequest == null)
            {
                // Error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.UPDATE_SOFTWARE_REQUEST_INVALID, ErrorCode.UPDATE_SOFTWARE_REQUEST_INVALID);
                return objResponse;
            }

            strBuf = x_objRequest.Title;
            if (string.IsNullOrWhiteSpace(strBuf))
            {
                // Error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.TITLE_SOFTWARE_REQUEST_INVALID, ErrorCode.TITLE_SOFTWARE_REQUEST_INVALID);
                return objResponse;
            }

            strBuf = x_objRequest.Description.RemoveHTMLTag();
            if (string.IsNullOrWhiteSpace(strBuf))
            {
                // Error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.DESCRIPTION_SOFTWARE_REQUEST_INVALID, ErrorCode.DESCRIPTION_SOFTWARE_REQUEST_INVALID);
                return objResponse;
            }

            strBuf = x_objRequest.FileName;
            if (string.IsNullOrWhiteSpace(strBuf))
            {
                // Error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.FILE_NAME_SOFTWARE_REQUEST_INVALID, ErrorCode.FILE_NAME_SOFTWARE_REQUEST_INVALID);
                return objResponse;
            }

            strBuf = x_objRequest.Version;
            if (string.IsNullOrWhiteSpace(strBuf))
            {
                // Error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.VERSION_SOFTWARE_REQUEST_INVALID, ErrorCode.VERSION_SOFTWARE_REQUEST_INVALID);
                return objResponse;
            }
            // Check file
            if ((x_bIsUpdate == false) || (x_objRequest.SoftwareFile != null))
            {
                if (x_objRequest.SoftwareFile == null)
                {
                    objResponse = ResponseManager.GetErrorResp(ErrorConstant.SOFTWARE_FILE_REQUEST_INVALID, ErrorCode.SOFTWARE_FILE_REQUEST_INVALID);
                    return objResponse;
                }

                if (x_objRequest.SoftwareFile.FileIsValid(ContentType.ZIP_CONTENT_TYPE, out strBuf) == false)
                {
                    if (strBuf.EqualsIgnoreCase(ErrorConstant.FILE_DOWNLOADED_IN_WRONG_FORMAT) == true)
                    {
                        strBuf = string.Format(ErrorConstant.FILE_DOWNLOADED_IN_WRONG_FORMAT, string.Join(" | ", ContentType.ZIP_CONTENT_TYPE));
                    }
                    objResponse = ResponseManager.GetErrorResp(strBuf, ErrorCode.SOFTWARE_FILE_REQUEST_INVALID);
                    return objResponse;
                }

                // Scan virus
                strBuf = x_objRequest.SoftwareFile.ScanVirus();
                if (strBuf.EqualsIgnoreCase(VirusScanner.EXIST_VIRUS) == true)
                {
                    objResponse = ResponseManager.GetErrorResp(strBuf, ErrorCode.SOFTWARE_FILE_REQUEST_INVALID);
                    return objResponse;
                }
            }

            return new ResponseModel();
        }

        /// <summary>
        /// Add Role Soft ware By Id
        /// </summary>
        /// <param name="x_objRequest"></param>
        /// <param name="x_strAccountId"></param>
        /// <param name="x_objContext"></param>
        /// <returns></returns>
        public async Task<ResponseModel> AddRoleSoftwareById(AddRoleRequestSoftwareModel x_objRequest, string x_strAccountId, HttpContext x_objContext)
        {
            Accounts objAccount;
            MemberDecryptModel objMemberInfo;
            Roles objRole;
            ResponseModel objResponse;
            SoftwareInfo objSoftwareInfo; 
            ResponseSoftwareModel objResponseSoftwareModel;
            GGDrivePermission objGGDrivePermission;
            Permission objPermission;

            bool bResult;
            string strClientIp;
            //----------------------------------
            // Get IP Address
            //----------------------------------
            strClientIp = x_objContext.GetClientIPAddress();
            // Account
            objAccount = await GetAccount(x_strAccountId);
            if (objAccount == null)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.ACCOUNT_IS_LOCKER, ErrorCode.ACCOUNT_IS_LOCKER);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.ACCOUNT_IS_LOCKER}", SOURCE);

                return objResponse;
            }

            // Member
            objMemberInfo = MemberInfoManager.GetMemberById(objAccount.MemberId);
            if (objMemberInfo == null)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.MEMBER_INFO_IS_NULL, ErrorCode.MEMBER_INFO_IS_NULL);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.MEMBER_INFO_IS_NULL}", SOURCE);

                return objResponse;
            }

            // Role
            objRole = await RolesDbManager.GetRoleById(objAccount.RoleId);
            if ((objRole == null) || (objRole.Permissions.Contains((int)DTUSVCPermission.SHARE_SOFTWARE) == false))
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.AUTHORITY_INSUFFICIENT, ErrorCode.AUTHORITY_INSUFFICIENT);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMemberInfo.GetMemberFullName(), $"{SOURCE}: {ErrorConstant.MEMBER_INFO_IS_NULL}", SOURCE);
                return objResponse;
            }

            // Check software exist
            objSoftwareInfo = await m_objSoftwareDbManager.GetSoftwareById(x_objRequest.Id);
            if (objSoftwareInfo == null)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.SOFTWARE_IS_NULL, ErrorCode.SOFTWARE_IS_NULL);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMemberInfo.GetMemberFullName(), $"{SOURCE}: {ErrorConstant.SOFTWARE_IS_NULL}", SOURCE);
                return objResponse;
            }

            if (x_objRequest == null)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.GG_ROLE_ADD_REQUEST_INVALID, ErrorCode.GG_ROLE_ADD_REQUEST_INVALID);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMemberInfo.GetMemberFullName(), $"{SOURCE}: {ErrorConstant.GG_ROLE_ADD_REQUEST_INVALID}", SOURCE);
                return objResponse;
            }

            if ((x_objRequest.Type.EqualsIgnoreCase("anyone") == false) && 
                (x_objRequest.Type.EqualsIgnoreCase("domain") == false) &&
                (x_objRequest.Type.EqualsIgnoreCase("group") == false) &&
                (x_objRequest.Type.EqualsIgnoreCase("user") == false))
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.GG_ROLE_TYPE_INVALID, ErrorCode.GG_ROLE_TYPE_INVALID);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMemberInfo.GetMemberFullName(), $"{SOURCE}: {ErrorConstant.GG_ROLE_TYPE_INVALID}", SOURCE);
                return objResponse;
            }

            if ((x_objRequest.Role.EqualsIgnoreCase("reader") == false) && 
                (x_objRequest.Role.EqualsIgnoreCase("commenter") == false) &&
                (x_objRequest.Role.EqualsIgnoreCase("writer") == false) &&
                (x_objRequest.Role.EqualsIgnoreCase("organizer") == false) &&
                (x_objRequest.Role.EqualsIgnoreCase("owner") == false) &&
                (x_objRequest.Role.EqualsIgnoreCase("fileOrganizer") == false))
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.GG_ROLE_INVALID, ErrorCode.GG_ROLE_INVALID);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMemberInfo.GetMemberFullName(), $"{SOURCE}: {ErrorConstant.GG_ROLE_INVALID}", SOURCE);
                return objResponse;
            }

            if ((x_objRequest.Type.EqualsIgnoreCase("anyone") == false) && (x_objRequest.EmailAddress.EmailIsValid() == false))
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.GG_ROLE_ADD_EMAIL_INVALID, ErrorCode.GG_ROLE_ADD_EMAIL_INVALID);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMemberInfo.GetMemberFullName(), $"{SOURCE}: {ErrorConstant.GG_ROLE_ADD_EMAIL_INVALID}", SOURCE);
                return objResponse;
            }

            if (x_objRequest.Type.EqualsIgnoreCase("anyone") == true)
            {
                x_objRequest.EmailAddress = string.Empty;
            }
            objPermission = await GoogleDriveHelper.Instant.ShareFile(objSoftwareInfo.FileId, x_objRequest.Type, x_objRequest.Role, x_objRequest.EmailAddress);
            if (objPermission == null)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.GG_ROLE_ADD_FAILED, ErrorCode.GG_ROLE_ADD_FAILED);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMemberInfo.GetMemberFullName(), $"{SOURCE}: {ErrorConstant.GG_ROLE_ADD_FAILED}", SOURCE);
                return objResponse;
            }

            objGGDrivePermission = objSoftwareInfo.Permission.Find(obj => obj.PermissionId.Equals(objPermission.Id) == true);
            if (objGGDrivePermission == null)
            {
                objGGDrivePermission = new GGDrivePermission
                {
                    EmailAddress = x_objRequest.EmailAddress,
                    Role = objPermission.Role,
                    Type = objPermission.Type,
                    PermissionId = objPermission.Id
                };
                objSoftwareInfo.Permission.Add(objGGDrivePermission);
            }
            else
            {
                objGGDrivePermission.EmailAddress = x_objRequest.EmailAddress;
                objGGDrivePermission.Role = objPermission.Role;
                objGGDrivePermission.Type = objPermission.Type;
                objGGDrivePermission.PermissionId = objPermission.Id;
            }
            
            bResult = await m_objSoftwareDbManager.UpdateSoftware(objSoftwareInfo);
            if (bResult == false)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.GG_ROLE_ADD_FAILED, ErrorCode.GG_ROLE_ADD_FAILED);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMemberInfo.GetMemberFullName(), $"{SOURCE}: {ErrorConstant.GG_ROLE_ADD_FAILED}", SOURCE);
                return objResponse;
            }
            objResponseSoftwareModel = new ResponseSoftwareModel
            {
                CreaterName = objMemberInfo.GetMemberFullName(),
                Description = objSoftwareInfo.Description,
                DownloadLink = objSoftwareInfo.DownloadLink,
                FileId = objSoftwareInfo.FileId,
                FileName = objSoftwareInfo.FileName,
                Id = objSoftwareInfo.Id,
                Permission = objSoftwareInfo.Permission,
                Title = objSoftwareInfo.Title,
                UpdateDate = objSoftwareInfo.UpdateDate,
                Version = objSoftwareInfo.Version,
                ViewLink = objSoftwareInfo.ViewLink
            };
            objResponse = ResponseManager.GetSuccsessResp(SuccsessConstant.ADD_SHARE_FILE_SUCCSESS, objResponseSoftwareModel);
            return objResponse;
        }

        /// <summary>
        /// Create Software
        /// </summary>
        /// <param name="x_objRequest"></param>
        /// <param name="x_strAccountId"></param>
        /// <param name="x_objContext"></param>
        /// <returns></returns>
        public async Task<ResponseModel> CreateSoftware(CreateRequestSoftwareModel x_objRequest, string x_strAccountId, HttpContext x_objContext)
        {
            Accounts objAccount;
            MemberDecryptModel objMemberInfo;
            Roles objRole;
            ResponseModel objResponse;
            UploadedModel objUploadedFileModel;
            SoftwareInfo objSoftwareInfo;
            GGDrivePermission objGGDrivePermission;
            ResponseSoftwareModel objResponseSoftwareModel;

            string strClientIp;
            string strFilePath;
            string strBuf;
            bool bResult;
            //----------------------------------
            // Get IP Address
            //----------------------------------
            strClientIp = x_objContext.GetClientIPAddress();
            //----------------------------------
            // Validate
            //----------------------------------
            objResponse = ValidateUpdateData(x_objRequest, false);
            if (objResponse.IsSuccsess == false)
            {
                return objResponse;
            }
            // Account
            objAccount = await GetAccount(x_strAccountId);
            if (objAccount == null)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.ACCOUNT_IS_LOCKER, ErrorCode.ACCOUNT_IS_LOCKER);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.ACCOUNT_IS_LOCKER}", SOURCE);

                return objResponse;
            }

            // Member
            objMemberInfo = MemberInfoManager.GetMemberById(objAccount.MemberId);
            if (objMemberInfo == null)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.MEMBER_INFO_IS_NULL, ErrorCode.MEMBER_INFO_IS_NULL);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.MEMBER_INFO_IS_NULL}", SOURCE);

                return objResponse;
            }

            // Role
            objRole = await RolesDbManager.GetRoleById(objAccount.RoleId);
            if ((objRole == null) || (objRole.Permissions.Contains((int)DTUSVCPermission.UPDATE_SOFTWARE) == false))
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.AUTHORITY_INSUFFICIENT, ErrorCode.AUTHORITY_INSUFFICIENT);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMemberInfo.GetMemberFullName(), $"{SOURCE}: {ErrorConstant.MEMBER_INFO_IS_NULL}", SOURCE);
                return objResponse;
            }

            strFilePath = FileHelper.GetFilePath(SysFolder.SOFTWARE, x_objRequest.SoftwareFile.FileName, true);
            strBuf = await x_objRequest.SoftwareFile.SaveFormFile(strFilePath);
            if (string.IsNullOrWhiteSpace(strBuf) == false)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(strBuf, ErrorCode.SOFTWARE_FILE_REQUEST_INVALID);
                return objResponse;
            }
            // Update To Google Drive
            objUploadedFileModel = await GoogleDriveHelper.Instant.UploadFileToGoogleDrive(strFilePath, GGDriveRootFolderId.SOFTWARE_FOLDER_ID);
            if (objUploadedFileModel == null)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.GG_DRIVE_DISCONECTED, ErrorCode.GG_DRIVE_DISCONECTED);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMemberInfo.GetMemberFullName(), $"{SOURCE}: {ErrorConstant.GG_DRIVE_DISCONECTED}", SOURCE);
                return objResponse;
            }

            objSoftwareInfo = new SoftwareInfo
            {
                CreaterId = objMemberInfo.Id,
                DownloadLink = objUploadedFileModel.DownloadLink,
                Description = x_objRequest.Description,
                FileId = objUploadedFileModel.Id,
                FileName = x_objRequest.FileName,
                Title = x_objRequest.Title,
                Version = x_objRequest.Version,
                ViewLink = objUploadedFileModel.ViewLink
            };
            if (objUploadedFileModel.Permissions != null)
            {
                foreach (Google.Apis.Drive.v3.Data.Permission objPermission in objUploadedFileModel.Permissions)
                {
                    if (objPermission == null)
                    {
                        continue;
                    }
                    objGGDrivePermission = new GGDrivePermission
                    {
                        EmailAddress = objPermission.EmailAddress,
                        Role = objPermission.Role,
                        Type = objPermission.Type,
                        PermissionId = objPermission.Id
                    };
                    objSoftwareInfo.Permission.Add(objGGDrivePermission);
                }
            }
            bResult = await m_objSoftwareDbManager.UpdateSoftware(objSoftwareInfo);
            if (bResult == false)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.UPDATE_SOFTWARE_FALIED, ErrorCode.UPDATE_SOFTWARE_FALIED);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMemberInfo.GetMemberFullName(), $"{SOURCE}: {ErrorConstant.UPDATE_SOFTWARE_FALIED}", SOURCE);
                return objResponse;
            }
            FileHelper.DeleteFile(strFilePath, out strBuf);

            objResponseSoftwareModel = new ResponseSoftwareModel
            {
                CreaterName = objMemberInfo.GetMemberFullName(),
                Description = objSoftwareInfo.Description,
                DownloadLink = objSoftwareInfo.DownloadLink,
                FileId = objSoftwareInfo.FileId,
                FileName = objSoftwareInfo.FileName,
                Id = objSoftwareInfo.Id,
                Permission = objSoftwareInfo.Permission,
                Title = objSoftwareInfo.Title,
                UpdateDate = objSoftwareInfo.UpdateDate,
                Version = objSoftwareInfo.Version,
                ViewLink = objSoftwareInfo.ViewLink
            };
            objResponse = ResponseManager.GetSuccsessResp(SuccsessConstant.UPDATE_SOFTWARE_SUCCSESS, objResponseSoftwareModel);
            return objResponse;
        }

        /// <summary>
        /// Delete Role Software By Id
        /// </summary>
        /// <param name="x_objRequest"></param>
        /// <param name="x_strAccountId"></param>
        /// <param name="x_objContext"></param>
        /// <returns></returns>
        public async Task<ResponseModel> DeleteRoleSoftwareById(DeleteRoleRequestSoftwareModel x_objRequest, string x_strAccountId, HttpContext x_objContext)
        {
            Accounts objAccount;
            MemberDecryptModel objMemberInfo;
            Roles objRole;
            ResponseModel objResponse;
            SoftwareInfo objSoftwareInfo;
            ResponseSoftwareModel objResponseSoftwareModel;

            bool bResult;
            string strClientIp;
            //----------------------------------
            // Get IP Address
            //----------------------------------
            strClientIp = x_objContext.GetClientIPAddress();
            // Account
            objAccount = await GetAccount(x_strAccountId);
            if (objAccount == null)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.ACCOUNT_IS_LOCKER, ErrorCode.ACCOUNT_IS_LOCKER);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.ACCOUNT_IS_LOCKER}", SOURCE);

                return objResponse;
            }

            // Member
            objMemberInfo = MemberInfoManager.GetMemberById(objAccount.MemberId);
            if (objMemberInfo == null)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.MEMBER_INFO_IS_NULL, ErrorCode.MEMBER_INFO_IS_NULL);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.MEMBER_INFO_IS_NULL}", SOURCE);

                return objResponse;
            }

            // Role
            objRole = await RolesDbManager.GetRoleById(objAccount.RoleId);
            if ((objRole == null) || (objRole.Permissions.Contains((int)DTUSVCPermission.SHARE_SOFTWARE) == false))
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.AUTHORITY_INSUFFICIENT, ErrorCode.AUTHORITY_INSUFFICIENT);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMemberInfo.GetMemberFullName(), $"{SOURCE}: {ErrorConstant.MEMBER_INFO_IS_NULL}", SOURCE);
                return objResponse;
            }

            // Check software exist
            objSoftwareInfo = await m_objSoftwareDbManager.GetSoftwareById(x_objRequest.Id);
            if (objSoftwareInfo == null)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.SOFTWARE_IS_NULL, ErrorCode.SOFTWARE_IS_NULL);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMemberInfo.GetMemberFullName(), $"{SOURCE}: {ErrorConstant.SOFTWARE_IS_NULL}", SOURCE);
                return objResponse;
            }

            if (x_objRequest == null)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.GG_ROLE_DELETE_REQUEST_INVALID, ErrorCode.GG_ROLE_DELETE_REQUEST_INVALID);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMemberInfo.GetMemberFullName(), $"{SOURCE}: {ErrorConstant.GG_ROLE_DELETE_REQUEST_INVALID}", SOURCE);
                return objResponse;
            }

            if (string.IsNullOrWhiteSpace(x_objRequest.PermissionId) == true)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.PERMISSION_ID_INVALID, ErrorCode.PERMISSION_ID_INVALID);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMemberInfo.GetMemberFullName(), $"{SOURCE}: {ErrorConstant.PERMISSION_ID_INVALID}", SOURCE);
                return objResponse;
            }
            bResult = GoogleDriveHelper.Instant.RevokeFileSharing(objSoftwareInfo.FileId, x_objRequest.PermissionId);
            if (bResult == false)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.GG_ROLE_DELETE_ROLLE_FAILED, ErrorCode.GG_ROLE_DELETE_ROLLE_FAILED);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMemberInfo.GetMemberFullName(), $"{SOURCE}: {ErrorConstant.GG_ROLE_DELETE_ROLLE_FAILED}", SOURCE);
                return objResponse;
            }
            objSoftwareInfo.Permission.RemoveAll(obj => obj.PermissionId.Equals(x_objRequest.PermissionId) == true); 

            objSoftwareInfo.Permission.RemoveAll(obj => obj.PermissionId.EqualsIgnoreCase(x_objRequest.PermissionId));
            bResult = await m_objSoftwareDbManager.UpdateSoftware(objSoftwareInfo);
            if (bResult == false)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.GG_ROLE_DELETE_ROLLE_FAILED, ErrorCode.GG_ROLE_DELETE_ROLLE_FAILED);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMemberInfo.GetMemberFullName(), $"{SOURCE}: {ErrorConstant.GG_ROLE_DELETE_ROLLE_FAILED}", SOURCE);
                return objResponse;
            }
            objResponseSoftwareModel = new ResponseSoftwareModel
            {
                CreaterName = objMemberInfo.GetMemberFullName(),
                Description = objSoftwareInfo.Description,
                DownloadLink = objSoftwareInfo.DownloadLink,
                FileId = objSoftwareInfo.FileId,
                FileName = objSoftwareInfo.FileName,
                Id = objSoftwareInfo.Id,
                Permission = objSoftwareInfo.Permission,
                Title = objSoftwareInfo.Title,
                UpdateDate = objSoftwareInfo.UpdateDate,
                Version = objSoftwareInfo.Version,
                ViewLink = objSoftwareInfo.ViewLink
            };
            objResponse = ResponseManager.GetSuccsessResp(SuccsessConstant.DELETE_SHARE_FILE_SUCCSESS, objResponseSoftwareModel);
            return objResponse;
        }

        /// <summary>
        /// Delete Software
        /// </summary>
        /// <param name="x_strSoftwareId"></param>
        /// <param name="x_strAccountId"></param>
        /// <param name="x_objContext"></param>
        /// <returns></returns>
        public async Task<ResponseModel> DeleteSoftware(string x_strSoftwareId, string x_strAccountId, HttpContext x_objContext)
        {
            Accounts objAccount;
            MemberDecryptModel objMemberInfo;
            Roles objRole;
            ResponseModel objResponse;
            SoftwareInfo objSoftwareInfo;
            string strClientIp;
            bool bResult;

            //----------------------------------
            // Get IP Address
            //----------------------------------
            strClientIp = x_objContext.GetClientIPAddress();
            //----------------------------------
            // Validate
            //----------------------------------
            // Account
            objAccount = await GetAccount(x_strAccountId);
            if (objAccount == null)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.ACCOUNT_IS_LOCKER, ErrorCode.ACCOUNT_IS_LOCKER);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.ACCOUNT_IS_LOCKER}", SOURCE);

                return objResponse;
            }

            // Member
            objMemberInfo = MemberInfoManager.GetMemberById(objAccount.MemberId);
            if (objMemberInfo == null)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.MEMBER_INFO_IS_NULL, ErrorCode.MEMBER_INFO_IS_NULL);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.MEMBER_INFO_IS_NULL}", SOURCE);

                return objResponse;
            }

            // Role
            objRole = await RolesDbManager.GetRoleById(objAccount.RoleId);
            if ((objRole == null) || (objRole.Permissions.Contains((int)DTUSVCPermission.DELETE_SHARE_SOFTWARE) == false))
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.AUTHORITY_INSUFFICIENT, ErrorCode.AUTHORITY_INSUFFICIENT);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMemberInfo.GetMemberFullName(), $"{SOURCE}: {ErrorConstant.MEMBER_INFO_IS_NULL}", SOURCE);
                return objResponse;
            }

            // Check software exist
            objSoftwareInfo = await m_objSoftwareDbManager.GetSoftwareById(x_strSoftwareId);
            if (objSoftwareInfo == null)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.SOFTWARE_IS_NULL, ErrorCode.SOFTWARE_IS_NULL);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMemberInfo.GetMemberFullName(), $"{SOURCE}: {ErrorConstant.SOFTWARE_IS_NULL}", SOURCE);
                return objResponse;
            }
            // Delete
            bResult = await m_objSoftwareDbManager.DeleteSoftware(x_strSoftwareId);
            if (bResult == false)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.DELETE_SOFTWARE_FAILED, ErrorCode.DELETE_SOFTWARE_FAILED);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMemberInfo.GetMemberFullName(), $"{SOURCE}: {ErrorConstant.DELETE_SOFTWARE_FAILED}", SOURCE);
                return objResponse;
            }

            await GoogleDriveHelper.Instant.DeleteFile(objSoftwareInfo.FileId);
            objResponse = ResponseManager.GetSuccsessResp(SuccsessConstant.DELETE_SHARE_FILE_SUCCSESS);
            return objResponse;
        }

        /// <summary>
        /// Get All Software
        /// </summary>
        /// <returns></returns>
        public async Task<ResponseModel> GetAllSoftware()
        {
            List<ResponseSoftwareListModel> lstResponseSoftwareListModel;
            List<SoftwareInfo> lstSoftwareInfo;
            MemberDecryptModel objCreater;
            ResponseSoftwareListModel objResponseSoftwareListModel;
            ResponseModel objResponse;
            string strCreaterName;

            lstSoftwareInfo = await m_objSoftwareDbManager.GetAllSoftware();
            if (lstSoftwareInfo == null)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.SOFTWARE_IS_EMPTY, ErrorCode.SOFTWARE_IS_EMPTY);
            }

            lstResponseSoftwareListModel = new List<ResponseSoftwareListModel>();
            foreach (SoftwareInfo objSoft in lstSoftwareInfo)
            {
                if (objSoft == null)
                {
                    continue;
                }
                objCreater = MemberInfoManager.GetMemberById(objSoft.CreaterId);
                if (objCreater == null)
                {
                    strCreaterName = "Đang cập nhật";
                }
                else
                {
                    strCreaterName = objCreater.GetMemberFullName();
                }
                objResponseSoftwareListModel = new ResponseSoftwareListModel
                {
                    CreaterName = strCreaterName,
                    Description = objSoft.Description,
                    DownloadLink = objSoft.DownloadLink,
                    FileId = objSoft.FileId,
                    FileName = objSoft.FileName,
                    Id = objSoft.Id,
                    Title = objSoft.Title,
                    UpdateDate = objSoft.UpdateDate,
                    Version = objSoft.Version,
                    ViewLink = objSoft.ViewLink
                };
                lstResponseSoftwareListModel.Add(objResponseSoftwareListModel);
            }

            objResponse = ResponseManager.GetSuccsessResp(string.Empty, lstResponseSoftwareListModel, lstResponseSoftwareListModel.Count);
            return objResponse;
        }

        /// <summary>
        /// Get Software By Id
        /// </summary>
        /// <param name="x_strSoftwareId"></param>
        /// <param name="x_strAccountId"></param>
        /// <param name="x_objContext"></param>
        /// <returns></returns>
        public async Task<ResponseModel> GetSoftwareById(string x_strSoftwareId, string x_strAccountId, HttpContext x_objContext)
        {
            ResponseSoftwareModel objResponseSoftwareModel;
            SoftwareInfo objSoftware;
            ResponseModel objResponse;
            Accounts objAccount;
            MemberDecryptModel objCreater;
            Roles objRole;
            string strClientIp;
            string strCreaterName;

            //----------------------------------
            // Get IP Address
            //----------------------------------
            strClientIp = x_objContext.GetClientIPAddress();
            //----------------------------------
            // Get software
            //----------------------------------
            objSoftware = await m_objSoftwareDbManager.GetSoftwareById(x_strSoftwareId);
            if (objSoftware == null)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.SOFTWARE_IS_NULL, ErrorCode.SOFTWARE_IS_NULL);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.SOFTWARE_IS_NULL}", SOURCE);
                return objResponse;
            }
            // Get creater name
            objCreater = MemberInfoManager.GetMemberById(objSoftware.CreaterId);
            if (objCreater == null)
            {
                strCreaterName = "Đang cập nhật";
            }
            else
            {
                strCreaterName = objCreater.GetMemberFullName();
            }
            objResponseSoftwareModel = new ResponseSoftwareModel
            {
                CreaterName = strCreaterName,
                Description = objSoftware.Description,
                DownloadLink = objSoftware.DownloadLink,
                FileId = objSoftware.FileId,
                FileName = objSoftware.FileName,
                Id = objSoftware.Id,
                Title = objSoftware.Title,
                UpdateDate = objSoftware.UpdateDate,
                Version = objSoftware.Version,
                ViewLink = objSoftware.ViewLink,
                Permission = new List<GGDrivePermission>()
            };
            // Account
            objAccount = await GetAccount(x_strAccountId);
            if ((objAccount != null) && (objAccount.IsLocked == false))
            {
                // Role
                objRole = await RolesDbManager.GetRoleById(objAccount.RoleId);
                if ((objRole != null) && (objRole.Permissions.Contains((int)DTUSVCPermission.UPDATE_SOFTWARE) == true))
                {
                    if (objSoftware.Permission != null)
                    {
                        objResponseSoftwareModel.Permission = objSoftware.Permission;
                    }
                }
            }
            objResponse = ResponseManager.GetSuccsessResp(string.Empty, objResponseSoftwareModel);
            return objResponse;
        }

        /// <summary>
        /// Update Software
        /// </summary>
        /// <param name="x_objRequest"></param>
        /// <param name="x_strAccountId"></param>
        /// <param name="x_objContext"></param>
        /// <returns></returns>
        public async Task<ResponseModel> UpdateSoftware(UpdateRequestSoftwareModel x_objRequest, string x_strAccountId, HttpContext x_objContext)
        {
            Accounts objAccount;
            Roles objRole;
            MemberDecryptModel objMemberInfo;
            ResponseModel objResponse;
            UploadedModel objUploadedFileModel;
            SoftwareInfo objSoftwareInfo;
            GGDrivePermission objGGDrivePermission;
            ResponseSoftwareModel objResponseSoftwareModel;

            string strClientIp;
            string strFilePath;
            string strBuf;
            bool bResult;
            //----------------------------------
            // Get IP Address
            //----------------------------------
            strClientIp = x_objContext.GetClientIPAddress();
            //----------------------------------
            // Validate
            //----------------------------------
            objResponse = ValidateUpdateData(x_objRequest, true);
            if (objResponse.IsSuccsess == false)
            {
                return objResponse;
            }
            // Account
            objAccount = await GetAccount(x_strAccountId);
            if (objAccount == null)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.ACCOUNT_IS_LOCKER, ErrorCode.ACCOUNT_IS_LOCKER);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.ACCOUNT_IS_LOCKER}", SOURCE);

                return objResponse;
            }

            // Member
            objMemberInfo = MemberInfoManager.GetMemberById(objAccount.MemberId);
            if (objMemberInfo == null)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.MEMBER_INFO_IS_NULL, ErrorCode.MEMBER_INFO_IS_NULL);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.MEMBER_INFO_IS_NULL}", SOURCE);

                return objResponse;
            }

            // Role
            objRole = await RolesDbManager.GetRoleById(objAccount.RoleId);
            if ((objRole == null) || (objRole.Permissions.Contains((int)DTUSVCPermission.UPDATE_SOFTWARE) == false))
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.AUTHORITY_INSUFFICIENT, ErrorCode.AUTHORITY_INSUFFICIENT);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMemberInfo.GetMemberFullName(), $"{SOURCE}: {ErrorConstant.MEMBER_INFO_IS_NULL}", SOURCE);
                return objResponse;
            }

            // Check software exist
            objSoftwareInfo = await m_objSoftwareDbManager.GetSoftwareById(x_objRequest.Id);
            if (objSoftwareInfo == null)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.SOFTWARE_IS_NULL, ErrorCode.SOFTWARE_IS_NULL);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMemberInfo.GetMemberFullName(), $"{SOURCE}: {ErrorConstant.SOFTWARE_IS_NULL}", SOURCE);
                return objResponse;
            }

            if (x_objRequest.SoftwareFile != null)
            {
                strFilePath = FileHelper.GetFilePath(SysFolder.SOFTWARE, x_objRequest.SoftwareFile.FileName, true);
                strBuf = await x_objRequest.SoftwareFile.SaveFormFile(strFilePath);
                if (string.IsNullOrWhiteSpace(strBuf) == false)
                {
                    // Get error
                    objResponse = ResponseManager.GetErrorResp(strBuf, ErrorCode.SOFTWARE_FILE_REQUEST_INVALID);
                    return objResponse;
                }
                // Update To Google Drive
                objUploadedFileModel = await GoogleDriveHelper.Instant.UploadFileToGoogleDrive(strFilePath, GGDriveRootFolderId.SOFTWARE_FOLDER_ID);
                if (objUploadedFileModel == null)
                {
                    // Get error
                    objResponse = ResponseManager.GetErrorResp(ErrorConstant.GG_DRIVE_DISCONECTED, ErrorCode.GG_DRIVE_DISCONECTED);
                    // Write log
                    LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMemberInfo.GetMemberFullName(), $"{SOURCE}: {ErrorConstant.GG_DRIVE_DISCONECTED}", SOURCE);
                    return objResponse;
                }
                // Delete old file
                if (objSoftwareInfo.FileId.Equals(objUploadedFileModel.Id) == false)
                {
                    await GoogleDriveHelper.Instant.DeleteFile(objSoftwareInfo.FileId);
                }
                objSoftwareInfo.DownloadLink = objUploadedFileModel.DownloadLink;
                objSoftwareInfo.FileId = objUploadedFileModel.Id;
                objSoftwareInfo.ViewLink = objUploadedFileModel.ViewLink;
                if (objUploadedFileModel.Permissions != null)
                {
                    foreach (Google.Apis.Drive.v3.Data.Permission objPermission in objUploadedFileModel.Permissions)
                    {
                        if (objPermission == null)
                        {
                            continue;
                        }

                        objGGDrivePermission = objSoftwareInfo.Permission.Find(obj => obj.PermissionId.EqualsIgnoreCase(objPermission.Id) == true);
                        if (objGGDrivePermission == null)
                        {
                            objGGDrivePermission = new GGDrivePermission();
                            objGGDrivePermission.EmailAddress = objPermission.EmailAddress;
                            objGGDrivePermission.Role = objPermission.Role;
                            objGGDrivePermission.Type = objPermission.Type;
                            objGGDrivePermission.PermissionId = objPermission.Id;
                            objSoftwareInfo.Permission.Add(objGGDrivePermission);
                        }
                        else
                        {
                            objGGDrivePermission.EmailAddress = objPermission.EmailAddress;
                            objGGDrivePermission.Role = objPermission.Role;
                            objGGDrivePermission.Type = objPermission.Type;
                            objGGDrivePermission.PermissionId = objPermission.Id;
                        }

                    }
                }
                FileHelper.DeleteFile(strFilePath, out strBuf);
            }

            objSoftwareInfo.CreaterId = objMemberInfo.Id;
            objSoftwareInfo.Description = x_objRequest.Description;
            objSoftwareInfo.FileName = x_objRequest.FileName;
            objSoftwareInfo.Title = x_objRequest.Title;
            objSoftwareInfo.Version = x_objRequest.Version;
            
            bResult = await m_objSoftwareDbManager.UpdateSoftware(objSoftwareInfo);
            if (bResult == false)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.UPDATE_SOFTWARE_FALIED, ErrorCode.UPDATE_SOFTWARE_FALIED);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMemberInfo.GetMemberFullName(), $"{SOURCE}: {ErrorConstant.UPDATE_SOFTWARE_FALIED}", SOURCE);
                return objResponse;
            }

            objResponseSoftwareModel = new ResponseSoftwareModel
            {
                CreaterName = objMemberInfo.GetMemberFullName(),
                Description = objSoftwareInfo.Description,
                DownloadLink = objSoftwareInfo.DownloadLink,
                FileId = objSoftwareInfo.FileId,
                FileName = objSoftwareInfo.FileName,
                Id = objSoftwareInfo.Id,
                Permission = objSoftwareInfo.Permission,
                Title = objSoftwareInfo.Title,
                UpdateDate = objSoftwareInfo.UpdateDate,
                Version = objSoftwareInfo.Version,
                ViewLink = objSoftwareInfo.ViewLink
            };
            objResponse = ResponseManager.GetSuccsessResp(SuccsessConstant.UPDATE_SOFTWARE_SUCCSESS, objResponseSoftwareModel);
            return objResponse;
        }
    }
}
