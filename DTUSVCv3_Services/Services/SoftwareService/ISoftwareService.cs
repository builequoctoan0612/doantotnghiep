﻿using DTUSVCv3_Library;
using Microsoft.AspNetCore.Http;

namespace DTUSVCv3_Services
{
    public interface ISoftwareService
    {
        /// <summary>
        /// Create Software
        /// </summary>
        /// <param name="x_objRequest"></param>
        /// <param name="x_strAccountId"></param>
        /// <param name="x_objContext"></param>
        /// <returns></returns>
        Task<ResponseModel> CreateSoftware(CreateRequestSoftwareModel x_objRequest, string x_strAccountId, HttpContext x_objContext);
        /// <summary>
        /// Update Software
        /// </summary>
        /// <param name="x_objRequest"></param>
        /// <param name="x_strAccountId"></param>
        /// <param name="x_objContext"></param>
        /// <returns></returns>
        Task<ResponseModel> UpdateSoftware(UpdateRequestSoftwareModel x_objRequest, string x_strAccountId, HttpContext x_objContext);
        /// <summary>
        /// Delete Software
        /// </summary>
        /// <param name="x_strSoftwareId"></param>
        /// <param name="x_strAccountId"></param>
        /// <param name="x_objContext"></param>
        /// <returns></returns>
        Task<ResponseModel> DeleteSoftware(string x_strSoftwareId, string x_strAccountId, HttpContext x_objContext);
        /// <summary>
        /// Get All Software
        /// </summary>
        /// <returns></returns>
        Task<ResponseModel> GetAllSoftware();
        /// <summary>
        /// Get Software By Id
        /// </summary>
        /// <param name="x_strSoftwareId"></param>
        /// <param name="x_strAccountId"></param>
        /// <param name="x_objContext"></param>
        /// <returns></returns>
        Task<ResponseModel> GetSoftwareById(string x_strSoftwareId, string x_strAccountId, HttpContext x_objContext);
        /// <summary>
        /// Add Role Software By Id
        /// </summary>
        /// <param name="x_objRequest"></param>
        /// <param name="x_strAccountId"></param>
        /// <param name="x_objContext"></param>
        /// <returns></returns>
        Task<ResponseModel> AddRoleSoftwareById(AddRoleRequestSoftwareModel x_objRequest, string x_strAccountId, HttpContext x_objContext);
        /// <summary>
        /// Delete Role Software By Id
        /// </summary>
        /// <param name="x_objRequest"></param>
        /// <param name="x_strAccountId"></param>
        /// <param name="x_objContext"></param>
        /// <returns></returns>
        Task<ResponseModel> DeleteRoleSoftwareById(DeleteRoleRequestSoftwareModel x_objRequest, string x_strAccountId, HttpContext x_objContext);
    }
}
