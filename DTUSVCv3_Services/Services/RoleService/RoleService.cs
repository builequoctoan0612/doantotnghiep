﻿using DTUSVCv3_EntityFramework;
using DTUSVCv3_Library;
using DTUSVCv3_MemoryData;
using DTUSVCv3_Services.Models.RoleModel;
using Microsoft.AspNetCore.Http;

namespace DTUSVCv3_Services
{
    public class RoleService : BaseSerivce, IRoleService
    {
        public RoleService() : base()
        {

        }

        private const string SOURCE = "RoleService";

        /// <summary>
        /// Create Role
        /// </summary>
        /// <param name="x_objRequest"></param>
        /// <param name="x_strAccountId"></param>
        /// <param name="x_objContext"></param>
        /// <returns></returns>
        public async Task<ResponseModel> CreateRole(CreateRoleRequestModel x_objRequest, string x_strAccountId, HttpContext x_objContext)
        {
            Accounts objAccount;
            MemberDecryptModel objMemberInfo;
            Roles objRole;
            ResponseModel objResponse;
            DTUSVCPermission ePermission;
            List<Roles> lstRoles;
            List<int> lstPermission;
            bool bResult;
            string strClientIp;

            //----------------------------------
            // Get IP Address
            //----------------------------------
            strClientIp = x_objContext.GetClientIPAddress();
            //----------------------------------
            // Validate request role
            //----------------------------------
            if (string.IsNullOrWhiteSpace(x_objRequest.RoleName) == true)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.ROLE_NAME_INVALID, ErrorCode.ROLE_NAME_INVALID);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.ROLE_NAME_INVALID}", SOURCE);
                return objResponse;
            }
            lstRoles = await RolesDbManager.GetAllRole();
            objRole = lstRoles.Find(obj => obj.RoleName.EqualsIgnoreCase(x_objRequest.RoleName) == true);
            if (objRole != null)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.ROLE_NAME_EXIST, ErrorCode.ROLE_NAME_EXIST);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.ROLE_NAME_EXIST}", SOURCE);
                return objResponse;
            }
            if (x_objRequest.Permissions == null || x_objRequest.Permissions.Count == 0)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.PERMISSION_INVALID, ErrorCode.PERMISSION_INVALID);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.PERMISSION_INVALID}", SOURCE);
                return objResponse;
            }

            // Account
            objAccount = await GetAccount(x_strAccountId);
            if (objAccount == null)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.ACCOUNT_IS_LOCKER, ErrorCode.ACCOUNT_IS_LOCKER);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.ACCOUNT_IS_LOCKER}", SOURCE);

                return objResponse;
            }

            // Member
            objMemberInfo = MemberInfoManager.GetMemberById(objAccount.MemberId);
            if (objMemberInfo == null)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.MEMBER_INFO_IS_NULL, ErrorCode.MEMBER_INFO_IS_NULL);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.MEMBER_INFO_IS_NULL}", SOURCE);

                return objResponse;
            }

            // Role
            objRole = await RolesDbManager.GetRoleById(objAccount.RoleId);
            if ((objRole == null) || (objRole.Permissions.Contains((int)DTUSVCPermission.CREATE_ROLE) == false))
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.AUTHORITY_INSUFFICIENT, ErrorCode.AUTHORITY_INSUFFICIENT);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMemberInfo.GetMemberFullName(), $"{SOURCE}: {ErrorConstant.MEMBER_INFO_IS_NULL}", SOURCE);
                return objResponse;
            }

            lstPermission = new List<int>();
            foreach (int nPermission in x_objRequest.Permissions)
            {
                ePermission = nPermission.ToEnum(DTUSVCPermission.NONE);
                if (ePermission == DTUSVCPermission.NONE ||
                    ePermission == DTUSVCPermission.GET_PROXY ||
                    ePermission == DTUSVCPermission.DELETE_PROXY ||
                    ePermission == DTUSVCPermission.UPDATE_PROXY)
                {
                    continue;
                }

                lstPermission.Add(nPermission);
            }
            if (lstPermission.Count == 0)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.PERMISSION_INVALID, ErrorCode.PERMISSION_INVALID);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.PERMISSION_INVALID}", SOURCE);
                return objResponse;
            }

            objRole = new Roles();
            objRole.CreaterId = objAccount.MemberId;
            objRole.RoleName = x_objRequest.RoleName;
            objRole.Permissions = lstPermission;
            objRole.Permissions = lstPermission;
            bResult = await RolesDbManager.UpdateRole(objRole);
            objResponse = ResponseManager.GetSuccsessResp(SuccsessConstant.UPDATE_ROLE_SUCCSESS);
            return objResponse;
        }

        /// <summary>
        /// Get All Permissions
        /// </summary>
        /// <param name="x_strAccountId"></param>
        /// <param name="x_objContext"></param>
        /// <returns></returns>
        public async Task<ResponseModel> GetAllPermissions(string x_strAccountId, HttpContext x_objContext)
        {
            Accounts objAccount;
            MemberDecryptModel objMemberInfo;
            Roles objRole;
            ResponseModel objResponse;
            PermissionsResponseModel objPermissionsResponseModel;
            PermissionsGroup objPermissionsGroup;
            List<PermissionsGroup> lstPermissionsGroup;
            List<DTUSVCPermission> lstPermission;

            int nPermissionsGroupId;
            string strClientIp;
            string strPermissionsText;

            //----------------------------------
            // Get IP Address
            //----------------------------------
            strClientIp = x_objContext.GetClientIPAddress();
            //----------------------------------
            // Validate request role
            //----------------------------------
            // Account
            objAccount = await GetAccount(x_strAccountId);
            if (objAccount == null)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.ACCOUNT_IS_LOCKER, ErrorCode.ACCOUNT_IS_LOCKER);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.ACCOUNT_IS_LOCKER}", SOURCE);

                return objResponse;
            }

            // Member
            objMemberInfo = MemberInfoManager.GetMemberById(objAccount.MemberId);
            if (objMemberInfo == null)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.MEMBER_INFO_IS_NULL, ErrorCode.MEMBER_INFO_IS_NULL);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.MEMBER_INFO_IS_NULL}", SOURCE);

                return objResponse;
            }

            // Role
            objRole = await RolesDbManager.GetRoleById(objAccount.RoleId);
            if ((objRole == null) || (objRole.Permissions.Contains((int)DTUSVCPermission.GET_ALL_PERMISSION) == false))
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.AUTHORITY_INSUFFICIENT, ErrorCode.AUTHORITY_INSUFFICIENT);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMemberInfo.GetMemberFullName(), $"{SOURCE}: {ErrorConstant.MEMBER_INFO_IS_NULL}", SOURCE);
                return objResponse;
            }

            lstPermission = Enum.GetValues(typeof(DTUSVCPermission)).Cast<DTUSVCPermission>().ToList();
            if (lstPermission == null || lstPermission.Count == 0)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.GET_ALL_PERMISSION_FAILED, ErrorCode.GET_ALL_PERMISSION_FAILED);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMemberInfo.GetMemberFullName(), $"{SOURCE}: {ErrorConstant.GET_ALL_PERMISSION_FAILED}", SOURCE);
                return objResponse;
            }

            lstPermissionsGroup = new List<PermissionsGroup>();
            foreach (DTUSVCPermission ePermission in lstPermission)
            {
                if (ePermission == DTUSVCPermission.NONE ||
                    ePermission == DTUSVCPermission.GET_PROXY ||
                    ePermission == DTUSVCPermission.DELETE_PROXY ||
                    ePermission == DTUSVCPermission.UPDATE_PROXY)
                {
                    continue;
                }
                nPermissionsGroupId = (int)ePermission / 1000;
                objPermissionsGroup = lstPermissionsGroup.Find(obj => obj.Id == nPermissionsGroupId);
                if (objPermissionsGroup == null)
                {
                    objPermissionsGroup = new PermissionsGroup();
                    objPermissionsGroup.Id = nPermissionsGroupId;
                    objPermissionsGroup.Name = GetPermissionGroupName(nPermissionsGroupId);
                    lstPermissionsGroup.Add(objPermissionsGroup);
                }
                strPermissionsText = ePermission.DisplayName();
                objPermissionsResponseModel = new PermissionsResponseModel();
                objPermissionsResponseModel.PermissionsName = strPermissionsText;
                objPermissionsResponseModel.PermissionsId = (int)ePermission;
                objPermissionsGroup.Permissions.Add(objPermissionsResponseModel);
            }
            objResponse = ResponseManager.GetSuccsessResp(string.Empty, lstPermissionsGroup, lstPermissionsGroup.Count);
            return objResponse;
        }

        /// <summary>
        /// Get All Role
        /// </summary>
        /// <param name="x_strAccountId"></param>
        /// <param name="x_objContext"></param>
        /// <returns></returns>
        public async Task<ResponseModel> GetAllRole(string x_strAccountId, HttpContext x_objContext)
        {
            Accounts objAccount;
            MemberDecryptModel objMemberInfo;
            Roles objRole;
            ResponseModel objResponse;
            PermissionsResponseModel objPermissionsResponseModel;
            PermissionsGroup objPermissionsGroup;
            List<PermissionsGroup> lstPermissionsGroup;
            DTUSVCPermission ePermission;
            List<Roles> lstRoles;
            List<RoleResponseModel> lstRoleResponseModel;
            RoleResponseModel objRoleResponseModel;

            int nPermissionsGroupId;
            string strClientIp;
            string strPermissionsText;

            //----------------------------------
            // Get IP Address
            //----------------------------------
            strClientIp = x_objContext.GetClientIPAddress();
            //----------------------------------
            // Validate request role
            //----------------------------------
            // Account
            objAccount = await GetAccount(x_strAccountId);
            if (objAccount == null)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.ACCOUNT_IS_LOCKER, ErrorCode.ACCOUNT_IS_LOCKER);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.ACCOUNT_IS_LOCKER}", SOURCE);

                return objResponse;
            }

            // Member
            objMemberInfo = MemberInfoManager.GetMemberById(objAccount.MemberId);
            if (objMemberInfo == null)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.MEMBER_INFO_IS_NULL, ErrorCode.MEMBER_INFO_IS_NULL);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.MEMBER_INFO_IS_NULL}", SOURCE);

                return objResponse;
            }

            // Role
            objRole = await RolesDbManager.GetRoleById(objAccount.RoleId);
            if ((objRole == null) || (objRole.Permissions.Contains((int)DTUSVCPermission.GET_ALL_ROLE) == false))
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.AUTHORITY_INSUFFICIENT, ErrorCode.AUTHORITY_INSUFFICIENT);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMemberInfo.GetMemberFullName(), $"{SOURCE}: {ErrorConstant.MEMBER_INFO_IS_NULL}", SOURCE);
                return objResponse;
            }

            lstRoles = await RolesDbManager.GetAllRole();
            lstRoleResponseModel = new List<RoleResponseModel>();
            foreach (Roles objRoleTemp in lstRoles)
            {
                objRoleResponseModel = new RoleResponseModel
                {
                    Id = objRoleTemp.Id,
                    RoleName = objRoleTemp.RoleName
                };
                lstRoleResponseModel.Add(objRoleResponseModel);
                if (objRoleTemp.Permissions == null)
                {
                    continue;
                }

                lstPermissionsGroup = new List<PermissionsGroup>();
                objRoleResponseModel.Permissions = lstPermissionsGroup;
                foreach (int nPermissionsId in objRoleTemp.Permissions)
                {
                    ePermission = nPermissionsId.ToEnum(DTUSVCPermission.NONE);
                    if (ePermission == DTUSVCPermission.NONE ||
                    ePermission == DTUSVCPermission.GET_PROXY ||
                    ePermission == DTUSVCPermission.DELETE_PROXY ||
                    ePermission == DTUSVCPermission.UPDATE_PROXY)
                    {
                        continue;
                    }
                    nPermissionsGroupId = nPermissionsId / 1000;
                    objPermissionsGroup = lstPermissionsGroup.Find(obj => obj.Id == nPermissionsGroupId);
                    if (objPermissionsGroup == null)
                    {
                        objPermissionsGroup = new PermissionsGroup();
                        objPermissionsGroup.Id = nPermissionsGroupId;
                        objPermissionsGroup.Name = GetPermissionGroupName(nPermissionsGroupId);
                        lstPermissionsGroup.Add(objPermissionsGroup);
                    }
                    strPermissionsText = ePermission.DisplayName();
                    objPermissionsResponseModel = new PermissionsResponseModel();
                    objPermissionsResponseModel.PermissionsName = strPermissionsText;
                    objPermissionsResponseModel.PermissionsId = (int)ePermission;
                    objPermissionsGroup.Permissions.Add(objPermissionsResponseModel);
                }
            }

            objResponse = ResponseManager.GetSuccsessResp(string.Empty, lstRoleResponseModel, lstRoleResponseModel.Count);
            return objResponse;
        }

        /// <summary>
        /// Get Permission Group Name
        /// </summary>
        /// <param name="x_nGroupId"></param>
        /// <returns></returns>
        private string GetPermissionGroupName(int x_nGroupId)
        {
            string strName;

            strName = string.Empty;
            switch (x_nGroupId)
            {
                case 1:
                    strName = "Quản lý tài khoản";
                    break;
                case 2:
                    strName = "Quản lý thành viên";
                    break;
                case 3:
                    strName = "Quản lý phân quyền";
                    break;
                case 4:
                    strName = "Quản lý MyDTU";
                    break;
                case 5:
                    strName = "Quản lý hoạt động";
                    break;
                case 6:
                    strName = "Quản lý tin tức";
                    break;
                case 7:
                    strName = "Quản lý khoa/viện";
                    break;
            }
            return strName;
        }

        /// <summary>
        /// Update Role
        /// </summary>
        /// <param name="x_objRequest"></param>
        /// <param name="x_strAccountId"></param>
        /// <param name="x_objContext"></param>
        /// <returns></returns>
        public async Task<ResponseModel> UpdateRole(UpdateRoleRequestModel x_objRequest, string x_strAccountId, HttpContext x_objContext)
        {
            Accounts objAccount;
            MemberDecryptModel objMemberInfo;
            Roles objRole;
            ResponseModel objResponse;
            DTUSVCPermission ePermission;
            List<Roles> lstRoles;
            List<int> lstPermission;
            bool bResult;
            string strClientIp;

            //----------------------------------
            // Get IP Address
            //----------------------------------
            strClientIp = x_objContext.GetClientIPAddress();
            //----------------------------------
            // Validate request role
            //----------------------------------
            if (string.IsNullOrWhiteSpace(x_objRequest.RoleName) == true)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.ROLE_NAME_INVALID, ErrorCode.ROLE_NAME_INVALID);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.ROLE_NAME_INVALID}", SOURCE);
                return objResponse;
            }
            lstRoles = await RolesDbManager.GetAllRole();
            objRole = lstRoles.Find(obj => obj.RoleName.EqualsIgnoreCase(x_objRequest.RoleName) == true);
            if (objRole != null)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.ROLE_NAME_EXIST, ErrorCode.ROLE_NAME_EXIST);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.ROLE_NAME_EXIST}", SOURCE);
                return objResponse;
            }
            if (x_objRequest.Permissions == null || x_objRequest.Permissions.Count == 0)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.PERMISSION_INVALID, ErrorCode.PERMISSION_INVALID);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.PERMISSION_INVALID}", SOURCE);
                return objResponse;
            }

            // Account
            objAccount = await GetAccount(x_strAccountId);
            if (objAccount == null)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.ACCOUNT_IS_LOCKER, ErrorCode.ACCOUNT_IS_LOCKER);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.ACCOUNT_IS_LOCKER}", SOURCE);

                return objResponse;
            }

            // Member
            objMemberInfo = MemberInfoManager.GetMemberById(objAccount.MemberId);
            if (objMemberInfo == null)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.MEMBER_INFO_IS_NULL, ErrorCode.MEMBER_INFO_IS_NULL);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.MEMBER_INFO_IS_NULL}", SOURCE);

                return objResponse;
            }

            // Role
            objRole = await RolesDbManager.GetRoleById(objAccount.RoleId);
            if ((objRole == null) || (objRole.Permissions.Contains((int)DTUSVCPermission.UPDATE_ROLE) == false))
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.AUTHORITY_INSUFFICIENT, ErrorCode.AUTHORITY_INSUFFICIENT);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMemberInfo.GetMemberFullName(), $"{SOURCE}: {ErrorConstant.MEMBER_INFO_IS_NULL}", SOURCE);
                return objResponse;
            }

            lstPermission = new List<int>();
            foreach (int nPermission in x_objRequest.Permissions)
            {
                ePermission = nPermission.ToEnum(DTUSVCPermission.NONE);
                if (ePermission == DTUSVCPermission.NONE)
                {
                    continue;
                }

                lstPermission.Add(nPermission);
            }
            if (lstPermission.Count == 0)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.PERMISSION_INVALID, ErrorCode.PERMISSION_INVALID);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMemberInfo.GetMemberFullName(), $"{SOURCE}: {ErrorConstant.PERMISSION_INVALID}", SOURCE);
                return objResponse;
            }

            objRole = await RolesDbManager.GetRoleById(x_objRequest.Id);
            if (objRole == null)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.ROLE_IS_NULL, ErrorCode.ROLE_IS_NULL);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMemberInfo.GetMemberFullName(), $"{SOURCE}: {ErrorConstant.ROLE_IS_NULL}", SOURCE);
                return objResponse;
            }
            if (objRole.RoleName.EqualsIgnoreCase("System Admin") == true)
            {
                // Get error
                objResponse = ResponseManager.GetErrorResp(ErrorConstant.AUTHORITY_INSUFFICIENT, ErrorCode.AUTHORITY_INSUFFICIENT);
                // Write log
                LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, objMemberInfo.GetMemberFullName(), $"{SOURCE}: {ErrorConstant.MEMBER_INFO_IS_NULL}", SOURCE);
                return objResponse;
            }
            objRole.CreaterId = objAccount.MemberId;
            objRole.RoleName = x_objRequest.RoleName;
            objRole.Permissions = lstPermission;
            objRole.Permissions = lstPermission;
            bResult = await RolesDbManager.UpdateRole(objRole);
            objResponse = ResponseManager.GetSuccsessResp(SuccsessConstant.UPDATE_ROLE_SUCCSESS);
            return objResponse;
        }
    }
}
