﻿using DTUSVCv3_Library;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTUSVCv3_Services
{
    public interface IRoleService
    {
        /// <summary>
        /// Get All Role
        /// </summary>
        /// <returns></returns>
        Task<ResponseModel> GetAllRole(string x_strAccountId, HttpContext x_objContext);
        /// <summary>
        /// Get All Permissions
        /// </summary>
        /// <returns></returns>
        Task<ResponseModel> GetAllPermissions(string x_strAccountId, HttpContext x_objContext);
        /// <summary>
        /// Create Role
        /// </summary>
        /// <param name="x_objRequest"></param>
        /// <param name="x_strAccountId"></param>
        /// <param name="x_objContext"></param>
        /// <returns></returns>
        Task<ResponseModel> CreateRole(CreateRoleRequestModel x_objRequest, string x_strAccountId, HttpContext x_objContext);
        /// <summary>
        /// Update Role
        /// </summary>
        /// <param name="x_objRequest"></param>
        /// <param name="x_strAccountId"></param>
        /// <param name="x_objContext"></param>
        /// <returns></returns>
        Task<ResponseModel> UpdateRole(UpdateRoleRequestModel x_objRequest, string x_strAccountId, HttpContext x_objContext);
    }
}
