﻿using DTUSVCv3_EntityFramework;
using DTUSVCv3_EntityFramework.DataBaseManager;
using DTUSVCv3_Library;
using DTUSVCv3_MemoryData;
using Microsoft.AspNetCore.Http;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace DTUSVCv3_Services
{
    public abstract class BaseSerivce
    {
        #region Constructor
        public BaseSerivce()
        {
            m_objMemberInfoMemory = MemberInfoMemory.Instant;
            m_objMemberRegisterMemory = MemberRegisterMemory.Instant;
            m_objAccountDbManager = new AccountDbManager();
            m_objAddressDbManager = new AddressDbManager();
            m_objFacultyDbManager = new FacultyDbManager();
            m_objRolesDbManager = new RolesDbManager();
            m_objActivityManager = new ActivityManager();
            m_objRecruitmentDbManager = new RecruitmentDbManager();
            m_objNewspaperDbManager = new NewspaperDbManager();
        }
        #endregion

        #region Fields
        private MemberInfoMemory m_objMemberInfoMemory;
        private MemberRegisterMemory m_objMemberRegisterMemory;
        private AccountDbManager m_objAccountDbManager;
        private AddressDbManager m_objAddressDbManager;
        private FacultyDbManager m_objFacultyDbManager;
        private RolesDbManager m_objRolesDbManager;
        private ActivityManager m_objActivityManager;
        private RecruitmentDbManager m_objRecruitmentDbManager;
        private NewspaperDbManager m_objNewspaperDbManager;
        #endregion

        #region Properties
        protected MemberInfoMemory MemberInfoManager
        {
            get
            {
                return m_objMemberInfoMemory;
            }
        }

        protected AccountDbManager AccountDbManager
        {
            get
            {
                return m_objAccountDbManager;
            }
        }
        protected AddressDbManager AddressDbManager
        {
            get
            {
                return m_objAddressDbManager;
            }
        }
        protected FacultyDbManager FacultyDbManager
        {
            get
            {
                return m_objFacultyDbManager;
            }
        }
        protected RolesDbManager RolesDbManager
        {
            get
            {
                return m_objRolesDbManager;
            }
        }

        protected RecruitmentDbManager RecruitmentDbManager
        {
            get
            {
                return m_objRecruitmentDbManager;
            }
        }

        protected MemberRegisterMemory MemberRegisterMemory
        {
            get
            {
                return m_objMemberRegisterMemory;
            }
        }

        protected ActivityManager ActivityDbManager
        {
            get
            {
                return m_objActivityManager;
            }
        }

        protected NewspaperDbManager NewsDbManager
        {
            get
            {
                return m_objNewspaperDbManager;
            }
        }
        #endregion

        #region Method
        /// <summary>
        /// Check Permission
        /// </summary>
        protected async Task<bool> CheckPermission(string x_strAccountId, DTUSVCPermission x_ePermission)
        {
            Accounts objAccounts;
            Roles objRole;
            try
            {
                if (x_strAccountId.MongoIdIsValid() == false)
                {
                    return false;
                }

                objAccounts = await m_objAccountDbManager.GetAccountById(x_strAccountId);
                if (objAccounts == null)
                {
                    return false;
                }

                objRole = await m_objRolesDbManager.GetRoleById(objAccounts.RoleId);
                if (objRole == null)
                {
                    return false;
                }
                if (objRole.Permissions.Contains((int)x_ePermission) == false)
                {
                    return false;
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Try Parse DateTime
        /// </summary>
        /// <param name="x_strDateTime"></param>
        /// <param name="x_objDateTime"></param>
        /// <returns></returns>
        protected bool TryParseDateTime(string x_strDateTime, out DateTime x_objDateTime)
        {
            string strDateTime;

            x_objDateTime = DateTimeHelper.GetCurrentDateTimeVN();
            if (x_strDateTime.CheckDateTimeFormat(out strDateTime) == false)
            {
                return false;
            }

            x_objDateTime = DateTimeHelper.ConvertStrToDateTime(x_strDateTime);
            return true;
        }

        /// <summary>
        /// Try Get Permistion By Member Id
        /// </summary>
        /// <param name="x_strMemberId"></param>
        /// <param name="x_lstPermissions"></param>
        /// <returns></returns>
        protected bool TryGetPermistionByMemberId(string x_strMemberId, out List<int> x_lstPermissions)
        {
            Accounts objAccount;
            Roles objRole;
            Task objTask;
            List<int> lstPermissions;
            bool bSuccsess;

            bSuccsess = false;
            x_lstPermissions = null;
            objTask = Task.Run(async () =>
            {
                // Get Account
                objAccount = await m_objAccountDbManager.GetAccountByMemberId(x_strMemberId);
                if ((objAccount == null) || (objAccount.IsLocked == true))
                {
                    return;
                }

                // Get Role
                objRole = await m_objRolesDbManager.GetRoleById(objAccount.RoleId);
                if (objRole == null)
                {
                    return;
                }

                lstPermissions = objRole.Permissions;
                bSuccsess = true;
            });
            objTask.Wait();
            return bSuccsess;
        }

        /// <summary>
        /// Check Address
        /// </summary>
        /// <param name="x_strProvinceId"></param>
        /// <param name="x_strDistrictId"></param>
        /// <param name="x_strWardId"></param>
        /// <param name="x_strSpecificAddress"></param>
        /// <param name="x_objAddress"></param>
        /// <returns></returns>
        protected ResponseModel CheckAddress(string x_strProvinceId, string x_strDistrictId, string x_strWardId, string x_strSpecificAddress,
                                                        out AddressInfo x_objAddress, out Provinces x_objProvince, out Districts x_objDistrict, out Wards x_objWard)
        {
            Task objTask;
            ResponseModel objResponse;
            Provinces objProvince;
            Districts objDistrict;
            Wards objWard;
            AddressInfo objAddress;

            objAddress = null;
            objProvince = null;
            objDistrict = null;
            objWard = null;
            objResponse = new ResponseModel();
            objTask = Task.Run(async () =>
            {
                // Ward
                objWard = await m_objAddressDbManager.GetWardById(x_strWardId);
                if (objWard == null)
                {
                    objResponse = ResponseManager.GetErrorResp(ErrorConstant.WARD_IS_NULL, ErrorCode.WARD_IS_NULL);
                    return;
                }
                // District
                objDistrict = await m_objAddressDbManager.GetDistrictById(x_strDistrictId);
                if ((objDistrict == null) || (objDistrict.DistrictId.Equals(objWard.DistrictId) == false))
                {
                    objResponse = ResponseManager.GetErrorResp(ErrorConstant.DISTRICT_IS_NULL, ErrorCode.DISTRICT_IS_NULL);
                    return;
                }
                // Province
                objProvince = await m_objAddressDbManager.GetProvinceById(x_strProvinceId);
                if ((objProvince == null) || (objProvince.ProvinceId.Equals(objDistrict.ProvinceId) == false))
                {
                    objResponse = ResponseManager.GetErrorResp(ErrorConstant.PROVINCE_IS_NULL, ErrorCode.PROVINCE_IS_NULL);
                    return;
                }
                // Specific Address
                if (string.IsNullOrWhiteSpace(x_strSpecificAddress) == true)
                {
                    objResponse = ResponseManager.GetErrorResp(ErrorConstant.SPECISIC_ADDRESS_IS_NULL, ErrorCode.SPECISIC_ADDRESS_IS_NULL);
                    return;
                }

                objAddress = new AddressInfo
                {
                    DistrictId = objDistrict.DistrictId,
                    ProvinceId = objProvince.ProvinceId,
                    SpecificAddress = x_strSpecificAddress,
                    WardId = objWard.WardId,
                    Id = string.Empty
                };
            });
            objTask.Wait();
            x_objAddress = objAddress;
            x_objProvince = objProvince;
            x_objDistrict = objDistrict;
            x_objWard = objWard;
            return objResponse;
        }

        /// <summary>
        /// Get Account
        /// </summary>
        protected async Task<Accounts> GetAccount(string x_strAccountIdOrUserName, bool x_bIsMemberId = false)
        {
            Accounts objAccounts;
            try
            {
                if (x_strAccountIdOrUserName.MongoIdIsValid() == false)
                {
                    objAccounts = await m_objAccountDbManager.GetAccountByUserName(x_strAccountIdOrUserName);
                }
                else
                {
                    if (x_bIsMemberId == true)
                    {
                        objAccounts = await m_objAccountDbManager.GetAccountByMemberId(x_strAccountIdOrUserName);
                    }
                    else
                    {
                        objAccounts = await m_objAccountDbManager.GetAccountById(x_strAccountIdOrUserName);
                    }
                }

                if (objAccounts.IsLocked == true)
                {
                    return null;
                }

                return objAccounts;
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Check Permission
        /// </summary>
        protected async Task<Roles> CheckRole(string x_strAccountId)
        {
            Accounts objAccounts;
            Roles objRole;
            try
            {
                if (x_strAccountId.MongoIdIsValid() == false)
                {
                    return null;
                }

                objAccounts = await m_objAccountDbManager.GetAccountById(x_strAccountId);
                if (objAccounts == null)
                {
                    return null;
                }

                objRole = await m_objRolesDbManager.GetRoleById(objAccounts.RoleId);

                return objRole;
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Check Faculty Id
        /// </summary>
        /// <param name="x_strFacultyId"></param>
        /// <returns></returns>
        protected async Task<bool> CheckFacultyId(string x_strFacultyId)
        {
            Facultys objFaculty;

            if (x_strFacultyId.MongoIdIsValid() == false)
            {
                return false;
            }

            try
            {
                objFaculty = await m_objFacultyDbManager.GetFacultyById(x_strFacultyId);
                if (objFaculty == null)
                {
                    return false;
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Create Token
        /// </summary>
        protected string CreateToken(string x_strAccountId, string x_strDTUId, string x_strRoleId, int x_nTimeOut = 24)
        {
            string strToken;
            byte[] arrHashKey;
            JwtSecurityTokenHandler objTokenHandler;
            SecurityTokenDescriptor objTokenDescriptor;
            SecurityToken objSecurityToken;

            // Create token
            objTokenHandler = new JwtSecurityTokenHandler();
            arrHashKey = Encoding.ASCII.GetBytes(SecurityConstant.CRYPTOKEY);
            objTokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                        new Claim(ClaimTypes.Name, x_strAccountId),
                        new Claim(ClaimTypes.Role, x_strRoleId),
                        new Claim(ClaimTypes.NameIdentifier, x_strDTUId)
                }),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(arrHashKey), SecurityAlgorithms.HmacSha256Signature)
            };
            // Set token die time
            if (x_nTimeOut > 0)
            {
                objTokenDescriptor.Expires = DateTime.Now.AddHours(x_nTimeOut);
            }
            objSecurityToken = objTokenHandler.CreateToken(objTokenDescriptor);
            strToken = objTokenHandler.WriteToken(objSecurityToken);
            return strToken;
        }

        /// <summary>
        /// Logout
        /// </summary>
        protected bool LogoutBase(HttpContext x_objContext)
        {
            string strToken;

            if (x_objContext == null)
            {
                return false;
            }

            strToken = x_objContext.GetTokenByHttpContext();
            if (string.IsNullOrWhiteSpace(strToken) == true)
            {
                return false;
            }

            LoginData.Logout(strToken);
            // TODO:
            // Logout to socket
            return true;
        }

        /// <summary>
        /// Get Member Score
        /// </summary>
        /// <param name="x_strMemberId"></param>
        /// <returns></returns>
        protected async Task<double> GetMemberScore(string x_strMemberId)
        {
            double dSum;
            List<ActivityChecklist> lstCheckList;

            try
            {
                lstCheckList = await m_objActivityManager.GetCheckListByMemberId(x_strMemberId);
                if (lstCheckList == null)
                {
                    return 0;
                }

                dSum = 0;
                foreach (ActivityChecklist objTemp in lstCheckList)
                {
                    if ((objTemp == null) || (objTemp.TimeToLeave <= objTemp.EntryTime) || (objTemp.TimeToLeave < SettingHelper.ScoringStartDate))
                    {
                        continue;
                    }

                    dSum += objTemp.CalScore();
                }

                return dSum;
            }
            catch
            {
                return 0;
            }
        }

        /// <summary>
        /// Address Info Db To Model
        /// </summary>
        protected async Task<AddressInfoModel> GetAddressModel(string x_strAddressId)
        {
            AddressInfoModel objAddressInfoModel;
            AddressInfo objAddressInfo;

            try
            {
                objAddressInfo = await m_objAddressDbManager.GetAddressInfoById(x_strAddressId);
                if (objAddressInfo == null)
                {
                    return null;
                }

                objAddressInfoModel = new AddressInfoModel();
                objAddressInfoModel.Id = objAddressInfo.Id;
                objAddressInfoModel.Province = await m_objAddressDbManager.GetProvinceById(objAddressInfo.ProvinceId);
                objAddressInfoModel.District = await m_objAddressDbManager.GetDistrictById(objAddressInfo.DistrictId);
                objAddressInfoModel.Ward = await m_objAddressDbManager.GetWardById(objAddressInfo.WardId);
                objAddressInfoModel.SpecificAddress = objAddressInfo.SpecificAddress;
                return objAddressInfoModel;
            }
            catch
            {
                return null;
            }
        }
        #endregion
    }
}
