﻿using DTUSVCv3_Library;
using DTUSVCv3_MemoryData;
using Quartz;

namespace DTUSVCv3_Services.QuartzJobs
{
    public class QuartzTokenRemove : IJob
    {
        /// <summary>
        /// Execute
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public Task Execute(IJobExecutionContext context)
        {
            List<LoginInfoModel> lstLoginToken;
            DateTime objCurrentDateTime;

            try
            {
                lstLoginToken = LoginData.GetListLogin();
                if (lstLoginToken == null)
                {
                    return Task.CompletedTask;
                }

                objCurrentDateTime = DateTimeHelper.GetCurrentDateTimeVN();
                // Refresh token expiration date is 7 days
                objCurrentDateTime.AddDays(-7);
                lstLoginToken.RemoveAll(obj => obj.LoginDate > objCurrentDateTime);

            }
            catch
            {
                // No need process
            }
            return Task.CompletedTask;
        }
    }
}
