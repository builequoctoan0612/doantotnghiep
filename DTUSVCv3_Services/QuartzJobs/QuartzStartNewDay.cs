﻿using DTUSVCv3_EntityFramework;
using DTUSVCv3_Library;
using DTUSVCv3_Library.LibraryModel;
using DTUSVCv3_MemoryData;
using MongoDB.Driver;
using Quartz;

namespace DTUSVCv3_Services.QuartzJobs
{
    public class QuartzStartNewDay : IJob
    {
        /// <summary>
        /// Execute
        /// </summary>
        /// <param name="x_objContext"></param>
        /// <returns></returns>
        public Task Execute(IJobExecutionContext x_objContext)
        {

            SystemSetting.IS_SYSTEM_MAINTENANCE = true;
            //----------------------------------------------------------
            // Send email happy birth day
            //----------------------------------------------------------
            SendHappyBirthDayEmail();
            //----------------------------------------------------------
            // Backup Data
            //----------------------------------------------------------
            BackupData();
            //----------------------------------------------------------
            // Delete download file
            //----------------------------------------------------------
            DeleteDownloadFile();
            SystemSetting.IS_SYSTEM_MAINTENANCE = false;
            return Task.CompletedTask;
        }

        /// <summary>
        /// Delete Download File
        /// </summary>
        private void DeleteDownloadFile()
        {
            const string EXPORT = "Export";
            string strFolderPath;
            double dTime;
            string[] arrFilePath;
            List<string> lstDeleteFile;
            FileInfo objFileInfo;
            DateTime objCurrentDate;
            DateTime objCreateDate;

            try
            {
                strFolderPath = FileHelper.GetFolderFullPath(EXPORT);
                if (Directory.Exists(strFolderPath) == false)
                {
                    return;
                }

                arrFilePath = Directory.GetFiles(strFolderPath, "*", SearchOption.AllDirectories);
                if ((arrFilePath == null) || (arrFilePath.Length == 0))
                {
                    return;
                }

                objCurrentDate = DateTimeHelper.GetCurrentDateTimeVN();
                lstDeleteFile = new List<string>();
                foreach (string strFilePath in arrFilePath)
                {
                    objFileInfo = new FileInfo(strFilePath);
                    objCreateDate = TimeZoneInfo.ConvertTimeFromUtc(objFileInfo.CreationTimeUtc, DateTimeHelper.GetVNTimeZone());
                    dTime = objCurrentDate.Subtract(objCreateDate).TotalHours;
                    if (dTime >= 24)
                    {
                        lstDeleteFile.Add(strFilePath);
                    }
                }

                foreach (string strFilePath in lstDeleteFile)
                {
                    if(File.Exists(strFilePath) == true)
                    {
                        File.Delete(strFilePath);
                    }
                }
            }
            catch
            {
                // No need process
            }
        }

        /// <summary>
        /// Send Happy Birth Day Email
        /// </summary>
        private void SendHappyBirthDayEmail()
        {
            const string DATE_TIME_FORMAT = "dd/MM";
            List<MemberDecryptModel> lstMember;
            SendEmailModel objSendEmailModel;
            DateTime objCurrentDateTime;
            string strCurrentDateTime;
            string strFilePath;
            string strEmailTemp;
            string strEmailContent;
            string strBuf;
            int nYear;
            int nOld;

            objCurrentDateTime = DateTimeHelper.GetCurrentDateTimeVN();
            lstMember = MemberInfoMemory.Instant.GetAllMemberList(GetMemberType.IS_MEMBER);
            if (lstMember == null || lstMember.Count == 0)
            {
                return;
            }

            strCurrentDateTime = objCurrentDateTime.ToString(DATE_TIME_FORMAT);
            // Read email temp
            strFilePath = FileHelper.GetFilePath(SysFolder.EMAIL_TEMP_FOLDER, SysFile.HAPPY_BIRTH_DAY);
            strEmailTemp = FileHelper.ReadTextFile(strFilePath);
            foreach (MemberDecryptModel objMember in lstMember)
            {
                if (objMember.BirthDay.StartsWith(strCurrentDateTime) == false)
                {
                    continue;
                }

                strBuf = objMember.BirthDay.Substring(objMember.BirthDay.Length - 4);
                int.TryParse(strBuf, out nYear);
                if (nYear < 1980)
                {
                    continue;
                }
                nOld = objCurrentDateTime.Year - nYear;

                // Send email
                strEmailContent = strEmailTemp.Replace(EmailReplaceText.MEMBER_FULL_NAME, objMember.GetMemberFullName())
                                              .Replace(EmailReplaceText.STUDENT_ID, objMember.StudentId)
                                              .Replace(EmailReplaceText.MEMBER_NAME, objMember.LastName)
                                              .Replace(EmailReplaceText.OLD, nOld.ToString());
                objSendEmailModel = new SendEmailModel
                {
                    EmailBody = strEmailContent,
                    EmailRecipient = objMember.Email,
                    Subject = EmailSubject.HAPPY_BIRTH_DAY,
                    EmailType = EmailType.HAPPY_BIRTHDAY
                };

                EmailHelper.Instant.EmailNomalQueue.Enqueue(objSendEmailModel);
            }
        }

        /// <summary>
        /// Backup Data
        /// </summary>
        private void BackupData()
        {
            const string NAME_FORMAT = "Backup_{0}";
            const string FOLDER_TIME_FORMAT = "dd_MM_yyyy";
            const string FILE_TIME_FORMAT = "dd_MM_yyyy_HH_mm_ss";
            const string BACKUP_FILE_PATH_FORMAT = "Backup_{0}.zip";
            const string BACKUP_STATIC_FILE = "Static_File.zip";
            const string BACKUP_PRIVATE_FILE_FORMAT = "Backup_{0}.zip";
            const string LOG_FOLDER = "DTUSVCv3_Log";
            const string EMAIL_TEMP = "EmailTemp";
            const string HASH_KEY = "HashKey";
            const string WEB_SETTING = "WebSetting";
            Task objTask;
            UploadedModel objUploadedFolder;
            DateTime objCurrentDate;
            string strFolderPath;
            string strFilePath;
            string strFileName;
            string strGGFolderName;
            string strGGFolderId;
            bool bResult;

            objTask = Task.Run(async () =>
            {
                try
                {
                    //----------------------------------------------------------
                    // Collect Backup Data and save to folder
                    //----------------------------------------------------------
                    // Collect database
                    await CollectBackupDataBase();
                    // Collect static file
                    strFilePath = FileHelper.GetFilePath(SysFolder.BACKUP_WWWROOT_FOLDER_PATH, BACKUP_STATIC_FILE, true);
                    FileHelper.CreateZipFile(SysFolder.WWWROOT_PATH, strFilePath);
                    // Collect private file
                    // Log folder
                    strFolderPath = FileHelper.GetFolderFullPath(LOG_FOLDER, true);
                    strFileName = string.Format(BACKUP_PRIVATE_FILE_FORMAT, LOG_FOLDER);
                    strFilePath = FileHelper.GetFilePath(SysFolder.BACKUP_PRIVATE_FOLDER_PATH, strFileName, true);
                    FileHelper.CreateZipFile(strFolderPath, strFilePath);
                    // Email Temp
                    strFolderPath = FileHelper.GetFolderFullPath(EMAIL_TEMP, true);
                    strFileName = string.Format(BACKUP_PRIVATE_FILE_FORMAT, EMAIL_TEMP);
                    strFilePath = FileHelper.GetFilePath(SysFolder.BACKUP_PRIVATE_FOLDER_PATH, strFileName, true);
                    FileHelper.CreateZipFile(strFolderPath, strFilePath);
                    // Hash key
                    strFolderPath = FileHelper.GetFolderFullPath(HASH_KEY, true);
                    strFileName = string.Format(BACKUP_PRIVATE_FILE_FORMAT, HASH_KEY);
                    strFilePath = FileHelper.GetFilePath(SysFolder.BACKUP_PRIVATE_FOLDER_PATH, strFileName, true);
                    FileHelper.CreateZipFile(strFolderPath, strFilePath);
                    // WEB_SETTING
                    strFolderPath = FileHelper.GetFolderFullPath(WEB_SETTING, true);
                    strFileName = string.Format(BACKUP_PRIVATE_FILE_FORMAT, WEB_SETTING);
                    strFilePath = FileHelper.GetFilePath(SysFolder.BACKUP_PRIVATE_FOLDER_PATH, strFileName, true);
                    FileHelper.CreateZipFile(strFolderPath, strFilePath);

                    //----------------------------------------------------------
                    // Zip folder 
                    //----------------------------------------------------------
                    objCurrentDate = DateTimeHelper.GetCurrentDateTimeVN();
                    strFilePath = string.Format(BACKUP_FILE_PATH_FORMAT, objCurrentDate.ToString(FILE_TIME_FORMAT));
                    strFilePath = FileHelper.GetFilePath(SysFolder.BACKUP_ROOT_FOLDER_PATH, strFilePath, true);
                    strFolderPath = FileHelper.GetFolderFullPath(SysFolder.BACKUP_TEMP_FOLDER_PATH, true);
                    bResult = FileHelper.CreateZipFile(strFolderPath, strFilePath);
                    if (bResult == false)
                    {
                        return;
                    }
                    //----------------------------------------------------------
                    // Upload file to Google Drive
                    //----------------------------------------------------------
                    // Create folder
                    strGGFolderName = string.Format(NAME_FORMAT, objCurrentDate.ToString(FOLDER_TIME_FORMAT));
                    objUploadedFolder = await GoogleDriveHelper.Instant.CreateFolder(strGGFolderName, GGDriveRootFolderId.BACKUP_FOLDER_ID);
                    if (objUploadedFolder != null)
                    {
                        strGGFolderId = objUploadedFolder.Id;
                    }
                    else
                    {
                        strGGFolderId = GGDriveRootFolderId.BACKUP_FOLDER_ID;
                    }
                    await GoogleDriveHelper.Instant.UploadFileToGoogleDrive(strFilePath, strGGFolderId);
                    //----------------------------------------------------------
                    // Delete folder
                    //----------------------------------------------------------
                    strFolderPath = FileHelper.GetFolderFullPath(SysFolder.BACKUP_ROOT_FOLDER_PATH, true);
                    if (Directory.Exists(strFolderPath) == true)
                    {
                        Directory.Delete(strFolderPath, true);
                    }
                }
                catch
                {
                    // No need process
                }
            });
            objTask.Wait();
        }

        /// <summary>
        /// Collect Backup Data Base
        /// </summary>
        /// <returns></returns>
        private async Task CollectBackupDataBase()
        {
            const string DB_FILE_NAME_FORMAT = "{0}.json";
            string strFileName;
            string strFilePath;
            string strFileContent;
            List<Accounts> lstAccounts;
            List<Roles> lstRoles;
            List<AddressInfo> lstAddressInfo;
            List<Districts> lstDistricts;
            List<Provinces> lstProvinces;
            List<Wards> lstWards;
            List<ActivityChecklist> lstActivityChecklist;
            List<CertificateSettingText> lstCertificateSettingText;
            List<CertificateTextFont> lstCertificateTextFont;
            List<CommunityRegistEvt> lstCommunityRegistEvt;
            List<EventInfo> lstEventInfo;
            List<LeaveOfAbsence> lstLeaveOfAbsence;
            List<MemberRegistEvt> lstMemberRegistEvt;
            List<MemberSupportEvents> lstMemberSupportEvents;
            List<Facultys> lstFacultys;
            List<InterviewRooms> lstInterviewRooms;
            List<MemberInfo> lstMemberInfo;
            List<MembershipRegistration> lstMembershipRegistration;
            List<RecruitmentInfo> lstRecruitmentInfo;
            List<Newspapers> lstNewspapers;
            List<ShortLinkInfo> lstShortLinkInfo;
            List<SoftwareInfo> lstSoftwareInfo;

            #region Collection
            IMongoCollection<Accounts> objAccountColl = DbCollertionManager.GetDbCollection<Accounts>();
            IMongoCollection<Roles> objRolesColl = DbCollertionManager.GetDbCollection<Roles>();
            IMongoCollection<AddressInfo> objAddressInfoColl = DbCollertionManager.GetDbCollection<AddressInfo>();
            IMongoCollection<Districts> objDistrictsColl = DbCollertionManager.GetDbCollection<Districts>();
            IMongoCollection<Provinces> objProvincesColl = DbCollertionManager.GetDbCollection<Provinces>();
            IMongoCollection<Wards> objWardsColl = DbCollertionManager.GetDbCollection<Wards>();
            IMongoCollection<ActivityChecklist> objActivityChecklistColl = DbCollertionManager.GetDbCollection<ActivityChecklist>();
            IMongoCollection<CertificateSettingText> objCertificateSettingTextColl = DbCollertionManager.GetDbCollection<CertificateSettingText>();
            IMongoCollection<CertificateTextFont> objCertificateTextFontColl = DbCollertionManager.GetDbCollection<CertificateTextFont>();
            IMongoCollection<CommunityRegistEvt> objCommunityRegistEvtColl = DbCollertionManager.GetDbCollection<CommunityRegistEvt>();
            IMongoCollection<EventInfo> objEventInfoColl = DbCollertionManager.GetDbCollection<EventInfo>();
            IMongoCollection<LeaveOfAbsence> objLeaveOfAbsenceColl = DbCollertionManager.GetDbCollection<LeaveOfAbsence>();
            IMongoCollection<MemberRegistEvt> objMemberRegistEvtColl = DbCollertionManager.GetDbCollection<MemberRegistEvt>();
            IMongoCollection<MemberSupportEvents> objMemberSupportEventsColl = DbCollertionManager.GetDbCollection<MemberSupportEvents>();
            IMongoCollection<Facultys> objFacultysColl = DbCollertionManager.GetDbCollection<Facultys>();
            IMongoCollection<InterviewRooms> objInterviewRoomsColl = DbCollertionManager.GetDbCollection<InterviewRooms>();
            IMongoCollection<MemberInfo> objMemberInfoColl = DbCollertionManager.GetDbCollection<MemberInfo>();
            IMongoCollection<MembershipRegistration> objMembershipRegistrationColl = DbCollertionManager.GetDbCollection<MembershipRegistration>();
            IMongoCollection<RecruitmentInfo> objRecruitmentInfoColl = DbCollertionManager.GetDbCollection<RecruitmentInfo>();
            IMongoCollection<Newspapers> objNewspapersColl = DbCollertionManager.GetDbCollection<Newspapers>();
            IMongoCollection<ShortLinkInfo> objShortLinkInfoColl = DbCollertionManager.GetDbCollection<ShortLinkInfo>();
            IMongoCollection<SoftwareInfo> objSoftwareInfoColl = DbCollertionManager.GetDbCollection<SoftwareInfo>();
            #endregion

            lstAccounts = await objAccountColl.FindAsync(obj => true).Result.ToListAsync();
            strFileName = string.Format(DB_FILE_NAME_FORMAT, "Accounts");
            strFilePath = FileHelper.GetFilePath(SysFolder.BACKUP_DB_FOLDER_PATH, strFileName, true);
            strFileContent = lstAccounts.ToJSON();
            FileHelper.SaveTextFile(strFileContent, strFilePath);

            lstRoles = await objRolesColl.FindAsync(obj => true).Result.ToListAsync();
            strFileName = string.Format(DB_FILE_NAME_FORMAT, "Roles");
            strFilePath = FileHelper.GetFilePath(SysFolder.BACKUP_DB_FOLDER_PATH, strFileName, true);
            strFileContent = lstRoles.ToJSON();
            FileHelper.SaveTextFile(strFileContent, strFilePath);

            lstAddressInfo = await objAddressInfoColl.FindAsync(obj => true).Result.ToListAsync();
            strFileName = string.Format(DB_FILE_NAME_FORMAT, "AddressInfo");
            strFilePath = FileHelper.GetFilePath(SysFolder.BACKUP_DB_FOLDER_PATH, strFileName, true);
            strFileContent = lstAddressInfo.ToJSON();
            FileHelper.SaveTextFile(strFileContent, strFilePath);

            lstDistricts = await objDistrictsColl.FindAsync(obj => true).Result.ToListAsync();
            strFileName = string.Format(DB_FILE_NAME_FORMAT, "Districts");
            strFilePath = FileHelper.GetFilePath(SysFolder.BACKUP_DB_FOLDER_PATH, strFileName, true);
            strFileContent = lstDistricts.ToJSON();
            FileHelper.SaveTextFile(strFileContent, strFilePath);

            lstProvinces = await objProvincesColl.FindAsync(obj => true).Result.ToListAsync();
            strFileName = string.Format(DB_FILE_NAME_FORMAT, "Provinces");
            strFilePath = FileHelper.GetFilePath(SysFolder.BACKUP_DB_FOLDER_PATH, strFileName, true);
            strFileContent = lstProvinces.ToJSON();
            FileHelper.SaveTextFile(strFileContent, strFilePath);

            lstWards = await objWardsColl.FindAsync(obj => true).Result.ToListAsync();
            strFileName = string.Format(DB_FILE_NAME_FORMAT, "Wards");
            strFilePath = FileHelper.GetFilePath(SysFolder.BACKUP_DB_FOLDER_PATH, strFileName, true);
            strFileContent = lstWards.ToJSON();
            FileHelper.SaveTextFile(strFileContent, strFilePath);

            lstActivityChecklist = await objActivityChecklistColl.FindAsync(obj => true).Result.ToListAsync();
            strFileName = string.Format(DB_FILE_NAME_FORMAT, "ActivityChecklist");
            strFilePath = FileHelper.GetFilePath(SysFolder.BACKUP_DB_FOLDER_PATH, strFileName, true);
            strFileContent = lstActivityChecklist.ToJSON();
            FileHelper.SaveTextFile(strFileContent, strFilePath);

            lstCertificateSettingText = await objCertificateSettingTextColl.FindAsync(obj => true).Result.ToListAsync();
            strFileName = string.Format(DB_FILE_NAME_FORMAT, "CertificateSettingText");
            strFilePath = FileHelper.GetFilePath(SysFolder.BACKUP_DB_FOLDER_PATH, strFileName, true);
            strFileContent = lstCertificateSettingText.ToJSON();
            FileHelper.SaveTextFile(strFileContent, strFilePath);

            lstCertificateTextFont = await objCertificateTextFontColl.FindAsync(obj => true).Result.ToListAsync();
            strFileName = string.Format(DB_FILE_NAME_FORMAT, "CertificateTextFont");
            strFilePath = FileHelper.GetFilePath(SysFolder.BACKUP_DB_FOLDER_PATH, strFileName, true);
            strFileContent = lstCertificateTextFont.ToJSON();
            FileHelper.SaveTextFile(strFileContent, strFilePath);

            lstCommunityRegistEvt = await objCommunityRegistEvtColl.FindAsync(obj => true).Result.ToListAsync();
            strFileName = string.Format(DB_FILE_NAME_FORMAT, "CommunityRegistEvt");
            strFilePath = FileHelper.GetFilePath(SysFolder.BACKUP_DB_FOLDER_PATH, strFileName, true);
            strFileContent = lstCommunityRegistEvt.ToJSON();
            FileHelper.SaveTextFile(strFileContent, strFilePath);

            lstEventInfo = await objEventInfoColl.FindAsync(obj => true).Result.ToListAsync();
            strFileName = string.Format(DB_FILE_NAME_FORMAT, "EventInfo");
            strFilePath = FileHelper.GetFilePath(SysFolder.BACKUP_DB_FOLDER_PATH, strFileName, true);
            strFileContent = lstEventInfo.ToJSON();
            FileHelper.SaveTextFile(strFileContent, strFilePath);

            lstLeaveOfAbsence = await objLeaveOfAbsenceColl.FindAsync(obj => true).Result.ToListAsync();
            strFileName = string.Format(DB_FILE_NAME_FORMAT, "LeaveOfAbsence");
            strFilePath = FileHelper.GetFilePath(SysFolder.BACKUP_DB_FOLDER_PATH, strFileName, true);
            strFileContent = lstLeaveOfAbsence.ToJSON();
            FileHelper.SaveTextFile(strFileContent, strFilePath);

            lstMemberRegistEvt = await objMemberRegistEvtColl.FindAsync(obj => true).Result.ToListAsync();
            strFileName = string.Format(DB_FILE_NAME_FORMAT, "MemberRegistEvt");
            strFilePath = FileHelper.GetFilePath(SysFolder.BACKUP_DB_FOLDER_PATH, strFileName, true);
            strFileContent = lstMemberRegistEvt.ToJSON();
            FileHelper.SaveTextFile(strFileContent, strFilePath);

            lstMemberSupportEvents = await objMemberSupportEventsColl.FindAsync(obj => true).Result.ToListAsync();
            strFileName = string.Format(DB_FILE_NAME_FORMAT, "MemberSupportEvents");
            strFilePath = FileHelper.GetFilePath(SysFolder.BACKUP_DB_FOLDER_PATH, strFileName, true);
            strFileContent = lstMemberSupportEvents.ToJSON();
            FileHelper.SaveTextFile(strFileContent, strFilePath);

            lstFacultys = await objFacultysColl.FindAsync(obj => true).Result.ToListAsync();
            strFileName = string.Format(DB_FILE_NAME_FORMAT, "Facultys");
            strFilePath = FileHelper.GetFilePath(SysFolder.BACKUP_DB_FOLDER_PATH, strFileName, true);
            strFileContent = lstFacultys.ToJSON();
            FileHelper.SaveTextFile(strFileContent, strFilePath);

            lstInterviewRooms = await objInterviewRoomsColl.FindAsync(obj => true).Result.ToListAsync();
            strFileName = string.Format(DB_FILE_NAME_FORMAT, "InterviewRooms");
            strFilePath = FileHelper.GetFilePath(SysFolder.BACKUP_DB_FOLDER_PATH, strFileName, true);
            strFileContent = lstInterviewRooms.ToJSON();
            FileHelper.SaveTextFile(strFileContent, strFilePath);

            lstMemberInfo = await objMemberInfoColl.FindAsync(obj => true).Result.ToListAsync();
            strFileName = string.Format(DB_FILE_NAME_FORMAT, "MemberInfo");
            strFilePath = FileHelper.GetFilePath(SysFolder.BACKUP_DB_FOLDER_PATH, strFileName, true);
            strFileContent = lstMemberInfo.ToJSON();
            FileHelper.SaveTextFile(strFileContent, strFilePath);

            lstMembershipRegistration = await objMembershipRegistrationColl.FindAsync(obj => true).Result.ToListAsync();
            strFileName = string.Format(DB_FILE_NAME_FORMAT, "MembershipRegistration");
            strFilePath = FileHelper.GetFilePath(SysFolder.BACKUP_DB_FOLDER_PATH, strFileName, true);
            strFileContent = lstMembershipRegistration.ToJSON();
            FileHelper.SaveTextFile(strFileContent, strFilePath);

            lstRecruitmentInfo = await objRecruitmentInfoColl.FindAsync(obj => true).Result.ToListAsync();
            strFileName = string.Format(DB_FILE_NAME_FORMAT, "RecruitmentInfo");
            strFilePath = FileHelper.GetFilePath(SysFolder.BACKUP_DB_FOLDER_PATH, strFileName, true);
            strFileContent = lstRecruitmentInfo.ToJSON();
            FileHelper.SaveTextFile(strFileContent, strFilePath);

            lstNewspapers = await objNewspapersColl.FindAsync(obj => true).Result.ToListAsync();
            strFileName = string.Format(DB_FILE_NAME_FORMAT, "Newspapers");
            strFilePath = FileHelper.GetFilePath(SysFolder.BACKUP_DB_FOLDER_PATH, strFileName, true);
            strFileContent = lstNewspapers.ToJSON();
            FileHelper.SaveTextFile(strFileContent, strFilePath);

            lstShortLinkInfo = await objShortLinkInfoColl.FindAsync(obj => true).Result.ToListAsync();
            strFileName = string.Format(DB_FILE_NAME_FORMAT, "ShortLinkInfo");
            strFilePath = FileHelper.GetFilePath(SysFolder.BACKUP_DB_FOLDER_PATH, strFileName, true);
            strFileContent = lstShortLinkInfo.ToJSON();
            FileHelper.SaveTextFile(strFileContent, strFilePath);

            lstSoftwareInfo = await objSoftwareInfoColl.FindAsync(obj => true).Result.ToListAsync();
            strFileName = string.Format(DB_FILE_NAME_FORMAT, "SoftwareInfo");
            strFilePath = FileHelper.GetFilePath(SysFolder.BACKUP_DB_FOLDER_PATH, strFileName, true);
            strFileContent = lstSoftwareInfo.ToJSON();
            FileHelper.SaveTextFile(strFileContent, strFilePath);

        }
    }
}
