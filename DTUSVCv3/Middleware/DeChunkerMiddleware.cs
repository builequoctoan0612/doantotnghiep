﻿//-------------------------------------------------------------------------
// Create               : Phan Ngoc Huong
//
// Create Date          : 17/12/2022
//
// Description          : Middleaware
//
// Editing history      :
//-------------------------------------------------------------------------

using DocumentFormat.OpenXml.InkML;
using DTUSVCv3_Library;
using DTUSVCv3_MemoryData;
using DTUSVCv3_WebSocket;
using Newtonsoft.Json;

namespace DTUSVCv3
{
    public class DeChunkerMiddleware
    {
        private readonly RequestDelegate m_objNext;

        private const string SOURCE = "DeChunkerMiddleware.cs";

        public DeChunkerMiddleware(NotificationsMessageHandler x_objNotificationsMessageHandler, RequestDelegate x_objNext)
        {
            m_objNext = x_objNext;
            SocketAutoSendMessage.Instant.NotificationMessage = x_objNotificationsMessageHandler;
        }

        /// <summary>
        /// Invoke
        /// </summary>
        public async Task Invoke(HttpContext x_objHttpContext)
        {
            int nLoginType;
            string strToken;
            string strResponse;
            string strEndPoint;
            string strClientIp;
            ResponseModel objResponseModel;

            x_objHttpContext.Response.Headers.Add("Access-Control-Allow-Origin", "*");
            x_objHttpContext.Response.Headers.Add("Access-Control-Allow-Credentials", "true");
            x_objHttpContext.Response.Headers.Add("Access-Control-Allow-Headers", "Content-Type, Accept, Content-Length, Authorization");
            x_objHttpContext.Response.Headers.Add("Access-Control-Allow-Methods", "POST,GET,PUT,DELETE");
            strClientIp = x_objHttpContext.GetClientIPAddress();
            if (SystemSetting.IS_SYSTEM_MAINTENANCE == true)
            {
                await Task.Run(
                    async () =>
                    {
                        objResponseModel = ResponseManager.GetErrorResp(ErrorConstant.SYSTEM_MAINTENANCE, ErrorCode.SYSTEM_MAINTENANCE);
                        // Write log
                        LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.SYSTEM_MAINTENANCE}", SOURCE);

                        x_objHttpContext.Response.ContentType = "application/json";
                        strResponse = JsonConvert.SerializeObject(objResponseModel, Formatting.Indented);
                        await x_objHttpContext.Response.WriteAsync(strResponse);
                    });
                return;
            }
            strEndPoint = x_objHttpContext.Request.Path;
            if ((AnonymousService.ANONYMOUS_SERVICE.Contains(strEndPoint) == true) || (strEndPoint.StartsWith("/api/") == false))
            {
                // Skip if it's anonymous access
                await m_objNext(x_objHttpContext);
                return;
            }

            strToken = x_objHttpContext.GetTokenByHttpContext();

            if (string.IsNullOrWhiteSpace(strToken) == true)
            {
                await m_objNext(x_objHttpContext);
                return;
            }

            nLoginType = LoginData.CheckLogin(strToken);
            if (nLoginType != 1)
            {
                await Task.Run(
                    async () =>
                    {
                        if (nLoginType == 0)
                        {
                            // Get error
                            objResponseModel = ResponseManager.GetErrorResp(ErrorConstant.LOGIN_SESSION_EXPIRED, ErrorCode.LOGIN_SESSION_EXPIRED);
                            // Write log
                            LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.LOGIN_SESSION_EXPIRED}", SOURCE);
                        }
                        else
                        {
                            // Get error
                            objResponseModel = ResponseManager.GetErrorResp(ErrorConstant.NOT_LOGIN, ErrorCode.NOT_LOGIN);
                            // Write log
                            LogHelper.WriteLog(LogTypeEnum.ERRORS, strClientIp, string.Empty, $"{SOURCE}: {ErrorConstant.NOT_LOGIN}", SOURCE);
                        }
                        
                        x_objHttpContext.Response.StatusCode = StatusCodes.Status401Unauthorized;
                        x_objHttpContext.Response.ContentType = "application/json";
                        strResponse = JsonConvert.SerializeObject(objResponseModel, Formatting.Indented);
                        await x_objHttpContext.Response.WriteAsync(strResponse);
                    });
                return;
            }

            await m_objNext(x_objHttpContext);
        }
    }
}
