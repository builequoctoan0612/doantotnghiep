﻿using DTUSVCv3;
using DTUSVCv3_EntityFramework;
using DTUSVCv3_Library;
using DTUSVCv3_MemoryData;
using DTUSVCv3_MemoryData.EventData;
using DTUSVCv3_Services;
using DTUSVCv3_Services.QuartzJobs;
using DTUSVCv3_WebSocket;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using Quartz;
using Quartz.AspNetCore;
using System.Text;

public class Program
{
    public static void Main(string[] x_arrArgs)
    {
        CreateHostBuilder(x_arrArgs).Build().Run();
    }

    public static IHostBuilder CreateHostBuilder(string[] x_arArgs)
    {
        return Host.CreateDefaultBuilder(x_arArgs)
            .ConfigureWebHostDefaults(webBuilder =>
            {
                webBuilder.UseSetting("https_port", "5001");
                webBuilder.UseStartup<Startup>();
            });
    }
}

public class Startup
{
    #region Constructor
    public Startup(IWebHostEnvironment x_objEnvironment)
    {
        m_objEnvironment = x_objEnvironment;
    }
    #endregion

    #region Fields
    private readonly IWebHostEnvironment m_objEnvironment;
    #endregion

    #region Types
    private const string QUARTZ_START_NEW_DAY_KEY = "DTUSVC_START_NEW_DAY";
    private const string QUARTZ_START_NEW_DAY_JOB_KEY = "DTUSVC_START_NEW_DAY_JOB";
    private const string QUARTZ_START_NEW_DAY_TIME = "00 00 01 * * ?";

    private const string QUARTZ_TOKEN_REMOVE_KEY = "DTUSVC_TOKEN_REMOVE";
    private const string QUARTZ_TOKEN_REMOVE_JOB_KEY = "DTUSVC_TOKEN_REMOVE_JOB";
    private const string QUARTZ_TOKEN_REMOVE_TIME = "00 0/1 * * * ?";
    #endregion

    #region Methods
    // This method gets called by the runtime. Use this method to add services to the container.
    public void ConfigureServices(IServiceCollection x_objServices)
    {
        IAccountService objIAccountService;
        //JobKey objJobKey;
        byte[] arrKey;
        string strAccountId;

        x_objServices.AddControllersWithViews();
        x_objServices.AddDistributedMemoryCache();
        // Set App path
        SysFolder.APP_PATH = m_objEnvironment.ContentRootPath;
        // set Folder wwwroot path
        SysFolder.WWWROOT_PATH = m_objEnvironment.WebRootPath;
        // Get setting file
        SettingHelper.GetScoringStartDate();

        MongoDbManager.CopyDataBase();
        MemberInfoMemory.Instant.LoadDataBase();
        MemberRegisterMemory.Instant.LoadAllMember();
        MemberRegistEvtData.Instant.LoadAllMember();

        // Start new day
        x_objServices.AddQuartz(obj =>
        {
            JobKey objJobKey;

            objJobKey = new JobKey(QUARTZ_START_NEW_DAY_KEY);
            obj.AddJob<QuartzStartNewDay>(opts => opts.WithIdentity(objJobKey));
            obj.AddTrigger(opts => opts
                    .ForJob(objJobKey)
                    .WithIdentity(QUARTZ_START_NEW_DAY_JOB_KEY)
                    .WithCronSchedule(QUARTZ_START_NEW_DAY_TIME, obj => obj.InTimeZone(DateTimeHelper.GetVNTimeZone())));
        });
        // Token remove
        x_objServices.AddQuartz(obj =>
        {
            JobKey objJobKey;

            objJobKey = new JobKey(QUARTZ_TOKEN_REMOVE_KEY);
            obj.AddJob<QuartzTokenRemove>(opts => opts.WithIdentity(objJobKey));
            obj.AddTrigger(opts => opts
                    .ForJob(objJobKey)
                    .WithIdentity(QUARTZ_TOKEN_REMOVE_JOB_KEY)
                    .WithCronSchedule(QUARTZ_TOKEN_REMOVE_TIME, obj => obj.InTimeZone(DateTimeHelper.GetVNTimeZone())));
        });
        // 
        x_objServices.AddQuartzServer(options =>
        {
            options.WaitForJobsToComplete = false;
        });

        x_objServices.Configure<CookiePolicyOptions>(options =>
        {
            options.CheckConsentNeeded = context => false;
            options.MinimumSameSitePolicy = SameSiteMode.None;
        });
        x_objServices.AddSession(options =>
        {
            options.IdleTimeout = TimeSpan.FromMinutes(60);
            options.Cookie.HttpOnly = false;
            options.Cookie.IsEssential = false;
        });

        x_objServices.AddCors(options =>
        {
            options.AddPolicy("AllowSpecificOrigin",
                builder => builder
                    .WithOrigins("http://localhost:3000")
                    .AllowAnyMethod()
                    .AllowAnyHeader()
                    .AllowCredentials());
        });

        // Add socket
        x_objServices.AddWebSocketManager();
        x_objServices.AddTransient<NotificationsMessageHandler>();

        // Add services to the container.
        x_objServices.AddControllers();
        x_objServices.AddInfrastructure();
        x_objServices.AddEndpointsApiExplorer();
        x_objServices.AddSwaggerGen();
        x_objServices.AddSwaggerGen(x_objSwagger =>
        {
            x_objSwagger.SwaggerDoc("v1", new OpenApiInfo { Title = "DTUSVC API", Version = "v2" });

            x_objSwagger.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
            {
                Name = "Authorization",
                Type = SecuritySchemeType.ApiKey,
                Scheme = "Bearer",
                BearerFormat = "JWT",
                In = ParameterLocation.Header,
                Description = "Enter: {Token}"
            });

            x_objSwagger.AddSecurityRequirement(new OpenApiSecurityRequirement
                {
                    {
                        new OpenApiSecurityScheme
                        {
                            Reference = new OpenApiReference
                            {
                                Type = ReferenceType.SecurityScheme,
                                Id = "Bearer"
                            }
                        },
                        new string[] {}
                    }
                });
        });

        arrKey = Encoding.ASCII.GetBytes(SecurityConstant.CRYPTOKEY);
        x_objServices.AddAuthentication(x_objAuthentication =>
        {
            x_objAuthentication.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
            x_objAuthentication.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
        })
        .AddJwtBearer(x_objJWT =>
        {
            x_objJWT.Events = new JwtBearerEvents
            {
                OnTokenValidated = x_objContext =>
                {
                    ResponseModel objResponseModel;
                    Accounts objAccount;
                    string strResponse;

                    objIAccountService = x_objContext.HttpContext.RequestServices.GetRequiredService<IAccountService>();
                    strAccountId = x_objContext.Principal.Identity.Name;
                    objAccount = objIAccountService.GetAccountById(strAccountId);
                    if (objAccount == null)
                    {
                        objResponseModel = ResponseManager.GetErrorResp(ErrorConstant.LOGIN_SESSION_EXPIRED, ErrorCode.LOGIN_SESSION_EXPIRED);
                        x_objContext.Response.ContentType = "application/json";
                        strResponse = objResponseModel.ToJSON();
                        x_objContext.Response.WriteAsync(strResponse);
                        x_objContext.Success();
                    }
                    return Task.CompletedTask;
                }
            };
            x_objJWT.RequireHttpsMetadata = false;
            x_objJWT.SaveToken = true;
            x_objJWT.TokenValidationParameters = new TokenValidationParameters
            {
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = new SymmetricSecurityKey(arrKey),
                ValidateIssuer = false,
                ValidateAudience = false
            };
        });
    }

    // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
    public void Configure(IApplicationBuilder x_objApp, IWebHostEnvironment x_objEnvironment)
    {
        // Configure the HTTP request pipeline.

        x_objApp.UseCors("AllowSpecificOrigin");
        if (x_objEnvironment.IsDevelopment())
        {
            x_objApp.UseDeveloperExceptionPage();
        }
        x_objApp.UseSwagger();
        x_objApp.UseSwaggerUI();

        //soket
        IServiceScopeFactory objServiceScopeFactory = x_objApp.ApplicationServices.GetRequiredService<IServiceScopeFactory>();
        IServiceProvider objServiceProvider = objServiceScopeFactory.CreateScope().ServiceProvider;
        x_objApp.UseForwardedHeaders(new ForwardedHeadersOptions
        {
            ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto
        });
        x_objApp.UseHttpsRedirection();
        x_objApp.UseStatusCodePages();
        x_objApp.UseWebSockets();
        x_objApp.MapWebSocketManager("/notification", objServiceProvider.GetService<NotificationsMessageHandler>());
        x_objApp.UseMiddleware<DeChunkerMiddleware>();
        x_objApp.UseSession();

        x_objApp.UseRouting();

        x_objApp.UseAuthentication();
        x_objApp.UseAuthorization();

        x_objApp.UseEndpoints(endpoints =>
        {
            endpoints.MapControllers();
        });

        x_objApp.UseStaticFiles(new StaticFileOptions
        {
            // Prevent access to wwwroot file with insufficient access rights
            OnPrepareResponse = ctx =>
            {
                bool bIsValid;
                // Check the request's access permission
                bIsValid = ctx.Context.ViewFileIsValid();
                if (bIsValid == false)
                {
                    // Navigate to another site if not authorized. (Design 404 page)
                    ctx.Context.Response.Redirect(SysFolder.PAGE_404_PATH);
                }
            }
        });
    }
    #endregion
}
