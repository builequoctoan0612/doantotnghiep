﻿using DTUSVCv3_Library;
using DTUSVCv3_Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace DTUSVCv3.Controllers
{
    public class AddressController : BaseController
    {
        #region Constructor
        public AddressController(IAddressService x_objAddressService)
        {
            m_objAddressService = x_objAddressService;
        }
        #endregion

        #region Fields
        private readonly IAddressService m_objAddressService;
        #endregion

        #region Methods
        /// <summary>
        /// Get Provinces
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpGet]
        public async Task<IActionResult> GetProvinces()
        {
            ResponseModel objResponse;

            objResponse = await m_objAddressService.GetProvinces();
            return Ok(objResponse);
        }

        /// <summary>
        /// Get Provinces
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpGet]
        public async Task<IActionResult> GetDistrictsByProvinceId([FromQuery] string x_strProvinceId)
        {
            ResponseModel objResponse;

            objResponse = await m_objAddressService.GetDistrictsByProvinceId(x_strProvinceId);
            return Ok(objResponse);
        }

        /// <summary>
        /// Get Provinces
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpGet]
        public async Task<IActionResult> GetWardsByDistrictId([FromQuery] string x_strDistrictId)
        {
            ResponseModel objResponse;

            objResponse = await m_objAddressService.GetWardsByDistrictId(x_strDistrictId);
            return Ok(objResponse);
        }

        /// <summary>
        /// Update Hometown
        /// </summary>
        /// <param name="x_objRequest"></param>
        /// <returns></returns>
        [HttpPut]
        public async Task<IActionResult> UpdateHometown([FromBody] UpdateHometownRequest x_objRequest)
        {
            ResponseModel objRes;
            string strAccountId;

            strAccountId = User.GetUserId();
            objRes = await m_objAddressService.UpdateHometown(x_objRequest, strAccountId, HttpContext);
            return Ok(objRes);
        }
        #endregion
    }
}
