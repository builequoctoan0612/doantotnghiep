﻿using DTUSVCv3_Library;
using DTUSVCv3_Services;
using DTUSVCv3_Services.Models.AccountModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace DTUSVCv3
{
    public class AccountController : BaseController
    {
        #region Constructor
        public AccountController(IAccountService x_objAccount)
        {
            m_objAccount = x_objAccount;
        }
        #endregion

        #region Fields
        private readonly IAccountService m_objAccount;
        #endregion

        #region Methods
        /// <summary>
        /// Authentication
        /// </summary>
        [AllowAnonymous]
        [HttpPost]
        public async Task<IActionResult> Authenticate([FromBody]LoginRequestModel x_objRequestModel)
        {
            ResponseModel objRes;

            objRes = await m_objAccount.Login(x_objRequestModel, HttpContext);
            return Ok(objRes);
        }

        /// <summary>
        /// Logout
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IActionResult Logout()
        {
            ResponseModel objRes;

            objRes = m_objAccount.Logout(HttpContext);
            return Ok(objRes);
        }

        /// <summary>
        /// Refresh Access Token
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpPost]
        public IActionResult RefreshAccessToken([FromBody] RefeshTokenRequestModel x_objRefreshToken)
        {
            ResponseModel objRes;

            objRes = m_objAccount.RefreshAccessToken(x_objRefreshToken, HttpContext);
            return Ok(objRes);
        }

        /// <summary>
        /// Change Password
        /// </summary>
        /// <returns></returns>
        [HttpPut]
        public async Task<IActionResult> ChangePassword([FromBody] ChangePasswdRequestModel x_objRequestModel)
        {
            ResponseModel objRes;
            string strAccountId;

            strAccountId = User.GetUserId();
            objRes = await m_objAccount.ChangePassword(x_objRequestModel, strAccountId, HttpContext);
            return Ok(objRes);
        }

        /// <summary>
        /// Get Captcha Forgot
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpPost]
        public IActionResult GetCaptcha([FromBody] GetCaptchaRequestModel x_objRequestModel)
        {
            ResponseModel objRes;

            objRes = m_objAccount.GetCaptcha(x_objRequestModel);
            return Ok(objRes);
        }

        /// <summary>
        /// Forgot Password
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpPost]
        public async Task<IActionResult> ForgotPassword([FromBody] ForgotPasswdRequesModel x_objRequestModel)
        {
            ResponseModel objRes;
            objRes = await m_objAccount.ForgotPassword(x_objRequestModel, HttpContext);
            return Ok(objRes);
        }

        /// <summary>
        /// Forgot Password
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpPut]
        public async Task<IActionResult> ResetPassword([FromBody] ForgotChangePasswdRequestModel x_objRequestModel)
        {
            ResponseModel objRes;
            objRes = await m_objAccount.ResetPassword(x_objRequestModel, HttpContext);
            return Ok(objRes);
        }

        /// <summary>
        /// Forgot Password
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpGet]
        public IActionResult CheckForExistingTokens([FromQuery] string x_objToken)
        {
            ResponseModel objRes;
            objRes = m_objAccount.CheckForExistingTokens(x_objToken);
            return Ok(objRes);
        }

        /// <summary>
        /// Reset Password By Admin
        /// </summary>
        /// <param name="x_strMemberId"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> ResetPasswordByAdmin([FromQuery] string x_strMemberId)
        {
            ResponseModel objRes;
            string strAccountId;

            strAccountId = User.GetUserId();
            objRes = await m_objAccount.ResetPasswordByAdmin(x_strMemberId, strAccountId, HttpContext);
            return Ok(objRes);
        }

        /// <summary>
        /// Lock Or Unlock Account
        /// </summary>
        /// <param name="x_strMemberId"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> LockOrUnlockAccount([FromQuery] string x_strMemberId)
        {
            ResponseModel objRes;
            string strAccountId;

            strAccountId = User.GetUserId();
            objRes = await m_objAccount.LockOrUnlockAccount(x_strMemberId, strAccountId, HttpContext);
            return Ok(objRes);
        }

        /// <summary>
        /// Get List Account
        /// </summary>
        /// <param name="x_strMemberId"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> GetListAccount()
        {
            ResponseModel objRes;
            string strAccountId;

            strAccountId = User.GetUserId();
            objRes = await m_objAccount.GetListAccount(strAccountId, HttpContext);
            return Ok(objRes);
        }

        /// <summary>
        /// Check Login
        /// </summary>
        /// <param name="x_strMemberId"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> CheckLogin()
        {
            ResponseModel objRes;
            string strAccountId;

            strAccountId = User.GetUserId();
            objRes = await m_objAccount.CheckLogin(strAccountId, HttpContext);
            return Ok(objRes);
        }

        /// <summary>
        /// Change Role Of Member
        /// </summary>
        /// <returns></returns>
        [HttpPut]
        public async Task<IActionResult> ChangeRoleOfMember([FromBody] ChangeRoleOfAccountRequestModel x_objRequestModel)
        {
            ResponseModel objRes;
            string strAccountId;

            strAccountId = User.GetUserId();
            objRes = await m_objAccount.ChangeRoleOfMember(x_objRequestModel, strAccountId, HttpContext);
            return Ok(objRes);
        }
        #endregion
    }
}
