﻿using DTUSVCv3_Library;
using DTUSVCv3_Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace DTUSVCv3
{
    public class RecruitmentController : BaseController
    {
        #region Constructor
        /// <summary>
        /// Recruitment Controller
        /// </summary>
        /// <param name="x_objRecruitmentService"></param>
        public RecruitmentController(IRecruitmentService x_objRecruitmentService)
        {
            m_objRecruitmentService = x_objRecruitmentService;
        }
        #endregion

        #region Fileds
        private IRecruitmentService m_objRecruitmentService;
        #endregion

        #region Methods
        /// <summary>
        /// Create Recruitment
        /// </summary>
        /// <param name="x_objRequestModel"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> CreateRecruitment([FromBody] CreateRecruitmentRequestModel x_objRequestModel)
        {
            ResponseModel objResponse;
            string strAccountId;

            // Get account id
            strAccountId = User.GetUserId();
            // Create recruitment
            objResponse = await m_objRecruitmentService.CreateRecruitment(x_objRequestModel, strAccountId, HttpContext);
            return Ok(objResponse);
        }

        /// <summary>
        /// Get Recruitment Info
        /// </summary>
        /// <param name="x_strRecrumentId"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> GetRecruitmentInfo(string x_strRecrumentId)
        {
            ResponseModel objResponse;
            objResponse = await m_objRecruitmentService.GetRecruitmentInfo(x_strRecrumentId, HttpContext);
            return Ok(objResponse);
        }

        /// <summary>
        /// Get Recruitment Valid
        /// </summary>
        /// <param name="x_strRecrumentId"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpGet]
        public async Task<IActionResult> GetRecruitmentValid()
        {
            ResponseModel objResponse;
            objResponse = await m_objRecruitmentService.GetRecruitmentValid(HttpContext);
            return Ok(objResponse);
        }

        /// <summary>
        /// Get Recruitment List
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> GetRecruitmentList()
        {
            ResponseModel objResponse;
            string strAccountId;

            // Get account id
            strAccountId = User.GetUserId();
            objResponse = await m_objRecruitmentService.GetRecruitmentList(strAccountId, HttpContext);
            return Ok(objResponse);
        }

        /// <summary>
        /// Create Recruitment
        /// </summary>
        /// <param name="x_objRequestModel"></param>
        /// <returns></returns>
        [HttpPut]
        public async Task<IActionResult> UpdateRecruitment([FromBody] UpdateRecruitmentRequestModel x_objRequestModel)
        {
            ResponseModel objResponse;
            string strAccountId;

            // Get account id
            strAccountId = User.GetUserId();
            // Create recruitment
            objResponse = await m_objRecruitmentService.UpdateRecruitment(x_objRequestModel, strAccountId, HttpContext);
            return Ok(objResponse);
        }

        /// <summary>
        /// Get Recruitment List
        /// </summary>
        /// <returns></returns>
        [HttpDelete]
        public async Task<IActionResult> DeleteRecruitment([FromQuery] string x_strRecruitmentId)
        {
            ResponseModel objResponse;
            string strAccountId;

            // Get account id
            strAccountId = User.GetUserId();
            objResponse = await m_objRecruitmentService.DeleteRecruitment(x_strRecruitmentId, strAccountId, HttpContext);
            return Ok(objResponse);
        }

        /// <summary>
        /// Lock Or Unlock Recruitment
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> LockOrUnlockRecruitment([FromQuery] string x_strRecruitmentId)
        {
            ResponseModel objResponse;
            string strAccountId;

            // Get account id
            strAccountId = User.GetUserId();
            objResponse = await m_objRecruitmentService.LockOrUnlockRecruitment(x_strRecruitmentId, strAccountId, HttpContext);
            return Ok(objResponse);
        }

        /// <summary>
        /// Set Finish Recruitment
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> SetFinishRecruitment([FromQuery] string x_strRecruitmentId)
        {
            ResponseModel objResponse;
            string strAccountId;

            // Get account id
            strAccountId = User.GetUserId();
            objResponse = await m_objRecruitmentService.SetFinishRecruitment(x_strRecruitmentId, strAccountId, HttpContext);
            return Ok(objResponse);
        }

        /// <summary>
        /// Set Finish Recruitment
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpPost]
        public async Task<IActionResult> ApplyingForMember([FromBody] CandidateInfoRequestModel x_objRequestModel)
        {
            ResponseModel objResponse;
            objResponse = await m_objRecruitmentService.ApplyingForMember(x_objRequestModel, HttpContext);
            return Ok(objResponse);
        }

        /// <summary>
        /// Create Interview Room
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> CreateInterviewRoom([FromBody] CreateInterviewRoomRequestModel x_objRequestModel)
        {
            ResponseModel objResponse;
            string strAccountId;

            // Get account id
            strAccountId = User.GetUserId();
            objResponse = await m_objRecruitmentService.CreateInterviewRoom(x_objRequestModel, strAccountId, HttpContext);
            return Ok(objResponse);
        }

        /// <summary>
        /// Create Interview Room
        /// </summary>
        /// <returns></returns>
        [HttpPut]
        public async Task<IActionResult> UpdateInterviewRoom([FromBody] UpdateInterviewRoomRequestModel x_objRequestModel)
        {
            ResponseModel objResponse;
            string strAccountId;

            // Get account id
            strAccountId = User.GetUserId();
            objResponse = await m_objRecruitmentService.UpdateInterviewRoom(x_objRequestModel, strAccountId, HttpContext);
            return Ok(objResponse);
        }

        /// <summary>
        /// Delete Interview Room
        /// </summary>
        /// <returns></returns>
        [HttpDelete]
        public async Task<IActionResult> DeleteInterviewRoom([FromQuery] string x_strRoomId, [FromQuery] string x_strRecruitmentId)
        {
            ResponseModel objResponse;
            string strAccountId;

            // Get account id
            strAccountId = User.GetUserId();
            objResponse = await m_objRecruitmentService.DeleteInterviewRoom(x_strRoomId, x_strRecruitmentId, strAccountId, HttpContext);
            return Ok(objResponse);
        }

        /// <summary>
        /// Delete Interview Room
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> AddInterviewToRoom([FromBody] AddInterviewList x_objRequestModel)
        {
            ResponseModel objResponse;
            string strAccountId;

            // Get account id
            strAccountId = User.GetUserId();
            objResponse = await m_objRecruitmentService.AddInterviewToRoom(x_objRequestModel, strAccountId, HttpContext);
            return Ok(objResponse);
        }

        /// <summary>
        /// Remove Interview From Room
        /// </summary>
        /// <returns></returns>
        [HttpDelete]
        public async Task<IActionResult> RemoveInterviewFromRoom([FromQuery] string x_strMemberId, [FromQuery] string x_strRoomId, [FromQuery] string x_strRecruitmentId)
        {
            ResponseModel objResponse;
            string strAccountId;

            // Get account id
            strAccountId = User.GetUserId();
            objResponse = await m_objRecruitmentService.RemoveInterviewFromRoom(x_strMemberId, x_strRoomId, x_strRecruitmentId, strAccountId, HttpContext);
            return Ok(objResponse);
        }

        /// <summary>
        /// Remove Member interview
        /// </summary>
        /// <returns></returns>
        [HttpDelete]
        public async Task<IActionResult> DeleteApplyingForMemberInfo([FromQuery] string x_strMemberId, [FromQuery] string x_strRecruitmentId)
        {
            ResponseModel objResponse;
            string strAccountId;

            // Get account id
            strAccountId = User.GetUserId();
            objResponse = await m_objRecruitmentService.DeleteApplyingForMemberInfo(x_strMemberId, x_strRecruitmentId, strAccountId, HttpContext);
            return Ok(objResponse);
        }

        /// <summary>
        /// Get Applying For Members
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> GetApplyingForMembers([FromQuery] string x_strRecruitmentId)
        {
            ResponseModel objResponse;
            string strAccountId;

            // Get account id
            strAccountId = User.GetUserId();
            objResponse = await m_objRecruitmentService.GetApplyingForMembers(x_strRecruitmentId, strAccountId, HttpContext);
            return Ok(objResponse);
        }

        /// <summary>
        /// Get Member List
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> DownloadApplyingForMembers([FromQuery] string x_strRecruitmentId)
        {
            ResponseDownloadFile objRes;
            ResponseModel objResult;
            string strAccountId;

            strAccountId = User.GetUserId();
            objResult = await m_objRecruitmentService.DownloadApplyingForMembers(x_strRecruitmentId, strAccountId, HttpContext);
            if (objResult.IsSuccsess == false)
            {
                if (objResult is ResponseDownloadFile)
                {
                    objRes = objResult as ResponseDownloadFile;
                    objRes.FilePath = null;
                    objResult = objRes;
                }
                return Ok(objResult);
            }
            else
            {
                objRes = objResult as ResponseDownloadFile;
            }

            return new FileStreamResult(objRes.FileStream, objRes.MimeType)
            {
                FileDownloadName = Path.GetFileName(objRes.FilePath)
            };
        }

        /// <summary>
        /// Get Applying For Members
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> GetApplyingMemberInfo([FromQuery] string x_strMemberId, [FromQuery] string x_strRecruitmentId)
        {
            ResponseModel objResponse;
            string strAccountId;

            // Get account id
            strAccountId = User.GetUserId();
            objResponse = await m_objRecruitmentService.GetApplyingMemberInfo(x_strMemberId, x_strRecruitmentId, strAccountId, HttpContext);
            return Ok(objResponse);
        }

        /// <summary>
        /// Get Room By Recruitment Id
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> GetRoomByRecruitmentId([FromQuery] string x_strRecruitmentId)
        {
            ResponseModel objResponse;
            string strAccountId;

            // Get account id
            strAccountId = User.GetUserId();
            objResponse = await m_objRecruitmentService.GetRoomByRecruitmentId(x_strRecruitmentId, strAccountId, HttpContext);
            return Ok(objResponse);
        }

        /// <summary>
        /// Delete Interview Room
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> MoveInterview([FromBody] MoveMemberFromRoom x_objRequestModel)
        {
            ResponseModel objResponse;
            string strAccountId;

            // Get account id
            strAccountId = User.GetUserId();
            objResponse = await m_objRecruitmentService.MoveInterview(x_objRequestModel, strAccountId, HttpContext);
            return Ok(objResponse);
        }

        /// <summary>
        /// Send Email Interview
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> SendEmailInterview([FromQuery] string x_strRoomId, [FromQuery] string x_strRecruitmentId)
        {
            ResponseModel objResponse;
            string strAccountId;

            // Get account id
            strAccountId = User.GetUserId();
            objResponse = await m_objRecruitmentService.SendEmailInterview(x_strRoomId, x_strRecruitmentId, strAccountId, HttpContext);
            return Ok(objResponse);
        }

        /// <summary>
        /// Delete Interview Room
        /// </summary>
        /// <returns></returns>
        [HttpPut]
        public async Task<IActionResult> UpdateReview([FromBody] UpdateReviewReqeusrModel x_objRequestModel)
        {
            ResponseModel objResponse;
            string strAccountId;

            // Get account id
            strAccountId = User.GetUserId();
            objResponse = await m_objRecruitmentService.UpdateReview(x_objRequestModel, strAccountId, HttpContext);
            return Ok(objResponse);
        }

        /// <summary>
        /// Member Approval
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> MemberApproval([FromBody] MemberApprovalRequestModel x_objRequestModel)
        {
            ResponseModel objResponse;
            string strAccountId;

            // Get account id
            strAccountId = User.GetUserId();
            objResponse = await m_objRecruitmentService.MemberApproval(x_objRequestModel, strAccountId, HttpContext);
            return Ok(objResponse);
        }
        #endregion
    }
}
