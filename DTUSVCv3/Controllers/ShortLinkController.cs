﻿using DTUSVCv3_Library;
using DTUSVCv3_Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace DTUSVCv3
{
    public class ShortLinkController : BaseController
    {
        #region Constructor
        public ShortLinkController(IShortLinkService x_objShortLink)
        {
            m_objShortLink = x_objShortLink;
        }
        #endregion

        #region Feilds
        private readonly IShortLinkService m_objShortLink;
        #endregion

        #region Methods
        /// <summary>
        /// Create Short Link
        /// </summary>
        /// <param name="x_objRequest"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpPost]
        public async Task<IActionResult> CreateShortLink([FromBody] ShortLinkRequestModel x_objRequest)
        {
            ResponseModel objResponse;

            objResponse = await m_objShortLink.CreateShortLink(x_objRequest);
            return Ok(objResponse);
        }

        /// <summary>
        /// Get Short Link
        /// </summary>
        /// <param name="x_strKeyCode"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpGet]
        public async Task<IActionResult> GetShortLink([FromQuery]string x_strKeyCode)
        {
            ResponseModel objResponse;

            objResponse = await m_objShortLink.GetShortLink(x_strKeyCode);
            return Ok(objResponse);
        }
        #endregion
    }
}
