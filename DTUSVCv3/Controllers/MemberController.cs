﻿using DTUSVCv3_Library;
using DTUSVCv3_Services;
using Microsoft.AspNetCore.Mvc;

namespace DTUSVCv3
{
    public class MemberController : BaseController
    {
        #region Constructor
        public MemberController(IMemberService x_objMemberService)
        {
            m_objMemberService = x_objMemberService;
        }
        #endregion

        #region Fields
        private readonly IMemberService m_objMemberService;
        #endregion

        #region Methods
        /// <summary>
        /// Get my info
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> GetMyInfo()
        {
            ResponseModel objRes;
            string strAccountId;

            strAccountId = User.GetUserId();
            objRes = await m_objMemberService.GetMyInfo(strAccountId, HttpContext);
            return Ok(objRes);
        }

        /// <summary>
        /// Update Member Info
        /// </summary>
        /// <param name="x_objRequestModel"></param>
        /// <returns></returns>
        [HttpPut]
        public async Task<IActionResult> UpdateMemberInfo([FromBody] ChangeMyInfoReqestModel x_objRequestModel)
        {
            ResponseModel objRes;
            string strAccountId;

            strAccountId = User.GetUserId();
            objRes = await m_objMemberService.UpdateMyInfo(x_objRequestModel, strAccountId, HttpContext);
            return Ok(objRes);
        }

        /// <summary>
        /// Logout
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> GetMemberRank()
        {
            ResponseModel objRes;
            string strAccountId;

            strAccountId = User.GetUserId();
            objRes = await m_objMemberService.GetMemberRank(strAccountId, HttpContext);
            return Ok(objRes);
        }

        /// <summary>
        /// Get Member Info
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> GetMemberInfo([FromQuery] string x_strMemberId)
        {
            ResponseModel objRes;
            string strAccountId;

            strAccountId = User.GetUserId();
            objRes = await m_objMemberService.GetMemberInfo(x_strMemberId, strAccountId, HttpContext);
            return Ok(objRes);
        }

        /// <summary>
        /// Search Member Info
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> SearchMemberInfo([FromQuery] string x_strMemberId)
        {
            ResponseModel objRes;
            string strAccountId;

            strAccountId = User.GetUserId();
            objRes = await m_objMemberService.SearchMemberInfo(x_strMemberId, strAccountId, HttpContext);
            return Ok(objRes);
        }

        /// <summary>
        /// Delete Member
        /// </summary>
        /// <returns></returns>
        [HttpDelete]
        public async Task<IActionResult> DeleteMember([FromQuery] string x_strMemberId)
        {
            ResponseModel objRes;
            string strAccountId;

            strAccountId = User.GetUserId();
            objRes = await m_objMemberService.DeleteMember(x_strMemberId, strAccountId, HttpContext);
            return Ok(objRes);
        }

        /// <summary>
        /// Get Member List
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> GetMemberList([FromQuery] int x_nMemberType)
        {
            ResponseModel objRes;
            string strAccountId;

            strAccountId = User.GetUserId();
            objRes = await m_objMemberService.GetMemberList(x_nMemberType, strAccountId, HttpContext);
            return Ok(objRes);
        }

        /// <summary>
        /// Get Member List
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> DownloadMemberList([FromQuery] int x_nMemberType)
        {
            ResponseDownloadFile objRes;
            ResponseModel objResult;
            string strAccountId;

            strAccountId = User.GetUserId();
            objResult = await m_objMemberService.DownloadMemberList(x_nMemberType, strAccountId, HttpContext);
            if (objResult.IsSuccsess == false)
            {
                if (objResult is ResponseDownloadFile)
                {
                    objRes = objResult as ResponseDownloadFile;
                    objRes.FilePath = null;
                    objResult = objRes;
                }
                return Ok(objResult);
            }
            else
            {
                objRes = objResult as ResponseDownloadFile;
            }

            return new FileStreamResult(objRes.FileStream, objRes.MimeType)
            {
                FileDownloadName = Path.GetFileName(objRes.FilePath)
            };
        }

        /// <summary>
        /// Get Member List
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> DownloadMemberCard([FromBody] DownloadMemberCardRequestModel x_objRequestModel)
        {
            ResponseDownloadFile objRes;
            ResponseModel objResult;
            string strAccountId;

            strAccountId = User.GetUserId();
            objResult = await m_objMemberService.DownloadMemberCard(x_objRequestModel, strAccountId, HttpContext);
            if (objResult.IsSuccsess == false)
            {
                if (objResult is ResponseDownloadFile)
                {
                    objRes = objResult as ResponseDownloadFile;
                    objRes.FilePath = null;
                    objResult = objRes;
                }
                return Ok(objResult);
            }
            else
            {
                objRes = objResult as ResponseDownloadFile;
            }

            return new FileStreamResult(objRes.FileStream, objRes.MimeType)
            {
                FileDownloadName = Path.GetFileName(objRes.FilePath)
            };
        }

        /// <summary>
        /// Update Avatar
        /// </summary>
        /// <param name="x_objRequestModel"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> UpdateAvatar([FromForm] ChangeAvatarRequestModel x_objRequestModel)
        {
            ResponseModel objRes;
            string strAccountId;

            strAccountId = User.GetUserId();
            objRes = await m_objMemberService.UpdateAvatar(x_objRequestModel, strAccountId, HttpContext);
            return Ok(objRes);
        }
        #endregion
    }
}
