﻿using DTUSVCv3_Library;
using DTUSVCv3_Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace DTUSVCv3
{
    public class NewspaperController : BaseController
    {
        public NewspaperController(INewspaperService x_objNewspaper)
        {
            m_objNewspaper = x_objNewspaper;
        }

        #region Feilds
        private readonly INewspaperService m_objNewspaper;
        #endregion

        #region Methods
        /// <summary>
        /// Create newspaper
        /// </summary>
        /// <param name="x_objRequest"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> CreateNewspaper([FromForm] CreateNewspaperRequestModel x_objRequest)
        {
            ResponseModel objRes;
            string strAccountId;

            strAccountId = User.GetUserId();
            objRes = await m_objNewspaper.CreateNewspaper(x_objRequest, strAccountId, HttpContext);
            return Ok(objRes);
        }

        /// <summary>
        /// Update Newspaper
        /// </summary>
        /// <param name="x_objRequest"></param>
        /// <returns></returns>
        [HttpPut]
        public async Task<IActionResult> UpdateNewspaper([FromForm] UpdateNewspaperRequestModel x_objRequest)
        {
            ResponseModel objRes;
            string strAccountId;

            strAccountId = User.GetUserId();
            objRes = await m_objNewspaper.UpdateNewspaper(x_objRequest, strAccountId, HttpContext);
            return Ok(objRes);
        }

        /// <summary>
        /// Get List Newspaper
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpGet]
        public async Task<IActionResult> GetNewspaperList(int x_nPageIndex, int x_nPageSize)
        {
            ResponseModel objRes;

            objRes = await m_objNewspaper.GetNewspaperList(x_nPageIndex, x_nPageSize, HttpContext);
            return Ok(objRes);
        }

        /// <summary>
        /// Get Newspaper By Id
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpGet]
        public async Task<IActionResult> GetNewspaperById(string x_strNewspaperId)
        {
            ResponseModel objRes;

            objRes = await m_objNewspaper.GetNewspaperById(x_strNewspaperId, HttpContext);
            return Ok(objRes);
        }

        /// <summary>
        /// Get Newspaper By Id
        /// </summary>
        [HttpDelete]
        public async Task<IActionResult> DeleteNewspaper(string x_strNewspaperId)
        {
            ResponseModel objRes;
            string strAccountId;

            strAccountId = User.GetUserId();
            objRes = await m_objNewspaper.DeleteNewspaper(x_strNewspaperId, strAccountId, HttpContext);
            return Ok(objRes);
        }

        /// <summary>
        /// Upload Image
        /// </summary>
        /// <param name="x_objRequest"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> UploadImage([FromForm] UploadImage x_objRequest)
        {
            ResponseModel objRes;
            string strAccountId;

            strAccountId = User.GetUserId();
            objRes = await m_objNewspaper.UploadImage(x_objRequest, strAccountId, HttpContext);
            return Ok(objRes);
        }
        #endregion
    }
}
