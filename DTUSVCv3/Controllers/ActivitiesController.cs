﻿using DTUSVCv3_Library;
using DTUSVCv3_Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace DTUSVCv3.Controllers
{
    public class ActivitiesController : BaseController
    {
        #region Constructor
        public ActivitiesController(IActivitiesService x_objActivitiesService)
        {
            m_objActivitiesService = x_objActivitiesService;
        }
        #endregion

        #region Fields
        private readonly IActivitiesService m_objActivitiesService;
        #endregion

        #region Methods
        /// <summary>
        /// Create Activitie
        /// </summary>
        /// <param name="x_objRequest"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> CreateActivitie([FromForm] CreateActivityRequestModel x_objRequest)
        {
            ResponseModel objRes;
            string strAccountId;

            strAccountId = User.GetUserId();
            objRes = await m_objActivitiesService.CreateActivitie(x_objRequest, strAccountId, HttpContext);
            return Ok(objRes);
        }

        /// <summary>
        /// Update Activitie
        /// </summary>
        /// <param name="x_objRequest"></param>
        /// <returns></returns>
        [HttpPut]
        public async Task<IActionResult> UpdateActivitie([FromForm] UpdateActivityRequestModel x_objRequest)
        {
            ResponseModel objRes;
            string strAccountId;

            strAccountId = User.GetUserId();
            objRes = await m_objActivitiesService.UpdateActivitie(x_objRequest, strAccountId, HttpContext);
            return Ok(objRes);
        }

        /// <summary>
        /// Delete Activitie
        /// </summary>
        /// <param name="x_strActivityId"></param>
        /// <returns></returns>
        [HttpDelete]
        public async Task<IActionResult> DeleteActivitie([FromQuery] string x_strActivityId)
        {
            ResponseModel objRes;
            string strAccountId;

            strAccountId = User.GetUserId();
            objRes = await m_objActivitiesService.DeleteActivitie(x_strActivityId, strAccountId, HttpContext);
            return Ok(objRes);
        }

        /// <summary>
        /// Get List Activity
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpGet]
        public async Task<IActionResult> GetListActivities(int x_nPageIndex, int x_nPageSize)
        {
            ResponseModel objRes;
            string strAccountId;

            strAccountId = User.GetUserId();
            objRes = await m_objActivitiesService.GetListActivities(x_nPageIndex, x_nPageSize, strAccountId, HttpContext);
            return Ok(objRes);
        }

        /// <summary>
        /// Get List Activity
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpGet]
        public async Task<IActionResult> GetActivitieById([FromQuery] string x_strActivityId)
        {
            ResponseModel objRes;
            string strAccountId;

            strAccountId = User.GetUserId();
            objRes = await m_objActivitiesService.GetActivitieById(x_strActivityId, strAccountId, HttpContext);
            return Ok(objRes);
        }

        /// <summary>
        /// Get List Activity
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> GetActivityHistories()
        {
            ResponseModel objRes;
            string strAccountId;

            strAccountId = User.GetUserId();
            objRes = await m_objActivitiesService.GetActivityHistories(strAccountId, HttpContext);
            return Ok(objRes);
        }

        /// <summary>
        /// Get List Activity
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> DownloadActivityReport([FromQuery] string x_strActivityId)
        {
            ResponseDownloadFile objRes;
            ResponseModel objResult;
            string strAccountId;

            strAccountId = User.GetUserId();
            objResult = await m_objActivitiesService.DownloadActivityReport(x_strActivityId, strAccountId, HttpContext);
            if (objResult.IsSuccsess == false)
            {
                if (objResult is ResponseDownloadFile)
                {
                    objRes = objResult as ResponseDownloadFile;
                    objRes.FilePath = null;
                    objResult = objRes;
                }
                return Ok(objResult);
            }
            else
            {
                objRes = objResult as ResponseDownloadFile;
            }

            return new FileStreamResult(objRes.FileStream, objRes.MimeType)
            {
                FileDownloadName = Path.GetFileName(objRes.FilePath)
            };
        }

        /// <summary>
        /// Get List Activity
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpPost]
        public async Task<IActionResult> CommunityRegistration([FromBody] CommunityRegisterRequestModel x_objRequest)
        {
            ResponseModel objRes;
            objRes = await m_objActivitiesService.CommunityRegistration(x_objRequest, HttpContext);
            return Ok(objRes);
        }

        /// <summary>
        /// Member Registration
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> MemberRegistration([FromQuery] string x_strActivityId)
        {
            ResponseModel objRes;
            string strAccountId;

            strAccountId = User.GetUserId();
            objRes = await m_objActivitiesService.MemberRegistration(x_strActivityId, strAccountId, HttpContext);
            return Ok(objRes);
        }

        /// <summary>
        /// Get List Activity
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> MemberLeaveRegistration([FromBody] MemberLeaveActivityRequestModel x_objRequest)
        {
            ResponseModel objRes;
            string strAccountId;

            strAccountId = User.GetUserId();
            objRes = await m_objActivitiesService.MemberLeaveRegistration(x_objRequest, strAccountId, HttpContext);
            return Ok(objRes);
        }

        /// <summary>
        /// Delete Registration
        /// </summary>
        /// <returns></returns>
        [HttpDelete]
        public async Task<IActionResult> DeleteRegistration([FromQuery] string x_strMemberId, [FromQuery] string x_strActivityId)
        {
            ResponseModel objRes;
            string strAccountId;

            strAccountId = User.GetUserId();
            objRes = await m_objActivitiesService.DeleteRegistration(x_strMemberId, x_strActivityId, strAccountId, HttpContext);
            return Ok(objRes);
        }

        /// <summary>
        /// Check in
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> CheckIn([FromQuery] string x_strMemberId, [FromQuery] string x_strActivityId)
        {
            ResponseModel objRes;
            string strAccountId;

            strAccountId = User.GetUserId();
            objRes = await m_objActivitiesService.CheckIn(x_strMemberId, x_strActivityId, strAccountId, HttpContext);
            return Ok(objRes);
        }

        /// <summary>
        /// Remove Registration
        /// </summary>
        /// <returns></returns>
        [HttpDelete]
        public async Task<IActionResult> DeleteCheckIn([FromQuery] string x_strMemberId, [FromQuery] string x_strActivityId, [FromQuery] string x_strCheckInId)
        {
            ResponseModel objRes;
            string strAccountId;

            strAccountId = User.GetUserId();
            objRes = await m_objActivitiesService.DeleteCheckIn(x_strMemberId, x_strActivityId, x_strCheckInId, strAccountId, HttpContext);
            return Ok(objRes);
        }

        /// <summary>
        /// Update Check In
        /// </summary>
        /// <returns></returns>
        [HttpPut]
        public async Task<IActionResult> UpdateCheckIn([FromBody] UpdateCheckInRequestModel x_objRequest)
        {
            ResponseModel objRes;
            string strAccountId;

            strAccountId = User.GetUserId();
            objRes = await m_objActivitiesService.UpdateCheckIn(x_objRequest, strAccountId, HttpContext);
            return Ok(objRes);
        }

        /// <summary>
        /// Check in
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> GetListMemberInActivity([FromQuery] string x_strActivityId)
        {
            ResponseModel objRes;
            string strAccountId;

            strAccountId = User.GetUserId();
            objRes = await m_objActivitiesService.GetListMemberInActivity(x_strActivityId, strAccountId, HttpContext);
            return Ok(objRes);
        }

        /// <summary>
        /// Upload Text Font
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> UploadTextFont([FromForm] UploadTextRequestFont x_objRequest)
        {
            ResponseModel objRes;
            string strAccountId;

            strAccountId = User.GetUserId();
            objRes = await m_objActivitiesService.UploadTextFont(x_objRequest, strAccountId, HttpContext);
            return Ok(objRes);
        }

        /// <summary>
        /// Upload Certificate
        /// </summary>
        /// <param name="x_objRequest"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> UploadCertificate([FromForm] UploadCertificateRequestModel x_objRequest)
        {
            ResponseModel objRes;
            string strAccountId;

            strAccountId = User.GetUserId();
            objRes = await m_objActivitiesService.UploadCertificate(x_objRequest, strAccountId, HttpContext);
            return Ok(objRes);
        }

        /// <summary>
        /// Get List Text Font
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> GetListTextFont()
        {
            ResponseModel objRes;
            string strAccountId;

            strAccountId = User.GetUserId();
            objRes = await m_objActivitiesService.GetListTextFont(strAccountId, HttpContext);
            return Ok(objRes);
        }

        /// <summary>
        /// Update Setup Certificate
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> UpdateSetupCertificate([FromBody] SetupCertificateTextRequestModel x_objRequest)
        {
            ResponseModel objRes;
            string strAccountId;

            strAccountId = User.GetUserId();
            objRes = await m_objActivitiesService.UpdateSetupCertificate(x_objRequest, strAccountId, HttpContext);
            return Ok(objRes);
        }

        /// <summary>
        /// Delete Setup Certificate
        /// </summary>
        /// <returns></returns>
        [HttpDelete]
        public async Task<IActionResult> DeleteSetupCertificate([FromQuery] string x_strTextId)
        {
            ResponseModel objRes;
            string strAccountId;

            strAccountId = User.GetUserId();
            objRes = await m_objActivitiesService.DeleteSetupCertificate(x_strTextId, strAccountId, HttpContext);
            return Ok(objRes);
        }

        /// <summary>
        /// Delete Setup Certificate
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> GetListSetupCertificate([FromQuery] string x_strActivityId)
        {
            ResponseModel objRes;
            string strAccountId;

            strAccountId = User.GetUserId();
            objRes = await m_objActivitiesService.GetListSetupCertificate(x_strActivityId, strAccountId, HttpContext);
            return Ok(objRes);
        }

        /// <summary>
        /// Get Statistics
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> GetStatistics([FromQuery] string x_strActivityId)
        {
            ResponseModel objRes;
            string strAccountId;

            strAccountId = User.GetUserId();
            objRes = await m_objActivitiesService.GetStatistics(x_strActivityId, strAccountId, HttpContext);
            return Ok(objRes);
        }

        /// <summary>
        /// Send Certificate
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> SendCertificate([FromBody] SendCertificateRequestModel x_objRequest)
        {
            ResponseModel objRes;
            string strAccountId;

            strAccountId = User.GetUserId();
            objRes = await m_objActivitiesService.SendCertificate(x_objRequest, strAccountId, HttpContext);
            return Ok(objRes);
        }

        /// <summary>
        /// Send Certificate
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpPost]
        public async Task<IActionResult> ConfirmCertificate([FromQuery] string x_strStudentId, [FromQuery] string x_strActivityId)
        {
            ResponseModel objRes;

            objRes = await m_objActivitiesService.ConfirmCertificate(x_strStudentId, x_strActivityId, HttpContext);
            return Ok(objRes);
        }
        #endregion
    }
}
