﻿//-------------------------------------------------------------------------
// Create               : Phan Ngoc Huong
//
// Create Date          : 17/12/2022
//
// Description          : Base Controller
//
// Editing history      :
//-------------------------------------------------------------------------
using DTUSVCv3_Library;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace DTUSVCv3
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]/[action]")]
    public class BaseController : ControllerBase
    {
        private IMediator _mediator;

        protected IMediator Mediator => _mediator ??= HttpContext.RequestServices.GetService<IMediator>();

        protected void GetErrors(ResponseModel response)
        {
            response.Errors = ModelState.Values
                                        .SelectMany(v => v.Errors)
                                        .Select(e => e.ErrorMessage).ToList();
        }
    }
}
