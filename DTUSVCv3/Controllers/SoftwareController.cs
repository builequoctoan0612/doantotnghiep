﻿using DTUSVCv3_Library;
using DTUSVCv3_Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace DTUSVCv3
{
    public class SoftwareController : BaseController
    {
        #region Constructor
        public SoftwareController(ISoftwareService x_objSoftwareService)
        {
            m_objSoftwareService = x_objSoftwareService;
        }
        #endregion

        #region Fields
        private readonly ISoftwareService m_objSoftwareService;
        #endregion

        #region Methods
        /// <summary>
        /// Create Software
        /// </summary>
        /// <param name="x_objRequest"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> CreateSoftware([FromForm] CreateRequestSoftwareModel x_objRequest)
        {
            ResponseModel objRes;
            string strAccountId;

            strAccountId = User.GetUserId();
            objRes = await m_objSoftwareService.CreateSoftware(x_objRequest, strAccountId, HttpContext);
            return Ok(objRes);
        }

        /// <summary>
        /// Update Software
        /// </summary>
        /// <param name="x_objRequest"></param>
        /// <returns></returns>
        [HttpPut]
        public async Task<IActionResult> UpdateSoftware([FromForm] UpdateRequestSoftwareModel x_objRequest)
        {
            ResponseModel objRes;
            string strAccountId;

            strAccountId = User.GetUserId();
            objRes = await m_objSoftwareService.UpdateSoftware(x_objRequest, strAccountId, HttpContext);
            return Ok(objRes);
        }

        /// <summary>
        /// Add Role Software By Id
        /// </summary>
        /// <param name="x_objRequest"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> AddRoleSoftwareById([FromBody] AddRoleRequestSoftwareModel x_objRequest)
        {
            ResponseModel objRes;
            string strAccountId;

            strAccountId = User.GetUserId();
            objRes = await m_objSoftwareService.AddRoleSoftwareById(x_objRequest, strAccountId, HttpContext);
            return Ok(objRes);
        }

        /// <summary>
        /// Delete Role Software By Id
        /// </summary>
        /// <param name="x_objRequest"></param>
        /// <returns></returns>
        [HttpDelete]
        public async Task<IActionResult> DeleteRoleSoftwareById([FromBody] DeleteRoleRequestSoftwareModel x_objRequest)
        {
            ResponseModel objRes;
            string strAccountId;

            strAccountId = User.GetUserId();
            objRes = await m_objSoftwareService.DeleteRoleSoftwareById(x_objRequest, strAccountId, HttpContext);
            return Ok(objRes);
        }

        /// <summary>
        /// Delete Software
        /// </summary>
        /// <param name="x_objRequest"></param>
        /// <returns></returns>
        [HttpDelete]
        public async Task<IActionResult> DeleteSoftware([FromQuery] string x_strSoftWareId)
        {
            ResponseModel objRes;
            string strAccountId;

            strAccountId = User.GetUserId();
            objRes = await m_objSoftwareService.DeleteSoftware(x_strSoftWareId, strAccountId, HttpContext);
            return Ok(objRes);
        }

        /// <summary>
        /// Get All Software
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpGet]
        public async Task<IActionResult> GetAllSoftware()
        {
            ResponseModel objRes;
            objRes = await m_objSoftwareService.GetAllSoftware();
            return Ok(objRes);
        }

        /// <summary>
        /// Get All Software
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpGet]
        public async Task<IActionResult> GetSoftwareById([FromQuery] string x_strSoftWareId)
        {
            ResponseModel objRes;
            string strAccountId;

            strAccountId = User.GetUserId();
            objRes = await m_objSoftwareService.GetSoftwareById(x_strSoftWareId, strAccountId, HttpContext);
            return Ok(objRes);
        }
        #endregion
    }
}
