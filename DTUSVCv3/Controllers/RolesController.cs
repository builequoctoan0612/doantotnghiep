﻿using DTUSVCv3_Library;
using DTUSVCv3_Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace DTUSVCv3.Controllers
{
    public class RolesController : BaseController
    {
        #region Constructor
        public RolesController(IRoleService x_objRoleService)
        {
            m_objRoleService = x_objRoleService;
        }
        #endregion

        #region Fields
        private readonly IRoleService m_objRoleService;
        #endregion

        #region Method
        /// <summary>
        /// Get All Role
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> GetAllRole()
        {
            ResponseModel objRes;
            string strAccountId;

            strAccountId = User.GetUserId();
            objRes = await m_objRoleService.GetAllRole(strAccountId, HttpContext);
            return Ok(objRes);
        }

        /// <summary>
        /// Get All Permissions
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> GetAllPermissions()
        {
            ResponseModel objRes;
            string strAccountId;

            strAccountId = User.GetUserId();
            objRes = await m_objRoleService.GetAllPermissions(strAccountId, HttpContext);
            return Ok(objRes);
        }

        /// <summary>
        /// Create Role
        /// </summary>
        /// <param name="x_objRequest"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> CreateRole([FromBody]CreateRoleRequestModel x_objRequest)
        {
            ResponseModel objRes;
            string strAccountId;

            strAccountId = User.GetUserId();
            objRes = await m_objRoleService.CreateRole(x_objRequest, strAccountId, HttpContext);
            return Ok(objRes);
        }

        /// <summary>
        /// Update Role
        /// </summary>
        /// <param name="x_objRequest"></param>
        /// <returns></returns>
        [HttpPut]
        public async Task<IActionResult> UpdateRole([FromBody]UpdateRoleRequestModel x_objRequest)
        {
            ResponseModel objRes;
            string strAccountId;

            strAccountId = User.GetUserId();
            objRes = await m_objRoleService.UpdateRole(x_objRequest, strAccountId, HttpContext);
            return Ok(objRes);
        }
        #endregion
    }
}
