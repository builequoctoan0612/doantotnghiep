﻿using DTUSVCv3_Library;
using DTUSVCv3_Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace DTUSVCv3
{
    public class FacultysController : BaseController
    {
        #region Constructor
        public FacultysController(IFacultyService x_objFacultysService)
        {
            m_objFacultyService = x_objFacultysService;
        }
        #endregion

        #region Fields
        private readonly IFacultyService m_objFacultyService;
        #endregion

        #region Methods
        /// <summary>
        /// Get All Faculty
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpGet]
        public async Task<IActionResult> GetAllFacultys()
        {
            ResponseModel objResponse;

            objResponse = await m_objFacultyService.GetAllFacultys();
            return Ok(objResponse);
        }

        /// <summary>
        /// Create Faculty
        /// </summary>
        /// <param name="x_objRequest"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> CreateFaculty([FromBody]CreateFacultyRequestModel x_objRequest)
        {
            ResponseModel objResponse;
            string strAccountId;

            strAccountId = User.GetUserId();
            objResponse = await m_objFacultyService.CreateFaculty(x_objRequest, strAccountId, HttpContext);
            return Ok(objResponse);
        }

        /// <summary>
        /// Update Faculty
        /// </summary>
        /// <param name="x_objRequest"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> UpdateFaculty([FromBody]UpdateFacultyRequestModel x_objRequest)
        {
            ResponseModel objResponse;
            string strAccountId;

            strAccountId = User.GetUserId();
            objResponse = await m_objFacultyService.UpdateFaculty(x_objRequest, strAccountId, HttpContext);
            return Ok(objResponse);
        }

        #endregion
    }
}
