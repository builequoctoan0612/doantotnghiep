﻿using System.Security.Claims;

namespace DTUSVCv3
{
    public static class UserHelpper
    {
        public static string GetUserId(this ClaimsPrincipal x_objClaims)
        {
            try
            {
                return x_objClaims.FindFirstValue(ClaimTypes.Name);
            }
            catch
            {
                return string.Empty;
            }
        }
    }
}
