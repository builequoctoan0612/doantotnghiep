﻿//-------------------------------------------------------------------------
// Create               : Phan Ngoc Huong
//                                          
// Create Date          : 17/12/2022 
//                                          
// Description          : Request Static Folder Helper
//                                          
// Editing history      :                   
//-------------------------------------------------------------------------
using DTUSVCv3_Library;
using DTUSVCv3_MemoryData;
using Microsoft.Extensions.Primitives;

namespace DTUSVCv3
{
    public static class RequestStaticFolderHelper
    {
        /// <summary>
        /// View File Is Valid
        /// </summary>
        /// <param name="x_objHttpContex"></param>
        /// <returns></returns>
        public static bool ViewFileIsValid(this HttpContext x_objHttpContex)
        {
            StringValues objViewToken;
            string strPath;
            string strViewToken;
            bool bIsValid;

            strPath = x_objHttpContex.Request.Path.Value;
            // Public path
            if (strPath.ContainsIgnoreCase(SysFolder.LOGO_FOLDER) == true ||
                strPath.ContainsIgnoreCase(SysFolder.CAPTCHA_FOLDER) == true ||
                strPath.ContainsIgnoreCase(SysFolder.ACTIVITY_POSTER_PATH) == true ||
                strPath.ContainsIgnoreCase(SysFolder.NEWSPAPER_IMAGE) == true ||
                strPath.ContainsIgnoreCase(SysFolder.EMAIL_TEMP_IMAGE_FOLDER) == true)
            {
                return true;
            }

            // Get view token
            strViewToken = string.Empty;
            bIsValid = x_objHttpContex.Request.Query.TryGetValue("viewfiletoken", out objViewToken);
            if ((bIsValid == true) && (string.IsNullOrWhiteSpace(objViewToken) == false))
            {
                strViewToken = objViewToken;
            }

            bIsValid = LoginData.CheckLoginByViewToken(strViewToken);
            // Login folder
            if ((strPath.ContainsIgnoreCase(SysFolder.AVATAR_FOLDER) == true) ||
                (strPath.ContainsIgnoreCase(SysFolder.FONT_FILE_FOLDER) == true))
            {
                return bIsValid;
            }
            // Check Permissions
            if (strPath.ContainsIgnoreCase(SysFolder.ACTIVITY_CERTIFICATE_PATH) == true)
            {
                bIsValid = LoginData.CheckPermissions(strViewToken, (int)DTUSVCPermission.UPDATE_CERTIFICATE);
                return bIsValid;
            }
            if (strPath.ContainsIgnoreCase(SysFolder.FONT_FOLDER) == true)
            {
                bIsValid = LoginData.CheckPermissions(strViewToken, (int)DTUSVCPermission.UPDATE_CERTIFICATE_TEXT_FONT);
                return bIsValid;
            }
            if (strPath.ContainsIgnoreCase(SysFolder.EXPORT_CARD_ALL) == true)
            {
                bIsValid = LoginData.CheckPermissions(strViewToken, (int)DTUSVCPermission.DOWNLOAD_MEMBER_CARD);
                return bIsValid;
            }
            if (strPath.ContainsIgnoreCase(SysFolder.EXPORT_MEMBER_REGISTER_EVT) == true)
            {
                bIsValid = LoginData.CheckPermissions(strViewToken, (int)DTUSVCPermission.EXPORT_ACTIVITY);
                return bIsValid;
            }
            if (strPath.ContainsIgnoreCase(SysFolder.EXPORT_MEMBER_REGISTER_LIST) == true)
            {
                bIsValid = LoginData.CheckPermissions(strViewToken, (int)DTUSVCPermission.DOWNLOAD_MEMBER_INTERVIEW);
                return bIsValid;
            }
            if (strPath.ContainsIgnoreCase(SysFolder.EXPORT_MEMBER_LIST) == true)
            {
                bIsValid = LoginData.CheckPermissions(strViewToken, (int)DTUSVCPermission.DOWNLOAD_MEMBER_LIST);
                return bIsValid;
            }

            // Return false if file is not support
            return false;
        }
    }
}
