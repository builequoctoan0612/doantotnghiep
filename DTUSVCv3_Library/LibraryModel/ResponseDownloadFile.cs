﻿namespace DTUSVCv3_Library
{
    public class ResponseDownloadFile : ResponseModel
    {
        public string FilePath { get; set; }
        public string MimeType
        {
            get
            {
                string strMimeType;

                strMimeType = FilePath.GetMimeType();
                return strMimeType;
            }
        }
        public Stream FileStream
        {
            get
            {
                Stream objFileStream;

                try
                {
                    objFileStream = new FileStream(FilePath, FileMode.Open, FileAccess.Read);
                    return objFileStream;
                }
                catch
                {
                    return null;
                }
            }
        }
    }
}
