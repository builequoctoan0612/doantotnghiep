﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTUSVCv3_Library
{
    public enum DataType
    {
        RESPONSE_MODEL = 0,
        CAPTCHA_MODEL = 1,
        MY_DTU_SCHEDULE_MODEL = 2,
        RECUMENT_MEMBER_MODEL = 3
    }
    public class NotifySendModel
    {
        public NotifyData Data { get; set; }
    }
    public class NotifySendAllMess : NotifySendModel
    {
        public GUIScreen Screen { get; set; }
        public string ActionId { get; set; }
        /// <summary>
        /// 0: Remove, 1: Add, 2: modify
        /// </summary>
        public int MessageType { get; set; }
    }

    public class NotifySendOneMess : NotifySendAllMess
    {
        public string MemberId { get; set; }
    }

    public class NotifyData
    {
        public NotifyData()
        {
            CheckInData = null;
            EmailState = null;
            MemberRegistState = null;
            CaptchaData = null;
            MyDTUProcessState = null;
            MyDTURegisterState = null;
            TempData = null;
        }
        public string ItemId { get; set; }
        public object CheckInData { get; set; }
        // 
        public string EmailState { get; set; }
        public ApprovalMember MemberRegistState { get; set; }
        public CaptchaData CaptchaData { get; set; }
        public string MyDTUProcessState { get; set; }
        public string MyDTURegisterState { get; set; }
        public object TempData { get; set; }
    }

    public class CaptchaData
    {
        public string CaptchaValue { get; set; }
        public string CaptchaPath { get; set; }
    }

    public class ApprovalMember
    {
        public object Data { get; set; }
        public bool IsApproved { get; set; }
    }
}
