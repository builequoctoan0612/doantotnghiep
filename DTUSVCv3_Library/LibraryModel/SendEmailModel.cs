﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTUSVCv3_Library.LibraryModel
{
    [Serializable]
    public class SendEmailModel
    {
        public SendEmailModel()
        {
            EmailType = EmailType.DEFAULT;
            IsSendCC = true;
            DeleteFileAfterSent = false;
            AttachFilePath = new List<string>();
        }
        public string EmailRecipient { get; set; }
        public bool IsSendCC { get; set; }
        public string EmailCC { get; set; }
        public string EmailBody { get; set; }
        public string Subject { get; set; }
        public EmailType EmailType { get; set; }
        public List<string> AttachFilePath { get; set; }
        public bool DeleteFileAfterSent { get; set; }
    }

    public class EmailInterviewModel : SendEmailModel
    {
        public EmailInterviewModel()
            : base()
        {

        }
        public string RoomId { get; set; }
        public string MemberId { get; set; }
        public string MemberInRoomId { get; set; }
        public DateTime InterviewTime { get; set; }
        public event EventHandler<SendEmailInterviewArg> SendEmailResult;

        /// <summary>
        /// Send Result
        /// </summary>
        /// <param name="x_strResult"></param>
        public void SendResult(string x_strResult)
        {
            if (SendEmailResult != null)
            {
                if (string.IsNullOrWhiteSpace(x_strResult) == true)
                {
                    SendEmailResult.Invoke(null, new SendEmailInterviewArg
                    {
                        EmailType = SendEmailType.SENT,
                        RoomId = this.RoomId,
                        MemberId = this.MemberId,
                        MemberInRoomId = this.MemberInRoomId,
                        InterviewTime = this.InterviewTime
                    });
                }
            }
        }
    }

    [Serializable]
    public class SendEmailInterviewArg : EventArgs
    {
        public SendEmailType EmailType;
        public string RoomId { get; set; }
        public string MemberId { get; set; }
        public string MemberInRoomId { get; set; }
        public DateTime InterviewTime { get; set; }
    }
}
