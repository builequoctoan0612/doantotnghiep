﻿//-------------------------------------------------------------------------
// Create               : Phan Ngoc Huong
//                                          
// Create Date          : 17/12/2022 
//                                          
// Description          : Certificate Text
//                                          
// Editing history      :                   
//-------------------------------------------------------------------------

using System;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Text;
using System.IO;

namespace DTUSVCv3_Library
{
    [Serializable]
    public class CertificateText
    {
        public CertificateText(int x_nLocationX, int x_nLocationY, int x_nMaxWidth, int x_nMaxHeight, bool x_bMiddleInPicture, float x_fFontSize, string x_strFontPath,
            string x_strText, FontStyle x_eFontStyle, string x_strColor = null)
        {
            Color objColor;

            m_nLocationX = x_nLocationX;
            m_nLocationY = x_nLocationY;
            m_nMaxWidth = x_nMaxWidth;
            m_nMaxHeight = x_nMaxHeight;
            m_bMiddleInPicture = x_bMiddleInPicture;
            m_fFontSize = x_fFontSize;
            m_strText = x_strText;
            m_strFontPath = x_strFontPath;
            m_eFontStyle = x_eFontStyle;
            m_strErrorText = string.Empty;
            m_bFontError = false;
            // Convert color
            objColor = StringToColor(x_strColor);
            m_objBrush = new SolidBrush(objColor);

            // Create Point
            if ((m_nLocationX >= 0) && (m_nLocationY >= 0))
            {
                m_objPoint = new Point(m_nLocationX, m_nLocationY);
            }

            // Set default string format
            m_objStringFormat = new StringFormat();
            m_objStringFormat.LineAlignment = StringAlignment.Center;
            m_objStringFormat.Alignment = StringAlignment.Center;
        }

        public CertificateText(float x_fFontSize, string x_strFontPath, FontStyle x_eFontStyle = FontStyle.Bold)
        {
            m_fFontSize = x_fFontSize;
            m_strFontPath = x_strFontPath;
            m_eFontStyle = x_eFontStyle;
        }

        #region Fields
        private bool m_bFontError;
        private bool m_bMiddleInPicture;
        private int m_nLocationX;
        private int m_nLocationY;
        private int m_nMaxWidth;
        private int m_nMaxHeight;
        private StringAlignment m_eVertical;
        private StringAlignment m_eHorizontal;
        private float m_fFontSize;
        private string m_strText;
        private string m_strFontPath;
        private string m_strFontName;
        private string m_strErrorText;
        private string m_strColor;

        private FontStyle m_eFontStyle;
        private Point m_objPoint;
        private Font m_objFont;
        private Brush m_objBrush;
        private StringFormat m_objStringFormat;
        #endregion

        #region properties
        public string StrText
        {
            get
            {
                return m_strText;
            }
            set
            {
                m_strText = value;
            }
        }

        public string FontPath
        {
            get
            {
                return m_strFontPath;
            }
        }

        public string FontName
        {
            get
            {
                return m_strFontName;
            }
        }

        public string ErrorText
        {
            get
            {
                return m_strErrorText;
            }
        }

        public bool FontError
        {
            get
            {
                return m_bFontError;
            }
        }

        public bool MiddleInPicture
        {
            get
            {
                return m_bMiddleInPicture;
            }
            set
            {
                m_bMiddleInPicture = value;
            }
        }

        public int MaxWidth
        {
            get
            {
                return m_nMaxWidth;
            }
            set
            {
                m_nMaxWidth = value;
            }
        }

        public int MaxHeight
        {
            get
            {
                return m_nMaxHeight;
            }
            set
            {
                m_nMaxHeight = value;
            }
        }

        public string TextColor
        {
            set
            {
                Color objColor;
                m_strColor = value;
                objColor = StringToColor(m_strColor);
                m_objBrush = new SolidBrush(objColor);
            }
        }

        public Font Font
        {
            get
            {
                m_objFont = GetFont(m_strFontPath, m_fFontSize, m_eFontStyle);
                return m_objFont;
            }
        }

        public Point Point
        {
            get
            {
                return m_objPoint;
            }
            set
            {
                m_objPoint = value;
            }
        }

        public float FontSize
        {
            get
            {
                return m_fFontSize;
            }
            set
            {
                m_fFontSize = value;
            }
        }

        public StringAlignment Vertical
        {
            set
            {
                m_eVertical = value;
            }
            get
            {
                return m_eVertical;
            }
        }

        public StringAlignment Horizontal
        {
            set
            {
                m_eHorizontal = value;
            }
            get
            {
                return m_eHorizontal;
            }
        }

        public Brush TextBrush
        {
            get
            {
                return m_objBrush;
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Set String Format
        /// </summary>
        public void SetStringFormat(StringAlignment x_eStrLineAlignment, StringAlignment x_eStrAlignment)
        {
            m_objStringFormat = new StringFormat();
            m_objStringFormat.LineAlignment = x_eStrLineAlignment;
            m_objStringFormat.Alignment = x_eStrAlignment;
        }

        /// <summary>
        /// Create font
        /// </summary>
        private Font GetFont(string x_strFontFilePath, float x_fSize, FontStyle x_objStyle)
        {
            PrivateFontCollection objCollection;
            Font objFont;

            try
            {
                if((string.IsNullOrWhiteSpace(x_strFontFilePath) == true) || (File.Exists(x_strFontFilePath) == false))
                {
                    m_strErrorText = "Font file is not exist.";
                    m_bFontError = true;
                    return new Font("Arial", m_fFontSize);
                }
                // Create font
                objCollection = new PrivateFontCollection();
                objCollection.AddFontFile(x_strFontFilePath);
                if(objCollection.Families.Length> 0 )
                {
                    objFont = new Font(objCollection.Families[0], x_fSize, x_objStyle);
                    m_strFontName = objFont.Name;
                    m_bFontError = false;
                    m_strErrorText = string.Empty;
                    return objFont;
                }
                m_strErrorText = "Read font failed.";
                m_bFontError = true;
                return new Font("Arial", m_fFontSize); ;
            }
            catch
            {
                m_strErrorText = "Read font failed."; 
                m_bFontError = true;
                return new Font("Arial", m_fFontSize); ;
            }
        }

        /// <summary>
        /// Resize Font
        /// </summary>
        /// <param name="x_fNewSize"></param>
        public void ResizeFont(float x_fNewSize)
        {
            m_fFontSize = x_fNewSize;
        }

        /// <summary>
        /// String To Color
        /// </summary>
        private Color StringToColor(string x_strColorStr)
        {
            TypeConverter objTypeConverter;
            Color objColor;

            try
            {
                if(string.IsNullOrWhiteSpace(x_strColorStr) == true)
                {
                    return Color.Black;
                }

                objTypeConverter = TypeDescriptor.GetConverter(typeof(Color));
                objColor = (Color)objTypeConverter.ConvertFromString(x_strColorStr);
            }
            catch
            {
                // Get color defauult
                objColor = Color.Black;
            }

            return objColor;
        }

        public CertificateText Clone()
        {
            try
            {
                return (CertificateText)this.MemberwiseClone();
            }
            catch
            {
                return this;
            }
        }
        #endregion
    }

    public class DrawTextInfo
    {
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string FacultyName { get; set; }
        public string ClassName { get; set; }
        public string StudentId { get; set; }

        public string GetFullName()
        {
            return string.Format("{0} {1}", FirstName, LastName);
        }
    }

    public class DrawTextSetting
    {
        #region Properties
        public float FontSize { get; set; }
        public float LocationX { get; set; }
        public float LocationY { get; set; }
        public int MaxWidth { get; set; }
        public int MaxHeight { get; set; }
        public string FontPath { get; set; }
        public int Vertical { get; set; }
        public int Horizontal { get; set; }
        public string Color { get; set; }
        public string DefaultText { get; set; }
        public int ValueType { get; set; }
        public string FontStyleStr { get; set; }
        public TextType TextType
        {
            get
            {
                TextType eTextType;

                eTextType = ValueType.ToEnum(TextType.DEFAULT_TEXT);
                return eTextType;
            }
        }

        public FontStyle FontStyle
        {
            get
            {
                FontStyle eFontStyle;

                eFontStyle = FontStyleStr.ToEnum(FontStyle.Regular);
                return eFontStyle;
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Get Font
        /// </summary>
        /// <returns></returns>
        public FontFamily GetFont()
        {
            FontFamily objFontFamily;

            TextFontHelper.TryGetFont(FontPath, out objFontFamily);
            return objFontFamily;
        }

        /// <summary>
        /// Get String Format
        /// </summary>
        /// <returns></returns>
        public StringFormat GetStringFormat()
        {
            StringFormat objStringFormat;

            objStringFormat = new StringFormat();

            switch (Vertical)
            {
                case 0:
                    objStringFormat.LineAlignment = StringAlignment.Near;
                    break;
                case 2:
                    objStringFormat.LineAlignment = StringAlignment.Far;
                    break;
                default:
                    objStringFormat.LineAlignment = StringAlignment.Center;
                    break;
            }

            switch (Horizontal)
            {
                case 0:
                    objStringFormat.Alignment = StringAlignment.Near;
                    break;
                case 1:
                    objStringFormat.Alignment = StringAlignment.Far;
                    break;
                default:
                    objStringFormat.Alignment = StringAlignment.Center;
                    break;
            }
            return objStringFormat;
        }

        /// <summary>
        /// Get Frame Point
        /// </summary>
        public PointF[] GetFramePoint(float x_ImageWidth, bool x_bMiddleInPicture)
        {
            PointF[] arrFramePoint;
            PointF objFramePoint;
            PointF objTextPoint;
            float fPointTempX;

            objTextPoint = new PointF(LocationX, LocationY);
            arrFramePoint = new PointF[5];
            if (x_bMiddleInPicture == true)
            {
                fPointTempX = (x_ImageWidth / 2) - (MaxWidth / 2);
                // Point 1
                objFramePoint = new PointF(fPointTempX, objTextPoint.Y);
                arrFramePoint[0] = objFramePoint;
                // Point 2
                objFramePoint = new PointF((fPointTempX + MaxWidth), objTextPoint.Y);
                arrFramePoint[1] = objFramePoint;
                // Point 3
                objFramePoint = new PointF((fPointTempX + MaxWidth), (objTextPoint.Y + MaxHeight));
                arrFramePoint[2] = objFramePoint;
                // Point 4
                objFramePoint = new PointF(fPointTempX, (objTextPoint.Y + MaxHeight));
                arrFramePoint[3] = objFramePoint;
                // Point 5
                objFramePoint = new PointF(fPointTempX, objTextPoint.Y);
                arrFramePoint[4] = objFramePoint;
            }
            else
            {
                // Point 1
                objFramePoint = new PointF(objTextPoint.X, objTextPoint.Y);
                arrFramePoint[0] = objFramePoint;
                // Point 2
                objFramePoint = new PointF((objTextPoint.X + MaxWidth), objTextPoint.Y);
                arrFramePoint[1] = objFramePoint;
                // Point 3
                objFramePoint = new PointF((objTextPoint.X + MaxWidth), (objTextPoint.Y + MaxHeight));
                arrFramePoint[2] = objFramePoint;
                // Point 4
                objFramePoint = new PointF(objTextPoint.X, (objTextPoint.Y + MaxHeight));
                arrFramePoint[3] = objFramePoint;
                // Point 5
                objFramePoint = new PointF(objTextPoint.X, objTextPoint.Y);
                arrFramePoint[4] = objFramePoint;
            }

            return arrFramePoint;
        }

        /// <summary>
        /// Get Color
        /// </summary>
        /// <returns></returns>
        public Color GetColor()
        {
            Color objColor;

            TextFontHelper.TryParseStringToColor(Color, out objColor);
            return objColor;
        }
        #endregion
    }
}
