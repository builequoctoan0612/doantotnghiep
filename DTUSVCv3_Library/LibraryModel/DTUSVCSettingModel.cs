﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTUSVCv3_Library
{
    public class DTUSVCSettingModel
    {
        public DateTime ScoringStartDate { get; set; }
        public int SemesterId { get; set; }
        public int YearId { get; set; }
    }
}
