﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTUSVCv3_Library.LibraryModel
{
    public class ExportMemberRegistEvtBase
    {
        public string MemberId { get; set; }
        public string StudentId { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public double TotalScore { get; set; }
        public bool IsMember { get; set; }
    }
    public class ExportMemberRegistEvt : ExportMemberRegistEvtBase
    {
        public string Faculty { get; set; }
        public string ClassName { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public string FacebookPath { get; set; }
    }

    public class ExportMemberScore
    {
        public string MemberId { get; set; }
        public string StudentId { get; set; }
        public List<ScoreInfo> ScoreData { get; set; }
    }

    public class ScoreInfo
    {
        public DateTime EntryTime { get; set; }
        public DateTime TimeToLeave { get; set; }
        public string ManagerName { get; set; }
        public double Score { get; set; }
    }
}
