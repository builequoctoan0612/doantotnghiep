﻿using Newtonsoft.Json;

namespace DTUSVCv3_Library
{
    [Serializable]
    public class ResponseModel
    {
        public ResponseModel()
        {
            Errors = new List<string>();
            IsSuccsess = true;
            DataCount = 0;
        }

        [JsonProperty("message")]
        public string Message { get; set; }

        [JsonProperty("isSuccsess")]
        public bool IsSuccsess { get; set; }


        [JsonProperty("errors")]
        public List<string> Errors { get; set; }


        [JsonProperty("errorsCode")]
        public int ErrorsCode { get; set; }


        [JsonProperty("dataValue")]
        public object DataValue { get; set; }


        [JsonProperty("dataCount")]
        public int DataCount { get; set; }
    }

    public static class ResponseManager
    {
        /// <summary>
        /// Get Error Resp
        /// </summary>
        public static ResponseModel GetErrorResp(string x_strErrorMess, ErrorCode x_eErrorCode)
        {
            ResponseModel objResponseModel;

            objResponseModel = new ResponseModel();
            objResponseModel.IsSuccsess = false;
            objResponseModel.Errors.Add(x_strErrorMess);
            objResponseModel.ErrorsCode = (int)x_eErrorCode;
            return objResponseModel;
        }

        /// <summary>
        /// Get Error Resp
        /// </summary>
        public static ResponseDownloadFile GetDownloadErrorResp(string x_strErrorMess, ErrorCode x_eErrorCode)
        {
            ResponseDownloadFile objResponseModel;

            objResponseModel = new ResponseDownloadFile();
            objResponseModel.IsSuccsess = false;
            objResponseModel.Errors.Add(x_strErrorMess);
            objResponseModel.ErrorsCode = (int)x_eErrorCode;
            return objResponseModel;
        }

        /// <summary>
        /// Get Succsess Resp
        /// </summary>
        public static ResponseModel GetSuccsessResp(string x_strMessage, object x_objDataValue = null, int x_nDataCount = 1)
        {
            ResponseModel objResponseModel;

            objResponseModel = new ResponseModel();
            objResponseModel.Message = x_strMessage;
            objResponseModel.DataValue = x_objDataValue;
            objResponseModel.DataCount = x_objDataValue == null ? 0 : x_nDataCount;
            return objResponseModel;
        }
    }
}
