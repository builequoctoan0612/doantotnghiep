﻿//-------------------------------------------------------------------------
// Create               : Phan Ngoc Huong
//
// Create Date          : 17/12/2022
//
// Description          : OS cmd Helper
//
// Editing history      :
//-------------------------------------------------------------------------
using System.Diagnostics;
using System.Runtime.InteropServices;

namespace DTUSVCv3_Library
{
    public static class OSCmdHelper
    {
        /// <summary>
        /// Is Linux OS
        /// </summary>
        public static bool IsLinuxOS()
        {
            if (RuntimeInformation.IsOSPlatform(OSPlatform.Linux) == true)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Run Cmd
        /// </summary>
        public static void RestartMongodbCmd()
        {
            bool bIsLinuxOS;
            string strCommand;

            try
            {
                bIsLinuxOS = IsLinuxOS();
                if (bIsLinuxOS == true)
                {
                    strCommand = "service mongod restart";
                    StartCommandInLinux(strCommand);
                }
                else
                {
                    strCommand = "net start MongoDB";
                    StartCommandInWindows(strCommand);
                }
            }
            catch { }
        }

        /// <summary>
        /// Run Cmd
        /// </summary>
        public static void RestartNetworkCmd()
        {
            bool bIsLinuxOS;
            string strCommand;

            try
            {
                bIsLinuxOS = IsLinuxOS();
                if (bIsLinuxOS == true)
                {
                    strCommand = "sudo systemctl restart systemd-networkd";
                    StartCommandInLinux(strCommand);
                }
                else
                {
                    strCommand = "netsh winsock reset";
                    StartCommandInWindows(strCommand);
                }
            }
            catch { }
        }

        /// <summary>
        /// Start Command In Linux
        /// </summary>
        private static void StartCommandInLinux(string x_strCommand)
        {
            Process objProc = new Process();

            objProc.StartInfo.FileName = "/bin/bash";
            objProc.StartInfo.Arguments = "-c \" " + x_strCommand + " \"";
            objProc.StartInfo.UseShellExecute = false;
            objProc.StartInfo.RedirectStandardOutput = true;
            objProc.Start();
        }

        /// <summary>
        /// Start Command In Windows
        /// </summary>
        private static void StartCommandInWindows(string x_strCommand)
        {
            Process objProc = new Process();

            objProc.StartInfo.FileName = "C:\\Windows\\system32\\cmd.exe";
            objProc.StartInfo.Verb = "runas";
            objProc.StartInfo.Arguments = x_strCommand;
            objProc.StartInfo.UseShellExecute = false;
            objProc.StartInfo.RedirectStandardOutput = true;
            objProc.Start();
        }
    }
}
