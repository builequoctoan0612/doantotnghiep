﻿using System;
using System.IO;

namespace DTUSVCv3_Library
{
    public static class LogHelper
    {
        #region Const
        private const string DATE_FORMAT_FILE_NAME = "ddMMyyyy";
        private const string DATE_TIME_FORMAT = "dd/MM/yyyy HH:mm:ss.fff";
        #endregion

        /// <summary>
        /// Write log
        /// </summary>
        /// <param name="x_enumLogType">Type log using LogTypeEnum</param>
        /// <param name="x_strIP">IP Client</param>
        /// <param name="x_strFullName">Full name client or Passing guests</param>
        /// <param name="x_strContent">Log content</param>
        /// <param name="x_strSource">Class name</param>
        public static void WriteLog(LogTypeEnum x_enumLogType, string x_strIP, string x_strFullName, string x_strContent, string x_strSource)
        {
            string strFolderPath;
            string strFileName;
            string strFilePath;
            string strMessage;
            DateTime dtLogTime;

            try
            {
                // Get date time now
                dtLogTime = DateTimeHelper.GetCurrentDateTimeVN();
                //
                switch (x_enumLogType)
                {
                    case LogTypeEnum.DEFAULT:
                        strFolderPath = Path.Combine(SysFolder.APP_PATH, SysFolder.DEFAULT_LOG_PATH);
                        strFileName = string.Format(SysFile.DEFAULT, dtLogTime.ToString(DATE_FORMAT_FILE_NAME));
                        break;
                    case LogTypeEnum.ERRORS:
                        strFolderPath = Path.Combine(SysFolder.APP_PATH, SysFolder.ERROR_LOG_PATH);
                        strFileName = string.Format(SysFile.ERROR, dtLogTime.ToString(DATE_FORMAT_FILE_NAME));
                        break;
                    case LogTypeEnum.FORGOT_PASSWORD:
                        strFolderPath = Path.Combine(SysFolder.APP_PATH, SysFolder.FORGOT_PASSWORD_LOG_PATH);
                        strFileName = string.Format(SysFile.FORGOT_PASSWORD, dtLogTime.ToString(DATE_FORMAT_FILE_NAME));
                        break;
                    case LogTypeEnum.LOGIN:
                        strFolderPath = Path.Combine(SysFolder.APP_PATH, SysFolder.LOGIN_LOG_PATH);
                        strFileName = string.Format(SysFile.LOGIN, dtLogTime.ToString(DATE_FORMAT_FILE_NAME));
                        break;
                    case LogTypeEnum.RESET_PASSWORD:
                        strFolderPath = Path.Combine(SysFolder.APP_PATH, SysFolder.RESET_PASSWORD_LOG_PATH);
                        strFileName = string.Format(SysFile.RESET_PASSWORD, dtLogTime.ToString(DATE_FORMAT_FILE_NAME));
                        break;
                    case LogTypeEnum.WARNING:
                        strFolderPath = Path.Combine(SysFolder.APP_PATH, SysFolder.WARNING_LOG_PATH);
                        strFileName = string.Format(SysFile.WARNING, dtLogTime.ToString(DATE_FORMAT_FILE_NAME));
                        break;
                    default:
                        // No processing
                        return;
                }

                strFilePath = Path.Combine(strFolderPath, strFileName);
                // Create folder if folder is not exists
                if (Directory.Exists(strFolderPath) == false)
                {
                    Directory.CreateDirectory(strFolderPath);
                }

                if(string.IsNullOrWhiteSpace(x_strIP) == true)
                {
                    x_strIP = ErrorConstant.UNKNOWN;
                }
                if(string.IsNullOrWhiteSpace(x_strFullName) == true)
                {
                    x_strFullName = ErrorConstant.UNKNOWN;
                }

                strMessage = string.Format("{0} - {1}: {2}\t{3}\t{4}.", x_strSource, dtLogTime.ToString(DATE_TIME_FORMAT), x_strIP, x_strFullName, x_strContent);
                // Create file if file is not EXists
                if (File.Exists(strFilePath) == false)
                {
                    using (FileStream objFileStream = File.Create(strFilePath))
                    {
                        using (StreamWriter objstWrite = new StreamWriter(objFileStream))
                        {
                            // Write log
                            objstWrite.WriteLine(strMessage);
                            objstWrite.Close();
                        }
                    }
                }
                else
                {
                    using (StreamWriter objstWrite = new StreamWriter(strFilePath, true))
                    {
                        // Write log
                        objstWrite.WriteLine(strMessage);
                        objstWrite.Close();
                    }
                }
            }
            catch { }
        }
    }
}
