﻿namespace DTUSVCv3_Library
{
    public static class SettingHelper
    {
        public static DateTime ScoringStartDate;
        public static int SemesterId { get; set; }
        public static int YearId { get; set; }

        /// <summary>
        /// Get Scoring Start Date
        /// </summary>
        public static void GetScoringStartDate()
        {
            string strFilePath;
            string strFileContent;
            DTUSVCSettingModel objDTUSVCSetting;

            strFilePath = FileHelper.GetFilePath(SysFolder.WEB_SETTING_FOLDER, DataFilePath.DTUSVC_SETTING, true);
            strFileContent = FileHelper.ReadTextFile(strFilePath);

            objDTUSVCSetting = ObjHelper.JSONToObject<DTUSVCSettingModel>(strFileContent);
            if (objDTUSVCSetting != null)
            {
                ScoringStartDate = objDTUSVCSetting.ScoringStartDate;
                SemesterId = objDTUSVCSetting.SemesterId;
                YearId = objDTUSVCSetting.YearId;
            }
            else
            {
                ScoringStartDate = DateTimeHelper.GetCurrentDateTimeVN();
                SemesterId = 83;
                YearId = 82;
                SaveSetting();
            }
        }

        /// <summary>
        /// Save Setting
        /// </summary>
        public static void SaveSetting()
        {
            string strFilePath;
            string strFileContent;
            DTUSVCSettingModel objDTUSVCSetting;

            try
            {
                objDTUSVCSetting = new DTUSVCSettingModel();
                objDTUSVCSetting.ScoringStartDate = ScoringStartDate;
                objDTUSVCSetting.SemesterId = SemesterId;
                objDTUSVCSetting.YearId = YearId;

                strFileContent = objDTUSVCSetting.ToJSON();
                // Create file path
                strFilePath = FileHelper.GetFilePath(SysFolder.WEB_SETTING_FOLDER, DataFilePath.DTUSVC_SETTING, true);
                // Save file
                if (File.Exists(strFilePath) == true)
                {
                    File.Delete(strFilePath);
                }

                using (FileStream objFileStream = File.Create(strFilePath))
                {
                    using (StreamWriter objstWrite = new StreamWriter(objFileStream))
                    {
                        objstWrite.WriteLine(strFileContent);
                        objstWrite.Close();
                    }
                }
            }
            catch
            {
                // No need process
            }
        }
    }
}
