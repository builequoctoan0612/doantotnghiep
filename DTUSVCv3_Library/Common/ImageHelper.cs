﻿using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Drawing.Text;

namespace DTUSVCv3_Library
{
    public static class ImageHelper
    {
        #region Type
        public const int AVATAR_IMAGE = 1;
        public const int POSTER_IMAGE = 2;
        public const int NEWS_IMAGE = 3;
        #endregion

        /// <summary>
        /// Image Resize 
        /// </summary>
        private static Bitmap ImageResize(this Bitmap x_objImage, Size x_objNewSize)
        {
            Bitmap objImageTemp;
            double dXFactor;
            double dYFactor;
            double dFractionX;
            double dFractionY;
            double dOneMinusX;
            double dOneMinusY;
            int nCeilX;
            int nCeilY;
            int nFloorX;
            int nFloorY;
            int nWidth;
            int nHeight;
            Color objColowr1;
            Color objColowr2;
            Color objColowr3;
            Color objColowr4;
            byte byteRed;
            byte byteGreen;
            byte byteBlue;
            byte byteTmp1;
            byte byteTmp2;


            try
            {
                nWidth = x_objNewSize.Width;
                nHeight = x_objNewSize.Height;

                // Skip if size = 0
                if ((nHeight <= 0) || (nWidth <= 0))
                {
                    return x_objImage;
                }

                objImageTemp = (Bitmap)x_objImage.Clone();
                x_objImage = new Bitmap(nWidth, nHeight, objImageTemp.PixelFormat);

                dXFactor = (double)objImageTemp.Width / (double)nWidth;
                dYFactor = (double)objImageTemp.Height / (double)nHeight;

                for (int nLocationX = 0; nLocationX < x_objImage.Width; ++nLocationX)
                {
                    for (int nLocationY = 0; nLocationY < x_objImage.Height; ++nLocationY)
                    {
                        nFloorX = (int)Math.Floor(nLocationX * dXFactor);
                        nFloorY = (int)Math.Floor(nLocationY * dYFactor);
                        nCeilX = nFloorX + 1;
                        if (nCeilX >= objImageTemp.Width) nCeilX = nFloorX;
                        nCeilY = nFloorY + 1;
                        if (nCeilY >= objImageTemp.Height) nCeilY = nFloorY;
                        dFractionX = nLocationX * dXFactor - nFloorX;
                        dFractionY = nLocationY * dYFactor - nFloorY;
                        dOneMinusX = 1.0 - dFractionX;
                        dOneMinusY = 1.0 - dFractionY;

                        objColowr1 = objImageTemp.GetPixel(nFloorX, nFloorY);
                        objColowr2 = objImageTemp.GetPixel(nCeilX, nFloorY);
                        objColowr3 = objImageTemp.GetPixel(nFloorX, nCeilY);
                        objColowr4 = objImageTemp.GetPixel(nCeilX, nCeilY);

                        // Blue
                        byteTmp1 = (byte)(dOneMinusX * objColowr1.B + dFractionX * objColowr2.B);
                        byteTmp2 = (byte)(dOneMinusX * objColowr3.B + dFractionX * objColowr4.B);
                        byteBlue = (byte)(dOneMinusY * (double)(byteTmp1) + dFractionY * (double)(byteTmp2));

                        // Green
                        byteTmp1 = (byte)(dOneMinusX * objColowr1.G + dFractionX * objColowr2.G);
                        byteTmp2 = (byte)(dOneMinusX * objColowr3.G + dFractionX * objColowr4.G);
                        byteGreen = (byte)(dOneMinusY * (double)(byteTmp1) + dFractionY * (double)(byteTmp2));

                        // Red
                        byteTmp1 = (byte)(dOneMinusX * objColowr1.R + dFractionX * objColowr2.R);
                        byteTmp2 = (byte)(dOneMinusX * objColowr3.R + dFractionX * objColowr4.R);
                        byteRed = (byte)(dOneMinusY * (double)(byteTmp1) + dFractionY * (double)(byteTmp2));

                        x_objImage.SetPixel(nLocationX, nLocationY, Color.FromArgb(255, byteRed, byteGreen, byteBlue));
                    }
                }
                return x_objImage;
            }
            catch
            {
                return x_objImage;
            }
        }

        /// <summary>
        /// Crop Image
        /// </summary>
        /// <param name="x_objImage"></param>
        /// <param name="x_nType"></param>
        /// <param name="x_strError"></param>
        /// <returns></returns>
        public static Bitmap CropImage(this Bitmap x_objImage, int x_nType, out string x_strError)
        {
            Size objAvatarSize = new Size(950, 1320);
            Size objPosterSize = new Size(1280, 720);
            Size objNewSize;
            Size objCurrentSize;
            Image objImage;

            x_strError = string.Empty;
            // Get current ratio
            switch (x_nType)
            {
                case AVATAR_IMAGE:
                    objCurrentSize = objAvatarSize;
                    break;
                case POSTER_IMAGE:
                case NEWS_IMAGE:
                    objCurrentSize = objPosterSize;
                    break;
                default:
                    x_strError = ErrorConstant.IMAGE_TYPE_INVALID;
                    return null;
            }

            if (x_nType != NEWS_IMAGE)
            {
                // Get new size
                objNewSize = GetNewSize(x_objImage, x_nType, out x_strError);
                if (objNewSize.Width == 0)
                {
                    return null;
                }
                // Crop image
                objImage = CropImage(x_objImage, objNewSize);
            }
            else
            {
                objImage = x_objImage;
                objCurrentSize = GetNewsImageSize((Bitmap)objImage);
            }
            // Resize image
            objImage = ImageResize((Bitmap)objImage, objCurrentSize);
            return (Bitmap)objImage;
        }

        /// <summary>
        /// Get New Size Image
        /// </summary>
        /// <param name="x_objCurrentImage"></param>
        /// <returns></returns>
        private static Size GetNewsImageSize(Bitmap x_objCurrentImage)
        {
            const int MAX = 1280;
            Size objCurrentSize;
            int nImageWidth;
            int nImageHeight;
            double dRatio;

            nImageHeight = x_objCurrentImage.Height;
            nImageWidth = x_objCurrentImage.Width;
            if ((nImageWidth < MAX) && (nImageHeight < MAX))
            {
                objCurrentSize = new Size(nImageWidth, nImageHeight);
                return objCurrentSize;
            }

            if (nImageHeight > MAX)
            {
                dRatio = (float)nImageHeight / MAX;
            }
            else
            {
                dRatio = (float)nImageWidth / MAX;
            }

            nImageWidth = (int)(nImageWidth / dRatio);
            nImageHeight = (int)(nImageHeight / dRatio);
            objCurrentSize = new Size(nImageWidth, nImageHeight);
            return objCurrentSize;
        }

        /// <summary>
        /// Get Size
        /// </summary>
        /// <param name="x_objImage"></param>
        /// <param name="x_nType"></param>
        /// <param name="x_strError"></param>
        /// <returns></returns>
        private static Size GetNewSize(Bitmap x_objImage, int x_nType, out string x_strError)
        {
            Size objAvatarRatio = new Size(950, 1320);
            Size objPosterRatio = new Size(1280, 720);
            Size objCurrentRatio;
            int nNewWidth;
            int nNewHeight;
            double dRatio;
            bool bHeightIsGreaterThanWidth;

            x_strError = string.Empty;
            // Get current ratio
            switch (x_nType)
            {
                case AVATAR_IMAGE:
                    objCurrentRatio = objAvatarRatio;
                    break;
                case POSTER_IMAGE:
                    objCurrentRatio = objPosterRatio;
                    break;
                default:
                    x_strError = ErrorConstant.IMAGE_TYPE_INVALID;
                    return new Size(0, 0);
            }
            // Check image size
            dRatio = 0;
            if (x_objImage.Size.Width < objCurrentRatio.Width || x_objImage.Size.Height < objCurrentRatio.Height)
            {
                x_strError = ErrorConstant.IMAGE_SIZE_INVALIE;
                return new Size(0, 0);
            }

            bHeightIsGreaterThanWidth = (x_objImage.Size.Height / objCurrentRatio.Height) > (x_objImage.Size.Width / objCurrentRatio.Width);
            if (bHeightIsGreaterThanWidth == true)
            {
                dRatio = (float)x_objImage.Size.Width / objCurrentRatio.Width;
            }
            else
            {
                dRatio = (float)x_objImage.Size.Height / objCurrentRatio.Height;
            }
            if ((x_objImage.Size.Height / dRatio) < 1)
            {
                dRatio = (float)x_objImage.Size.Height / objCurrentRatio.Height;
            }
            if ((x_objImage.Size.Width / dRatio) < 1)
            {
                dRatio = (float)x_objImage.Size.Width / objCurrentRatio.Width;
            }

            nNewWidth = (int)((float)objCurrentRatio.Width * dRatio);
            nNewHeight = (int)((float)objCurrentRatio.Height * dRatio);
            return new Size(nNewWidth, nNewHeight);
        }

        /// <summary>
        /// Crop Image
        /// </summary>
        private static Image CropImage(Image x_objImage, Size x_objNewSize)
        {
            Bitmap objTmpImage;
            Bitmap objTarget;
            Rectangle objCropRect;
            int nX;
            int nY;
            int nSurPlus;

            try
            {
                // Skip if intput invalid
                if ((x_objImage == null) ||
                    (x_objNewSize == null) ||
                    (x_objNewSize.Width <= 0) ||
                    (x_objNewSize.Height <= 0) ||
                    (x_objNewSize.Height > x_objImage.Height) ||
                    (x_objNewSize.Width > x_objImage.Width))
                {
                    return null;
                }

                nX = 0;
                nY = 0;
                objTmpImage = (Bitmap)x_objImage.Clone();
                if (((objTmpImage.Width - x_objNewSize.Width) == 0) && ((objTmpImage.Height - x_objNewSize.Height) > 0))
                {
                    nSurPlus = objTmpImage.Height - x_objNewSize.Height;
                    nY = nSurPlus / 2;
                }
                else
                {
                    if (((objTmpImage.Height - x_objNewSize.Height) == 0) && ((objTmpImage.Width - x_objNewSize.Width) > 0))
                    {
                        nSurPlus = objTmpImage.Width - x_objNewSize.Width;
                        nX = nSurPlus / 2;
                    }
                    else
                    {
                        return x_objImage;
                    }
                }

                // Create Crop Rect
                objCropRect = new Rectangle(nX, nY, x_objNewSize.Width, x_objNewSize.Height);
                // Create new Bitmap
                objTarget = new Bitmap(objCropRect.Width, objCropRect.Height);

                using (Graphics objGraphics = Graphics.FromImage(objTarget))
                {
                    // Redraw image
                    objGraphics.DrawImage(objTmpImage, new Rectangle(0, 0, x_objNewSize.Width, x_objNewSize.Height),
                                     objCropRect,
                                     GraphicsUnit.Pixel);
                }
                return objTarget;
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Create Certificate
        /// </summary>
        public static bool CreateCertificate(Bitmap x_bmCertificate,
                                            List<DrawTextSetting> x_lstCertificateText,
                                            string x_strFilePath,
                                            bool x_bIsDemo = true,
                                            DrawTextInfo x_DrawTextInfo = null)
        {
            Bitmap bmCertificateClone;
            Graphics objGraphics;
            FileStream objFS;
            Font objFont;
            Brush objBrush;
            StringFormat objFormat;
            ImageFormat objImageFormat;
            SizeF objTextSize;
            Rectangle objFillRect;
            float fFontSizeNomal;
            string strTextValue;
            string strFolderPath;

            try
            {
                // Clone Certificate
                bmCertificateClone = (Bitmap)x_bmCertificate.Clone();
                x_bmCertificate.Dispose();

                objGraphics = Graphics.FromImage(bmCertificateClone);
                objGraphics.TextRenderingHint = TextRenderingHint.AntiAliasGridFit;
                objGraphics.SmoothingMode = SmoothingMode.HighSpeed;
                foreach (DrawTextSetting objCertificateText in x_lstCertificateText)
                {
                    // draw frame
                    objFillRect = new Rectangle((int)objCertificateText.LocationX, (int)objCertificateText.LocationY, objCertificateText.MaxWidth, objCertificateText.MaxHeight);
                    if (x_bIsDemo == true)
                    {
                        objGraphics.DrawRectangle(Pens.Red, objFillRect);
                        strTextValue = objCertificateText.TextType.DisplayName();
                    }
                    else
                    {
                        switch (objCertificateText.TextType)
                        {
                            case TextType.FULL_NAME:
                                strTextValue = x_DrawTextInfo.GetFullName();
                                break;
                            case TextType.FIRST_NAME:
                                strTextValue = x_DrawTextInfo.FirstName;
                                break;
                            case TextType.LAST_NAME:
                                strTextValue = x_DrawTextInfo.LastName;
                                break;
                            case TextType.STUDENT_ID:
                                strTextValue = x_DrawTextInfo.StudentId;
                                break;
                            case TextType.FACULTY_NAME:
                                strTextValue = x_DrawTextInfo.FacultyName;
                                break;
                            case TextType.CLASS_NAME:
                                strTextValue = x_DrawTextInfo.ClassName;
                                break;
                            default:
                                strTextValue = objCertificateText.DefaultText;
                                break;
                        }
                    }

                    fFontSizeNomal = objCertificateText.FontSize;
                    objFont = new Font(objCertificateText.GetFont(), fFontSizeNomal, objCertificateText.FontStyle);
                    // When the name is long beyond the margin of the certificate, reduce the font
                    do
                    {
                        objTextSize = objGraphics.MeasureString(strTextValue, objFont);
                        if ((objCertificateText.MaxWidth < objTextSize.Width) || (objCertificateText.MaxHeight < objTextSize.Height))
                        {
                            fFontSizeNomal--;
                            objFont = new Font(objCertificateText.GetFont(), fFontSizeNomal, objCertificateText.FontStyle);
                        }
                    } while ((objCertificateText.MaxWidth < objTextSize.Width) || (objCertificateText.MaxHeight < objTextSize.Height));

                    objBrush = new SolidBrush(objCertificateText.GetColor());
                    objFormat = objCertificateText.GetStringFormat();
                    objGraphics.DrawString(strTextValue, objFont, objBrush, (RectangleF)objFillRect, objFormat);
                }
                // Save file to Disk
                strFolderPath = Path.GetDirectoryName(x_strFilePath);
                if (Directory.Exists(strFolderPath) == false)
                {
                    Directory.CreateDirectory(strFolderPath);
                }

                using (objFS = new FileStream(x_strFilePath, FileMode.Create))
                {
                    objImageFormat = GetImageFormat(x_strFilePath);
                    bmCertificateClone.Save(objFS, objImageFormat);
                    bmCertificateClone.Dispose();
                }
                objGraphics.Dispose();
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Cal Text Location
        /// </summary>
        private static PointF CalTextLocation(PointF x_objBeforePoint, StringAlignment x_eHorizontal, StringAlignment x_eVertical, SizeF x_objTextSize, float x_ImageWidth,
            float x_fFrameW, float x_fFrameH, bool x_bMiddleInPicture)
        {
            PointF objPoint;

            objPoint = new PointF();
            if (x_bMiddleInPicture == true)
            {
                objPoint.X = (x_ImageWidth / 2) - (x_objTextSize.Width / 2);
            }
            else
            {
                // Width
                switch (x_eHorizontal)
                {
                    // Right alignment: (Start point (X) + Frame width) - Text Width
                    case StringAlignment.Far:
                        objPoint.X = (x_objBeforePoint.X + x_fFrameW) - x_objTextSize.Width;
                        break;
                    // Left alignment: Start point (X)
                    case StringAlignment.Near:
                        objPoint.X = x_objBeforePoint.X;
                        break;
                    // Center Frame: Start point (X) + ((Frame width / 2) - (Text size width / 2))
                    case StringAlignment.Center:
                        objPoint.X = x_objBeforePoint.X + ((x_fFrameW / 2) - (x_objTextSize.Width / 2));
                        break;
                }
            }

            // Height
            switch (x_eVertical)
            {
                // Bottom: Start point (Y) + (Max Height - Text Size Height)
                case StringAlignment.Far:
                    objPoint.Y = x_objBeforePoint.Y + (x_fFrameH - x_objTextSize.Height);
                    break;
                // Top: Start point (Y)
                case StringAlignment.Near:
                    objPoint.Y = x_objBeforePoint.Y;
                    break;
                // Middle: Start point (Y) + ((Frame Height / 2) - (Text height / 2))
                case StringAlignment.Center:
                    objPoint.Y = x_objBeforePoint.Y + ((x_fFrameH / 2) - (x_objTextSize.Height / 2));
                    break;
            }

            return objPoint;
        }

        /// <summary>
        /// Get Image Format
        /// </summary>
        public static ImageFormat GetImageFormat(string x_strFilePath)
        {
            string strExtension;

            strExtension = Path.GetExtension(x_strFilePath).ToLower();
            switch (strExtension)
            {
                case ".png":
                    return ImageFormat.Png;
                case ".jpg":
                case ".jpeg":
                    return ImageFormat.Jpeg;
                case ".ico":
                    return ImageFormat.Icon;
                case ".tif":
                    return ImageFormat.Tiff;
            }
            return ImageFormat.Jpeg;
        }

        /// <summary>
        /// Get Frame Point
        /// </summary>
        private static PointF[] GetFramePoint(PointF x_objTextPoint, float x_ImageWidth, int x_nMaxWidth, int x_nMaxHeight, bool x_bMiddleInPicture)
        {
            PointF[] arrFramePoint;
            PointF objFramePoint;
            float fPointTempX;

            arrFramePoint = new PointF[5];
            if (x_bMiddleInPicture == true)
            {
                fPointTempX = (x_ImageWidth / 2) - (x_nMaxWidth / 2);
                // Point 1
                objFramePoint = new PointF(fPointTempX, x_objTextPoint.Y);
                arrFramePoint[0] = objFramePoint;
                // Point 2
                objFramePoint = new PointF((fPointTempX + x_nMaxWidth), x_objTextPoint.Y);
                arrFramePoint[1] = objFramePoint;
                // Point 3
                objFramePoint = new PointF((fPointTempX + x_nMaxWidth), (x_objTextPoint.Y + x_nMaxHeight));
                arrFramePoint[2] = objFramePoint;
                // Point 4
                objFramePoint = new PointF(fPointTempX, (x_objTextPoint.Y + x_nMaxHeight));
                arrFramePoint[3] = objFramePoint;
                // Point 5
                objFramePoint = new PointF(fPointTempX, x_objTextPoint.Y);
                arrFramePoint[4] = objFramePoint;
            }
            else
            {
                // Point 1
                objFramePoint = new PointF(x_objTextPoint.X, x_objTextPoint.Y);
                arrFramePoint[0] = objFramePoint;
                // Point 2
                objFramePoint = new PointF((x_objTextPoint.X + x_nMaxWidth), x_objTextPoint.Y);
                arrFramePoint[1] = objFramePoint;
                // Point 3
                objFramePoint = new PointF((x_objTextPoint.X + x_nMaxWidth), (x_objTextPoint.Y + x_nMaxHeight));
                arrFramePoint[2] = objFramePoint;
                // Point 4
                objFramePoint = new PointF(x_objTextPoint.X, (x_objTextPoint.Y + x_nMaxHeight));
                arrFramePoint[3] = objFramePoint;
                // Point 5
                objFramePoint = new PointF(x_objTextPoint.X, x_objTextPoint.Y);
                arrFramePoint[4] = objFramePoint;
            }

            return arrFramePoint;
        }

        /// <summary>
        /// Create Captcha
        /// </summary>
        /// <param name="x_strFilePath"></param>
        /// <param name="x_strCaptchaCode"></param>
        /// <returns></returns>
        public static bool CreateCaptcha(string x_strFilePath, string x_strCaptchaCode)
        {
            const int HEIGHT = 90;
            const int WIDTH = 300;
            int nM;
            int nX;
            int nY;
            int nW;
            int nH;
            int nColorIndex;
            float fFontSize;
            Random objRandom;
            Bitmap objBitmap;
            Graphics objGraphics;
            HatchBrush objHatchBrush;
            Font objFont;
            StringFormat objStringFormat;
            CertificateText objCertificateText;
            Color[] arrColors;
            Rectangle objRect;
            SizeF objSize;

            try
            {
                // Create a new 32-bit bit
                objBitmap = new Bitmap(WIDTH, HEIGHT, PixelFormat.Format32bppArgb);
                objRandom = new Random();
                // Create a graphics object for drawing.
                using (objGraphics = Graphics.FromImage(objBitmap))
                {
                    objGraphics.SmoothingMode = SmoothingMode.AntiAlias;
                    objRect = new Rectangle(0, 0, WIDTH, HEIGHT);

                    arrColors = new Color[7];
                    arrColors[0] = Color.Red;
                    arrColors[1] = Color.Blue;
                    arrColors[2] = Color.Gray;
                    arrColors[3] = Color.Black;
                    arrColors[4] = Color.Green;
                    arrColors[5] = Color.OrangeRed;
                    arrColors[6] = Color.Pink;
                    nColorIndex = objRandom.Next(0, 6);
                    // Fill in the background.
                    objHatchBrush = new HatchBrush(HatchStyle.SmallConfetti, arrColors[nColorIndex], Color.White);
                    objGraphics.FillRectangle(objHatchBrush, objRect);

                    // Set up the text font.
                    objSize = default(SizeF);
                    fFontSize = objRect.Height + 1;
                    objCertificateText = new CertificateText(fFontSize, Path.Combine(SysFolder.FONT_FOLDER, SysFile.TAHOMA_FONT_FILE_NAME));
                    objFont = default(Font);
                    // Adjust the font size until the text fits within the image.
                    do
                    {
                        fFontSize -= 1;
                        objCertificateText.FontSize = fFontSize;
                        objFont = objCertificateText.Font;
                        objSize = objGraphics.MeasureString(x_strCaptchaCode, objFont);
                    } while ((objSize.Width > (objRect.Width - 40)) || (objSize.Height > (objRect.Height - 15)));
                    // Set up the text format.
                    objStringFormat = new StringFormat();
                    objStringFormat.Alignment = StringAlignment.Center;
                    objStringFormat.LineAlignment = StringAlignment.Center;

                    // Create a path using the text and warp it randomly.
                    // Draw the text.
                    nColorIndex = objRandom.Next(0, 6);
                    objHatchBrush = new HatchBrush(HatchStyle.LargeConfetti, arrColors[nColorIndex], Color.DarkGray);
                    objGraphics.DrawString(x_strCaptchaCode, objFont, objHatchBrush, new PointF(((objRect.Width / 2) - (objSize.Width / 2)),
                                                                                                ((objRect.Height / 2) - (objSize.Height / 2))));
                    // Add some random noise.
                    nM = Math.Max(objRect.Width, objRect.Height);
                    for (int nIndex = 0; nIndex <= Convert.ToInt32(Math.Truncate(objRect.Width * objRect.Height / 30f)) - 1; nIndex++)
                    {
                        nX = objRandom.Next(objRect.Width);
                        nY = objRandom.Next(objRect.Height);
                        nW = objRandom.Next(nM / 50);
                        nH = objRandom.Next(nM / 50);
                        objGraphics.FillEllipse(objHatchBrush, nX, nY, nW, nH);
                    }

                    // Save file
                    objBitmap.SaveBitmapStream(x_strFilePath);

                    // Clean up.
                    objFont.Dispose();
                    objHatchBrush.Dispose();
                    objGraphics.Dispose();
                }
                return true;
            }
            catch (Exception ex)
            {
                LogHelper.WriteLog(LogTypeEnum.ERRORS, string.Empty, string.Empty, $"{ex.Source}: {ex.Message}", "CreateCaptcha");
                return false;
            }
        }
    }
}
