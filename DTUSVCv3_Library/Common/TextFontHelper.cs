﻿using System.ComponentModel;
using System.Drawing;
using System.Drawing.Text;
using System.Runtime.InteropServices;

namespace DTUSVCv3_Library
{
    public static class TextFontHelper
    {
        /// <summary>
        /// Try Get Font
        /// </summary>
        public static bool TryGetFont(string x_strFontFilePath, out FontFamily x_objFont)
        {
            PrivateFontCollection objCollection;

            try
            {
                if ((string.IsNullOrWhiteSpace(x_strFontFilePath) == true) || (File.Exists(x_strFontFilePath) == false))
                {
                    x_objFont = null;
                    return false;
                }
                // Create font
                objCollection = new PrivateFontCollection();
                objCollection.AddFontFile(x_strFontFilePath);
                if (objCollection.Families.Length > 0)
                {
                    x_objFont = objCollection.Families[0];
                    objCollection.Dispose();
                    return true;
                }
                x_objFont = null;
                return false;
            }
            catch
            {
                x_objFont = null;
                return false;
            }
        }

        /// <summary>
        /// String To Color
        /// </summary>
        public static bool TryParseStringToColor(string x_strColorStr, out Color x_objColor)
        {
            TypeConverter objTypeConverter;
            Color objColor;

            try
            {
                if (string.IsNullOrWhiteSpace(x_strColorStr) == true)
                {
                    x_objColor = Color.Black;
                }

                objTypeConverter = TypeDescriptor.GetConverter(typeof(Color));
                objColor = (Color)objTypeConverter.ConvertFromString(x_strColorStr);
                x_objColor = objColor;
                return true;
            }
            catch
            {
                // Get color defauult
                x_objColor = Color.Black;
                return false;
            }
        }
    }
}
