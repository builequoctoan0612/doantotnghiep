﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;

namespace DTUSVCv3_Library
{
    public static class EnumHelper
    {
        /// <summary>
        /// Convert int to enum
        /// </summary>
        public static T ToEnum<T>(this string x_strVal, T x_eDefault) where T : Enum
        {
            T eNum;
            try
            {
                eNum = (T)Enum.Parse(typeof(T), x_strVal);
                return eNum;
            }
            catch
            {
                return x_eDefault;
            }
        }

        /// <summary>
        /// Convert int to enum
        /// </summary>
        public static T ToEnum<T>(this int x_nVal, T x_eDefault) where T : Enum
        {
            T eNum;
            string strVal;
            try
            {
                strVal = string.Format("{0}", x_nVal);
                eNum = (T)Enum.Parse(typeof(T), strVal);
                return eNum;
            }
            catch
            {
                return x_eDefault;
            }
        }

        /// <summary>
        /// Get Display name
        /// </summary>
        public static string DisplayName<T>(this T x_eValue)
            where T : Enum
        {
            DisplayAttribute objDspAttri;
            string strDisplayName;
            try
            {
                objDspAttri = x_eValue.GetType().GetMember(x_eValue.ToString()).First().GetCustomAttribute<DisplayAttribute>();
                strDisplayName = objDspAttri.Name;
                return strDisplayName;
            }
            catch
            {
                return null;
            }
        }
    }
}
