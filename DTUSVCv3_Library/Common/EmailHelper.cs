﻿using DTUSVCv3_Library.LibraryModel;
using System.Net;
using System.Net.Mail;

namespace DTUSVCv3_Library
{
    public class EmailHelper
    {
        #region Constructor
        public EmailHelper()
        {
            m_objLock = new object();
            m_objEmailQueue = new Queue<SendEmailModel>();
            m_objResetPasswdEmailQueue = new Queue<SendEmailModel>();
            m_objEmailInterviewQueue = new Queue<EmailInterviewModel>();
            if (m_objThread == null)
            {
                m_objThread = new Thread(DequeueNomal);
                m_objThread.Start();
            }
            if (m_objThreadResetPass == null)
            {
                m_objThreadResetPass = new Thread(DequeueResetPasswd);
                m_objThreadResetPass.Start();
            }
            if (m_objInterviewThread == null)
            {
                m_objInterviewThread = new Thread(DequeueInterviewEmail);
                m_objInterviewThread.Start();
            }
        }

        #endregion

        #region Types
        private const string EMAIL_CC_DEFAULT = "dtusvc.itteam@gmail.com";
        #endregion

        #region Fields
        private object m_objLock;
        private Thread m_objThread;
        private Thread m_objInterviewThread;
        private Thread m_objThreadResetPass;
        private Queue<SendEmailModel> m_objEmailQueue;
        private Queue<EmailInterviewModel> m_objEmailInterviewQueue;
        private Queue<SendEmailModel> m_objResetPasswdEmailQueue;
        private static EmailHelper m_objInstant;
        #endregion

        #region Properties
        public static EmailHelper Instant
        {
            get
            {
                if (m_objInstant == null)
                {
                    m_objInstant = new EmailHelper();
                }
                return m_objInstant;
            }
        }

        public Queue<SendEmailModel> ResetPasswdQueue
        {
            get
            {
                return m_objResetPasswdEmailQueue;
            }
        }

        public Queue<SendEmailModel> EmailNomalQueue
        {
            get
            {
                return m_objEmailQueue;
            }
        }

        public Queue<EmailInterviewModel> EmailInterviewQueue
        {
            get
            {
                return m_objEmailInterviewQueue;
            }
        }
        #endregion

        #region Methods
        private void DequeueInterviewEmail()
        {
            EmailInterviewModel objSendEmailModel;
            string strSendEmailResult;
            while (true)
            {
                try
                {
                    if (m_objEmailInterviewQueue == null)
                    {
                        m_objEmailInterviewQueue = new Queue<EmailInterviewModel>();
                    }

                    if (m_objEmailInterviewQueue.Count == 0)
                    {
                        Thread.Sleep(1000);
                        continue;
                    }
                    // Dequeue
                    objSendEmailModel = m_objEmailInterviewQueue.Dequeue();
                    if (objSendEmailModel == null)
                    {
                        Thread.Sleep(10);
                        continue;
                    }

                    strSendEmailResult = SendEmail(objSendEmailModel);
                    if (string.IsNullOrWhiteSpace(strSendEmailResult) == false)
                    {
                        LogHelper.WriteLog(LogTypeEnum.ERRORS, string.Empty, string.Empty, strSendEmailResult, "EmailHelper");
                    }
                    objSendEmailModel.SendResult(strSendEmailResult);
                    Thread.Sleep(10);
                }
                catch
                {
                    Thread.Sleep(10);
                }
            }
        }
        /// <summary>
        /// Dequeue Nomal
        /// </summary>
        private void DequeueNomal()
        {
            SendEmailModel objSendEmailModel;
            string strSendEmailResult;
            while (true)
            {
                try
                {
                    if (m_objEmailQueue == null)
                    {
                        m_objEmailQueue = new Queue<SendEmailModel>();
                    }

                    if (m_objEmailQueue.Count == 0)
                    {
                        Thread.Sleep(1000);
                        continue;
                    }
                    // Dequeue
                    objSendEmailModel = m_objEmailQueue.Dequeue();
                    if (objSendEmailModel == null)
                    {
                        Thread.Sleep(10);
                        continue;
                    }

                    strSendEmailResult = SendEmail(objSendEmailModel);
                    if (string.IsNullOrWhiteSpace(strSendEmailResult) == false)
                    {
                        LogHelper.WriteLog(LogTypeEnum.ERRORS, string.Empty, string.Empty, strSendEmailResult, "EmailHelper");
                    }
                    Thread.Sleep(10);
                }
                catch
                {
                    Thread.Sleep(10);
                }
            }
        }

        /// <summary>
        /// Dequeue Nomal
        /// </summary>
        private void DequeueResetPasswd()
        {
            SendEmailModel objSendEmailModel;
            string strSendEmailResult;
            while (true)
            {
                try
                {
                    if (SystemSetting.IS_SYSTEM_MAINTENANCE == true)
                    {
                        Thread.Sleep(10000);
                        continue;
                    }

                    if (m_objResetPasswdEmailQueue == null)
                    {
                        m_objResetPasswdEmailQueue = new Queue<SendEmailModel>();
                    }

                    if (m_objResetPasswdEmailQueue.Count == 0)
                    {
                        Thread.Sleep(1000);
                        continue;
                    }
                    // Dequeue
                    objSendEmailModel = m_objResetPasswdEmailQueue.Dequeue();
                    if (objSendEmailModel == null)
                    {
                        Thread.Sleep(10);
                        continue;
                    }

                    strSendEmailResult = SendEmail(objSendEmailModel);
                    if (string.IsNullOrWhiteSpace(strSendEmailResult) == false)
                    {
                        LogHelper.WriteLog(LogTypeEnum.ERRORS, string.Empty, string.Empty, strSendEmailResult, "EmailHelper");
                    }
                    Thread.Sleep(10);
                }
                catch
                {
                    Thread.Sleep(10);
                }
            }
        }

        /// <summary>
        /// Send Email
        /// </summary>
        private string SendEmail(SendEmailModel x_objSendEmailModel)
        {
            string strDisplayName;
            string strError;
            SmtpClient objMailClient;
            MailAddress objEmailFrom;
            MailAddress objEmailTo;
            MailAddress objEmailCc;
            MailMessage objMessage;
            List<Attachment> lstAttach;
            SendEmailModel objSendEmailModel;

            try
            {
                lock (m_objLock)
                {
                    objSendEmailModel = x_objSendEmailModel.DeepClone();
                    objMailClient = new SmtpClient(EmailAccount.MAIL_SERVER, EmailAccount.PORT);
                    objMailClient.EnableSsl = true;
                    objMailClient.Credentials = new NetworkCredential(EmailAccount.ACCOUNT, EmailAccount.PASSWD);

                    // Get display name
                    strDisplayName = objSendEmailModel.EmailType.DisplayName();
                    objEmailFrom = new MailAddress(EmailAccount.ACCOUNT, strDisplayName);
                    objEmailTo = new MailAddress(objSendEmailModel.EmailRecipient.Trim());
                    // Message
                    objMessage = new MailMessage(objEmailFrom, objEmailTo);
                    if (objSendEmailModel.IsSendCC == true)
                    {
                        if (string.IsNullOrEmpty(objSendEmailModel.EmailCC) == true)
                        {
                            objEmailCc = new MailAddress(EMAIL_CC_DEFAULT);
                            objMessage.CC.Add(objEmailCc);
                        }
                        else if (objSendEmailModel.EmailCC.Trim().EmailIsValid() == true)
                        {
                            objEmailCc = new MailAddress(objSendEmailModel.EmailCC.Trim());
                            objMessage.CC.Add(objEmailCc);
                        }
                    }
                    objMessage.Subject = objSendEmailModel.Subject;
                    objMessage.Body = objSendEmailModel.EmailBody;
                    objMessage.IsBodyHtml = true;
                    objMessage.BodyEncoding = System.Text.Encoding.UTF8;

                    // Attach file
                    lstAttach = null;
                    if ((objSendEmailModel.AttachFilePath != null) && (objSendEmailModel.AttachFilePath.Count > 0))
                    {
                        lstAttach = GetListAttach(objSendEmailModel.AttachFilePath, out strError);
                        if (string.IsNullOrWhiteSpace(strError) == false)
                        {
                            return strError;
                        }
                        // Add attach file
                        foreach (Attachment objAttach in lstAttach)
                        {
                            objMessage.Attachments.Add(objAttach);
                        }
                    }
                    objMailClient.Send(objMessage);

                    // Delete attach file
                    if (objSendEmailModel.DeleteFileAfterSent == true)
                    {
                        DeleteAttachFile(objSendEmailModel.AttachFilePath, lstAttach);
                    }
                    return string.Empty;
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        /// <summary>
        /// Delete Attach File
        /// </summary>
        private void DeleteAttachFile(List<string> x_lstAttachFilePath, List<Attachment> x_lstAttach)
        {
            try
            {
                if (x_lstAttach == null)
                {
                    return;
                }

                for (int nIndex = 0; nIndex < x_lstAttach.Count; nIndex++)
                {
                    x_lstAttach[nIndex].Dispose();
                }

                foreach (string strFilePath in x_lstAttachFilePath)
                {
                    if (File.Exists(strFilePath) == true)
                    {
                        File.Delete(strFilePath);
                    }
                }
            }
            catch
            {
                // No need process
            }
        }

        /// <summary>
        /// Get List Attach
        /// </summary>
        private List<Attachment> GetListAttach(List<string> x_lstAttachFilePath, out string x_strError)
        {
            List<Attachment> lstAttach;
            Attachment objAttachment;
            FileInfo objFileInfo;
            long lTotalFileSize;

            x_strError = string.Empty;
            if (x_lstAttachFilePath == null)
            {
                return null;
            }

            lTotalFileSize = 0;
            lstAttach = new List<Attachment>();
            foreach (string strFilePath in x_lstAttachFilePath)
            {
                if (File.Exists(strFilePath) == true)
                {
                    objFileInfo = new FileInfo(strFilePath);
                    lTotalFileSize += objFileInfo.Length;
                    if (lTotalFileSize > FileUploadSize.SEND_EMAIL_ATTACHFILE)
                    {
                        x_strError = ErrorConstant.ATTACH_FILE_OUT_SIZE;
                        break;
                    }
                    objAttachment = new Attachment(strFilePath);
                    lstAttach.Add(objAttachment);
                }
                else
                {
                    x_strError += string.Format("File: {0} không tồn tại.\n", Path.GetFileName(strFilePath));
                }
            }

            return lstAttach;
        }

        #endregion
    }
}
