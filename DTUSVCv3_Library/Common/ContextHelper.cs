﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.Extensions.Primitives;
using System;
using System.Net;

namespace DTUSVCv3_Library
{
    public static class ContextHelper
    {
        /// <summary>
        /// Get Token By Http Context
        /// </summary>
        public static string GetTokenByHttpContext(this HttpContext x_objHttpContext)
        {
            string strToken;
            StringValues objTokenValue;

            try
            {
                if (x_objHttpContext.Request.Headers.TryGetValue("Authorization", out objTokenValue) == false)
                {
                    return ErrorConstant.UNKNOWN;
                }

                strToken = objTokenValue.ToString().Replace("Bearer", "").Trim();
                return strToken;
            }
            catch
            {
                return ErrorConstant.UNKNOWN;
            }
        }

        /// <summary>
        /// Get Client IP Address
        /// </summary>
        public static string GetClientIPAddress(this HttpContext x_objContext, bool x_bIsGetAddress = true)
        {
            const string VALUE_FORMAT = "IP:{0} - City: {1} - Country: {2} - Postal: {3}";
            string strIp;
            IpInfo objIpInfo;

            strIp = x_objContext.Request.Headers["X-Forwarded-For"];
            if (string.IsNullOrWhiteSpace(strIp) == true)
            {
                strIp = x_objContext.Request.HttpContext.Features.Get<IHttpConnectionFeature>().RemoteIpAddress.ToString();
            }
            if (x_bIsGetAddress == true)
            {
                // Get User Location
                objIpInfo = GetUserLocation(strIp);
                if (objIpInfo != null)
                {
                    strIp = string.Format(VALUE_FORMAT, objIpInfo.Ip, objIpInfo.City, objIpInfo.Country, objIpInfo.Postal);
                }
            }

            return strIp;
        }

        /// <summary>
        /// Get User Location
        /// </summary>
        private static IpInfo GetUserLocation(string x_strIp)
        {
            const string API_PATH_FORMAT = "http://ipinfo.io/{0}";

            IpInfo objIpInfo;
            string strInfo;
            string strAPIPath;

            try
            {
                // Create path
                strAPIPath = string.Format(API_PATH_FORMAT, x_strIp);
                // Get info
                strInfo = new WebClient().DownloadString(strAPIPath);
                objIpInfo = strInfo.JSONToObject<IpInfo>();
                return objIpInfo;
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}
