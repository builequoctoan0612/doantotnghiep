﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace DTUSVCv3_Library
{
    public static class CommonHelper
    {
        /// <summary>
        /// Equals Ignore Case
        /// </summary>
        public static bool EqualsIgnoreCase(this string x_strText1, string x_strText2)
        {
            if (string.IsNullOrWhiteSpace(x_strText1) == true && string.IsNullOrWhiteSpace(x_strText2) == true)
            {
                return true;
            }
            return x_strText1.Equals(x_strText2, StringComparison.OrdinalIgnoreCase);
        }

        /// <summary>
        /// Equals Ignore Case
        /// </summary>
        public static bool ContainsIgnoreCase(this string x_strText1, string x_strText2)
        {
            if (string.IsNullOrEmpty(x_strText1) || string.IsNullOrEmpty(x_strText2))
            {
                return false;
            }
            return x_strText1.Contains(x_strText2, StringComparison.OrdinalIgnoreCase);
        }

        /// <summary>
        /// Contains array
        /// </summary>
        public static bool ContainsArr(this string[] x_arrVal1, string x_strVal2)
        {
            List<string> lstVal;
            bool bRes;

            if (x_arrVal1 == null)
            {
                return false;
            }
            // Convert array to list
            lstVal = x_arrVal1.ToList();
            bRes = lstVal.Contains(x_strVal2);
            return bRes;
        }

        /// <summary>
        /// Contains array
        /// </summary>
        public static bool ContainsArrIgnoreCase(this string[] x_arrVal1, string x_strVal2)
        {
            List<string> lstVal;
            string strVal;
            bool bRes;

            if (x_arrVal1 == null)
            {
                return false;
            }
            // Convert array to list
            lstVal = x_arrVal1.ToList();
            // Remove value null
            lstVal = lstVal.FindAll(obj => string.IsNullOrWhiteSpace(obj) == false);
            lstVal = lstVal.Select(obj => obj.ToLower()).ToList();
            strVal = x_strVal2.ToLower();
            bRes = lstVal.Contains(x_strVal2);
            return bRes;
        }
    }
}
