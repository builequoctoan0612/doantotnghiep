﻿using DocumentFormat.OpenXml.Spreadsheet;
using EmailValidator.NET;
using GemBox.Email;
using Microsoft.AspNetCore.Http;
using MongoDB.Bson;
using System;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Text.RegularExpressions;

namespace DTUSVCv3_Library
{
    public static class ValidateHelper
    {
        /// <summary>
        /// MongoDb Id Is Valid
        /// </summary>
        public static bool MongoIdIsValid(this string x_strId)
        {
            string strTmpId;
            byte[] arrBytes;

            strTmpId = x_strId;

            if (string.IsNullOrWhiteSpace(strTmpId) == true)
            {
                return false;
            }

            // don't throw ArgumentNullException if s is null
            if ((strTmpId != null) && (strTmpId.Length == 24))
            {
                if (BsonUtils.TryParseHexString(strTmpId, out arrBytes) == true)
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Name Is Valid
        /// </summary>
        /// <param name="x_strName"></param>
        /// <returns></returns>
        public static bool CheckNameIsValid(this string x_strName)
        {
            bool bValid;

            if ((string.IsNullOrWhiteSpace(x_strName) == true) || (x_strName.Length > 30))
            {
                return false;
            }
            x_strName = TextHelper.RemoveUnicode(x_strName);
            bValid = Regex.IsMatch(x_strName, RegexConstant.REGEX_FULL_NAME);
            return bValid;
        }

        /// <summary>
        /// Check Date Time Format
        /// </summary>
        /// <param name="x_strDateTime"></param>
        /// <returns></returns>
        public static bool CheckDateTimeFormat(this string x_strDateTime, out string x_strNewDateTime)
        {
            const string DATE_FORMAT = "dd/MM/yyyy";
            string[] arrDateTimeFormat = { "dd/MM/yyyy",
                                           "d/MM/yyyy",
                                           "dd/M/yyyy",
                                           "d/M/yyyy",
                                           "MM/dd/yyyy",
                                           "M/d/yyyy",
                                           "M/dd/yyyy",
                                           "MM/d/yyyy",
                                           "yyyy/MM/dd",
                                           "dd/MM/yyyy HH:mm:ss",
                                           "d/MM/yyyy HH:mm:ss",
                                           "dd/M/yyyy HH:mm:ss",
                                           "d/M/yyyy HH:mm:ss",
                                           "dd/MM/yyyy HH:mm",
                                           "d/MM/yyyy HH:mm",
                                           "dd/M/yyyy HH:mm",
                                           "d/M/yyyy HH:mm"};
            DateTime objDateTime;

            try
            {
                if (DateTime.TryParseExact(x_strDateTime, arrDateTimeFormat, new CultureInfo("vi-VN"), DateTimeStyles.None, out objDateTime) == true)
                {
                    x_strNewDateTime = objDateTime.ToString(DATE_FORMAT);
                    return true;
                }
                x_strNewDateTime = string.Empty;
                return true;
            }
            catch
            {
                x_strNewDateTime = string.Empty;
            }
            return false;
        }

        /// <summary>
        /// Validate Sex
        /// </summary>
        /// <param name="x_strSex"></param>
        /// <returns></returns>
        public static bool ValidateSex(this string x_strSex)
        {
            const string MEN = "Nam";
            const string WORMEN = "Nữ";
            string strSex;
            if (string.IsNullOrWhiteSpace(x_strSex) == true)
            {
                return false;
            }

            strSex = x_strSex.Trim();
            if ((strSex.EqualsIgnoreCase(MEN) == true) || (strSex.EqualsIgnoreCase(WORMEN) == true))
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// Email Is Valid
        /// </summary>
        public static bool EmailIsValid(this string x_strEmail)
        {
            System.Net.Mail.MailAddress objMailAddress;
            int nRes;

            try
            {
                objMailAddress = new System.Net.Mail.MailAddress(x_strEmail);
                nRes = CheckEmailExists(x_strEmail);
                if (nRes >= 0)
                {
                    return true;
                }
                return false;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Email Validator
        /// </summary>
        private static int CheckEmailExists(string x_strEmail)
        {
            EmailValidator.NET.EmailValidator objEmailValidator;
            MailAddressValidationResult objMailValidator;
            EmailValidationResult eResult;

            objEmailValidator = new EmailValidator.NET.EmailValidator();
            // Validate google
            if (objEmailValidator.Validate(x_strEmail, out eResult) == false)
            {
                return -1;
            }
            if (eResult == EmailValidationResult.OK)
            {
                return 0;
            }
            else
            {
                try
                {
                    ComponentInfo.SetLicense("FREE-LIMITED-KEY");
                    objMailValidator = MailAddressValidator.Validate(x_strEmail);
                    if (objMailValidator.Status == MailAddressValidationStatus.Ok)
                    {
                        return 0;
                    }
                    else
                    {
                        return 1;
                    }
                }
                catch
                {
                    return 0;
                }
            }
        }

        /// <summary>
        /// Phone Number Is Valid
        /// </summary>
        public static bool PhoneNumberIsValid(this string x_strPhoneNumber)
        {
            bool bPhonenumberValid;

            try
            {
                if (string.IsNullOrWhiteSpace(x_strPhoneNumber) == true)
                {
                    return false;
                }

                bPhonenumberValid = Regex.IsMatch(x_strPhoneNumber, RegexConstant.REGEX_PHONENUMBER);
                return bPhonenumberValid;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Passwd Is Valid
        /// </summary>
        public static bool PasswdIsValid(this string x_strPasswd)
        {
            bool bPasswdValid;

            if (string.IsNullOrWhiteSpace(x_strPasswd) == true)
            {
                return false;
            }
            bPasswdValid = Regex.IsMatch(x_strPasswd, RegexConstant.REGEX_PASSWD);
            return bPasswdValid;
        }

        /// <summary>
        /// Student Is Valid
        /// </summary>
        public static bool StudentIsValid(this string x_strStudent)
        {
            bool bStudentValid;

            if (string.IsNullOrWhiteSpace(x_strStudent) == true)
            {
                return false;
            }
            bStudentValid = Regex.IsMatch(x_strStudent, RegexConstant.REGEX_STUDENT_ID);
            return bStudentValid;
        }

        /// <summary>
        /// File is valid
        /// </summary>
        public static bool FileIsValid(this IFormFile x_objFile, string[] x_arrFileType, out string x_strErrorMessage)
        {
            bool bFileValid;

            x_strErrorMessage = string.Empty;
            bFileValid = true;
            if (x_objFile == null)
            {
                x_strErrorMessage = ErrorConstant.UPLOAD_FILE_IS_NULL;
                return false;
            }

            // Check file type excel
            if ((x_arrFileType.ContainsArrIgnoreCase(x_objFile.ContentType) == true) && (x_arrFileType == ContentType.EXCEL_CONTENT_TYPE))
            {
                // Check size document
                if (x_objFile.Length > FileUploadSize.DOCUMENT_FILE)
                {
                    x_strErrorMessage = ErrorConstant.FILE_UPLOAD_IS_TOO_LARGE_5;
                    return false;
                }
            }
            // Check file type Image 
            else if ((x_arrFileType.ContainsArrIgnoreCase(x_objFile.ContentType) == true) && (x_arrFileType == ContentType.IMAGE_CONTENT_TYPE))
            {
                // Check size image
                if (x_objFile.Length > FileUploadSize.IMAGE_FILE)
                {
                    x_strErrorMessage = ErrorConstant.FILE_UPLOAD_IS_TOO_LARGE_15;
                    return false;
                }
            }
            // Check font
            else if ((x_arrFileType.ContainsArrIgnoreCase(x_objFile.ContentType) == true) && (x_arrFileType == ContentType.FONT_CONTENT_TYPE))
            {
                // Check size image
                if (x_objFile.Length > FileUploadSize.FONT_FILE)
                {
                    x_strErrorMessage = ErrorConstant.FILE_UPLOAD_IS_TOO_LARGE_1;
                    return false;
                }
            }
            else if ((x_arrFileType.ContainsArrIgnoreCase(x_objFile.ContentType) == true) && (x_arrFileType == ContentType.ZIP_CONTENT_TYPE))
            {
                // Check size zip
                if (x_objFile.Length > FileUploadSize.ZIP_FILE)
                {
                    x_strErrorMessage = ErrorConstant.FILE_UPLOAD_IS_TOO_LARGE_100;
                    return false;
                }
            }
            // File type invalid
            else
            {
                x_strErrorMessage = ErrorConstant.FILE_DOWNLOADED_IN_WRONG_FORMAT;
                return false;
            }

            return bFileValid;
        }

        /// <summary>
        /// Check File Name Is Valid
        /// </summary>
        public static bool CheckFileNameIsValid(this string x_strFileName)
        {
            bool bValid;

            if (string.IsNullOrWhiteSpace(x_strFileName) == true)
            {
                return false;
            }

            if (x_strFileName.Length > 250)
            {
                return false;
            }

            bValid = x_strFileName.IndexOfAny(Path.GetInvalidFileNameChars()) == -1;

            return bValid;
        }

        /// <summary>
        /// Check Image Is Valid
        /// </summary>
        public static bool CheckImageIsValid(this Bitmap x_objBitmap, int x_nWidth = 1080, int x_nHeight = 1080)
        {
            if (x_objBitmap == null)
            {
                return false;
            }

            if ((x_objBitmap.Size.Width < x_nWidth) && (x_objBitmap.Size.Height < x_nHeight))
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Check Fb Valid
        /// </summary>
        public static bool CheckFbValid(this string x_strLinkFacebook)
        {
            string strLinkFb;
            bool bRes;

            strLinkFb = x_strLinkFacebook.ToLower();
            bRes = Regex.IsMatch(strLinkFb, RegexConstant.REGEX_FB_PATH);
            if (bRes == false)
            {
                return false;
            }

            if ((strLinkFb.ContainsIgnoreCase("facebook.com") == false) &&
                (strLinkFb.ContainsIgnoreCase("fb.com") == false) &&
                (strLinkFb.ContainsIgnoreCase("messenger.com") == false))
            {
                return false;
            }

            if ((strLinkFb.ContainsIgnoreCase("profile.php?ref=bookmarks") == false) ||
                (strLinkFb.ContainsIgnoreCase(".com/?_rdr") == false))
            {
                return false;
            }

            switch (strLinkFb)
            {
                case "facebook.com":
                case "http://facebook.com":
                case "https://facebook.com":
                case "http://www.facebook.com":
                case "https://m.facebook.com":
                case "fb.com":
                case "https://www.facebook.com":
                case "https://www.m.facebook.com":
                case "m.fb.com":
                case @"facebook.com/":
                case @"http://facebook.com/":
                case @"https://facebook.com/":
                case @"http://www.facebook.com/":
                case @"https://m.facebook.com/":
                case @"fb.com/":
                case @"https://www.facebook.com/":
                case @"https://www.m.facebook.com/":
                case @"http://m.fb.com":
                case @"http://fb.com":
                case @"https://m.fb.com":
                case @"https://fb.com":
                case @"https://wwww.m.fb.com":
                case @"https://www.facebook.com/profile.php?id=":
                case @"https://www.facebook.com/profile.php?id":
                case @"https://www.facebook.com/profile.php?i":
                case @"https://www.facebook.com/profile.php?":
                case @"https://www.m.facebook.com/profile.php?id=":
                case @"https://www.m.facebook.com/profile.php?id":
                case @"https://www.m.facebook.com/profile.php?i":
                case @"https://www.m.facebook.com/profile.php?":
                case @"facebook.com/profile.php?id=":
                case @"facebook.com/profile.php?id":
                case @"facebook.com/profile.php?i":
                case @"facebook.com/profile.php?":
                case @"m.facebook.com/profile.php?id=":
                case @"m.facebook.com/profile.php?id":
                case @"m.facebook.com/profile.php?i":
                case @"m.facebook.com/profile.php?":
                case @"www.facebook.com/profile.php?id=":
                case @"www.facebook.com/profile.php?id":
                case @"www.facebook.com/profile.php?i":
                case @"www.facebook.com/profile.php?":
                case @"www.m.facebook.com/profile.php?id=":
                case @"www.m.facebook.com/profile.php?id":
                case @"www.m.facebook.com/profile.php?i":
                case @"www.m.facebook.com/profile.php?":
                case @"https://facebook.com/profile.php?id=":
                case @"https://facebook.com/profile.php?id":
                case @"https://facebook.com/profile.php?i":
                case @"https://facebook.com/profile.php?":
                case @"http://facebook.com/profile.php?id=":
                case @"http://facebook.com/profile.php?id":
                case @"http://facebook.com/profile.php?i":
                case @"http://facebook.com/profile.php?":
                case @"https://www.fb.com/profile.php?id=":
                case @"https://www.fb.com/profile.php?id":
                case @"https://www.fb.com/profile.php?i":
                case @"https://www.fb.com/profile.php?":
                case @"https://www.m.fb.com/profile.php?id=":
                case @"https://www.m.fb.com/profile.php?id":
                case @"https://www.m.fb.com/profile.php?i":
                case @"https://www.m.fb.com/profile.php?":
                case @"fb.com/profile.php?id=":
                case @"fb.com/profile.php?id":
                case @"fb.com/profile.php?i":
                case @"fb.com/profile.php?":
                case @"m.fb.com/profile.php?id=":
                case @"m.fb.com/profile.php?id":
                case @"m.fb.com/profile.php?i":
                case @"m.fb.com/profile.php?":
                case @"www.fb.com/profile.php?id=":
                case @"www.fb.com/profile.php?id":
                case @"www.fb.com/profile.php?i":
                case @"www.fb.com/profile.php?":
                case @"www.m.fb.com/profile.php?id=":
                case @"www.m.fb.com/profile.php?id":
                case @"www.m.fb.com/profile.php?i":
                case @"www.m.fb.com/profile.php?":
                case @"https://fb.com/profile.php?id=":
                case @"https://fb.com/profile.php?id":
                case @"https://fb.com/profile.php?i":
                case @"https://fb.com/profile.php?":
                case @"http://fb.com/profile.php?id=":
                case @"http://fb.com/profile.php?id":
                case @"http://fb.com/profile.php?i":
                case @"http://fb.com/profile.php?":
                case @"https://www.fb.com": return false;
                default: break;
            }

            return bRes;
        }

        /// <summary>
        /// Is Valid Url
        /// </summary>
        /// <param name="x_strURL"></param>
        /// <returns></returns>
        public static bool IsValidUrl(this string x_strURL)
        {
            Uri objUri;
            bool bResult;

            try
            {
                bResult = Uri.TryCreate(x_strURL, UriKind.Absolute, out objUri)
                && (objUri.Scheme == Uri.UriSchemeHttp
                 || objUri.Scheme == Uri.UriSchemeHttps);

                if (bResult == false)
                {
                    bResult = Uri.IsWellFormedUriString(x_strURL, UriKind.RelativeOrAbsolute);
                }
                return bResult;
            }
            catch
            {
                return false;
            }
        }
    }
}
