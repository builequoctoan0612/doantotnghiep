﻿using System;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;

namespace DTUSVCv3_Library
{
    public static class DateTimeHelper
    {
        /// <summary>
        /// Get Current Date Time VN
        /// </summary>
        public static DateTime GetCurrentDateTimeVN()
        {
            DateTime dtCurrentTime;
            TimeZoneInfo objVNTimeZone;

            try
            {
                // Get time zone
                objVNTimeZone = GetVNTimeZone();
                if (objVNTimeZone != null)
                {
                    // Get date time now
                    dtCurrentTime = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, objVNTimeZone);
                }
                else
                {
                    dtCurrentTime = DateTime.UtcNow;
                }

                return dtCurrentTime;
            }
            catch
            {
                return DateTime.UtcNow;
            }
        }

        /// <summary>
        /// Get VN Time Zone
        /// </summary>
        /// <returns></returns>
        public static TimeZoneInfo GetVNTimeZone()
        {
            TimeZoneInfo objVNTimeZone;
            try
            {
                // Get time zone
                ReadOnlyCollection<TimeZoneInfo> collection = TimeZoneInfo.GetSystemTimeZones();
                objVNTimeZone = collection.ToList().Find(obj => (obj.DisplayName.ContainsIgnoreCase("hanoi") == true) ||
                                                                (obj.DisplayName.ContainsIgnoreCase("ho chi minh") == true) ||
                                                                (obj.DisplayName.ContainsIgnoreCase("vietnam") == true));
            }
            catch
            {
                objVNTimeZone = TimeZoneInfo.Utc;
            }
            return objVNTimeZone;
        }

        /// <summary>
        /// Convert String To Date Time
        /// </summary>
        /// <param name="x_strDateTime"></param>
        /// <returns></returns>
        public static DateTime ConvertStrToDateTime(this string x_strDateTime)
        {
            string[] DateTimeFormat = new string[]
            {
                "dd/MM/yyyy HH:mm:ss",
                "d/MM/yyyy HH:mm:ss",
                "dd/M/yyyy HH:mm:ss",
                "d/M/yyyy HH:mm:ss",
                "dd/MM/yyyy HH:mm",
                "d/MM/yyyy HH:mm",
                "dd/M/yyyy HH:mm",
                "d/M/yyyy HH:mm",
                "dd/MM/yyyy",
                "d/MM/yyyy",
                "dd/M/yyyy",
                "d/M/yyyy",
            };
            DateTime objDateTime;

            try
            {
                DateTime.TryParseExact(x_strDateTime.Trim(), DateTimeFormat, new CultureInfo("vi-VN"), DateTimeStyles.None, out objDateTime);
                return objDateTime;
            }
            catch
            {
                objDateTime = GetCurrentDateTimeVN();
                return objDateTime;
            }
        }
    }
}
