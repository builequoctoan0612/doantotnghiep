﻿using Google.Apis.Auth.OAuth2;
using Google.Apis.Drive.v3;
using Google.Apis.Drive.v3.Data;
using Google.Apis.Services;
using Google.Apis.Util.Store;

namespace DTUSVCv3_Library
{
    public class GoogleDriveHelper
    {
        public GoogleDriveHelper()
        {
            UserCredential objUserCredential;

            objUserCredential = GetGoogleDriveCredential().Result;
            m_objDriveService = new DriveService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = objUserCredential,
                ApplicationName = APPLICATION_NAME,
            });
        }

        private const string APPLICATION_NAME = "DTUSVCBackup";
        private const string FOLDER_MIME_TYPE = "application/vnd.google-apps.folder";
        private const string FILE_FIELDS = "*";

        private readonly DriveService m_objDriveService;
        private static GoogleDriveHelper m_objGoogleDriveHelper;

        public static GoogleDriveHelper Instant
        {
            get
            {
                if (m_objGoogleDriveHelper == null)
                {
                    m_objGoogleDriveHelper = new GoogleDriveHelper();
                }
                return m_objGoogleDriveHelper;
            }
        }

        /// <summary>
        /// Get Google Drive Credential
        /// </summary>
        /// <returns></returns>
        private async Task<UserCredential> GetGoogleDriveCredential()
        {
            UserCredential objUserCredential;
            string strCredPath;

            strCredPath = FileHelper.GetFilePath(SysFolder.WEB_SETTING_FOLDER, SysFile.USER_CREDENTIALS_FILE, true);
            using (FileStream objStream = new FileStream(strCredPath, FileMode.Open, FileAccess.Read))
            {
                strCredPath = FileHelper.GetFilePath(SysFolder.WEB_SETTING_FOLDER, SysFile.TOKEN_STORE_FILE, true);

                objUserCredential = await GoogleWebAuthorizationBroker.AuthorizeAsync(
                    GoogleClientSecrets.Load(objStream).Secrets,
                    new[] { DriveService.Scope.Drive, DriveService.Scope.DriveMetadata, DriveService.Scope.DriveMetadataReadonly },
                    "user",
                    CancellationToken.None,
                    new FileDataStore(strCredPath, true));
            }

            return objUserCredential;
        }

        /// <summary>
        /// Upload File To Google Drive
        /// </summary>
        /// <param name="x_strFilePath"></param>
        /// <param name="x_strFolderId"></param>
        /// <returns></returns>
        public async Task<UploadedModel> UploadFileToGoogleDrive(string x_strFilePath, string x_strFolderId)
        {
            Google.Apis.Drive.v3.Data.File objFileMetadata;
            Google.Apis.Drive.v3.Data.File objResponse;
            FilesResource.CreateMediaUpload objRequest;
            UploadedModel objUploadedFileModel;
            string strMimeType;

            try
            {
                objFileMetadata = GetFileMetadata(Path.GetFileName(x_strFilePath), x_strFolderId);
                strMimeType = new System.Net.Mime.ContentDisposition { FileName = x_strFilePath }.CreationDate.ToString();
                objFileMetadata.MimeType = strMimeType;
                using (FileStream strFileStream = new FileStream(x_strFilePath, FileMode.Open))
                {
                    objRequest = m_objDriveService.Files.Create(objFileMetadata, strFileStream, objFileMetadata.MimeType);
                    objRequest.Fields = FILE_FIELDS;
                    await objRequest.UploadAsync();
                }

                //
                objResponse = objRequest.ResponseBody;
                // 
                objUploadedFileModel = new UploadedModel
                {
                    Id = objResponse.Id,
                    ViewLink = objResponse.WebViewLink,
                    DownloadLink = objResponse.WebContentLink,
                    Name = objResponse.Name,
                    IsFolder = false,
                    Permissions = objResponse.Permissions.ToList()
                };

                return objUploadedFileModel;
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Get File Metadata
        /// </summary>
        /// <param name="x_strFileName"></param>
        /// <param name="x_strFolderId"></param>
        /// <returns></returns>
        private Google.Apis.Drive.v3.Data.File GetFileMetadata(string x_strFileName, string x_strFolderId)
        {
            Google.Apis.Drive.v3.Data.File objFileMetadata;

            objFileMetadata = new Google.Apis.Drive.v3.Data.File
            {
                Name = x_strFileName,
                Parents = new List<string> { x_strFolderId },
            };
            return objFileMetadata;
        }

        /// <summary>
        /// Delete File
        /// </summary>
        /// <param name="x_strFileId"></param>
        /// <returns></returns>
        public async Task<bool> DeleteFile(string x_strFileId)
        {
            try
            {
                await m_objDriveService.Files.Delete(x_strFileId).ExecuteAsync();
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Create Folder
        /// </summary>
        /// <param name="x_strFolderName"></param>
        /// <returns></returns>
        public async Task<UploadedModel> CreateFolder(string x_strFolderName, string x_strFolderId)
        {
            FilesResource.CreateRequest objRequest;
            Google.Apis.Drive.v3.Data.File objFolderMetadata;
            Google.Apis.Drive.v3.Data.File objFolder;
            UploadedModel objUploadedFolder;

            try
            {
                objFolderMetadata = new Google.Apis.Drive.v3.Data.File
                {
                    Name = x_strFolderName,
                    MimeType = FOLDER_MIME_TYPE,
                    Parents = new List<string> { x_strFolderId }
                };

                objRequest = m_objDriveService.Files.Create(objFolderMetadata);
                objRequest.Fields = FILE_FIELDS;

                objFolder = await objRequest.ExecuteAsync();

                objUploadedFolder = new UploadedModel
                {
                    Id = objFolder.Id,
                    Name = objFolder.Name,
                    ViewLink = objFolder.WebViewLink,
                    DownloadLink = objFolder.WebContentLink,
                    IsFolder = true,
                    Permissions = (List<Permission>)objFolder.Permissions
                };

                return objUploadedFolder;
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Share File
        /// </summary>
        /// <param name="x_strFileId"></param>
        /// <param name="x_strPermissionType"></param>
        /// <param name="x_strRole"></param>
        /// <returns></returns>
        public async Task<Permission> ShareFile(string x_strFileId, string x_strPermissionType, string x_strRole, string x_strEmail = null)
        {
            Permission objPermission;
            try
            {
                if (string.IsNullOrWhiteSpace(x_strEmail) == true)
                {
                    objPermission = new Permission
                    {
                        Type = x_strPermissionType,
                        Role = x_strRole,
                    };

                    objPermission = await m_objDriveService.Permissions.Create(objPermission, x_strFileId).ExecuteAsync();
                }
                else
                {
                    objPermission = new Permission
                    {
                        Type = x_strPermissionType,
                        Role = x_strRole,
                        EmailAddress = x_strEmail,
                    };

                    objPermission = await m_objDriveService.Permissions.Create(objPermission, x_strFileId).ExecuteAsync();
                }

                return objPermission;
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Revoke File Sharing
        /// </summary>
        /// <param name="x_strFileId"></param>
        /// <param name="x_strPermissionId"></param>
        /// <returns></returns>
        public bool RevokeFileSharing(string x_strFileId, string x_strPermissionId)
        {
            PermissionsResource.DeleteRequest objDeleteRequest;
            try
            {
                objDeleteRequest = m_objDriveService.Permissions.Delete(x_strFileId, x_strPermissionId);
                objDeleteRequest.SupportsAllDrives = true;

                objDeleteRequest.Execute();
                return true;
            }
            catch
            {
                return false;
            }
        }
    }

    public class UploadedModel
    {
        public string Id { get; set; }
        public string ViewLink { get; set; }
        public string DownloadLink { get; set; }
        public string Name { get; set; }
        public List<Permission> Permissions { get; set; }
        public bool IsFolder { get; set; }
    }
}
