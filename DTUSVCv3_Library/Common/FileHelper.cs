﻿using ClosedXML.Excel;
using DTUSVCv3_Library.LibraryModel;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.StaticFiles;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO.Compression;
using System.Reflection;

namespace DTUSVCv3_Library
{
    public static class FileHelper
    {
        /// <summary>
        /// Export Data To Excel
        /// </summary>
        public static bool ExportDataToExcel<T>(this Dictionary<string, List<T>> x_objData, string x_strFilePath)
        {
            bool bExportIsSuccsess;
            Dictionary<string, DataTable> dicData;
            DataTable objDataTable;

            dicData = new Dictionary<string, DataTable>();

            // Skip if data is null
            if (x_objData == null)
            {
                return false;
            }

            foreach (KeyValuePair<string, List<T>> objItem in x_objData)
            {
                objDataTable = objItem.Value.ToDataTable();
                if (objDataTable != null)
                {
                    dicData[objItem.Key] = objDataTable;
                }
            }
            // Export file excel
            bExportIsSuccsess = ExportToExcel(dicData, x_strFilePath);

            return bExportIsSuccsess;
        }

        /// <summary>
        /// Export Excel
        /// </summary>
        private static bool ExportToExcel(Dictionary<string, DataTable> x_dicDataTable, string x_strFilePath)
        {
            DataTable objData;
            XLWorkbook objWorkbook;
            IXLWorksheet objWorkSheet;
            FileStream objFileStream;
            string strFolderPath;

            // Skip if value is null or empty
            if ((x_dicDataTable == null) || (x_dicDataTable.Count == 0))
            {
                return false;
            }

            try
            {
                using (objWorkbook = new XLWorkbook())
                {
                    foreach (KeyValuePair<string, DataTable> objItem in x_dicDataTable)
                    {
                        objData = objItem.Value;

                        objWorkSheet = objWorkbook.Worksheets.Add(objItem.Key);
                        for (int nCol = 1; nCol <= objData.Columns.Count; nCol++)
                        {
                            objWorkSheet.Cell(1, nCol).Value = objData.Columns[nCol - 1].ColumnName;
                            objWorkSheet.Cell(1, nCol).Style.Font.Bold = true;
                        }
                        // Data start index Row = 2 and Col = 1;
                        objWorkSheet.Cell(2, 1).InsertData(objData.Rows);
                        objWorkSheet.Columns().AdjustToContents();

                        // Set cell border
                        objWorkSheet.Range(1, 1, objData.Rows.Count + 1, objData.Columns.Count).Style.Border.TopBorder = XLBorderStyleValues.Thin;
                        objWorkSheet.Range(1, 1, objData.Rows.Count + 1, objData.Columns.Count).Style.Border.InsideBorder = XLBorderStyleValues.Dotted;
                        objWorkSheet.Range(1, 1, objData.Rows.Count + 1, objData.Columns.Count).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                        objWorkSheet.Range(1, 1, objData.Rows.Count + 1, objData.Columns.Count).Style.Border.LeftBorder = XLBorderStyleValues.Thin;
                        objWorkSheet.Range(1, 1, objData.Rows.Count + 1, objData.Columns.Count).Style.Border.RightBorder = XLBorderStyleValues.Thin;
                        objWorkSheet.Range(1, 1, objData.Rows.Count + 1, objData.Columns.Count).Style.Border.TopBorder = XLBorderStyleValues.Thin;
                    }
                    // Create folder
                    strFolderPath = Path.GetDirectoryName(x_strFilePath);
                    if (Directory.Exists(strFolderPath) == false)
                    {
                        Directory.CreateDirectory(strFolderPath);
                    }

                    // Save to file
                    using (objFileStream = new FileStream(x_strFilePath, FileMode.Create))
                    {
                        objWorkbook.SaveAs(objFileStream);
                    }
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Export Member Regist Evt To Excel
        /// </summary>
        /// <param name="lstMemberRegistEvt"></param>
        /// <param name="x_lstExportMemberScore"></param>
        /// <param name="x_strFilePath"></param>
        /// <returns></returns>
        public static bool ExportMemberRegistEvtToExcel(List<ExportMemberRegistEvt> lstMemberRegistEvt, List<ExportMemberScore> x_lstExportMemberScore, string x_strFilePath)
        {
            XLWorkbook objWorkbook;
            FileStream objFileStream;
            ExportMemberRegistEvt objExportMemberRegistEvt;
            ExportMemberScore objExportMemberScore;
            IXLWorksheet objWorkSheet;
            IXLWorksheet objSubWorkSheet;
            DateTime objCurrentDate;
            int nDuplicate;
            int nSkipCount;
            string strSheetName;
            string strHomeSheetName;
            string strFolderPath;

            try
            {
                if ((lstMemberRegistEvt == null) || (lstMemberRegistEvt.Count == 0))
                {
                    return false;
                }

                objCurrentDate = DateTimeHelper.GetCurrentDateTimeVN();
                strHomeSheetName = string.Format("Tổng_Hợp_{0}", objCurrentDate.ToString("dd_MM_yyyy"));
                // Sort
                lstMemberRegistEvt = lstMemberRegistEvt.OrderBy(obj => obj.LastName).ThenBy(obj => obj.FirstName).ThenBy(obj => obj.TotalScore).ToList();
                // Create Sheet Total
                using (objWorkbook = new XLWorkbook())
                {
                    // Create Column
                    objWorkSheet = objWorkbook.Worksheets.Add(strHomeSheetName);
                    // No.
                    objWorkSheet.Cell(1, 1).Value = "STT";
                    objWorkSheet.Cell(1, 1).Style.Font.Bold = true;
                    objWorkSheet.Cell(1, 1).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                    // Student Id
                    objWorkSheet.Cell(1, 2).Value = "MSSV";
                    objWorkSheet.Cell(1, 2).Style.Font.Bold = true;
                    objWorkSheet.Cell(1, 2).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                    // First name
                    objWorkSheet.Cell(1, 3).Value = "Họ";
                    objWorkSheet.Cell(1, 3).Style.Font.Bold = true;
                    objWorkSheet.Cell(1, 3).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                    // Last name
                    objWorkSheet.Cell(1, 4).Value = "Tên";
                    objWorkSheet.Cell(1, 4).Style.Font.Bold = true;
                    objWorkSheet.Cell(1, 4).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                    // Faculty
                    objWorkSheet.Cell(1, 5).Value = "Khoa";
                    objWorkSheet.Cell(1, 5).Style.Font.Bold = true;
                    objWorkSheet.Cell(1, 5).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                    // Class
                    objWorkSheet.Cell(1, 6).Value = "Lớp";
                    objWorkSheet.Cell(1, 6).Style.Font.Bold = true;
                    objWorkSheet.Cell(1, 6).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                    // Phone number
                    objWorkSheet.Cell(1, 7).Value = "SĐT";
                    objWorkSheet.Cell(1, 7).Style.Font.Bold = true;
                    objWorkSheet.Cell(1, 7).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                    // Email
                    objWorkSheet.Cell(1, 8).Value = "Email";
                    objWorkSheet.Cell(1, 8).Style.Font.Bold = true;
                    objWorkSheet.Cell(1, 8).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                    // Facebook
                    objWorkSheet.Cell(1, 9).Value = "Facebook";
                    objWorkSheet.Cell(1, 9).Style.Font.Bold = true;
                    objWorkSheet.Cell(1, 9).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                    // Type
                    objWorkSheet.Cell(1, 10).Value = "Loại";
                    objWorkSheet.Cell(1, 10).Style.Font.Bold = true;
                    objWorkSheet.Cell(1, 10).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                    // Total Score
                    objWorkSheet.Cell(1, 11).Value = "Tổng điểm";
                    objWorkSheet.Cell(1, 11).Style.Font.Bold = true;
                    objWorkSheet.Cell(1, 11).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;

                    // Add Data to row
                    for (int nRowIndex = 0; nRowIndex < lstMemberRegistEvt.Count; nRowIndex++)
                    {
                        objExportMemberRegistEvt = lstMemberRegistEvt[nRowIndex];
                        objWorkSheet.Cell(nRowIndex + 2, 1).Value = nRowIndex + 1;
                        objWorkSheet.Cell(nRowIndex + 2, 2).Value = objExportMemberRegistEvt.StudentId;
                        objWorkSheet.Cell(nRowIndex + 2, 3).Value = objExportMemberRegistEvt.FirstName;
                        objWorkSheet.Cell(nRowIndex + 2, 4).Value = objExportMemberRegistEvt.LastName;
                        objWorkSheet.Cell(nRowIndex + 2, 5).Value = objExportMemberRegistEvt.Faculty;
                        objWorkSheet.Cell(nRowIndex + 2, 6).Value = objExportMemberRegistEvt.ClassName;
                        objWorkSheet.Cell(nRowIndex + 2, 7).Value = objExportMemberRegistEvt.PhoneNumber;
                        objWorkSheet.Cell(nRowIndex + 2, 8).Value = objExportMemberRegistEvt.Email;
                        objWorkSheet.Cell(nRowIndex + 2, 9).Value = objExportMemberRegistEvt.FacebookPath;
                        objWorkSheet.Cell(nRowIndex + 2, 10).Value = objExportMemberRegistEvt.IsMember == true ? "Thành viên" : "CTV";
                        objWorkSheet.Cell(nRowIndex + 2, 11).Value = objExportMemberRegistEvt.TotalScore;

                        if (objExportMemberRegistEvt.TotalScore <= 0)
                        {
                            objWorkSheet.Range(nRowIndex + 2, 1, nRowIndex + 2, 11).Style.Fill.BackgroundColor = XLColor.Yellow;
                            continue;
                        }

                        if (x_lstExportMemberScore == null)
                        {
                            continue;
                        }
                        objExportMemberScore = x_lstExportMemberScore.Find(obj => obj.MemberId.Equals(objExportMemberRegistEvt.MemberId) == true);
                        if (objExportMemberScore == null)
                        {
                            continue;
                        }

                        nDuplicate = 0;
                        do
                        {
                            if (nDuplicate == 0)
                            {
                                strSheetName = objExportMemberRegistEvt.StudentId;
                            }
                            else
                            {
                                strSheetName = string.Format("{0}_{1}", objExportMemberRegistEvt.StudentId, nDuplicate);
                            }
                            if (objWorkbook.Worksheets.Contains(strSheetName) == false)
                            {
                                break;
                            }
                            nDuplicate++;
                        } while (true);

                        // Add sub work sheet
                        objSubWorkSheet = objWorkbook.Worksheets.Add(strSheetName);
                        objSubWorkSheet.Cell(1, 1).Value = "Mã Sinh Viên";
                        objSubWorkSheet.Cell(1, 1).Style.Font.Bold = true;
                        objSubWorkSheet.Cell(1, 2).Value = objExportMemberRegistEvt.StudentId;
                        objSubWorkSheet.Cell(1, 2).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
                        objSubWorkSheet.Range("B1:D1").Row(1).Merge();

                        objSubWorkSheet.Cell(2, 1).Value = "Họ & Tên";
                        objSubWorkSheet.Cell(2, 1).Style.Font.Bold = true;
                        objSubWorkSheet.Cell(2, 2).Value = string.Format("{0} {1}", objExportMemberRegistEvt.FirstName, objExportMemberRegistEvt.LastName);
                        objSubWorkSheet.Range("B2:D2").Row(1).Merge();

                        objSubWorkSheet.Cell(3, 1).Value = "Loại";
                        objSubWorkSheet.Cell(3, 1).Style.Font.Bold = true;
                        objSubWorkSheet.Cell(3, 2).Value = objExportMemberRegistEvt.IsMember == true ? "Thành viên" : "CTV";
                        objSubWorkSheet.Range("B3:D3").Row(1).Merge();

                        objSubWorkSheet.Cell(4, 1).Value = "Tổng điểm";
                        objSubWorkSheet.Cell(4, 1).Style.Font.Bold = true;
                        objSubWorkSheet.Cell(4, 2).Value = string.Format("{0:F1}", objExportMemberRegistEvt.TotalScore);
                        objSubWorkSheet.Range("B4:D4").Row(1).Merge();

                        objSubWorkSheet.Cell("E1").Value = "Quay về trang Tổng Hợp";
                        objSubWorkSheet.Cell("E1").Style.Font.Bold = true;
                        objSubWorkSheet.Cell("E1").Style.Alignment.WrapText = true;
                        objSubWorkSheet.Cell("E1").Style.Alignment.Vertical = XLAlignmentVerticalValues.Center;
                        objSubWorkSheet.Cell("E1").Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                        objSubWorkSheet.Cell("E1").SetHyperlink(new XLHyperlink(string.Format("'{0}'!B{1}", strHomeSheetName, (nRowIndex + 2))));
                        objSubWorkSheet.Range("E1:E4").Style.Fill.BackgroundColor = XLColor.LightBlue;
                        objSubWorkSheet.Range("E1:E4").Column(1).Merge();

                        // Set cell border
                        objSubWorkSheet.Range(1, 1, 4, 4).Style.Border.TopBorder = XLBorderStyleValues.Thin;
                        objSubWorkSheet.Range(1, 1, 4, 4).Style.Border.InsideBorder = XLBorderStyleValues.Dotted;
                        objSubWorkSheet.Range(1, 1, 4, 4).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                        objSubWorkSheet.Range(1, 1, 4, 4).Style.Border.LeftBorder = XLBorderStyleValues.Thin;
                        objSubWorkSheet.Range(1, 1, 4, 4).Style.Border.RightBorder = XLBorderStyleValues.Thin;
                        objSubWorkSheet.Range(1, 1, 4, 4).Style.Border.TopBorder = XLBorderStyleValues.Thin;

                        // Score Data
                        objSubWorkSheet.Cell(6, 1).Value = "Bảng Điểm Danh Chi Tiết";
                        objSubWorkSheet.Cell(6, 1).Style.Font.Bold = true;
                        objSubWorkSheet.Cell(6, 1).Style.Alignment.Vertical = XLAlignmentVerticalValues.Center;
                        objSubWorkSheet.Cell(6, 1).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                        objSubWorkSheet.Range("A6:E6").Row(1).Merge();

                        objSubWorkSheet.Cell(7, 1).Value = "STT";
                        objSubWorkSheet.Cell(7, 1).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                        objSubWorkSheet.Cell(7, 1).Style.Font.Bold = true;

                        objSubWorkSheet.Cell(7, 2).Value = "Thời gian vào";
                        objSubWorkSheet.Cell(7, 2).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                        objSubWorkSheet.Cell(7, 2).Style.Font.Bold = true;

                        objSubWorkSheet.Cell(7, 3).Value = "Thời gian ra";
                        objSubWorkSheet.Cell(7, 3).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                        objSubWorkSheet.Cell(7, 3).Style.Font.Bold = true;

                        objSubWorkSheet.Cell(7, 4).Value = "Người quản lý";
                        objSubWorkSheet.Cell(7, 4).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                        objSubWorkSheet.Cell(7, 4).Style.Font.Bold = true;

                        objSubWorkSheet.Cell(7, 5).Value = "Điểm hoạt động";
                        objSubWorkSheet.Cell(7, 5).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                        objSubWorkSheet.Cell(7, 5).Style.Font.Bold = true;

                        nSkipCount = 0;
                        for (int nScoreIndex = 0; nScoreIndex < objExportMemberScore.ScoreData.Count; nScoreIndex++)
                        {
                            if (objExportMemberScore.ScoreData[nScoreIndex].Score <= 0)
                            {
                                nSkipCount++;
                                continue;
                            }
                            objSubWorkSheet.Cell(nScoreIndex + 8 - nSkipCount, 1).Value = nScoreIndex + 1 - nSkipCount;
                            objSubWorkSheet.Cell(nScoreIndex + 8 - nSkipCount, 2).Value = objExportMemberScore.ScoreData[nScoreIndex].EntryTime;
                            objSubWorkSheet.Cell(nScoreIndex + 8 - nSkipCount, 3).Value = objExportMemberScore.ScoreData[nScoreIndex].TimeToLeave;
                            objSubWorkSheet.Cell(nScoreIndex + 8 - nSkipCount, 4).Value = objExportMemberScore.ScoreData[nScoreIndex].ManagerName;
                            objSubWorkSheet.Cell(nScoreIndex + 8 - nSkipCount, 5).Value = objExportMemberScore.ScoreData[nScoreIndex].Score;
                        }
                        // Set cell border
                        objSubWorkSheet.Range(6, 1, objExportMemberScore.ScoreData.Count + 7 - nSkipCount, 5).Style.Border.TopBorder = XLBorderStyleValues.Thin;
                        objSubWorkSheet.Range(6, 1, objExportMemberScore.ScoreData.Count + 7 - nSkipCount, 5).Style.Border.InsideBorder = XLBorderStyleValues.Dotted;
                        objSubWorkSheet.Range(6, 1, objExportMemberScore.ScoreData.Count + 7 - nSkipCount, 5).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                        objSubWorkSheet.Range(6, 1, objExportMemberScore.ScoreData.Count + 7 - nSkipCount, 5).Style.Border.LeftBorder = XLBorderStyleValues.Thin;
                        objSubWorkSheet.Range(6, 1, objExportMemberScore.ScoreData.Count + 7 - nSkipCount, 5).Style.Border.RightBorder = XLBorderStyleValues.Thin;
                        objSubWorkSheet.Range(6, 1, objExportMemberScore.ScoreData.Count + 7 - nSkipCount, 5).Style.Border.TopBorder = XLBorderStyleValues.Thin;
                        objWorkSheet.Cell(nRowIndex + 2, 2).SetHyperlink(new XLHyperlink(string.Format("'{0}'!A1", strSheetName)));
                        objSubWorkSheet.Columns().AdjustToContents();
                    }

                    // Set cell border
                    objWorkSheet.Range(1, 1, lstMemberRegistEvt.Count + 1, 11).Style.Border.TopBorder = XLBorderStyleValues.Thin;
                    objWorkSheet.Range(1, 1, lstMemberRegistEvt.Count + 1, 11).Style.Border.InsideBorder = XLBorderStyleValues.Dotted;
                    objWorkSheet.Range(1, 1, lstMemberRegistEvt.Count + 1, 11).Style.Border.OutsideBorder = XLBorderStyleValues.Thin;
                    objWorkSheet.Range(1, 1, lstMemberRegistEvt.Count + 1, 11).Style.Border.LeftBorder = XLBorderStyleValues.Thin;
                    objWorkSheet.Range(1, 1, lstMemberRegistEvt.Count + 1, 11).Style.Border.RightBorder = XLBorderStyleValues.Thin;
                    objWorkSheet.Range(1, 1, lstMemberRegistEvt.Count + 1, 11).Style.Border.TopBorder = XLBorderStyleValues.Thin;

                    objWorkSheet.Columns().AdjustToContents();

                    // Create folder
                    strFolderPath = Path.GetDirectoryName(x_strFilePath);
                    if (Directory.Exists(strFolderPath) == false)
                    {
                        Directory.CreateDirectory(strFolderPath);
                    }

                    // Save to file
                    using (objFileStream = new FileStream(x_strFilePath, FileMode.Create))
                    {
                        objWorkbook.SaveAs(objFileStream);
                    }
                }

                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Read File Excel
        /// </summary>
        public static List<T> ReadFileExcel<T>(string x_strFilePath) where T : class
        {
            List<T> lstObject;
            T obj;
            T objEmpty;
            XLWorkbook objXLWorkbook;
            PropertyInfo[] arrProperties;
            Dictionary<string, int> dicColIndex;
            IXLWorksheet objWorksheet;
            IXLRow objRow;
            DateTime dtValue;
            int nValue;
            int nColNumber;
            int nRowCount;
            double dValue;
            double fValue;
            string strValue;
            string strObjEmptyJson;
            string strObjValJson;
            string strPropertyName;

            lstObject = new List<T>();
            // Skip if file is not exists
            if ((string.IsNullOrWhiteSpace(x_strFilePath) == true) || (File.Exists(x_strFilePath) == false))
            {
                return lstObject;
            }

            objXLWorkbook = new XLWorkbook(x_strFilePath);
            if (objXLWorkbook == null)
            {
                return lstObject;
            }

            // Get First Work Sheet
            objWorksheet = objXLWorkbook.Worksheet(1);
            if (objWorksheet == null)
            {
                return lstObject;
            }

            // Get Properties
            arrProperties = typeof(T).GetProperties();
            // Get Header
            objRow = objWorksheet.Row(1);
            dicColIndex = new Dictionary<string, int>();
            foreach (PropertyInfo objProperty in arrProperties)
            {
                strPropertyName = objProperty.Name;
                for (int nColIndex = 1; nColIndex <= objRow.CellCount(); nColIndex++)
                {
                    objRow.Cell(nColIndex).TryGetValue(out strValue);
                    if (strPropertyName.EqualsIgnoreCase(strValue) == true)
                    {
                        dicColIndex[strPropertyName] = nColIndex;
                        break;
                    }
                }
                if (string.IsNullOrEmpty(strPropertyName) == true)
                {
                    break;
                }
            }

            objEmpty = (T)Activator.CreateInstance(typeof(T));
            strObjEmptyJson = objEmpty.ToJSON();
            nRowCount = objWorksheet.RowCount();
            // set value
            for (int nRowIndex = 2; nRowIndex <= nRowCount; nRowIndex++)
            {
                objRow = objWorksheet.Row(nRowIndex);
                obj = (T)Activator.CreateInstance(typeof(T));
                // Loop all property in object
                foreach (PropertyInfo objProperty in arrProperties)
                {
                    strPropertyName = objProperty.Name;
                    if (dicColIndex.TryGetValue(strPropertyName, out nColNumber) == false)
                    {
                        continue;
                    }

                    // Type string
                    if (objProperty.PropertyType == typeof(string))
                    {
                        if ((objRow.Cell(nColNumber).TryGetValue(out strValue) == false) || (string.IsNullOrWhiteSpace(strValue) == true))
                        {
                            continue;
                        }

                        objProperty.SetValue(obj, strValue);
                    }
                    // type int
                    else if (objProperty.PropertyType == typeof(int))
                    {
                        if (objRow.Cell(nColNumber).TryGetValue(out nValue) == false)
                        {
                            continue;
                        }

                        objProperty.SetValue(obj, nValue);
                    }
                    // type float
                    else if (objProperty.PropertyType == typeof(float))
                    {
                        if (objRow.Cell(nColNumber).TryGetValue(out fValue) == false)
                        {
                            continue;
                        }

                        objProperty.SetValue(obj, fValue);
                    }
                    // type double
                    else if (objProperty.PropertyType == typeof(double))
                    {
                        if (objRow.Cell(nColNumber).TryGetValue(out dValue) == false)
                        {
                            continue;
                        }

                        objProperty.SetValue(obj, dValue);
                    }
                    // Type DateTime
                    else if (objProperty.PropertyType == typeof(DateTime))
                    {
                        if (objRow.Cell(nColNumber).TryGetValue(out dtValue) == false)
                        {
                            continue;
                        }

                        objProperty.SetValue(obj, dtValue);
                    }
                }

                strObjValJson = obj.ToJSON();
                if (strObjValJson.Equals(strObjEmptyJson) == false)
                {
                    // Add object to list
                    lstObject.Add(obj);
                }
                else
                {
                    break;
                }
            }

            objXLWorkbook.Dispose();
            return lstObject;
        }

        /// <summary>
        /// To DataTable
        /// </summary>
        private static DataTable ToDataTable<T>(this List<T> x_lstData)
        {
            DataTable objDataTable;
            PropertyDescriptorCollection objPropertyDesColl;
            PropertyDescriptor objPropertyDes;
            Type objType;
            object[] arrValues;

            try
            {
                objDataTable = new DataTable();
                objPropertyDesColl = TypeDescriptor.GetProperties(typeof(T));
                for (int nPropertyIndex = 0; nPropertyIndex < objPropertyDesColl.Count; nPropertyIndex++)
                {
                    objPropertyDes = objPropertyDesColl[nPropertyIndex];
                    objType = objPropertyDes.PropertyType;

                    if (objType.IsGenericType && objType.GetGenericTypeDefinition() == typeof(Nullable<>))
                    {
                        objType = Nullable.GetUnderlyingType(objType);
                    }

                    if (string.IsNullOrWhiteSpace(objPropertyDes.DisplayName) == true)
                    {
                        objDataTable.Columns.Add(objPropertyDes.Name, objType);
                    }
                    else
                    {
                        objDataTable.Columns.Add(objPropertyDes.DisplayName, objType);
                    }
                }
                arrValues = new object[objPropertyDesColl.Count];
                foreach (T objItem in x_lstData)
                {
                    for (int nCellIndex = 0; nCellIndex < arrValues.Length; nCellIndex++)
                    {
                        arrValues[nCellIndex] = objPropertyDesColl[nCellIndex].GetValue(objItem);
                    }
                    objDataTable.Rows.Add(arrValues);
                }
                return objDataTable;
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Save Form File
        /// </summary>
        public static async Task<string> SaveFormFile(this IFormFile x_objFile, string x_strFilePath, bool x_bOverwrite = true)
        {
            string strError = string.Empty;
            string strFolderPath;

            try
            {
                if (string.IsNullOrWhiteSpace(x_strFilePath) == true)
                {
                    strError = ErrorConstant.FILE_NAME_IS_NULL;
                    return strError;
                }

                // Check and Create folder
                strFolderPath = Path.GetDirectoryName(x_strFilePath);
                if (Directory.Exists(strFolderPath) == false)
                {
                    Directory.CreateDirectory(strFolderPath);
                }

                // Check file is exist
                if (File.Exists(x_strFilePath) == true)
                {
                    // Return false if Overwrite is false
                    if (x_bOverwrite == false)
                    {
                        strError = ErrorConstant.FILE_IS_EXIST;
                        return strError;
                    }
                    // delete file if Overwrite is true
                    else
                    {
                        File.Delete(x_strFilePath);
                    }
                }

                // Copy file to HDD
                using (FileStream fileStream = new FileStream(x_strFilePath, FileMode.Create))
                {
                    await x_objFile.CopyToAsync(fileStream);
                }
                return strError;
            }
            catch (Exception ex)
            {
                strError = ex.Message;
                return strError;
            }
        }

        /// <summary>
        /// Save Bitmap
        /// </summary>
        /// <param name="x_objBitmap"></param>
        /// <param name="x_strFilePath"></param>
        /// <returns></returns>
        public static bool SaveBitmapStream(this Bitmap x_objBitmap, string x_strFilePath)
        {
            ImageFormat objImageFormat;
            string strFolderPath;

            if (x_objBitmap == null)
            {
                return false;
            }

            try
            {
                // Delete old file
                if (File.Exists(x_strFilePath) == true)
                {
                    File.Delete(x_strFilePath);
                }
                // Create Folder
                strFolderPath = Path.GetDirectoryName(x_strFilePath);
                if (Directory.Exists(strFolderPath) == false)
                {
                    Directory.CreateDirectory(strFolderPath);
                }
                // Save file
                using (FileStream objFS = new FileStream(x_strFilePath, FileMode.Create))
                {
                    objImageFormat = ImageHelper.GetImageFormat(x_strFilePath);
                    x_objBitmap.Save(objFS, objImageFormat);
                    x_objBitmap.Dispose();
                    objFS.Dispose();
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Delete File
        /// </summary>
        public static bool DeleteFile(string x_strFilePath, out string x_strError)
        {
            string strFolderPath;
            try
            {
                x_strError = string.Empty;
                if (string.IsNullOrWhiteSpace(x_strFilePath) == true)
                {
                    x_strError = ErrorConstant.FILE_NAME_IS_NULL;
                    return false;
                }

                if (File.Exists(x_strFilePath) == false)
                {
                    x_strError = ErrorConstant.FILE_IS_NOT_EXIST;
                    return false;
                }

                File.Delete(x_strFilePath);
                strFolderPath = Path.GetDirectoryName(x_strFilePath);
                if (Directory.GetFiles(strFolderPath).Length == 0)
                {
                    Directory.Delete(strFolderPath);
                }
                return true;
            }
            catch (Exception ex)
            {
                x_strError = ex.Message;
                return false;
            }
        }

        /// <summary>
        /// Get Folder fulle Path
        /// </summary>
        public static string GetFolderFullPath(string x_strFolderName1, bool x_bIsAppPath = false)
        {
            string strFolderPath;
            string strRootPath;
            try
            {
                if (string.IsNullOrWhiteSpace(x_strFolderName1) == true)
                {
                    return string.Empty;
                }
                //
                if (string.IsNullOrWhiteSpace(SysFolder.WWWROOT_PATH) == true)
                {
                    SysFolder.WWWROOT_PATH = Path.Combine(SysFolder.APP_PATH, "wwwroot");
                }
                // Get Root path
                strRootPath = x_bIsAppPath == true ? SysFolder.APP_PATH : SysFolder.WWWROOT_PATH;
                // create folder full path
                strFolderPath = Path.Combine(strRootPath, x_strFolderName1);

                return strFolderPath;
            }
            catch
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Read Text File
        /// </summary>
        /// <param name="x_strFilePath"></param>
        /// <returns></returns>
        public static string ReadTextFile(string x_strFilePath)
        {
            string strFileContent;

            try
            {
                if (File.Exists(x_strFilePath) == false)
                {
                    return string.Empty;
                }

                using (FileStream objFileSt = new FileStream(x_strFilePath, FileMode.Open, FileAccess.Read))
                {
                    using (StreamReader objStreamReader = new StreamReader(objFileSt))
                    {
                        strFileContent = objStreamReader.ReadToEnd();
                        return strFileContent;
                    }
                }
            }
            catch
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Save Text File
        /// </summary>
        /// <param name="x_strContent"></param>
        /// <param name="x_strFilePath"></param>
        /// <returns></returns>
        public static bool SaveTextFile(string x_strContent, string x_strFilePath)
        {
            string strFolderPath;

            try
            {
                if (string.IsNullOrEmpty(x_strContent) == true)
                {
                    return false;
                }

                strFolderPath = Path.GetDirectoryName(x_strFilePath);
                if (Directory.Exists(strFolderPath) == false)
                {
                    Directory.CreateDirectory(strFolderPath);
                }

                if (File.Exists(x_strFilePath) == true)
                {
                    File.Delete(x_strFilePath);
                }

                using (FileStream objFileStream = File.Create(x_strFilePath))
                {
                    using (StreamWriter objstWrite = new StreamWriter(objFileStream))
                    {
                        // Write log
                        objstWrite.WriteLine(x_strContent);
                        objstWrite.Close();
                    }
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Read Image File
        /// </summary>
        /// <param name="x_strFilePath"></param>
        /// <returns></returns>
        public static Image ReadImageFile(string x_strFilePath)
        {
            Image objImage;
            try
            {
                if (File.Exists(x_strFilePath) == false)
                {
                    return null;
                }

                using (FileStream objFileSt = new FileStream(x_strFilePath, FileMode.Open, FileAccess.Read))
                {
                    objImage = Image.FromStream(objFileSt);
                    return objImage;
                }
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Create File Name By Title
        /// </summary>
        /// <param name="x_strTitle"></param>
        /// <param name="x_strExtension"></param>
        /// <returns></returns>
        public static string CreateFileNameByTitle(string x_strTitle, string x_strExtension = null)
        {
            DateTime objCurrentDate;
            string strFileName;

            objCurrentDate = DateTimeHelper.GetCurrentDateTimeVN();

            if (string.IsNullOrWhiteSpace(x_strTitle) == true || string.IsNullOrWhiteSpace(x_strExtension) == true)
            {
                return string.Empty;
            }

            strFileName = x_strTitle.Trim().RemoveUnicode().Replace(' ', '_');
            strFileName += string.Format("_{0}", objCurrentDate.Ticks);
            if (string.IsNullOrWhiteSpace(x_strExtension) == false)
            {
                strFileName = Path.ChangeExtension(strFileName, x_strExtension.Trim());
            }
            return strFileName;
        }

        /// <summary>
        /// Get Random File Name
        /// </summary>
        /// <param name="x_strFileExtension"></param>
        /// <returns></returns>
        public static string GetRandomFileName(string x_strFileExtension)
        {
            const string STR_CHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            string strFileName;
            Random objRandom;
            DateTime objCurrentDate;

            objCurrentDate = DateTimeHelper.GetCurrentDateTimeVN();
            objRandom = new Random();
            // Random 32 chars
            // There are 62^32 ways to choose 32 characters from a set of 32 elements. Probability to generate 2 identical token is 1/62^32
            strFileName = new string(Enumerable.Repeat(STR_CHARS, 32).Select(str => str[objRandom.Next(str.Length)]).ToArray());
            strFileName += string.Format("_{0}", objCurrentDate.Ticks);
            strFileName = Path.ChangeExtension(strFileName, x_strFileExtension.Trim());
            return strFileName;
        }

        /// <summary>
        /// Get Folder fulle Path
        /// </summary>
        public static string GetFolderFullPath(string x_strFolderName1, string x_strFolderName2, bool x_bIsAppPath = false)
        {
            string strFolderPath;
            string strRootPath;
            try
            {
                if (string.IsNullOrWhiteSpace(x_strFolderName1) == true)
                {
                    return string.Empty;
                }

                if (string.IsNullOrWhiteSpace(x_strFolderName2) == true)
                {
                    return string.Empty;
                }
                //
                if (string.IsNullOrWhiteSpace(SysFolder.WWWROOT_PATH) == true)
                {
                    SysFolder.WWWROOT_PATH = Path.Combine(SysFolder.APP_PATH, "wwwroot");
                }
                // Get Root path
                strRootPath = x_bIsAppPath == true ? SysFolder.APP_PATH : SysFolder.WWWROOT_PATH;
                // create folder full path
                strFolderPath = Path.Combine(strRootPath, x_strFolderName1, x_strFolderName2);

                return strFolderPath;
            }
            catch
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Get Folder fulle Path
        /// </summary>
        public static string GetFolderFullPath(string x_strFolderName1, string x_strFolderName2, string x_strFolderName3, bool x_bIsAppPath = false)
        {
            string strFolderPath;
            string strRootPath;
            try
            {
                if (string.IsNullOrWhiteSpace(x_strFolderName1) == true)
                {
                    return string.Empty;
                }

                if (string.IsNullOrWhiteSpace(x_strFolderName2) == true)
                {
                    return string.Empty;
                }

                if (string.IsNullOrWhiteSpace(x_strFolderName3) == true)
                {
                    return string.Empty;
                }
                //
                if (string.IsNullOrWhiteSpace(SysFolder.WWWROOT_PATH) == true)
                {
                    SysFolder.WWWROOT_PATH = Path.Combine(SysFolder.APP_PATH, "wwwroot");
                }
                // Get Root path
                strRootPath = x_bIsAppPath == true ? SysFolder.APP_PATH : SysFolder.WWWROOT_PATH;
                // create folder full path
                strFolderPath = Path.Combine(strRootPath, x_strFolderName1, x_strFolderName2, x_strFolderName3);

                return strFolderPath;
            }
            catch
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Open image file
        /// </summary>
        public static Bitmap OpenImageFile(string x_strFilePath)
        {
            Bitmap objImage;

            try
            {
                using (Stream objStream = File.Open(x_strFilePath, FileMode.Open))
                {
                    objImage = (Bitmap)Image.FromStream(objStream);
                    objStream.Dispose();
                    return objImage;
                }
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Get File Path
        /// </summary>
        public static string GetFilePath(string x_strFolderName1, string x_strFileName, bool x_bIsAppPath = false)
        {
            string strFolderPath;
            string strFilePath;

            try
            {
                // Skip if file name is null or white space
                if (string.IsNullOrWhiteSpace(x_strFileName) == true)
                {
                    return string.Empty;
                }
                // Get Folder full path
                strFolderPath = GetFolderFullPath(x_strFolderName1, x_bIsAppPath);
                if (string.IsNullOrWhiteSpace(strFolderPath) == true)
                {
                    return string.Empty;
                }

                // Create file path
                strFilePath = Path.Combine(strFolderPath, x_strFileName);
                return strFilePath;
            }
            catch
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Get File Path
        /// </summary>
        public static string GetFilePath(string x_strFolderName1, string x_strFolderName2, string x_strFileName, bool x_bIsAppPath = false)
        {
            string strFolderPath;
            string strFilePath;

            try
            {
                // Skip if file name is null or white space
                if (string.IsNullOrWhiteSpace(x_strFileName) == true)
                {
                    return string.Empty;
                }
                // Get Folder full path
                strFolderPath = GetFolderFullPath(x_strFolderName1, x_strFolderName2, x_bIsAppPath);
                if (string.IsNullOrWhiteSpace(strFolderPath) == true)
                {
                    return string.Empty;
                }

                // Create file path
                strFilePath = Path.Combine(strFolderPath, x_strFileName);
                return strFilePath;
            }
            catch
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Get File Path
        /// </summary>
        public static string GetFilePath(string x_strFolderName1, string x_strFolderName2, string x_strFolderName3, string x_strFileName, bool x_bIsAppPath = false)
        {
            string strFolderPath;
            string strFilePath;

            try
            {
                // Skip if file name is null or white space
                if (string.IsNullOrWhiteSpace(x_strFileName) == true)
                {
                    return string.Empty;
                }
                // Get Folder full path
                strFolderPath = GetFolderFullPath(x_strFolderName1, x_strFolderName2, x_strFolderName3, x_bIsAppPath);
                if (string.IsNullOrWhiteSpace(strFolderPath) == true)
                {
                    return string.Empty;
                }

                // Create file path
                strFilePath = Path.Combine(strFolderPath, x_strFileName);
                return strFilePath;
            }
            catch
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Create Folder Name
        /// </summary>
        public static string CreateFolderName(string x_strName)
        {
            string strFolderName;
            if (string.IsNullOrWhiteSpace(x_strName) == true)
            {
                return string.Empty;
            }

            try
            {
                // Trim
                strFolderName = x_strName.Trim();
                // Remove Unicode
                strFolderName = strFolderName.RemoveUnicode();
                // To Title Case
                strFolderName = strFolderName.ToTitleCase();
                // Remove space char
                strFolderName = strFolderName.Replace(" ", "");
                if (strFolderName.Length > 250)
                {
                    strFolderName = strFolderName.Substring(0, 250);
                }
            }
            catch
            {
                strFolderName = x_strName;
            }

            return strFolderName;
        }

        /// <summary>
        /// Copy File
        /// </summary>
        public static bool CopyFile(string x_strSource, string x_strDestination, bool x_bIsDeleteSourceFile = false)
        {
            string strFolderPath;
            if ((string.IsNullOrWhiteSpace(x_strSource) == true) || (string.IsNullOrWhiteSpace(x_strDestination) == true))
            {
                return false;
            }

            try
            {
                // Create folder
                strFolderPath = Path.GetDirectoryName(x_strDestination);
                if (Directory.Exists(strFolderPath) == false)
                {
                    Directory.CreateDirectory(strFolderPath);
                }

                File.Copy(x_strSource, x_strDestination, true);
                // Delete source file
                if ((x_bIsDeleteSourceFile == true) && (File.Exists(x_strSource) == true))
                {
                    File.Delete(x_strSource);
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Get Mime Type
        /// </summary>
        /// <param name="x_strFileName"></param>
        /// <returns></returns>
        public static string GetMimeType(this string x_strFileName)
        {
            FileExtensionContentTypeProvider objProvider;
            try
            {
                objProvider = new FileExtensionContentTypeProvider();
                if (objProvider.TryGetContentType(x_strFileName, out string strContentType) == false)
                {
                    strContentType = "application/octet-stream";
                }
                return strContentType;
            }
            catch
            {
                return "application/octet-stream";
            }
        }

        /// <summary>
        /// Create Zip File
        /// </summary>
        /// <param name="x_strFolderPath"></param>
        /// <param name="x_strFileExportPath"></param>
        /// <returns></returns>
        public static bool CreateZipFile(string x_strFolderPath, string x_strFileExportPath)
        {
            string strFolderPath;
            try
            {
                strFolderPath = Path.GetDirectoryName(x_strFileExportPath);
                if (Directory.Exists(strFolderPath) == false)
                {
                    Directory.CreateDirectory(strFolderPath);
                }
                ZipFile.CreateFromDirectory(x_strFolderPath, x_strFileExportPath);
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
