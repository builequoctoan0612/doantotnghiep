﻿
using Newtonsoft.Json;
using System.Runtime.Serialization.Formatters.Binary;
using System.Security.Cryptography;

namespace DTUSVCv3_Library
{
    public static class ObjHelper
    {
        private static object m_objSaveLock;

        /// <summary>
        /// Convert object to JSON string
        /// </summary>
        public static string ToJSON<T>(this T x_obj)
        {
            string strJson;
            try
            {
                if (x_obj == null)
                {
                    return string.Empty;
                }

                strJson = JsonConvert.SerializeObject(x_obj, Formatting.Indented);
                return strJson;
            }
            catch
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Deep Clone
        /// </summary>
        public static T DeepClone<T>(this T x_obj)
        {
            MemoryStream objMemoryStream;
            BinaryFormatter objFormatter;

            try
            {
                using (objMemoryStream = new MemoryStream())
                {
                    objFormatter = new BinaryFormatter();
                    objFormatter.Serialize(objMemoryStream, x_obj);
                    objMemoryStream.Position = 0;

                    return (T)objFormatter.Deserialize(objMemoryStream);
                }
            }
            catch
            {
                return default(T);
            }
        }

        /// <summary>
        /// Convert JSON to object
        /// </summary>
        public static T JSONToObject<T>(this string x_strJSON)
        {
            T obj;

            try
            {
                if (string.IsNullOrWhiteSpace(x_strJSON) == true)
                {
                    return default(T);
                }

                obj = JsonConvert.DeserializeObject<T>(x_strJSON);
                return obj;
            }
            catch
            {
                return default(T);
            }
        }

        /// <summary>
        /// Read file binary
        /// </summary>
        public static T ReadBinaryFile<T>(string x_strFilePath)
        {
            object? objData = null;
            FileStream objFS;
            BinaryFormatter objBF;
            T objRes;

            if(m_objSaveLock == null)
            {
                m_objSaveLock = new object();
            }

            lock (m_objSaveLock)
            {
                try
                {
                    if (File.Exists(x_strFilePath) == true)
                    {
                        using (objFS = new FileStream(x_strFilePath, FileMode.Open, FileAccess.Read, FileShare.Read))
                        {
                            using (CryptoStream objCryptoStream = DTUSVCv3Aes.CreateDecryptionStream(objFS))
                            {
                                objBF = new BinaryFormatter();
                                //読み込んで逆シリアル化する
                                objData = objBF.Deserialize(objCryptoStream);

                                objCryptoStream.Close();
                                objFS.Close();
                                // Convert Data
                                if (objData != null)
                                {
                                    objRes = (T)objData;
                                    return objRes;
                                }
                            }
                        }
                    }
                }
                catch
                {
                    // No need process
                }
                return default(T);
            }
        }
    }
}
