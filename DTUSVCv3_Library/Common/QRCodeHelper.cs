﻿//-------------------------------------------------------------------------
// Create               : Phan Ngoc Huong
//                                          
// Create Date          : 17/12/2022 
//                                          
// Description          : Create Qr-Code
//                                          
// Editing history      :                   
//-------------------------------------------------------------------------

using QRCoder;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;

namespace DTUSVCv3_Library
{
    public static class QRCodeHelper
    {
        /// <summary>
        /// Crteate Qr Code
        /// </summary>
        public static Bitmap? CreateQrCode(this string x_strText, bool x_bAddLogo = true)
        {
            string strLogoPath;
            QRCodeGenerator objQrGenerator;
            QRCodeData objQrCodeData;
            QRCode objQrCode;
            Bitmap objQrCodeImage;
            Bitmap objLogoImage;

            try
            {
                objQrGenerator = new QRCodeGenerator();
                // Create Qr-code
                objQrCodeData = objQrGenerator.CreateQrCode(x_strText, QRCodeGenerator.ECCLevel.Q);
                objQrCode = new QRCode(objQrCodeData);
                // Create Logo File Path
                strLogoPath = Path.Combine(SysFolder.WWWROOT_PATH, SysFolder.LOGO_FOLDER, SysFile.LOGO_FILE_NAME);
                if ((File.Exists(strLogoPath) == true) && (x_bAddLogo == true))
                {
                    objLogoImage = new Bitmap(strLogoPath);
                    objQrCodeImage = objQrCode.GetGraphic(50, Color.Black, Color.White, objLogoImage);
                }
                else
                {
                    objQrCodeImage = objQrCode.GetGraphic(50);
                }
                // Drop radius
                objQrCodeImage = DropRadiusQr(objQrCodeImage);
                return objQrCodeImage;
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Drop radius
        /// </summary>
        private static Bitmap? DropRadiusQr(Bitmap x_objImage)
        {
            int nWidth = x_objImage.Width;
            int nHeight = x_objImage.Height;
            Bitmap objTmpBitMap;

            objTmpBitMap = new Bitmap(nWidth, nHeight);
            using (Graphics objGraphic = Graphics.FromImage(objTmpBitMap))
            {
                objGraphic.Clear(Color.Transparent);
                objGraphic.SmoothingMode = SmoothingMode.AntiAlias;
                objGraphic.InterpolationMode = InterpolationMode.High;
                GraphicsPath path = MakeRoundedRect(
                    new Rectangle(0, 0, nWidth, nHeight),
                    50, 50,
                    true, true, true, true);

                TextureBrush brush = new TextureBrush(x_objImage);
                objGraphic.FillPath(brush, path);
            }
            return objTmpBitMap;
        }

        /// <summary>
        /// Make rounded rect
        /// </summary>
        private static GraphicsPath MakeRoundedRect(RectangleF x_objRect, float x_fXRadius, float x_fYRadius,
            bool x_bRoundUl, bool x_bRoundUr, bool x_bRoundlr, bool x_bRoundLl)
        {
            PointF objPoint1, objPoint2;
            GraphicsPath objGraphicsPath;
            RectangleF objCorner;

            objGraphicsPath = new GraphicsPath();
            if (x_bRoundUl == true)
            {
                objCorner = new RectangleF(
                    x_objRect.X, x_objRect.Y,
                    2 * x_fXRadius, 2 * x_fYRadius);

                objGraphicsPath.AddArc(objCorner, 180, 90);
                objPoint1 = new PointF(x_objRect.X + x_fXRadius, x_objRect.Y);
            }
            else
            {
                objPoint1 = new PointF(x_objRect.X, x_objRect.Y);
            }

            if (x_bRoundUr == true)
            {
                objPoint2 = new PointF(x_objRect.Right - x_fXRadius, x_objRect.Y);
            }
            else
            {
                objPoint2 = new PointF(x_objRect.Right, x_objRect.Y);
            }

            objGraphicsPath.AddLine(objPoint1, objPoint2);

            if (x_bRoundUr == true)
            {
                objCorner = new RectangleF(
                    x_objRect.Right - 2 * x_fXRadius, x_objRect.Y,
                    2 * x_fXRadius, 2 * x_fYRadius);

                objGraphicsPath.AddArc(objCorner, 270, 90);
                objPoint1 = new PointF(x_objRect.Right, x_objRect.Y + x_fYRadius);
            }
            else
            {
                objPoint1 = new PointF(x_objRect.Right, x_objRect.Y);
            }

            if (x_bRoundlr == true)
            {
                objPoint2 = new PointF(x_objRect.Right, x_objRect.Bottom - x_fYRadius);
            }
            else
            {
                objPoint2 = new PointF(x_objRect.Right, x_objRect.Bottom);
            }
            objGraphicsPath.AddLine(objPoint1, objPoint2);

            if (x_bRoundlr == true)
            {
                objCorner = new RectangleF(
                    x_objRect.Right - 2 * x_fXRadius,
                    x_objRect.Bottom - 2 * x_fYRadius,
                    2 * x_fXRadius, 2 * x_fYRadius);
                objGraphicsPath.AddArc(objCorner, 0, 90);
                objPoint1 = new PointF(x_objRect.Right - x_fXRadius, x_objRect.Bottom);
            }
            else
            {
                objPoint1 = new PointF(x_objRect.Right, x_objRect.Bottom);
            }

            if (x_bRoundLl == true)
            {
                objPoint2 = new PointF(x_objRect.X + x_fXRadius, x_objRect.Bottom);
            }
            else
            {
                objPoint2 = new PointF(x_objRect.X, x_objRect.Bottom);
            }
            objGraphicsPath.AddLine(objPoint1, objPoint2);

            if (x_bRoundLl == true)
            {
                objCorner = new RectangleF(
                    x_objRect.X, x_objRect.Bottom - 2 * x_fYRadius,
                    2 * x_fXRadius, 2 * x_fYRadius);
                objGraphicsPath.AddArc(objCorner, 90, 90);
                objPoint1 = new PointF(x_objRect.X, x_objRect.Bottom - x_fYRadius);
            }
            else
            {
                objPoint1 = new PointF(x_objRect.X, x_objRect.Bottom);
            }

            if (x_bRoundUl == true)
            {
                objPoint2 = new PointF(x_objRect.X, x_objRect.Y + x_fYRadius);
            }
            else
            {
                objPoint2 = new PointF(x_objRect.X, x_objRect.Y);
            }
            objGraphicsPath.AddLine(objPoint1, objPoint2);

            objGraphicsPath.CloseFigure();

            return objGraphicsPath;
        }
    }

    public class QRCode : AbstractQRCode, IDisposable
    {
        public QRCode()
        {
        }

        public QRCode(QRCodeData data)
            : base(data)
        {
        }

        /// <summary>
        /// Get Graphic
        /// </summary>
        public Bitmap GetGraphic(int x_nPixelsPerModule)
        {
            return GetGraphic(x_nPixelsPerModule, Color.Black, Color.White, x_bDrawQuietZones: true);
        }

        /// <summary>
        /// Get Graphic
        /// </summary>
        public Bitmap GetGraphic(int x_nPixelsPerModule, string x_strDarkColorHtmlHex, string x_strLightColorHtmlHex, bool x_DrawQuietZones = true)
        {
            return GetGraphic(x_nPixelsPerModule, ColorTranslator.FromHtml(x_strDarkColorHtmlHex), ColorTranslator.FromHtml(x_strLightColorHtmlHex), x_DrawQuietZones);
        }

        /// <summary>
        /// Get Graphic
        /// </summary>
        public Bitmap GetGraphic(int x_nPixelsPerModule, Color x_objDarkColor, Color x_objLightColor, bool x_bDrawQuietZones = true)
        {
            int nNum1 = (base.QrCodeData.ModuleMatrix.Count - ((!x_bDrawQuietZones) ? 8 : 0)) * x_nPixelsPerModule;
            int nNum2 = ((!x_bDrawQuietZones) ? (4 * x_nPixelsPerModule) : 0);
            Bitmap bmBitmap = new Bitmap(nNum1, nNum1);
            using Graphics objGraphics = Graphics.FromImage(bmBitmap);
            using SolidBrush objBrush2 = new SolidBrush(x_objLightColor);
            using SolidBrush objBrush1 = new SolidBrush(x_objDarkColor);
            for (int nPxIndex1 = 0; nPxIndex1 < nNum1 + nNum2; nPxIndex1 += x_nPixelsPerModule)
            {
                for (int nPxIndex2 = 0; nPxIndex2 < nNum1 + nNum2; nPxIndex2 += x_nPixelsPerModule)
                {
                    if (base.QrCodeData.ModuleMatrix[(nPxIndex2 + x_nPixelsPerModule) / x_nPixelsPerModule - 1][(nPxIndex1 + x_nPixelsPerModule) / x_nPixelsPerModule - 1])
                    {
                        objGraphics.FillRectangle(objBrush1, new Rectangle(nPxIndex1 - nNum2, nPxIndex2 - nNum2, x_nPixelsPerModule, x_nPixelsPerModule));
                    }
                    else
                    {
                        objGraphics.FillRectangle(objBrush2, new Rectangle(nPxIndex1 - nNum2, nPxIndex2 - nNum2, x_nPixelsPerModule, x_nPixelsPerModule));
                    }
                }
            }

            objGraphics.Save();
            return bmBitmap;
        }

        /// <summary>
        /// Get Graphic
        /// </summary>
        public Bitmap GetGraphic(int x_nPixelsPerModule, Color x_objDarkColor, Color x_objLightColor, Bitmap? x_bmIcon = null, int x_nIconSizePercent = 15, int x_nIconBorderWidth = 0, bool x_bDrawQuietZones = true, Color? x_objIconBackgroundColor = null)
        {
            int nNum1 = (base.QrCodeData.ModuleMatrix.Count - ((!x_bDrawQuietZones) ? 8 : 0)) * x_nPixelsPerModule;
            int nNum2 = ((!x_bDrawQuietZones) ? (4 * x_nPixelsPerModule) : 0);
            Bitmap bmBitmap = new Bitmap(nNum1, nNum1, PixelFormat.Format32bppArgb);
            using Graphics objGraphics = Graphics.FromImage(bmBitmap);
            using SolidBrush objBrush2 = new SolidBrush(x_objLightColor);
            using SolidBrush objBrush1 = new SolidBrush(x_objDarkColor);
            objGraphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
            objGraphics.CompositingQuality = CompositingQuality.HighQuality;
            objGraphics.Clear(x_objLightColor);
            bool bFlag = x_bmIcon != null && x_nIconSizePercent > 0 && x_nIconSizePercent <= 100;
            for (int nPxIndex1 = 0; nPxIndex1 < nNum1 + nNum2; nPxIndex1 += x_nPixelsPerModule)
            {
                for (int nPxIndex2 = 0; nPxIndex2 < nNum1 + nNum2; nPxIndex2 += x_nPixelsPerModule)
                {
                    SolidBrush brush = (base.QrCodeData.ModuleMatrix[(nPxIndex2 + x_nPixelsPerModule) / x_nPixelsPerModule - 1][(nPxIndex1 + x_nPixelsPerModule) / x_nPixelsPerModule - 1] ? objBrush1 : objBrush2);
                    objGraphics.FillRectangle(brush, new Rectangle(nPxIndex1 - nNum2, nPxIndex2 - nNum2, x_nPixelsPerModule, x_nPixelsPerModule));
                }
            }

            if (bFlag)
            {
                float fNum3 = (float)(x_nIconSizePercent * bmBitmap.Width) / 100f;
                float fNum4 = (bFlag ? (fNum3 * (float)x_bmIcon.Height / (float)x_bmIcon.Width) : 0f);
                float fNum5 = ((float)bmBitmap.Width - fNum3) / 2f;
                float fNum6 = ((float)bmBitmap.Height - fNum4) / 2f;
                RectangleF objRect = new RectangleF(fNum5 - (float)x_nIconBorderWidth, fNum6 - (float)x_nIconBorderWidth, fNum3 + (float)(x_nIconBorderWidth * 2), fNum4 + (float)(x_nIconBorderWidth * 2));
                RectangleF objDestRect = new RectangleF(fNum5, fNum6, fNum3, fNum4);
                SolidBrush objBrush3 = (x_objIconBackgroundColor.HasValue ? new SolidBrush(x_objIconBackgroundColor.Value) : objBrush2);
                if (x_nIconBorderWidth > 0)
                {
                    using GraphicsPath objGrPath = CreateRoundedRectanglePath(objRect, x_nIconBorderWidth * 2);
                    objGraphics.FillPath(objBrush3, objGrPath);
                }

                objGraphics.DrawImage(x_bmIcon, objDestRect, new RectangleF(0f, 0f, x_bmIcon.Width, x_bmIcon.Height), GraphicsUnit.Pixel);
            }

            objGraphics.Save();
            return bmBitmap;
        }

        /// <summary>
        /// Create Rounded Rectangle Path
        /// </summary>
        internal GraphicsPath CreateRoundedRectanglePath(RectangleF x_objRect, int x_nCornerRadius)
        {
            GraphicsPath objGrPath = new GraphicsPath();
            objGrPath.AddArc(x_objRect.X, x_objRect.Y, x_nCornerRadius * 2, x_nCornerRadius * 2, 180f, 90f);
            objGrPath.AddLine(x_objRect.X + (float)x_nCornerRadius, x_objRect.Y, x_objRect.Right - (float)(x_nCornerRadius * 2), x_objRect.Y);
            objGrPath.AddArc(x_objRect.X + x_objRect.Width - (float)(x_nCornerRadius * 2), x_objRect.Y, x_nCornerRadius * 2, x_nCornerRadius * 2, 270f, 90f);
            objGrPath.AddLine(x_objRect.Right, x_objRect.Y + (float)(x_nCornerRadius * 2), x_objRect.Right, x_objRect.Y + x_objRect.Height - (float)(x_nCornerRadius * 2));
            objGrPath.AddArc(x_objRect.X + x_objRect.Width - (float)(x_nCornerRadius * 2), x_objRect.Y + x_objRect.Height - (float)(x_nCornerRadius * 2), x_nCornerRadius * 2, x_nCornerRadius * 2, 0f, 90f);
            objGrPath.AddLine(x_objRect.Right - (float)(x_nCornerRadius * 2), x_objRect.Bottom, x_objRect.X + (float)(x_nCornerRadius * 2), x_objRect.Bottom);
            objGrPath.AddArc(x_objRect.X, x_objRect.Bottom - (float)(x_nCornerRadius * 2), x_nCornerRadius * 2, x_nCornerRadius * 2, 90f, 90f);
            objGrPath.AddLine(x_objRect.X, x_objRect.Bottom - (float)(x_nCornerRadius * 2), x_objRect.X, x_objRect.Y + (float)(x_nCornerRadius * 2));
            objGrPath.CloseFigure();
            return objGrPath;
        }
    }
}
