﻿using HtmlAgilityPack;
using System.Globalization;

namespace DTUSVCv3_Library
{
    public static class TextHelper
    {
        /// <summary>
        /// Remove Unicode
        /// </summary>
        public static string RemoveUnicode(this string x_strText)
        {
            string[] arrUnicodeChar;
            string[] arrASCIChar;

            if (string.IsNullOrWhiteSpace(x_strText) == true)
            {
                return x_strText;
            }

            arrUnicodeChar = new string[] { "á", "à", "ả", "ã", "ạ", "â", "ấ", "ầ", "ẩ", "ẫ", "ậ", "ă", "ắ", "ằ", "ẳ", "ẵ", "ặ",
            "đ",
            "é","è","ẻ","ẽ","ẹ","ê","ế","ề","ể","ễ","ệ",
            "í","ì","ỉ","ĩ","ị",
            "ó","ò","ỏ","õ","ọ","ô","ố","ồ","ổ","ỗ","ộ","ơ","ớ","ờ","ở","ỡ","ợ",
            "ú","ù","ủ","ũ","ụ","ư","ứ","ừ","ử","ữ","ự",
            "ý","ỳ","ỷ","ỹ","ỵ",};
            arrASCIChar = new string[] { "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a",
            "d",
            "e","e","e","e","e","e","e","e","e","e","e",
            "i","i","i","i","i",
            "o","o","o","o","o","o","o","o","o","o","o","o","o","o","o","o","o",
            "u","u","u","u","u","u","u","u","u","u","u",
            "y","y","y","y","y",};
            for (int i = 0; i < arrUnicodeChar.Length; i++)
            {
                x_strText = x_strText.Replace(arrUnicodeChar[i], arrASCIChar[i]);
                x_strText = x_strText.Replace(arrUnicodeChar[i].ToUpper(), arrASCIChar[i].ToUpper());
            }
            return x_strText;
        }

        /// <summary>
        /// Name Separation
        /// </summary>
        /// <param name="x_strFullName"></param>
        /// <param name="x_strFirstName"></param>
        /// <param name="x_strLastName"></param>
        /// <returns></returns>
        public static bool NameSeparation(this string x_strFullName, out string x_strFirstName, out string x_strLastName)
        {
            string[] arrWord;
            List<string> lstWord;

            x_strFirstName = string.Empty;
            x_strLastName = string.Empty;

            try
            {
                if (string.IsNullOrWhiteSpace(x_strFullName) == true)
                {
                    return false;
                }

                // Name separetion
                arrWord = x_strFullName.Split(' ');
                lstWord = arrWord.ToList();

                x_strLastName = lstWord[lstWord.Count - 1];
                x_strLastName = x_strLastName.Trim();
                lstWord.RemoveAt(lstWord.Count - 1);
                x_strFirstName = string.Join(" ", lstWord);
                x_strFirstName = x_strFirstName.Trim();
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Remove HTML Tag
        /// </summary>
        public static string RemoveHTMLTag(this string x_strText)
        {
            string strResult;
            HtmlDocument objHtmlDoc;

            try
            {
                if (string.IsNullOrWhiteSpace(x_strText) == true)
                {
                    return string.Empty;
                }
                objHtmlDoc = new HtmlDocument();
                objHtmlDoc.LoadHtml(x_strText);
                strResult = objHtmlDoc.DocumentNode.InnerText;
                return strResult;
            }
            catch
            {
                return x_strText;
            }
        }

        /// <summary>
        /// To Title Case
        /// </summary>
        public static string ToTitleCase(this string x_strText)
        {
            string strTitleCase;

            if (string.IsNullOrWhiteSpace(x_strText) == true)
            {
                return x_strText;
            }
            strTitleCase = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(x_strText.Trim().ToLower());
            return strTitleCase;
        }
    }
}
