﻿namespace DTUSVCv3_Library
{
    public class SuccsessConstant
    {
        public const string LOGIN_SUCCSESS = "Đăng nhập thành công.";
        public const string LOGOUT_SUCCSESS = "Đăng xuất thành công.";
        public const string REFESH_TOKEN_SUCCSESS = "Làm mới token thành công.";
        public const string CHANGE_PASSWD_SUCCSESS = "Thay đổi mật khẩu thành công.";
        public const string LOCK_SUCCSESS = "Khoá tài khoản thành công.";
        public const string UNLOCK_SUCCSESS = "Mở khoá tài khoản thành công.";
        public const string SEND_RESET_PASSWD_SUCCSESS = "Đường dẫn thay đổi mật khẩu đã được gửi qua email {0}.";
        public const string PASSWORD_RECOVERY_PATH_IS_STILL_ACTIVE = " Đường dẫn khôi phục mật khẩu còn hiệu lực.";
        public const string GET_CAPTCHA_SUCCSESS = "Tạo mã Captcha thành công.";
        public const string GET_ALL_ACCOUNT_SUCCSESS = "Lấy danh sách tài khoản thành công.";
        public const string LOGIN_STILL_VALID = "Token còn hiệu lực.";
        public const string REGISTER_AVTIVITY_SUCCSESS = "Đăng ký tham gia hoạt động thành công.";
        public const string REGISTER_LEAVE_ACTIVITY_SUCCSESS = "Đăng ký nghỉ phép thành công.";
        public const string UPDATE_MEMBER_ROLE_SUCCSESS = "Cập nhật chức vụ thành viên thành công.";

        #region Member
        public const string GET_MY_INFO_SUCCSESS = "Lấy thông tin cá nhân thành công.";
        public const string UPDATE_MEMBER_INFO_SUCCSESS = "Cập nhật thông tin cá nhân thành công.";
        public const string DELETE_MEMBER_IS_SUCCSESS = "Xoá thành viên thành công.";
        public const string GET_LIST_MEMBER_SUCCSESS = "Lấy danh sách thành viên thành công.";
        public const string UPDATE_AVATAR_SUCCSESS = "Cập nhật hình đại diện thành công.";
        public const string CREATE_RECRUITMENT_SUCCSESS = "Tạo tuyển thành viên thành công.";
        public const string GET_RECRUITMENT_SUCCSESS = "Lấy thông tin tuyển thành viên thành công.";
        public const string GET_LIST_RECRUITMENT_SUCCSESS = "Lấy danh sách tuyển thành viên thành công.";
        public const string UPDATE_RECRUITMENT_SUCCSESS = "Cập nhật thông tin tuyển thành viên thành công.";
        public const string DELETE_RECRUITMENT_SUCCSESS = "Xoá thông tin tuyển thành viên thành công.";
        public const string LOCK_RECRUITMENT_SUCCSESS = "Khoá thông tin tuyển thành viên thành công.";
        public const string UNLOCK_RECRUITMENT_SUCCSESS = "Mở khoá thông tin tuyển thành viên thành công.";
        public const string STOP_RECRUITMENT_SUCCSESS = "Dừng tuyển thành viên thành công.";
        public const string REGISTER_SUCCSESS = "Đăng ký thành viên thành công.";
        public const string UPDATE_ROOM_SUCCSESS = "Cập nhật nhóm phỏng vấn thành công.";
        public const string DELETE_ROOM_SUCCSESS = "Xoá nhóm phỏng vấn thành công.";
        public const string ADD_MEMBER_TO_ROOM_SUCCSESS = "Thêm thành viên vào nhóm phỏng vấn thành công.";
        public const string DELETE_MEMBER_IN_ROOM_SUCCSESS = "Xoá thành viên khỏi nhóm phỏng vấn thành công.";
        public const string DELETE_MEMBER_INTERVIEW_SUCCSESS = "Xoá thành viên đăng ký thành viên thành công.";
        public const string GET_LIST_MEMBER_REGISTER_SUCCSESS = "Lấy danh sách đăng ký thành viên thành công.";
        public const string GET_MEMBER_REGISTER_SUCCSESS = "Lấy thông tin thành viên phỏng vấn thành công.";
        public const string SEND_INTERVIEW_EMAIL_SUCCSESS = "Gửi email phỏng vấn thành công.";
        public const string UPDATE_REVIEW_SUCCSESS = "Cập nhật điểm phỏng vấn thành công.";
        public const string APPROVAL_SUCCSESS = "Quá trình duyệt thành viên đang diễn ra, hệ thống sẽ liên tục cập nhật tiến độ tại màn hình này.";
        #endregion
        #region MyDTU
        public const string UPDATE_SCHEDULE_SUCCSESS = "Cập nhật lịch học thành công.";
        public const string ADD_COURSE_SUCCSESS = "Thêm môn học thành công.";
        public const string DELETE_COURSE_SUCCSESS = "Xoá môn học thành công.";
        public const string GET_LIST_SCHEDULE_SUCCSESS = "Lấy danh sách lịch học thành công.";
        public const string START_SUCCSESS = "Quá trình đăng ký môn học đã khởi chạy, bạn vui lòng không nhấn đăng xuất ở MyDTU.";
        public const string STTOP_SUCCSESS = "Ngưng quá trình đăng ký môn học thành công.";
        public const string SAVE_MYDTU_SETTING_SUCCSESS = "Lưu cài đặt năm học thành công.";
        public const string GET_PROXY_SUCCSESS = "Lấy danh sách proxy thành công.";
        public const string DELETE_PROXY_SUCCSESS = "Xoá proxy thành công.";
        public const string UPDATE_PROXY_SUCCSESS = "Cập nhật proxy thành công.";
        #endregion

        #region Activity
        public const string CREATE_ACTIVITY_SUCCSESS = "Tạo hoạt động thành công.";
        public const string UPDATE_ACTIVITY_SUCCSESS = "Cập nhật hoạt động thành công.";
        public const string DELETE_ACTIVITY_SUCCSESS = "Xoá hoạt động thành công.";
        public const string GET_HISTORIES_ACTIVITY_SUCCSESS = "Lấy lịch sử hoạt động thành công.";
        public const string DELETE_MEMBER_ACTIVITY_SUCCSESS = "Xoá đăng ký tham gia thành công.";
        public const string CHECKIN_ACTIVITY_SUCCSESS = "Checkin/check out thành công.";
        public const string DEL_CHECKIN_ACTIVITY_SUCCSESS = "Xoá điểm danh thành công.";
        public const string UPDATE_ALL_CHECKIN_ACTIVITY_SUCCSESS = "Việc cập nhật toàn bộ bản điểm danh sẽ mất khá nhiều thời gian, hệ thống sẽ tiến hành cập nhật ở background.";
        public const string UPDATE_CHECKIN_ACTIVITY_SUCCSESS = "Cập nhật điểm danh thành công.";
        public const string GET_LIST_MEMBER_IN_ACTIVITY_SUCCSESS = "Lấy danh sách thành viên tham gia hoạt động thành công.";
        public const string UPLOAD_TEXT_FONT_SUCCSESS = "Cập nhật font chữ thành công.";
        public const string UPLOAD_CERTIFICATE_SUCCSESS = "Cập nhật giấy chứng nhận thành công.";
        public const string DELETE_CERTIFICATE_TEXT_SUCCSESS = "Xoá text giấy chứng nhận thành công.";
        public const string SEND_CERTIFICATE_SUCCSESS = "Quá trình gửi giấy chứng nhận đã được bắt đầu. Bây giờ bạn có thể thoát khỏi trang này.";
        #endregion

        #region Newspaper
        public const string UPDATE_NEWSPAPER_SUCCSESS = "Cập nhật tin tức thành công.";
        public const string DELETE_NEWSPAPER_SUCCSESS = "Xoá tin tức thành công.";
        #endregion

        #region Faculty
        public const string UPDATE_FACULTY_SUCCSESS = "Cập nhật khoa/viện thành công.";
        #endregion

        #region Role
        public const string UPDATE_ROLE_SUCCSESS = "Cập nhật chức vụ thành công.";
        #endregion

        #region Software
        public const string UPDATE_SOFTWARE_SUCCSESS = "Cập nhật phần mềm thành công.";
        public const string ADD_SHARE_FILE_SUCCSESS = "Thêm chia sẻ file thành công.";
        public const string DELETE_SHARE_FILE_SUCCSESS = "Xoá chia sẻ file thành công.";
        #endregion
    }
}
