﻿using DocumentFormat.OpenXml.Wordprocessing;
using System.ComponentModel.DataAnnotations;

namespace DTUSVCv3_Library
{
    /// <summary>
    /// Specify log type
    /// </summary>
    public enum LogTypeEnum
    {
        /// <summary>
        /// Record the errors that occurred
        /// </summary>
        ERRORS = 0,
        /// <summary>
        /// Record the warning that occurred
        /// </summary>
        WARNING = 1,
        /// <summary>
        /// Record login activities
        /// </summary>
        LOGIN = 2,
        /// <summary>
        /// Record forgot password
        /// </summary>
        FORGOT_PASSWORD = 3,
        /// <summary>
        /// Record reset password
        /// </summary>
        RESET_PASSWORD = 4,
        /// <summary>
        /// Record all normal user activities
        /// </summary>
        DEFAULT = 5,
    }

    public enum EmailType
    {
        /// <summary>
        /// Email display name is [DTUSVC] - No-Reply
        /// </summary>
        [Display(Name = "[DTUSVC] - NO-REPLY")]
        DEFAULT = 0,
        /// <summary>
        /// Email display name is [DTUSVC] - Mã xác nhận
        /// </summary>
        [Display(Name = "[DTUSVC] - MÃ XÁC NHẬN")]
        CONFIRM_CODE = 1,
        /// <summary>
        /// Email display name is [DTUSVC] - Thông báo
        /// </summary>
        [Display(Name = "[DTUSVC] - THÔNG BÁO")]
        NOTIFICATION = 2,
        /// <summary>
        /// Email display name is [DTUSVC] - Giấy chứng nhận
        /// </summary>
        [Display(Name = "[DTUSVC] - GIẤY CHỨNG NHẬN")]
        CERTIFICATE = 3,
        /// <summary>
        /// Email display name is [DTUSVC] - Chúc mừng sinh nhật
        /// </summary>
        [Display(Name = "[DTUSVC] - CHÚC MỪNG SINH NHẬT")]
        HAPPY_BIRTHDAY = 4,
        /// <summary>
        /// Email display name is [DTUSVC] - Khôi phục mật khẩu
        /// </summary>
        [Display(Name = "[DTUSVC] - KHÔI PHỤC MẬT KHẨU")]
        RESET_PASSWD = 5,
        /// <summary>
        /// Email display name is [DTUSVC] - Thư mời phỏng vấn
        /// </summary>
        [Display(Name = "[DTUSVC] - THƯ MỜI PHỎNG VẤN")]
        INTERVIEW = 6,
        /// <summary>
        /// Email display name is [DTUSVC] - thông báo kết quả phỏng vấn
        /// </summary>
        [Display(Name = "[DTUSVC] - THÔNG BÁO KẾT QUẢ PHỎNG VẤN")]
        WELLCOME_TO_DTUSVC = 7,
    }

    public enum ErrorCode
    {
        // 100xxx
        #region Account
        /// <summary>
        /// Login failed
        /// </summary>
        LOGIN_FAILD = 100001,
        /// <summary>
        /// login session expiered
        /// </summary>
        LOGIN_SESSION_EXPIRED = 100002,
        /// <summary>
        /// Logout failed
        /// </summary>
        LOGOUT_FAILED = 100003,
        /// <summary>
        /// Not Login
        /// </summary>
        NOT_LOGIN = 100004,
        /// <summary>
        /// CHANGE PASS MODEL INVALID
        /// </summary>
        CHANGE_PASS_MODEL_INVALID = 100005,
        /// <summary>
        /// ACCOUNT IS LOCKER
        /// </summary>
        ACCOUNT_IS_LOCKER = 100006,
        /// <summary>
        /// NEW_PASSWD_NOT_MATCH_REPASSWD
        /// </summary>
        NEW_PASSWD_NOT_MATCH_REPASSWD = 100007,
        /// <summary>
        /// OLD_PASSWD_INVALID
        /// </summary>
        OLD_PASSWD_INVALID = 100008,
        /// <summary>
        /// CHANGE_PASSWD_FAILD
        /// </summary>
        CHANGE_PASSWD_FAILD = 100009,
        /// <summary>
        /// NEW_PASSWD_INVALID
        /// </summary>
        NEW_PASSWD_INVALID = 100010,
        /// <summary>
        /// EXPIRED PASSWORD RECOVERY PATH
        /// </summary>
        EXPIRED_PASSWORD_RECOVERY_PATH = 100011,
        /// <summary>
        /// CAPTCHA_INVALID
        /// </summary>
        CAPTCHA_INVALID = 100012,
        /// <summary>
        /// FORGOT_PASSWD_TOKEN_INVALID
        /// </summary>
        FORGOT_PASSWD_TOKEN_INVALID = 100013,
        /// <summary>
        /// ACCOUNT_MEMBER_IS_LOCKER
        /// </summary>
        ACCOUNT_MEMBER_IS_LOCKER = 100014,
        /// <summary>
        /// ACCOUNT_IS_NULL
        /// </summary>
        ACCOUNT_IS_NULL = 100015,
        /// <summary>
        /// UPDATE_ACCOUNT_FAILED
        /// </summary>
        UPDATE_ACCOUNT_FAILED = 100016,
        /// <summary>
        /// GET_ALL_ACCOUNT_FAILED
        /// </summary>
        GET_ALL_ACCOUNT_FAILED = 100017,
        UPDATE_ROLE_FAILED = 100018,
        #endregion

        // 101xxx
        #region Member
        MEMBER_INFO_IS_NULL = 101001,
        CHANGE_MEMBER_INFO_MODEL_IS_NULL = 101002,
        UPDATE_MEMBER_INFO_INVALID = 101003,
        UPDATE_MEMBER_INFO_FAILED = 101004,
        MEMBER_IS_NOT_EXIST_IN_DATABASE = 101005,

        CREATE_RECRUITMENT_DATA_REQUSET_NULL = 101006,
        RECRUITMENT_TITLE_INVALID = 101007,
        RECRUITMENT_CONTENT_INVALID = 101008,
        RECRUITMENT_DATE_INVALID = 101009,
        UPDATE_RECRUITMENT_FAILED = 101010,
        EXIST_RECRUITMENT_VALID = 101011,
        RECRUITMENT_IS_NULL = 101012,
        UPDATE_RECRUITMENT_MODEL_IS_NULL = 101013,
        RECRUITMENT_IS_EXPIRED = 101014,
        DELETE_RECRUITMENT_FAILED = 101015,
        LOCK_RECRUITMENT_FAILED = 101016,
        UNLOCK_RECRUITMENT_FAILED = 101017,
        SET_STOP_RECRUITMENT_FAILED = 101018,

        MEMBER_APPLYING_MODEL_IS_NULL = 101019,
        MEMBER_FULL_NAME_INVALID = 101020,
        BIRTH_DAY_INVALID = 101021,
        SEX_INVALID = 101022,
        PHONE_NUMBER_INVALID = 101023,
        EMAIL_INVALID = 101024,
        LINK_FACEBOOK_INVALID = 101025,
        FACULTY_INVALID = 101026,
        CLASS_NAME_INVALID = 101027,
        STUDENT_ID_INVALID = 101028,
        REASON_INVALID = 101029,
        CREATE_MEMBER_TO_DB_FAILED = 101030,
        DATETIME_FORMAT_INVALID = 101031,
        START_DATE_AND_END_DATE_INVALID = 101032,
        ROOM_ADDRESS_INVALID = 101033,
        RATED_TIME_INVALID = 101034,
        UPDATE_ROOM_FAILED = 101035,
        ROOM_IS_STARTED = 101036,
        ROOM_EMAIL_SENDING = 101037,
        DELETE_ROOM_FAILED = 101038,
        INTERVIEW_ROOM_IS_NULL = 101039,
        ROOM_IS_END = 101040,
        MEMBER_IN_ROOM_IS_MAX = 101041,
        DELETE_MEMBER_IN_ROOM_FAILED_EMAIL_SENNT = 101042,
        MEMBER_INTERVIEW_NULL = 101043,
        MEMBER_IS_INTERVIEWED = 101044,
        MEMBER_IS_IN_ROOM = 101045,
        DELETE_MEMBER_INTERVIEW_FAILED = 101046,
        MEMBER_LIST_REGISTER_IS_NULL = 101047,
        INTERVIEW_DEST_ROOM_IS_NULL = 101048,
        MEMBER_IS_IN_DEST_ROOM = 101049,
        MOVE_INTERVIEW_FAILED = 101050,
        ROOM_IS_EMPTY = 101051,
        YOU_DONT_ROOM_MANAGER = 101052,
        MEMBER_INTERVIEW_IS_UNSEND_EMAIL = 101053,
        MEMBER_INTERVIEW_IS_MOVE_ROOM = 101054,
        SCORCES_INVALID = 101055,
        REVIEW_CONTENT_IS_NULL = 101056,
        EMAIL_IS_EXIST = 101057,
        STUDENT_ID_IS_EXIST = 101058,
        #endregion

        // 102xxx
        #region Event
        #endregion

        // 103xxx
        #region Address
        WARD_IS_NULL = 103001,
        DISTRICT_IS_NULL = 103002,
        PROVINCE_IS_NULL = 103003,
        SPECISIC_ADDRESS_IS_NULL = 103004,
        UPDATE_ADDRESS_TO_DB_FAILED = 103005,
        #endregion

        // 104xxx
        #region Faculty
        #endregion

        // 105xxx
        #region Fund
        #endregion

        // 106xxx
        #region News
        #endregion

        // 107xxx
        #region Email
        /// <summary>
        /// SEND_EMAIL_FAILED
        /// </summary>
        SEND_EMAIL_FAILED = 107001,

        #endregion

        // 108xxx
        #region Roles
        AUTHORITY_INSUFFICIENT = 108001,
        #endregion
        // 109xxx
        #region File
        EXPORT_EXCEL_FAILED = 109001,
        UPLOAD_FILE_IS_NULL = 109002,
        IMAGE_TYPE_INVALID = 109003,
        IMAGE_SIZE_INVALIE = 109004,
        FONT_TYPE_INVALID = 109005,
        FONT_SIZE_INVALIE = 109006,
        FONT_INVALID = 109007,
        FONT_UPLOAD_FAILED = 109008,
        #endregion

        // 110xxx
        #region MyDTU
        CREATE_SCHEDULE_REQUEST_IS_NULL = 110001,
        SCHEDULE_TITLE_NULL = 110002,
        SCHEDULE_REGISTER_DATE_INVALID = 110003,
        UPDATE_SCHEDULE_FAILED = 110004,
        CREATE_SCHEDULE_LESS_60DAY = 110005,
        SCHEDULE_IS_NULL = 110006,
        CHANGE_SCHEDULE_LESS_60DAY = 110007,
        COURSE_DTU_INVALID = 110008,
        UPDATE_COURSE_DTU_FAILED = 110009,
        COURSE_EXIST_IN_SCHEDULE = 110010,
        COURSE_IS_PROCESS = 110011,
        SCHEDULE_IS_IN_PROCESS = 110012,
        SCHEDULE_NOT_YOURS = 110013,
        START_TYPE_INVALID = 110014,
        LOOP_COUNT_INVALID = 110015,
        START_FAILED = 110016,
        COURSE_IS_NULL = 110017,
        MYDTU_LOGIN_FALIED = 110018,
        MYDTU_INFO_IS_NOT_MATCH = 110019,
        MYDTU_REGISTER_IN_PROCESS = 110020,
        NUMBER_LEARNING_UNITS_OVER_20 = 110021,
        SCHOOL_YEAR_INVALID = 110022,
        DELETE_PROXY_FAILED = 110023,
        PROXY_INVALID = 110024,
        PROXY_EXPIRATION_DATE_INVALID = 110025,
        UPDATE_PROXY_FAILED = 110026,
        #endregion

        // 111xxx
        #region Activity
        ACTIVITY_IS_NULL = 111001,
        ACTIVITY_TITLE_INVALID = 111002,
        ACTIVITY_CONTENT_INVALID = 111003,
        ACTIVITY_POSTER_INVALID = 111004,
        ACTIVITY_START_TIME_INVALID = 111005,
        ACTIVITY_END_TIME_INVALID = 111006,
        ACTIVITY_DEADLINE_TIME_INVALID = 111007,
        ACTIVITY_IS_FINISHED = 111008,
        DELETE_ACTIVITY_FAILED = 111009,
        ACTIVITY_IS_PRIVATE = 111010,
        ACTIVITY_HISTORIES_IS_NULL = 111011,
        MEMBER_IN_ACTIVITY_IS_NULL = 111012,
        MEMBER_IN_ACTIVITY_EXPORT_FAILED = 111013,
        MEMBER_REGISTER_INVALID = 111014,
        MEMBER_REGISTER_FULL_NAME_INVALID = 111015,
        MEMBER_REGISTER_PHONENUMBER_INVALID = 111016,
        MEMBER_REGISTER_EMAIL_INVALID = 111017,
        MEMBER_REGISTER_FACULTY_INVALID = 111018,
        MEMBER_REGISTER_CLASS_NAME_INVALID = 111019,
        MEMBER_REGISTER_STUDENTID_IS_MEMBER = 111020,
        MEMBER_REGISTER_STUDENTID_IS_EXIST = 111021,
        ACTIVITY_IS_EXPIRED_REGISTER = 111022,
        ACTIVITY_IS_NOT_PUBLIC = 111023,
        MEMBER_REGISTER_ACTIVITY_FAILED = 111024,
        MEMBER_LEAVE_ACTIVITY_MODEL_INVALID = 111025,
        MEMBER_LEAVE_ACTIVITY_RASON_INVALID = 111026,
        LEAVE_ACTIVITY_IS_EXIST = 111027,
        REGISTER_LEAVE_ACTIVITY_FAILED = 111028,
        DELETE_MEMBER_ACTIVITY_FAILED = 111029,
        CHECKIN_ACTIVITY_FAILED = 111030,
        MEMBER_IS_NOT_EXIST_ACTIVITY = 111031,
        DEL_CHECKIN_FAILED = 111032,
        UPDATE_CHECKIN_MODEL_INVALID = 111033,
        CHECKIN_DOES_NOT_EXIST = 111034,
        CHECKIN_TIME_INVALID = 111035,
        CHECKOUT_TIME_INVALID = 111036,
        DIFFERENT_DAY = 111037,
        START_BIGGER_END_DATETIME = 111038,
        UPDATE_CHECKIN_ONLY_DATE = 111039,
        CERTIFICATE_UPDATE_INVALID = 111040,
        UPDATE_CERTIFICATE_FAILED = 111045,
        TEXT_FONT_IS_EMPTY = 111046,
        MAX_WIDTH_INVALID = 111047,
        MAX_HEIGHT_INVALID = 111048,
        LOCATION_X_INVALID = 111049,
        LOCATION_Y_INVALID = 111050,
        FONT_SIZE_INVALID = 111051,
        VERTICAL_INVALID = 111052,
        HORIZONTAL_INVALID = 111053,
        COLOR_INVALID = 111054,
        FONT_STYLE_INVALID = 111055,
        UPDATE_SETTING_TEXT_FAILED = 111056,
        DELETE_SETTING_TEXT_FAILED = 111057,
        SETTING_TEXT_IS_EMPTY = 111058,
        CERTIFICATE_IS_SENDING = 111059,
        SEND_CERTIFICATE_REQUEST_INVALID = 111060,
        CERTIFICATE_IS_NULL = 111061,
        IS_NOT_SEND_CERTIFICATE = 111062,
        STATISTICS_IS_EMPTY = 111063,
        #endregion

        // 112xxx
        #region Newspaper
        CREATE_NEWS_REQUEST_INVALID = 112001,
        NEWS_TITLE_INVALID = 112002,
        NEWS_CONTENT_INVALID = 112003,
        NEWS_POSTER_INVALID = 112004,
        UPDATE_NEWSPAPER_FAILED = 112005,
        NEWSPAPER_IS_NULL = 112006,
        DELETE_NEWS_FAILED = 112007,
        UPLOAD_INVALID = 112008,
        #endregion

        // 113xxx
        #region Faculty
        FACULTY_NAME_EXIST = 113001,
        FACULTY_NAME_INVALID = 113002,
        UPDATE_FACULTY_FAILED = 113003,
        FACULTY_IS_NULL = 113004,
        #endregion

        // 114xxx
        #region Roles
        GET_ALL_PERMISSION_FAILED = 114001,
        ROLE_NAME_EXIST = 114002,
        ROLE_NAME_INVALID = 114003,
        CREATE_ROLE_FAILED = 114004,
        PERMISSION_INVALID = 114005,
        ROLE_IS_NULL = 114006,
        #endregion

        // 115xxx
        #region Short Link
        SHORT_LINK_IS_NULL = 115001,
        CREATE_SHORT_LINK_FAILED = 115002,
        URL_IS_NULL = 115003,
        #endregion

        // 116xxx
        #region Software
        UPDATE_SOFTWARE_REQUEST_INVALID = 116001,
        TITLE_SOFTWARE_REQUEST_INVALID = 116002,
        DESCRIPTION_SOFTWARE_REQUEST_INVALID = 116003,
        FILE_NAME_SOFTWARE_REQUEST_INVALID = 116004,
        VERSION_SOFTWARE_REQUEST_INVALID = 116005,
        SOFTWARE_FILE_REQUEST_INVALID = 116006,
        UPDATE_SOFTWARE_FALIED = 116007,
        GG_DRIVE_DISCONECTED = 116008,
        SOFTWARE_IS_NULL = 116009,
        GG_ROLE_TYPE_INVALID = 116010,
        GG_ROLE_INVALID = 116011,
        GG_ROLE_ADD_FAILED = 116012,
        GG_ROLE_ADD_REQUEST_INVALID = 116013,
        GG_ROLE_ADD_EMAIL_INVALID = 116014,
        GG_ROLE_DELETE_REQUEST_INVALID = 116015,
        GG_ROLE_DELETE_ROLLE_FAILED = 116016,
        PERMISSION_ID_INVALID = 116017,
        DELETE_SOFTWARE_FAILED = 116018,
        SOFTWARE_IS_EMPTY = 116019,
        #endregion
        // 200xxx
        #region System
        SYSTEM_MAINTENANCE = 200001,
        SYSTEM_DISCONNECT = 200002
        #endregion

    }

    public enum DTUSVCPermission
    {
        NONE = 0,
        // 1xxx
        #region Account
        [Display(Name = "Xem danh sách tài khoản")]
        GET_ALL_ACCOUNT = 1001,
        [Display(Name = "Thay đổi chức vụ cho thành viên")]
        CHANGE_ROLE = 1002,
        [Display(Name = "Khoá/Mở khoá tài khoản")]
        LOCK_UNLOCK_ACCOUNT = 1003,
        [Display(Name = "Reset mật khẩu")]
        ADMIN_RESET_PASSWD = 1004,
        #endregion

        // 2XXX
        #region Member
        [Display(Name = "Tìm kiếm thành viên")]
        SEARCH_MEMBER = 2001,
        [Display(Name = "Xoá thành viên")]
        DELETE_MEMBER = 2002,
        [Display(Name = "Xem danh sách thành viên")]
        GET_MEMBER_LIST = 2003,
        [Display(Name = "Tải xuống danh sách thành viên")]
        DOWNLOAD_MEMBER_LIST = 2004,
        [Display(Name = "Tải xuống thẻ thành viên")]
        DOWNLOAD_MEMBER_CARD = 2005,
        [Display(Name = "Tìm kiếm thành viên theo tên")]
        SEARCH_MEMBER_CONTAINS = 2006,

        [Display(Name = "Tạo tuyển thành viên")]
        CREATE_RECRUITMENT = 2007,
        [Display(Name = "Xem toàn bộ danh sách tuyển thành viên")]
        GET_ALL_RECRUITMENT = 2008,
        [Display(Name = "Cập nhật tuyển thành viên")]
        UPDATE_RECRUITMENT = 2009,
        [Display(Name = "Xoá tuyển thành viên")]
        DELETE_RECRUITMENT = 2010,
        [Display(Name = "Khoá tuyển thành viên")]
        LOCK_RECRUITMENT = 2011,
        [Display(Name = "Kết thúc tuyển thành viên")]
        STOP_RECRUITMENT = 2012,

        [Display(Name = "Chấm điểm phỏng vấn")]
        REVIEWER = 2013,
        [Display(Name = "Tạo phòng phỏng vấn")]
        CREATE_INTERVIEW_ROOM = 2014,
        [Display(Name = "Cập nhật phòng phỏng vấn")]
        UPDATE_INTERVIEW_ROOM = 2015,
        [Display(Name = "Xoá phòng phỏng vấn")]
        DELETE_INTERVIEW_ROOM = 2016,
        [Display(Name = "Xoá hồ sơ ứng viên")]
        DELETE_MEMBER_INTERVIEW = 2017,
        [Display(Name = "Xem danh sách ứng viên")]
        GET_MEMBER_INTERVIEW = 2018,
        [Display(Name = "Xem thông tin ứng viên")]
        GET_MEMBER_INFO_INTERVIEW = 2019,
        [Display(Name = "Xoá ứng viên khỏi phòng phỏng vấn")]
        DELETE_MEMBER_INTERVIEW_IN_ROOM = 2020,
        [Display(Name = "Đổi phòng phỏng vấn")]
        MOVE_INTERVIEW = 2021,
        [Display(Name = "Gửi email mời phỏng vấn")]
        SEND_INTERVIEW_MAIL = 2022,
        [Display(Name = "Tải xuống danh sách ứng viên")]
        DOWNLOAD_MEMBER_INTERVIEW = 2023,
        #endregion

        // 3xxx
        #region Role
        [Display(Name = "Xem danh sách quyền hệ thống")]
        GET_ALL_PERMISSION = 3001,
        [Display(Name = "Xem danh sách chức vụ")]
        GET_ALL_ROLE = 3002,
        [Display(Name = "Tạo chức vụ")]
        CREATE_ROLE = 3003,
        [Display(Name = "Cập nhật chức vụ")]
        UPDATE_ROLE = 3004,
        #endregion

        // 4xxx
        #region MyDTU Role
        [Display(Name = "Cập nhật cài đặt mã học kỳ MyDTU")]
        UPDATE_MYDTU_YEAR_SETTING = 4001,
        [Display(Name = "Lấy danh sách proxy")]
        GET_PROXY = 4002,
        [Display(Name = "Xoá proxy")]
        DELETE_PROXY = 4003,
        [Display(Name = "Cập nhật proxy")]
        UPDATE_PROXY = 4004,
        #endregion

        // 5xxx
        #region Activity
        [Display(Name = "Tạo hoạt động")]
        CREATE_ACTIVITY = 5001,
        [Display(Name = "Cập nhật hoạt động")]
        UPDATE_ACTIVITY = 5002,
        [Display(Name = "Xoá hoạt động")]
        DELETE_ACTIVITY = 5003,
        [Display(Name = "Tài xuống danh sách đăng ký tham gia hoạt động")]
        EXPORT_ACTIVITY = 5004,
        [Display(Name = "Xoá thành viên khỏi hoạt động")]
        DELETE_MEMBER_IN_ACTIVITY = 5005,
        [Display(Name = "Điểm danh hoạt động")]
        CHECK_IN_ACTIVITY = 5006,
        [Display(Name = "Xoá điểm danh hoạt động")]
        DEL_CHECK_IN_ACTIVITY = 5007,
        [Display(Name = "Cập nhật điểm danh hoạt động")]
        UPDATE_CHECK_IN_ACTIVITY = 5008,
        [Display(Name = "Xem danh sách thành viên đăng ký hoạt động")]
        GET_LIST_MEMBER_IN_ACTIVITY = 5009,
        [Display(Name = "Cập nhật font chữ cho hệ thống")]
        UPDATE_CERTIFICATE_TEXT_FONT = 5010,
        [Display(Name = "Upload giấy chứng nhận cho hoạt động")]
        UPDATE_CERTIFICATE = 5011,
        [Display(Name = "Cài đặt chèn chữ cho giấy chứng nhận")]
        UPDATE_SETTING_TEXT = 5012,
        [Display(Name = "Xoá cài đặt chèn chữ cho giấy chứng nhận")]
        DELETE_SETTING_TEXT = 5013,
        [Display(Name = "Xem danh sách cài đặt chèn chữ cho giấy chứng nhận")]
        GET_SETTING_TEXT = 5014,
        [Display(Name = "Xem dữ liệu thống kê của hoạt động")]
        GET_ACTIVITY_STATISTICS = 5015,
        #endregion

        // 6xxx
        #region Newspaper
        [Display(Name = "Tạo tin tức")]
        CREATE_NEWSPAPER = 6001,
        [Display(Name = "Cập nhật tin tức")]
        UPDATE_NEWSPAPER = 6002,
        [Display(Name = "Xoá tin tức")]
        DELETE_NEWSPAPER = 6003,
        [Display(Name = "Tải lên hình ảnh")]
        UPLOAD_IMAGE = 6004,
        #endregion

        // 7xxx
        #region Facultys
        [Display(Name = "Tạo khoa/viện")]
        CREATE_FACULTY = 7001,
        [Display(Name = "Cập nhật khoa/viện")]
        UPDATE_FACULTY = 7002,
        #endregion

        // 8xxx
        #region SoftWare
        UPDATE_SOFTWARE = 8001,
        SHARE_SOFTWARE = 8002,
        DELETE_SHARE_SOFTWARE = 8003,
        #endregion
    }

    public enum GetMemberType
    {
        ALL_MEMBER = 1,
        IS_MEMBER = 2,
        MEMBER_LEAVES = 3
    }

    public enum MemberRecruitmentType
    {
        UNKNOWN = 0,
        ALL_MEMBER = 1,
        IN_GROUP = 2,
        OUT_GROUP = 3
    }

    public enum SendEmailType
    {
        UNSENT = 1,
        SENDING = 2,
        SENT = 3,
        WAITING_RESEND = 4
    }

    public enum ChangeRoomResult
    {
        ID_INVALID = 0,
        [Display(Name = "Nhóm đích không tồn tại")]
        DEST_ID_INVALID = 1,
        [Display(Name = "Nhóm đã bắt đầu phỏng vấn")]
        ROOM_STARTED = 2,
        [Display(Name = "Nhóm đã đầy")]
        ROOM_FULL = 3,
        [Display(Name = "Thành viên không tồn tại")]
        MEMBER_ID_INVALID = 4,
        [Display(Name = "Đổi nhóm phỏng vấn thất bại")]
        UPDATE_FAILED = 5,
        [Display(Name = "Đổi nhóm phỏng vấn thành công")]
        UPDATE_SUCCSESS = 6,
    }

    public enum FindType
    {
        EQUALS = 1,
        CONTAINS = 2
    }

    public enum GUIScreen
    {
        /// <summary>
        /// Unknown
        /// </summary>
        UNKNOWN = 0,
        /// <summary>
        /// Certificate
        /// </summary>
        CERTIFICATE = 1,
        /// <summary>
        /// Account
        /// </summary>
        ACCOUNT = 2,
        /// <summary>
        /// Interview
        /// </summary>
        INTERVIEW = 3,
        /// <summary>
        /// MYDTU REGISTER COURSE
        /// </summary>
        MYDTU_REGISTER_COURSE = 5,
    }

    public enum ScheduleCourseType
    {
        /// <summary>
        /// Unknown
        /// </summary>
        [Display(Name = "Không xác định")]
        UNKNOWN = 0,
        /// <summary>
        /// Unregistered
        /// </summary>
        [Display(Name = "Chưa đăng ký")]
        UNREGISTERED = 1,
        /// <summary>
        /// Registered
        /// </summary>
        [Display(Name = "Đã đăng ký")]
        REGISTERED = 2,
        /// <summary>
        /// Registering
        /// </summary>
        [Display(Name = "Đang đăng ký")]
        REGISTERING = 3,
        /// <summary>
        /// Registering
        /// </summary>
        [Display(Name = "Đang thử lại")]
        TRYING_AGAIN = 4,
        /// <summary>
        /// Registering
        /// </summary>
        [Display(Name = "Lỗi")]
        FAILED = 5,
        /// <summary>
        /// Registering
        /// </summary>
        [Display(Name = "Lỗi đăng nhập")]
        LOGIN_FAILED = 6,
    }

    public enum ScheduleProcessType
    {
        /// <summary>
        /// Unknown
        /// </summary>
        [Display(Name = "Không xác định")]
        UNKNOWN = 0,
        /// <summary>
        /// Unknown
        /// </summary>
        [Display(Name = "Sẵn sàng")]
        IDLE = 1,
        /// <summary>
        /// Unknown
        /// </summary>
        [Display(Name = "Đang tiến hành")]
        IN_PROCESS = 2,
        /// <summary>
        /// Unknown
        /// </summary>
        [Display(Name = "Chưa tới thời gian đăng ký")]
        NOT_TIME_REGISTER = 3,
    }

    public enum TextType
    {
        /// <summary>
        /// Full name
        /// </summary>
        [Display(Name = "<<FullName>>")]
        FULL_NAME = 0,
        /// <summary>
        /// First name
        /// </summary>
        [Display(Name = "<<FirstName>>")]
        FIRST_NAME = 1,
        /// <summary>
        /// Last Name
        /// </summary>
        [Display(Name = "<<LastName>>")]
        LAST_NAME = 2,
        /// <summary>
        /// Student Id
        /// </summary>
        [Display(Name = "<<StudentId>>")]
        STUDENT_ID = 3,
        /// <summary>
        /// Faculty name
        /// </summary>
        [Display(Name = "<<FacultyName>>")]
        FACULTY_NAME = 4,
        /// <summary>
        /// Class name
        /// </summary>
        [Display(Name = "<<ClassName>>")]
        CLASS_NAME = 5,
        /// <summary>
        /// Default Text
        /// </summary>
        [Display(Name = "<<DefaultText>>")]
        DEFAULT_TEXT = 6
    }
}
