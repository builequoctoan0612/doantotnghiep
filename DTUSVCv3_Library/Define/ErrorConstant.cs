﻿namespace DTUSVCv3_Library
{
    public class ErrorConstant
    {
        #region Account
        public const string LOGIN_SESSION_EXPIRED = "Refesh token.";
        public const string NOT_LOGIN = "Phiên đăng nhập đã hết hạn, vui lòng đăng nhập lại rồi tiếp tục.";
        public const string USER_PASSWD_INVALID = "Tên đăng nhập hoặc mật khẩu không đúng.";
        public const string OLD_PASSWD_INVALID = "Mật khẩu cũ không hợp lệ.";
        public const string LOGOUT_FAILED = "Đã có lỗi xảy ra trong quá trình đăng xuất.";

        public const string CHANGE_PASS_MODEL_INVALID = "Thông tin khôi phục mật khẩu không hợp lệ!";
        public const string ACCOUNT_IS_LOCKER = "Tài khoản của bạn đã bị khoá. Vui lòng liên hệ BCN để được giải quyết.";
        public const string ACCOUNT_IS_NULL = "Tài khoản không tồn tại.";
        public const string ACCOUNT_MEMBER_IS_LOCKER = "Tài khoản của thành viên đã bị khoá. Vui lòng mở khoá để tiếp tục";

        public const string NEW_PASSWD_NOT_MATCH_REPASSWD = "Nhập lại mật khẩu không trùng khớp.";
        public const string NEW_PASSWD_INVALID = "Mật khẩu mới không hợp lệ.";

        public const string CHANGE_PASSWD_FAILED = "Đã xảy ra lỗi trong quá trình cập nhật mật khẩu.";
        public const string EXPIRED_PASSWORD_RECOVERY_PATH = "Đường dẫn khôi phục mật khẩu đã hết hạn.";
        public const string UPDATE_ACCOUNT_FAILED = "Đã xãy ra lỗi khi cập nhật tài khoản.";
        public const string CAPTCHA_INVALID = "Nhập mã xác nhận không hợp lệ.";

        public const string FORGOT_PASSWD_TOKEN_INVALID = "Mã xác thực không hợp lệ.";
        public const string GET_ALL_ACCOUNT_FAILED = "Đã xảy ra lỗi khi lấy danh sách tài khoản từ CSDL.";
        public const string UPDATE_ROLE_FAILED = "Cập nhật chức vụ thành viên thất bại.";
        #endregion

        #region Member Info
        public const string MEMBER_INFO_IS_NULL = "Thông tin thành viên không tồn tại.";
        public const string CHANGE_MEMBER_INFO_MODEL_IS_NULL = "Danh mục cập nhập thông tin thành viên không hợp lệ.";
        public const string UPDATE_MEMBER_INFO_INVALID = "Không có bất kỳ trường nào trong dữ liệu cần cập nhật hợp lệ. Quá trình cập nhật thất bại.";
        public const string UPDATE_MEMBER_INFO_FAILED = " Cập nhật thông tin thành viên thất bại.";
        public const string MEMBER_IS_NOT_EXIST_IN_DATABASE = "Danh sách thành viên không tồn tại.";

        public const string CREATE_RECRUITMENT_DATA_REQUSET_NULL = "Dữ liệu yêu cầu tạo tuyển thành viên rỗng.";
        public const string RECRUITMENT_TITLE_INVALID = "Tiêu đề tuyển thành viên không hợp lệ";
        public const string RECRUITMENT_CONTENT_INVALID = "Nội dung tuyển thành viên không hợp lệ.";
        public const string RECRUITMENT_DATE_INVALID = "Ngày kết thúc không hợp lệ.";
        public const string UPDATE_RECRUITMENT_FAILED = "Cập nhật thông tin tuyển thành viên lên CSDL thất bại.";
        public const string EXIST_RECRUITMENT_VALID = "Tồn tại 1 đợt tuyển thành viên đang hoạt động, vui lòng kết thúc đợt tuyển thành viên đó trước khi tạo mới.";
        public const string RECRUITMENT_IS_NULL = "Không tìm thấy thông tin tuyển thành viên.";
        public const string UPDATE_RECRUITMENT_MODEL_IS_NULL = "Nội dung cập nhật cho tuyển thành viên rỗng.";
        public const string RECRUITMENT_IS_EXPIRED = "Đợt tuyển thành viên ngày đã kết thúc.";
        public const string DELETE_RECRUITMENT_FAILED = "Xoá tuyển thành viên thất bại.";
        public const string LOCK_RECRUITMENT_FAILED = "Khoá tuyển thành viên thất bại.";
        public const string UNLOCK_RECRUITMENT_FAILED = "Mở khoá tuyển thành viên thất bại.";
        public const string SET_STOP_RECRUITMENT_FAILED = "Thiết lập dừng tuyển thành viên thất bại.";
        public const string MEMBER_LIST_REGISTER_IS_NULL = "Danh sách đăng ký rỗng.";

        public const string MEMBER_APPLYING_MODEL_IS_NULL = "Dữ liệu đăng ký thành viên không hợp lệ.";
        public const string MEMBER_FULL_NAME_INVALID = "Họ và tên không hợp lệ.";
        public const string BIRTH_DAY_INVALID = "Ngày sinh không hợp lệ.";
        public const string SEX_INVALID = "Giới tính không hợp lệ.";
        public const string PHONE_NUMBER_INVALID = "Số điện thoại không hợp lệ.";
        public const string EMAIL_INVALID = "Email không hợp lệ.";
        public const string EMAIL_IS_EXIST = "Email đã tồn tại. Vui lòng liên hiện BCN để được giải quyết.";
        public const string STUDENT_ID_IS_EXIST = "Mã sinh viên đã tồn tại. Vui lòng liên hiện BCN để được giải quyết.";
        public const string LINK_FACEBOOK_INVALID = "Link facebook không hợp lệ.";
        public const string FACULTY_INVALID = "Khoa/Viện không hợp lệ.";
        public const string CLASS_NAME_INVALID = "Tên lớp không hợp lệ.";
        public const string STUDENT_ID_INVALID = "Mã sinh viên không hợp lệ.";
        public const string REASON_INVALID = "Hãy cho chúng tôi biết lý do bạn muốn tham gia CLB.";
        public const string CREATE_MEMBER_TO_DB_FAILED = "Đã xảy ra lỗi khi cập nhật thông tin đăng ký lên CSDL.";

        public const string DATETIME_FORMAT_INVALID = "Định dạng ngày tháng không hợp lệ.";
        public const string START_DATE_AND_END_DATE_INVALID = "Thời gian bắt đầu không được lớn hơn hoặc bằng thời gian kết thúc.";
        public const string ROOM_ADDRESS_INVALID = "Địa chỉ phòng không được để trống.";
        public const string RATED_TIME_INVALID = "Thời gian phỏng vấn trung bình không được nhỏ hơn 1p.";
        public const string UPDATE_ROOM_FAILED = "Cập nhật nhóm phỏng vấn thất bại.";
        public const string ROOM_IS_STARTED = "Phòng này đã bắt đầu thời gian phỏng vấn.";
        public const string ROOM_IS_END = "Phòng này kết thúc phỏng vấn.";
        public const string ROOM_EMAIL_SENDING = "Quá trình gửi email đang diễn ra, vui lòng đợi quá trình này kết thúc rồi thử lại.";
        public const string DELETE_ROOM_FAILED = "Quá trình xoá nhóm đã xảy ra lỗi. Có thể do mất kết nối với CSDL.";
        public const string INTERVIEW_ROOM_IS_NULL = "Phòng phỏng vấn không tồn tại.";
        public const string MEMBER_IN_ROOM_IS_MAX = "Số lượng người phỏng vấn trong phòng đã đạt mức tối đa.";
        public const string DELETE_MEMBER_IN_ROOM_FAILED_EMAIL_SENNT = "Người này đã được gửi email phỏng vấn. Vui lòng sử dụng chức năng thay đổi lịch phỏng vấn.";
        public const string DELETE_MEMBER_INTERVIEW_FAILED = "Xoá bản đăng ký thành viên thất bại.";
        public const string MEMBER_INTERVIEW_NULL = "Không tìm thấy thành viên phỏng vấn.";
        public const string MEMBER_IS_INTERVIEWED = "Bạn không thể xoá thông tin người đã được phỏng vấn.";
        public const string MEMBER_IS_IN_ROOM = "Thành viên này đã được xếp ở trong 1 phòng phỏng vấn, vui lòng xoá khỏi phòng phỏng vấn trước.";
        public const string INTERVIEW_DEST_ROOM_IS_NULL = "Phòng phỏng vấn đích không tồn tại.";
        public const string MEMBER_IS_IN_DEST_ROOM = "Thành viên này đã tồn tại ở phòng phỏng vấn đích.";
        public const string ROOM_IS_EMPTY = "Phòng này chưa có thành viên nào.";
        public const string YOU_DONT_ROOM_MANAGER = "Chỉ có thành viên quản lý mới được gửi email phỏng vấn.";
        public const string MEMBER_INTERVIEW_IS_UNSEND_EMAIL = "Người này chưa được gửi email mời phỏng vấn.";
        public const string MEMBER_INTERVIEW_IS_MOVE_ROOM = "Người này đã được chuyển tới phòng phỏng vấn khác.";
        public const string SCORCES_INVALID = "Điểm không hợp lệ. (0 ~ 10).";
        public const string REVIEW_CONTENT_IS_NULL = "Nội dung đã phỏng vấn không được để trống.";
        #endregion

        #region Address
        public const string WARD_IS_NULL = "Xã/Phường không tồn tại.";
        public const string DISTRICT_IS_NULL = "Quận/Huyện không tồn tại.";
        public const string PROVINCE_IS_NULL = "Tỉnh/Thành phố không tồn tại.";
        public const string SPECISIC_ADDRESS_IS_NULL = "Địa chỉ cụ thể không được bỏ trống.";
        public const string UPDATE_ADDRESS_TO_DB_FAILED = "Cập nhật địa chỉ lên CSDL bị lỗi.";
        #endregion

        #region File
        public const string FILE_NAME_IS_NULL = "Tên file rỗng.";
        public const string FILE_IS_EXIST = "File đã tồn tại.";
        public const string FILE_IS_NOT_EXIST = "File không tồn tại.";
        public const string IMAGE_OPEN_FAILED = "Đã xảy ra lỗi khi đọc file ảnh.";
        public const string FILE_UPLOAD_IS_TOO_LARGE_15 = "File tải lên phải nhỏ hơn 15MB.";
        public const string FILE_UPLOAD_IS_TOO_LARGE_5 = "File tải lên phải nhỏ hơn 5MB.";
        public const string FILE_UPLOAD_IS_TOO_LARGE_1 = "File tải lên phải nhỏ hơn 1MB.";
        public const string FILE_UPLOAD_IS_TOO_LARGE_100 = "File tải lên phải nhỏ hơn 100MB.";
        public const string FILE_DOWNLOADED_IN_WRONG_FORMAT = "File tải lên không đúng định dạng.\nĐịnh dạng cho phép là: {0}";
        public const string UPLOAD_FILE_IS_NULL = "File tải lên không được rỗng.";
        public const string EXPORT_EXCEL_FAILED = "Đã xảy ra lỗi trong quá trình xuất file excel.";
        public const string FONT_INVALID = "Font chữ không hợp lệ.";
        public const string FONT_UPLOAD_FAILED = "Quá trình tải font chữ gặp lỗi.";
        #region Images
        public const string IMAGE_TYPE_INVALID = "Type không hợp lệ.";
        public const string IMAGE_SIZE_INVALIE = "Kích thước ảnh không hợp lệ. Kích thước ảnh tối thiểu: {0}px X {1}px.";
        #endregion
        #endregion

        #region System
        public const string SYSTEM_MAINTENANCE = "Hệ thống đang bảo trì. Vui lòng quay lại sau!";
        public const string SYSTEM_DISCONNECT = "Mất kết nối với máy chủ!";
        #endregion

        #region UNKNOWN
        public const string UNKNOWN = "Unknown";
        #endregion

        #region Send email
        public const string ATTACH_FILE_OUT_SIZE = "Kích thước file đính kèm vượt quá mức cho phép. (1 email chỉ được đính kèm file có tổng dung lượng không được > 20MB)";
        public const string SEND_EMAIL_FAILED = "Gửi email thất bại.";
        #endregion

        #region Roles
        public const string AUTHORITY_INSUFFICIENT = "Bạn không có quyền thực hiện chức năng này.";
        #endregion

        #region MyDTU
        public const string CREATE_SCHEDULE_REQUEST_IS_NULL = "Dữ liệu tạo lịch học rỗng.";
        public const string SCHEDULE_TITLE_NULL = "Tiêu đề lịch học rỗng.";
        public const string SCHEDULE_REGISTER_DATE_INVALID = "Ngày bắt đầu đăng ký không hợp lệ.";
        public const string UPDATE_SCHEDULE_FAILED = "Cập nhật lịch học thất bại.";
        public const string CREATE_SCHEDULE_LESS_60DAY = "Hệ thống chỉ cho phép tạo 2 bộ lịch học cách nhau tối thiểu 60 ngày.";
        public const string SCHEDULE_IS_NULL = "Lịch học không tồn tại.";
        public const string CHANGE_SCHEDULE_LESS_60DAY = "Hệ thống chỉ cho phép chỉnh sửa lịch trong vòng 60 ngày.";
        public const string COURSE_DTU_INVALID = "Đường dẫn môn học không hợp lệ.";
        public const string UPDATE_COURSE_DTU_FAILED = "Cập nhật lịch học thất bại.";
        public const string COURSE_EXIST_IN_SCHEDULE = "Môn học này đã tồn tại trong lịch học của bạn.";
        public const string COURSE_IS_PROCESS = "Môn học này đã hoặc đang trong tiến trình đăng ký.";
        public const string SCHEDULE_IS_IN_PROCESS = "Bạn không thể thực hiện thao tác khi tiến trình đăng ký đang hoạt động.";
        public const string SCHEDULE_NOT_YOURS = "Bạn chỉ được phép thao tác với schedule của mình.";
        public const string START_TYPE_INVALID = "Kiểu khởi chạy không hợp lệ.";
        public const string LOOP_COUNT_INVALID = "Số lần chạy phải nằm trong khoảng từ 1 -> 100000.";
        public const string START_FAILED = "Khởi chạy thất bại.";
        public const string COURSE_IS_NULL = "Không tìm thấy môn học cần đăng ký.";
        public const string MYDTU_LOGIN_FALIED = "Đăng nhập MyDTU thất bại, vui lòng cập nhật SessionId rồi thử lại.";
        public const string MYDTU_INFO_IS_NOT_MATCH = "Thông tin cá nhân của bạn không trùng khớp với MyDTU.";
        public const string MYDTU_REGISTER_IN_PROCESS = "Tiến trình đăng ký vẫn đang hoạt động.";
        public const string NUMBER_LEARNING_UNITS_OVER_20 = "Bạn chỉ được phép đăng ký tối đa 20 tín chỉ.";
        public const string SCHOOL_YEAR_INVALID = "Dữ liệu năm học không hợp lệ.";
        public const string DELETE_PROXY_FAILED = "Xoá proxy thất bại.";
        public const string PROXY_INVALID = "Thông tin proxy không hợp lệ.";
        public const string PROXY_EXPIRATION_DATE_INVALID = "Ngày hết hạn proxy không hợp lệ.";
        public const string UPDATE_PROXY_FAILED = "Cập nhật proxy thất bại.";
        #endregion

        #region Activities
        public const string ACTIVITY_IS_NULL = "Hoạt động không tồn tại.";
        public const string ACTIVITY_TITLE_INVALID = "Tiêu đề hoạt động không hợp lệ.";
        public const string ACTIVITY_CONTENT_INVALID = "Nội dung hoạt động không hợp lệ.";
        public const string ACTIVITY_POSTER_INVALID = "Poster hoạt động không hợp lệ.";
        public const string ACTIVITY_START_TIME_INVALID = "Thời gian bắt đầu hoạt động không hợp lệ.";
        public const string ACTIVITY_END_TIME_INVALID = "Thời gian kết thúc hoạt động không hợp lệ.";
        public const string ACTIVITY_DEADLINE_TIME_INVALID = "Hạn cuối đăng ký tham gia hoạt động không hợp lệ.";
        public const string ACTIVITY_IS_FINISHED = "Hoạt động này đã kết thúc, không thể cập nhật.";
        public const string DELETE_ACTIVITY_FAILED = "Xoá hoạt động thất bại.";
        public const string ACTIVITY_IS_PRIVATE = "Hoạt động nội bộ yêu cầu đăng nhập mới được xem.";
        public const string ACTIVITY_HISTORIES_IS_NULL = "Không tìm thấy hoạt động bạn đã tham gia.";
        public const string MEMBER_IN_ACTIVITY_IS_NULL = "Chưa có thành viên đăng ký tham gia hoạt động này.";
        public const string MEMBER_IN_ACTIVITY_EXPORT_FAILED = "Đã xảy ra lỗi khi xuất báo cáo hoạt động.";
        public const string MEMBER_REGISTER_INVALID = "Thông tin đăng ký không hợp lệ.";
        public const string MEMBER_REGISTER_FULL_NAME_INVALID = "Họ và tên không hợp lệ.";
        public const string MEMBER_REGISTER_PHONENUMBER_INVALID = "Số điện thoại không hợp lệ.";
        public const string MEMBER_REGISTER_EMAIL_INVALID = "Email không hợp lệ.";
        public const string MEMBER_REGISTER_FACULTY_INVALID = "Khoa/Viện không hợp lệ.";
        public const string MEMBER_REGISTER_CLASS_NAME_INVALID = "Tên lớp không hợp lệ.";
        public const string MEMBER_REGISTER_STUDENTID_IS_MEMBER = "Nếu là thành viên CLB vui lòng đăng nhập trước khi đăng ký.";
        public const string MEMBER_REGISTER_STUDENTID_IS_EXIST = "MSSV đã được đăng ký trước đó. Nếu không phải là bạn vui long liên hệ BCN để được giải quyết.";
        public const string ACTIVITY_IS_EXPIRED_REGISTER = "Hoạt động này đã hết thời hạn đăng ký.";
        public const string ACTIVITY_IS_NOT_PUBLIC = "Bạn không thể đăng ký tham gia hoạt động nội bộ.";
        public const string MEMBER_REGISTER_ACTIVITY_FAILED = "Đã xảy ra lỗi trong quá trình đăng ký tham gia hoạt động.";
        public const string MEMBER_LEAVE_ACTIVITY_MODEL_INVALID = "Dữ liệu yêu cầu không hợp lệ.";
        public const string MEMBER_LEAVE_ACTIVITY_RASON_INVALID = "Lý do xin nghỉ phép không hợp lệ.";
        public const string LEAVE_ACTIVITY_IS_EXIST = "Bản đăng ký nghỉ phép đã tồn tại.";
        public const string REGISTER_LEAVE_ACTIVITY_FAILED = "Đã xảy ra lỗi khi đăng ký nghỉ phép.";
        public const string DELETE_MEMBER_ACTIVITY_FAILED = "Đã xảy ra lỗi khi đăng ký tham gia của thành viên.";
        public const string CHECKIN_ACTIVITY_FAILED = "Đã xuất hiện lỗi khi Check in/ Check out.";
        public const string MEMBER_IS_NOT_EXIST_ACTIVITY = "Thành viên chưa đăng ký tham gia hoạt động.";
        public const string DEL_CHECKIN_FAILED = "Xoá điểm danh thất bại.";
        public const string UPDATE_CHECKIN_MODEL_INVALID = "Dữ liệu cập nhật điểm danh không hợp lệ.";
        public const string CHECKIN_DOES_NOT_EXIST = "Không tồn tại bản điểm danh này.";
        public const string CHECKIN_TIME_INVALID = "Thời gian check in không hợp lệ.";
        public const string CHECKOUT_TIME_INVALID = "Thời gian check out không hợp lệ.";
        public const string DIFFERENT_DAY = "Chỉ được phép cập nhật thời gian check in và check out trong cùng 1 ngày.";
        public const string START_BIGGER_END_DATETIME = "Thời gian check out phải lớn hơn thời gian check in.";
        public const string UPDATE_CHECKIN_ONLY_DATE = "Bạn chỉ được phép thay đổi giờ, không được phép thay đổi ngày điểm danh.";
        public const string CERTIFICATE_UPDATE_INVALID = "Giấy chứng nhận không hợp lệ.";
        public const string UPDATE_CERTIFICATE_FAILED = "Cập nhật giấy chứng nhận thất bại.";
        public const string TEXT_FONT_IS_EMPTY = "Không có font chữ nào trong CSDL.";
        public const string SETTING_TEXT_REQUEST_INVALID = "Setting text không hợp lệ.";
        public const string MAX_WIDTH_INVALID = "Giá trị max width phải lớn hơn 100px.";
        public const string MAX_HEIGHT_INVALID = "Giá trị max height phải lớn hơn 10px.";
        public const string LOCATION_X_INVALID = "Vị trí text X phải lớn hơn 0.";
        public const string LOCATION_Y_INVALID = "Vị trí text Y phải lớn hơn 0.";
        public const string FONT_SIZE_INVALID = "Cỡ chữ phải lớn hơn 0.";
        public const string VERTICAL_INVALID = "Giá trị Vertical phải ở khoảng từ 0 -> 2.";
        public const string HORIZONTAL_INVALID = "Giá trị Horizontal phải ở khoảng từ 0 -> 2.";
        public const string COLOR_INVALID = "Giá trị mà chữ không hợp lệ.";
        public const string FONT_STYLE_INVALID = "Font style không hợp lệ.";
        public const string UPDATE_SETTING_TEXT_FAILED = "Cập nhật setting text thất bại.";
        public const string DELETE_SETTING_TEXT_FAILED = "Xoá setting text thất bại.";
        public const string SETTING_TEXT_IS_EMPTY = "Không tồn tại cài đặt text nào.";
        public const string CERTIFICATE_IS_SENDING = "Quá trình gửi giấy chứng nhận đang diễn ra, bạn không thể thực hiện thao tác này.";
        public const string CERTIFICATE_IS_NULL = "Giấy chứng nhận không tồn tại. Vui lòng cập nhật giấy chứng nhận.";
        public const string SEND_CERTIFICATE_REQUEST_INVALID = "Yêu cầu gửi giấy chứng nhận không hợp lệ.";
        public const string IS_NOT_SEND_CERTIFICATE = "Giấy chứng nhận chưa được cấp.";
        public const string STATISTICS_IS_EMPTY = "Chưa có dữ liệu thống kê.";
        #endregion

        #region Newspaper
        public const string CREATE_NEWS_REQUEST_INVALID = "Yêu cầu tạo tin tức không hợp lệ.";
        public const string NEWS_TITLE_INVALID = "Tiêu đề tin tức không hợp lệ.";
        public const string NEWS_CONTENT_INVALID = "Nội dung tin tức không hợp lệ.";
        public const string NEWS_POSTER_INVALID = "Poster tin tức không hợp lệ.";
        public const string UPDATE_NEWSPAPER_FAILED = "Cập nhật tin tức thất bại.";
        public const string NEWSPAPER_IS_NULL = "Tin tức không tồn tại.";
        public const string DELETE_NEWS_FAILED = "Xoá tin tức thất bại.";
        public const string UPLOAD_INVALID = "File upload không hợp lệ.";
        #endregion

        #region Facultys
        public const string FACULTY_NAME_EXIST = "Tên khoa đã tồn tại.";
        public const string FACULTY_NAME_INVALID = "Tên khoa không được để trống.";
        public const string UPDATE_FACULTY_FAILED = "Cập nhật khoa/viện thất bại.";
        public const string FACULTY_IS_NULL = "Khoa/viện không tồn tại.";
        #endregion

        #region Roles
        public const string GET_ALL_PERMISSION_FAILED = "Lấy danh sách quyền hệ thống thất bại.";
        public const string CREATE_ROLE_FAILED = "Tạo chức vụ thất bại.";
        public const string ROLE_NAME_EXIST = "Tên chức vụ đã tồn tại.";
        public const string ROLE_NAME_INVALID = "Tên chức vụ không hợp lệ.";
        public const string PERMISSION_INVALID = "Quyền hạn cho chức vụ mới không hợp lệ.";
        public const string ROLE_IS_NULL = "Chức vụ không tồn tại.";
        #endregion

        #region Short Link
        public const string SHORT_LINK_IS_NULL = "Không tìm thấy đường dẫn rút gọn.";
        public const string CREATE_SHORT_LINK_FAILED = "Tạo đường dẫn rút gọn thất bại.";
        public const string URL_IS_NULL = "Đường dẫn rút gọn không hợp lệ.";
        #endregion

        #region Software
        public const string UPDATE_SOFTWARE_REQUEST_INVALID = "Dữ liệu cập nhật phần mềm không hợp lệ.";
        public const string TITLE_SOFTWARE_REQUEST_INVALID = "Tiêu đề phần mềm không hợp lệ.";
        public const string DESCRIPTION_SOFTWARE_REQUEST_INVALID = "Mô tả phần mềm không hợp lệ.";
        public const string FILE_NAME_SOFTWARE_REQUEST_INVALID = "Tên phần mềm không hợp lệ.";
        public const string VERSION_SOFTWARE_REQUEST_INVALID = "Phiên bản phần mềm không hợp lệ.";
        public const string SOFTWARE_FILE_REQUEST_INVALID = "File phần mềm không hợp lệ.";
        public const string UPDATE_SOFTWARE_FALIED = "Cập nhật phần mềm lỗi.";
        public const string GG_DRIVE_DISCONECTED = "Mất kết nối với google drive.";
        public const string SOFTWARE_IS_NULL = "Phần mềm này không tồn tại.";
        public const string SOFTWARE_IS_EMPTY = "Không có phần mềm nào đang được chia sẻ.";
        public const string GG_ROLE_TYPE_INVALID = "Phân loại quyền drive không hợp lệ.";
        public const string GG_ROLE_INVALID = "Quyền drive không hợp lệ.";
        public const string GG_ROLE_ADD_FAILED = "Thêm chia sẻ thất bại.";
        public const string GG_ROLE_ADD_REQUEST_INVALID = "Yêu cầu chia sẻ không hợp lệ.";
        public const string GG_ROLE_DELETE_REQUEST_INVALID = "Yêu cầu xoá chia sẻ không hợp lệ.";
        public const string GG_ROLE_ADD_EMAIL_INVALID = "Email không hợp lệ. Chỉ có share file cho bất kỳ ai có đường dẫn thì mới không cần gmail.";
        public const string GG_ROLE_DELETE_ROLLE_FAILED = "Xoá chia sẻ đã xảy ra lỗi.";
        public const string PERMISSION_ID_INVALID = "Id quyền không hợp lệ.";
        public const string DELETE_SOFTWARE_FAILED = "Xoá phần mềm đã xảy ra lỗi.";
        #endregion
    }
}
