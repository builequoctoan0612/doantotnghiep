﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTUSVCv3_Library
{
    public class SecurityConstant
    {
        public const string DEFAULT_HASH_KEY = "76210119DTUHuongPN";
        public const string CRYPTOKEY = "boelQ7K0JjEZcVcmCUCxMCn7iXfkyh8qMB5Z56obsJA=";
        public const int KEY_SIZE = 256;
        public const int IV_SIZE = 16; // block size is 128-bit
    }

    public class SysFolder
    {
        /// <summary>
        /// wwwroot folder path
        /// </summary>
        public static string WWWROOT_PATH;
        /// <summary>
        /// application path
        /// </summary>
        public static string APP_PATH;
        /// <summary>
        /// Error log
        /// </summary>
        public const string ERROR_LOG_PATH = "DTUSVCv3_Log/Errors";
        /// <summary>
        /// Warning log
        /// </summary>
        public const string WARNING_LOG_PATH = "DTUSVCv3_Log/Warning";
        /// <summary>
        /// Login log
        /// </summary>
        public const string LOGIN_LOG_PATH = "DTUSVCv3_Log/Login";
        /// <summary>
        /// forget passwd log
        /// </summary>
        public const string FORGOT_PASSWORD_LOG_PATH = "DTUSVCv3_Log/Forgot_Passwd";
        /// <summary>
        /// Reset passwd log
        /// </summary>
        public const string RESET_PASSWORD_LOG_PATH = "DTUSVCv3_Log/Reset_Passwd";
        /// <summary>
        /// Default log
        /// </summary>
        public const string DEFAULT_LOG_PATH = "DTUSVCv3_Log/Default";
        /// <summary>
        /// Logo Folder
        /// </summary>
        public const string LOGO_FOLDER = "Images/Logo";
        /// <summary>
        /// email temp image folder
        /// </summary>
        public const string EMAIL_TEMP_IMAGE_FOLDER = "Images/EmailTempImage";
        /// <summary>
        /// Avatar folder
        /// </summary>
        public const string AVATAR_FOLDER = "Images/Avatar";
        /// <summary>
        /// Captcha folder
        /// </summary>
        public const string CAPTCHA_FOLDER = "Images/Captcha";
        /// <summary>
        /// Export member list
        /// </summary>
        public const string EXPORT_MEMBER_LIST = "Export/MemberList";
        /// <summary>
        /// Export member register list
        /// </summary>
        public const string EXPORT_MEMBER_REGISTER_LIST = "Export/MemberRegisterList";
        /// <summary>
        /// EXPORT MEMBER REGISTER EVT
        /// </summary>
        public const string EXPORT_MEMBER_REGISTER_EVT = "Export/MemberRegisterEvt";
        /// <summary>
        /// Member card form
        /// </summary>
        public const string MEMBER_CARD_FORM = "MemberCardForm";
        /// <summary>
        /// Member card form
        /// </summary>
        public const string EXPORT_CARD_ALL = "Export/MemberCard";
        /// <summary>
        /// Folder avatar export member card format
        /// </summary>
        public const string EXPORT_CARD_AVATAR = "Avatar";
        /// <summary>
        /// Folder Qr-Code export member card format
        /// </summary>
        public const string EXPORT_CARD_QR_CODE = "QrCode";
        /// <summary>
        /// Qr-Code member
        /// </summary>
        public const string QR_CODE_FOLDER = "Images/QrCodeMember";
        /// <summary>
        /// QR_CODE_CHECKIN_ACTIVITY_FOLDER
        /// </summary>
        public const string QR_CODE_CHECKIN_ACTIVITY_FOLDER = "Images/ActivityQrCode";
        /// <summary>
        /// Page 404
        /// </summary>
        public const string PAGE_404_PATH = "https://dtusvc.com/error404";
        /// <summary>
        /// Reset passwd path
        /// </summary>
        public const string RESET_PASSWD_PATH = "https://dtusvc.com/forgotpasswd?token={0}";
        /// <summary>
        /// Hash Key folder
        /// </summary>
        public const string HASH_KEY_FOLDER = "HashKey";
        /// <summary>
        /// Database old folder
        /// </summary>
        public const string DATABASE_OLD_FOLDER = "BaseData";
        /// <summary>
        /// Email Temp
        /// </summary>
        public const string EMAIL_TEMP_FOLDER = "EmailTemp";
        /// <summary>
        /// Web setting
        /// </summary>
        public const string WEB_SETTING_FOLDER = "WebSetting";
        /// <summary>
        /// Font folder
        /// </summary>
        public const string FONT_FOLDER = "WebSetting/SystemFont";
        /// <summary>
        /// ACTIVITY POSTER PATH
        /// </summary>
        public const string ACTIVITY_POSTER_PATH = "NewsImages/EventPoster";
        /// <summary>
        /// NEWSPAPER_IMAGE
        /// </summary>
        public const string NEWSPAPER_IMAGE = "NewsImages/";
        /// <summary>
        /// ACTIVITY CERTIFICATE PATH
        /// </summary>
        public const string ACTIVITY_CERTIFICATE_PATH = "Images/Certificate";
        /// <summary>
        /// BACKUP_TEMP_FOLDER_PATH
        /// </summary>
        public const string BACKUP_TEMP_FOLDER_PATH = BACKUP_ROOT_FOLDER_PATH + "/TempFolder";
        /// <summary>
        /// BACKUP_DB_FOLDER_PATH
        /// </summary>
        public const string BACKUP_DB_FOLDER_PATH = BACKUP_TEMP_FOLDER_PATH + "/Database";
        /// <summary>
        /// BACKUP_WWWROOT_FOLDER_PATH
        /// </summary>
        public const string BACKUP_WWWROOT_FOLDER_PATH = BACKUP_TEMP_FOLDER_PATH + "/StaticFile";
        /// <summary>
        /// BACKUP_PRIVATE_FOLDER_PATH
        /// </summary>
        public const string BACKUP_PRIVATE_FOLDER_PATH = BACKUP_TEMP_FOLDER_PATH + "/PrivateFile";
        /// <summary>
        /// BACKUP_ROOT_FOLDER_PATH
        /// </summary>
        public const string BACKUP_ROOT_FOLDER_PATH = "Backup";
        /// <summary>
        /// FONT_FILE_FOLDER
        /// </summary>
        public const string FONT_FILE_FOLDER = "TextFont";
        /// <summary>
        /// SOFTWARE
        /// </summary>
        public const string SOFTWARE = "DTUSVCSoftware";
    }

    public class SysFile
    {
        /// <summary>
        /// Error log file name
        /// </summary>
        public const string ERROR = "Errors_{0}.txt";
        /// <summary>
        /// warning log file name
        /// </summary>
        public const string WARNING = "Warning_{0}.txt";
        /// <summary>
        /// Login log file name
        /// </summary>
        public const string LOGIN = "Login_{0}.txt";
        /// <summary>
        /// forgot passwd log file name
        /// </summary>
        public const string FORGOT_PASSWORD = "ForgotPasswd_{0}.txt";
        /// <summary>
        /// reset passwd passwd log file name
        /// </summary>
        public const string RESET_PASSWORD = "ResetPasswd_{0}.txt";
        /// <summary>
        /// Default log file name
        /// </summary>
        public const string DEFAULT = "Default_{0}.txt";
        /// <summary>
        /// Logo file Name
        /// </summary>
        public const string LOGO_FILE_NAME = "LogoCLB.png";
        /// <summary>
        /// Member hash key file name
        /// </summary>
        public const string MEMBER_HASH_KEY_FILE_NAME = "MemberHashKey.dtusvc";
        /// <summary>
        /// Passwd hash key file name
        /// </summary>
        public const string PASSWD_HASH_KEY_FILE_NAME = "PasswdHashKey.dtusvc";
        /// <summary>
        /// Member card form file name
        /// </summary>
        public const string MEMBER_CARD_FROM_FILE = "MemberCardForm.zip";
        /// <summary>
        /// Qr-Code file name format
        /// </summary>
        public const string QR_CODE_NAME_FORMAT = "{0}.png";
        /// <summary>
        /// Font Tahoma file name
        /// </summary>
        public const string TAHOMA_FONT_FILE_NAME = "Tahoma Regular font.ttf";

        #region Email Temp
        /// <summary>
        /// FORGOT PASSWD TEMP
        /// </summary>
        public const string FORGOT_PASSWD_TEMP = "ForgotPasswdEmailTemp.txt";
        /// <summary>
        /// FAILED INTERVIEW RESULT
        /// </summary>
        public const string FAILED_INTERVIEW_RESULT = "FailedInterviewResult.txt";
        /// <summary>
        /// INTERVIEW SCHEDULE CANCEL
        /// </summary>
        public const string INTERVIEW_SCHEDULE_CANCEL = "InterviewScheduleCancel.txt";
        /// <summary>
        /// INTERVIEW EMAIL TEMP
        /// </summary>
        public const string INTERVIEW_EMAIL_TEMP = "InterviewEmailTemp.txt";
        /// <summary>
        /// WELLCOME TO DTUSVC FBURL EMAIL TEMP
        /// </summary>
        public const string WELLCOME_TO_DTUSVC_FBURL_EMAIL_TEMP = "WellcomeToDTUSVCMessageUrl.txt";
        /// <summary>
        /// WELLCOME TO DTUSVC EMAIL TEMP
        /// </summary>
        public const string WELLCOME_TO_DTUSVC_EMAIL_TEMP = "WellcomeToDTUSVC.txt";
        /// <summary>
        /// CONFIRM REGISTER ACTIVITY
        /// </summary>
        public const string CONFIRM_REGISTER_ACTIVITY = "ConfirmRegisterActivity.txt";
        /// <summary>
        /// SEND_CERTIFICATE
        /// </summary>
        public const string SEND_CERTIFICATE = "SendCertificate.txt";
        /// <summary>
        /// Table Check In Content
        /// </summary>
        public const string TABLE_CHECK_IN = "TableCheckInContent.txt";
        /// <summary>
        /// Table Check In Content
        /// </summary>
        public const string TABLE_SCORE = "TableTotal.txt";
        /// <summary>
        /// Happy birth day email temp
        /// </summary>
        public const string HAPPY_BIRTH_DAY = "HappyBirthDay.txt";
        #endregion
        #region Google service
        /// <summary>
        /// USER_CREDENTIALS_FILE
        /// </summary>
        public const string USER_CREDENTIALS_FILE = "client_secret_131536252065-ejre6r6k2mkbgvblond7gud2cpk36d0b.apps.googleusercontent.com.json";
        /// <summary>
        /// TOKEN_STORE_FILE
        /// </summary>
        public const string TOKEN_STORE_FILE = "client_secret_131536252065_token.json";
        #endregion
    }

    public class FileExtension
    {
        /// <summary>
        /// EXCEL EXTENSION
        /// </summary>
        public const string EXCEL_EXTENSION = "xlsx";
    }

    public class DataFilePath
    {
        /// <summary>
        /// Account json
        /// </summary>
        public const string ACCOUNT_FILE_NAME = "Accounts.json";
        /// <summary>
        /// Member json
        /// </summary>
        public const string MEMBERS_FILE_NAME = "Members.json";
        /// <summary>
        /// Address json
        /// </summary>
        public const string ADDRESS_FILE_NAME = "Address.json";
        /// <summary>
        /// Provinces json
        /// </summary>
        public const string PROVINCES_FILE_NAME = "Provinces.json";
        /// <summary>
        /// Districts json
        /// </summary>
        public const string DISTRICTS_FILE_NAME = "Districts.json";
        /// <summary>
        /// Wards json
        /// </summary>
        public const string WARDS_FILE_NAME = "Wards.json";
        /// <summary>
        /// Facultys json
        /// </summary>
        public const string FACULTY_FILE_NAME = "Facultys.json";
        /// <summary>
        /// Roles json
        /// </summary>
        public const string ROLE_FILE_NAME = "Role.json";
        /// <summary>
        /// Activity Check list
        /// </summary>
        public const string ACTIVITY_CHECK_LIST = "ActivityChecklist.json";
        /// <summary>
        /// Recruits
        /// </summary>
        public const string RECRUMENT = "Recruits.json";
        /// <summary>
        /// Web setting file
        /// </summary>
        public const string DTUSVC_SETTING = "DTUSVCSetting.json";
        /// <summary>
        /// Membership Registration
        /// </summary>
        public const string MEMBER_REGISTER = "MembershipRegistration.json";
        /// <summary>
        /// Event info
        /// </summary>
        public const string EVENT_INFO = "ActivitiesAndEvents.json";
        /// <summary>
        /// Member regist event
        /// </summary>
        public const string MEMBER_REGIST_EVT = "EventRegistrationInformationMember.json";
        /// <summary>
        /// Community regist event
        /// </summary>
        public const string COMMUNITY_REGIST_EVT = "EventRegistrationInformationPublic.json";
        /// <summary>
        /// Leave Of Absence
        /// </summary>
        public const string LEAVE_OF_ABSENCE = "LeaveOfAbsence.json";
        /// <summary>
        /// Member support event
        /// </summary>
        public const string MEMBER_SUPPORT_EVENT = "MemberSupportEvents.json";
        /// <summary>
        /// Banner
        /// </summary>
        public const string BANNER = "Banners.json";
        /// <summary>
        /// Newspapers
        /// </summary>
        public const string NEWSPAPER = "Newspapers.json";
        /// <summary>
        /// Regulations
        /// </summary>
        public const string REGULATION = "Regulations.json";
    }

    public static class DatabaseConnect
    {
        /// <summary>
        /// Database path
        /// </summary>
        public const string DATA_BASE_PATH = "mongodb://localhost:27017";
        /// <summary>
        /// Database name
        /// </summary>
        public const string DATA_BASE_NAME = "DTUSVCv3Db";
    }

    public class RegexConstant
    {
        /// <summary>
        /// Regex password
        /// </summary>
        public const string REGEX_PASSWD = @"^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$";
        /// <summary>
        /// Regex link
        /// </summary>
        public const string REGEX_LINK = @"^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+([^\.])$";
        /// <summary>
        /// regex check link facebook
        /// </summary>
        public const string REGEX_FB_PATH = @"^(?:(?:http|https):\/\/)?(?:www.)?(m\.)?facebook.com\/((profile.php(?=(\?id=)([0-9]{15})))|([0-9]{15}))|(.{1,})";
        /// <summary>
        /// Regex check phone number
        /// </summary>
        public const string REGEX_PHONENUMBER = @"(\+)?(84|0[3|5|7|8|9])+([0-9]{8})\b";
        /// <summary>
        /// regex check student id
        /// </summary>
        public const string REGEX_STUDENT_ID = @"^[0-9]{8,20}$";
        /// <summary>
        /// Regex check full name
        /// </summary>
        public const string REGEX_FULL_NAME = @"^[a-zA-Z\x20]+$";
    }

    public static class ContentType
    {
        /// <summary>
        /// Excel content type
        /// </summary>
        public static readonly string[] EXCEL_CONTENT_TYPE = { "application/vnd.ms-excel",
                                                                "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"};
        /// <summary>
        /// Image content file
        /// </summary>
        public static readonly string[] IMAGE_CONTENT_TYPE = {
                                            "image/jpeg",
                                            "image/pjpeg",
                                            "image/png"
                                        };
        public static readonly string[] FONT_CONTENT_TYPE = {
                                            "TrueType font file (.ttf)",
                                            "OpenType font file (.otf)",
                                            "application/octet-stream",
                                            "font/otf",
                                            "font/ttf",
                                            "font/woff",
                                            "font/woff2"
                                        };
        public static readonly string[] ZIP_CONTENT_TYPE = {
                                            "application/zip",
                                            "application/vnd.rar",
                                            "application/x-7z-compressed",
                                            "application/x-zip-compressed",
                                            "application/x-rar-compressed",
                                        };
    }

    public static class FileUploadSize
    {
        //                                   MB  => KB  => Byte
        private const long CONVERSION_RATE = 1024 * 1024 * 1024;

        public const long IMAGE_FILE = 15 * CONVERSION_RATE;
        public const long DOCUMENT_FILE = 5 * CONVERSION_RATE;
        public const long SEND_EMAIL_ATTACHFILE = 20 * CONVERSION_RATE;
        public const long FONT_FILE = 1 * CONVERSION_RATE;
        public const long ZIP_FILE = 100 * CONVERSION_RATE;
    }

    public static class EmailAccount
    {
        public const string ACCOUNT = "noreply@dtusvc.com";
        public const string PASSWD = "01682319448CDlCM@@";
        public const string MAIL_SERVER = "smtp.hostinger.com";
        public const int PORT = 587;
    }

    public static class SystemSetting
    {
        static public bool IS_SYSTEM_MAINTENANCE = false;
    }

    public static class AnonymousService
    {
        static public string[] ANONYMOUS_SERVICE = new string[]
        {
            #region Account
            "/api/Account/Authenticate",
            "/api/Account/ForgotPassword",
            "/api/Account/RefreshAccessToken",
            "/api/Account/CheckForExistingTokens",
            "/api/Account/GetCaptchaForgot",
            "/api/Account/ResetPassword",
            "/api/Account/GetCaptcha",
            #endregion
            #region Recruitment
            "/api/Recruitment/GetRecruitmentValid",
            "/api/Recruitment/ApplyingForMember",
            #endregion
            #region Address
            "/api/Address/GetProvinces",
            "/api/Address/GetDistrictsByProvinceId",
            "/api/Address/GetWardsByDistrictId",
            #endregion
            #region Activities
            "/api/Activities/GetListActivities",
            "/api/Activities/GetActivitieById",
            "/api/Activities/CommunityRegistration",
            "/api/Activities/ConfirmCertificate",
            #endregion
            #region Newspapser
            "/api/Newspaper/GetNewspaperList",
            "/api/Newspaper/GetNewspaperById",
            #endregion
            #region Faculty
            "/api/Facultys/GetAllFacultys",
            #endregion
            "/api/ShortLink/CreateShortLink",
            "/api/ShortLink/GetShortLink",
            #region Short Link
            #region Software
            "/api/Software/GetAllSoftware",
            "/api/Software/GetSoftwareById",
	        #endregion
            #endregion
            "/swagger/v1/swagger.json",
            "/swagger/index.html",
        };
    }

    public class EmailReplaceText
    {
        public const string MEMBER_FULL_NAME = "##MemberFullName##";
        public const string MEMBER_NAME = "##LastName##";
        public const string STUDENT_ID = "##StudentId##";
        public const string END_DATETIME = "##EndDateTime##";
        public const string RESET_PASSWD_PATH = "##ResetPasswdPath##";
        public const string INTERVIEW_TIME = "##InterviewTime##";
        public const string INTERVIEW_ADDRESS = "##InterviewAddress##";
        public const string INTERVIEW_FORMAT = "##InterviewFormat##";
        public const string USERNAME = "##Username##";
        public const string FB_MESSAGE_URL = "##FbMessageUrl##";
        public const string ACTIVITY_NAME = "##ActivityName##";
        public const string REGIST_DATE = "##RegisterDate##";
        public const string START_DATE = "##StartDate##";
        public const string ADDRESS = "##Address##";
        public const string TABLE_CHECK_IN_CONTENT = "##TableCheckIn##";
        public const string TOTAL_SCORE_TABLE = "##TotalScore##";
        public const string TOTAL_SCORE = "##TotalScoreNumber##";
        public const string CERTIFICATE_CONFIRM_PATH = "##CertificateConfirm##";
        public const string CHECK_IN_TIME = "##CheckInTime##";
        public const string CHECK_OUT_TIME = "##CheckOutTime##";
        public const string MANAGER_NAME = "##ManagerName##";
        public const string SCORE = "##Score##";
        public const string OLD = "##Old##";
    }

    public class EmailSubject
    {
        public const string FORGOT_PASSWD = "[DTUSVC] - Khôi phục mật khẩu {0}";
        public const string INTERVIEW_RESULT = "[DTUSVC] - Kết quả phỏng vấn {0}";
        public const string INTERVIEW_SCHEDULE_CANCEL = "[DTUSVC] - Thông báo huỷ lịch phỏng vấn";
        public const string REGISTER_ACTIVITY = "[DTUSVC] - Đăng ký tham gia hoạt động {0} thành công.";
        public const string HAPPY_BIRTH_DAY = "[DTUSVC] - Chúc mừng sinh nhật - {0}.";
    }

    public class GGDriveRootFolderId
    {
        public const string SOFTWARE_FOLDER_ID = "1xDJDTsVHPWcpShLNGomM4fqsaDMGXKpv";
        public const string BACKUP_FOLDER_ID = "1a8YGF4pPJXMjr4QJLPeALSkSB0B6xSn2";
    }
}
