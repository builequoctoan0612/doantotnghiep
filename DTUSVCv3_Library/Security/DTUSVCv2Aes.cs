﻿using Microsoft.AspNetCore.Cryptography.KeyDerivation;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;

namespace DTUSVCv3_Library
{
    public static class DTUSVCv3Aes
    {
        /// <summary>
        /// Decrypt
        /// </summary>
        public static string Decrypt(this string x_strCipherText, string x_strPassWd)
        {
            string strTmp = x_strCipherText;
            return DecryptText(strTmp, x_strPassWd);
        }

        /// <summary>
        /// Encrypt
        /// </summary>
        public static string Encrypt(this string x_strCipherText, string x_strPassWd)
        {
            string strTmp = x_strCipherText;
            return EncryptText(strTmp, x_strPassWd);
        }

        /// <summary>
        /// Encrypt Text private
        /// </summary>
        private static string EncryptText(string x_strText, string x_strPasswd)
        {
            byte[] arrClearBytes;
            string strTmpText;
            Aes objEncryptor;
            MemoryStream objMemoryStream;
            Rfc2898DeriveBytes objRfc2898Derive;
            CryptoStream objCryptoStream;

            try
            {
                strTmpText = x_strText;
                // Skip if input is null or empty
                if ((string.IsNullOrWhiteSpace(strTmpText) == true) || (string.IsNullOrEmpty(x_strPasswd) == true))
                {
                    return strTmpText;
                }

                arrClearBytes = Encoding.Unicode.GetBytes(strTmpText);
                using (objEncryptor = Aes.Create())
                {
                    objRfc2898Derive = new Rfc2898DeriveBytes(x_strPasswd, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                    objEncryptor.Key = objRfc2898Derive.GetBytes(32);
                    objEncryptor.IV = objRfc2898Derive.GetBytes(16);
                    using (objMemoryStream = new MemoryStream())
                    {
                        using (objCryptoStream = new CryptoStream(objMemoryStream, objEncryptor.CreateEncryptor(), CryptoStreamMode.Write))
                        {
                            objCryptoStream.Write(arrClearBytes, 0, arrClearBytes.Length);
                            objCryptoStream.Close();
                        }
                        strTmpText = Convert.ToBase64String(objMemoryStream.ToArray());
                    }
                }

                return strTmpText;
            }
            catch
            {
                return x_strText;
            }
        }

        /// <summary>
        /// Decrypt Text private
        /// </summary>
        private static string DecryptText(string x_strCipherText, string x_strPasswd)
        {
            string srtTpmText;
            byte[] arrCipherBytes;
            Aes objEncryptor;
            Rfc2898DeriveBytes objRfc2898Derive;
            MemoryStream objMemoryStream;
            CryptoStream objCryStream;

            srtTpmText = x_strCipherText;
            if ((string.IsNullOrWhiteSpace(srtTpmText) == true) || (string.IsNullOrWhiteSpace(x_strPasswd) == true))
            {
                return srtTpmText;
            }

            try
            {
                srtTpmText = srtTpmText.Replace(" ", "+");
                arrCipherBytes = Convert.FromBase64String(srtTpmText);
                using (objEncryptor = Aes.Create())
                {
                    objRfc2898Derive = new Rfc2898DeriveBytes(x_strPasswd, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                    objEncryptor.Key = objRfc2898Derive.GetBytes(32);
                    objEncryptor.IV = objRfc2898Derive.GetBytes(16);
                    using (objMemoryStream = new MemoryStream())
                    {
                        using (objCryStream = new CryptoStream(objMemoryStream, objEncryptor.CreateDecryptor(), CryptoStreamMode.Write))
                        {
                            objCryStream.Write(arrCipherBytes, 0, arrCipherBytes.Length);
                            objCryStream.Close();
                        }
                        srtTpmText = Encoding.Unicode.GetString(objMemoryStream.ToArray());
                    }
                }
                return srtTpmText;
            }
            catch
            {
                return x_strCipherText;
            }
        }

        /// <summary>
        /// Object Decript
        /// </summary>
        public static T ObjDecript<T>(this T x_objEncript, string x_strPassWd)
        {
            string strTmpValue;
            object objPropertyTmp;
            PropertyInfo[] arrProperties;
            T objTmp;

            try
            {
                // Skip if x_objEncript is null or PassWd is null
                if ((string.IsNullOrWhiteSpace(x_strPassWd) == true) || (x_objEncript == null))
                {
                    return default(T);
                }

                objTmp = x_objEncript;
                arrProperties = objTmp.GetType().GetProperties();
                foreach (PropertyInfo objProperty in arrProperties)
                {
                    objPropertyTmp = objProperty.GetValue(x_objEncript);
                    // Decript if property is string
                    if (objProperty.PropertyType == typeof(string))
                    {
                        if (objPropertyTmp == null)
                        {
                            continue;
                        }
                        strTmpValue = objPropertyTmp.ToString();
                        // Not decrype if value is MongoId
                        if (strTmpValue.MongoIdIsValid() == false)
                        {
                            strTmpValue = strTmpValue.Decrypt(x_strPassWd);
                        }
                        // Set value to property
                        objProperty.SetValue(objTmp, strTmpValue);
                    }
                    else if (objProperty.PropertyType.IsClass == true)
                    {
                        if (objPropertyTmp == null)
                        {
                            continue;
                        }
                        objPropertyTmp = ObjDecript(objPropertyTmp, x_strPassWd);
                        // Set value to property
                        objProperty.SetValue(objTmp, objPropertyTmp);
                    }
                    else
                    {
                        // Set value to property
                        objProperty.SetValue(objTmp, objPropertyTmp);
                    }
                }

                return (T)objTmp;
            }
            catch
            {
                return default(T);
            }
        }

        /// <summary>
        /// Object Encript
        /// </summary>
        public static T ObjEncript<T>(this T x_objEncript, string x_strPassWd)
        {
            string strTmpValue;
            object objPropertyTmp;
            PropertyInfo[] arrProperties;
            T objTmp;

            try
            {
                // Skip if x_objEncript is null or PassWd is null
                if ((string.IsNullOrWhiteSpace(x_strPassWd) == true) || (x_objEncript == null))
                {
                    return default(T);
                }

                objTmp = x_objEncript;
                arrProperties = objTmp.GetType().GetProperties();
                foreach (PropertyInfo objProperty in arrProperties)
                {
                    if (objProperty == null)
                    {
                        continue;
                    }
                    objPropertyTmp = objProperty.GetValue(x_objEncript);
                    // Decript if property is string
                    if (objProperty.PropertyType == typeof(string))
                    {
                        if (objPropertyTmp == null)
                        {
                            continue;
                        }
                        strTmpValue = objPropertyTmp.ToString();
                        // Not decrype if value is MongoId
                        if (strTmpValue.MongoIdIsValid() == false)
                        {
                            strTmpValue = strTmpValue.Encrypt(x_strPassWd);
                        }
                        // Set value to property
                        objProperty.SetValue(objTmp, strTmpValue);
                    }
                    else if (objProperty.PropertyType.IsClass == true)
                    {
                        if (objPropertyTmp == null)
                        {
                            continue;
                        }
                        objPropertyTmp = ObjEncript(objPropertyTmp, x_strPassWd);
                        // Set value to property
                        objProperty.SetValue(objTmp, objPropertyTmp);
                    }
                    else
                    {
                        // Set value to property
                        objProperty.SetValue(objTmp, objPropertyTmp);
                    }
                }

                return (T)objTmp;
            }
            catch
            {
                return default(T);
            }
        }

        /// <summary>
        /// Create Encryption Stream
        /// </summary>
        public static CryptoStream CreateEncryptionStream(Stream objStream)
        {
            byte[] arrIV;
            byte[] arrKey;
            Rijndael objRijndael;
            CryptoStream objEncryptor;

            arrIV = new byte[SecurityConstant.IV_SIZE];
            arrKey = Convert.FromBase64String(SecurityConstant.CRYPTOKEY);

            using (RNGCryptoServiceProvider objRNG = new RNGCryptoServiceProvider())
            {
                // Using a cryptographic random number generator
                objRNG.GetNonZeroBytes(arrIV);
            }

            // Write IV to the start of the stream
            objStream.Write(arrIV, 0, arrIV.Length);

            objRijndael = new RijndaelManaged();
            objRijndael.KeySize = SecurityConstant.KEY_SIZE;

            objEncryptor = new CryptoStream(objStream, objRijndael.CreateEncryptor(arrKey, arrIV), CryptoStreamMode.Write);
            return objEncryptor;
        }

        /// <summary>
        /// Create Decryption Stream
        /// </summary>
        /// <exception cref="ApplicationException">Failed to read IV from stream.</exception>
        public static CryptoStream CreateDecryptionStream(Stream strInputStream)
        {
            byte[] arrIV;
            byte[] arrKey;
            Rijndael objRijndael;
            CryptoStream objDecryptor;

            arrIV = new byte[SecurityConstant.IV_SIZE];
            arrKey = Convert.FromBase64String(SecurityConstant.CRYPTOKEY);

            if (strInputStream.Read(arrIV, 0, arrIV.Length) != arrIV.Length)
            {
                throw new ApplicationException("Failed to read IV from stream.");
            }

            objRijndael = new RijndaelManaged();
            objRijndael.KeySize = SecurityConstant.KEY_SIZE;

            objDecryptor = new CryptoStream(strInputStream, objRijndael.CreateDecryptor(arrKey, arrIV), CryptoStreamMode.Read);
            return objDecryptor;
        }


        /// <summary>
        /// Get Salt
        /// </summary>
        public static string GetSalt(byte[] x_arrSalt = null)
        {
            RandomNumberGenerator objRandomNumberGenerator;

            if (x_arrSalt == null || x_arrSalt.Length != 16)
            {
                // generate a 128-bit salt using a secure PRNG
                x_arrSalt = new byte[128 / 8];
                using (objRandomNumberGenerator = RandomNumberGenerator.Create())
                {
                    objRandomNumberGenerator.GetBytes(x_arrSalt);
                }
            }

            return Convert.ToBase64String(x_arrSalt);
        }

        /// <summary>
        /// Hash Password With Salt
        /// </summary>
        public static string HashPasswordWithSalt(string x_strPassword, string x_strSalt)
        {
            return Convert.ToBase64String(KeyDerivation.Pbkdf2(
                password: x_strPassword,
                salt: Convert.FromBase64String(x_strSalt),
                prf: KeyDerivationPrf.HMACSHA256,
                iterationCount: 10000,
                numBytesRequested: 256 / 8));
        }

        /// <summary>
        /// Verify Password
        /// </summary>
        public static bool VerifyPassword(string x_strSalt, string x_strRequestPasswd, string x_strPasswdToCheck)
        {
            string strHashOfPasswdToCheck;

            strHashOfPasswdToCheck = HashPasswordWithSalt(x_strRequestPasswd, x_strSalt);
            if (string.Compare(x_strPasswdToCheck, strHashOfPasswdToCheck, false) == 0)
            {
                return true;
            }
            return false;
        }
    }
}
