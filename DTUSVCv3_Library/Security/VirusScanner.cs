﻿using Microsoft.AspNetCore.Http;
using nClam;
using System.Net;

namespace DTUSVCv3_Library
{
    //------------------------------------------------
    // NOTE: Install ClamAV in Server
    //------------------------------------------------
    // sudo apt update
    // sudo apt install clamav clamav-daemon -y
    // clamscan -V
    // sudo systemctl status clamav-freshclam
    // sudo systemctl start clamav-freshclam
    //-------------------------------------------------
    public static class VirusScanner
    {
        #region Type
        public const string IS_DISCONNECT_SERVER = "Không kết nối được với máy chủ quét virus.";
        public const string FILE_IS_NULL = "File không tồn tại.";
        public const string IS_NOT_EXIST_VIRUS = "Không tìm thấy virus.";
        public const string EXIST_VIRUS = "File chứa virus.";
        #endregion

        /// <summary>
        /// Scan file
        /// </summary>
        /// <param name="x_objFile"></param>
        /// <returns></returns>
        public static string ScanVirus(this IFormFile x_objFile)
        {
            Task objTask;
            ClamClient objClam;
            ClamScanResult objScanResult;
            string strResult;
            byte[] arrFileBytes;
            try
            {

                strResult = string.Empty;
                objTask = Task.Run(async () =>
                {
                    if ((x_objFile == null) || (x_objFile.Length == 0))
                    {
                        strResult = FILE_IS_NULL;
                    }
                    else
                    {
                        using (MemoryStream objStream = new MemoryStream())
                        {
                            x_objFile.OpenReadStream().CopyTo(objStream);
                            arrFileBytes = objStream.ToArray();
                            strResult = string.Empty;
                            try
                            {
                                // Scan with Clam Av server  
                                objClam = new ClamClient(IPAddress.Parse("127.0.0.1"), 3310);
                                objScanResult = await objClam.SendAndScanFileAsync(arrFileBytes);

                                switch (objScanResult.Result)
                                {
                                    case ClamScanResults.Clean:
                                        strResult = IS_NOT_EXIST_VIRUS;
                                        break;
                                    case ClamScanResults.VirusDetected:
                                        strResult = EXIST_VIRUS;
                                        break;
                                    default:
                                        strResult = FILE_IS_NULL;
                                        break;
                                }
                            }
                            catch
                            {
                                strResult = IS_DISCONNECT_SERVER;
                            }
                        }
                    }
                });
                objTask.Wait();
                return strResult;
            }
            catch
            {
                return IS_DISCONNECT_SERVER;
            }
        }
    }
}
