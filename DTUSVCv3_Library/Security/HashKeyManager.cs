﻿using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace DTUSVCv3_Library
{
    public static class HashKeyManager
    {
        private static Dictionary<string, string> m_dicMemberHashKey;
        private static Dictionary<string, string> m_dicPasswdHashKey;

        #region Methods
        #region Member Hash Key
        /// <summary>
        /// Read Member Hash Key
        /// </summary>
        public static void ReadMemberHashKey()
        {
            string strFilePath;

            strFilePath = GetMemberHashKeyFile();

            // Read binary file
            m_dicMemberHashKey = ObjHelper.ReadBinaryFile<Dictionary<string, string>>(strFilePath);
        }

        /// <summary>
        /// Set Member Hash Key
        /// </summary>
        public static string GetNewMemberHashKey(string x_strMemberId)
        {
            string strSalt;

            strSalt = DTUSVCv3Aes.GetSalt();

            if (m_dicMemberHashKey == null)
            {
                ReadMemberHashKey();
            }
            if (m_dicMemberHashKey == null)
            {
                m_dicMemberHashKey = new Dictionary<string, string>();
            }

            // Set Hash key to member variable
            m_dicMemberHashKey[x_strMemberId] = strSalt;

            Task.Run(() =>
            {
                // Save to Binary File
                SaveMemberHashKeyToBinaryFile();
            });

            return strSalt;
        }

        /// <summary>
        /// Get Member Hash Key
        /// </summary>
        public static string GetMemberHashKey(string x_strMemberId)
        {
            try
            {
                if (m_dicMemberHashKey == null)
                {
                    ReadMemberHashKey();
                }

                if (m_dicMemberHashKey.ContainsKey(x_strMemberId) == true)
                {
                    return m_dicMemberHashKey[x_strMemberId];
                }
                return string.Empty;
            }
            catch
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Set Member Hash Key
        /// </summary>
        /// <param name="x_strMemberId"></param>
        /// <param name="x_strHashKey"></param>
        public static void SetMemberHashKey(string x_strMemberId, string x_strHashKey)
        {
            try
            {
                m_dicMemberHashKey[x_strMemberId] = x_strHashKey;
                Task.Run(() =>
                {
                    // Save to Binary File
                    SaveMemberHashKeyToBinaryFile();
                });

            }
            catch
            {
            }
        }

        /// <summary>
        /// Save Member Hash Key To Binary File
        /// </summary>
        private static void SaveMemberHashKeyToBinaryFile()
        {
            string strFilePath;

            strFilePath = GetMemberHashKeyFile();
            // Write binary file
            m_dicMemberHashKey.WriteBinaryFile(strFilePath);
        }
        #endregion

        /// <summary>
        /// Get Folder Hash Key Path
        /// </summary>
        private static string GetFolderHashKeyPath()
        {
            return Path.Combine(SysFolder.APP_PATH, SysFolder.HASH_KEY_FOLDER);
        }

        /// <summary>
        /// Get Member Hash Key File
        /// </summary>
        private static string GetMemberHashKeyFile()
        {
            string strFilePath;
            string strFolderPath;
            strFolderPath = GetFolderHashKeyPath();
            strFilePath = Path.Combine(strFolderPath, SysFile.MEMBER_HASH_KEY_FILE_NAME);
            return strFilePath;
        }

        #region Passwd Hash Key
        /// <summary>
        /// Read Passwd Hash Key
        /// </summary>
        public static void ReadPasswdHashKey()
        {
            string strFilePath;

            strFilePath = GetPasswdHashKeyFile();

            // Read binary file
            m_dicPasswdHashKey = ObjHelper.ReadBinaryFile<Dictionary<string, string>>(strFilePath);
        }

        /// <summary>
        /// Set Passwd Hash Key
        /// </summary>
        public static string GetNewPasswdHashKey(string x_strAccountId)
        {
            string strSalt;

            strSalt = DTUSVCv3Aes.GetSalt();

            if (m_dicPasswdHashKey == null)
            {
                ReadPasswdHashKey();
            }

            if (m_dicPasswdHashKey == null)
            {
                m_dicPasswdHashKey = new Dictionary<string, string>();
            }

            // Set Hash key to Passwd variable
            m_dicPasswdHashKey[x_strAccountId] = strSalt;

            Task.Run(() =>
            {
                // Save to Binary File
                SavePasswdHashKeyToBinaryFile();
            });

            return strSalt;
        }

        /// <summary>
        /// Set Passwd Hash Key
        /// </summary>
        public static string AddPasswdHashKey(string x_strAccountId, string x_strHashKey)
        {
            string strSalt;

            strSalt = x_strHashKey;

            if (m_dicPasswdHashKey == null)
            {
                m_dicPasswdHashKey = new Dictionary<string, string>();
            }

            // Set Hash key to Passwd variable
            m_dicPasswdHashKey[x_strAccountId] = strSalt;

            Task.Run(() =>
            {
                // Save to Binary File
                SavePasswdHashKeyToBinaryFile();
            });

            return strSalt;
        }

        /// <summary>
        /// Get Passwd Hash Key
        /// </summary>
        public static string GetPasswdHashKey(string x_strAccountId)
        {
            try
            {
                if (m_dicPasswdHashKey == null)
                {
                    ReadPasswdHashKey();
                }

                if (m_dicPasswdHashKey.ContainsKey(x_strAccountId) == true)
                {
                    return m_dicPasswdHashKey[x_strAccountId];
                }
                return string.Empty;
            }
            catch
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Save Passwd Hash Key To Binary File
        /// </summary>
        private static void SavePasswdHashKeyToBinaryFile()
        {
            string strFilePath;

            strFilePath = GetPasswdHashKeyFile();
            // Write binary file
            m_dicPasswdHashKey.WriteBinaryFile(strFilePath);
        }

        /// <summary>
        /// Get Passwd Hash Key File
        /// </summary>
        private static string GetPasswdHashKeyFile()
        {
            string strFilePath;
            string strFolderPath;
            strFolderPath = GetFolderHashKeyPath();
            strFilePath = Path.Combine(strFolderPath, SysFile.PASSWD_HASH_KEY_FILE_NAME);
            return strFilePath;
        }
        #endregion
        #endregion
    }
}
