﻿using System.Runtime.Serialization.Formatters.Binary;
using System.Security.Cryptography;

namespace DTUSVCv3_Library
{
    public static class FileEncryptHelper
    {
        #region Types
        private static readonly object m_objSaveLock = new object();
        private static string SOURCE = "FileHelper.cs";
        #endregion

        /// <summary>
        /// Write Binary File
        /// </summary>
        public static bool WriteBinaryFile<T>(this T x_obj, string x_strFilePath)
        {
            string sFolderPath;
            FileStream objFS;
            BinaryFormatter objBF;

            lock (m_objSaveLock)
            {
                // Skip if x_objWaferData is null
                if (x_obj == null)
                {
                    return false;
                }
                // Get hash key Folder
                sFolderPath = Path.GetDirectoryName(x_strFilePath);
                // Create new Folder if Folder is not Exists
                if (Directory.Exists(sFolderPath) == false)
                {
                    Directory.CreateDirectory(sFolderPath);
                }

                try
                {
                    using (objFS = new FileStream(x_strFilePath, FileMode.Create))
                    {
                        using (CryptoStream objCryptoStream = DTUSVCv3Aes.CreateEncryptionStream(objFS))
                        {
                            if (ReferenceEquals(null, objCryptoStream))
                            {
                                return false;
                            }
                            objBF = new BinaryFormatter();
                            //シリアル化して書き込む
                            objBF.Serialize(objCryptoStream, x_obj);
                            // Write log
                            objCryptoStream.Close();
                            objFS.Close();
                            return true;
                        }
                    }
                }
                catch
                {
                    return false;
                }
            }
        }

        /// <summary>
        /// Read file binary
        /// </summary>
        public static T ReadBinaryFile<T>(string x_strFilePath, string x_strComment)
        {
            object? objData = null;
            FileStream objFS;
            BinaryFormatter objBF;
            T objRes;

            lock (m_objSaveLock)
            {
                try
                {
                    if (File.Exists(x_strFilePath) == true)
                    {
                        using (objFS = new FileStream(x_strFilePath, FileMode.Open, FileAccess.Read, FileShare.Read))
                        {
                            using (CryptoStream objCryptoStream = DTUSVCv3Aes.CreateDecryptionStream(objFS))
                            {
                                objBF = new BinaryFormatter();
                                //読み込んで逆シリアル化する
                                objData = objBF.Deserialize(objCryptoStream);

                                LogHelper.WriteLog(LogTypeEnum.DEFAULT, string.Empty, string.Empty, x_strComment, SOURCE);
                                objCryptoStream.Close();
                                objFS.Close();
                                // Convert Data
                                if (objData != null)
                                {
                                    objRes = (T)objData;
                                    return objRes;
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    string strError;
                    strError = ex.Message;
                    // Write Error Log
                    LogHelper.WriteLog(LogTypeEnum.ERRORS, string.Empty, string.Empty, strError, SOURCE);
                }
                return default;
            }
        }
    }
}
