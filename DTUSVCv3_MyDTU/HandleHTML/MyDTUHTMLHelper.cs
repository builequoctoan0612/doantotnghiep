﻿using DTUSVCv3_EntityFramework;
using DTUSVCv3_Library;
using HtmlAgilityPack;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace DTUSVCv3_MyDTU
{
    public static class MyDTUHTMLHelper
    {
        /// <summary>
        /// Convert String To Model
        /// </summary>
        /// <param name="x_strData"></param>
        /// <param name="x_objDatabase"></param>
        /// <returns></returns>
        public static CourseInformation ConvertStrToModel(string x_strData, MyDTUDbManager x_objDatabase)
        {
            const string REGISTER_CODE = "Mã đăng ký:";
            const string SEMESTER = "Học kỳ:";
            const string NUMBER_LEARNING_UNIT = "Số ĐVHT:";
            const string TIME = "Ngày, giờ học:";
            const string START_END = "Bắt đầu - Kết thúc:";
            const string CLASS_ROOM = "Phòng học:";
            const string LECTURER = "Giảng viên:";

            Dictionary<string, List<string>> dicVal;
            List<string> lstVal;
            CourseInformation objCourseInfo;
            CourseInformation objTempCourseInfo;
            HtmlDocument objDocument;
            HtmlNodeCollection objNode;
            HtmlNode objTempNode;
            HtmlNode objBufNode;
            Task objTask;
            int nBuf;
            string[] arrBuf;

            try
            {
                if (string.IsNullOrWhiteSpace(x_strData) == true)
                {
                    return null;
                }

                objDocument = new HtmlDocument();
                objDocument.LoadHtml(x_strData);
                // Get table
                objNode = objDocument.GetElementbyId("tb_chuongtrinhdaotao").ChildNodes;
                if (objNode == null)
                {
                    return null;
                }
                objCourseInfo = new CourseInformation();
                // Get title node
                objTempNode = objNode[1].SelectSingleNode("//div[@class='title-1']");
                arrBuf = objTempNode.InnerText.Split("/");
                arrBuf = arrBuf[0].Replace("&nbsp;", "").Split("–");
                // get Course code
                if (arrBuf.Length > 0)
                {
                    objCourseInfo.Code = arrBuf[0].Trim();
                }
                // get Course name
                if (arrBuf.Length > 1)
                {
                    objCourseInfo.CourseName = arrBuf[1].Trim();
                }
                // Get table
                objTempNode = objNode[1].SelectSingleNode("//table[@class='tb-ctdt tb-chitietclass']");
                objBufNode = objTempNode.ChildNodes[3].ChildNodes[1].ChildNodes[1];
                dicVal = ConvertTableHtmlToDic(objBufNode);
                // get Registration Code
                foreach (string strKey in dicVal.Keys)
                {
                    dicVal.TryGetValue(strKey, out lstVal);
                    if (lstVal.Count == 0)
                    {
                        continue;
                    }
                    // get Register code
                    if (strKey.EqualsIgnoreCase(REGISTER_CODE) == true)
                    {
                        objCourseInfo.RegistrationCode = lstVal[0];
                        if (string.IsNullOrWhiteSpace(objCourseInfo.RegistrationCode) == false)
                        {
                            objTempCourseInfo = null;
                            objTask = Task.Run(async () =>
                            {
                                objTempCourseInfo = await x_objDatabase.GetCourseInfoById(objCourseInfo.RegistrationCode);
                            });
                            objTask.Wait();
                            if (objTempCourseInfo != null)
                            {
                                return objTempCourseInfo;
                            }
                        }
                    }
                    // Get semester
                    if (strKey.EqualsIgnoreCase(SEMESTER) == true)
                    {
                        objCourseInfo.Semester = lstVal[0];
                    }
                    // Get Number Learning Units
                    if (strKey.EqualsIgnoreCase(NUMBER_LEARNING_UNIT) == true)
                    {
                        arrBuf = lstVal[0].Split(' ');
                        int.TryParse(arrBuf[0], out nBuf);
                        objCourseInfo.NumberLearningUnits = nBuf;
                        if (arrBuf.Length > 1)
                        {
                            objCourseInfo.Type = arrBuf[1];
                        }
                    }
                    // Class time
                    if (strKey.EqualsIgnoreCase(TIME) == true)
                    {
                        objCourseInfo.ClassTime = lstVal;
                    }
                    // Start-end date
                    if (strKey.EqualsIgnoreCase(START_END) == true)
                    {
                        arrBuf = lstVal[0].Split('-');
                        objCourseInfo.StartDate = DateTimeHelper.ConvertStrToDateTime(arrBuf[0]);
                        if (arrBuf.Length > 1)
                        {
                            objCourseInfo.EndDate = DateTimeHelper.ConvertStrToDateTime(arrBuf[1]);
                        }
                    }
                    // Room name
                    if (strKey.EqualsIgnoreCase(CLASS_ROOM) == true)
                    {
                        objCourseInfo.Classroom = lstVal;
                    }
                    if (strKey.EqualsIgnoreCase(LECTURER) == true)
                    {
                        objCourseInfo.LecturerName = lstVal[0];
                    }
                }

                if (objCourseInfo.Id.MongoIdIsValid() == false)
                {
                    objTask = Task.Run(async () =>
                    {
                        await x_objDatabase.UpdateCourseInfo(objCourseInfo);
                    });
                    objTask.Wait();
                }
                return objCourseInfo;
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Get Student Info
        /// </summary>
        /// <param name="x_strData"></param>
        /// <returns></returns>
        public static string GetStudentInfo(string x_strData)
        {
            HtmlDocument objDocument;
            HtmlNode objRootNode;
            string strFullName;
            string strStudentId;
            string strSex;
            string strBirthDay;
            try
            {
                if (string.IsNullOrWhiteSpace(x_strData) == true)
                {
                    return null;
                }

                objDocument = new HtmlDocument();
                objDocument.LoadHtml(x_strData);

                objRootNode = objDocument.DocumentNode.SelectSingleNode("//div[@class='info-basic']");
                objRootNode = objRootNode.ChildNodes[1];
                if (objRootNode == null)
                {
                    return string.Empty;
                }
                // 3 - 7 - 11 - 15
                strFullName = ClearText(objRootNode.ChildNodes[3].InnerText);
                strStudentId = ClearText(objRootNode.ChildNodes[7].InnerText);
                strSex = ClearText(objRootNode.ChildNodes[11].InnerText);
                strBirthDay = ClearText(objRootNode.ChildNodes[15].InnerText);
                return string.Format("{0}|{1}|{2}|{3}", strFullName, strStudentId, strSex, strBirthDay);
            }
            catch
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Get Curriculum Id
        /// </summary>
        /// <param name="x_strData"></param>
        /// <returns></returns>
        public static string GetCurriculumId(string x_strData)
        {
            HtmlDocument objDocument;
            HtmlNode objRootNode;
            string strCurriculumId;
            string strButtonHtml;
            string[] arrButtonClickVal;
            try
            {
                if (string.IsNullOrWhiteSpace(x_strData) == true)
                {
                    return null;
                }

                objDocument = new HtmlDocument();
                objDocument.LoadHtml(x_strData);

                objRootNode = objDocument.DocumentNode.SelectSingleNode("//input[@class='btn-dangky btn-dangky-vn']");
                if (objRootNode == null)
                {
                    return string.Empty;
                }
                strButtonHtml = objRootNode.Attributes["onclick"].Value;
                arrButtonClickVal = strButtonHtml.Split(',');
                strCurriculumId = arrButtonClickVal[4].Replace("'", "").Trim();
                return strCurriculumId;
            }
            catch
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Convert Table Html To Dic
        /// </summary>
        /// <param name="x_objTableNode"></param>
        /// <returns></returns>
        private static Dictionary<string, List<string>> ConvertTableHtmlToDic(HtmlNode x_objTableNode)
        {
            Dictionary<string, List<string>> dicVal;
            List<string> lstVal;
            HtmlNode objNodeTemp;
            string strBuf;

            dicVal = new Dictionary<string, List<string>>();
            foreach (HtmlNode objNode in x_objTableNode.ChildNodes)
            {
                if (objNode.Name.EqualsIgnoreCase("tr") == false)
                {
                    continue;
                }

                lstVal = new List<string>();
                strBuf = ClearText(objNode.ChildNodes[1].InnerText);
                dicVal[strBuf] = lstVal;
                if (objNode.ChildNodes.Count < 4)
                {
                    continue;
                }

                objNodeTemp = objNode.ChildNodes[3];
                foreach (HtmlNode objNodeLv2 in objNodeTemp.ChildNodes)
                {
                    // span tag
                    if (objNodeLv2.Name.EqualsIgnoreCase("span") == true)
                    {
                        strBuf = ClearText(objNodeLv2.InnerText);
                        if (string.IsNullOrWhiteSpace(strBuf) == true)
                        {
                            continue;
                        }
                        lstVal.Add(strBuf);
                        continue;
                    }
                    // ul & li
                    if (objNodeLv2.Name.EqualsIgnoreCase("ul") == true)
                    {
                        foreach (HtmlNode objNodeLv3 in objNodeLv2.ChildNodes)
                        {
                            if (objNodeLv3.Name.EqualsIgnoreCase("li") == false)
                            {
                                continue;
                            }

                            strBuf = ClearText(objNodeLv3.InnerText);
                            if (string.IsNullOrWhiteSpace(strBuf) == true)
                            {
                                continue;
                            }
                            lstVal.Add(strBuf);
                        }
                    }
                    // nomal
                    if (lstVal.Count == 0)
                    {
                        strBuf = ClearText(objNodeLv2.InnerText);
                        if (string.IsNullOrWhiteSpace(strBuf) == true)
                        {
                            continue;
                        }
                        lstVal.Add(strBuf);
                    }
                }
            }
            return dicVal;
        }

        /// <summary>
        /// Clear Text
        /// </summary>
        /// <param name="x_strText"></param>
        /// <returns></returns>
        private static string ClearText(string x_strText)
        {
            string strBuf;
            Regex objTrim = new Regex(@"\s\s+");

            if (string.IsNullOrWhiteSpace(x_strText) == true)
            {
                return string.Empty;
            }

            strBuf = x_strText.Replace("&nbsp;", "").Replace("Giờ", "").Replace("\r\n", "").Trim();
            strBuf = objTrim.Replace(strBuf, " ");
            return strBuf;
        }

        /// <summary>
        /// Get Captcha URL
        /// </summary>
        /// <param name="x_strHtmlBody"></param>
        /// <returns></returns>
        public static string GetCaptchaUrl(string x_strHtmlBody)
        {
            HtmlDocument objDocument;
            HtmlNodeCollection objNode;

            try
            {
                if (string.IsNullOrWhiteSpace(x_strHtmlBody) == true)
                {
                    return null;
                }

                objDocument = new HtmlDocument();
                objDocument.LoadHtml(x_strHtmlBody);
                // Get table
                objNode = objDocument.GetElementbyId("imgCapt").ChildNodes;
                if (objNode == null)
                {
                    return null;
                }

                return string.Empty;
            }
            catch
            {
                return string.Empty;
            }
        }
    }
}
