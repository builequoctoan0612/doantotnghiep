﻿using DTUSVCv3_EntityFramework;
using DTUSVCv3_Library;
using DTUSVCv3_MemoryData;
using Newtonsoft.Json;
using System.Drawing;

namespace DTUSVCv3_MyDTU
{
    public class MyDTURequest : Common
    {
        #region Contructor
        public MyDTURequest(List<ProxyMemoryDataModel> x_lstProxyData)
            : base(x_lstProxyData)
        {
            m_objMyDTUDbManager = new MyDTUDbManager();
        }
        #endregion

        #region Fieleds
        private MyDTUDbManager m_objMyDTUDbManager;
        private EventHandler<CaptchaErgArgs> m_objResultCaptchaDetect;
        private string m_strCurrentCaptchaPath;
        private string m_strCurrentCaptcha;
        #endregion

        #region Properties
        public EventHandler<CaptchaErgArgs> ResultCaptchaDetect
        {
            get
            {
                return m_objResultCaptchaDetect;
            }
            set
            {
                m_objResultCaptchaDetect = value;
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Get Course Info By Url
        /// </summary>
        /// <param name="x_strUrl"></param>
        /// <returns></returns>
        public async Task<CourseInformation> GetCourseInfoByUrl(string x_strUrl)
        {
            CourseInformation objCourseInfo;
            string strHtmlVal;

            strHtmlVal = await GetHttpAsync(x_strUrl, null);

            objCourseInfo = MyDTUHTMLHelper.ConvertStrToModel(strHtmlVal, m_objMyDTUDbManager);
            if ((objCourseInfo != null) && (objCourseInfo.Id.MongoIdIsValid() == false))
            {
                await m_objMyDTUDbManager.UpdateCourseInfo(objCourseInfo);
            }
            return objCourseInfo;
        }

        /// <summary>
        /// Reload Register Page
        /// </summary>
        /// <param name="x_strDTUToken"></param>
        /// <returns></returns>
        public async Task<string> GetAndDetectCaptcha(string x_strDTUToken)
        {
            string strResult;
            Dictionary<string, string> dicCookies;
            CaptchaErgArgs objCaptchaErgArgs;

            try
            {
                dicCookies = new Dictionary<string, string>();
                dicCookies[SESSION_ID] = x_strDTUToken;
                // Get new captcha
                await GetHttpAsync(GET_CAPTCHA_IMAGE_URL, dicCookies);

                // try detect
                strResult = string.Empty;
                for (int nIndex = 0; nIndex < 3; nIndex++)
                {
                    strResult = await DetectCaptcha(dicCookies);
                    if (strResult.Length == 4)
                    {
                        return strResult;
                    }
                }

                if (m_objResultCaptchaDetect != null)
                {
                    m_strCurrentCaptcha = strResult;
                    objCaptchaErgArgs = new CaptchaErgArgs
                    {
                        CaptchaCode = m_strCurrentCaptcha,
                        CaptchaPath = m_strCurrentCaptchaPath
                    };
                    m_objResultCaptchaDetect.Invoke(null, objCaptchaErgArgs);
                }

                return strResult;
            }
            catch
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Get Student Info
        /// </summary>
        /// <param name="x_strDTUToken"></param>
        /// <returns></returns>
        public async Task<string> GetStudentInfo(string x_strDTUToken)
        {
            string strHTMLResult;
            string strStudentInfo;
            Dictionary<string, string> dicCookies;

            try
            {
                dicCookies = new Dictionary<string, string>();
                dicCookies[SESSION_ID] = x_strDTUToken;
                strHTMLResult = await GetHttpAsync(STUDENT_INFO_URL, dicCookies);
                strStudentInfo = MyDTUHTMLHelper.GetStudentInfo(strHTMLResult);
            }
            catch
            {
                return string.Empty;
            }
            return strStudentInfo;
        }

        /// <summary>
        /// Get Curriculum Id
        /// </summary>
        /// <param name="x_strDTUToken"></param>
        /// <returns></returns>
        public async Task<string> GetCurriculumId(string x_strDTUToken)
        {
            string strHTMLResult;
            string strStudentInfo;
            string strUrl;
            Dictionary<string, string> dicCookies;

            try
            {
                strUrl = string.Format(REGISTER_ALL_URL, SettingHelper.SemesterId, SettingHelper.YearId);
                dicCookies = new Dictionary<string, string>();
                dicCookies[SESSION_ID] = x_strDTUToken;
                strHTMLResult = await GetHttpAsync(strUrl, dicCookies);
                strStudentInfo = MyDTUHTMLHelper.GetCurriculumId(strHTMLResult);
            }
            catch
            {
                return string.Empty;
            }
            return strStudentInfo;
        }

        /// <summary>
        /// Check Registration Allowed
        /// </summary>
        /// <param name="x_strDTUToken"></param>
        /// <param name="x_strCurriculumId"></param>
        /// <returns></returns>
        private async Task<bool> CheckRegistrationAllowed(string x_strDTUToken, string x_strCurriculumId)
        {
            Dictionary<string, string> dicCookies;
            PreCheckStepModel objPreCheckStepModel;
            PreCheckStepResModel objPreCheckStepResModel;
            string strJsonVal;
            string strResult;

            try
            {
                dicCookies = new Dictionary<string, string>();
                dicCookies[SESSION_ID] = x_strDTUToken;
                objPreCheckStepModel = new PreCheckStepModel();
                objPreCheckStepModel.TermVal = x_strCurriculumId;
                strJsonVal = objPreCheckStepModel.ToJSON();

                // pre-check step
                strResult = await PostHttpAsync(TEMP_ENDPOINT, strJsonVal, "application/json", dicCookies);
                if (string.IsNullOrWhiteSpace(strResult) == true)
                {
                    return false;
                }
                objPreCheckStepResModel = strResult.JSONToObject<PreCheckStepResModel>();
                if (objPreCheckStepResModel.Val.Equals("1") == true)
                {
                    return true;
                }
            }
            catch
            {
                return false;
            }
            return false;
        }

        /// <summary>
        /// Course Registration
        /// </summary>
        /// <param name="x_strDTUToken"></param>
        /// <param name="x_strStudentId"></param>
        /// <param name="x_strClassRegCodes"></param>
        /// <param name="x_strCurriculumId"></param>
        /// <param name="x_strCaptcha"></param>
        /// <returns></returns>
        public async Task<ScheduleCourseType> CourseRegistration(string x_strDTUToken, string x_strStudentId, string x_strClassRegCodes, string x_strCurriculumId, string x_strCaptcha)
        {
            Dictionary<string, string> dicCookies;
            CourseRegistModel objRequsetModel;
            PreCheckStepResModel objPreCheckStepResModel;
            bool bIsRegistrationAllowed;
            string strJsonVal;
            string strData;
            string strResult;

            try
            {
                dicCookies = new Dictionary<string, string>();
                dicCookies[SESSION_ID] = x_strDTUToken;
                // Change proxy
                ReloadCurrentProxy();
                strData = string.Format("{0},{1},{2},{3},{4},{5}", x_strClassRegCodes, SettingHelper.YearId, SettingHelper.SemesterId, x_strStudentId, x_strCurriculumId, x_strCaptcha);
                objRequsetModel = new CourseRegistModel();
                objRequsetModel.EncryptedPara = EncryptDTUBody(strData);
                strJsonVal = objRequsetModel.ToJSON();
                // pre-check step
                bIsRegistrationAllowed = await CheckRegistrationAllowed(x_strDTUToken, x_strCurriculumId);
                if (bIsRegistrationAllowed == false)
                {
                    return ScheduleCourseType.FAILED;
                }
                strResult = await PostHttpAsync(ENDPOINT, strJsonVal, "application/json; charset=utf-8", dicCookies);
                if (string.IsNullOrWhiteSpace(strResult) == true)
                {
                    return ScheduleCourseType.TRYING_AGAIN;
                }
                objPreCheckStepResModel = strResult.JSONToObject<PreCheckStepResModel>();
                if (objPreCheckStepResModel.Val.EqualsIgnoreCase("Bạn không thể Đăng ký Lớp học này vì Lớp đã đủ Sinh viên.") == true)
                {
                    return ScheduleCourseType.TRYING_AGAIN;
                }
                else if (objPreCheckStepResModel.Val.EqualsIgnoreCase("Đăng ký Thành công") == true)
                {
                    return ScheduleCourseType.REGISTERED;
                }
                else if ((objPreCheckStepResModel.Val.EqualsIgnoreCase("Bạn phải Đăng nhập vào hệ thống trước khi Đăng ký Lớp!") == true) ||
                    ((objPreCheckStepResModel.Val.EqualsIgnoreCase("Phiên Đăng ký không hợp lệ. Đề nghị quay ra chọn lại Năm học và Học kỳ cần Đăng ký.") == true)))
                {
                    return ScheduleCourseType.LOGIN_FAILED;
                }
                return ScheduleCourseType.FAILED;
            }
            catch
            {
                return ScheduleCourseType.FAILED;
            }
        }

        /// <summary>
        /// Check Captcha
        /// </summary>
        /// <param name="x_strCaptcha"></param>
        /// <param name="x_strDTUToken"></param>
        /// <returns></returns>
        public async Task<bool> CheckCaptcha(string x_strCaptcha, string x_strDTUToken)
        {
            CheckCaptchaRequest objCheckCaptchaRequest;
            PreCheckStepResModel objPreCheckStepResModel;
            Dictionary<string, string> dicCookies;
            string strJsonVal;
            string strResult;
            try
            {
                dicCookies = new Dictionary<string, string>();
                dicCookies[SESSION_ID] = x_strDTUToken;
                objCheckCaptchaRequest = new CheckCaptchaRequest
                {
                    CaptValue = x_strCaptcha
                };
                // Convert to json
                strJsonVal = objCheckCaptchaRequest.ToJSON();

                strResult = await PostHttpAsync(CHECK_CAPTCHA_URL, strJsonVal, "application/json; charset=utf-8", dicCookies);
                if (string.IsNullOrWhiteSpace(strResult) == true)
                {
                    return false;
                }
                objPreCheckStepResModel = strResult.JSONToObject<PreCheckStepResModel>();

                if (objPreCheckStepResModel.Val == "1")
                {
                    return true;
                }
                return false;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Detect Captcha
        /// </summary>
        /// <param name="x_dicCookies"></param>
        /// <returns></returns>
        private async Task<string> DetectCaptcha(Dictionary<string, string> x_dicCookies)
        {
            string strResult;
            Image objImage;
            DateTime objCurrentDate;

            objCurrentDate = DateTimeHelper.GetCurrentDateTimeVN();
            try
            {
                if (File.Exists(m_strCurrentCaptchaPath) == true)
                {
                    File.Delete(m_strCurrentCaptchaPath);
                }
            }
            catch { }
            m_strCurrentCaptchaPath = FileHelper.GetFilePath(SysFolder.CAPTCHA_FOLDER, $"{objCurrentDate.Ticks}.png");
            // Save captcha
            await SaveCaptchaImageAsync(CAPTCHA_IMAGE_URL, m_strCurrentCaptchaPath, x_dicCookies);
            // Getimage file
            if (File.Exists(m_strCurrentCaptchaPath) == false)
            {
                return string.Empty;
            }

            using (FileStream objFileSt = new FileStream(m_strCurrentCaptchaPath, FileMode.Open, FileAccess.Read))
            {
                objImage = Image.FromStream(objFileSt);
                strResult = ReconhecerCaptcha(objImage);
            }
            return strResult;
        }
        #endregion
    }

    [Serializable]
    public class PreCheckStepModel
    {
        [JsonProperty("termVal")]
        public string TermVal { get; set; }
    }

    [Serializable]
    public class PreCheckStepResModel
    {
        [JsonProperty("d")]
        public string Val { get; set; }
    }

    [Serializable]
    public class CourseRegistModel
    {
        [JsonProperty("encryptedPara")]
        public string EncryptedPara { get; set; }
    }

    [Serializable]
    public class CheckCaptchaRequest
    {
        [JsonProperty("captValue")]
        public string CaptValue { get; set; }
    }

    [Serializable]
    public class CaptchaErgArgs : EventArgs
    {
        [JsonProperty("captchaPath")]
        public string CaptchaPath { get; set; }
        [JsonProperty("captchaCode")]
        public string CaptchaCode { get; set; }
    }

}
