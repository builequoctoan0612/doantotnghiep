﻿using AForge.Imaging.Filters;
using DTUSVCv3_EntityFramework;
using DTUSVCv3_Library;
using DTUSVCv3_MemoryData;
using System.Drawing;
using System.Drawing.Imaging;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using Tesseract;

namespace DTUSVCv3_MyDTU
{
    public class Common
    {
        #region Constructor
        public Common(List<ProxyMemoryDataModel> x_lstProxyData)
        {
            m_nSemesterId = SettingHelper.SemesterId;
            m_nYearId = SettingHelper.YearId;
            m_objBitmapToPixConverter = new BitmapToPixConverter();
            m_strTesseractFolder = FileHelper.GetFolderFullPath(SysFolder.WEB_SETTING_FOLDER, "tessdata", true);
            m_lstProxyData = x_lstProxyData;
            m_lstWebProxy = new List<WebProxy>();
            LoadProxy();
        }
        #endregion

        #region Types
        protected const string REGISTER_ALL_URL = "https://mydtu.duytan.edu.vn/sites/index.aspx?p=home_registeredall&semesterid={0}&yearid={1}";
        protected const string CAPTCHA_IMAGE_URL = "https://mydtu.duytan.edu.vn/Modules/portal/JpegImage.aspx";
        protected const string GET_CAPTCHA_IMAGE_URL = "https://mydtu.duytan.edu.vn/Modules/regonline/ajax/LoadCaptcha.aspx";
        protected const string CHECK_CAPTCHA_URL = "https://mydtu.duytan.edu.vn/Modules/regonline/ajax/LoadCaptcha.aspx/CheckCapt";
        protected const string STUDENT_INFO_URL = "https://mydtu.duytan.edu.vn/sites/index.aspx?p=home_userinfor&functionid=30";
        protected const string TEMP_ENDPOINT = "https://mydtu.duytan.edu.vn/Modules/regonline/ajax/LoadCaptcha.aspx/Temp";
        protected const string ENDPOINT = "https://mydtu.duytan.edu.vn/Modules/regonline/ajax/RegistrationProcessing.aspx/AddClassProcessing";
        protected const string SESSION_ID = "ASP.NET_SessionId";
        #endregion

        #region Fields
        protected int m_nSemesterId;
        protected int m_nYearId;
        private string m_strTesseractFolder;
        private BitmapToPixConverter m_objBitmapToPixConverter;
        private List<ProxyMemoryDataModel> m_lstProxyData;
        private List<WebProxy> m_lstWebProxy;
        private WebProxy m_objCurrentWebProxy;
        #endregion

        #region Methods
        /// <summary>
        /// Load Proxy
        /// </summary>
        private void LoadProxy()
        {
            WebProxy objWebProxy;
            DateTime objCurrentDate;

            if ((m_lstProxyData == null) || (m_lstProxyData.Count == 0))
            {
                return;
            }
            // Load proxy
            objCurrentDate = DateTimeHelper.GetCurrentDateTimeVN();
            foreach (ProxyMemoryDataModel objProxyData in m_lstProxyData)
            {
                if ((objProxyData == null) || (objCurrentDate <= objProxyData.ExpirationDate))
                {
                    continue;
                }

                objWebProxy = new WebProxy(objProxyData.URL, objProxyData.Port)
                {
                    BypassProxyOnLocal = false,
                    UseDefaultCredentials = false,
                    Credentials = new NetworkCredential
                    {
                        UserName = objProxyData.Username,
                        Password = objProxyData.Passwd
                    }
                };
                m_lstWebProxy.Add(objWebProxy);
            }
            // Reload  current proxy
            ReloadCurrentProxy();
        }

        /// <summary>
        /// Reload Proxy
        /// </summary>
        public void ReloadCurrentProxy()
        {
            Random objRandom;
            int nIndex;

            if ((m_lstWebProxy == null) || (m_lstWebProxy.Count == 0))
            {
                m_objCurrentWebProxy = null;
            }

            objRandom = new Random();
            nIndex = objRandom.Next(0, (m_lstWebProxy.Count - 1));
            m_objCurrentWebProxy = m_lstWebProxy[nIndex];
        }

        /// <summary>
        /// Set Semester And Year Id
        /// </summary>
        /// <param name="x_nSemesterId"></param>
        /// <param name="x_nYearId"></param>
        public void SetSemesterAndYearId(int x_nSemesterId, int x_nYearId)
        {
            m_nSemesterId = x_nSemesterId;
            m_nYearId = x_nYearId;
        }
        /// <summary>
        /// Add Cookie
        /// </summary>
        /// <param name="x_objWebRequest"></param>
        /// <param name="x_objCookie"></param>
        /// <returns></returns>
        private bool AddCookie(WebRequest x_objWebRequest, Cookie x_objCookie, Uri x_objUri = null)
        {
            HttpWebRequest objHttpRequest;

            objHttpRequest = x_objWebRequest as HttpWebRequest;
            if (objHttpRequest == null)
            {
                return false;
            }

            if (objHttpRequest.CookieContainer == null)
            {
                objHttpRequest.CookieContainer = new CookieContainer();
            }

            if (x_objUri == null)
            {
                objHttpRequest.CookieContainer.Add(x_objCookie);
            }
            else
            {
                objHttpRequest.CookieContainer.Add(x_objUri, x_objCookie);
            }
            return true;
        }

        /// <summary>
        /// Get Http Async
        /// </summary>
        /// <param name="x_strUrl"></param>
        /// <param name="x_dicCookies"></param>
        /// <returns></returns>
        public async Task<string> GetHttpAsync(string x_strUrl, Dictionary<string, string> x_dicCookies)
        {
            HttpWebRequest objRequest;
            Cookie objCookie;
            Uri objUri;
            string strValue;

            try
            {
                objRequest = (HttpWebRequest)WebRequest.Create(x_strUrl);
                objRequest.Timeout = 3 * 60 * 1000;
                // Add proxy
                if (m_objCurrentWebProxy != null)
                {
                    objRequest.Proxy = m_objCurrentWebProxy;
                }
                objRequest.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;
                // Add cookie
                if (x_dicCookies != null)
                {
                    objUri = new Uri(x_strUrl);
                    foreach (string strKey in x_dicCookies.Keys)
                    {
                        objCookie = new Cookie(strKey, x_dicCookies[strKey]);
                        AddCookie(objRequest, objCookie, objUri);
                    }
                }

                using (HttpWebResponse objResponse = (HttpWebResponse)await objRequest.GetResponseAsync())
                {
                    using (Stream objStream = objResponse.GetResponseStream())
                    {
                        using (StreamReader objReader = new StreamReader(objStream))
                        {
                            strValue = await objReader.ReadToEndAsync();
                            return strValue;
                        }
                    }
                }
            }
            catch
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Save Captcha Image
        /// </summary>
        /// <param name="x_strImageUrl"></param>
        /// <param name="x_strFilename"></param>
        /// <param name="x_strSessionId"></param>
        public async Task<bool> SaveCaptchaImageAsync(string x_strImageUrl, string x_strFilename, Dictionary<string, string> x_dicCookies)
        {
            HttpWebRequest objRequest;
            Cookie objCookie;
            Bitmap objBitmap;
            Uri objTarget;
            string strFolder;

            try
            {
                objRequest = (HttpWebRequest)WebRequest.Create(x_strImageUrl);
                objRequest.Timeout = 3 * 60 * 1000;
                // Add proxy
                if (m_objCurrentWebProxy != null)
                {
                    objRequest.Proxy = m_objCurrentWebProxy;
                }
                objRequest.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;
                // Add cookie
                if (x_dicCookies != null)
                {
                    objTarget = new Uri(x_strImageUrl);
                    foreach (string strKey in x_dicCookies.Keys)
                    {
                        objCookie = new Cookie(strKey, x_dicCookies[strKey]);
                        AddCookie(objRequest, objCookie, objTarget);
                    }
                }

                strFolder = Path.GetDirectoryName(x_strFilename);
                if (Directory.Exists(strFolder) == false)
                {
                    Directory.CreateDirectory(strFolder);
                }

                using (HttpWebResponse objResponse = (HttpWebResponse)await objRequest.GetResponseAsync())
                {
                    using (Stream objStream = objResponse.GetResponseStream())
                    {
                        using (objBitmap = new Bitmap(objStream))
                        {
                            objBitmap.SaveBitmapStream(x_strFilename);
                        }
                    }
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Post Http Async
        /// </summary>
        /// <param name="x_strUrl"></param>
        /// <param name="x_strDataJson"></param>
        /// <param name="x_strContentType"></param>
        /// <param name="x_strMethod"></param>
        /// <returns></returns>
        public async Task<string> PostHttpAsync(string x_strUrl, string x_strDataJson, string x_strContentType, Dictionary<string, string> x_dicCookies, string x_strMethod = "POST")
        {
            byte[] arrDataBytes;
            Cookie objCookie;
            HttpWebRequest objRequest;
            Uri objUri;

            try
            {
                arrDataBytes = Encoding.UTF8.GetBytes(x_strDataJson);
                objRequest = (HttpWebRequest)WebRequest.Create(x_strUrl);
                // Add proxy
                if (m_objCurrentWebProxy != null)
                {
                    objRequest.Proxy = m_objCurrentWebProxy;
                }
                objRequest.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;
                objRequest.ContentLength = arrDataBytes.Length;
                objRequest.ContentType = x_strContentType;
                objRequest.Method = x_strMethod;
                objRequest.Timeout = 3 * 60 * 1000;

                if (x_dicCookies != null)
                {
                    objUri = new Uri(x_strUrl);
                    foreach (string strKey in x_dicCookies.Keys)
                    {
                        objCookie = new Cookie(strKey, x_dicCookies[strKey]);
                        AddCookie(objRequest, objCookie, objUri);
                    }
                }
                // Write body
                using (Stream objRequestBody = objRequest.GetRequestStream())
                {
                    await objRequestBody.WriteAsync(arrDataBytes, 0, arrDataBytes.Length);
                }
                // Call Http
                using (HttpWebResponse objResponse = (HttpWebResponse)await objRequest.GetResponseAsync())
                {
                    using (Stream objStream = objResponse.GetResponseStream())
                    {
                        using (StreamReader objReader = new StreamReader(objStream))
                        {
                            return await objReader.ReadToEndAsync();
                        }
                    }
                }
            }
            catch
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Encrypt DTU Body
        /// </summary>
        /// <param name="x_strPlainText"></param>
        /// <returns></returns>
        public string EncryptDTUBody(string x_strPlainText)
        {
            byte[] arrEncrypted;
            byte[] arrPlainBytes;
            string strValue;
            ICryptoTransform objEncryptor;

            if (string.IsNullOrWhiteSpace(x_strPlainText) == true)
            {
                return string.Empty;
            }
            // Create an Aes object
            using (Aes objAes = Aes.Create())
            {
                objAes.KeySize = 128;
                objAes.Key = Encoding.UTF8.GetBytes("AMINHAKEYTEM32NYTES1234567891234");
                objAes.IV = Encoding.UTF8.GetBytes("7061737323313233");
                objAes.Mode = CipherMode.CBC;
                objAes.Padding = PaddingMode.PKCS7;

                objEncryptor = objAes.CreateEncryptor(objAes.Key, objAes.IV);

                using (MemoryStream objMemberyStream = new MemoryStream())
                {
                    using (CryptoStream objCryptoStream = new CryptoStream(objMemberyStream, objEncryptor, CryptoStreamMode.Write))
                    {
                        arrPlainBytes = Encoding.UTF8.GetBytes(x_strPlainText);
                        objCryptoStream.Write(arrPlainBytes, 0, arrPlainBytes.Length);
                    }
                    arrEncrypted = objMemberyStream.ToArray();
                }

                strValue = Convert.ToBase64String(arrEncrypted);
                return strValue;
            }
        }

        /// <summary>
        /// Reconhecer Captcha
        /// </summary>
        /// <param name="x_objCaptchaImage"></param>
        /// <returns></returns>
        public string ReconhecerCaptcha(Image x_objCaptchaImage)
        {
            Image objImage;
            Bitmap bmImage;
            Invert objInvert;
            ColorFiltering objColorFiltering;
            Opening objOpen;
            BlobsFiltering objBlobs;
            Closing objClose;
            GaussianSharpen objGaussianSharpen;
            ContrastCorrection objContrastCorrection;
            FiltersSequence objFiltersSequence;
            string strReconhecido;

            bmImage = new Bitmap(x_objCaptchaImage);
            bmImage = bmImage.Clone(new Rectangle(0, 0, x_objCaptchaImage.Width, x_objCaptchaImage.Height), System.Drawing.Imaging.PixelFormat.Format24bppRgb);
            objInvert = new Invert();
            objColorFiltering = new ColorFiltering();
            objColorFiltering.Blue = new AForge.IntRange(200, 255);
            objColorFiltering.Red = new AForge.IntRange(200, 255);
            objColorFiltering.Green = new AForge.IntRange(200, 255);
            objOpen = new Opening();
            objBlobs = new BlobsFiltering();
            objClose = new Closing();
            objGaussianSharpen = new GaussianSharpen();
            objContrastCorrection = new ContrastCorrection();
            objBlobs.MinHeight = 10;
            objFiltersSequence = new FiltersSequence(objGaussianSharpen, objInvert, objOpen, objInvert, objBlobs, objInvert, objOpen, objContrastCorrection, objColorFiltering, objBlobs, objInvert);
            objImage = objFiltersSequence.Apply(bmImage);
            strReconhecido = OCR((Bitmap)objImage);
            return strReconhecido;
        }

        /// <summary>
        /// OCR
        /// </summary>
        /// <param name="x_bmImage"></param>
        /// <returns></returns>
        private string OCR(Bitmap x_bmImage)
        {
            string strResult;
            Pix objPix;

            using (TesseractEngine objEngine = new TesseractEngine(m_strTesseractFolder, "eng", EngineMode.Default))
            {
                objEngine.SetVariable("tessedit_char_whitelist", "1234567890");
                objEngine.SetVariable("tessedit_unrej_any_wd", true);

                objPix = m_objBitmapToPixConverter.Convert(x_bmImage);
                using (Page objPage = objEngine.Process(objPix, PageSegMode.SingleLine))
                {
                    strResult = objPage.GetText();
                }    
            }
            return strResult.Replace(" ", "").Trim();
        }
        #endregion
    }

    public class BitmapToPixConverter
    {
        /// <summary>
        /// Converts the specified <paramref name="x_bmImage"/> to a <see cref="Pix"/>.
        /// </summary>
        /// <param name="x_bmImage">The source image to be converted.</param>
        /// <returns>The converted pix.</returns>
        public Pix Convert(Bitmap x_bmImage)
        {
            int nPixDepth;
            Pix nPix;

            nPixDepth = GetPixDepth(x_bmImage.PixelFormat);
            nPix = Pix.Create(x_bmImage.Width, x_bmImage.Height, nPixDepth);
            nPix.XRes = (int)Math.Round(x_bmImage.HorizontalResolution);
            nPix.YRes = (int)Math.Round(x_bmImage.VerticalResolution);

            BitmapData bmImgData = null;
            PixData objPixData = null;
            try
            {
                // TODO: Set X and Y resolution

                if ((x_bmImage.PixelFormat & PixelFormat.Indexed) == PixelFormat.Indexed)
                {
                    CopyColormap(x_bmImage, nPix);
                }

                // transfer data
                bmImgData = x_bmImage.LockBits(new Rectangle(0, 0, x_bmImage.Width, x_bmImage.Height), ImageLockMode.ReadOnly, x_bmImage.PixelFormat);
                objPixData = nPix.GetData();

                if (bmImgData.PixelFormat == PixelFormat.Format32bppArgb)
                {
                    TransferDataFormat32bppArgb(bmImgData, objPixData);
                }
                else if (bmImgData.PixelFormat == PixelFormat.Format32bppRgb)
                {
                    TransferDataFormat32bppRgb(bmImgData, objPixData);
                }
                else if (bmImgData.PixelFormat == PixelFormat.Format24bppRgb)
                {
                    TransferDataFormat24bppRgb(bmImgData, objPixData);
                }
                else if (bmImgData.PixelFormat == PixelFormat.Format8bppIndexed)
                {
                    TransferDataFormat8bppIndexed(bmImgData, objPixData);
                }
                else if (bmImgData.PixelFormat == PixelFormat.Format1bppIndexed)
                {
                    TransferDataFormat1bppIndexed(bmImgData, objPixData);
                }
                return nPix;
            }
            catch (Exception)
            {
                nPix.Dispose();
                throw;
            }
            finally
            {
                if (bmImgData != null)
                {
                    x_bmImage.UnlockBits(bmImgData);
                }
            }
        }

        /// <summary>
        /// Copy Colormap
        /// </summary>
        /// <param name="x_bmImage"></param>
        /// <param name="x_objPix"></param>
        /// <exception cref="InvalidOperationException"></exception>
        private void CopyColormap(Bitmap x_bmImage, Pix x_objPix)
        {
            ColorPalette objImgPalette;
            Color[] arrImgPaletteEntries;
            PixColormap objPixColormap;

            objImgPalette = x_bmImage.Palette;
            arrImgPaletteEntries = objImgPalette.Entries;
            objPixColormap = PixColormap.Create(x_objPix.Depth);
            try
            {
                for (int i = 0; i < arrImgPaletteEntries.Length; i++)
                {
                    if (!objPixColormap.AddColor(ToPixColor(arrImgPaletteEntries[i])))
                    {
                        throw new InvalidOperationException(String.Format("Failed to add colormap entry {0}.", i));
                    }
                }
                x_objPix.Colormap = objPixColormap;
            }
            catch (Exception)
            {
                objPixColormap.Dispose();
                throw;
            }
        }

        /// <summary>
        /// Get Pix Depth
        /// </summary>
        /// <param name="x_ePixelFormat"></param>
        /// <returns></returns>
        /// <exception cref="InvalidOperationException"></exception>
        private int GetPixDepth(PixelFormat x_ePixelFormat)
        {
            switch (x_ePixelFormat)
            {
                case PixelFormat.Format1bppIndexed:
                    return 1;
                case PixelFormat.Format8bppIndexed:
                    return 8;
                case PixelFormat.Format32bppArgb:
                case PixelFormat.Format32bppRgb:
                case PixelFormat.Format24bppRgb:
                    return 32;
                default:
                    throw new InvalidOperationException(String.Format("Source bitmap's pixel format {0} is not supported.", x_ePixelFormat));
            }
        }

        /// <summary>
        /// Transfer Data Format1bpp Indexed
        /// </summary>
        /// <param name="x_bmImgData"></param>
        /// <param name="x_objPixData"></param>
        private unsafe void TransferDataFormat1bppIndexed(BitmapData x_bmImgData, PixData x_objPixData)
        {
            int nHeight;
            int nWidth;
            byte bytePixelVal;
            byte* objImgLine;
            uint* objPixLine;

            nHeight = x_bmImgData.Height;
            nWidth = x_bmImgData.Width / 8;
            for (int nY = 0; nY < nHeight; nY++)
            {
                objImgLine = (byte*)x_bmImgData.Scan0 + (nY * x_bmImgData.Stride);
                objPixLine = (uint*)x_objPixData.Data + (nY * x_objPixData.WordsPerLine);

                for (int nX = 0; nX < nWidth; nX++)
                {
                    bytePixelVal = BitmapHelper.GetDataByte(objImgLine, nX);
                    PixData.SetDataByte(objPixLine, nX, bytePixelVal);
                }
            }
        }

        /// <summary>
        /// Transfer Data Format 24 bpp Rgb
        /// </summary>
        /// <param name="x_bmImage"></param>
        /// <param name="x_objPixData"></param>
        private unsafe void TransferDataFormat24bppRgb(BitmapData x_bmImage, PixData x_objPixData)
        {
            PixelFormat eImgFormat;
            int nHeight;
            int nWidth;
            byte* objImgLine;
            byte* objPixelPtr;
            byte byteBlue;
            byte byteGreen;
            byte byteRed;
            uint* objPixLine;

            eImgFormat = x_bmImage.PixelFormat;
            nHeight = x_bmImage.Height;
            nWidth = x_bmImage.Width;
            for (int y = 0; y < nHeight; y++)
            {
                objImgLine = (byte*)x_bmImage.Scan0 + (y * x_bmImage.Stride);
                objPixLine = (uint*)x_objPixData.Data + (y * x_objPixData.WordsPerLine);

                for (int nX = 0; nX < nWidth; nX++)
                {
                    objPixelPtr = objImgLine + nX * 3;
                    byteBlue = objPixelPtr[0];
                    byteGreen = objPixelPtr[1];
                    byteRed = objPixelPtr[2];
                    PixData.SetDataFourByte(objPixLine, nX, BitmapHelper.EncodeAsRGBA(byteRed, byteGreen, byteBlue, 255));
                }
            }
        }

        /// <summary>
        /// Transfer Data Format 32bpp Rgb
        /// </summary>
        /// <param name="x_bmImgData"></param>
        /// <param name="x_objPixData"></param>
        private unsafe void TransferDataFormat32bppRgb(BitmapData x_bmImgData, PixData x_objPixData)
        {
            PixelFormat eImgFormat;
            int nHeight;
            int nWidth;
            byte byteBlue;
            byte byteGreen;
            byte byteRed;
            byte* objImgLine;
            byte* objPixelPtr;
            uint* objPixLine;

            eImgFormat = x_bmImgData.PixelFormat;
            nHeight = x_bmImgData.Height;
            nWidth = x_bmImgData.Width;

            for (int nY = 0; nY < nHeight; nY++)
            {
                objImgLine = (byte*)x_bmImgData.Scan0 + (nY * x_bmImgData.Stride);
                objPixLine = (uint*)x_objPixData.Data + (nY * x_objPixData.WordsPerLine);

                for (int nX = 0; nX < nWidth; nX++)
                {
                    objPixelPtr = objImgLine + (nX << 2);
                    byteBlue = *objPixelPtr;
                    byteGreen = *(objPixelPtr + 1);
                    byteRed = *(objPixelPtr + 2);
                    PixData.SetDataFourByte(objPixLine, nX, BitmapHelper.EncodeAsRGBA(byteRed, byteGreen, byteBlue, 255));
                }
            }
        }

        /// <summary>
        /// Transfer Data Format 32bpp Argb
        /// </summary>
        /// <param name="x_bmImgData"></param>
        /// <param name="x_objPixData"></param>
        private unsafe void TransferDataFormat32bppArgb(BitmapData x_bmImgData, PixData x_objPixData)
        {
            PixelFormat eImgFormat;
            int nHeight;
            int nWidth;
            byte byteBlue;
            byte byteGreen;
            byte byteRed;
            byte byteAlpha;
            byte* objPixelPtr;
            byte* objImgLine;
            uint* objPixLine;

            eImgFormat = x_bmImgData.PixelFormat;
            nHeight = x_bmImgData.Height;
            nWidth = x_bmImgData.Width;

            for (int y = 0; y < nHeight; y++)
            {
                objImgLine = (byte*)x_bmImgData.Scan0 + (y * x_bmImgData.Stride);
                objPixLine = (uint*)x_objPixData.Data + (y * x_objPixData.WordsPerLine);

                for (int x = 0; x < nWidth; x++)
                {
                    objPixelPtr = objImgLine + (x << 2);
                    byteBlue = *objPixelPtr;
                    byteGreen = *(objPixelPtr + 1);
                    byteRed = *(objPixelPtr + 2);
                    byteAlpha = *(objPixelPtr + 3);
                    PixData.SetDataFourByte(objPixLine, x, BitmapHelper.EncodeAsRGBA(byteRed, byteGreen, byteBlue, byteAlpha));
                }
            }
        }

        /// <summary>
        /// Transfer Data Format 8bpp Indexed
        /// </summary>
        /// <param name="x_bmImgData"></param>
        /// <param name="x_objPixData"></param>
        private unsafe void TransferDataFormat8bppIndexed(BitmapData x_bmImgData, PixData x_objPixData)
        {
            int nHeight;
            int nWidth;
            byte bytePixelVal;
            byte* objImgLine;
            uint* objPixLine;

            nHeight = x_bmImgData.Height;
            nWidth = x_bmImgData.Width;
            for (int y = 0; y < nHeight; y++)
            {
                objImgLine = (byte*)x_bmImgData.Scan0 + (y * x_bmImgData.Stride);
                objPixLine = (uint*)x_objPixData.Data + (y * x_objPixData.WordsPerLine);

                for (int x = 0; x < nWidth; x++)
                {
                    bytePixelVal = *(objImgLine + x);
                    PixData.SetDataByte(objPixLine, x, bytePixelVal);
                }
            }
        }

        /// <summary>
        /// To Pix Color
        /// </summary>
        /// <param name="x_objColor"></param>
        /// <returns></returns>
        public PixColor ToPixColor(Color x_objColor)
        {
            return new PixColor(x_objColor.R, x_objColor.G, x_objColor.B, x_objColor.A);
        }
    }
}
