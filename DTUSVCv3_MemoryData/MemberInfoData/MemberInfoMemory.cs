﻿using DTUSVCv3_EntityFramework;
using DTUSVCv3_Library;
using System.Drawing;
using System.Text;

namespace DTUSVCv3_MemoryData
{
    public class MemberInfoMemory
    {
        #region Constructor
        public MemberInfoMemory()
        {
            m_objMemberDbManager = new MemberDbManager();
            m_objAddressDbManager = new AddressDbManager();
            m_objFacultyDbManager = new FacultyDbManager();
            m_lstMemberInfo = new List<MemberDecryptModel>();
        }
        #endregion

        #region Fields
        private readonly MemberDbManager m_objMemberDbManager;
        private readonly AddressDbManager m_objAddressDbManager;
        private readonly FacultyDbManager m_objFacultyDbManager;
        private List<MemberDecryptModel> m_lstMemberInfo;

        private static MemberInfoMemory m_objMemberInfoMemory;
        #endregion

        #region Properties
        public static MemberInfoMemory Instant
        {
            get
            {
                if (m_objMemberInfoMemory == null)
                {
                    m_objMemberInfoMemory = new MemberInfoMemory();
                }
                return m_objMemberInfoMemory;
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Load Data Base
        /// </summary>
        public void LoadDataBase()
        {
            List<MemberInfo> lstMember;
            Task objTask;
            DateTime objCurrentDate;
            int nCount;

            objTask = Task.Run(async () =>
            {
                MemberDecryptModel objMemberDecryptModel;

                lstMember = await m_objMemberDbManager.GetAllMember();
                if (lstMember == null)
                {
                    return;
                }

                m_lstMemberInfo.Clear();
                Console.OutputEncoding = Encoding.UTF8;
                nCount = 0;
                foreach (MemberInfo objMemberItem in lstMember)
                {
                    objMemberDecryptModel = await MemberDbToMemberModel(objMemberItem);
                    nCount++;
                    objCurrentDate = DateTimeHelper.GetCurrentDateTimeVN();
                    if (objMemberDecryptModel == null)
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine($"{nCount} - {objCurrentDate.ToString("dd/MM/yyyy HH:mm:ss.fff")}: Decript member data failed!");
                        Console.ForegroundColor = ConsoleColor.White;
                        continue;
                    }
                    m_lstMemberInfo.Add(objMemberDecryptModel);
                    Console.WriteLine($"{nCount} - {objCurrentDate.ToString("dd/MM/yyyy HH:mm:ss.fff")}: Decript member data - {objMemberDecryptModel.GetMemberFullName()}");
                }
            });
            objTask.Wait();
        }

        /// <summary>
        /// Get All Member List
        /// </summary>
        /// <param name="x_eGetMemberType"></param>
        /// <returns></returns>
        public List<MemberDecryptModel> GetAllMemberList(GetMemberType x_eGetMemberType)
        {
            List<MemberDecryptModel> lstMember;

            if (m_lstMemberInfo == null)
            {
                return null;
            }

            lstMember = m_lstMemberInfo.DeepClone();
            if (lstMember == null)
            {
                return null;
            }

            if (x_eGetMemberType == GetMemberType.IS_MEMBER)
            {
                lstMember = lstMember.FindAll(obj => obj.IsMember == true);
            }
            else if (x_eGetMemberType == GetMemberType.MEMBER_LEAVES)
            {
                lstMember = lstMember.FindAll(obj => obj.IsMember == false);
            }

            return lstMember;
        }

        /// <summary>
        /// Update Faculty
        /// </summary>
        /// <param name="x_objFacultys"></param>
        public void UpdateFaculty(Facultys x_objFacultys)
        {
            if (m_lstMemberInfo == null)
            {
                return;
            }

            foreach (MemberDecryptModel objMember in m_lstMemberInfo)
            {
                if (objMember.Faculty.Id.Equals(x_objFacultys.Id) == true)
                {
                    objMember.Faculty.FacultyName = x_objFacultys.FacultyName;
                }
            }
        }

        /// <summary>
        /// Member Db To Member Model
        /// </summary>
        private async Task<MemberDecryptModel> MemberDbToMemberModel(MemberInfo x_objMemberInfo)
        {
            MemberDecryptModel objMemberDecryptModel;
            MemberInfo objMemberInfoDecrypt;
            AddressInfo objAddress;
            Facultys objFaculty;
            AddressInfoModel objAddressInfoModel;

            string strMemberDecryptKey;
            string strAvatarPath;

            try
            {
                if (x_objMemberInfo == null)
                {
                    return null;
                }

                strMemberDecryptKey = HashKeyManager.GetMemberHashKey(x_objMemberInfo.Id);
                if (string.IsNullOrWhiteSpace(strMemberDecryptKey) == true)
                {
                    return null;
                }

                // Get address
                objAddress = await m_objAddressDbManager.GetAddressInfoById(x_objMemberInfo.HometownId);
                if (objAddress == null)
                {
                    return null;
                }
                // Get Faculty
                objFaculty = await m_objFacultyDbManager.GetFacultyById(x_objMemberInfo.FacultyId);
                if (objFaculty == null)
                {
                    return null;
                }
                // Decrypt data
                objAddress = objAddress.ObjDecript(strMemberDecryptKey);
                objMemberInfoDecrypt = x_objMemberInfo.ObjDecript(strMemberDecryptKey);
                // convert model
                objAddressInfoModel = await AddressInfoDbToModel(objAddress);
                if (objAddressInfoModel == null)
                {
                    return null;
                }

                // Check avatar path
                strAvatarPath = GetAvatarFilePath(objMemberInfoDecrypt.AvatarPath);

                objMemberDecryptModel = new MemberDecryptModel();
                objMemberDecryptModel.AvatarPath = strAvatarPath;
                objMemberDecryptModel.BirthDay = objMemberInfoDecrypt.BirthDay;
                objMemberDecryptModel.ClassName = objMemberInfoDecrypt.ClassName;
                objMemberDecryptModel.Email = objMemberInfoDecrypt.Email;
                objMemberDecryptModel.Faculty = objFaculty;
                objMemberDecryptModel.FirstName = objMemberInfoDecrypt.FirstName;
                objMemberDecryptModel.Hometown = objAddressInfoModel;
                objMemberDecryptModel.Id = objMemberInfoDecrypt.Id;
                objMemberDecryptModel.IsMember = objMemberInfoDecrypt.IsMember;
                objMemberDecryptModel.JoinDate = objMemberInfoDecrypt.JoinDate;
                objMemberDecryptModel.LastName = objMemberInfoDecrypt.LastName;
                objMemberDecryptModel.LinkFacebook = objMemberInfoDecrypt.LinkFacebook;
                objMemberDecryptModel.PhoneNumber = objMemberInfoDecrypt.PhoneNumber;
                objMemberDecryptModel.Sex = objMemberInfoDecrypt.Sex;
                objMemberDecryptModel.StudentId = objMemberInfoDecrypt.StudentId;
                return objMemberDecryptModel;
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Member Model To Member Db
        /// </summary>
        private MemberInfo MemberModelToMemberDb(MemberDecryptModel x_objMemberInfo)
        {
            MemberInfo objMemberInfo;
            string strAvatarFileName;
            try
            {
                if (x_objMemberInfo == null)
                {
                    return null;
                }

                strAvatarFileName = Path.GetFileName(x_objMemberInfo.AvatarPath);
                if (strAvatarFileName.EqualsIgnoreCase(SysFile.LOGO_FILE_NAME) == true)
                {
                    strAvatarFileName = string.Empty;
                }
                objMemberInfo = new MemberInfo();
                objMemberInfo.AvatarPath = strAvatarFileName;
                objMemberInfo.BirthDay = x_objMemberInfo.BirthDay;
                objMemberInfo.ClassName = x_objMemberInfo.ClassName;
                objMemberInfo.Email = x_objMemberInfo.Email;
                objMemberInfo.FacultyId = x_objMemberInfo.Faculty.Id;
                objMemberInfo.Id = x_objMemberInfo.Id;
                objMemberInfo.FirstName = x_objMemberInfo.FirstName;
                objMemberInfo.HometownId = x_objMemberInfo.Hometown.Id;
                objMemberInfo.IsMember = x_objMemberInfo.IsMember;
                objMemberInfo.JoinDate = x_objMemberInfo.JoinDate;
                objMemberInfo.LastName = x_objMemberInfo.LastName;
                objMemberInfo.LinkFacebook = x_objMemberInfo.LinkFacebook;
                objMemberInfo.PhoneNumber = x_objMemberInfo.PhoneNumber;
                objMemberInfo.Sex = x_objMemberInfo.Sex;
                objMemberInfo.StudentId = x_objMemberInfo.StudentId;
                return objMemberInfo;
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Update Member Info
        /// </summary>
        /// <param name="x_objData"></param>
        /// <returns></returns>
        public async Task<bool> UpdateMemberInfo(MemberDecryptModel x_objData)
        {
            MemberInfo objMemberInfo;
            MemberDecryptModel objBackupMemberInfo;
            Facultys objFaculty;
            AddressInfo objAddress;
            Bitmap objQrCode;
            string strFilePath;
            string strFileName;
            string strMemberHashKey;
            string strOldMemberHashKey;
            string strAvatarPath;
            bool bUpdateSuccsess;

            if (x_objData == null)
            {
                return false;
            }

            objBackupMemberInfo = null;
            // Update
            if (x_objData.Id.MongoIdIsValid() == true)
            {
                // Create backup
                objBackupMemberInfo = m_lstMemberInfo.Find(obj => obj.Id.Equals(x_objData.Id) == true);
                // Delete old info
                m_lstMemberInfo.RemoveAll(obj => obj.Id.Equals(x_objData.Id) == true);
                // Convert data model to databse model
                objMemberInfo = MemberModelToMemberDb(x_objData);

                if (objMemberInfo == null)
                {
                    if (objBackupMemberInfo != null)
                    {
                        m_lstMemberInfo.Add(objBackupMemberInfo);
                    }
                    return false;
                }

                // Get new hash key
                strOldMemberHashKey = HashKeyManager.GetMemberHashKey(objMemberInfo.Id);
                strMemberHashKey = HashKeyManager.GetNewMemberHashKey(objMemberInfo.Id);

                // Encrypt member info
                objMemberInfo = objMemberInfo.ObjEncript(strMemberHashKey);
                // Update to data base
                bUpdateSuccsess = await m_objMemberDbManager.UpdateMember(objMemberInfo);
                if (bUpdateSuccsess == false)
                {
                    if (objBackupMemberInfo != null)
                    {
                        HashKeyManager.SetMemberHashKey(objMemberInfo.Id, strOldMemberHashKey);
                        m_lstMemberInfo.Add(objBackupMemberInfo);
                    }
                    return false;
                }
            }
            // add new mamber
            else
            {
                // Convert data model to databse model
                objMemberInfo = MemberModelToMemberDb(x_objData);
                if (objMemberInfo == null)
                {
                    return false;
                }

                objMemberInfo.Id = string.Empty;
                // Add member to data base
                bUpdateSuccsess = await m_objMemberDbManager.UpdateMember(objMemberInfo);
                if (bUpdateSuccsess == false)
                {
                    return false;
                }
                // Get member hash key
                strMemberHashKey = HashKeyManager.GetNewMemberHashKey(objMemberInfo.Id);
                // Encrypt member info
                objMemberInfo = objMemberInfo.ObjEncript(strMemberHashKey);
                // Update member encrypt to data base
                bUpdateSuccsess = await m_objMemberDbManager.UpdateMember(objMemberInfo);
                if (bUpdateSuccsess == false)
                {
                    return false;
                }
            }

            objAddress = new AddressInfo
            {
                Id = x_objData.Hometown.Id,
                SpecificAddress = x_objData.Hometown.SpecificAddress,
                DistrictId = x_objData.Hometown.District.DistrictId,
                ProvinceId = x_objData.Hometown.Province.ProvinceId,
                WardId = x_objData.Hometown.Ward.WardId
            };
            objAddress = objAddress.ObjEncript(strMemberHashKey);
            await m_objAddressDbManager.UpdateAddress(objAddress);
            objFaculty = await m_objFacultyDbManager.GetFacultyById(objMemberInfo.FacultyId);
            x_objData.Faculty = objFaculty;
            strAvatarPath = GetAvatarFilePath(Path.GetFileName(x_objData.AvatarPath));
            x_objData.AvatarPath = strAvatarPath;
            m_lstMemberInfo.Add(x_objData);
            // Copy Qr-Code
            strFileName = string.Format(SysFile.QR_CODE_NAME_FORMAT, x_objData.StudentId);
            strFilePath = FileHelper.GetFilePath(SysFolder.QR_CODE_FOLDER, strFileName);
            objQrCode = QRCodeHelper.CreateQrCode(objMemberInfo.Id);
            objQrCode.SaveBitmapStream(strFilePath);
            return true;
        }

        /// <summary>
        /// Get Member By Id
        /// </summary>
        public MemberDecryptModel GetMemberById(string x_strId, FindType x_eFindType = FindType.EQUALS)
        {
            MemberDecryptModel objMemberDecryptModel;

            if ((m_lstMemberInfo == null) || (m_lstMemberInfo.Count == 0))
            {
                return null;
            }

            if (x_strId.MongoIdIsValid() == true)
            {
                objMemberDecryptModel = m_lstMemberInfo.Find(obj => obj.Id.Equals(x_strId) == true);
            }
            else
            {
                if (x_eFindType == FindType.EQUALS)
                {
                    objMemberDecryptModel = m_lstMemberInfo.Find(obj => obj.StudentId.EqualsIgnoreCase(x_strId) == true);
                }
                else
                {
                    objMemberDecryptModel = m_lstMemberInfo.Find(obj => obj.StudentId.ContainsIgnoreCase(x_strId) == true);
                }
            }

            return objMemberDecryptModel.DeepClone();
        }

        /// <summary>
        /// Get Member By Id
        /// </summary>
        public MemberDecryptModel GetMemberExistById(string x_strId, bool x_bOnlyMember = false)
        {
            MemberDecryptModel objMemberDecryptModel;

            if ((m_lstMemberInfo == null) || (m_lstMemberInfo.Count == 0))
            {
                return null;
            }

            if (x_strId.MongoIdIsValid() == true)
            {
                if (x_bOnlyMember == false)
                {
                    objMemberDecryptModel = m_lstMemberInfo.Find(obj => obj.Id.Equals(x_strId) == true);
                }
                else
                {
                    objMemberDecryptModel = m_lstMemberInfo.Find(obj => obj.Id.Equals(x_strId) == true && obj.IsMember == true);
                }
            }
            else
            {
                objMemberDecryptModel = m_lstMemberInfo.Find(obj => obj.StudentId.EqualsIgnoreCase(x_strId) == true && obj.IsMember == true);
            }

            return objMemberDecryptModel.DeepClone();
        }

        /// <summary>
        /// Check Email Exist
        /// </summary>
        /// <param name="x_strEmail"></param>
        /// <returns></returns>
        public bool CheckEmailExist(string x_strEmail)
        {
            bool bResult;

            if ((m_lstMemberInfo == null) || (m_lstMemberInfo.Count == 0))
            {
                return false;
            }
            bResult = m_lstMemberInfo.Any(obj => obj.Email.EqualsIgnoreCase(x_strEmail) && (obj.IsMember == true));
            return bResult;
        }

        /// <summary>
        /// Check Student Exist
        /// </summary>
        /// <param name="x_strStudent"></param>
        /// <returns></returns>
        public bool CheckStudentIdExist(string x_strStudent, string x_strMemberId = null)
        {
            bool bResult;
            MemberDecryptModel objMember;

            if ((m_lstMemberInfo == null) || (m_lstMemberInfo.Count == 0))
            {
                return false;
            }
            if (x_strMemberId.MongoIdIsValid() == true)
            {
                objMember = m_lstMemberInfo.Find(obj => obj.StudentId.EqualsIgnoreCase(x_strStudent) && (obj.IsMember == true));
                if ((objMember != null) && (objMember.StudentId.Equals(x_strStudent) == true))
                {
                    bResult = true;
                }
                else
                {
                    bResult = false;
                }
            }
            else
            {
                bResult = m_lstMemberInfo.Any(obj => obj.StudentId.EqualsIgnoreCase(x_strStudent) && (obj.IsMember == true));
            }
            return bResult;
        }

        /// <summary>
        /// Check Is Member
        /// </summary>
        /// <param name="x_strId"></param>
        /// <returns></returns>
        public bool CheckIsMember(string x_strId)
        {
            MemberDecryptModel objMemberDecryptModel;

            objMemberDecryptModel = GetMemberById(x_strId);
            if (objMemberDecryptModel == null)
            {
                return false;
            }

            return objMemberDecryptModel.IsMember;
        }

        /// <summary>
        /// Get Avatar File Path
        /// </summary>
        private string GetAvatarFilePath(string x_strAvatarPath)
        {
            string strAvatarName;
            string strAvatarPath;
            string strAvatarFullPath;

            if (string.IsNullOrWhiteSpace(x_strAvatarPath) == true)
            {
                strAvatarName = SysFile.LOGO_FILE_NAME;
                strAvatarPath = Path.Combine(SysFolder.LOGO_FOLDER, strAvatarName);
                return strAvatarPath;
            }
            else
            {
                strAvatarName = x_strAvatarPath;
                strAvatarPath = Path.Combine(SysFolder.AVATAR_FOLDER, strAvatarName);
                strAvatarFullPath = Path.Combine(SysFolder.WWWROOT_PATH, strAvatarPath);
                if (File.Exists(strAvatarFullPath) == true)
                {
                    return strAvatarPath;
                }
                else
                {
                    strAvatarName = SysFile.LOGO_FILE_NAME;
                    strAvatarPath = Path.Combine(SysFolder.LOGO_FOLDER, strAvatarName);
                    return strAvatarPath;
                }
            }
        }

        /// <summary>
        /// Address Info Db To Model
        /// </summary>
        private async Task<AddressInfoModel> AddressInfoDbToModel(AddressInfo x_objAddressInfo)
        {
            AddressInfoModel objAddressInfoModel;

            try
            {
                if (x_objAddressInfo == null)
                {
                    return null;
                }

                objAddressInfoModel = new AddressInfoModel();
                objAddressInfoModel.Id = x_objAddressInfo.Id;
                objAddressInfoModel.Province = await m_objAddressDbManager.GetProvinceById(x_objAddressInfo.ProvinceId);
                objAddressInfoModel.District = await m_objAddressDbManager.GetDistrictById(x_objAddressInfo.DistrictId);
                objAddressInfoModel.Ward = await m_objAddressDbManager.GetWardById(x_objAddressInfo.WardId);
                objAddressInfoModel.SpecificAddress = x_objAddressInfo.SpecificAddress;
                return objAddressInfoModel;
            }
            catch
            {
                return null;
            }
        }
        #endregion
    }
}
