﻿using DTUSVCv3_EntityFramework;

namespace DTUSVCv3_MemoryData
{
    [Serializable]
    public class MemberDecryptModel
    {
        public string Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string BirthDay { get; set; }
        public string Sex { get; set; }
        public AddressInfoModel Hometown { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public string LinkFacebook { get; set; }
        public Facultys Faculty { get; set; }
        public string ClassName { get; set; }
        public string StudentId { get; set; }
        public string AvatarPath { get; set; }
        public DateTime JoinDate { get; set; }
        public bool IsMember { get; set; }

        /// <summary>
        /// Get Member Full Name
        /// </summary>
        /// <returns></returns>
        public string GetMemberFullName()
        {
            return string.Format("{0} {1}", FirstName, LastName);
        }
    }
}
