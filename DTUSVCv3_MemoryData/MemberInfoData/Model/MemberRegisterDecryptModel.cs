﻿using DTUSVCv3_EntityFramework;
using DTUSVCv3_Library;

namespace DTUSVCv3_MemoryData
{
    [Serializable]
    public class MemberRegisterDecryptModel
    {
        public MemberRegisterDecryptModel()
        {
            RegisDate = DateTimeHelper.GetCurrentDateTimeVN();
        }
        public string Id { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string BirthDay { get; set; }
        public string Sex { get; set; }
        public AddressInfoModel Hometown { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public string LinkFacebook { get; set; }
        public Facultys Faculty { get; set; }
        public string ClassName { get; set; }
        public string StudentId { get; set; }
        public DateTime RegisDate { get; set; }
        public string Reason { get; set; }
        public string RecruitId { get; set; }
        public InterviewDecryptInfo InterviewInfo { get; set; }
        public ApprovalDecryptInfo ApprovalInfo { get; set; }
        public bool FailedInterView { get; set; }

        /// <summary>
        /// Get Member Full Name
        /// </summary>
        /// <returns></returns>
        public string GetMemberFullName()
        {
            return string.Format("{0} {1}", FirstName, LastName);
        }
    }

    [Serializable]
    public class InterviewDecryptInfo
    {
        public InterviewDecryptInfo()
        {
            Reviewer = null;
            ReviewContent = null;
            Note = null;
            Scores = 0;
            EmailSent = false;
            Interviewed = false;
            InterviewAppointmentTime = DateTime.MinValue;
            ReviewDate = DateTime.MinValue;
        }
        public MemberDecryptModel Reviewer { get; set; }
        public string ReviewContent { get; set; }
        public string Note { get; set; }
        public float Scores { get; set; }
        public bool EmailSent { get; set; }
        public bool Interviewed { get; set; }
        public DateTime InterviewAppointmentTime { get; set; }
        public DateTime ReviewDate { get; set; }
    }

    [Serializable]
    public class ApprovalDecryptInfo
    {
        public MemberDecryptModel Approver { get; set; }
        public DateTime ApprovalDate { get; set; }
    }

    [Serializable]
    public class InterviewRoomReponse
    {
        public InterviewRoomReponse()
        {
            Managers = new List<ManagerRoom>();
        }
        public string Id { get; set; }
        public string RecruitId { get; set; }
        public string RoomAddress { get; set; }
        public string Note { get; set; }
        public DateTime StartDateTime { get; set; }
        public DateTime EndDateTime { get; set; }
        public List<BreakTime> BreakTimes { get; set; }
        public List<ManagerRoom> Managers { get; set; }
        public List<MemberInRoomModel> Members { get; set; }
        public int RatedTime { get; set; }
        public string CreaterName { get; set; }
        public bool EmailSending { get; set; }
        public DateTime CreateDate { get; set; }
    }

    [Serializable]
    public class ManagerRoom
    {
        public string Id { get; set; }
        public string FullName { get; set; }
        public string AvatarPath { get; set; }
        public string StudentId { get; set; }
    }

    [Serializable]
    public class MemberInRoomModel
    {
        public string Id { get; set; }
        public MemberRegisterDecryptModel MemberInfo { get; set; }
        public ManagerRoom Creater { get; set; }
        public int SendEmailState { get; set; }
        public DateTime InterviewTime { get; set; }
        public DateTime CreateDate { get; set; }
        public bool IsInterviewed { get; set; }
        public bool IsChangeSchedule { get; set; }
    }
}
