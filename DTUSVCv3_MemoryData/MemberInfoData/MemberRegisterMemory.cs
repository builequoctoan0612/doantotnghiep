﻿using DTUSVCv3_EntityFramework;
using DTUSVCv3_Library;
using System.Collections.Generic;
using System.Text;

namespace DTUSVCv3_MemoryData
{
    public class MemberRegisterMemory
    {
        #region Constructor
        public MemberRegisterMemory()
        {
            m_objRecruitmentDbManager = new RecruitmentDbManager();
            m_objAddressDbManager = new AddressDbManager();
            m_objFacultyDbManager = new FacultyDbManager();
            m_objAccountDbManager = new AccountDbManager();
            m_objRoleDbManager = new RolesDbManager();
            m_dicInterviewRoom = new Dictionary<string, List<InterviewRoomReponse>>();
        }
        #endregion

        #region Fields
        private readonly RecruitmentDbManager m_objRecruitmentDbManager;
        private readonly AddressDbManager m_objAddressDbManager;
        private readonly AccountDbManager m_objAccountDbManager;
        private readonly FacultyDbManager m_objFacultyDbManager;
        private readonly RolesDbManager m_objRoleDbManager;
        private List<MemberRegisterDecryptModel> m_lstMemberRegisterData;
        private Dictionary<string, List<InterviewRoomReponse>> m_dicInterviewRoom;
        private static MemberRegisterMemory m_objMemberRegisterData;
        #endregion

        #region Properties
        /// <summary>
        /// Member Register Data
        /// </summary>
        public static MemberRegisterMemory Instant
        {
            get
            {
                if (m_objMemberRegisterData == null)
                {
                    m_objMemberRegisterData = new MemberRegisterMemory();
                }

                return m_objMemberRegisterData;
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Load All Member
        /// </summary>
        public void LoadAllMember()
        {
            Task objTask;
            List<MembershipRegistration> lstMemberRegisterDb;
            MemberRegisterDecryptModel objMemberRegisterDecrypt;
            DateTime objCurrentDate;
            int nCount;

            objTask = Task.Run(async () =>
            {
                try
                {
                    lstMemberRegisterDb = await m_objRecruitmentDbManager.GetAllMembers();
                    if ((lstMemberRegisterDb == null) || (lstMemberRegisterDb.Count == 0))
                    {
                        return;
                    }

                    // Create memory data
                    m_lstMemberRegisterData = new List<MemberRegisterDecryptModel>();
                    Console.OutputEncoding = Encoding.UTF8;
                    nCount = 0;
                    foreach (MembershipRegistration objTemp in lstMemberRegisterDb)
                    {
                        objCurrentDate = DateTimeHelper.GetCurrentDateTimeVN();
                        nCount++;
                        // Convert data
                        objMemberRegisterDecrypt = await ConvertDbToModel(objTemp);
                        if (objMemberRegisterDecrypt == null)
                        {
                            Console.ForegroundColor = ConsoleColor.Red;
                            Console.WriteLine($"{nCount} - {objCurrentDate.ToString("dd/MM/yyyy HH:mm:ss.fff")}: Decript member register data failed!");
                            Console.ForegroundColor = ConsoleColor.White;
                            continue;
                        }
                        Console.WriteLine($"{nCount} - {objCurrentDate.ToString("dd/MM/yyyy HH:mm:ss.fff")}: Decript member register data - {objMemberRegisterDecrypt.GetMemberFullName()}");
                        // Add to data
                        m_lstMemberRegisterData.Add(objMemberRegisterDecrypt);
                    }
                    await LoadAllInterviewRoom();
                }
                catch
                {
                    // No need process
                }
            });
            objTask.Wait();
        }

        /// <summary>
        /// Load All Interview Room
        /// </summary>
        /// <returns></returns>
        private async Task LoadAllInterviewRoom()
        {
            List<InterviewRoomReponse> lstInterviewRoom;
            List<InterviewRooms> lstInterviewRoomDb;
            List<RecruitmentInfo> lstRecruitmentInfo;
            InterviewRoomReponse objInterviewRoom;

            lstRecruitmentInfo = await m_objRecruitmentDbManager.GetAllRecruitment();
            if (lstRecruitmentInfo == null || lstRecruitmentInfo.Count == 0)
            {
                return;
            }

            m_dicInterviewRoom = new Dictionary<string, List<InterviewRoomReponse>>();
            foreach (RecruitmentInfo objRecruit in lstRecruitmentInfo)
            {
                lstInterviewRoomDb = await m_objRecruitmentDbManager.GetRoomsByRecruitmentId(objRecruit.Id);
                if (lstInterviewRoomDb == null || lstInterviewRoomDb.Count == 0)
                {
                    continue;
                }

                if (m_dicInterviewRoom.ContainsKey(objRecruit.Id) == false)
                {
                    lstInterviewRoom = new List<InterviewRoomReponse>();
                    m_dicInterviewRoom[objRecruit.Id] = lstInterviewRoom;
                }
                else
                {
                    lstInterviewRoom = m_dicInterviewRoom[objRecruit.Id];
                    if (lstInterviewRoom == null)
                    {
                        lstInterviewRoom = new List<InterviewRoomReponse>();
                    }
                }
                foreach (InterviewRooms objTempRoom in lstInterviewRoomDb)
                {
                    objInterviewRoom = await ConvertRoomDbToModel(objTempRoom);
                    lstInterviewRoom.Add(objInterviewRoom);
                }
            }
        }

        /// <summary>
        /// Convert Room Db To Model
        /// </summary>
        /// <param name="x_objInterviewRoom"></param>
        /// <returns></returns>
        private async Task<InterviewRoomReponse> ConvertRoomDbToModel(InterviewRooms x_objInterviewRoom)
        {
            InterviewRoomReponse objInterviewRoom;
            MemberDecryptModel objMember;
            MemberInRoomModel objMemberInRoomModel;
            List<ManagerRoom> lstManager;
            List<MemberInterviewRoom> lstMemberDb;
            List<MemberInRoomModel> lstMember;

            lstManager = new List<ManagerRoom>();
            lstMember = new List<MemberInRoomModel>();
            objInterviewRoom = new InterviewRoomReponse
            {
                Id = x_objInterviewRoom.Id,
                BreakTimes = x_objInterviewRoom.BreakTimes,
                CreateDate = x_objInterviewRoom.CreateDate,
                EmailSending = x_objInterviewRoom.EmailSending,
                EndDateTime = x_objInterviewRoom.EndDateTime,
                Note = x_objInterviewRoom.Note,
                RatedTime = x_objInterviewRoom.RatedTime,
                RecruitId = x_objInterviewRoom.RecruitId,
                RoomAddress = x_objInterviewRoom.RoomAddress,
                StartDateTime = x_objInterviewRoom.StartDateTime,
            };
            objMember = MemberInfoMemory.Instant.GetMemberById(x_objInterviewRoom.CreaterId);
            if (objMember != null)
            {
                objInterviewRoom.CreaterName = objMember.GetMemberFullName();
            }
            // Get member in room
            lstMemberDb = await m_objRecruitmentDbManager.GetMemberInterviewByRoomId(x_objInterviewRoom.Id);
            if (lstMemberDb != null)
            {
                foreach (MemberInterviewRoom objTempMember in lstMemberDb)
                {
                    objMemberInRoomModel = ConvertMemberInRoomDbToModel(objTempMember);
                    if (objMemberInRoomModel == null)
                    {
                        continue;
                    }

                    lstMember.Add(objMemberInRoomModel);
                }
                objInterviewRoom.Members = lstMember;
            }

            // Get manager
            if (x_objInterviewRoom.ManagerIds != null)
            {
                foreach (string objManagerId in x_objInterviewRoom.ManagerIds)
                {
                    objMember = MemberInfoMemory.Instant.GetMemberById(objManagerId);
                    if (objMember == null)
                    {
                        continue;
                    }
                    lstManager.Add(new ManagerRoom
                    {
                        AvatarPath = objMember.AvatarPath,
                        FullName = objMember.GetMemberFullName(),
                        Id = objMember.Id,
                        StudentId = objMember.StudentId
                    });
                }
                objInterviewRoom.Managers = lstManager;
            }
            return objInterviewRoom;
        }

        /// <summary>
        /// Convert Member In Room Db To Model
        /// </summary>
        /// <param name="x_objMemberInterviewRoom"></param>
        /// <returns></returns>
        private MemberInRoomModel ConvertMemberInRoomDbToModel(MemberInterviewRoom x_objMemberInterviewRoom)
        {
            MemberInRoomModel objMemberInRoomModel;
            MemberRegisterDecryptModel objMemberRegisterDecryptModel;
            MemberDecryptModel objMember;

            if (x_objMemberInterviewRoom == null)
            {
                return null;
            }
            objMemberInRoomModel = new MemberInRoomModel();
            objMemberRegisterDecryptModel = GetMemberById(x_objMemberInterviewRoom.MemberId);
            if (objMemberRegisterDecryptModel == null)
            {
                return null;
            }
            objMemberInRoomModel = new MemberInRoomModel
            {
                CreateDate = x_objMemberInterviewRoom.CreateDate,
                Id = x_objMemberInterviewRoom.Id,
                InterviewTime = x_objMemberInterviewRoom.InterviewTime,
                IsChangeSchedule = x_objMemberInterviewRoom.IsChangeSchedule,
                IsInterviewed = x_objMemberInterviewRoom.IsInterviewed,
                SendEmailState = x_objMemberInterviewRoom.SendEmailState,
                MemberInfo = objMemberRegisterDecryptModel
            };
            objMember = MemberInfoMemory.Instant.GetMemberById(x_objMemberInterviewRoom.CreaterId);
            if (objMember != null)
            {
                objMemberInRoomModel.Creater = new ManagerRoom
                {
                    AvatarPath = objMember.AvatarPath,
                    FullName = objMember.GetMemberFullName(),
                    Id = objMember.Id,
                    StudentId = objMember.StudentId,
                };
            }
            return objMemberInRoomModel;
        }

        /// <summary>
        /// Update Interview Room
        /// </summary>
        /// <param name="x_strRecruitId"></param>
        /// <param name="x_strRoomId"></param>
        /// <param name="x_bIsDelete"></param>
        /// <returns></returns>
        public async Task<InterviewRoomReponse> UpdateInterviewRoom(string x_strRecruitId, string x_strRoomId, bool x_bIsDelete)
        {
            List<InterviewRoomReponse> lstInterviewRoom;
            InterviewRoomReponse objInterviewRoom;
            InterviewRooms objTempRoom;
            int nIndex;

            if (x_bIsDelete == true)
            {
                if (m_dicInterviewRoom.ContainsKey(x_strRecruitId) == true)
                {
                    if (m_dicInterviewRoom[x_strRecruitId] != null)
                    {
                        m_dicInterviewRoom[x_strRecruitId].RemoveAll(obj => obj.Id.Equals(x_strRoomId) == true);
                    }
                }
                return null;
            }
            else
            {
                if (m_dicInterviewRoom.ContainsKey(x_strRecruitId) == false)
                {
                    lstInterviewRoom = new List<InterviewRoomReponse>();
                    m_dicInterviewRoom[x_strRecruitId] = lstInterviewRoom;
                }
                else
                {
                    lstInterviewRoom = m_dicInterviewRoom[x_strRecruitId];
                    if (lstInterviewRoom == null)
                    {
                        lstInterviewRoom = new List<InterviewRoomReponse>();
                    }
                }
                objTempRoom = await m_objRecruitmentDbManager.GetRoomById(x_strRoomId);
                if (objTempRoom == null)
                {
                    return null;
                }
                objInterviewRoom = await ConvertRoomDbToModel(objTempRoom);
                nIndex = lstInterviewRoom.FindIndex(obj => obj.Id.Equals(objInterviewRoom.Id) == true);
                if (nIndex < 0)
                {
                    lstInterviewRoom.Add(objInterviewRoom);
                }
                else
                {
                    lstInterviewRoom[nIndex] = objInterviewRoom;
                }
                return objInterviewRoom;
            }
        }

        /// <summary>
        /// Delete Recruit
        /// </summary>
        /// <param name="x_strRecruitId"></param>
        /// <returns></returns>
        public bool DeleteRecruit(string x_strRecruitId)
        {
            if (m_dicInterviewRoom.ContainsKey(x_strRecruitId) == true)
            {
                m_dicInterviewRoom.Remove(x_strRecruitId);
            }
            return true;
        }

        /// <summary>
        /// Update Member To Interview
        /// </summary>
        /// <param name="x_strRecruitId"></param>
        /// <param name="x_strRoomId"></param>
        /// <param name="x_strMemberInRoomId"></param>
        /// <param name="x_bIsDelete"></param>
        /// <returns></returns>
        public async Task<MemberInRoomModel> UpdateMemberToInterview(string x_strRecruitId, string x_strRoomId, string x_strMemberInRoomId, bool x_bIsDelete)
        {
            List<InterviewRoomReponse> lstInterviewRoom;
            InterviewRoomReponse objInterviewRoom;
            MemberInRoomModel objMemberInRoomModel;
            MemberInterviewRoom objMemberInterviewRoom;
            int nIndex;

            if (x_bIsDelete == true)
            {
                if (m_dicInterviewRoom.ContainsKey(x_strRecruitId) == true)
                {
                    if (m_dicInterviewRoom[x_strRecruitId] != null)
                    {
                        objInterviewRoom = m_dicInterviewRoom[x_strRecruitId].Find(obj => obj.Id.Equals(x_strRoomId) == true);
                        if (objInterviewRoom != null && objInterviewRoom.Members != null)
                        {
                            objInterviewRoom.Members.RemoveAll(obj => obj.Id.Equals(x_strMemberInRoomId));
                        }
                    }
                }
                return null;
            }
            else
            {
                if (m_dicInterviewRoom.ContainsKey(x_strRecruitId) == false)
                {
                    lstInterviewRoom = new List<InterviewRoomReponse>();
                    m_dicInterviewRoom[x_strRecruitId] = lstInterviewRoom;
                }
                else
                {
                    lstInterviewRoom = m_dicInterviewRoom[x_strRecruitId];
                    if (lstInterviewRoom == null)
                    {
                        lstInterviewRoom = new List<InterviewRoomReponse>();
                    }
                }

                objInterviewRoom = m_dicInterviewRoom[x_strRecruitId].Find(obj => obj.Id.Equals(x_strRoomId) == true);
                if (objInterviewRoom == null)
                {
                    return null;
                }

                if (objInterviewRoom.Members == null)
                {
                    objInterviewRoom.Members = new List<MemberInRoomModel>();
                }

                objMemberInterviewRoom = await m_objRecruitmentDbManager.GetMemberInterviewRoomById(x_strMemberInRoomId, x_strRoomId, false);
                objMemberInRoomModel = ConvertMemberInRoomDbToModel(objMemberInterviewRoom);
                if (objMemberInRoomModel == null)
                {
                    return null;
                }
                nIndex = objInterviewRoom.Members.FindIndex(obj => obj.Id.Equals(x_strMemberInRoomId) == true);
                if (nIndex <= 0)
                {
                    objInterviewRoom.Members.Add(objMemberInRoomModel);
                }
                else
                {
                    objInterviewRoom.Members[nIndex] = objMemberInRoomModel;
                }
                return objMemberInRoomModel;
            }
        }

        /// <summary>
        /// Get Rooms By RecruitId
        /// </summary>
        /// <param name="x_strRecruitId"></param>
        /// <returns></returns>
        public List<InterviewRoomReponse> GetRoomsByRecruitId(string x_strRecruitId)
        {
            List<InterviewRoomReponse> lstInterviewRoom;
            if (m_dicInterviewRoom.ContainsKey(x_strRecruitId) == false)
            {
                return null;
            }
            lstInterviewRoom = m_dicInterviewRoom[x_strRecruitId];
            return lstInterviewRoom;
        }

        /// <summary>
        /// Member Approval
        /// </summary>
        /// <param name="x_objMemberInfo"></param>
        /// <returns></returns>
        public async Task<string> MemberApproval(MemberRegisterDecryptModel x_objMemberInfo)
        {
            MemberDecryptModel objMemberInfo;
            AddressInfo objAddress;
            AddressInfoModel objAddressInfoModel;
            Accounts objAccount;
            Roles objRole;
            List<Roles> lstRoles;
            string strNewKey;
            string strNewPasswd;
            bool bResult;

            try
            {
                bResult = MemberInfoMemory.Instant.CheckEmailExist(x_objMemberInfo.Email);
                if (bResult == false)
                {
                    return string.Empty;
                }
                // Add new address
                objAddress = new AddressInfo
                {
                    DistrictId = x_objMemberInfo.Hometown.District.DistrictId,
                    Id = string.Empty,
                    ProvinceId = x_objMemberInfo.Hometown.Province.ProvinceId,
                    SpecificAddress = x_objMemberInfo.Hometown.SpecificAddress,
                    WardId = x_objMemberInfo.Hometown.Ward.WardId
                };
                bResult = await m_objAddressDbManager.UpdateAddress(objAddress);
                if (bResult == false)
                {
                    return string.Empty;
                }
                objAddressInfoModel = await AddressInfoDbToModel(objAddress);
                // Add member
                objMemberInfo = new MemberDecryptModel
                {
                    Id = string.Empty,
                    AvatarPath = string.Empty,
                    BirthDay = x_objMemberInfo.BirthDay,
                    FirstName = x_objMemberInfo.FirstName,
                    ClassName = x_objMemberInfo.ClassName,
                    Email = x_objMemberInfo.Email,
                    Faculty = x_objMemberInfo.Faculty,
                    Hometown = objAddressInfoModel,
                    IsMember = true,
                    LastName = x_objMemberInfo.LastName,
                    LinkFacebook = x_objMemberInfo.LinkFacebook,
                    PhoneNumber = x_objMemberInfo.PhoneNumber,
                    Sex = x_objMemberInfo.Sex,
                    StudentId = x_objMemberInfo.StudentId,
                    JoinDate = DateTimeHelper.GetCurrentDateTimeVN()
                };
                bResult = await MemberInfoMemory.Instant.UpdateMemberInfo(objMemberInfo);
                if (bResult == false)
                {
                    return string.Empty;
                }

                // Add Account
                lstRoles = await m_objRoleDbManager.GetAllRole();
                objRole = lstRoles.Find(obj => obj.RoleName.EqualsIgnoreCase("Thành viên") == true);
                strNewPasswd = CreatePassword(x_objMemberInfo.StudentId, x_objMemberInfo.BirthDay);
                objAccount = new Accounts
                {
                    Id = string.Empty,
                    IsDefaultPassword = true,
                    IsLocked = false,
                    MemberId = objMemberInfo.Id,
                    Password = strNewPasswd,
                    RoleId = objRole.Id,
                    Username = await CreateUsername(x_objMemberInfo.GetMemberFullName(), x_objMemberInfo.StudentId)
                };
                bResult = await m_objAccountDbManager.UpdateAccount(objAccount);
                if (bResult == false)
                {
                    return string.Empty;
                }
                // Encrypt passed
                strNewKey = HashKeyManager.GetNewPasswdHashKey(objAccount.Id);
                strNewPasswd = DTUSVCv3Aes.HashPasswordWithSalt(objAccount.Password, strNewKey);
                HashKeyManager.AddPasswdHashKey(objAccount.Id, strNewKey);
                objAccount.Password = strNewPasswd;
                await m_objAccountDbManager.UpdateAccount(objAccount);
                return objMemberInfo.Id;
            }
            catch
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Create Username
        /// </summary>
        /// <param name="x_strFullName"></param>
        /// <param name="x_strStudentId"></param>
        /// <param name="x_nCount"></param>
        /// <returns></returns>
        private async Task<string> CreateUsername(string x_strFullName, string x_strStudentId, int x_nCount = 2)
        {
            List<string> lstFullname;
            Accounts objAccount;
            string strUsername;

            lstFullname = x_strFullName.Split(' ').ToList();
            if ((lstFullname == null) || (lstFullname.Count == 0))
            {
                return string.Empty;
            }
            if (x_strStudentId.Length >= x_nCount)
            {
                return string.Empty;
            }

            strUsername = lstFullname[lstFullname.Count - 1];
            lstFullname.RemoveAt(lstFullname.Count - 1);
            if (lstFullname.Count > 1)
            {
                foreach (string objTemp in lstFullname)
                {
                    if (string.IsNullOrWhiteSpace(objTemp) == true)
                    {
                        continue;
                    }
                    strUsername += objTemp[0];
                }
            }
            strUsername = strUsername.RemoveUnicode().ToLower().Trim();
            if (string.IsNullOrWhiteSpace(x_strStudentId) == true)
            {
                return string.Empty;
            }
            strUsername += x_strStudentId.Substring(x_strStudentId.Length - x_nCount);
            objAccount = await m_objAccountDbManager.GetAccountByUserName(strUsername);
            if (objAccount != null)
            {
                strUsername = await CreateUsername(x_strFullName, x_strStudentId, (x_nCount + 1));
            }
            return strUsername;
        }

        /// <summary>
        /// Create Password
        /// </summary>
        /// <param name="x_strStudentId"></param>
        /// <param name="x_strBirthday"></param>
        /// <returns></returns>
        private string CreatePassword(string x_strStudentId, string x_strBirthday)
        {
            x_strBirthday = x_strBirthday.Replace("/", "");
            return string.Format("{0}{1}", x_strStudentId, x_strBirthday);
        }

        /// <summary>
        /// Convert Db To Model
        /// </summary>
        /// <param name="x_objData"></param>
        /// <returns></returns>
        private async Task<MemberRegisterDecryptModel> ConvertDbToModel(MembershipRegistration x_objData)
        {
            MemberRegisterDecryptModel objMemberRegisterModel;
            MembershipRegistration objDataDecrypt;
            AddressInfoModel objHometown;
            AddressInfo objAddress;
            Facultys objFaculty;
            InterviewDecryptInfo objInterviewInfo;
            ApprovalDecryptInfo objApprovalInfo;
            MemberDecryptModel objMemberInfo;
            string strMemberHashKey;

            if (x_objData == null)
            {
                return null;
            }

            // Get Member hash key
            strMemberHashKey = HashKeyManager.GetMemberHashKey(x_objData.Id);
            if (string.IsNullOrWhiteSpace(strMemberHashKey) == true)
            {
                return null;
            }
            // Decrypt
            objDataDecrypt = x_objData.ObjDecript(strMemberHashKey);
            // Get Faculty
            objFaculty = await m_objFacultyDbManager.GetFacultyById(x_objData.FacultyId);
            // Get address
            objAddress = await m_objAddressDbManager.GetAddressInfoById(x_objData.HometownId);
            objAddress = objAddress.ObjDecript(strMemberHashKey);
            objHometown = await AddressInfoDbToModel(objAddress);

            objMemberRegisterModel = new MemberRegisterDecryptModel
            {
                Id = x_objData.Id,
                BirthDay = x_objData.BirthDay,
                ClassName = x_objData.ClassName,
                Email = x_objData.Email,
                Faculty = objFaculty,
                FirstName = x_objData.FirstName,
                Hometown = objHometown,
                LastName = x_objData.LastName,
                LinkFacebook = x_objData.LinkFacebook,
                PhoneNumber = x_objData.PhoneNumber,
                Reason = x_objData.Reason,
                RecruitId = x_objData.RecruitId,
                RegisDate = x_objData.RegisDate,
                Sex = x_objData.Sex,
                StudentId = x_objData.StudentId,
                FailedInterView = x_objData.FailedInterView
            };
            // Set Approval
            if (x_objData.ApprovalInfo != null)
            {
                objMemberInfo = MemberInfoMemory.Instant.GetMemberById(x_objData.ApprovalInfo.ApproverId);
                objApprovalInfo = new ApprovalDecryptInfo
                {
                    ApprovalDate = x_objData.ApprovalInfo.ApprovalDate,
                    Approver = objMemberInfo
                };
                objMemberRegisterModel.ApprovalInfo = objApprovalInfo;
            }

            // set Interview Info
            if (x_objData.InterviewInfo != null)
            {
                objMemberInfo = MemberInfoMemory.Instant.GetMemberById(x_objData.InterviewInfo.ReviewerId);
                objInterviewInfo = new InterviewDecryptInfo
                {
                    Interviewed = x_objData.InterviewInfo.Interviewed,
                    EmailSent = x_objData.InterviewInfo.EmailSent,
                    InterviewAppointmentTime = x_objData.InterviewInfo.InterviewAppointmentTime,
                    Note = x_objData.InterviewInfo.ReviewContent,
                    ReviewContent = x_objData.InterviewInfo.ReviewContent,
                    ReviewDate = x_objData.InterviewInfo.ReviewDate,
                    Reviewer = objMemberInfo,
                    Scores = x_objData.InterviewInfo.Scores
                };

                objMemberRegisterModel.InterviewInfo = objInterviewInfo;
            }

            return objMemberRegisterModel;
        }

        /// <summary>
        /// Convert Model To Db
        /// </summary>
        /// <param name="x_objModelData"></param>
        /// <returns></returns>
        private MembershipRegistration ConvertModelToDb(MemberRegisterDecryptModel x_objModelData)
        {
            MembershipRegistration objDbData;
            InterviewInfo objInterviewInfo;
            ApprovalInfo objApprovalInfo;

            if (x_objModelData == null)
            {
                return null;
            }

            objInterviewInfo = null;
            objApprovalInfo = null;
            // Interview Info
            if (x_objModelData.InterviewInfo != null)
            {
                objInterviewInfo = new InterviewInfo
                {
                    EmailSent = x_objModelData.InterviewInfo.EmailSent,
                    InterviewAppointmentTime = x_objModelData.InterviewInfo.InterviewAppointmentTime,
                    Interviewed = x_objModelData.InterviewInfo.Interviewed,
                    Note = x_objModelData.InterviewInfo.Note,
                    ReviewContent = x_objModelData.InterviewInfo.ReviewContent,
                    ReviewDate = x_objModelData.InterviewInfo.ReviewDate,
                    ReviewerId = x_objModelData.InterviewInfo.Reviewer.Id,
                    Scores = x_objModelData.InterviewInfo.Scores
                };
            }
            // Approval Info
            if (x_objModelData.ApprovalInfo != null)
            {
                objApprovalInfo = new ApprovalInfo
                {
                    ApprovalDate = x_objModelData.ApprovalInfo.ApprovalDate,
                    ApproverId = x_objModelData.ApprovalInfo.Approver.Id
                };
            }
            objDbData = new MembershipRegistration
            {
                Id = x_objModelData.Id,
                ApprovalInfo = objApprovalInfo,
                BirthDay = x_objModelData.BirthDay,
                ClassName = x_objModelData.ClassName,
                Email = x_objModelData.Email,
                FacultyId = x_objModelData.Faculty.Id,
                FirstName = x_objModelData.FirstName,
                HometownId = x_objModelData.Hometown.Id,
                InterviewInfo = objInterviewInfo,
                LastName = x_objModelData.LastName,
                LinkFacebook = x_objModelData.LinkFacebook,
                PhoneNumber = x_objModelData.PhoneNumber,
                Reason = x_objModelData.Reason,
                RecruitId = x_objModelData.RecruitId,
                RegisDate = x_objModelData.RegisDate,
                Sex = x_objModelData.Sex,
                StudentId = x_objModelData.StudentId,
                FailedInterView = x_objModelData.FailedInterView
            };
            return objDbData;
        }

        /// <summary>
        /// Get List Member By RecruitId
        /// </summary>
        /// <param name="x_strRecruitId"></param>
        /// <returns></returns>
        public List<MemberRegisterDecryptModel> GetListMemberByRecruitId(string x_strRecruitId)
        {
            List<MemberRegisterDecryptModel> lstMembers;
            if ((m_lstMemberRegisterData == null) || (m_lstMemberRegisterData.Count == 0))
            {
                return null;
            }

            if (x_strRecruitId.MongoIdIsValid() == false)
            {
                return null;
            }

            lstMembers = m_lstMemberRegisterData.FindAll(obj => obj.RecruitId.Equals(x_strRecruitId) == true);

            lstMembers = lstMembers.DeepClone();
            return lstMembers;
        }

        /// <summary>
        /// Check Email Is Exist
        /// </summary>
        /// <param name="x_strEmail"></param>
        /// <param name="x_strRecruitId"></param>
        /// <returns></returns>
        public bool CheckEmailIsExist(string x_strEmail, string x_strRecruitId)
        {
            bool bIsAny;
            if ((m_lstMemberRegisterData == null) || (m_lstMemberRegisterData.Count == 0))
            {
                return false;
            }
            bIsAny = m_lstMemberRegisterData.Any(obj => obj.Email.EqualsIgnoreCase(x_strEmail) == true && obj.RecruitId.Equals(x_strRecruitId));
            return bIsAny;
        }

        /// <summary>
        /// Check Student id Is Exist
        /// </summary>
        /// <param name="x_strStudentId"></param>
        /// <param name="x_strRecruitId"></param>
        /// <returns></returns>
        public bool CheckStudentIdIsExist(string x_strStudentId, string x_strRecruitId)
        {
            RecruitmentInfo objRecruitmentInfo;
            Task objTask;
            bool bIsAny;
            if ((m_lstMemberRegisterData == null) || (m_lstMemberRegisterData.Count == 0))
            {
                return false;
            }
            if (x_strRecruitId.MongoIdIsValid() == true)
            {
                bIsAny = m_lstMemberRegisterData.Any(obj => obj.StudentId.EqualsIgnoreCase(x_strStudentId) == true && obj.RecruitId.Equals(x_strRecruitId));
            }
            else
            {
                objRecruitmentInfo = null;
                objTask = Task.Run(async () =>
                {
                    objRecruitmentInfo = await m_objRecruitmentDbManager.GetRecruitmentValid();
                });
                if (objRecruitmentInfo == null)
                {
                    return true;
                }
                bIsAny = m_lstMemberRegisterData.Any(obj => obj.StudentId.EqualsIgnoreCase(x_strStudentId) == true && obj.RecruitId.Equals(objRecruitmentInfo.Id));
            }
            return bIsAny;
        }

        /// <summary>
        /// Get Member By Id
        /// </summary>
        /// <param name="x_strMemberId"></param>
        /// <returns></returns>
        public MemberRegisterDecryptModel GetMemberById(string x_strMemberId)
        {
            MemberRegisterDecryptModel objMemberInfo;
            if ((m_lstMemberRegisterData == null) || (m_lstMemberRegisterData.Count == 0))
            {
                return null;
            }

            if (x_strMemberId.MongoIdIsValid() == false)
            {
                return null;
            }

            objMemberInfo = m_lstMemberRegisterData.Find(obj => obj.Id.Equals(x_strMemberId) == true);
            return objMemberInfo;
        }

        /// <summary>
        /// Update Member
        /// </summary>
        /// <param name="x_objModelData"></param>
        /// <returns></returns>
        public async Task<bool> UpdateMember(MemberRegisterDecryptModel x_objModelData)
        {
            MembershipRegistration objMembershipRegistration;
            MemberRegisterDecryptModel objDecrypt;
            AddressInfo objAddress;
            string strMemberHashKey;
            bool bIsSuccsess;
            bool bIsUpdate;

            if (x_objModelData == null)
            {
                return false;
            }

            objMembershipRegistration = ConvertModelToDb(x_objModelData);
            // Get address
            objAddress = await m_objAddressDbManager.GetAddressInfoById(objMembershipRegistration.Id);
            if (objMembershipRegistration.Id.MongoIdIsValid() == true)
            {
                // Decrypt address
                strMemberHashKey = HashKeyManager.GetMemberHashKey(objMembershipRegistration.Id);
                objAddress = objAddress.ObjDecript(strMemberHashKey);
                // Encrypt member info
                strMemberHashKey = HashKeyManager.GetNewMemberHashKey(objMembershipRegistration.Id);
                objMembershipRegistration = objMembershipRegistration.ObjEncript(strMemberHashKey);

                // Re-Encrypt address
                objAddress = objAddress.ObjEncript(strMemberHashKey);
                await m_objAddressDbManager.UpdateAddress(objAddress);
                m_lstMemberRegisterData.RemoveAll(obj => obj.Id.Equals(objMembershipRegistration.Id) == true);
                bIsUpdate = true;
            }
            else
            {
                objMembershipRegistration.Id = string.Empty;
                bIsUpdate = false;
            }
            // Update to data base
            bIsSuccsess = await m_objRecruitmentDbManager.UpdateMemberRegist(objMembershipRegistration);
            // Encrypt
            if (bIsUpdate == false)
            {
                strMemberHashKey = HashKeyManager.GetNewMemberHashKey(objMembershipRegistration.Id);
                objMembershipRegistration = objMembershipRegistration.ObjEncript(strMemberHashKey);
                // Update to data base
                bIsSuccsess = await m_objRecruitmentDbManager.UpdateMemberRegist(objMembershipRegistration);

                // Re-Encrypt address
                objAddress = objAddress.ObjEncript(strMemberHashKey);
                await m_objAddressDbManager.UpdateAddress(objAddress);
            }
            // Convert Db to model
            objDecrypt = await ConvertDbToModel(objMembershipRegistration);
            bIsSuccsess = false;
            if (objDecrypt != null)
            {
                m_lstMemberRegisterData.Add(objDecrypt);
                bIsSuccsess = true;
            }
            return bIsSuccsess;
        }

        /// <summary>
        /// Delete Member Register
        /// </summary>
        /// <param name="x_strRecruitId"></param>
        /// <param name="x_strMemberId"></param>
        /// <returns></returns>
        public async Task<bool> DeleteMemberRegister(string x_strRecruitId, string x_strMemberId)
        {
            bool bIsSuccsess;

            bIsSuccsess = await m_objRecruitmentDbManager.DeleteMemberById(x_strRecruitId, x_strMemberId);
            if (bIsSuccsess == true)
            {
                m_lstMemberRegisterData.RemoveAll(obj => obj.Id.Equals(x_strMemberId));
            }
            return bIsSuccsess;
        }

        /// <summary>
        /// Delete All Member
        /// </summary>
        /// <param name="x_strRecruitId"></param>
        /// <returns></returns>
        public async Task<bool> DeleteAllMember(string x_strRecruitId)
        {
            bool bIsSuccsess;

            bIsSuccsess = await m_objRecruitmentDbManager.DeleteAllMember(x_strRecruitId);
            if (bIsSuccsess == true)
            {
                m_lstMemberRegisterData.RemoveAll(obj => obj.RecruitId.Equals(x_strRecruitId) == true);
            }
            return bIsSuccsess;
        }

        /// <summary>
        /// Address Info Db To Model
        /// </summary>
        private async Task<AddressInfoModel> AddressInfoDbToModel(AddressInfo x_objAddressInfo)
        {
            AddressInfoModel objAddressInfoModel;

            try
            {
                if (x_objAddressInfo == null)
                {
                    return null;
                }

                objAddressInfoModel = new AddressInfoModel();
                objAddressInfoModel.Id = x_objAddressInfo.Id;
                objAddressInfoModel.Province = await m_objAddressDbManager.GetProvinceById(x_objAddressInfo.ProvinceId);
                objAddressInfoModel.District = await m_objAddressDbManager.GetDistrictById(x_objAddressInfo.DistrictId);
                objAddressInfoModel.Ward = await m_objAddressDbManager.GetWardById(x_objAddressInfo.WardId);
                objAddressInfoModel.SpecificAddress = x_objAddressInfo.SpecificAddress;
                return objAddressInfoModel;
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// Update Faculty
        /// </summary>
        /// <param name="x_objFacultys"></param>
        public void UpdateFaculty(Facultys x_objFacultys)
        {
            if (m_lstMemberRegisterData == null)
            {
                return;
            }

            foreach (MemberRegisterDecryptModel objMember in m_lstMemberRegisterData)
            {
                if (objMember.Faculty.Id.Equals(x_objFacultys.Id) == true)
                {
                    objMember.Faculty.FacultyName = x_objFacultys.FacultyName;
                }
            }
        }
        #endregion
    }
}
