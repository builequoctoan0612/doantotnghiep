﻿using DTUSVCv3_EntityFramework;

namespace DTUSVCv3_MemoryData
{
    [Serializable]
    public class AddressInfoModel
    {
        public string Id { get; set; }
        public Provinces Province { get; set; }
        public Districts District { get; set; }
        public Wards Ward { get; set; }
        public string SpecificAddress { get; set; }

        /// <summary>
        /// To String
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            string strFullAddress;
            try
            {
                if ((Province == null) || (District == null) || (Ward == null))
                {
                    return string.Empty;
                }

                strFullAddress = string.Format("{0} - {1} - {2} - {3}", SpecificAddress, Ward.WardName, District.DistrictName, Province.ProvinceName);
                return strFullAddress;
            }
            catch
            {
                return string.Empty;
            }
        }
    }
}
