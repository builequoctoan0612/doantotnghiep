﻿using DTUSVCv3_Library;
using Microsoft.AspNetCore.Http;

namespace DTUSVCv3_MemoryData
{
    public static class LoginData
    {
        private static List<LoginInfoModel> m_lstLoginData;
        private const string SOURCE = "LoginData";

        /// <summary>
        /// Get List Login
        /// </summary>
        /// <returns></returns>
        public static List<LoginInfoModel> GetListLogin()
        {
            return m_lstLoginData;
        }

        /// <summary>
        /// Set Login
        /// </summary>
        public static LoginInfoModel SetLogin(string x_strRoldeId, List<int> x_lstPermissions, string x_strStudentId,
                                                string x_strMemberId, string x_strToken, string x_strIp, string x_strUserId)
        {
            LoginInfoModel objLoginInfo;

            if (m_lstLoginData == null)
            {
                m_lstLoginData = new List<LoginInfoModel>();
            }

            objLoginInfo = new LoginInfoModel
            {
                IpAdderss = x_strIp,
                MemberId = x_strMemberId,
                AccessToken = x_strToken,
                UserId = x_strUserId,
                RoleDbId = x_strRoldeId,
                Permissions = x_lstPermissions,
                StudentId = x_strStudentId,
            };
            SetRefreshToken(objLoginInfo);
            SetViewToken(objLoginInfo);
            m_lstLoginData.Add(objLoginInfo);
            LogHelper.WriteLog(LogTypeEnum.LOGIN, x_strIp, x_strMemberId, SuccsessConstant.LOGIN_SUCCSESS, SOURCE);
            return objLoginInfo;
        }

        /// <summary>
        /// Get Login Info
        /// </summary>
        /// <param name="x_strRefeshToken"></param>
        /// <param name="x_objHttpContext"></param>
        /// <returns></returns>
        public static LoginInfoModel GetLoginInfo(string x_strRefeshToken, HttpContext x_objHttpContext)
        {
            string strAccessToken;
            LoginInfoModel objLoginInfoModel;

            if ((m_lstLoginData == null) || (m_lstLoginData.Count == 0))
            {
                return null;
            }

            strAccessToken = x_objHttpContext.GetTokenByHttpContext();

            if ((strAccessToken.EqualsIgnoreCase(ErrorConstant.UNKNOWN) == true) || (string.IsNullOrWhiteSpace(x_strRefeshToken) == true))
            {
                return null;
            }

            objLoginInfoModel = m_lstLoginData.FirstOrDefault(obj => (obj.AccessToken.Equals(strAccessToken) == true) &&
                                                                        (obj.RefreshToken.Equals(x_strRefeshToken) == true));
            return objLoginInfoModel;
        }

        /// <summary>
        /// Logout With Out Token
        /// </summary>
        /// <param name="x_strToken"></param>
        /// <param name="x_strAccountId"></param>
        public static void LogoutWithOutToken(string x_strToken, string x_strAccountId)
        {
            if (string.IsNullOrWhiteSpace(x_strToken) == true ||
                (string.IsNullOrWhiteSpace(x_strAccountId) == true) ||
                m_lstLoginData == null)
            {
                return;
            }

            m_lstLoginData.RemoveAll(obj => (obj.UserId.Equals(x_strAccountId) == true) && (obj.AccessToken.Equals(x_strToken) == false));
        }

        /// <summary>
        /// Referh Token
        /// </summary>
        /// <param name="x_strRefeshToken"></param>
        /// <param name="x_strAccessToken"></param>
        /// <param name="x_objLoginInfoModel"></param>
        /// <returns></returns>
        public static bool ReferhToken(string x_strAccessToken, LoginInfoModel x_objLoginInfoModel)
        {
            if (x_objLoginInfoModel == null)
            {
                return false;
            }

            x_objLoginInfoModel.LoginDate = DateTimeHelper.GetCurrentDateTimeVN();
            x_objLoginInfoModel.AccessToken = x_strAccessToken;
            SetViewToken(x_objLoginInfoModel);
            return true;
        }

        /// <summary>
        /// Set Refresh Token
        /// </summary>
        private static void SetRefreshToken(LoginInfoModel x_objLoginInfoModel)
        {
            string strRefreshToken;
            int nIndex;

            if (x_objLoginInfoModel == null)
            {
                return;
            }

            strRefreshToken = x_objLoginInfoModel.CreateToken();
            nIndex = m_lstLoginData.FindIndex(obj => obj.RefreshToken.Equals(strRefreshToken));
            if (nIndex == -1)
            {
                x_objLoginInfoModel.RefreshToken = strRefreshToken;
                return;
            }
            else
            {
                SetRefreshToken(x_objLoginInfoModel);
                return;
            }
        }

        /// <summary>
        /// Set View Token
        /// </summary>
        private static void SetViewToken(LoginInfoModel x_objLoginInfoModel)
        {
            string strViewToken;
            int nIndex;

            if (x_objLoginInfoModel == null)
            {
                return;
            }

            strViewToken = x_objLoginInfoModel.CreateToken();
            nIndex = m_lstLoginData.FindIndex(obj => obj.ViewFileToken.Equals(strViewToken));
            if (nIndex == -1)
            {
                x_objLoginInfoModel.ViewFileToken = strViewToken;
                return;
            }
            else
            {
                SetRefreshToken(x_objLoginInfoModel);
                return;
            }
        }

        /// <summary>
        /// Get View File Token
        /// </summary>
        public static string GetViewFileToken(string x_strToken)
        {
            string strToken;
            LoginInfoModel objLoginInfoModel;

            if (m_lstLoginData == null)
            {
                return string.Empty;
            }

            objLoginInfoModel = m_lstLoginData.Find(obj => obj.AccessToken.Equals(x_strToken) == true);
            if (objLoginInfoModel == null)
            {
                return string.Empty;
            }

            strToken = objLoginInfoModel.ViewFileToken;
            return strToken;
        }

        /// <summary>
        /// Check Login Info
        /// </summary>
        /// <param name="x_strToken"></param>
        /// <returns>-1: Not logged in yet; 0: Login session has expired; 1: Logged</returns>
        public static int CheckLogin(string x_strToken)
        {
            LoginInfoModel objLoginInfo;
            DateTime objDateNow;
            TimeSpan objTimeSpan;

            if (m_lstLoginData == null)
            {
                return -1;
            }

            objLoginInfo = m_lstLoginData.Find(obj => obj.AccessToken.Equals(x_strToken));
            if (objLoginInfo == null)
            {
                return -1;
            }

            objDateNow = DateTimeHelper.GetCurrentDateTimeVN();
            objTimeSpan = objLoginInfo.LoginDate.AddHours(24).Subtract(objDateNow);
            if (objTimeSpan.TotalSeconds <= 0)
            {
                return 0;
            }

            return 1;
        }

        /// <summary>
        /// Check Login By Id
        /// </summary>
        public static bool CheckLoginById(string x_strAccountId)
        {
            bool bIsLogin;
            if (m_lstLoginData == null)
            {
                return false;
            }

            bIsLogin = m_lstLoginData.Find(obj => obj.UserId.Equals(x_strAccountId)) != null;
            return bIsLogin;
        }

        /// <summary>
        /// Logout
        /// </summary>
        public static void Logout(string x_strToken)
        {
            if (m_lstLoginData == null || string.IsNullOrWhiteSpace(x_strToken))
            {
                return;
            }

            m_lstLoginData.RemoveAll(obj => obj.AccessToken.Equals(x_strToken) || obj.RefreshToken.Equals(x_strToken));
        }

        /// <summary>
        /// Logout by account id
        /// </summary>
        public static void LogoutById(string x_strAccountId)
        {
            if (m_lstLoginData == null || string.IsNullOrWhiteSpace(x_strAccountId))
            {
                return;
            }
            m_lstLoginData.RemoveAll(obj => obj.UserId.Equals(x_strAccountId));
        }

        /// <summary>
        /// Get Member Id By Token
        /// </summary>
        public static string GetMemberIdByToken(string x_strToken)
        {
            LoginInfoModel objLoginInfo;

            if (string.IsNullOrEmpty(x_strToken) == true)
            {
                return string.Empty;
            }

            if (m_lstLoginData == null)
            {
                return string.Empty;
            }

            objLoginInfo = m_lstLoginData.Find(obj => obj.AccessToken.Equals(x_strToken));
            if (objLoginInfo == null)
            {
                return string.Empty;
            }

            return objLoginInfo.MemberId;
        }

        /// <summary>
        /// Check Login By View Token
        /// </summary>
        public static bool CheckLoginByViewToken(string x_strViewToken)
        {
            bool bIsLogin;

            bIsLogin = m_lstLoginData.Find(obj => obj.ViewFileToken.Equals(x_strViewToken) == true) != null;
            return bIsLogin;
        }

        /// <summary>
        /// Check Permissions
        /// </summary>
        /// <param name="x_strViewToken"></param>
        /// <param name="x_nPermissions"></param>
        /// <returns></returns>
        public static bool CheckPermissions(string x_strViewToken, params int[] x_nPermissions)
        {
            LoginInfoModel objLoginModel;

            objLoginModel = m_lstLoginData.Find(obj => obj.ViewFileToken.Equals(x_strViewToken) == true);
            if (objLoginModel == null)
            {
                return false;
            }

            if (objLoginModel.Permissions == null)
            {
                return false;
            }
            if (objLoginModel.Permissions.Intersect(x_nPermissions).Any() == false)
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// Get Role By View Token
        /// </summary>
        public static List<int> GetRoleIdByViewToken(string x_strViewToken)
        {
            LoginInfoModel objLoginInfoModel;

            objLoginInfoModel = m_lstLoginData.Find(obj => obj.ViewFileToken.Equals(x_strViewToken) == true);
            if (objLoginInfoModel == null)
            {
                return null;
            }

            return objLoginInfoModel.Permissions;
        }
    }
}