﻿using DTUSVCv3_Library;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTUSVCv3_MemoryData
{
    public class LoginInfoModel
    {
        #region Types
        private const string STR_CHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        #endregion
        public DateTime LoginDate { get; set; }
        public List<int> Permissions { get; set; }
        public string MemberId { get; set; }
        public string RoleDbId { get; set; }
        public string StudentId { get; set; }
        public string UserId { get; set; }
        public string RefreshToken { get; set; }
        public string AccessToken { get; set; }
        public string IpAdderss { get; set; }
        public string ViewFileToken { get; set; }

        public LoginInfoModel()
        {
            LoginDate = DateTimeHelper.GetCurrentDateTimeVN();
        }

        /// <summary>
        /// Create Token
        /// </summary>
        public string CreateToken()
        {
            string strToken;
            Random objRandom;

            objRandom = new Random();
            // Random 32 chars
            // There are 62^64 ways to choose 128 characters from a set of 62 elements. Probability to generate 2 identical token is 1/62^64
            strToken = new string(Enumerable.Repeat(STR_CHARS, 64).Select(str => str[objRandom.Next(str.Length)]).ToArray());

            return strToken;
        }
    }
}
