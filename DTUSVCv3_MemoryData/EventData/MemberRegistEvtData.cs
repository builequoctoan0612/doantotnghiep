﻿using DTUSVCv3_EntityFramework;
using DTUSVCv3_Library;
using System.Collections.Generic;

namespace DTUSVCv3_MemoryData.EventData
{
    public class MemberRegistEvtData
    {
        #region Constructor

        public MemberRegistEvtData()
        {
            m_objActivityManager = new ActivityManager();
            m_objFacultyDbManager = new FacultyDbManager();
            m_dicMemberRegistEvtModel = new Dictionary<string, List<MemberRegistEvtModel>>();
            m_dicLeaveOfAbsenceModel = new Dictionary<string, List<LeaveOfAbsenceModel>>();
        }
        #endregion

        #region Fields
        private ActivityManager m_objActivityManager;
        private FacultyDbManager m_objFacultyDbManager;
        private static MemberRegistEvtData m_objMemberRegistEvtData;
        private Dictionary<string, List<MemberRegistEvtModel>> m_dicMemberRegistEvtModel;
        private Dictionary<string, List<LeaveOfAbsenceModel>> m_dicLeaveOfAbsenceModel;
        #endregion

        #region Properties
        public static MemberRegistEvtData Instant
        {
            get
            {
                if (m_objMemberRegistEvtData == null)
                {
                    m_objMemberRegistEvtData = new MemberRegistEvtData();
                }
                return m_objMemberRegistEvtData;
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Decript Data
        /// </summary>
        /// <returns></returns>
        public void LoadAllMember()
        {
            List<MemberRegistEvtModel> lstMemberRegistEvtModel;
            List<ActivityChecklist> lstActivityChecklist;
            List<CommunityRegistEvt> lstCommunityRegistEvt;
            List<MemberRegistEvt> lstMemberRegistEvt;
            List<LeaveOfAbsence> lstLeaveOfAbsence;
            List<LeaveOfAbsenceModel> lstLeaveOfAbsenceModel;
            MemberRegistEvtModel objMemberRegistEvtModel;
            LeaveOfAbsenceModel objLeaveOfAbsenceModel;
            Task objTask;
            DateTime objCurrentDate;
            int nCount;

            objTask = Task.Run(async () =>
            {
                try
                {
                    lstActivityChecklist = await m_objActivityManager.GetAllCheckList();
                    lstCommunityRegistEvt = await m_objActivityManager.GetAllCommunityRegistEvt();
                    lstMemberRegistEvt = await m_objActivityManager.GetAllMemberRegistEvt();
                    lstLeaveOfAbsence = await m_objActivityManager.GetAllLeaveOfAbsence();

                    // Community member
                    if (lstCommunityRegistEvt != null)
                    {
                        nCount = 0;
                        m_dicMemberRegistEvtModel = new Dictionary<string, List<MemberRegistEvtModel>>();
                        foreach (CommunityRegistEvt objCommunityMember in lstCommunityRegistEvt)
                        {
                            objCurrentDate = DateTimeHelper.GetCurrentDateTimeVN();
                            // Get List
                            if (m_dicMemberRegistEvtModel.ContainsKey(objCommunityMember.EventId) == false)
                            {
                                lstMemberRegistEvtModel = new List<MemberRegistEvtModel>();
                                m_dicMemberRegistEvtModel[objCommunityMember.EventId] = lstMemberRegistEvtModel;
                            }
                            else
                            {
                                lstMemberRegistEvtModel = m_dicMemberRegistEvtModel[objCommunityMember.EventId];
                            }

                            // Convert to model
                            objMemberRegistEvtModel = await CommunityDbToModel(objCommunityMember);
                            nCount++;
                            if (objMemberRegistEvtModel == null)
                            {
                                Console.ForegroundColor = ConsoleColor.Red;
                                Console.WriteLine($"{nCount} - {objCurrentDate.ToString("dd/MM/yyyy HH:mm:ss.fff")}: Decript community member register event data failed!");
                                Console.ForegroundColor = ConsoleColor.White;
                                continue;
                            }
                            Console.WriteLine($"{nCount} - {objCurrentDate.ToString("dd/MM/yyyy HH:mm:ss.fff")}: Decript Community member register event data - {objMemberRegistEvtModel.GetMemberFullName()} - Score {objMemberRegistEvtModel.TotalScore}");
                            lstMemberRegistEvtModel.Add(objMemberRegistEvtModel);
                        }
                    }

                    // Member register
                    if (lstMemberRegistEvt != null)
                    {
                        nCount = 0;
                        foreach (MemberRegistEvt objMemberRegist in lstMemberRegistEvt)
                        {
                            objCurrentDate = DateTimeHelper.GetCurrentDateTimeVN();
                            // Get List
                            if (m_dicMemberRegistEvtModel.ContainsKey(objMemberRegist.EventId) == false)
                            {
                                lstMemberRegistEvtModel = new List<MemberRegistEvtModel>();
                                m_dicMemberRegistEvtModel[objMemberRegist.EventId] = lstMemberRegistEvtModel;
                            }
                            else
                            {
                                lstMemberRegistEvtModel = m_dicMemberRegistEvtModel[objMemberRegist.EventId];
                            }

                            // Convert to model
                            objMemberRegistEvtModel = await ConvertMemberToModel(objMemberRegist);
                            nCount++;
                            if (objMemberRegistEvtModel == null)
                            {
                                Console.ForegroundColor = ConsoleColor.Red;
                                Console.WriteLine($"{nCount} - {objCurrentDate.ToString("dd/MM/yyyy HH:mm:ss.fff")}: Decript member register event data failed!");
                                Console.ForegroundColor = ConsoleColor.White;
                                continue;
                            }
                            Console.WriteLine($"{nCount} - {objCurrentDate.ToString("dd/MM/yyyy HH:mm:ss.fff")}: Decript Member member register event data - {objMemberRegistEvtModel.GetMemberFullName()} - Score {objMemberRegistEvtModel.TotalScore}");
                            lstMemberRegistEvtModel.Add(objMemberRegistEvtModel);
                        }
                    }

                    // Leave
                    if (lstLeaveOfAbsence != null)
                    {
                        m_dicLeaveOfAbsenceModel = new Dictionary<string, List<LeaveOfAbsenceModel>>();
                        foreach (LeaveOfAbsence objLeave in lstLeaveOfAbsence)
                        {
                            objLeaveOfAbsenceModel = ConvertLeaveToModel(objLeave);
                            if (objLeaveOfAbsenceModel == null)
                            {
                                continue;
                            }

                            if (m_dicLeaveOfAbsenceModel.ContainsKey(objLeave.EventId) == false)
                            {
                                lstLeaveOfAbsenceModel = new List<LeaveOfAbsenceModel>();
                                m_dicLeaveOfAbsenceModel[objLeave.EventId] = lstLeaveOfAbsenceModel;
                            }
                            else
                            {
                                lstLeaveOfAbsenceModel = m_dicLeaveOfAbsenceModel[objLeave.EventId];
                            }

                            lstLeaveOfAbsenceModel.Add(objLeaveOfAbsenceModel);
                        }
                    }
                }
                catch
                {
                    // No need process
                }
            });
            objTask.Wait();
        }

        /// <summary>
        /// Convert Leave To Model
        /// </summary>
        /// <param name="x_objData"></param>
        /// <returns></returns>
        private LeaveOfAbsenceModel ConvertLeaveToModel(LeaveOfAbsence x_objData)
        {
            MemberDecryptModel objMember;
            LeaveOfAbsenceModel objLeaveOfAbsenceModel;
            if (x_objData == null)
            {
                return null;
            }

            objMember = MemberInfoMemory.Instant.GetMemberById(x_objData.MemberId);
            if (objMember == null)
            {
                return null;
            }

            objLeaveOfAbsenceModel = new LeaveOfAbsenceModel
            {
                MemberId = objMember.Id,
                Id = x_objData.Id,
                CreateDate = x_objData.CreateDate,
                EventId = x_objData.EventId,
                FacebookPath = objMember.LinkFacebook,
                FirstName = objMember.FirstName,
                LastName = objMember.LastName,
                Reason = x_objData.Reason,
                StudentId = objMember.StudentId
            };

            return objLeaveOfAbsenceModel;
        }

        /// <summary>
        /// Community Db To Model
        /// </summary>
        /// <param name="x_objCommunityMember"></param>
        /// <returns></returns>
        private async Task<MemberRegistEvtModel> CommunityDbToModel(CommunityRegistEvt x_objCommunityMember)
        {
            CommunityRegistEvt objCommunityMemberDecrypt;
            MemberRegistEvtModel objMemberRegistEvtModel;
            Facultys objFaculty;
            List<ActivityChecklistData> lstEvtCheckList;
            string strDcryptKey;
            double dScore;

            if (x_objCommunityMember == null)
            {
                return null;
            }

            // Get Dcrypt key and Decrypt data
            strDcryptKey = HashKeyManager.GetMemberHashKey(x_objCommunityMember.Id);
            objCommunityMemberDecrypt = x_objCommunityMember.ObjDecript(strDcryptKey);
            if (objCommunityMemberDecrypt == null)
            {
                return null;
            }
            // Get faculty
            objFaculty = await m_objFacultyDbManager.GetFacultyById(objCommunityMemberDecrypt.FacultyId);
            // Get check list
            lstEvtCheckList = await GetCheckList(objCommunityMemberDecrypt.Id, objCommunityMemberDecrypt.EventId);
            dScore = 0;
            if ((lstEvtCheckList != null) || (lstEvtCheckList.Count > 0))
            {
                dScore = lstEvtCheckList.Sum(obj => obj.Score);
            }
            // Convert model
            objMemberRegistEvtModel = new MemberRegistEvtModel
            {
                ClassName = objCommunityMemberDecrypt.ClassName,
                CreateDate = objCommunityMemberDecrypt.CreateDate,
                Email = objCommunityMemberDecrypt.Email,
                EventId = objCommunityMemberDecrypt.EventId,
                FacebookPath = objCommunityMemberDecrypt.FacebookPath,
                Faculty = objFaculty,
                FirstName = objCommunityMemberDecrypt.FirstName,
                MemberId = objCommunityMemberDecrypt.Id,
                IsMember = false,
                LastName = objCommunityMemberDecrypt.LastName,
                PhoneNumber = objCommunityMemberDecrypt.PhoneNumber,
                StudentId = objCommunityMemberDecrypt.StudentId,
                EvtCheckList = lstEvtCheckList,
                TotalScore = dScore,
                Id = objCommunityMemberDecrypt.Id,
                SendCertificate = objCommunityMemberDecrypt.SendCertificate
            };
            return objMemberRegistEvtModel;
        }

        /// <summary>
        /// Convert Member To Model
        /// </summary>
        /// <param name="x_objMemberRegistEvt"></param>
        /// <returns></returns>
        private async Task<MemberRegistEvtModel> ConvertMemberToModel(MemberRegistEvt x_objMemberRegistEvt)
        {
            MemberRegistEvtModel objMemberRegistEvtModel;
            List<ActivityChecklistData> lstEvtCheckList;
            MemberDecryptModel objMemberDecrypt;
            double dScore;

            if (x_objMemberRegistEvt == null)
            {
                return null;
            }
            // Get member data
            objMemberDecrypt = MemberInfoMemory.Instant.GetMemberExistById(x_objMemberRegistEvt.MemberId);
            if (objMemberDecrypt == null)
            {
                return null;
            }
            // Get check list
            lstEvtCheckList = await GetCheckList(x_objMemberRegistEvt.MemberId, x_objMemberRegistEvt.EventId);
            dScore = 0;
            if ((lstEvtCheckList != null) || (lstEvtCheckList.Count > 0))
            {
                dScore = lstEvtCheckList.Sum(obj => obj.Score);
            }
            // Convert model
            objMemberRegistEvtModel = new MemberRegistEvtModel
            {
                ClassName = objMemberDecrypt.ClassName,
                CreateDate = x_objMemberRegistEvt.CreateDate,
                Email = objMemberDecrypt.Email,
                EventId = x_objMemberRegistEvt.EventId,
                FacebookPath = objMemberDecrypt.LinkFacebook,
                Faculty = objMemberDecrypt.Faculty,
                FirstName = objMemberDecrypt.FirstName,
                MemberId = objMemberDecrypt.Id,
                IsMember = true,
                LastName = objMemberDecrypt.LastName,
                PhoneNumber = objMemberDecrypt.PhoneNumber,
                StudentId = objMemberDecrypt.StudentId,
                EvtCheckList = lstEvtCheckList,
                TotalScore = dScore,
                Id = x_objMemberRegistEvt.Id,
                SendCertificate = x_objMemberRegistEvt.SendCertificate
            };
            return objMemberRegistEvtModel;
        }

        /// <summary>
        /// Update Member Register
        /// </summary>
        /// <param name="x_objMemberRegistEvtModel"></param>
        /// <returns></returns>
        public async Task UpdateMemberRegister(MemberRegistEvtModel x_objMemberRegistEvtModel)
        {
            MemberRegistEvt objMemberRegistEvt;
            CommunityRegistEvt objCommunityMember;
            // Update Member
            if (x_objMemberRegistEvtModel.IsMember == true)
            {
                objMemberRegistEvt = await m_objActivityManager.GetMemberRegistEvtById(x_objMemberRegistEvtModel.Id);
                if (objMemberRegistEvt != null)
                {
                    objMemberRegistEvt.SendCertificate = x_objMemberRegistEvtModel.SendCertificate;
                    await m_objActivityManager.UpdateMemberRegistEvt(objMemberRegistEvt);
                }
            }
            // Update Community
            else
            {
                objCommunityMember = await m_objActivityManager.GetCommunityRegistEvt(x_objMemberRegistEvtModel.MemberId);
                if (objCommunityMember != null)
                {
                    objCommunityMember.SendCertificate = x_objMemberRegistEvtModel.SendCertificate;
                    await m_objActivityManager.UpdateCommunityRegistEvt(objCommunityMember);
                }
            }
        }

        /// <summary>
        /// Get Check List
        /// </summary>
        /// <param name="x_strMemberId"></param>
        /// <param name="x_strEvtId"></param>
        /// <returns></returns>
        public async Task<List<ActivityChecklistData>> GetCheckList(string x_strMemberId, string x_strEvtId)
        {
            List<ActivityChecklistData> lstEvtCheckList;
            List<ActivityChecklist> lstActivityChecklist;
            MemberDecryptModel objManager;

            lstActivityChecklist = await m_objActivityManager.GetCheckListByEvt(x_strEvtId);
            lstEvtCheckList = new List<ActivityChecklistData>();
            if (lstActivityChecklist != null)
            {
                lstActivityChecklist = lstActivityChecklist.FindAll(obj => obj.MemberId.Equals(x_strMemberId));
                if (lstActivityChecklist != null)
                {
                    foreach (ActivityChecklist objCheckList in lstActivityChecklist)
                    {
                        objManager = MemberInfoMemory.Instant.GetMemberById(objCheckList.ManagerId);

                        lstEvtCheckList.Add(new ActivityChecklistData
                        {
                            ManagerId = objCheckList.ManagerId,
                            EntryTime = objCheckList.EntryTime,
                            EventsId = objCheckList.EventsId,
                            Id = objCheckList.Id,
                            ManagerName = objManager?.GetMemberFullName(),
                            MemberId = objCheckList.MemberId,
                            Score = objCheckList.CalScore(),
                            TimeToLeave = objCheckList.TimeToLeave
                        });
                    }
                }
            }
            return lstEvtCheckList;
        }

        /// <summary>
        /// Member Regis Event
        /// </summary>
        /// <param name="x_strMemberId"></param>
        /// <param name="x_strEvtId"></param>
        /// <returns></returns>
        public async Task<MemberRegistEvtModel> MemberRegisEvent(string x_strMemberId, string x_strEvtId)
        {
            List<MemberRegistEvtModel> lstMemberRegistEvtModel;
            MemberRegistEvt objMemberRegistEvt;
            MemberRegistEvtModel objMemberRegistEvtModel;
            bool bResult;

            if ((x_strMemberId.MongoIdIsValid() == false) || (x_strEvtId.MongoIdIsValid() == false))
            {
                return null;
            }

            if (m_dicMemberRegistEvtModel.ContainsKey(x_strEvtId) == false)
            {
                lstMemberRegistEvtModel = new List<MemberRegistEvtModel>();
                m_dicMemberRegistEvtModel[x_strEvtId] = lstMemberRegistEvtModel;
            }
            else
            {
                lstMemberRegistEvtModel = m_dicMemberRegistEvtModel[x_strEvtId];
            }
            // Find member regist
            objMemberRegistEvtModel = lstMemberRegistEvtModel.Find(obj => obj.MemberId.Equals(x_strMemberId) == true);
            if (objMemberRegistEvtModel != null)
            {
                return objMemberRegistEvtModel;
            }
            // Install
            objMemberRegistEvt = new MemberRegistEvt
            {
                EventId = x_strEvtId,
                MemberId = x_strMemberId
            };
            bResult = await m_objActivityManager.UpdateMemberRegistEvt(objMemberRegistEvt);
            if (bResult == false)
            {
                return null;
            }
            // Convert to model
            objMemberRegistEvtModel = await ConvertMemberToModel(objMemberRegistEvt);
            // Add to list
            lstMemberRegistEvtModel.Add(objMemberRegistEvtModel);
            // delete Leave Of Absence
            await DeleteLeave(x_strMemberId, x_strEvtId);
            return objMemberRegistEvtModel;
        }

        /// <summary>
        /// Delete Member Regist Evt
        /// </summary>
        /// <param name="x_strMemberId"></param>
        /// <returns></returns>
        public async Task<bool> DeleteMemberRegistEvt(string x_strMemberId, string x_strActivityId)
        {
            List<MemberRegistEvtModel> lstMemberRegistEvtModel;
            MemberRegistEvtModel objMemberRegistEvtModel;
            bool bResult;

            if ((x_strMemberId.MongoIdIsValid() == false) || (x_strActivityId.MongoIdIsValid() == false))
            {
                return false;
            }

            if (m_dicMemberRegistEvtModel.ContainsKey(x_strActivityId) == false)
            {
                return false;
            }
            // Find member regist
            lstMemberRegistEvtModel = m_dicMemberRegistEvtModel[x_strActivityId];
            if (lstMemberRegistEvtModel == null)
            {
                return false;
            }
            objMemberRegistEvtModel = lstMemberRegistEvtModel.Find(obj => obj.MemberId.Equals(x_strMemberId) == true);
            if (objMemberRegistEvtModel == null)
            {
                return false;
            }
            // Delete Database
            bResult = await m_objActivityManager.DeleteMemberRegistEvt(objMemberRegistEvtModel.Id);
            if (bResult == true)
            {
                await m_objActivityManager.DeleteCheckListByMemberId(objMemberRegistEvtModel.MemberId, objMemberRegistEvtModel.EventId);
            }
            // Delete memory
            lstMemberRegistEvtModel.RemoveAll(obj => obj.MemberId.Equals(objMemberRegistEvtModel.MemberId));
            return bResult;
        }

        /// <summary>
        /// Community Regis Event
        /// </summary>
        /// <param name="x_objCommunityRegistEvtModel"></param>
        /// <returns></returns>
        public async Task<MemberRegistEvtModel> CommunityRegisEvent(CommunityRegistEvtModel x_objCommunityRegistEvtModel)
        {
            List<MemberRegistEvtModel> lstMemberRegistEvtModel;
            CommunityRegistEvt objCommunityRegistEvt;
            MemberRegistEvtModel objMemberRegistEvtModel;
            string strHashKey;
            bool bResult;

            if (x_objCommunityRegistEvtModel == null)
            {
                return null;
            }

            if (m_dicMemberRegistEvtModel.ContainsKey(x_objCommunityRegistEvtModel.EventId) == false)
            {
                lstMemberRegistEvtModel = new List<MemberRegistEvtModel>();
                m_dicMemberRegistEvtModel[x_objCommunityRegistEvtModel.EventId] = lstMemberRegistEvtModel;
            }
            else
            {
                lstMemberRegistEvtModel = m_dicMemberRegistEvtModel[x_objCommunityRegistEvtModel.EventId];
            }
            // Find member regist
            objMemberRegistEvtModel = lstMemberRegistEvtModel.Find(obj => (obj.StudentId.Equals(x_objCommunityRegistEvtModel.StudentId) == true) ||
                                                                          (obj.Email.Equals(x_objCommunityRegistEvtModel.Email) == true) ||
                                                                          (obj.PhoneNumber.Equals(x_objCommunityRegistEvtModel.PhoneNumber) == true));
            if (objMemberRegistEvtModel != null)
            {
                return null;
            }
            // Install
            objCommunityRegistEvt = new CommunityRegistEvt
            {
                ClassName = x_objCommunityRegistEvtModel.ClassName,
                Email = x_objCommunityRegistEvtModel.Email,
                EventId = x_objCommunityRegistEvtModel.EventId,
                FacebookPath = x_objCommunityRegistEvtModel.FacebookPath,
                FacultyId = x_objCommunityRegistEvtModel.FacultyId,
                FirstName = x_objCommunityRegistEvtModel.FirstName,
                StudentId = x_objCommunityRegistEvtModel.StudentId,
                LastName = x_objCommunityRegistEvtModel.LastName,
                PhoneNumber = x_objCommunityRegistEvtModel.PhoneNumber
            };
            bResult = await m_objActivityManager.UpdateCommunityRegistEvt(objCommunityRegistEvt);
            if (bResult == false)
            {
                return null;
            }
            // Get HashKey
            strHashKey = HashKeyManager.GetMemberHashKey(objCommunityRegistEvt.Id);
            // Encrypt data
            objCommunityRegistEvt = objCommunityRegistEvt.ObjEncript(strHashKey);
            // Update to database
            bResult = await m_objActivityManager.UpdateCommunityRegistEvt(objCommunityRegistEvt);
            if (bResult == false)
            {
                return null;
            }
            // Convert to model
            objMemberRegistEvtModel = await CommunityDbToModel(objCommunityRegistEvt);
            lstMemberRegistEvtModel.Add(objMemberRegistEvtModel);
            return objMemberRegistEvtModel;
        }

        /// <summary>
        /// Get Member Regis Evt
        /// </summary>
        /// <param name="x_strMemberId"></param>
        /// <param name="x_strActivityId"></param>
        /// <returns></returns>
        public MemberRegistEvtModel GetMemberRegisEvt(string x_strMemberId, string x_strActivityId)
        {
            MemberRegistEvtModel objMemberRegistEvtModel;
            List<MemberRegistEvtModel> lstMemberRegistEvtModel;

            if (x_strActivityId.MongoIdIsValid() == false)
            {
                return null;
            }

            if (m_dicMemberRegistEvtModel.ContainsKey(x_strActivityId) == false)
            {
                return null;
            }

            lstMemberRegistEvtModel = m_dicMemberRegistEvtModel[x_strActivityId];
            if (x_strMemberId.MongoIdIsValid() == true)
            {
                objMemberRegistEvtModel = lstMemberRegistEvtModel.Find(obj => obj.MemberId.Equals(x_strMemberId) == true);
            }
            else
            {
                objMemberRegistEvtModel = lstMemberRegistEvtModel.Find(obj => obj.StudentId.Equals(x_strMemberId) == true);
            }

            return objMemberRegistEvtModel;
        }

        /// <summary>
        /// Update CheckIn
        /// </summary>
        /// <param name="x_objActivityChecklistData"></param>
        /// <returns></returns>
        public async Task<bool> UpdateCheckIn(ActivityChecklistData x_objActivityChecklistData)
        {
            bool bResult;
            ActivityChecklist objActivityChecklist;
            MemberRegistEvtModel objMemberRegistEvtModel;

            // Update database
            objActivityChecklist = new ActivityChecklist
            {
                Id = x_objActivityChecklistData.Id,
                EntryTime = x_objActivityChecklistData.EntryTime,
                EventsId = x_objActivityChecklistData.EventsId,
                ManagerId = x_objActivityChecklistData.ManagerId,
                MemberId = x_objActivityChecklistData.MemberId,
                TimeToLeave = x_objActivityChecklistData.TimeToLeave
            };
            bResult = await m_objActivityManager.UpdateEventCheckList(objActivityChecklist);
            if (bResult == false)
            {
                return false;
            }

            // 1. Is check out
            if (x_objActivityChecklistData.Id.MongoIdIsValid() == true)
            {
                x_objActivityChecklistData.Score = x_objActivityChecklistData.CalScore();
            }
            // 2. Is Check in
            else
            {
                objMemberRegistEvtModel = await MemberRegisEvent(x_objActivityChecklistData.MemberId, x_objActivityChecklistData.EventsId);
                if (objMemberRegistEvtModel == null)
                {
                    return false;
                }
                x_objActivityChecklistData.Score = x_objActivityChecklistData.CalScore();
                x_objActivityChecklistData.Id = objActivityChecklist.Id;
                objMemberRegistEvtModel.EvtCheckList.Add(x_objActivityChecklistData);
            }
            return true;
        }

        /// <summary>
        /// Remove Check In
        /// </summary>
        /// <param name="x_strMemberId"></param>
        /// <param name="x_strActivityId"></param>
        /// <param name="x_strCheckInId"></param>
        /// <returns></returns>
        public async Task<bool> DeleteCheckIn(string x_strMemberId, string x_strActivityId, string x_strCheckInId)
        {
            bool bResult;
            int nResult;
            MemberRegistEvtModel objMemberRegistEvtModel;

            if ((x_strMemberId.MongoIdIsValid() == false) || (x_strActivityId.MongoIdIsValid() == false) || (x_strCheckInId.MongoIdIsValid() == false))
            {
                return false;
            }

            objMemberRegistEvtModel = GetMemberRegisEvt(x_strMemberId, x_strActivityId);
            if (objMemberRegistEvtModel == null)
            {
                return false;
            }

            nResult = objMemberRegistEvtModel.EvtCheckList.RemoveAll(obj => obj.Id.Equals(x_strCheckInId) == true);
            if (nResult <= 0)
            {
                return false;
            }

            objMemberRegistEvtModel.TotalScore = objMemberRegistEvtModel.EvtCheckList.Sum(obj => obj.Score);

            bResult = await m_objActivityManager.DeleteCheckListById(x_strCheckInId);
            return bResult;
        }

        /// <summary>
        /// Get List Member Regist Evt
        /// </summary>
        /// <param name="x_strActivityId"></param>
        /// <returns></returns>
        public List<MemberRegistEvtModel> GetListMemberRegistEvt(string x_strActivityId)
        {
            if (m_dicMemberRegistEvtModel.ContainsKey(x_strActivityId) == true)
            {
                return m_dicMemberRegistEvtModel[x_strActivityId];
            }
            return null;
        }

        /// <summary>
        /// Check Regist Exist
        /// </summary>
        /// <param name="x_strStudentId"></param>
        /// <param name="x_strActivityId"></param>
        /// <returns></returns>
        public bool CheckRegistExist(string x_strStudentId, string x_strActivityId)
        {
            bool bResult;

            if (m_dicMemberRegistEvtModel.ContainsKey(x_strActivityId) == false)
            {
                return false;
            }

            if (m_dicMemberRegistEvtModel[x_strActivityId] == null)
            {
                return false;
            }

            bResult = m_dicMemberRegistEvtModel[x_strActivityId].Any(obj => obj.StudentId.Equals(x_strStudentId) == true);
            return bResult;
        }

        /// <summary>
        /// Update Leave
        /// </summary>
        /// <param name="x_objLeaveOfAbsence"></param>
        /// <returns></returns>
        public async Task<bool> UpdateLeave(LeaveOfAbsence x_objLeaveOfAbsence)
        {
            List<LeaveOfAbsenceModel> lstLeaveOfAbsenceModel;
            LeaveOfAbsenceModel objLeaveOfAbsenceModel;
            bool bResult;

            if (x_objLeaveOfAbsence == null)
            {
                return false;
            }

            // Update to database
            bResult = await m_objActivityManager.AddLeaveOfAbsence(x_objLeaveOfAbsence);
            if (bResult == false)
            {
                return false;
            }

            if (m_dicLeaveOfAbsenceModel.ContainsKey(x_objLeaveOfAbsence.EventId) == false)
            {
                lstLeaveOfAbsenceModel = new List<LeaveOfAbsenceModel>();
                m_dicLeaveOfAbsenceModel[x_objLeaveOfAbsence.EventId] = lstLeaveOfAbsenceModel;
            }
            else
            {
                lstLeaveOfAbsenceModel = m_dicLeaveOfAbsenceModel[x_objLeaveOfAbsence.EventId];
            }

            // Update to model
            objLeaveOfAbsenceModel = ConvertLeaveToModel(x_objLeaveOfAbsence);
            if (objLeaveOfAbsenceModel == null)
            {
                return false;
            }
            lstLeaveOfAbsenceModel.Add(objLeaveOfAbsenceModel);

            await DeleteMemberRegistEvt(objLeaveOfAbsenceModel.MemberId, objLeaveOfAbsenceModel.EventId);
            return true;
        }

        /// <summary>
        /// Delete Leave
        /// </summary>
        /// <param name="x_strMemberId"></param>
        /// <param name="x_strActivityId"></param>
        /// <returns></returns>
        public async Task<bool> DeleteLeave(string x_strMemberId, string x_strActivityId)
        {
            List<LeaveOfAbsenceModel> lstLeaveOfAbsenceModel;
            if ((x_strMemberId.MongoIdIsValid() == false) || (x_strActivityId.MongoIdIsValid() == false))
            {
                return false;
            }

            if (m_dicLeaveOfAbsenceModel.ContainsKey(x_strActivityId) == false)
            {
                return false;
            }
            lstLeaveOfAbsenceModel = m_dicLeaveOfAbsenceModel[x_strActivityId];
            lstLeaveOfAbsenceModel.RemoveAll(obj => obj.MemberId.Equals(x_strMemberId) == true);

            await m_objActivityManager.DeleteLeaveOfAbsenceById(x_strMemberId, x_strActivityId);
            return true;
        }

        /// <summary>
        /// Get List Leave By Activity Id
        /// </summary>
        /// <param name="x_strActivityId"></param>
        /// <returns></returns>
        public List<LeaveOfAbsenceModel> GetListLeaveByActivityId(string x_strActivityId)
        {
            List<LeaveOfAbsenceModel> lstLeaveOfAbsenceModel;

            if (x_strActivityId.MongoIdIsValid() == false)
            {
                return null;
            }

            if (m_dicLeaveOfAbsenceModel.ContainsKey(x_strActivityId) == false)
            {
                return null;
            }
            lstLeaveOfAbsenceModel = m_dicLeaveOfAbsenceModel[x_strActivityId];
            return lstLeaveOfAbsenceModel;
        }

        /// <summary>
        /// Update Faculty
        /// </summary>
        /// <param name="x_objFacultys"></param>
        /// <exception cref="NotImplementedException"></exception>
        public void UpdateFaculty(Facultys x_objFacultys)
        {
            if (m_dicMemberRegistEvtModel == null)
            {
                return;
            }

            foreach (KeyValuePair<string, List<MemberRegistEvtModel>> objMemberList in m_dicMemberRegistEvtModel)
            {
                if (objMemberList.Value == null || objMemberList.Value.Count == 0)
                {
                    continue;
                }
                foreach (MemberRegistEvtModel objMember in objMemberList.Value)
                {
                    if (objMember.Faculty.Id.Equals(x_objFacultys.Id) == true)
                    {
                        objMember.Faculty.FacultyName = x_objFacultys.FacultyName;
                    }
                }
            }
        }
        #endregion
    }
}
