﻿using DTUSVCv3_EntityFramework;

namespace DTUSVCv3_MemoryData.EventData
{
    [Serializable]
    public class MemberRegistEvtModel
    {
        public MemberRegistEvtModel()
        {
            EvtCheckList = new List<ActivityChecklistData>();
            TotalScore = 0;
        }
        public string Id { get; set; }
        public string MemberId { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public Facultys Faculty { get; set; }
        public string ClassName { get; set; }
        public string StudentId { get; set; }
        public string FacebookPath { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime SendCertificate { get; set; }
        public string EventId { get; set; }
        public double TotalScore { get; set; }
        public bool IsMember { get; set; }
        public List<ActivityChecklistData> EvtCheckList { get; set; }

        /// <summary>
        /// Get Member Full Name
        /// </summary>
        /// <returns></returns>
        public string GetMemberFullName()
        {
            return string.Format("{0} {1}", FirstName, LastName);
        }

        /// <summary>
        /// Get Check In
        /// </summary>
        /// <param name="x_objCheckInDate"></param>
        /// <returns></returns>
        public bool GetCheckIn(DateTime x_objCheckInDate, out ActivityChecklistData x_objActivityChecklistData)
        {
            const string FORMAT_DATE = "dd/MM/yyyy";

            ActivityChecklistData objActivityChecklistData;
            double dCalTime;
            string strDateParam;
            string strDateCheckin;

            x_objActivityChecklistData = null;
            // Check in
            if (EvtCheckList == null)
            {
                return false;
            }
            // Get check in Entry Time max
            objActivityChecklistData = EvtCheckList.MaxBy(obj => obj.EntryTime);
            // Check in
            if (objActivityChecklistData == null)
            {
                return false;
            }
            // Check date time beet ween current date
            strDateParam = x_objCheckInDate.ToString(FORMAT_DATE);
            strDateCheckin = objActivityChecklistData.EntryTime.ToString(FORMAT_DATE);
            // Check in
            if (strDateParam.Equals(strDateCheckin) == false)
            {
                return false;
            }

            // Check Entry time and Out time
            dCalTime = x_objCheckInDate.Subtract(objActivityChecklistData.TimeToLeave).TotalMinutes;
            // Check out
            if ((objActivityChecklistData.EntryTime == objActivityChecklistData.TimeToLeave) && (dCalTime > 15))
            {
                x_objActivityChecklistData = objActivityChecklistData;
                return true;
            }
            // Check in
            if ((objActivityChecklistData.EntryTime != objActivityChecklistData.TimeToLeave) && (dCalTime > 5))
            {
                return false;
            }
            // Does not check in and check out
            return true;
        }
    }

    [Serializable]
    public class ActivityChecklistData : ActivityChecklist
    {
        public ActivityChecklistData() : base()
        {
            Score = CalScore();
        }

        public string ManagerName { get; set; }
        public double Score { get; set; }
    }

    public class CommunityRegistEvtModel
    {
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public string FacultyId { get; set; }
        public string ClassName { get; set; }
        public string StudentId { get; set; }
        public string FacebookPath { get; set; }
        public string EventId { get; set; }
    }

    public class LeaveOfAbsenceModel
    {
        public string Id { get; set; }
        public string MemberId { get; set; }
        public string EventId { get; set; }
        public string StudentId { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string Reason { get; set; }
        public string FacebookPath { get; set; }
        public DateTime CreateDate { get; set; }
    }
}
