﻿using DTUSVCv3_Library;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTUSVCv3_MemoryData.ForgotPasswData
{
    public static class ForgotPasswdManager
    {
        #region Types
        private const string STR_CHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        #endregion

        #region Fields
        private static List<ForgotPasswdModel> m_lstForgotPasswdModel;
        private static List<CaptchaModel> m_lstCaptchaModel;
        #endregion

        #region Methods Captcha
        /// <summary>
        /// Set Captcha
        /// </summary>
        /// <param name="x_strToken"></param>
        /// <returns></returns>
        public static CaptchaModel SetCaptcha(string x_strToken)
        {
            CaptchaModel objCaptchaModel;
            Random objRandom;
            string strCaptchaCode;
            string strCaptchaPath;
            string strCaptchaFileName;

            objCaptchaModel = GetCaptcha(x_strToken);
            if (objCaptchaModel == null)
            {
                objCaptchaModel = new CaptchaModel();
                objCaptchaModel.Token = GetCaptcahrToken();
                m_lstCaptchaModel.Add(objCaptchaModel);
            }
            else
            {
                if (File.Exists(objCaptchaModel.CaptchaPath) == true)
                {
                    File.Delete(objCaptchaModel.CaptchaPath);
                }
            }

            objRandom = new Random();
            strCaptchaCode = new string(Enumerable.Repeat(STR_CHARS, 6).Select(str => str[objRandom.Next(str.Length)]).ToArray());
            strCaptchaFileName = string.Format("{0}_{1}.jpg", DateTime.Now.Ticks, objRandom.Next(1000, 9999));
            strCaptchaPath = FileHelper.GetFilePath(SysFolder.CAPTCHA_FOLDER, strCaptchaFileName);

            ImageHelper.CreateCaptcha(strCaptchaPath, strCaptchaCode);
            objCaptchaModel.CaptchaPath = strCaptchaPath;
            objCaptchaModel.CaptchaCode = strCaptchaCode;

            return objCaptchaModel;
        }

        /// <summary>
        /// Get Token
        /// </summary>
        /// <returns></returns>
        private static string GetCaptcahrToken()
        {
            CaptchaModel objCaptchaModel;
            string strToken;

            strToken = ForgotPasswdModel.CreateToken();
            objCaptchaModel = m_lstCaptchaModel.Find(obj => obj.Token.Equals(strToken) == true);

            if (objCaptchaModel == null)
            {
                return strToken;
            }

            strToken = GetCaptcahrToken();
            return strToken;
        }

        /// <summary>
        /// Get Captcha
        /// </summary>
        /// <param name="x_strToken"></param>
        /// <returns></returns>
        public static CaptchaModel GetCaptcha(string x_strToken)
        {
            CaptchaModel objCaptchaModel;

            if (m_lstCaptchaModel == null )
            {
                m_lstCaptchaModel = new List<CaptchaModel>();
                return null;
            }

            if (string.IsNullOrWhiteSpace(x_strToken) == true)
            {
                return null;
            }

            objCaptchaModel = m_lstCaptchaModel.FirstOrDefault(obj => obj.Token.Equals(x_strToken) == true);
            return objCaptchaModel;
        }

        /// <summary>
        /// Delete Captcha
        /// </summary>
        /// <param name="x_objCatcha"></param>
        public static void DeleteCaptcha(CaptchaModel x_objCatcha)
        {
            if (x_objCatcha == null)
            {
                return;
            }

            if (File.Exists(x_objCatcha.CaptchaPath) == true)
            {
                File.Delete(x_objCatcha.CaptchaPath);
            }

            if (m_lstCaptchaModel != null)
            {
                m_lstCaptchaModel.RemoveAll(obj => obj.Token.Equals(x_objCatcha.Token) == true);
            }
        }
        #endregion

        #region Methods Forgot
        /// <summary>
        /// Set Forgot Passwd
        /// </summary>
        /// <param name="x_strAccountId"></param>
        public static ForgotPasswdModel SetForgotPasswd(string x_strAccountId)
        {
            ForgotPasswdModel objForgotPasswdModel;
            string strToken;

            if (m_lstForgotPasswdModel == null)
            {
                m_lstForgotPasswdModel = new List<ForgotPasswdModel>();
            }

            m_lstForgotPasswdModel.RemoveAll(obj => obj.AccountId.Equals(x_strAccountId));

            strToken = GetToken();
            objForgotPasswdModel = new ForgotPasswdModel(x_strAccountId, strToken);
            m_lstForgotPasswdModel.Add(objForgotPasswdModel);

            return objForgotPasswdModel;
        }

        /// <summary>
        /// Get Token
        /// </summary>
        /// <returns></returns>
        private static string GetToken()
        {
            ForgotPasswdModel objForgotPasswdModel;
            string strToken;

            strToken = ForgotPasswdModel.CreateToken();
            objForgotPasswdModel = m_lstForgotPasswdModel.Find(obj => obj.Token.Equals(strToken) == true);

            if (objForgotPasswdModel == null)
            {
                return strToken;
            }

            strToken = GetToken();
            return strToken;
        }

        /// <summary>
        /// Get Forgot Passwd
        /// </summary>
        /// <param name="x_strToken"></param>
        /// <returns></returns>
        public static ForgotPasswdModel GetForgotPasswd(string x_strToken)
        {
            ForgotPasswdModel objForgotPasswdModel;

            if (m_lstForgotPasswdModel == null)
            {
                m_lstForgotPasswdModel = new List<ForgotPasswdModel>();
            }

            objForgotPasswdModel = m_lstForgotPasswdModel.Find(obj => obj.Token.Equals(x_strToken) == true);
            return objForgotPasswdModel;
        }

        /// <summary>
        /// Remove Token
        /// </summary>
        /// <param name="x_strAccountId"></param>
        public static void RemoveToken(string x_strAccountId)
        {
            if (m_lstForgotPasswdModel == null)
            {
                m_lstForgotPasswdModel = new List<ForgotPasswdModel>();
            }

            m_lstForgotPasswdModel.RemoveAll(obj => obj.AccountId.Equals(x_strAccountId));
        }
        #endregion
    }
}
