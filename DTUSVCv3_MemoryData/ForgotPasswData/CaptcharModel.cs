﻿using DTUSVCv3_Library;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTUSVCv3_MemoryData.ForgotPasswData
{
    public class CaptchaModel
    {
        public CaptchaModel()
        {
            CreateDate = DateTimeHelper.GetCurrentDateTimeVN();
        }

        public string Token { get; set; }
        public string CaptchaCode { get; set; }
        public string CaptchaPath { get; set; }
        public DateTime CreateDate { get; set; }
    }
}
