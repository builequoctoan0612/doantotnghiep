﻿using DTUSVCv3_Library;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTUSVCv3_MemoryData
{
    public class ForgotPasswdModel
    {
        public ForgotPasswdModel()
        {
            CreateDate = DateTimeHelper.GetCurrentDateTimeVN();
            DeadLine = CreateDate.AddHours(1);
        }

        public ForgotPasswdModel(string x_strAccountId, string x_strToken)
        {
            AccountId = x_strAccountId;
            Token = x_strToken;
            CreateDate = DateTimeHelper.GetCurrentDateTimeVN();
            DeadLine = CreateDate.AddHours(1);
        }


        #region Properties
        public string AccountId { get; set; }
        public string Token { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime DeadLine { get; set; }
        #endregion

        #region Types
        private const string STR_CHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        #endregion

        #region Methods
        /// <summary>
        /// Create Token
        /// </summary>
        public static string CreateToken()
        {
            string strToken;
            Random objRandom;

            objRandom = new Random();
            // Random 32 chars
            // There are 62^64 ways to choose 128 characters from a set of 62 elements. Probability to generate 2 identical token is 1/62^64
            strToken = new string(Enumerable.Repeat(STR_CHARS, 64).Select(str => str[objRandom.Next(str.Length)]).ToArray());

            return strToken;
        }
        #endregion
    }
}
